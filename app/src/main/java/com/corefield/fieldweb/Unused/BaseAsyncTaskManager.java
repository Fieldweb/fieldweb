package com.corefield.fieldweb.Unused;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;

import java.util.ArrayList;

/**
 * Base async task manager is a singleton class to manage task stack based on it status
 */
public class BaseAsyncTaskManager {
    private static volatile BaseAsyncTaskManager fwAsyncTaskManager;

    private BaseAsyncTaskManager() {
    }

    /**
     * Get instance of BaseAsyncTaskManager
     *
     * @return instance of BaseAsyncTaskManager
     */
    public static BaseAsyncTaskManager getInstance() {
        if (fwAsyncTaskManager == null) { //if there is no instance available... create new one
            fwAsyncTaskManager = new BaseAsyncTaskManager();
        }

        return fwAsyncTaskManager;
    }

    /**
     * List of task
     */
    public ArrayList<BaseAsyncTask> fwAsyncTasks = new ArrayList<>();

    /**
     * Add task to list
     *
     * @param fwAsyncTask base async task
     */
    /*public void addAsyncTask(BaseAsyncTask fwAsyncTask) {
        fwAsyncTask.TaskState = BaseAsyncTask.ISRUNNING;
        fwAsyncTasks.add(fwAsyncTask);
    }*/

    /**
     * Remove task from list
     *
     * @param index base async task
     */
//    public void removeAsyncTask(int index) {
//        if (fwAsyncTasks != null && fwAsyncTasks.size() > 0 && fwAsyncTasks.get(index).TaskState) {
//            fwAsyncTasks.get(index).TaskState = BaseAsyncTask.ISSTOPED;
//            fwAsyncTasks.remove(index);
//        }
//    }

    /**
     * Remove and stop all task from list task
     */
//    public void flushAllAsyncTask() {
//        if (fwAsyncTasks != null && fwAsyncTasks.size() > 0) {
//            for (BaseAsyncTask fwAsyncTask : fwAsyncTasks) {
//                if (isRunning(fwAsyncTask)) {
//                    fwAsyncTask.cancel(true);
//                    fwAsyncTask.TaskState = BaseAsyncTask.ISSTOPED;
//                }
//            }
//            fwAsyncTasks.clear();
//        }
//    }

    /**
     * Get the status of current task
     *
     * @param fwAsyncTask Current task
     * @return status of the task RUNNING or STOPPED
     */
//    public boolean isRunning(BaseAsyncTask fwAsyncTask) {
//        return fwAsyncTask.TaskState;
//    }

    /**
     * Get the priority of current task
     *
     * @param fwAsyncTask Current task
     * @return priority of the task LOW,MEDIUM or HIGH
     */
//    public BaseAsyncTask.Priority getPriority(BaseAsyncTask fwAsyncTask) {
//        return fwAsyncTask.priority;
//    }

    public ArrayList<BaseAsyncTask> getAllRunningAsyncTask() {
        return null;
    }

}
