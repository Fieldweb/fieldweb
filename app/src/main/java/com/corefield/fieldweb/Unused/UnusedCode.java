package com.corefield.fieldweb.Unused;

import android.content.Context;

public class UnusedCode {

    //URLConnectionRequest.java
    /*switch (response.statusCode) {
                case HttpURLConnection.HTTP_OK: //200//TODO Remove legacy library and apply HttpsURLConnection status code
                    break;

                case HttpURLConnection.HTTP_CREATED: //201
                    FWLogger.logInfo(TAG, String.format("%d -- Created -- %s", HttpURLConnection.HTTP_CREATED, urlString));
                    break;

                case HttpURLConnection.HTTP_ACCEPTED: //202
                    FWLogger.logInfo(TAG, String.format("%d -- Accepted -- %s", HttpURLConnection.HTTP_ACCEPTED, urlString));
                    break;

                case HttpURLConnection.HTTP_NOT_AUTHORITATIVE: //203
                    throw new HTTPErrorException(HttpURLConnection.HTTP_NOT_AUTHORITATIVE, String.format("%d -- Not Authoritative -- %s -- Response: %s", HttpURLConnection.HTTP_NOT_AUTHORITATIVE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_NO_CONTENT: //204
                    FWLogger.logInfo(TAG, String.format("%d -- No Content -- %s", HttpURLConnection.HTTP_NO_CONTENT, urlString));
                    break;

                case HttpURLConnection.HTTP_RESET: //205
                    FWLogger.logInfo(TAG, String.format("%d -- Reset -- %s", HttpURLConnection.HTTP_RESET, urlString));
                    break;

                case HttpURLConnection.HTTP_PARTIAL: //206
                    FWLogger.logInfo(TAG, String.format("%d -- Partial -- %s", HttpURLConnection.HTTP_PARTIAL, urlString));
                    break;

                case HttpURLConnection.HTTP_MULT_CHOICE: //300
                    FWLogger.logInfo(TAG, String.format("%d -- Multiple Choices -- %s", HttpURLConnection.HTTP_MULT_CHOICE, urlString));
                    break;

                case HttpURLConnection.HTTP_MOVED_PERM: //301
                    throw new HTTPErrorException(HttpURLConnection.HTTP_MOVED_PERM, String.format("%d -- Move Permanently -- %s -- Response: %s", HttpURLConnection.HTTP_MOVED_PERM, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_MOVED_TEMP: //302
                    throw new HTTPErrorException(HttpURLConnection.HTTP_MOVED_TEMP, String.format("%d -- Move Temporarily -- %s -- Response: %s", HttpURLConnection.HTTP_MOVED_PERM, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_SEE_OTHER: //303
                    FWLogger.logInfo(TAG, String.format("%d -- See Other -- %s", HttpURLConnection.HTTP_SEE_OTHER, urlString));
                    break;

                case HttpURLConnection.HTTP_NOT_MODIFIED: //304
                    FWLogger.logInfo(TAG, String.format("%d -- Not Modified -- %s", HttpURLConnection.HTTP_NOT_MODIFIED, urlString));
                    break;

                case HttpURLConnection.HTTP_USE_PROXY: //305
                    FWLogger.logInfo(TAG, String.format("%d -- Use Proxy -- %s", HttpURLConnection.HTTP_USE_PROXY, urlString));
                    break;

                case HttpURLConnection.HTTP_BAD_REQUEST: //400
                    throw new HTTPErrorException(HttpURLConnection.HTTP_BAD_REQUEST, String.format("%d -- Bad Request -- %s -- Response: %s", HttpURLConnection.HTTP_BAD_REQUEST, urlString, responseBuilder.toString()));

                    //NOTE : Don not through the exception as it has been already handled by maintenance screen
                //case HttpURLConnection.HTTP_UNAUTHORIZED: //401
                    //throw new HTTPErrorException(HttpURLConnection.HTTP_UNAUTHORIZED, String.format("%d -- Unauthorized -- %s -- Response: %s", HttpURLConnection.HTTP_UNAUTHORIZED, urlString, responseBuilder.toString()));
                    //FWLogger.logInfo(logging, String.format("%d -- Unauthorized -- %s", HttpURLConnection.HTTP_UNAUTHORIZED, urlString));
                    //break;

                case HttpURLConnection.HTTP_PAYMENT_REQUIRED: //402
                    FWLogger.logInfo(TAG, String.format("%d -- Payment Required -- %s", HttpURLConnection.HTTP_PAYMENT_REQUIRED, urlString));
                    break;

                case HttpURLConnection.HTTP_FORBIDDEN: //403
                    throw new HTTPErrorException(HttpURLConnection.HTTP_FORBIDDEN, String.format("%d -- Forbidden -- %s -- Response: %s", HttpURLConnection.HTTP_FORBIDDEN, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_NOT_FOUND: //404
                    throw new HTTPErrorException(HttpURLConnection.HTTP_NOT_FOUND, String.format("%d -- Not Found -- %s -- Response: %s", HttpURLConnection.HTTP_NOT_FOUND, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_BAD_METHOD: //405
                    throw new HTTPErrorException(HttpURLConnection.HTTP_BAD_METHOD, String.format("%d -- Bad Method -- %s -- Response: %s", HttpURLConnection.HTTP_BAD_METHOD, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_NOT_ACCEPTABLE: //406
                    throw new HTTPErrorException(HttpURLConnection.HTTP_NOT_ACCEPTABLE, String.format("%d - Not Acceptable -- %s -- Response: %s", HttpURLConnection.HTTP_NOT_ACCEPTABLE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_PROXY_AUTH: //407
                    throw new HTTPErrorException(HttpURLConnection.HTTP_PROXY_AUTH, String.format("%d -- Proxy Authentication Required -- %s -- Response: %s", HttpURLConnection.HTTP_PROXY_AUTH, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_CLIENT_TIMEOUT: //408
                    throw new HTTPErrorException(HttpURLConnection.HTTP_CLIENT_TIMEOUT, String.format("%d -- Client Timeout -- %s -- Response: %s", HttpURLConnection.HTTP_CLIENT_TIMEOUT, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_CONFLICT: //409
                    throw new HTTPErrorException(HttpURLConnection.HTTP_CONFLICT, String.format("%d -- Conflict -- %s -- Response: %s", HttpURLConnection.HTTP_CONFLICT, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_GONE: //410
                    throw new HTTPErrorException(HttpURLConnection.HTTP_GONE, String.format("%d -- Gone -- %s -- Response: %s", HttpURLConnection.HTTP_GONE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_LENGTH_REQUIRED: //411
                    throw new HTTPErrorException(HttpURLConnection.HTTP_LENGTH_REQUIRED, String.format("%d -- Length Required -- %s -- Response: %s", HttpURLConnection.HTTP_LENGTH_REQUIRED, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_PRECON_FAILED: //412
                    throw new HTTPErrorException(HttpURLConnection.HTTP_PRECON_FAILED, String.format("%d -- Precondition Failed -- %s -- Response: %s", HttpURLConnection.HTTP_PRECON_FAILED, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_ENTITY_TOO_LARGE: //413
                    throw new HTTPErrorException(HttpURLConnection.HTTP_ENTITY_TOO_LARGE, String.format("%d -- Entity Too Large -- %s -- Response: %s", HttpURLConnection.HTTP_ENTITY_TOO_LARGE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_REQ_TOO_LONG: //414
                    throw new HTTPErrorException(HttpURLConnection.HTTP_REQ_TOO_LONG, String.format("%d -- Request Too Long -- %s -- Response: %s", HttpURLConnection.HTTP_REQ_TOO_LONG, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_UNSUPPORTED_TYPE: //415
                    throw new HTTPErrorException(HttpURLConnection.HTTP_UNSUPPORTED_TYPE, String.format("%d -- Unsupported Type -- %s -- Response: %s", HttpURLConnection.HTTP_UNSUPPORTED_TYPE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_INTERNAL_ERROR: //500
                    throw new HTTPErrorException(HttpURLConnection.HTTP_INTERNAL_ERROR, String.format("%d -- Internal Error -- URL: %s -- Response: %s", HttpURLConnection.HTTP_INTERNAL_ERROR, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_NOT_IMPLEMENTED: //501
                    throw new HTTPErrorException(HttpURLConnection.HTTP_NOT_IMPLEMENTED, String.format("%d -- Not Implemented -- %s -- Response: %s", HttpURLConnection.HTTP_NOT_IMPLEMENTED, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_BAD_GATEWAY: //502
                    throw new HTTPErrorException(HttpURLConnection.HTTP_BAD_GATEWAY, String.format("%d -- Bad Gateway -- %s -- Response: %s", HttpURLConnection.HTTP_BAD_GATEWAY, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_UNAVAILABLE: //503
                    throw new HTTPErrorException(HttpURLConnection.HTTP_UNAVAILABLE, String.format("%d -- Service Unavailable -- %s -- Response: %s", HttpURLConnection.HTTP_UNAVAILABLE, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_GATEWAY_TIMEOUT: //504
                    throw new HTTPErrorException(HttpURLConnection.HTTP_GATEWAY_TIMEOUT, String.format("%d -- Gateway Timeout -- %s -- Response: %s", HttpURLConnection.HTTP_GATEWAY_TIMEOUT, urlString, responseBuilder.toString()));

                case HttpURLConnection.HTTP_VERSION: //505
                    throw new HTTPErrorException(HttpURLConnection.HTTP_VERSION, String.format("%d -- Version Not Supported -- %s -- Response: %s", HttpURLConnection.HTTP_VERSION, urlString, responseBuilder.toString()));

                case Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE: //999
                    throw new HTTPErrorException(HttpURLConnection.HTTP_VERSION, String.format("%d -- Version Not Supported -- %s -- Response: %s", Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE, urlString, responseBuilder.toString()));

                default:
                    FWLogger.logInfo(TAG, "Unexpected status code: " + response.statusCode);
                    break;
            }*/


    //this same method is used in every adapter but with different ResultData
    /*public void updateList(List<EnquiryList.ResultData> list) {
        mResultDataListFilter = list;
        notifyDataSetChanged();
    }*/

    //NotificationListAdapter.java
    /*public void addItem(Notification.ResultData resultData, int position) {
        mResultData.add(position, resultData);
        notifyItemInserted(position);
    }*/

    //EnquiryListFragment.java
    /* @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!mSearchView.isIconified()) {
            mSearchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }*/

    //ExpenseCalendarFragment.java in OnDestroy()
    /*mRootView = null;
        mTabLayoutTaskByTaskStatus = null;
        mViewPagerTaskByTaskType = null;
        mTasksLists.clear();
        mTasksLists = null;*/

    //ProfileFragment.java
    //  manageBackPress();
    /* private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    HomeActivity.isOwner = true;
                    return false;
                }
                return true;
            }
        });
    }*/
    //        mLastNameTextView = mRootView.findViewById(R.id.textView_lastname);  - inIt()
    //        mLastNameTextView.setText(mUserDetails.getLastName()); - updateUserField()
    // File photo = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_profile.jpg"); - captureImage()

    //TechAdminFragment.java
    //transaction.isAddToBackStackAllowed(); - loadFragment(Fragment fragment)

    //TechItemInventoryFragment.java
    // onTaskComplete():
    /*else if (classType.equalsIgnoreCase(UnAssignItem.class.getSimpleName())) {
            Gson gson = new Gson();
            UnAssignItem unAssignItem = gson.fromJson(urlConnectionResponse.resultData, UnAssignItem.class);
            if (unAssignItem != null) {
                if (unAssignItem.getCode().equalsIgnoreCase("200") && unAssignItem.getMessage().equalsIgnoreCase("Item Unassigned sucessfully")) {
                    Toast.makeText(getContext(), unAssignItem.getMessage(), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(logging, "Item UnAssign successfully.");
                } else {
                    Toast.makeText(getContext(), unAssignItem.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        }*/
    // onClick():
    /*ItemIssueList.ResultData itemList = mItemsLists.get(position);
        mItemID = itemList.getId();
        FWLogger.logInfo(logging, "Item ID on click" + itemList.getId());
        AlertDialog.unAssignItem(getContext(), this, itemList.getName());*/

    //AMCDetailsFragment.java
//    onCreateView():
     /*if (getArguments().getSerializable(EnquiryList.KEY_SERIALIZABLE) != null) {
         mResultData = (EnquiryList.ResultData) getArguments().getSerializable(EnquiryList.KEY_SERIALIZABLE);
         setDate();
      }*/
    //        mTextViewCustomerPinCode = mRootView.findViewById(R.id.textView_customer_pin_code);

    //AddBulkTechValidationAsyncTask.java
    /*String userId = "", techFullName = "";
                long contactNo = 0;
                //Assign value
                techFullName = (String) objects[0];
                contactNo = (long) objects[1];
                userId = (String) objects[2];
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("Name", techFullName);
                    jsonObject.put("ContactNo", contactNo);
                    jsonObject.put("CreatedBy", userId);
                } catch (JSONException e) {
                    jsonObject = null;
                    e.printStackTrace();
                }
     FWLogger.logInfo(logging, "jsonObject = " + jsonObject.toString());*/

    //OwnerDashboardFragment.java
    /*private String getScreenSize() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        String size;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                size = "Large screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                size = "Normal screen";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                size = "Small screen";
                break;
            default:
                size = "Screen size is neither large, normal or small";
        }
        return size;
    }*/

    //NotificationFragment.java
    /* Do not delete this code may helpful in future if updateItem does not work
    //Add to existing list on receive from sync
    if (notification.getResultData().size() > mResultData.size()) {
        FWLogger.logInfo(TAG, "new result data > old result data");
        int noOfNewItems = notification.getResultData().size() - mResultData.size();
        for (int i = 0; i < noOfNewItems; i++) {
            //mResultData.add(notification.getResultData().get(i));
            FWLogger.logInfo(TAG, "loop on pos = " + i);
            if (mNotificationListAdapter != null) {
                FWLogger.logInfo(TAG, "item to add at pos = " + i);
                FWLogger.logInfo(TAG, "item to add in existing list = " + notification.getResultData().get(i).getTaskName());
                //mNotificationListAdapter.addItem(notification.getResultData().get(i), 0);
                mNotificationListAdapter.updateItems(notification.getResultData());
                //mRecyclerViewNotification.smoothScrollToPosition(0);
                FWLogger.logInfo(TAG, "old list size after adding  = " + mResultData.size());
                clearBadge();
            }
        }
    } else if (notification.getResultData().size() < mResultData.size()) {
        FWLogger.logInfo(TAG, "new result data < old result data");
        mNotificationListAdapter.updateItems(notification.getResultData());
        //setNotificationAdapterData(gson, urlConnectionResponse);
    } else {
        FWLogger.logInfo(TAG, "new result data == old result data");
        if (mNotificationListAdapter != null) {
            FWLogger.logInfo(TAG, "only notify");
            //mNotificationListAdapter.notifyDataSetChanged();
            mNotificationListAdapter.updateItems(notification.getResultData());
            clearBadge();
        }
    }*/

    //4. Ongoing with TaskState.NOT_STARTED : Navigate to TechTaskTrackingFragment for LocationTechFragment/CountdownTechFragment
    //5. Ongoing with TaskState.STARTED_NOT_ENDED: Navigate to TechTaskTrackingFragment for EndTaskTechFragment
    //7. Ongoing with TaskState.ENDED_NO_PAYMENT and AMC : Navigate to TaskClosureFragment
    //8. Ongoing with TaskState.PAYMENT_RECEIVED and RATE : Navigate to TaskClosureFragment

    /*Do not delete this code may be useful in future if we add more condition to navigate for technician
    if (taskStateId == TaskState.NOT_STARTED) {//Note :We are not receiving notification for this condition
        navigateToTechTaskTrackingFragment(task, 1);
    } else if (taskStateId == TaskState.STARTED_NOT_ENDED) {//Note :We are not receiving notification for this condition
        navigateToTechTaskTrackingFragment(task, 3);
    } else if (taskStateId == TaskState.ENDED_NO_PAYMENT && taskMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING)) {
        navigateToTechPaymentReceivedFragment(task);
    } else if (taskStateId == TaskState.ENDED_NO_PAYMENT && taskMode.equalsIgnoreCase(Constant.PaymentMode.AMC_STRING)) {//Note :We are not receiving notification for this condition
        navigateToTaskClosureFragment(task);
    } else if (taskStateId == TaskState.PAYMENT_RECEIVED && taskMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING)) {//Note :We are not receiving notification for this condition
        navigateToTaskClosureFragment(task);
    }

       private void navigateToTaskClosureFragment(TasksList.ResultData data) {
        TaskClosureFragment taskClosureFragment = new TaskClosureFragment();
        taskClosureFragment.setArguments(getBundle(data));
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, taskClosureFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }*/

    //HomeTaskFragment.java
//    onDestroyView():
    /*mRootView = null;
        mTabLayoutTaskByTaskStatus = null;
        mViewPagerTaskByTaskType = null;
        mTasksLists.clear();
        mTasksLists = null;*/

    //LocationTechFragment.java
    //startNavigation():
    /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=" + mDestinationLat + "," + mDestinationLong));
            startActivity(intent);*/

    //OwnerTaskTrackingFragment.java
//    init():
    /* mLinearLayoutTechAbsent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mRiderImageView.setRotation(mRiderImageView.getRotation() + 15);
                return false;
            }
        });*/

       /* mLinearLayoutTechAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRiderImageView.setRotation(mRiderImageView.getRotation() + 45);
            }
        });*/

    //TaskClosureFragment.java
    //onSignature():
    /*int w = 972, h = 717;
        Bitmap.Config conf = Bitmap.Config.RGB_565; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(w, h, conf);
        Bitmap emptyBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
        if (bitmap.sameAs(emptyBitmap)) {
            // myBitmap is empty/blank
            FWLogger.logInfo(TAG, "Blank Bitmap ");
        }*/

    //AlertDialog.java
    //NOTE : Currently not in use but can use in future
               /* AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
                RectangularBounds bounds = RectangularBounds.newInstance(
                        new LatLng(-33.880490, 151.184363),
                        new LatLng(-33.858754, 151.229596));
                FindAutocompletePredictionsRequest requestPrediction =
                        FindAutocompletePredictionsRequest.builder()
                                .setLocationBias(bounds)
                                .setCountry("au")
                                .setTypeFilter(TypeFilter.REGIONS)
                                .setSessionToken(token)
                                .setQuery("Xanadutec")
                                .build();

                placesClient.findAutocompletePredictions(requestPrediction)
                        .addOnSuccessListener(
                                (response) -> {
                                    FWLogger.logInfo(TAG, "number of results in search places response"
                                            +response.getAutocompletePredictions().size());
                                    StringBuilder sb = new StringBuilder();
                                    for (AutocompletePrediction prediction :
                                            response.getAutocompletePredictions()) {
                                        FWLogger.logInfo(TAG, "\nplaces ID = "
                                                +prediction.getPlaceId());
                                        sb.append(prediction.getPrimaryText(null).toString());
                                        sb.append("\n");
                                    }
                                    FWLogger.logInfo(TAG,sb.toString());
                                    //.setText(sb.toString());
                                    //response.getAutocompletePredictions().get(0).getPlaceId();
                                })
                        .addOnFailureListener((exception) -> {
                            exception.printStackTrace();
                        });*/

// Create a new Places client instance.
//PlacesClient placesClient = Places.createClient(activity);

               /* // Construct a request object, passing the place ID and fields array.
                FetchPlaceRequest request = FetchPlaceRequest.builder("ChIJ93Ly2g3BwjsRvttJUx5YITs", placeFields)
                        .build();

                // Add a listener to handle the response.
                placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                    Place place = response.getPlace();
                    FWLogger.logInfo(TAG, "Place found: " + place.getName());
                }).addOnFailureListener((exception) -> {
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        int statusCode = apiException.getStatusCode();
                        // Handle error with given status code.
                        FWLogger.logInfo(TAG, "Place not found: " + exception.getMessage());
                    }
                });*/

            /*   else if (isEmpty(custumer_name)) {//NOTE removed from task save validation
                       custumer_name.setError("Please Enter Customer Name");
                       } else if (isEmpty(custumer_number)) {
                       custumer_number.setError("Please Enter Customer Contact Number");
                       } else if (isEmpty(task_item_quantity) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase("Select Item")) {
                       task_item_quantity.setError("Enter a Valid Quantity");
                       } else if (isEmpty(task_description)) {
                       task_description.setError("Please Enter special Instruction");
                       }*/

//currentDatePicker(datePicker); //NOTE removed from task save first line
//timePickerSpinner.setSelection(getPositionOfHoursFromTimeSlotList(getCurrentHours()));
//if (getCurrentMeridian() == 0) segmentedGroupTime.check(R.id.radiobuttonAM);
//else segmentedGroupTime.check(R.id.radiobuttonPM);

/* int currentTimePlusOne = getCurrentHours() + 1;
                    if (currentTimePlusOne > 12 && getCurrentMeridian() == 0) {
                        segmentedGroupTime.check(R.id.radiobuttonPM);//If afternoon 1200 then time plus one will be 13 means change AM to PM
                        currentTimePlusOne = 1; // Make 12+ as 1 PM
                        currentDatePicker(datePicker);
                    } else if (currentTimePlusOne > 12 && getCurrentMeridian() == 1) {
                        segmentedGroupTime.check(R.id.radiobuttonAM);//If midnight 1200 then time plus one will be 13 means change PM to AM
                        currentTimePlusOne = 1;// Make 12+ as 1 AM
                        nextDayDatePicker(datePicker);
                        //May need to add next date
                    } else {
                        if (getCurrentMeridian() == 0) segmentedGroupTime.check(R.id.radiobuttonAM);
                        else segmentedGroupTime.check(R.id.radiobuttonPM);
                        currentDatePicker(datePicker);
                    }
                    timePickerSpinner.setSelection(getPositionOfHoursFromTimeSlotList(currentTimePlusOne));*/

    //HomeActivity.java
//    onMenuItemClick(MenuItem item):
    /*case R.id.passbook:
                    isOwner = true;
                    clearBackStackEntries();
                    loadFragment(new HomePassbookFragment(), R.id.navigation_admin);
                    return true;*/
    //onNavigationItemSelected():
    /*case R.id.navigation_passbook:
                    mToolbar.setTitle("Passbook");
                    clearBackStackEntries();
                    loadFragment(new HomePassbookFragment(), R.id.navigation_passbook);
                    return true;*/
    /*@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.navigation_amc);
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            mToolbar.setTitle("Passbook");
            if (item.getTitle().equals(getString(R.string.title_amc))) {
                item.setTitle("Passbook");
            }
            clearBackStackEntries();
            loadFragment(new HomePassbookFragment(), R.id.navigation_amc);
        } else {
            mToolbar.setTitle("AMC");
            clearBackStackEntries();
            loadFragment(new AMCDashboardFragment(), R.id.navigation_amc);
        }
        return super.onPrepareOptionsMenu(menu);
    }*/
    /*public void saveAddTech(String name, Long number) {
        addTechAsyncTask = new AddTechAsyncTask(HomeActivity.this, BaseAsyncTask.Priority.LOW, this);
        addTechAsyncTask.execute(name, number, SharedPrefManager.getInstance(this).getUserId());
    }*/


    //LocationUtils.java
//    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates";
    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    /*static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }*/

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    /*static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }*/

    //MapUtils.java
    //for future use
    /*public List<LatLng> getPathBetweenTwoLocation(Context mContext, LatLng originLatLong, LatLng destinationLatLong) {
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList<>();

        //Execute Directions API request
        //Enable this
//        geoApiContext = new GeoApiContext.Builder()
//                //.apiKey( mContext.getString(R.string.google_maps_key))
//                .apiKey(mContext.getResources().getString(R.string.direction_api_key))
//                .build();
        DirectionsApiRequest req = DirectionsApi.getDirections(geoApiContext, "" + originLatLong.latitude + "," + originLatLong.longitude + "", "" + destinationLatLong.latitude + "," + destinationLatLong.longitude + "");
        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs != null) {
                    for (int i = 0; i < route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j = 0; j < leg.steps.length; j++) {
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length > 0) {
                                    for (int k = 0; k < step.steps.length; k++) {
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            FWLogger.logInfo(logging, ex.getLocalizedMessage());
        }

        return path;
    }*/

    //CameraUtils.java
    /*public static final int MEDIA_TYPE_IMAGE = 1;
    public static final String GALLERY_DIRECTORY_NAME = "FieldWeb";
    public static final String IMAGE_EXTENSION = "jpg";*/
    /*public static void refreshGallery(Context context, String filePath) {
        MediaScannerConnection.scanFile(context,
                new String[]{filePath}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
    }

    public static boolean checkPermissions(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }*/
    /*public static void openSettings(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", BuildConfig.APPLICATION_ID, null));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static Uri getOutputMediaFileUri(Context context, File file) {
        return FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
    }

    public static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                GALLERY_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                FWLogger.logInfo(GALLERY_DIRECTORY_NAME, "Oops! Failed create "
                        + GALLERY_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + "." + IMAGE_EXTENSION);
        } else {
            return null;
        }
        return mediaFile;
    }*/

    //DateUtils.java
    /*static public String getCurrentDate() {
        long milliSeconds = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date resultDate = new Date(milliSeconds);
        FWLogger.logInfo("CurrentDate : ","" + sdf.format(resultDate));
        return sdf.format(resultDate);
    }*/

    //FWFirebaseMessagingService.java
    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    /*private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, BaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setContentTitle("FW FCM Test Notification")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/

    //ImageUtils.java
    /*public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }*/

    //NotificationHelper.java
    /**
     * Showing notification with text and image
     */
    /*protected void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }*/
    /*private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Constant.FCM.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }*/

    //NotificationUtils.java
    /*public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }*/

    //SharedPrefManager.java
    /*public void clearAllPref() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }*/
    /*public void clearRegFCMID() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_FCM_REG_ID, null);
        editor.apply();
    }*/
    /*public long getUserLat() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getLong(KEY_USER_LAT, 0));
    }
    public long getUserLong() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getLong(KEY_USER_LONG, 0));
    }*/

    //Signature.java
    /*private void createDirectory(String uniqueId){
        tempDir = Environment.getExternalStorageDirectory() + "/" + mContext.getResources().getString(R.string.external_dir) + "/";
        ContextWrapper cw = new ContextWrapper(mContext.getApplicationContext());
        File directory = cw.getDir(mContext.getResources().getString(R.string.external_dir), Context.MODE_PRIVATE);
        if(FileUtils.prepareDirectory(tempDir));{
            try {
                current = FileUtils.getFileName(uniqueId)+ ".png";
                current = File.createTempFile(getFileN,  ".png", storageDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mFilePath= new File(directory,current);
        File storageDir = getContext().getExternalCacheDir();
        try {
            mFilePath = File.createTempFile(FileUtils.getFileName(uniqueId),  ".png", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


}
