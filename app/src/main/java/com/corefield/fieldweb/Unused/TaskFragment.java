package com.corefield.fieldweb.Unused;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.corefield.fieldweb.Adapter.TasksListAdapter;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.OwnerTaskTrackingFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TechPaymentReceivedFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.TaskState;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Fragment Controller for Tasks
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show list of task according to task type and status
 */
@SuppressLint("ValidFragment")
public class TaskFragment extends Fragment {

    private static final String TAG = TaskFragment.class.getSimpleName();
    private View mRootView;
    private Constant.TaskCategories mTaskCategory;
    private ArrayList<TasksList.ResultData> tasksLists;
    private TasksListAdapter tasksListAdapter;
    private RecyclerView mTaskListRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerTouchListener mRecyclerTouchListener;
    private int mPosition;
    private String mUserGroup = "";

    @SuppressLint("ValidFragment")
    public TaskFragment(Constant.TaskCategories taskCategory, ArrayList<TasksList.ResultData> tasksLists, String userGroup) {
        FWLogger.logInfo(TAG, "Constructor");
        this.mTaskCategory = taskCategory;
        this.tasksLists = tasksLists;
        this.mUserGroup = userGroup;
    }

    public TaskFragment() { }

    @Override
    public void onResume() {
        FWLogger.logInfo(TAG, "OnResume");
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        FWLogger.logInfo(TAG, "onAttach");
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        FWLogger.logInfo(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onPause() {
        FWLogger.logInfo(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        FWLogger.logInfo(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        FWLogger.logInfo(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onStop() {
        FWLogger.logInfo(TAG, "onStop");
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onCreateView");
        mRootView = inflater.inflate(R.layout.task_list_fragment, container, false);
        FWLogger.logInfo(TAG, "Loaded successfully");
        mTaskListRecyclerView = mRootView.findViewById(R.id.recycler_taskList);

        mRecyclerTouchListener = new RecyclerTouchListener() {
            @Override
            public void onClick(View view, int position) {
                if (view.getId() == R.id.textView_reassign_from_list) {
                    //((HomeActivity) getActivity()).addTaskWithReassignDefaults(tasksLists.get(position));
                    if(tasksLists.get(position).getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name())){
                        ((HomeActivityNew) getActivity()).openUpdateTaskDialog(tasksLists.get(position));
                    } else {
                        ((HomeActivityNew) getActivity()).addTaskWithReassignDefaults(tasksLists.get(position));
                    }
                } else if (mTaskCategory.equals(Constant.TaskCategories.Urgent) ||
                        mTaskCategory.equals(Constant.TaskCategories.Today) ||
                        mTaskCategory.equals(Constant.TaskCategories.Schedule) ||
                        mTaskCategory.equals(Constant.TaskCategories.Ongoing)) {
                    if (mUserGroup.equalsIgnoreCase("Owner")) {
                        OwnerTaskTrackingFragmentNew ownerTaskTrackingFragment = new OwnerTaskTrackingFragmentNew();
                        TasksList.ResultData data = tasksLists.get(position);
                        Bundle bundle = new Bundle();
                        if (data != null) {
                            bundle.putString("Task Status", data.getTaskStatus());
                            bundle.putInt("TaskStatusId", data.getTaskStatusId());
                            bundle.putString("Task Name", data.getName());
                            bundle.putString("Task Assignee", data.getAssignedTo());
                            bundle.putInt("Task Amount", data.getWagesPerHours());
                            bundle.putString("Image", "a");
                            bundle.putString("FieldLat", data.getLatitude());
                            bundle.putString("FieldLong", data.getLongitude());
                            bundle.putString("Photo", data.getPhoto());
                            //fake location
                            bundle.putString("TechLat", data.getTechLatitude());
                            bundle.putString("TechLong", data.getTechLongitude());
                            bundle.putString("ContactNo", data.getContactNo());
                            bundle.putString("CustomerName", data.getCustomerName());
                            bundle.putInt("TaskState", data.getTaskState());
                            bundle.putString("FullAddress", data.getFullAddress());
                            bundle.putInt("UserId", data.getUserId());
                        }
                        bundle.putBoolean("navigateToTaskTab",true);
                        ownerTaskTrackingFragment.setArguments(bundle);
                        FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, ownerTaskTrackingFragment);
                        //NOTE : Do not add to back stack as we are navigating from back press in OwnerTaskTrackingFragment
                        //transaction.isAddToBackStackAllowed();
                        //transaction.addToBackStack(null);
                        transaction.commit();
                    } else if (mUserGroup.equalsIgnoreCase("Technician")) {
                        TasksList.ResultData data = tasksLists.get(position);
                        Bundle bundle = new Bundle();
                        if (data != null) {
                            bundle.putString("Task Status", data.getTaskStatus());
                            bundle.putString("Task Name", data.getName());
                            bundle.putString("Task Assignee", data.getAssignedTo());
                            bundle.putInt("Task Amount", data.getWagesPerHours());
                            bundle.putString("Image", "a");
                            bundle.putString("FieldLat", data.getLatitude());
                            bundle.putString("FieldLong", data.getLongitude());
                            bundle.putString("TaskDescription", data.getDescription()); // task desc
                            bundle.putString("AddressDesc", data.getLocationDesc()); // Address desc
                            bundle.putString("ShortAddress", data.getLocationName()); //TODO error
                            bundle.putString("ContactNo", data.getContactNo());
                            bundle.putString("CustomerName", data.getCustomerName());
                            bundle.putInt("TaskId", data.getId());
                            bundle.putInt("TaskStatusId", data.getTaskStatusId());
                            bundle.putLong("StartDate", data.getStartDate());
                            bundle.putLong("EndDate", data.getEndDate());
                            bundle.putInt("TaskState", data.getTaskState());
                            bundle.putString("FullAddress", data.getFullAddress());
                            bundle.putString("PaymentMode", data.getPaymentMode());
                            bundle.putInt("PaymentModeId", data.getPaymentModeId());
                            bundle.putString("TaskDate", data.getTaskDate());
                            FWLogger.logInfo("PaymentMode : ", " " + data.getPaymentMode());
                            FWLogger.logInfo("PaymentModeId : ", " " + data.getPaymentModeId());
                        }
                        //Check for PNR(Payment Not Received)
                        FWLogger.logInfo(TAG, "getPaymentNotReceived  = " + data.getPaymentNotReceived());

                        //NOTE :
                        // If task is  the open TechTaskTrackingFragment
                        // TASKSTATE 0 : Task not stated then open TechTaskTrackingFragment
                        // TASKSTATE 1 : Task started but not ended then open TechTaskTrackingFragment
                        // TASKSTATE 2 : Task Ended but payment not rec then open TechPaymentReceviedFragment directly
                        // TASKSTATE 3 : Show the pop  that task has been already completed

                        /*if (data.getTaskState() == TaskState.NOT_STARTED || data.getTaskState() == TaskState.STARTED_NOT_ENDED) {
                            TechTaskTrackingFragment techTaskTrackingFragment = new TechTaskTrackingFragment();
                            bundle.putBoolean("navigateToTaskTab",true);
                            techTaskTrackingFragment.setArguments(bundle);
                            FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                            transaction.replace(R.id.home_task_frag_layout, techTaskTrackingFragment);
                            //NOTE : Do not add to back satck as we are navigating from back press in TechTaskTrackingFragment and TechPaymentReceivedFragment
                            //transaction.isAddToBackStackAllowed();
                            //transaction.addToBackStack(null);
                            transaction.commit();
                        }*/

                        if (data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING) && data.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
                            TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                            taskClosureFragment.setArguments(bundle);
                            FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                            transaction.replace(R.id.home_task_frag_layout, taskClosureFragment);
                            transaction.commit();
                        }
                        if (data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && data.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                            TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
                            bundle.putBoolean("navigateToTaskTab",true);
                            techPaymentReceivedFragment.setArguments(bundle);
                            FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                            //FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            //NOTE : Do not add to back satck as we are navigating from back press in TechTaskTrackingFragment and TechPaymentReceivedFragment
                            transaction.replace(R.id.home_task_frag_layout, techPaymentReceivedFragment);
                            transaction.commit();
                        }
                        if (data.getTaskState() == TaskState.PAYMENT_RECEIVED && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && data.getPaymentModeId() == Constant.PaymentMode.RATE_ID && !data.isTaskClosureStatus()) {
                            TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                            taskClosureFragment.setArguments(bundle);
                            FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                            transaction.replace(R.id.home_task_frag_layout, taskClosureFragment);
                            transaction.commit();
                        } else if (data.getTaskState() == TaskState.PAYMENT_RECEIVED && data.isTaskClosureStatus()) {
                            Toast.makeText(getContext(), "This Task is Competed and Payment is Received", Toast.LENGTH_SHORT).show();
                        } else {
                        }
                    }
                } else if (mTaskCategory.equals(Constant.TaskCategories.Completed) && mUserGroup.equalsIgnoreCase("Owner")){
                    /*TaskDetailsFragment taskDetailsFragment = new TaskDetailsFragment(tasksLists.get(position));
                    FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_task_frag_layout, taskDetailsFragment);
                    transaction.commit();*/
                } else if (mUserGroup.equalsIgnoreCase("Owner") && mTaskCategory.equals(Constant.TaskCategories.Rejected)) {
                   /* TaskDetailsFragment taskDetailsFragment = new TaskDetailsFragment(tasksLists.get(position));
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("allowToReassign", true);
                    taskDetailsFragment.setArguments(bundle);
                    FragmentTransaction transaction = getParentFragment().getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_task_frag_layout, taskDetailsFragment);
                    transaction.commit();*/
                }
            }

        };

        tasksListAdapter = new TasksListAdapter(getContext(), tasksLists); //, mTaskCategory
        tasksListAdapter.setClickListener(mRecyclerTouchListener);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mTaskListRecyclerView.setLayoutManager(mLayoutManager);
        mTaskListRecyclerView.setAdapter(tasksListAdapter);
        return mRootView;
    }

}