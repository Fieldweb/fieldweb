package com.corefield.fieldweb.Unused;

import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;

import java.io.File;
import java.io.IOException;

/**
 * //
 * Created by CFS on 5/29/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class FileUtils {

    private static final String TAG = FileUtils.class.getSimpleName();

    public static boolean prepareDirectory(String tempDir) {
        try {
            return makedirs(tempDir);
        } catch (Exception e) {
            e.printStackTrace();
            FWLogger.logInfo(TAG, " Could not initiate File System.. Is Sdcard mounted properly?");
            return false;
        }
    }

    public static boolean makedirs(String tempDir) {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory()) {
            File[] files = tempdir.listFiles();
            for (File file : files) {
                if (!file.delete()) {
                    FWLogger.logInfo("Failed to delete ", "" + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }


    public static String getFileName(String uniqueId) throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        String imageFileName = uniqueId + "_" + timeStamp + "_";
        return imageFileName;
    }
}
