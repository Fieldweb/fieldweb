package com.corefield.fieldweb.Unused;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.UpdatePassbook;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Async Task for Update Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Async Task class is used to call -  Update Passbook API
 */
public class UpdatePassbookAsyncTask extends BaseAsyncTask {

    protected static String TAG = UpdatePassbookAsyncTask.class.getSimpleName();

    public UpdatePassbookAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                Gson gson = new Gson();

                //String data = gson.toJson((UpdateTask.ResultData) objects[0]);
                String userId = "";
                int taskId = 0, amount = 0;


                //Assign value
                userId = (String) objects[0];
                taskId = (int) objects[1];
                amount = (int) objects[2];

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("UserId", userId);
                    jsonObject.put("TaskId", taskId);
                    jsonObject.put("EarningAmount", amount);
                } catch (JSONException e) {
                    jsonObject = null;
                    e.printStackTrace();
                }
                FWLogger.logInfo(TAG, "jsonObject = " + jsonObject.toString());


                if (jsonObject != null) {
                    URLConnectionRequest URLConnectionRequest = new URLConnectionRequest();
                    if (jsonObject != null && URLConnectionRequest != null) {
                        //No need to pass data to request builder as Gson gives data in string JSON format
                        Request request = RequestBuilder.buildRequest(mContext, URLConstant.Passbook.UPDATE_PASSBOOK, jsonObject);
                        mUrlConnectionResponse = URLConnectionRequest.sendPostRequest(request.getmURL(), request.getmHeader(), request.getmData().toString(), request.getToken());
                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }

            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null && mUrlConnectionResponse != null) {
            if (mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (status) {
                    if (onTaskCompleteListener != null)
                        onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, UpdatePassbook.class.getSimpleName());
                } else {
                    //Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
