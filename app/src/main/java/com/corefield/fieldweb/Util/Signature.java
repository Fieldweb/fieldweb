package com.corefield.fieldweb.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.corefield.fieldweb.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

/**
 * //
 * Created by CFS on 5/29/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class Signature extends View {

    private final String TAG = Signature.class.getSimpleName();
    private static final float STROKE_WIDTH = 5f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
    private Paint mPaint = new Paint();
    private Path mPath = new Path();
    private static final double MINIMUM_SIGNATURE_LENGTH = 2;
    private static final float TOUCH_TOLERANCE = 4;
    private double mTotal;

    private float lastTouchX;
    private float lastTouchY;
    private final RectF dirtyRect = new RectF();

    private Context mContext;
    private Bitmap mBitmap;
    private LinearLayout mContent;
    private File mFilePath;

    //String tempDir;
    //String current = null;
    private OnSignatureSave mSignatureSave;

    public Signature(Context context, AttributeSet attrs,LinearLayout contentLayout,String uniqueId,OnSignatureSave saveCallback) {
        super(context, attrs);
        mContext = context;
        //createDirectory(uniqueId);
        mContent = contentLayout;
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(STROKE_WIDTH);
        mSignatureSave = saveCallback;
    }

    public void save(View v) {
        FWLogger.logInfo(TAG, " Width: " + v.getWidth());
        FWLogger.logInfo(TAG, " Height: " + v.getHeight());
        if (mBitmap == null) {
            mBitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
        }
        if(mTotal >= MINIMUM_SIGNATURE_LENGTH) {
            Canvas canvas = new Canvas(mBitmap);
            try {
                v.draw(canvas);
                /*FileOutputStream mFileOutStream = new FileOutputStream(mFilePath);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
                String url = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), mBitmap, "title" + Calendar.getInstance().getTime(), null);
                FWLogger.logInfo(TAG, "url: " + url);*/
                if (mSignatureSave != null) mSignatureSave.onSignature(mBitmap);
                //In case you want to delete the file
                //boolean deleted = mFilePath.delete();
                //Log.v("log_tag","deleted: " + mFilePath.toString() + deleted);
                //If you want to convert the image to string use base64 converter

            } catch (Exception e) {
                FWLogger.logInfo(TAG, e.toString());
            }
        } else {
            mSignatureSave.onSignature(null);
            Toast.makeText(mContext, "Signature is blank", Toast.LENGTH_SHORT).show();
        }
    }

    public void clear() {
        mPath.reset();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();
        //mGetSign.setEnabled(true);
        ViewUtils.hideKeyboard((Activity) mContext);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mPath.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:
                touchMove(eventX, eventY);

            case MotionEvent.ACTION_UP:

                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    mPath.lineTo(historicalX, historicalY);
                }
                mPath.lineTo(eventX, eventY);
                break;

            default:
                debug("Ignored touch event: " + event.toString());
                return false;
        }

        invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;

        return true;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - lastTouchX);
        float dy = Math.abs(y - lastTouchY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(lastTouchX, lastTouchY, (x + lastTouchX) / 2, (y + lastTouchY) / 2);
            lastTouchX = x;
            lastTouchY = y;
        }

        mTotal += Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    private void debug(String string) {
    }

    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX;
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX;
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY;
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY;
        }
    }

    private void resetDirtyRect(float eventX, float eventY) {
        dirtyRect.left = Math.min(lastTouchX, eventX);
        dirtyRect.right = Math.max(lastTouchX, eventX);
        dirtyRect.top = Math.min(lastTouchY, eventY);
        dirtyRect.bottom = Math.max(lastTouchY, eventY);
    }


    public interface OnSignatureSave{
        void onSignature(Bitmap bitmap);
    }

}
