package com.corefield.fieldweb.Util.GoogleMapUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Toast;

import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Util Class for MAP Functions
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Utility class is used for drawing path between two marker on google map
 */
public class MapUtils {
    private static String TAG = MapUtils.class.getSimpleName();
    private static Marker mMarkerTech = null;
    private static List<LatLng> path = null;
    private static GeoApiContext geoApiContext = null;
    private final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
    private static int emission = 0;
    private static Polyline mPolyline = null;
    private static LatLng mPrevCoordinates = null;
    private static boolean isReached = false;
    public static String totalDistance = "";
    public static String totalDuration = "";

    public static List<LatLng> getPathBetweenTwoLocation(Context mContext, LatLng originLatLong, LatLng destinationLatLong) {
        new Thread(new Runnable() {
            public void run() {
                FWLogger.logInfo(TAG, "originLatLong : " + originLatLong.latitude + " , " + originLatLong.longitude + "  destinationLatLong : " + destinationLatLong.latitude + " , " + destinationLatLong.longitude);

                //Define list to get all latlng for the route
                List<LatLng> path = new ArrayList<>();
                // BY MANISH : START
                //path.add(new LatLng(originLatLong.latitude, originLatLong.longitude));
                //path.add(new LatLng(destinationLatLong.latitude, destinationLatLong.longitude))
                // BY MANISH : END

                //Execute Directions API request
                geoApiContext = new GeoApiContext.Builder()
                        //.apiKey(mContext.getString(R.string.map_api_key));
                        .apiKey(mContext.getResources().getString(R.string.direction_api_key)).build();
                DirectionsApiRequest req = DirectionsApi.getDirections(geoApiContext, "" + originLatLong.latitude + "," + originLatLong.longitude + "", "" + destinationLatLong.latitude + "," + destinationLatLong.longitude + "");
                try {
                    DirectionsResult res = req.await();

          /*  JSONObject jsonObject = new JSONObject(response);
            JSONArray array = jsonObject.getJSONArray("routes");
            JSONObject routes = array.getJSONObject(0);
            JSONArray legs = routes.getJSONArray("legs");
            JSONObject steps = legs.getJSONObject(0);
            JSONObject distance = steps.getJSONObject("distance");
            String parsedDistance=distance.getString("text");*/

                    //Loop through legs and steps to get encoded polylines of each step
                    if (res.routes != null && res.routes.length > 0) {
                        DirectionsRoute route = res.routes[0];
                        if (route.legs != null) {
                            totalDistance = String.valueOf(route.legs[0].distance);
                            totalDuration = String.valueOf(route.legs[0].duration);
                            FWLogger.logInfo(TAG, "Distance : " + totalDistance.split(" ")[0] + "  Duration : " + totalDuration);
                            for (int i = 0; i < route.legs.length; i++) {
                                DirectionsLeg leg = route.legs[i];
                                if (leg.steps != null) {

                                    for (int j = 0; j < leg.steps.length; j++) {
                                        DirectionsStep step = leg.steps[j];
                                        if (step.steps != null && step.steps.length > 0) {
                                            for (int k = 0; k < step.steps.length; k++) {
                                                DirectionsStep step1 = step.steps[k];
                                                EncodedPolyline points1 = step1.polyline;
                                                if (points1 != null) {
                                                    //Decode polyline and add points to list of route coordinates
                                                    List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                                    for (com.google.maps.model.LatLng coord1 : coords1) {
                                                        path.add(new LatLng(coord1.lat, coord1.lng));
                                                    }
                                                }
                                            }
                                        } else {
                                            EncodedPolyline points = step.polyline;
                                            if (points != null) {
                                                //Decode polyline and add points to list of route coordinates
                                                List<com.google.maps.model.LatLng> coords = points.decodePath();
                                                for (com.google.maps.model.LatLng coord : coords) {
                                                    path.add(new LatLng(coord.lat, coord.lng));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    FWLogger.logInfo(TAG, "Exception : " + ex.getMessage() + "  " + ex.getLocalizedMessage());
                }
            }
        }).start();
        return path;
    }

    public static String getFullAddressFromLatLong(Context context, double latitude, double longitude) {
        Geocoder geocoder;
        String address = "";
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            if (Geocoder.isPresent()) {
                FWLogger.logInfo(TAG, "from geocoder");
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses.size() != 0) {
                    address = addresses.get(0).getAddressLine(0);// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }
            } else {
                Toast.makeText(context, "No geocoder available", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }

    public static String getPostalCodeFromLatLong(Context context, double latitude, double longitude) {
        Geocoder geocoder;
        String postalCode = "";
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            if (Geocoder.isPresent()) {
                FWLogger.logInfo(TAG, "from geocoder");
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses.size() != 0) {
                    postalCode = addresses.get(0).getPostalCode();// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postalCode;
    }

    public static synchronized void updateMarkerOnMap(boolean isFirstTime, Context context, GoogleMap mGoogleMap, LatLng mCoordinateOrigin, LatLng mCoordinateDestination) {
        FWLogger.logInfo(TAG, "updateGoogleMapUI - mGoogleMap");
        if (mGoogleMap != null) {
            if (isFirstTime) {
                if (mCoordinateOrigin != null) {
                    try {
                        mMarkerTech = mGoogleMap.addMarker(new MarkerOptions().anchor(0.5f, 0.5f).position(mCoordinateOrigin)
//                            .icon(bitmapDescriptorFromVector(context, R.drawable.ic_fieldweb_rider)));
                                .icon(bitmapDescriptorFromVector(context, R.drawable.ic_fieldweb_tech)));
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                }
            } else {
                if (mMarkerTech != null && mCoordinateOrigin != null) {
                    mMarkerTech.setPosition(mCoordinateOrigin);
                    mMarkerTech.setAnchor(0.5f, 0.5f);
//                    mMarkerTech.setRotation(getBearing(mCoordinateOrigin, mCoordinateDestination));
                }
            }
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(mCoordinateOrigin);
            builder.include(mCoordinateDestination);
            // Create bounds from positions
            LatLngBounds bounds = builder.build();

            // Setup camera movement
            /*final int width = context.getResources().getDisplayMetrics().widthPixels;
            final int height = context.getResources().getDisplayMetrics().heightPixels;
            final int minMetric = Math.min(width, height);
            final int padding = (int) (minMetric * 0.40); // offset from edges of the map in pixels*/

            //Animate camera to all location
            int padding = 200; // offset from edges of the map in pixels
            mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    try {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//                    CameraUpdate cameraUpdate =  CameraUpdateFactory.newLatLngZoom(mCoordinateDestination, 12);
                        // Move the camera instantly to location with a zoom of 15.
//                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCoordinateDestination, 10));
                        // Zoom in, animating the camera.
//                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
                        // ADDED BY MANISH FOR : TECH LOC
                        //mGoogleMap.setMinZoomPreference(10.0f); // Set a preference for minimum zoom (Zoom out).
                        mGoogleMap.animateCamera(cameraUpdate);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                }
            });
        }
    }

    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static void clearMapAttr() {
        mMarkerTech = null;
        path = null;
    }

    public static int calculateDistanceInKilometer(double userLat, double userLng, double venueLat, double venueLng) {
        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat)) * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c));
    }
}
