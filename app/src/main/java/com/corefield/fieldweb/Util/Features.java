package com.corefield.fieldweb.Util;

public class Features {

    public interface Common {
        boolean LOGIN = false;
        boolean SIGN_UP = false;
        boolean DISCLAIMER = false;
        boolean HEALTH_AND_SAFETY = false;
        boolean ATTENDANCE = false;
        boolean LOCATION = false;
        boolean DASHBOARD = false;
        boolean NOTIFICATION = false;
        boolean PROFILE = false;
        boolean EDIT_PROFILE = false;
        boolean CHANGE_PASSWORD = false;
        boolean FORGOT_PASSWORD = false;
        boolean CONTACT_SUPPORT = false;
        boolean CHANGE_LANGUAGE = false;
        boolean CHECK_IN = false;
        boolean CHECK_OUT = false;
        boolean TASK_LIST = false;
        boolean ADD_TASK = false;
        boolean EDIT_TASK = false;
        boolean ENQUIRY_ADD_TASK = false;
        boolean AMC_ADD_TASK = false;
        boolean ITEM_ISSUED = false;
        boolean ASSIGN_ITEM = false;
        boolean ITEM_INVENTORY = false;
        boolean ADD_ITEM = false;
        boolean ISSUE_ITEM = false;
        boolean EXPENDITURE = false;
        boolean PASSBOOK = false;
        boolean ENQUIRY_FORM = false;
        boolean ADD_TECHNICIANS = false;
        boolean GENERATE_INVOICE = false;
        boolean ENQUIRY_LIST = false;
        boolean CUSTOMER_LIST = false;
        boolean CALL_CUSTOMER = false;
        boolean SMS_CUSTOMER = false;
        boolean EMPLOYEE_LIST = false;
        boolean DOWNLOAD_EMPLOYEE_LIST = false;
        boolean ADD_BALANCE = false;
        boolean DEDUCT_BALANCE = false;
        boolean AMC_LIST = false;
        boolean ADD_AMC = false;
        boolean EXPENSE_LIST = false;
    }
}
