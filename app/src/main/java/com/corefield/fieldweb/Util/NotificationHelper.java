package com.corefield.fieldweb.Util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.corefield.fieldweb.FieldWeb.BaseActivity;

/**
 * //
 * Created by CFS on 2/15/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class NotificationHelper {

    protected void showNotification(Context context, String title,String messageBody,String timestamp,String dataMessage){
        //App is in foreground then show notification in tray
        if (!NotificationUtils.isAppIsInBackground(context)) {
            Intent resultIntent = new Intent(context, BaseActivity.class);
            if(dataMessage!= null){
                Bundle bundle = new Bundle();
                bundle.putString("dataMessage",dataMessage);
                resultIntent.putExtras(bundle);
            }
            resultIntent.putExtra("message", messageBody);
            showNotificationMessage(context, title, messageBody, timestamp, resultIntent);
        }
    }

    /**
     * Showing notification with text only
     */
    protected void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }
}
