package com.corefield.fieldweb.Util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.corefield.fieldweb.R;

import java.io.File;
import java.io.IOException;

public class DownloadFile {

    public static void downloadFile(String path, Activity activity) {
        try {
            final ProgressDialog pd = new ProgressDialog(activity);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(activity.getString(R.string.please_wait));
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean downloadResponse = HttpDownloadUtility.downloadFile(path, storageDir.getAbsolutePath());
                        if (downloadResponse) {
                            if (activity != null) {
                                activity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(activity, R.string.file_download_successfully, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            String filename = path.substring(path.lastIndexOf("/") + 1);
                            File file = new File(storageDir, filename);
                            Uri uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", file);
                            // Open file with user selected app
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);

                            if (file.toString().contains(".doc") || file.toString().contains(".docx")) {
                                // Word document
                                intent.setDataAndType(uri, "application/msword");
                            } else if (file.toString().contains(".pdf")) {
                                // PDF file
                                intent.setDataAndType(uri, "application/pdf");
                            } else if (file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                                // Powerpoint file
                                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            } else if (file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                                // Excel file
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                            } else if (file.toString().contains(".zip") || file.toString().contains(".rar")) {
                                // WAV audio file
                                intent.setDataAndType(uri, "application/x-wav");
                            } else if (file.toString().contains(".rtf")) {
                                // RTF file
                                intent.setDataAndType(uri, "application/rtf");
                            } else if (file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                                // WAV audio file
                                intent.setDataAndType(uri, "audio/x-wav");
                            } else if (file.toString().contains(".gif")) {
                                // GIF file
                                intent.setDataAndType(uri, "image/gif");
                            } else if (file.toString().contains(".jpg") || file.toString().contains(".jpeg") || file.toString().contains(".png")) {
                                // JPG file
                                intent.setDataAndType(uri, "image/jpeg");
                            } else if (file.toString().contains(".txt")) {
                                // Text file
                                intent.setDataAndType(uri, "text/plain");
                            } else if (file.toString().contains(".3gp") || file.toString().contains(".mpg") || file.toString().contains(".mpeg") || file.toString().contains(".mpe") || file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                                // Video files
                                intent.setDataAndType(uri, "video/*");
                            } else {
                                //if you want you can also define the intent type for any other file
                                //additionally use else clause below, to manage other unknown extensions
                                //in this case, Android will show all applications installed on the device
                                //so you can choose which application to use
                                intent.setDataAndType(uri, "*/*");
                            }

//                            intent.setDataAndType(uri, "application/pdf");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            activity.startActivity(intent);
                        } else {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(activity, R.string.sownload_failed, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        pd.dismiss();
                    } catch (IOException ex) {
                        pd.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
