package com.corefield.fieldweb.Util;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.corefield.fieldweb.DTO.TeleCMI.Connect;
import com.corefield.fieldweb.DTO.TeleCMI.Pcmo;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.LocationService.GoogleLocAlarmReceiver;
import com.corefield.fieldweb.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonFunction {

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    public static ProgressDialog progressDialog;

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    public static boolean isValidPhone(EditText text) {
        CharSequence phone = text.getText().toString();
        return phone.length() != 10;
    }

    public static boolean isValidPhoneNRI(EditText text) {
        CharSequence phone = text.getText().toString();
        return phone.length() <= 6;
    }

    @SuppressLint("HardwareIds")
    public static String getUniqueDeviceID(Context mContext) {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public static void showProgressDialog(final Activity activity, final String title, final String message) {*/
    public static void showProgressDialog(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              /*  progressDialog = ProgressDialog.show(activity, title, message);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);*/
                progressDialog = ProgressDialog.show(activity, "", "");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                //progressDialog.show();//displays the progress bar
                progressDialog.setContentView(R.layout.progress_dialog);
            }
        });
    }

    public static void hideProgressDialog(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        });
    }

/*
    public static void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918037222880"));
            teleCMICall.setTo(Long.valueOf(techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("8037222880"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf(custContactnum));
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
*/

    public static void TelCMIForLead(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918035234087"));
            teleCMICall.setTo(Long.valueOf("91" + techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("918035234087"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf("91" + custContactnum));
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public static void TelCMIForGeneral(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918035234087"));
            teleCMICall.setTo(Long.valueOf(Long.valueOf("91" + techMobile)));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");

            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("918035234087"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf("91" + custContactnum));// CUST NUM
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }



    public static Bitmap ImgCompressUpto200KB(String imagePath, Bitmap compressedImg) {
        try {

            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            float imgRatio = (float) actualWidth / (float) actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(imagePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            if (bmp != null) {
                bmp.recycle();
            }

            ExifInterface exif;
            try {
                exif = new ExifInterface(imagePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            String filepath = imagePath;/*getFilename();*/
            try {
                out = new FileOutputStream(filepath);

                //write the compressed bitmap at the destination specified by filename.
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            ex.getMessage();
        }

        return compressedImg;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


}
