package com.corefield.fieldweb.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.corefield.fieldweb.DTO.FWUser;
import com.corefield.fieldweb.DTO.RegisterHere.RegisterOwner;
import com.corefield.fieldweb.FieldWeb.LoginTouchlessActivity;
import com.corefield.fieldweb.FieldWeb.RuntimePermissionActivity;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;

/**
 * SharedPreference for storing tokens and user details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This SharedPreference class is used for storing tokens and user details
 */
public class SharedPrefManager {

    private static final String SHARED_PREF_NAME = "FiledWeb";
    private static final String KEY_USERNAME_GROUP = "user group";
    private static final String KEY_TOKEN = "Token";
    private static final String KEY_USERID = "UserId";
    private static final String KEY_DISCLAIMER = "Disclaimer";
    private static final String KEY_FCM_REG_ID = "regId";
    private static final String KEY_USER_LAT = "UserLat";
    private static final String KEY_USER_LONG = "UserLong";
    private static final String KEY_HEALTH_SAFETY_ENABLED = "HealthSafetyEnabled";
    private static final String KEY_HEALTH_SAFETY_DATE = "HealthSafetyDate";
    private static final String KEY_HEALTH_SAFETY_LOGGED = "HealthSafetyLogged";
    private static final String KEY_ANDROID_ID = "AndroidInstallationID";
    private static final String KEY_LANGUAGE = "Language";
    private static final String KEY_LANGUAGE_POS = "LanguagePos";
    private static final String KEY_FIRST_TIME = "FirstTime";
    private static final String KEY_FIRST_TIME_ADD_TECH_POUP = "AddTechFirstTime";
    private static final String KEY_FIRST_TIME_CHANGE_LANG = "ChangeLangFlag";
    private static final String KEY_PERMISSION_ACCEPT = "PermissionAccepted";
    private static final String KEY_APP_TOUR_COMPL = "AppTourComplted";
    private static final String KEY_USER_OTP = "Otp";
    private static final String KEY_COUNTRY_DETAILS_ID = "CountryDetailsId";
    private static final String IS_SLIDER_FIRST_TIME_LAUNCH = "IsSliderFirstTimeLaunch";
    //private static final String KEY_IS_MOBILE_OR_EMAIL = "isMobileEmail";
    private static final String KEY_OTP_VERIFIED = "otpVerified";
    private static final String KEY_AUTOFILL_SMS = "autoFillSMS";
    private static final String KEY_IS_MOBILE_OR_EMAIL = "isMobileEmail";
    private static final String KEY_FIRST_NAME = "firstName";
    private static final String KEY_IS_TELECMI_ENABLED = "IsTeleCmiEnabled";

    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setAppTourCompleted(boolean isAppGuideCompleted) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_APP_TOUR_COMPL, isAppGuideCompleted);
        editor.commit();
    }

    public boolean isAppTourCompleted() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_APP_TOUR_COMPL, false));
    }

    public void setPermissionEnabled(boolean isPermissionAccepted) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_PERMISSION_ACCEPT, isPermissionAccepted);
        editor.commit();
    }

    public boolean isPermissionAccepted() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_PERMISSION_ACCEPT, false));
    }

    public boolean isFirstTime() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_FIRST_TIME, true));
    }

    public boolean isFirstTimeAddMoreTechPopup() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_FIRST_TIME_ADD_TECH_POUP, true));
    }

    public boolean isFirstTimeChangeLang() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_FIRST_TIME_CHANGE_LANG, true));
    }

    public void setOTPVerified(boolean OTPverified) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_OTP_VERIFIED, OTPverified);
        editor.apply();
    }

    public boolean isOTPVerified() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_OTP_VERIFIED, false));
    }

    public void setFirstTime(boolean firstTime) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_TIME, firstTime);
        editor.apply();
    }

    public void setisFirstTimeAddMoreTechPopup(boolean firstTime) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_TIME_ADD_TECH_POUP, firstTime);
        editor.apply();
    }

    public void setisFirstTimeChangeLang(boolean firstTime) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_TIME_CHANGE_LANG, firstTime);
        editor.apply();
    }

    public void setSMSAutoFillFlag(boolean autoFillFlag) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_AUTOFILL_SMS, autoFillFlag);
        editor.apply();
    }

    public boolean isOTPAutoFillGranted() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(KEY_AUTOFILL_SMS, false));
    }

    public void userLogin(FWUser user, String androidId) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USERID, user.getResultdata().getUserid());
        editor.putString(KEY_TOKEN, user.getResultdata().getToken());
        editor.putString(KEY_USERNAME_GROUP, user.getResultdata().getUsergroupname());
        editor.putString(KEY_USER_OTP, user.getResultdata().getOTP());
        editor.putInt(KEY_COUNTRY_DETAILS_ID, user.getResultdata().getCountryDetailsId());
        if (androidId != null) editor.putString(KEY_ANDROID_ID, androidId);
        editor.apply();
    }

    public void ownerSignUp(RegisterOwner registerOwner, String androidId) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USERID, registerOwner.getResultData().getLoginOutPut().getUserID());
        editor.putString(KEY_TOKEN, registerOwner.getResultData().getLoginOutPut().getToken());
        editor.putString(KEY_USERNAME_GROUP, registerOwner.getResultData().getLoginOutPut().getUserGroupName());
        editor.putInt(KEY_COUNTRY_DETAILS_ID, registerOwner.getResultData().getCountryDetailsId());
        if (androidId != null) editor.putString(KEY_ANDROID_ID, androidId);
        editor.apply();
    }

   /* public void setIsIndian_NonIndian(String isMobile_Email) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_IS_MOBILE_OR_EMAIL, isMobile_Email);
        editor.apply();
    }*/

   /* public String getIsIndian_NonIndian() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_IS_MOBILE_OR_EMAIL, null));
    }*/


    public void setDisclaimer(String agree) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_DISCLAIMER, agree);
        editor.apply();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_USERID, 0) != 0;
    }

    public int getCountryDetailsID() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getInt(KEY_COUNTRY_DETAILS_ID, 0));
    }


    public int getUserId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getInt(KEY_USERID, 0));
    }

    public String getUserOTP() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_USER_OTP, null));
    }

    public boolean getDisclaimer() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DISCLAIMER, null) != null;
    }

    public String getUserGroup() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_USERNAME_GROUP, null)
        );
    }

    public static String getUserToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_TOKEN, null)
        );
    }


    public void logout(boolean openLoginActivity) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USERID, 0);
        editor.putString(KEY_TOKEN, null);
        editor.putString(KEY_USERNAME_GROUP, null);
        editor.putString(KEY_DISCLAIMER, null);
        editor.putBoolean(KEY_HEALTH_SAFETY_ENABLED, false);
        editor.putBoolean(KEY_HEALTH_SAFETY_LOGGED, false);
        editor.putString(KEY_HEALTH_SAFETY_DATE, null);
        editor.putBoolean(KEY_APP_TOUR_COMPL, false);
        if (getAndroidID() != null) clearAndroidID(editor);
        editor.apply();
        if (openLoginActivity) mCtx.startActivity(new Intent(mCtx, LoginTouchlessActivity.class));
    }

    public void logoutTechnician(boolean openLoginActivity) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USERID, 0);
        editor.putString(KEY_TOKEN, null);
        editor.putString(KEY_USERNAME_GROUP, null);
        editor.putString(KEY_DISCLAIMER, null);
        editor.putBoolean(KEY_HEALTH_SAFETY_ENABLED, false);
        editor.putBoolean(KEY_HEALTH_SAFETY_LOGGED, false);
        editor.putString(KEY_HEALTH_SAFETY_DATE, null);
        if (getAndroidID() != null) clearAndroidID(editor);
        editor.apply();
        if (openLoginActivity) mCtx.startActivity(new Intent(mCtx, LoginTouchlessActivity.class));
    }


    public void regFCMID(String regId) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_FCM_REG_ID, regId);
        editor.apply();
    }

    public String getRegFCMID() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_FCM_REG_ID, null)
        );
    }

    public void addUserLocation(double latitude, double longitude) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(KEY_USER_LAT, Double.doubleToLongBits(latitude));
        editor.putLong(KEY_USER_LONG, Double.doubleToLongBits(longitude));
        editor.apply();
    }

    public Location getUserLocation() {
        Location location = new Location("");
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        location.setLatitude(Double.longBitsToDouble(sharedPreferences.getLong(KEY_USER_LAT, 0)));
        location.setLongitude(Double.longBitsToDouble(sharedPreferences.getLong(KEY_USER_LONG, 0)));
        return location;
    }

    public void setHealthSafetyEnabled(boolean isEnabled) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_HEALTH_SAFETY_ENABLED, isEnabled);
        editor.apply();
    }

    public boolean isHealthSafetyEnabled() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_HEALTH_SAFETY_ENABLED, false);
    }

    public void setHealthSafetyDate(String date) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_HEALTH_SAFETY_DATE, date);
        editor.apply();
    }

    public String getHealthSafetyDate() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_HEALTH_SAFETY_DATE, null);
    }

    public void setHealthSafetyLogged(boolean isLogged) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_HEALTH_SAFETY_LOGGED, isLogged);
        editor.apply();
    }

    public boolean isHealthSafetyLogged() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_HEALTH_SAFETY_LOGGED, false);
    }

    public void putAndroidID(String androidId) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (androidId != null) editor.putString(KEY_ANDROID_ID, androidId);
        editor.apply();
    }

    public String getAndroidID() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_ANDROID_ID, null));
    }

    public void clearAndroidID(SharedPreferences.Editor editor) {
        editor.putString(KEY_ANDROID_ID, null);
    }

    public void setPreferredLanguage(String language, int position) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LANGUAGE, language);
        editor.putInt(KEY_LANGUAGE_POS, position);
        editor.apply();
    }

    public String getPreferredLanguage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LANGUAGE, null);
    }

    public void setTeleCMIModuleFlag(String teleCMIFlag) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_IS_TELECMI_ENABLED, teleCMIFlag);
        editor.apply();
    }

    public String getTeleCMIModuleFlag() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_IS_TELECMI_ENABLED, null);
    }

    public int getPreferredLanguagePos() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_LANGUAGE_POS, 0);
    }

    public void removeUserId() {
        SharedPreferences settings = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        settings.edit().remove(KEY_USERID).commit();
    }

    public void clearSharedPref() {
        SharedPreferences settings = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    public void clearPreferredLanguage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LANGUAGE, null);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_SLIDER_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getBoolean(IS_SLIDER_FIRST_TIME_LAUNCH, true));
    }

    public void setMOBILE_OR_EMAIL(String isMobile_Email) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_IS_MOBILE_OR_EMAIL, isMobile_Email);
        editor.apply();
    }

    public String getMOBILE_OR_EMAIL() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_IS_MOBILE_OR_EMAIL, null));
    }

    public void setUserFirstName(String firstName) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_FIRST_NAME, firstName);
        editor.apply();
    }

    public String getUserFirstName() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(KEY_FIRST_NAME, null));
    }
}