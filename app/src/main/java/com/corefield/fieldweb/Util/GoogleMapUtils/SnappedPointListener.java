package com.corefield.fieldweb.Util.GoogleMapUtils;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * //
 * Created by CFS on 10/6/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util.GoogleMapUtils
 * Version : 1.2.2
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public interface SnappedPointListener {

    void onNewSnappedPoint(List<LatLng> snappedPath);
}
