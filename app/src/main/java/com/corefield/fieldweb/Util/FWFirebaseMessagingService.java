package com.corefield.fieldweb.Util;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 * //
 * Created by CFS on 2/14/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : Firbase messaging service to receive message and send token
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * * are declared in the Manifest then the first one will be chosen.
 * //
 **/
public class FWFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FWFirebaseMessagingService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        FWLogger.logInfo(TAG, "onMessageReceived ");
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        //Data message = app is in foreground/background = handle in onMessageReceived
        //Noti massage = app is in foreground = handle in onMessageReceived
        //Noti message = app is in background = handle by automatically generated notification

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.


        //This work in foreground

        if (remoteMessage == null)
            return;

        NotificationHelper notificationHelper  = new NotificationHelper();
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            FWLogger.logInfo(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            Constant.UNREAD_COUNT ++;
            Intent unreadCountIntent = new Intent(Constant.FCM.UNREAD_FOREGROUND);
            unreadCountIntent.putExtra(Constant.FCM.UPDATE_BADGE,Constant.FCM.UPDATE_BADGE);
            LocalBroadcastManager.getInstance(this).sendBroadcast(unreadCountIntent);

            // Check if message contains a data payload.
            String dataMessage = "";
            if (remoteMessage.getData().size() > 0) {
                FWLogger.logInfo(TAG, "Data Payload: " + remoteMessage.getData().toString());
                dataMessage = remoteMessage.getData().toString();
            }
            notificationHelper.showNotification(getApplicationContext(),remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(),"12",dataMessage
                    );
        }

    }



    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        FWLogger.logInfo(TAG, "Refreshed token: " + token);

        // Saving reg id to shared preferences
        storeRegIdInPref(token);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        //Intent registrationComplete = new Intent(Constant.FCM.REGISTRATION_COMPLETE);
        //registrationComplete.putExtra("token", token);
        //LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);//Note Don not call this from here as this results in device token without User ID (So call after login only)
    }
    // [END on_new_token]

    private void storeRegIdInPref(String token) {
        SharedPrefManager.getInstance(getApplicationContext()).regFCMID(token);
    }

}
