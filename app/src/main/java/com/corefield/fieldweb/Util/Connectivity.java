package com.corefield.fieldweb.Util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;

/**
 * Util Class for Connectivity
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Utility class is used for Connectivity function
 */
public class Connectivity {

    /**
     * CHECK WHETHER INTERNET CONNECTION IS AVAILABLE OR NOT
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isNetworkAvailableRetro(Activity context) {
        ConnectivityManager objConnectivityManager;
        try {
            objConnectivityManager = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            final boolean IsWifiAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
            //final boolean IsInternetAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
            boolean IsInternetAvailable = false;
            if (objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null)
                IsInternetAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
            if (IsWifiAvailable == true || IsInternetAvailable == true)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        } finally {
            objConnectivityManager = null;
        }
    }

    public static boolean isNetworkAvailableRetroNew(Context context) {
        ConnectivityManager objConnectivityManager;
        try {
            objConnectivityManager = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            final boolean IsWifiAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
            //final boolean IsInternetAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
            boolean IsInternetAvailable = false;
            if (objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null)
                IsInternetAvailable = objConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
            if (IsWifiAvailable == true || IsInternetAvailable == true)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        } finally {
            objConnectivityManager = null;
        }
    }


}
