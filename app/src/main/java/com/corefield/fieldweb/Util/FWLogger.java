package com.corefield.fieldweb.Util;

import android.util.Log;

import com.corefield.fieldweb.BuildConfig;

/**
 * Util Class for FW Logger
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Utility class is used for custom log information
 */
public class FWLogger {
    public static void logInfo(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg);
        }
    }
}
