package com.corefield.fieldweb.Util;

/**
 * //
 * Created by CFS on 2/6/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class Constant {

    public static int UNREAD_COUNT = 0;

    public interface PaymentMode {
        int AMC_ID = 1; // AMC mode 1
        int RATE_ID = 2; //Rate mode 0
        String RATE_STRING = "Rate"; // Rate Mode string
        String AMC_STRING = "AMC"; // AMC Mode string
    }

    public interface HttpUrlConn {
        int HTTP_UNDER_MAINTENANCE = 999;
        int HTTP_ANDROID_ID_EXPIRE = 888;
    }

    public interface FCM {
        // global topic to receive app wide push notifications
        String TOPIC_GLOBAL = "global";

        // broadcast receiver intent filters
        String UNREAD_FOREGROUND = "Unread Foreground";

        // id to handle the notification in the notification tray
        int NOTIFICATION_ID = 100;
        int NOTIFICATION_ID_BIG_IMAGE = 101;
        String CLEAR_BADGE = "ClearBadge";
        String UPDATE_BADGE = "UpdateBadge";

    }


    public interface NotificationType {
        String TYPE_TASK = "Task";
        String TYPE_USER_INFO = "UserInfo";
        String TYPE_EARNINGS = "Earning";
        String TYPE_AMC = "AMC";
    }

    public interface NotificationDay {
        String DAY_TODAY = "Today";
        String DAY_YESTERDAY = "YesterDay";
        String DAY_OLD = "Old";
    }

    public interface UserGroup {
        String TECHNICIAN = "Technician";
        String OWNER = "Owner";
    }

    public interface WorkMode {
        int INSTALLATION_ID = 1; // New Installation mode 1
        int REPAIR_ID = 2; //Repair mode 2
        int SERVICE_ID = 3; //Service mode 3
        String INSTALLATION_STRING = "New Installation"; // Installation Mode string
        String REPAIR_STRING = "Repair"; // Repair Mode string
        String SERVICE_STRING = "Service"; // Repair Mode string
    }

    public interface UserDisclaimer {
        String AGREE = "Agree";
        String DIS_AGREE = "Disagree";
    }

    /*  public interface PreferredLanguage {
          String ENGLISH = "English";
          String HINDI = "हिन्दी";
          String LOCALE_ENGLISH = "en";
          String LOCALE_HINDI = "hi";
      }*/
    public interface PreferredLanguage {
        String ENGLISH = "English";
        String HINDI = "हिन्दी";
        String LOCALE_ENGLISH = "en";
        String LOCALE_HINDI = "hi";
        String LOCALE_PUNJABI = "pa";
        String LOCALE_BENGALI = "bn";
        String LOCALE_MARATHI = "mr";
        String LOCALE_KANNADA = "kn";
        String LOCALE_TAMIL = "ta";
        String LOCALE_TELUGU = "te";
        String LOCALE_MALAYALAM = "ml";
        String LOCALE_ARABIC = "ar";
    }

    /**
     * To dynamically load task by Task category
     */
    public enum TaskCategories {
        Urgent, Today, Schedule, Completed, Rejected, Ongoing, InActive, OnHold
    }
}
