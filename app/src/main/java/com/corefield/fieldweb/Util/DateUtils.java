package com.corefield.fieldweb.Util;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * //
 * Created by CFS on 11/13/2019.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.0
 * Copyright (c) 2019 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class DateUtils {
    protected static String TAG = DateUtils.class.getSimpleName();

    static public Date stringToDateFormatter(String strDate, String pattern) {
        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            date = format.parse(strDate);
            FWLogger.logInfo("DateUtils_Date : ", "" + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    static public long convertDateToMilliSeconds(Date date) {
        long timeInMilliseconds = 0;
        try {
            timeInMilliseconds = date.getTime();
            FWLogger.logInfo("Date in milli :: ", "" + timeInMilliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @param dateFormat   Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    /**
     * Returns current date time for GA
     *
     * @return Return current date in dd-MM-yyyy HH:mm format (Format must natch to GA event)
     */
    public static String getDateForWithGAFormat() {
        return DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy HH:mm");
    }

    /**
     * @param time          time to convert
     * @param inputPattern  current format of time (input time)
     * @param outputPattern in which format to convert (output format)
     * @return Convert from one format to another format
     */
    public static String convertDateFormat(String time, String inputPattern, String outputPattern) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * Compare two dates : This function is used to compare 'dateToCompare' with current date
     *
     * @param dateToCompare Date to be compared with current date (dateToCompare must have this "yyyy-MM-dd" format or else
     *                      function will try to covert it to "yyyy-MM-dd" format)
     * @return ret = 0 currentDate is equal to dateToCompare (currentDate == dateToCompare), ret = -1 currentDate is before dateToCompare (currentDate < dateToCompare)
     * ret = 1 currentDate is after dateToCompare (currentDate > dateToCompare), ret = 999 Date Format Mismatched.
     */
    static public int compareTo(String dateToCompare) {
        int ret = 999;
        if (dateToCompare != null && !dateToCompare.equalsIgnoreCase("")) {
            Date convertedToDate = DateUtils.stringToDateFormatter(dateToCompare, "yyyy-MM-dd");
            String currentDateMillis = DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd");
            FWLogger.logInfo("DateUtils", "currentDateMillis : " + currentDateMillis);
            Date currentDate = DateUtils.stringToDateFormatter(currentDateMillis, "yyyy-MM-dd");
            FWLogger.logInfo("DateUtils", "currentDate : " + currentDate);
            ret = currentDate.compareTo(convertedToDate);
        } else {
            ret = 999;
        }
        return ret;
    }

    /**
     * This function is used to convert the time format
     *
     * @param time         - the input time requires to convert
     * @param inputFormat  - format of input time (Format must match to input time)
     * @param outputFormat - expected output format
     * @return - formatted string of time
     */
    static public String timeFormatConversion(String time, String inputFormat, String outputFormat) {
        String formattedTime = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat(inputFormat);
            final Date dateObj = sdf.parse(time);
//            System.out.println(dateObj);
            formattedTime = new SimpleDateFormat(outputFormat).format(dateObj);
//            System.out.println(formattedTime);
        } catch (final ParseException e) {
            formattedTime = null;
            e.printStackTrace();
        }
        return formattedTime;
    }

    // Move code from AlertDialog to here
    public static int getCurrentHours() {
        Calendar mCurrentTime = Calendar.getInstance();
        return mCurrentTime.get(Calendar.HOUR);
    }

    /*public static Date getPlusOneHrs() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 1);
        return cal.getTime();
    }*/

    public static Date getPlusFifteenMins() {
        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.HOUR, 1);
        //As per requirement needs to add 15 mins in current time
        cal.add(Calendar.MINUTE, 15);
        return cal.getTime();
    }

    public static int getCurrentMeridian() {
        Calendar mCurrentTime = Calendar.getInstance();
        return mCurrentTime.get(Calendar.AM_PM); //0=AM and 1=PM
    }

    public static int getPositionOfHoursFromTimeSlotList(int hour, Context context) {
        int i = 0;
        for (i = 0; i < TimeSlot.getTimeSlotArrayList(context).size(); i++) {
            if (hour == Integer.valueOf(TimeSlot.getTimeSlotArrayList(context).get(i))) {
                break;
            }
        }
        return i;
    }

    public static void timePicker(Context context, EditText editText) {

        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR);
        int minute = mCurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editText.setError(null);
                editText.setText(selectedHour + ":" + String.format("%02d", selectedMinute) + ":00");
            }
        }, hour, minute, false);//True = Yes 24 hour time And False = No 12 hour
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public static String getTimeFromTimeSlot(int hour, String meridian) {
        String time = "00:00:00";
        if (meridian.equalsIgnoreCase("AM")) {
            if (hour == 12) {
                time = "00:00:00";
            } else {
                time = hour + ":" + "00:00";
            }
        } else if (meridian.equalsIgnoreCase("PM")) {
            if (hour == 12) {
                time = hour + ":00:00";
            } else {
                time = (hour + 12) + ":" + "00:00";
            }
        }
        return time;
    }

    public static void birthDayDatePicker(Context context, EditText editText) {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM-dd-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                editText.setError(null);
                editText.setText(sdf.format(myCalendar.getTime()));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
//        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());

        dialog.show();
    }

    public static void datePicker(Context context, EditText editText) {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                editText.setError(null);
                editText.setText(sdf.format(myCalendar.getTime()));
            }

        };
        DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    public static void datePickerToAcceptBackMonth(Context context, EditText editText) {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                editText.setError(null);
                editText.setText(sdf.format(myCalendar.getTime()));
            }

        };
        DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        /*COMMENTED TO BLOCK LAST MONTH CALENDAR*/
        /*dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);*/
        dialog.show();
    }


    public static void currentDatePicker(EditText editText) {
        Date currentTime = Calendar.getInstance().getTime();
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        FWLogger.logInfo("TAG", "CURRENT DATE : " + sdf.format(currentTime.getTime()));
        editText.setText(sdf.format(currentTime.getTime()));
    }

    public static void nextDayDatePicker(EditText editText) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        FWLogger.logInfo("TAG", "NEXT DATE : " + sdf.format(tomorrow.getTime()));
        editText.setText(sdf.format(tomorrow.getTime()));
    }

    public static boolean isFutureDate(String strDate, String strTime) {
        if (strTime.equalsIgnoreCase("00:00:00")) strTime = "24:00:00";
        FWLogger.logInfo("Date : ", strDate + "T" + strTime + "Z");
        if (DateUtils.convertDateToMilliSeconds(DateUtils.stringToDateFormatter(strDate + "T" + strTime + "Z", "yyyy-MM-dd'T'HH:mm:ss'Z'")) > Calendar.getInstance().getTimeInMillis()) {
            FWLogger.logInfo("Date : ", "isFutureDate true");
            return true;
        } else {
            FWLogger.logInfo("Date : ", "isFutureDate false");
            return false;
        }
    }

    public static int getCurrentDateOnly() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calDate = Calendar.getInstance();
        FWLogger.logInfo("TAG", "GET CURRENT DATE : " + dateFormat.format(calDate.getTime()).split("-")[0]);

        int currentDate = Integer.parseInt(dateFormat.format(calDate.getTime()).split("-")[0]);

        return currentDate - 1;
    }

    public static String getSelectedDate(String filter) {
        String setDateFilter = "";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        switch (filter) {
            case "Today":
                FWLogger.logInfo(TAG, "Today : " + DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));
                setDateFilter = DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy");
                break;
            case "Week":
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.set(Calendar.DAY_OF_WEEK, 1);
                FWLogger.logInfo(TAG, "Week : " + dateFormat.format(calendar.getTime()));
                setDateFilter = dateFormat.format(calendar.getTime()) + " to " + DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy");
                break;
            case "Month":
                Calendar c = Calendar.getInstance(); // this takes current date
                c.set(Calendar.DAY_OF_MONTH, 1);
                FWLogger.logInfo(TAG, "First Date : " + dateFormat.format(c.getTime()));
                FWLogger.logInfo(TAG, "Month : " + DateUtils.getCurrentDateOnly());
                setDateFilter = dateFormat.format(c.getTime()) + " to " + DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy");
                break;
            case "Year":
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(Calendar.DAY_OF_YEAR, 1);
                FWLogger.logInfo(TAG, "Week : " + dateFormat.format(cal.getTime()));
                setDateFilter = dateFormat.format(cal.getTime()) + " to " + DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy");
                FWLogger.logInfo(TAG, "Year : " + DateUtils.getCurrentDateOnly());
                break;
            default:
                break;
        }
        return setDateFilter;
    }

    public static String getMonthName(int month) {
        String[] mons = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        String currentMonthName = mons[month];
        return currentMonthName;
    }
}
