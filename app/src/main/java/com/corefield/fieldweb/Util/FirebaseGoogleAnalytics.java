package com.corefield.fieldweb.Util;

import com.corefield.fieldweb.BuildConfig;

/**
 * //
 * Created by CFS on 5/14/2021.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.4.0
 * Copyright (c) 2021 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class FirebaseGoogleAnalytics {

    /**
     * This interface define Event Parameter
     * Value is equivalent to param column in GA
     */
    public interface Param {

        String USER_ID = "User_Id";
        String USERNAME = "Username";
        String USER_TYPE = "User_Type";
        String TIME_STAMP = "Time";
        String PLATFORM = "Platform";
        String MOBILE_NO = "Mobile_No";
        String EMAIL_ID = "Email_Id";
        String FIRST_NAME = "First_Name";
        String BOTTOM_MENU = "Bottom_Menu";
        String TOOLBAR_MENU = "Toolbar_Menu";
        String TASK_ID = "Task_Id";
        String TASK_STATUS = "Task_Status";
        String TASK_TYPE = "Task_Type";
        String TASK_STATE = "Task_State";
        String WORK_MODE = "Work_Mode";
        String AMOUNT = "Amount";
        String VERSION_NAME = "Version_Name";
        String VERSION_CODE = "Version_Code";
        String AVAILABLE_VERSION_CODE = "Available_Version_Code";
        String UPDATE_AVAILABLE = "Update_Available";
        String OTP_ATTEMPT = "Otp_Attempt";
        String NO_OF_TECH = "No_Of_Tech";
        String TASK_CREATED_BY = "Task_Created_By";
        String TASK_ADDED = "Task_Added";
        String OPERATION = "Operation";
        String ACTION = "Action";
        String EXP_NAME = "Exp_Name";
        String BALANCE = "Balance";
        String ITEM_ID = "Item_Id";
        String ITEM_QTY = "Item_Qty";
        String TECH_ID = "Tech_Id";
        String AMC_ID = "AMC_Id";
        String ENQ_ID = "Enq_Id";
        String NO_OF_NOTES = "No_Of_Notes";
        String TASK_UPDATED = "Task_Updated";
        String TASK_REASSIGNED = "Task_Reassigned";
        String REFERRAL_CODE = "Referral_Code";
    }

    /**
     * This interface define the Firebase GA Event
     * Value is Equivalent to Event Name in GA
     */
    public interface Event {
        String BOTTOM_NAV_MENU_CLICK = "bottom_nav_menu_click";
        String TOOLBAR_MENU_CLICK = "toolbar_menu_click";
        String END_TASK = "end_task";
        String ON_START_NAV = "on_start_nav";
        String REACHED = "reached";
        String NOT_REACHED = "not_reached";
        String TASK_CLICK = "task_clicked";
        String START_TASK = "start_task";
        String TASK_CLOSURE = "task_closure";
        String GENERATE_INVOICE = "generate_invoice";
        String PAYMENT_RECEIVED = "payment_received";
        String REJECT_TASK = "reject_task";
        String ENABLE_PERMISSION = "enable_permission";
        String APP_UPDATE_RECEIVED = "app_update_received";
        String APP_UPDATE_CONF = "app_update_Conf";
        String OTP_SENT = "otp_sent";
        String OTP_SUBMIT = "otp_submit";
        String ADD_BULK_TECH = "add_bulk_tech";
        String CUSTOMER_LIST = "customer_list";
        String ADD_TASK = "add_task";
        String ENQUIRY_DETAILS = "enquiry_details";
        String ENQUIRY_LIST = "enquiry_list";
        String ADD_BAL = "add_bal";
        String DEDUCT_BAL = "deduct_bal";
        String EXPENSE = "expense";
        String EXP_DETAIL = "exp_detail";
        String EXP_PHOTO = "exp_photo";
        String EXP_TECHNICIAN = "exp_technician";
        String OWNER_ATTENDANCE = "owner_attendance";
        String ITEM_INVENTORY = "item_inventory";
        String LEAD_MANGEMENT = "lead_management";
        String ADD_LEAD = "add_lead";
        String INVENTORY_CLICK = "inventory_click";
        String INVENTORY_CONFIRMATION = "inventory_confirmation";
        String PROFILE = "profile";
        String EDIT_CLICK_PROFILE = "edit_click_profile";
        String CONFIRM_PROFILE = "confirm_profile";
        String TECH_ATTENDANCE = "tech_attendance";
        String TECH_ITEM_INVENTORY = "tech_item_inventory";
        String TECH_LIST = "tech_list";
        String AMC = "amc";
        String ADD_AMC = "add_amc";
        String AMC_DETAILS = "amc_details";
        String ADD_ENQ = "add_enq";
        String FORGOT_PASSWORD = "forgot_password";
        String CHANGE_PASSWORD = "change_password";
        String REFRESH_INTERNET = "refresh_internet";
        String GPS_ENABLE = "gps_enable";
        String ADD_NOTES = "add_notes";
        String REASSIGN_TASK = "reassign_task";
        String UPDATE_TASK = "update_task";
        String CONTACT_SUPPORT = "contact_support";
        String PASSBOOK = "passbook";
        String DUPLICATE_LOGIN = "duplicate_login";
        // ADDONS EVENT
        String FIELDFINZ_CLICK = "fieldfinz_click";
        String FIELDSTAFFING_CLICK = "fieldstaffing_click";
        String FIELDWEBFIXIT_CLICK = "fieldwebfixit_click";
        String FIELDSTRADE_CLICK = "fieldtrade_click";
    }

    /**
     * This interface define the Firebase GA User Properties
     * Value is Equivalent to User Properties in GA
     */
    public interface UserProperty {

    }

    public interface Defaults {
        String PLATFORM_ANDROID = "Android";
        String AMC = "AMC";
        String ENQ = "Enquiry";
        String TASK = "Task";
        String CLICKED = "Clicked";
        String TRANSACTION = "Transaction";
        String CONFIRMATION = "Confirmation";
        String ADD = "Add";
        String ISSUE = "Issue";
        String UNASSIGN = "Unassign";
        String UPDATE = "Update";
        String EDIT = "Edit";
        String DOWNLOAD = "Download";
        String TECH_CLICKED = "Tech Clicked";
        String REASSIGN = "Reassign";
    }
}
