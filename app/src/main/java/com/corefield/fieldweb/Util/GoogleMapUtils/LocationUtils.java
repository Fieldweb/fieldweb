package com.corefield.fieldweb.Util.GoogleMapUtils;

import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;

import com.corefield.fieldweb.R;

import java.text.DateFormat;
import java.util.Date;

/**
 * //
 * Created by CFS on 3/21/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class LocationUtils {
    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link android.location.Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }
}
