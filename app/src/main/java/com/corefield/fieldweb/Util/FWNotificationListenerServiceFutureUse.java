package com.corefield.fieldweb.Util;

import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import androidx.annotation.RequiresApi;

/**
 * //
 * Created by CFS on 2/18/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class FWNotificationListenerServiceFutureUse extends NotificationListenerService {

    private static String TAG = FWNotificationListenerServiceFutureUse.class.getSimpleName();

    /*
        These are the package names of the apps. for which we want to
        listen the notifications
     */
    private static final class ApplicationPackageNames {
        public static final String FW_DEV_PACK_NAME = "com.corefield.fieldweb.dev";
        public static final String FW_PACK_NAME = "com.corefield.fieldweb";
        public static final String FW_QA_PACK_NAME = "com.corefield.fieldweb.qa";
    }

    /*
        These are the return codes we use in the method which intercepts
        the notifications, to decide whether we should do something or not
     */
    public static final class InterceptedNotificationCode {
        public static final int FW_DEV_CODE = 1;
        public static final int FW_CODE = 2;
        public static final int FW_QA_CODE = 3;
        public static final int OTHER_NOTIFICATIONS_CODE = 4; // We ignore all notification with code == 4
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        int notificationCode = matchNotificationCode(sbn);
        FWLogger.logInfo(TAG,"onNotificationPosted ");
        if(notificationCode != InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE){
            Intent intent = new  Intent("com.corefield.fieldweb.FieldWeb");
            intent.putExtra("Notification Code", notificationCode);
            sendBroadcast(intent);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        int notificationCode = matchNotificationCode(sbn);
        FWLogger.logInfo(TAG,"onNotificationRemoved ");
        if(notificationCode != InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE) {

            StatusBarNotification[] activeNotifications = this.getActiveNotifications();

            if(activeNotifications != null && activeNotifications.length > 0) {
                for (int i = 0; i < activeNotifications.length; i++) {
                    if (notificationCode == matchNotificationCode(activeNotifications[i])) {
                        Intent intent = new  Intent("com.corefield.fieldweb.FieldWeb");
                        intent.putExtra("Notification Code", notificationCode);
                        sendBroadcast(intent);
                        break;
                    }
                }
            }
        }
    }

    private int matchNotificationCode(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        FWLogger.logInfo(TAG,"matchNotificationCode ");
        switch (packageName) {
            case ApplicationPackageNames.FW_DEV_PACK_NAME:
                return(InterceptedNotificationCode.FW_DEV_CODE);

            case ApplicationPackageNames.FW_QA_PACK_NAME:
                return(InterceptedNotificationCode.FW_QA_CODE);

            case ApplicationPackageNames.FW_PACK_NAME:
                return(InterceptedNotificationCode.FW_CODE);

            default:
                return(InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE);
        }
        /*if(packageName.equals(ApplicationPackageNames.FW_DEV_PACK_NAME)){
            return(InterceptedNotificationCode.FW_DEV_CODE);
        }
        else if(packageName.equals(ApplicationPackageNames.FW_QA_PACK_NAME)){
            return(InterceptedNotificationCode.FW_QA_CODE);
        }
        else if(packageName.equals(ApplicationPackageNames.FW_PACK_NAME)){
            return(InterceptedNotificationCode.FW_CODE);
        }
        else{
            return(InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE);
        }*/
    }
}
