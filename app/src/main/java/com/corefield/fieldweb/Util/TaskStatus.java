package com.corefield.fieldweb.Util;

/**
 * interface for Task Status
 *
 * @author CoreField
 * @version 1.1
 * @implNote This interface is used for static value of Task Status
 */
public interface TaskStatus {
    int ONHOLD = 5;
    int IN_ACTIVE = 4;
    int ON_GOING = 3;
    int REJECTED = 2;
    int COMPLETED = 1;
}
