package com.corefield.fieldweb.Util;

import android.content.Context;

import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * //
 * Created by CFS on 1/31/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class TimeSlot {

    public static String[] getTimeSlot(Context context) {
        return new String []{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",context.getString(R.string.time)};
    }

    public static ArrayList<String> getTimeSlotArrayList(Context context) {
        return new ArrayList<String>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",context.getString(R.string.time))) ;
    }

    public static ArrayList<String> getYearArrayList() {
        return new ArrayList<String>(Arrays.asList("2020","2021","2022","2023","2024")) ;
    }

    public static ArrayList<String> getServiceOccurrenceArrayList(Context context) {
        return new ArrayList<String>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", context.getString(R.string.no_of_services))) ;
    }
}
