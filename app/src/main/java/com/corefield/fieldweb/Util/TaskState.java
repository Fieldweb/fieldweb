package com.corefield.fieldweb.Util;

/**
 * interface for Task State
 *
 * @author CoreField
 * @version 1.1
 * @implNote This interface is used for static value of Task State
 */
public interface TaskState {
    int NOT_STARTED = 0; // TASKSTATE 0 : Task not stated then open TechTaskTrackingFragment
    int STARTED_NOT_ENDED = 1; // TASKSTATE 1 : Task started but not ended then open TechTaskTrackingFragment
    int ENDED_NO_PAYMENT = 2; // TASKSTATE 2 : Task Ended but payment not rec then open TechPaymentReceviedFragment directly
    int PAYMENT_RECEIVED = 3; // TASKSTATE 3 : Show the pop  that task has been already completed
    int TASK_CLOSURE = 4; // TASKSTATE 4 : Task is closed //TODO this need to implement
//    int ENDED_NO_CLOSURE = 5; // TASKSTATE 5 : Task is not Closed //TODO this need to implement
}



