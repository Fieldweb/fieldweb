package com.corefield.fieldweb.Util;

import android.app.Application;
import com.google.firebase.BuildConfig;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

/**
 * //
 * Created by CFS on 2/28/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Util
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class FWApplication extends Application {

    private String TAG = FWApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        FWLogger.logInfo(TAG, "onCreate");
        //Note : Do not set Firebase Crashlytics for int_qa as build type int_qa and release have
        // the same package id and they may get conflicted in production
        if (!BuildConfig.BUILD_TYPE.equalsIgnoreCase("int_qa")) {
            FWLogger.logInfo(TAG, "not int_qa build");
            FirebaseApp.initializeApp(getApplicationContext());
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        }
    }

}
