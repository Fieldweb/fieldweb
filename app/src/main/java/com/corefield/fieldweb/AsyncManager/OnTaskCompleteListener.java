package com.corefield.fieldweb.AsyncManager;

import com.corefield.fieldweb.Network.URLConnectionResponse;

/**
 * Task complete Listener for async task
 * Calls in postExecute() once API call is completed
 */
public interface OnTaskCompleteListener {
    /**
     * Calls whn API response fully completed and loaded into  Result data of urlConnectionResponse
     *
     * @param urlConnectionResponse Result data
     * @param classType             class name of response in string format
     */
    void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType);
}
