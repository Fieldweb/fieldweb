package com.corefield.fieldweb.AsyncManager.CommonAsyncManager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.corefield.fieldweb.Network.URLConstant;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;


public class DataAsyncTask extends AsyncTask<String, String, String> {
    private String oDataService;
    private DataCallBack oDataCallBack;
    private String TAG = DataAsyncTask.class.getSimpleName();
    private Context context;
    private DataOperation operation;
    private String payloadData;

    public DataAsyncTask(Context context, DataOperation operation, String payloadData, String oDataService, DataCallBack oDataCallBack) {
        this.context = context;
        this.oDataService = oDataService;
        this.oDataCallBack = oDataCallBack;
        this.operation = operation;
        this.payloadData = payloadData;
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        switch (operation) {
            case WEBAPI_POST:
                result = WEB_API_POST();
                break;
            case WEBAPI_POST_TELECMI:
                result = TELEICMI_API_POST();
                break;

        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.contains("Exception::")) {
            result = result.replace("Exception::", "");
            if (result.startsWith("Bad Request"))
                result = result.replace("Bad Request", "");
            oDataCallBack.onFailed(result);
        } else {
            oDataCallBack.onSuccess(result);
        }
    }


    private String WEB_API_POST() {
        String result = "";
        int responseStatus = 0;
        HttpClient httpclient = null;
        HttpPost httpPost = null;
        String strUrl = URLConstant.Base.BASEURL + oDataService;
        try {

            httpPost = new HttpPost(strUrl);
            //SimpleSSLSocketFactory sslFactory = new SimpleSSLSocketFactory(null);
            //sslFactory.setHostnameVerifier(SimpleSSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            // Enable HTTP parameters
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            // Register the HTTP and HTTPS Protocols. For HTTPS, register our custom SSL Factory object.
            //SchemeRegistry registry = new SchemeRegistry();
            //registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            //registry.register(new Scheme("https", sslFactory, 443));

            // Create a new connection manager using the newly created registry and then create a new HTTP client using this connection manager
            //ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

//                httpget.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getUsername(context), getPassword(context)), "UTF-8", false));

            // httpclient = new DefaultHttpClient(ccm, params);
            httpclient = new DefaultHttpClient();
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            httpPost.setEntity(new StringEntity(payloadData, "UTF-8"));
//            Log.i("JSON", String.valueOf ( payloadData.replaceAll ( "\n","" )));
            HttpResponse response = httpclient.execute(httpPost);
            responseStatus = response.getStatusLine().getStatusCode();
            if (responseStatus == 201 || responseStatus == 200 || responseStatus == 202) {

                result = EntityUtils.toString(response.getEntity());


                Log.d(TAG, oDataService + " response: " + result);
            } else {
                result = "Exception::" + response.getStatusLine().getReasonPhrase() + "-" + response.getStatusLine().getStatusCode();
            }
        } catch (Exception e) {
            result = "Exception::" + e.toString();
            Log.d(TAG, oDataService + " error: " + e.toString());
        }
        return result;
    }

    private String TELEICMI_API_POST() {
        String result = "";
        int responseStatus = 0;
        HttpClient httpclient = null;
        HttpPost httpPost = null;
        /* String strUrl = URLConstant.Base.BASEURL + oDataService;*/
       /* String strUrl = "https://piopiy.telecmi.com/v1/pcmo_make_call";*/
        String strUrl = "https://rest.telecmi.com/v2/ind_pcmo_make_call";
        try {

            httpPost = new HttpPost(strUrl);
            //SimpleSSLSocketFactory sslFactory = new SimpleSSLSocketFactory(null);
            //sslFactory.setHostnameVerifier(SimpleSSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            // Enable HTTP parameters
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            httpclient = new DefaultHttpClient();
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            httpPost.setEntity(new StringEntity(payloadData, "UTF-8"));
//            Log.i("JSON", String.valueOf ( payloadData.replaceAll ( "\n","" )));
            HttpResponse response = httpclient.execute(httpPost);
            responseStatus = response.getStatusLine().getStatusCode();
            if (responseStatus == 201 || responseStatus == 200 || responseStatus == 202) {

                result = EntityUtils.toString(response.getEntity());


                Log.d(TAG, oDataService + " response: " + result);
            } else {
                result = "Exception::" + response.getStatusLine().getReasonPhrase() + "-" + response.getStatusLine().getStatusCode();
            }
        } catch (Exception e) {
            result = "Exception::" + e.toString();
            Log.d(TAG, oDataService + " error: " + e.toString());
        }
        return result;
    }


}
