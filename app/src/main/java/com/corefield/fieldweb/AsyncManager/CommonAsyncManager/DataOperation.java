package com.corefield.fieldweb.AsyncManager.CommonAsyncManager;

public enum DataOperation {
    GET,
    POST,
    PUT,
    REGISTRATION,
    DELETE,
    WEBAPI_POST,
    WEBAPI_POST_TELECMI
}
