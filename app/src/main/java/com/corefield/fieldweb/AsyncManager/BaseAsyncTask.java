package com.corefield.fieldweb.AsyncManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;

import com.corefield.fieldweb.FieldWeb.Dialogs.AlertDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.net.HttpURLConnection;

public abstract class BaseAsyncTask extends AsyncTask<Object, Object, Boolean> {
    /**
     * Status of task
     */
    public static boolean ISRUNNING = true;
    public static boolean ISSTOPED = false;
    protected boolean TaskState = ISSTOPED;
    protected ProgressDialog progressDialog;
    protected Context mContext;
    public OnTaskCompleteListener onTaskCompleteListener;
    protected boolean mShowDialog = true;

    /**
     * Priority enum
     */
    public enum Priority {
        LOW, MEDIUM, HIGH
    }

    /**
     * Common response header for all API
     */
    protected URLConnectionResponse mUrlConnectionResponse;

    protected Priority priority;

    /**
     * Constructor based on priority
     *
     * @param context  Activity/Fragment context
     * @param priority LOW,MEDIUM,HIGH
     */
    public BaseAsyncTask(Context context, Priority priority) {
        this.mContext = context;
        this.priority = priority;
    }


    /**
     * Constructor based on priority
     *
     * @param context    Activity/Fragment context
     * @param priority   LOW,MEDIUM,HIGH
     * @param showDialog show(visibility) progress dialog Default:true
     */
    public BaseAsyncTask(Context context, Priority priority, boolean showDialog) {
        this.mContext = context;
        this.priority = priority;
        this.mShowDialog = showDialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressDialog == null && mShowDialog) {
            progressDialog = new ProgressDialog(mContext);
            if (!progressDialog.isShowing()) {
                progressDialog.setCancelable(true);//you can cancel it by pressing back button
//                progressDialog.setMessage(mContext.getResources().getString(R.string.please_wait));
//                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                progressDialog.show();//displays the progress bar
                progressDialog.setContentView(R.layout.progress_dialog);
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean == null && mShowDialog) {
            if (mContext != null && !AlertDialog.isAlertDialogShowing) {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialog(mContext);
            }
        } else if (aBoolean != null && mUrlConnectionResponse != null && mShowDialog) {
            if (mUrlConnectionResponse.statusCode == Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (!AlertDialog.isAlertDialogShowing) {
                    SharedPrefManager.getInstance(mContext).logout(false);
                    AlertDialog.showMaintenanceDialog(mContext, mUrlConnectionResponse.message);
                    aBoolean = null;
                    cancel(true);
                }

            } else if (mUrlConnectionResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                if (!AlertDialog.isAlertDialogShowing) {
                    SharedPrefManager.getInstance(mContext).logout(false);
                    AlertDialog.showTokenExpiredDialog(mContext, mUrlConnectionResponse.message);
                    aBoolean = null;
                    cancel(true);
                }

            } else if (mUrlConnectionResponse.statusCode == Constant.HttpUrlConn.HTTP_ANDROID_ID_EXPIRE) {
                if (!AlertDialog.isAlertDialogShowing) {
                    SharedPrefManager.getInstance(mContext).logout(false);
                    AlertDialog.showInvalidAndroidInstanceDialog(mContext);
                    aBoolean = null;
                    cancel(true);
                }

            }
        }

        if (progressDialog != null && mShowDialog) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }

    }

    @Override
    protected Boolean doInBackground(Object... objects) {
        return Connectivity.isNetworkAvailable(mContext);
    }

    @Override
    protected void onCancelled() {
        if (onTaskCompleteListener != null) {
            onTaskCompleteListener = null;
        }
        if (progressDialog != null && mShowDialog) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
        super.onCancelled();
    }
}
