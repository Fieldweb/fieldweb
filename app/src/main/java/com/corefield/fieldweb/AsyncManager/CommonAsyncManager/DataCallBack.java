package com.corefield.fieldweb.AsyncManager.CommonAsyncManager;


public interface DataCallBack {


    public void onSuccess(Object result);

    public void onFailed(Object result);

}
