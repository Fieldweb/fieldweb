package com.corefield.fieldweb.Listener;

/**
 * interface for Dialog Update Listener
 *
 * @author CoreField
 * @version 1.1
 * @implNote This interface is used to update address picked for google place picker
 */
public interface DialogUpdateListener {
    void onDialogUpdate(Object... object);
}
