package com.corefield.fieldweb.Listener;

import android.view.View;

/**
 * interface for Recycler Touch Listener
 *
 * @author CoreField
 * @version 1.1
 * @implNote This interface is used to on click of list in recycler view
 */
public interface RecyclerTouchListenerUpdateItem {
    void onUpdateClick(View view, int position);
}