package com.corefield.fieldweb.LocationService;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.TravelPathHistory.TravelledPathHistory;
import com.corefield.fieldweb.FieldWeb.Database.DatabaseHandler;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.GoogleMapUtils.LocationUpdatesService;
import com.corefield.fieldweb.Util.GoogleMapUtils.LocationUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class GoogleLocationService extends Service {

    private static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;

    private static final String TAG = LocationUpdatesService.class.getSimpleName();

    DatabaseHandler databaseHandler;

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 300000;
    /**
     * The desired displacement for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_SMALLEST_DISPLACEMENT = 10;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
  /*  private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;*/

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 321654987;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;


    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;
    String UserId = "";

    /**
     * The current location.
     */
    private Location mLocation;
    private DatabaseReference databaseReference;

    public GoogleLocationService() {
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //For sending User to FirebaseDb
        UserId = "" + SharedPrefManager.getInstance(this).getUserId();
        databaseReference = FirebaseDatabase.getInstance().getReference(UserId);
        databaseHandler = new DatabaseHandler(getApplicationContext());

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.i(TAG, "onCreate onLocationResult");
                super.onLocationResult(locationResult);
                //save as last location
                SharedPrefManager.getInstance(getApplicationContext()).addUserLocation(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
                onNewLocation(locationResult.getLastLocation());
            }
        };

        Log.i(TAG, "onCreate after onLocationResult");
        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            //Configure the notification channel, NO SOUND
            mChannel.setDescription("no sound");
            mChannel.setSound(null, null);
            mChannel.enableLights(false);
            mChannel.setLightColor(Color.BLUE);
            mChannel.enableVibration(false);
            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false);

        getLastLocation();

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }
        // Tells the system to not try to recreate the service after it has been killed.
//        return START_NOT_STICKY;
        return START_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration) {
            Log.i(TAG, "Starting foreground service");
            // startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        //LocationUtils.setRequestingLocationUpdates(this, true);
        startService(new Intent(this.getApplicationContext(), LocationUpdatesService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            //LocationUtils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            //LocationUtils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            //LocationUtils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                                //save as last location
                                SharedPrefManager.getInstance(getApplicationContext()).addUserLocation(mLocation.getLatitude(), mLocation.getLongitude());
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
            mFusedLocationClient.getLastLocation().addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, "onFailure Failed to get location.");
                    //Toast.makeText(LocationUpdatesService.this, "Failed to get location.Please restart your Location service from setting and try again.", Toast.LENGTH_LONG).show();
                }
            });
            //Call this to get location (Last location) at first time if no displacement by user
            onNewLocation(SharedPrefManager.getInstance(getApplicationContext()).getUserLocation());
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        Log.i(TAG, "New Travelled location after 5 min: " + location);

        mLocation = location;

        // Notify anyone listening for broadcasts about the new location.
        Intent intent = new Intent(ACTION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, mLocation);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

       /* databaseReference.child("lat").setValue(mLocation.getLatitude());
        databaseReference.child("lng").setValue(mLocation.getLongitude());
//        databaseReference.child("angle").setValue(90);
        databaseReference.child("step").setValue(5);*/
        try {
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            String dateTime = currentDate + "T" + currentTime + "Z";
            //DateTime currdateTime = DateTime.parse(dateTime);

            TravelledPathHistory.ResultData obj = new TravelledPathHistory.ResultData();
            obj.setLatitude(String.valueOf(mLocation.getLatitude()));
            obj.setLongitude(String.valueOf(mLocation.getLongitude()));
            obj.setLocAddress("");
            obj.setTaskId(0);
            obj.setUserID(Integer.parseInt(UserId));
            obj.setCreatedBy(Integer.parseInt(UserId));
            obj.setCreatedDate(dateTime);
            obj.setUpdatedBy(Integer.parseInt(UserId));
            obj.setUpdatedDate("");
            databaseHandler.insertTravelledPathData(obj);


            if (mNotificationManager == null)
                mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            // Update notification content if running as a foreground service.
            if (serviceIsRunningInForeground(this)) {
                assert mNotificationManager != null;
                mNotificationManager.notify(NOTIFICATION_ID, getNotification());
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        //mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(UPDATE_SMALLEST_DISPLACEMENT);
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public GoogleLocationService getService() {
            return GoogleLocationService.this;
        }
    }

    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdatesService.class);

        //CharSequence text = LocationUtils.getLocationText(mLocation);
        CharSequence text = "Updating your location...";

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_MUTABLE);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeActivityNew.class), PendingIntent.FLAG_MUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .addAction(R.drawable.fw_logo, "Open",
                        activityPendingIntent)
                .addAction(R.drawable.fw_logo, "Remove Updates",
                        servicePendingIntent)
                .setContentText(text)
                .setContentTitle(LocationUtils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.fw_logo)
                .setTicker(text)
                .setVibrate(null)
                .setSound(null)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }


    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    // START ::: EXTRA CODE ADDED FOR Screen Lock and foreground
    @Override
    public ComponentName startForegroundService(Intent service) {
        try {
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
            restartServiceIntent.setPackage(getPackageName());
            startService(restartServiceIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
        return super.startForegroundService(service);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        try {
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
            restartServiceIntent.setPackage(getPackageName());
            startService(restartServiceIntent);
            super.onTaskRemoved(rootIntent);
        } catch (Exception e) {
            e.getMessage();
        }
    }

}