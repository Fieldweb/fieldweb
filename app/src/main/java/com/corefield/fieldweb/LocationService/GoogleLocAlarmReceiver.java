package com.corefield.fieldweb.LocationService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GoogleLocAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Context mContext = context.getApplicationContext();

        if (mContext == null) {
            mContext = context;
        }

        try {
            Intent serviceIntent = new Intent(mContext, GoogleLocationService.class);
            mContext.startService(serviceIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
}
