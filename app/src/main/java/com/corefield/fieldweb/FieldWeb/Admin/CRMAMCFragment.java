package com.corefield.fieldweb.FieldWeb.Admin;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CRMAMCListAdapter;
import com.corefield.fieldweb.Adapter.CRMTasksListAdapter;
import com.corefield.fieldweb.Adapter.CustomerListAdapterNew;
import com.corefield.fieldweb.Adapter.DatesListAdapter;
import com.corefield.fieldweb.Adapter.EnquiryListAdapterNew;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.AMC.AMCTypeList;
import com.corefield.fieldweb.DTO.CRMTask.CRMAMCkList;
import com.corefield.fieldweb.DTO.CRMTask.CRMTaskList;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.DatesDto;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateEnquiryAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Listener.RecyclerTouchListenerUpdateItem;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for CRMFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Enquiry and Customer list
 */
public class CRMAMCFragment extends Fragment implements OnTaskCompleteListener, View.OnClickListener, RecyclerTouchListener, RecyclerTouchListenerUpdateItem {
    protected static String TAG = CRMAMCFragment.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewList;
    private RecyclerView mcrmRecyclerViewList;
    private RecyclerView mAmcRecyclerViewList;
    private Button mButtonEnquiries, mButtonCustomers;
    private List<CRMTaskList.ResultData> mCRMTaskList;
    private List<CRMAMCkList.ResultData> mCRMAmcList;
    private EnquiryListAdapterNew mEnquiryListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLinearLayoutManager;
    private LinearLayoutManager mLinearLayoutManagerAmc;
    private SearchView mSearchView;
    CRMTasksListAdapter crmtasksListAdapter;
    CRMAMCListAdapter crmamcListAdapter;
    //
    public List<DatesDto> mDatesLists = null;
    DatesListAdapter mDatesListAdapter;
    public int mSelectedDate;

    TextView txtTaskCount, txtAmcCount;

    // PAGE INDEX LOGIC
    private static final int PAGE_START = 1;
    private int mCurrentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private boolean isLoadFirstTime = false;
    LinearLayout enquiryCustListLayout;
    private List<CustomerList.ResultData> mCustomerList;
    private CustomerListAdapterNew mCustomerListAdapter;
    private String mSelection = "", currentMonthName = "", mCustName = "";
    Bundle mBundle;
    private int custId;
    CardView taskCardview, amcCardView;
    LinearLayout linear_user_header;
    ImageView noResultImg;


    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_crm_amc, container, false);
        //NOTE: Log GA event
        mBundle = new Bundle();
        mBundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        mBundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        mBundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        mBundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        mBundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);

        inIT();
        return mRootView;
    }

    private void inIT() {
        mRecyclerViewList = mRootView.findViewById(R.id.recycler_list_view);
        mcrmRecyclerViewList = mRootView.findViewById(R.id.recycler_list_viewcrm);
        mAmcRecyclerViewList = mRootView.findViewById(R.id.recycler_list_viewAmc);

        noResultImg = mRootView.findViewById(R.id.noResultImg);

        mButtonEnquiries = mRootView.findViewById(R.id.button_enquiries);
        mButtonCustomers = mRootView.findViewById(R.id.button_customers);
        mSearchView = mRootView.findViewById(R.id.edittext_search);

        enquiryCustListLayout = mRootView.findViewById(R.id.enquiryCustListLayout);
        linear_user_header = mRootView.findViewById(R.id.linear_user_header);
      /*  mTaskTypeSpinner = mRootView.findViewById(R.id.spin_task_type);
        mTaskStatusSpinner = mRootView.findViewById(R.id.spin_task_status);
        mEditTextYearMonth = mRootView.findViewById(R.id.editText_year_month);

        imgTaskNxt = mRootView.findViewById(R.id.imgTaskNxt);
        imgNxt2 = mRootView.findViewById(R.id.imgNxt2);

        txtTaskCount = mRootView.findViewById(R.id.txtTaskCount);
        txtAmcCount = mRootView.findViewById(R.id.txtAmcCount);*/

        taskCardview = mRootView.findViewById(R.id.taskCardview);
        amcCardView = mRootView.findViewById(R.id.amcCardView);

        //taskCardview.setOnClickListener(this);
        //amcCardView.setOnClickListener(this);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                custId = getBundle.getInt("CustID");
                mCustName = getBundle.getString("CustName");
                //textCustName.setVisibility(View.VISIBLE);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(mCustName);
                //textCustName.setText(mCustName);
                enquiryCustListLayout.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }        //mSearchView.setQueryHint("Search by Enq no., Customer name...");
       /* mButtonEnquiries.setOnClickListener(this);
        mButtonCustomers.setOnClickListener(this);*/

        mLinearLayoutManagerAmc = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mAmcRecyclerViewList.addOnScrollListener(new PaginationScrollListener(mLinearLayoutManagerAmc) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //
        mSelection = "AMC";
        mCurrentPage = PAGE_START;

        getCRMAMCList();

    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        if (mSelection.equalsIgnoreCase("AMC")) {
            getCRMAMCList();
        }

    }

    private void getCRMAMCList() {
      /*  mGetCRM_amcListAsyncTask = new GetCRM_AMCListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mGetCRM_amcListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), custId, mCurrentPage);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int ownerId = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<CRMAMCkList> call = RetrofitClient.getInstance(getContext()).getMyApi().getCRMAMCList(ownerId, custId, mCurrentPage);
                call.enqueue(new retrofit2.Callback<CRMAMCkList>() {
                    @Override
                    public void onResponse(Call<CRMAMCkList> call, Response<CRMAMCkList> response) {
                        try {
                            if (response.code() == 200) {
                                CRMAMCkList crmamCkList = response.body();
                                if (crmamCkList.getCode().equalsIgnoreCase("200") && crmamCkList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + crmamCkList.getMessage());
                                    if (crmamCkList != null) {
                                        mCRMAmcList = new ArrayList<>();
                                        mCRMAmcList = crmamCkList.getResultData();
                                        if (mCRMAmcList.size() > 0) {
                                            setData();
                                        } else {
                                            noResultImg.setVisibility(View.VISIBLE);
                                            noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CRMAMCkList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in CRMGetAMCList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in CRMGetAMCList API:");
            ex.getMessage();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;
            case R.id.button_enquiries:
               /* mSelection = "ENQ";
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
                mSearchView.setQueryHint("Search by Enq no., Customer name...");
                mButtonEnquiries.setBackground(getResources().getDrawable(R.drawable.bg_curve_border_selected));
                mButtonEnquiries.setTextColor(getResources().getColor(R.color.black));
                mButtonCustomers.setBackground(getResources().getDrawable(R.drawable.bg_curve_border));
                mButtonCustomers.setTextColor(getResources().getColor(R.color.White));
                getEnquiryList();
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);*/
                break;

            case R.id.button_customers:

                break;

            case R.id.taskCardview:
                break;

            case R.id.amcCardView:
                mSelection = "AMC";
                getCRMAMCList();
                //imgNxt2.setImageDrawable(getResources().getDrawable(R.drawable.d_arrow));
                //mAmcRecyclerViewList.setVisibility(View.VISIBLE);
                //enquiryCustListLayout.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }


    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(CRMAMCkList.class.getSimpleName())) {
            Gson gson = new Gson();
            CRMAMCkList crmamCkList = gson.fromJson(urlConnectionResponse.resultData, CRMAMCkList.class);
            if (crmamCkList.getCode().equalsIgnoreCase("200") && crmamCkList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + crmamCkList.getMessage());
                if (crmamCkList != null) {
                    mCRMAmcList = new ArrayList<>();
                    mCRMAmcList = crmamCkList.getResultData();
                    if (mCRMAmcList.size() > 0) {
                        //txtAmcCount.setText(String.valueOf(mCRMAmcList.size()));
                        setData();
                    } else {
                        //Toast.makeText(getActivity(), "Record not available!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }

    private void setData() {
        // AMC
        if (mSelection.equalsIgnoreCase("AMC")) {
            if (mCRMAmcList != null) {
                mAmcRecyclerViewList.setVisibility(View.VISIBLE);
                noResultImg.setVisibility(View.GONE);
                crmamcListAdapter = new CRMAMCListAdapter(getContext(), mCRMAmcList, this);
                crmamcListAdapter.setClickListener(this);
                mAmcRecyclerViewList.setLayoutManager(mLinearLayoutManagerAmc);
                mAmcRecyclerViewList.setAdapter(crmamcListAdapter);
            }
        }
    }


    @Override
    public void onClick(View view, int position) {

        switch (view.getId()) {
            case R.id.enquiry_card_view:
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_DETAILS, bundle);

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.enquiryDetailsDialog(getActivity(), mEnquiryListAdapter.getItem(position));
                break;

            case R.id.imageView_call:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mCustomerListAdapter.getItem(position).getMobileNumber()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    try {
                        String number = mCustomerListAdapter.getItem(position).getMobileNumber();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }

    }

    @Override
    public void onUpdateClick(View view, int position) {

    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
    }

    public void clearTaskList() {

        try {
            int size = mCRMTaskList.size();
            mCRMTaskList.clear();
            crmtasksListAdapter.notifyItemRangeRemoved(0, size);

            // For loading data after selecting task type from spinner
            isLoadFirstTime = true;
            mCurrentPage = PAGE_START;
            isLoading = false;
            isLastPage = false;
            // mSearchParam = "";

            crmtasksListAdapter = new CRMTasksListAdapter(getActivity(), mCRMTaskList, this);
            mcrmRecyclerViewList.setAdapter(crmtasksListAdapter);
            crmtasksListAdapter.setClickListener(this);
            crmtasksListAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

}
