package com.corefield.fieldweb.FieldWeb.Dialogs;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.AddItemsAdapter;
import com.corefield.fieldweb.Adapter.AddItemsEditAdapter;
import com.corefield.fieldweb.Adapter.AddTechListAdapter;
import com.corefield.fieldweb.Adapter.CustomerAdapter;
import com.corefield.fieldweb.Adapter.ReassignAddItemsAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.AMC.AMCDetails;
import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.QuoteBindListDTO;
import com.corefield.fieldweb.DTO.Account.UpdateQuotationStatusDTO;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.DTO.Item.AddItem;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.OnHoldTaskDetails;
import com.corefield.fieldweb.DTO.Task.ReassignTask;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.Task.UpdateTask;
import com.corefield.fieldweb.DTO.TeleCMI.Connect;
import com.corefield.fieldweb.DTO.TeleCMI.Pcmo;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.DTO.User.AddBulkTech;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Accounts.QuotationDetailsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddTaskActivity;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.corefield.fieldweb.Util.WriteWaveFileHeader;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Response;

public class TaskDialogNew extends Activity implements AddItemsAdapter.MultipleMaterialPostListener, ReassignAddItemsAdapter.ReassignItemPostListener, AddItemsEditAdapter.MultipleMaterialPostListener {

    private String mName, mNumber;
    private static final int PICK_CONTACT_REQUEST = 1;
    String mTempLocName = "", mEditAudio = "";
    double mTempLat = 0, mTempLong = 0;
    Place mTempPlace = null;
    public final String TAG = TaskDialogNew.class.getSimpleName();

    private static TaskDialogNew mTaskDialog;
    public static ImageView mImageViewBeforeTask, mImageViewBeforeTask2, mImageViewBeforeTask3;
    public static ImageView photo1, photo2, photo3;
    CustomerList.ResultData customerObj = null;
    // VARIABLES FOR AUDIO REC : START
    ImageView recAudio, recAudioStop, recAudioPlay;
    ImageView editRecAudio, editRecAudioStop, editRecAudioPlay;
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private AudioRecord recorder = null;
    private int bufferSize = 0;
    private int quoteId = 0;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    Chronometer chronometer, editChronometer;
    CounterForChronometer chronometerTimer;
    CounterForEditChronometer editChronometerTimer;
    String audioData = "", recFilePath = "", audioFilePath = "";
    // VARIABLES FOR AUDIO REC : END
    Context mContext;
    AddItemsAdapter addItemsAdapter;
    AddItemsEditAdapter addItemsEditAdapter;
    ReassignAddItemsAdapter reassignAddItemsAdapter;
    AddItemsEditAdapter addItemRejectedAdpter;
    private LinearLayoutManager mLayoutManager;
    ArrayList<AddTask.MultipleItemAssigned> finalPostArray = new ArrayList<AddTask.MultipleItemAssigned>();
    ArrayList<TasksList.MultipleItemAssigned> reassignMultiItemList = new ArrayList<TasksList.MultipleItemAssigned>();
    private TasksList.ResultData mResultData;
    AutoCompleteTextView mAutoCompleteCustName;
    EditText mEditTextCustNumber;

    /**
     * The private constructor for the TaskDialog Singleton class
     */
    private TaskDialogNew() {
    }

    public static TaskDialogNew getInstance() {
        //instantiate a new TaskDialog if we didn't instantiate one yet
        if (mTaskDialog == null) {
            mTaskDialog = new TaskDialogNew();
        }
        return mTaskDialog;
    }

    public void addTaskDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, boolean onEnquiry, EnquiryList.ResultData enquiry, int techPosition, int amcServiceDetailsId, boolean isAMC, AMCDetails.ResultData AMCResultData, List<CustomerList.ResultData> customerList) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
//        Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
//        TextInputLayout textInputCustomerName = dialog.findViewById(R.id.textInput_customer_name);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputAddress = dialog.findViewById(R.id.textInput_task_address);
        TextInputLayout textInputAddressDesc = dialog.findViewById(R.id.textInput_address_desc);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);

        mContext = activity;

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);
        /*Spinner technicians = dialog.findViewById(R.id.spin_technicians);*/

        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner quotelist = (SearchableSpinner) dialog.findViewById(R.id.spin_quote);


        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button buttonSaveTask = dialog.findViewById(R.id.button_add);
        Button buttonCancelTask = dialog.findViewById(R.id.button_cancel);

        ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);

        mLayoutManager = new LinearLayoutManager(mContext);
        addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList/*addItemList*/, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView_Item_list.setLayoutManager(mLayoutManager);
        recyclerView_Item_list.setAdapter(addItemsAdapter);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        LinearLayout quote = dialog.findViewById(R.id.quote);

        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);

        chronometer = (Chronometer) dialog.findViewById(R.id.chronometer);
        recAudio = (ImageView) dialog.findViewById(R.id.recAudio);
        recAudioStop = (ImageView) dialog.findViewById(R.id.imageView_stop_audio);
        recAudioPlay = (ImageView) dialog.findViewById(R.id.imageView_play_audio);
        chronometerTimer = new CounterForChronometer(31000, 1000); // 30 SEC TIMER INITALIZATION FOR AUDIO REC
        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

//        recAudio.setEnabled(false);
//        recAudio.setAlpha(.5f);
        recAudioStop.setEnabled(false);
        recAudioStop.setAlpha(.5f);
        recAudioPlay.setEnabled(false);
        recAudioPlay.setAlpha(.5f);

        /*if(!TextUtils.isEmpty(editTextTaskTitle.getText().toString())
                && !TextUtils.isEmpty(editTextAddress.getText().toString())
                && !TextUtils.isEmpty(editTextLandmark.getText().toString())
                && technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
            recAudio.setEnabled(true);
            recAudio.setAlpha(1.0f);
        } else {
            recAudio.setEnabled(false);
            recAudio.setAlpha(.5f);
        }*/

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });

        howto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent send = new Intent(mContext.getApplicationContext(), YouTubeAddTaskActivity.class);
                mContext.startActivity(send);
            }
        });


        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerObj.getMobileNumber());
                editTextAddress.setText(customerObj.getAddress());
                editTextLandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        /*autoCompleteCustName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {

                } else {

                }
            }
        });*/

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        /////////////////////////////////////////Time picker on Add Task form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });


        ArrayList<String> mQuoteBindList = new ArrayList<>();
        List<QuoteBindListDTO.ResultData> listOfQutoeBind = ((HomeActivityNew) activity).mQuoteBindListResultData;

        for (QuoteBindListDTO.ResultData serviceResultList : listOfQutoeBind) {
            mQuoteBindList.add(serviceResultList.getQuoteName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mQuoteBindList);
        mQuoteBindList.clear();
        mQuoteBindList.addAll(set);

        ArrayAdapter<String> arrayAdapterQuoteBindList = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mQuoteBindList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }

        };
        arrayAdapterQuoteBindList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quotelist.setAdapter(arrayAdapterQuoteBindList);
        quotelist.setSelection(arrayAdapterQuoteBindList.getCount());
        quotelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfQutoeBind.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfQutoeBind.get(i).getQuoteName())) {
                        quoteId = listOfQutoeBind.get(i).getId();
                        editTextTaskTitle.setText(listOfQutoeBind.get(i).getQuoteName());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /* if (itemsNameList.size() == 0) {
//            spinnerItem.setVisibility(View.GONE);
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        itemsNameList.add(0, activity.getString(R.string.none));//Add this to select none
        itemsNameList.add(activity.getResources().getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /* userNameList.add(activity.getResources().getString(R.string.select_technicians));
        LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        userNameList.add(activity.getString(R.string.select_technicians));
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        if (techPosition != -1) {
            technicians.setSelection(techPosition);
            technicians.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //technicians.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        } else technicians.setSelection(arrayAdapterTech.getCount());

        //////////////////////////////AMC//////////////////////////////////
        if (isAMC) {
            if (AMCResultData.getAMCName() != null) {
                editTextTaskTitle.setText(AMCResultData.getAMCName());
            }
            if (AMCResultData.getProductDetail().getCustomerDetailInfoDto().getCustomerName() != null) {
                autoCompleteCustName.setText(AMCResultData.getProductDetail().getCustomerDetailInfoDto().getCustomerName());
                autoCompleteCustName.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                // autoCompleteCustName.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerDetailInfoDto().getMobileNumber() != null) {
                editTextCustNumber.setText("" + AMCResultData.getProductDetail().getCustomerDetailInfoDto().getMobileNumber());
                textInputCustomerNumber.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                //editTextCustNumber.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getAddress() != null) {
                editTextAddress.setText(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getAddress());
                textInputAddress.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                //editTextAddress.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getDescription() != null) {
                editTextLandmark.setText(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getDescription());
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getName() != null)
                mTempLocName = AMCResultData.getProductDetail().getCustomerLocationInfoDto().getName();

            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLatitude() != null)
                mTempLat = Double.parseDouble(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLatitude());
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLongitude() != null)
                mTempLong = Double.parseDouble(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLongitude());
        }
        ////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (onEnquiry) {
            if (enquiry != null) {
                //Verify all temp place
                //need to verify task type position (may mismatch with new task type list )
                //Need to verify the pre-fill date
                //Need to verify pre-fill the time
                if (enquiry.getServiceName() != null)
                    editTextTaskTitle.setText(enquiry.getServiceName());
                if (enquiry.getCustomerName() != null) {
                    autoCompleteCustName.setText(enquiry.getCustomerName());
                    autoCompleteCustName.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // autoCompleteCustName.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getMobileNumber() != null) {
                    editTextCustNumber.setText(enquiry.getMobileNumber());
                    textInputCustomerNumber.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextCustNumber.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getLocDescription() != null) {
                    editTextLandmark.setText(enquiry.getLocDescription());
                    textInputAddressDesc.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextLandmark.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getAddress() != null) {
                    editTextAddress.setText(enquiry.getAddress());
                    textInputAddress.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextAddress.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getTechnicalProblem() != null)
                    editTextTaskDesc.setText(enquiry.getTechnicalProblem());
                if (enquiry.getTaskTypeId() != 0) {
                    if (enquiry.getTaskTypeId() == 1)
                        segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
                    else if (enquiry.getTaskTypeId() == 2) {
                        segmentedGroupTaskType.check(R.id.radiobuttonToday);
                        textInputTimePicker.setEnabled(true);
                    } else {
                        segmentedGroupTaskType.check(R.id.radiobuttonOther);
                        textInputDatePicker.setEnabled(true);
                        textInputTimePicker.setEnabled(true);
                    }
                }
                if (enquiry.getLatitude() != null)
                    mTempLat = Double.parseDouble(enquiry.getLatitude());
                if (enquiry.getLongitude() != null)
                    mTempLong = Double.parseDouble(enquiry.getLongitude());
                if (enquiry.getLocName() != null) mTempLocName = enquiry.getLocName();
                if (enquiry.getPrefDate() != null) {
                    editTextDatePicker.setText(enquiry.getPrefDate());
                }
            }
        }

        //--------------To set default Urgent date time--------------
        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //----------------------------------------------------------

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*segmentedGroupTaskType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonUrgent:
                        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
                        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());

                        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        textInputDatePicker.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database

                        editTextTimePicker.setError(null);
                        editTextTimePicker.setText(currentTime);
                        textInputTimePicker.setEnabled(false);
                        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        break;
                    case R.id.radiobuttonToday:
                        //Set to default
                        DateUtils.currentDatePicker(editTextDatePicker);
                        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        textInputDatePicker.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database

                        editTextTimePicker.setError(null);
                        editTextTimePicker.setText("");
                        textInputTimePicker.setEnabled(true);
                        break;
                    case R.id.radiobuttonOther:
                        //All Default Selection
                        editTextDatePicker.setEnabled(true);//Don't allow to edit as this may results in duplicate record in the database
                        editTextDatePicker.setText("");
                        textInputDatePicker.setEnabled(true);
                        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));

                        editTextTimePicker.setText("");
                        textInputTimePicker.setEnabled(true);
                        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });*/


        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        if (isAMC) radioButtonRate.setClickable(false);

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
                        textInputWages.setEnabled(false);
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        break;
                    case R.id.radiobuttonRate:
                        editTextWages.setHint("");
                        textInputWages.setEnabled(true);
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        break;
                    default:
                        // Nothing to do
                }
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedTime = editTextTimePicker.getText().toString();

                if (isEmpty(editTextTaskTitle)) {
                    editTextTaskTitle.setError(activity.getString(R.string.please_enter_task_name));
                } else if (!isEmpty(editTextTaskTitle) && Character.isWhitespace(editTextTaskTitle.getText().toString().charAt(0)) || editTextTaskTitle.getText().toString().trim().isEmpty()) {
                    editTextTaskTitle.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(autoCompleteCustName) && (Character.isWhitespace(autoCompleteCustName.getText().toString().charAt(0)) || autoCompleteCustName.getText().toString().trim().isEmpty())) {
                    autoCompleteCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextAddress)) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextLandmark)) {
                    editTextLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextLandmark) && (Character.isWhitespace(editTextLandmark.getText().toString().charAt(0)) || editTextLandmark.getText().toString().trim().isEmpty())) {
                    editTextLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextTimePicker)) {
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_time));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else if (!isEmpty(editTextTaskDesc) && (Character.isWhitespace(editTextTaskDesc.getText().toString().charAt(0)) || editTextTaskDesc.getText().toString().trim().isEmpty())) {
                    editTextTaskDesc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (mTempLat == 0 || mTempLong == 0) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                    Toast.makeText(activity, activity.getResources().getString(R.string.please_enter_address_correctly), Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmpty(editTextCustNumber)) { //This will validate only if user enter the customer no
                        if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(editTextCustNumber)) {
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(editTextCustNumber)) { // NRI
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }

                       /* else if (isValidPhone(editTextCustNumber)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }*/
                    }
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {//This will validate only if user select rate
                        if (isEmpty(editTextWages)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_rate));
                            return;
                        } else if ((Integer.valueOf(editTextWages.getText().toString()) == 0)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_valid_amount));
                            return;
                        }
                    }
                    // COMMENTED BY MANISH : TO PASS MULTI ITEM ARRAY
                   /* if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() == 0) {
                            ((TextView) items.getSelectedView()).setError(activity.getString(R.string.quantity_is_0));
                            Toast.makeText(activity, activity.getString(R.string.quantity_is_0), Toast.LENGTH_SHORT).show();
                            arrayAdapterItem.notifyDataSetChanged();
                            return;
                        }
                        if (isEmpty(editTextItemQty)) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        } else if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    } else {
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    }*/

                    AddTask.ResultData addTask = new AddTask.ResultData();
                    UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    FWLogger.logInfo(TAG, "tasktype : " + tasktype);
                    if (onEnquiry) addTask.setId(enquiry.getEnquiryId());
                    else addTask.setId(0);

                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        addTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        addTask.setCustomerDetailsid(0);
                    }

                    addTask.setName(editTextTaskTitle.getText().toString());
                    addTask.setCustomerName(autoCompleteCustName.getText().toString());
                    addTask.setContactNo(editTextCustNumber.getText().toString());
                    addTask.setTaskStatus(4);
                    addTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    addTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    addTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + addTask.getTime());
                    addTask.setLocationId(0);
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        addTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        addTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        addTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        addTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        addTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }

                    addTask.setUserId(usersLists.getId());
                    // COMMENTED BY MANISH
                    /*if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                            addTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                        }
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                            addTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        }
                    }*/
                    addTask.setItemId(0);
                    //addTask.setItemQuantity(0);

                    addTask.setIsActive(true);
                    addTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    addTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) addTask.setLocName(mTempLocName);//short address
                    if (mTempLat != 0) addTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) addTask.setLongitude(String.valueOf(mTempLong));//long

                    if (editTextLandmark.getText().toString() != null)
                        addTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        addTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        addTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    addTask.setLocIsActive(true);
                    addTask.setIsSuccessful(true);
                    addTask.setIsModelError(true);
                    addTask.setDescription(editTextTaskDesc.getText().toString());
                    if (amcServiceDetailsId != 0)
                        addTask.setAMCServiceDetailsId(amcServiceDetailsId);

                    if (onEnquiry) addTask.setCustomerDetailsid(enquiry.getCustomerDetailsid());
                    try {
                        if (audioData == null || audioData.isEmpty()) {
                            audioData = null;
                            addTask.setAudioFilePath("");
//                            addTask.setBase64AudioString(audioData);
                        } else {
                            addTask.setAudioFilePath("");
                            addTask.setBase64AudioString(audioData);
                        }

                        try {
                            List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < finalPostArray.size(); i++) {
                                AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            addTask.setMultipleItemAssigned(itemAssignedList);

                            // DURING ADD TASK : 0
                            addTask.setOnHoldTaskId(0); // CFT : Fresh
                            //addTask.setQuotationId(quoteId);

                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        ((HomeActivityNew) activity).saveAddTask(addTask);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    // Clear file details of recorded audio
                    clearFileFromMem();

                    //arrayAdapterItem.remove(activity.getString(R.string.none));
                    //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }


            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });


        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //arrayAdapterItem.remove(activity.getString(R.string.none));
                //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                if (null != recorder) {
                    isRecording = false;
                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonCancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //arrayAdapterItem.remove(activity.getString(R.string.none));
                //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                if (place != null) {
                    editTextAddress.setError(null);
                    editTextLandmark.setError(null);
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        // AUDIO REC EVENT :
        recAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*AppLog.logString("Start Recording");
                enableButtons(true);*/
//                recAudioStop.setVisibility(View.VISIBLE);
                recAudioStop.setEnabled(true);
                recAudioStop.setAlpha(1.0f);
                recAudio.setEnabled(false);
                recAudio.setAlpha(.5f);
                recAudioPlay.setEnabled(false);
                recAudioPlay.setAlpha(.5f);
//                recAudio.setVisibility(View.GONE);
                chronometerTimer.start();
                chronometer.setFormat("Time (%s)");
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                startRecording();
            }
        });

        recAudioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                recAudio.setVisibility(View.VISIBLE);
//                recAudioStop.setVisibility(View.GONE);
                recAudio.setEnabled(true);
                recAudio.setAlpha(1.0f);
                recAudioStop.setEnabled(false);
                recAudioStop.setAlpha(.5f);
                recAudioPlay.setEnabled(true);
                recAudioPlay.setAlpha(1.0f);

                chronometerTimer.cancel();
                chronometer.stop();
                stopRecording();

            }
        });

        recAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FWLogger.logInfo(TAG, "getFilename() : " + audioFilePath);

                    /*if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {
                        if (audioFilePath != null && !audioFilePath.isEmpty()) {
                            Uri uri = Uri.parse(audioFilePath);
                            Intent viewMediaIntent = new Intent();
                            viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
                            viewMediaIntent.setDataAndType(uri, "audio/*");
                            viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            activity.startActivity(viewMediaIntent);
                        } else {
                            Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                    }*/

                    /*if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {*/
                    if (((HomeActivityNew) activity).AudioPermission()) {
                        File file = new File(audioFilePath);
                        if (file.exists()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                Uri contentUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.setDataAndType(contentUri, "audio/*");
                                activity.startActivity(intent);
                            } else {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "audio");
                                activity.startActivity(intent);
                            }
                        } else {
                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                            builder.setTitle("");
                            builder.setMessage("File does not exist");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            builder.create().show();
                        }
                    } else {
                        //((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                        ((HomeActivityNew) activity).AudioPermission();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        dialog.show();
    }

    public void getContactData(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        //Toast.makeText(mContext, mName + "/" + mNumber, Toast.LENGTH_SHORT).show();
        //if(!mName.isEmpty() && !mNumber.isEmpty()){
        mAutoCompleteCustName.setText(mName);
        mEditTextCustNumber.setText(mNumber);
        //}
    }

    private void clearFileFromMem() {
        audioFilePath = "";
        audioData = null;
        if (null != recorder) {
            isRecording = false;

            int i = recorder.getState();
            if (i == 1) recorder.stop();
            recorder.release();

            recorder = null;
            recordingThread = null;
        }
        deleteWavFilesFromPath();
    }

    private int getCurrentTimePlusOne(EditText datePicker) {
        int currentTimePlusOne = DateUtils.getCurrentHours() + 1;
        if (currentTimePlusOne == 12 && DateUtils.getCurrentMeridian() == 0) {
            DateUtils.currentDatePicker(datePicker);
        } else if (currentTimePlusOne == 12 && DateUtils.getCurrentMeridian() == 1) {
            DateUtils.currentDatePicker(datePicker);
        } else if (currentTimePlusOne > 12 && DateUtils.getCurrentMeridian() == 0) {
            currentTimePlusOne = 1; // Make 12+ as 1 PM
            DateUtils.currentDatePicker(datePicker);
        } else if (currentTimePlusOne > 12 && DateUtils.getCurrentMeridian() == 1) {
            currentTimePlusOne = 1;// Make 12+ as 1 AM
            DateUtils.nextDayDatePicker(datePicker);
            //May need to add next date
        } else {
            DateUtils.currentDatePicker(datePicker);
        }
        FWLogger.logInfo(TAG, "+1 Time : " + currentTimePlusOne);
        return currentTimePlusOne;
    }

    public void updateTaskDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, TasksList.ResultData mResultData, List<CustomerList.ResultData> customerList) {  //List<UsersList.ResultData> usersLists,
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputQuantity = dialog.findViewById(R.id.textInput_quantity);

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_task_item_quantity);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        TextView textViewDialogTitle = dialog.findViewById(R.id.textView_dialog_title);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        /*Spinner technicians = dialog.findViewById(R.id.spin_technicians);*/
        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner items = (SearchableSpinner) dialog.findViewById(R.id.spin_items);
        SearchableSpinner quotelist = (SearchableSpinner) dialog.findViewById(R.id.spin_quote);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button buttonUpdateTask = dialog.findViewById(R.id.button_add);
        Button buttonCancelTask = dialog.findViewById(R.id.button_cancel);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);
        howto.setText(R.string.hedittask);

        try {
            if (mResultData.getMultipleItemAssigned().size() > 0) {
                List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                itemAssigneds = mResultData.getMultipleItemAssigned();
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsEditAdapter = new AddItemsEditAdapter(this, itemAssigneds, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsEditAdapter);
            } else {
                ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
                AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
                multipleItemAssigned.setItemId(0);
                multipleItemAssigned.setItemQuantity(0);
                multipleItemAssigned.setItemName("");
                multipleItemAssignedArrayList.add(multipleItemAssigned);
                //
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsAdapter);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }
       /* ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);

        mLayoutManager = new LinearLayoutManager(mContext);
        addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList*//*addItemList*//*, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView_Item_list.setLayoutManager(mLayoutManager);
        recyclerView_Item_list.setAdapter(addItemsAdapter);*/

        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        LinearLayout quote = dialog.findViewById(R.id.quote);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });
        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddTaskActivity.class);
                activity.startActivity(send);
            }
        });

        // FOR UPDATE RECORDING :::
        editRecAudio = (ImageView) dialog.findViewById(R.id.recAudio);
        editRecAudioStop = (ImageView) dialog.findViewById(R.id.imageView_stop_audio);
        editRecAudioPlay = (ImageView) dialog.findViewById(R.id.imageView_play_audio);
        editChronometer = (Chronometer) dialog.findViewById(R.id.chronometer);
        editChronometerTimer = new CounterForEditChronometer(31000, 1000); // 30 SEC TIMER INITALIZATION FOR AUDIO REC
        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        /*RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);*/

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        editRecAudioStop.setEnabled(false);
        editRecAudioStop.setAlpha(.5f);


        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerList.get(position).getMobileNumber());
                editTextAddress.setText(customerList.get(position).getAddress());
                editTextLandmark.setText(customerList.get(position).getDescription());
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        /////////////////////////////////////////Time picker on Edit Task form/////////////////////////////////////////////////////////
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
//        editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //textInputDatePicker.setEnabled(false);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        buttonUpdateTask.setText(R.string.update);


        ArrayList<String> mQuoteBindList = new ArrayList<>();
        List<QuoteBindListDTO.ResultData> listOfQutoeBind = ((HomeActivityNew) activity).mQuoteBindListResultData;

        for (QuoteBindListDTO.ResultData serviceResultList : listOfQutoeBind) {
            mQuoteBindList.add(serviceResultList.getQuoteName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mQuoteBindList);
        mQuoteBindList.clear();
        mQuoteBindList.addAll(set);

        ArrayAdapter<String> arrayAdapterQuoteBindList = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mQuoteBindList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }

        };
        arrayAdapterQuoteBindList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quotelist.setAdapter(arrayAdapterQuoteBindList);
        quotelist.setSelection(arrayAdapterQuoteBindList.getCount());
        quotelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfQutoeBind.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfQutoeBind.get(i).getQuoteName())) {
                        quoteId = listOfQutoeBind.get(i).getId();
                        editTextTaskTitle.setText(listOfQutoeBind.get(i).getQuoteName());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (itemsNameList.size() == 0) {
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        itemsNameList.add(0, activity.getString(R.string.none));//Add this to select none
        itemsNameList.add(activity.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());
        if (mResultData.getItemId() != 0 && mResultData.getItemName() != null) {
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
        }
        items.setEnabled(false);
        items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        if (mResultData.getQuantity() != 0) editTextItemQty.setText("" + mResultData.getQuantity());
        else
            textInputQuantity.setHint(activity.getResources().getString(R.string.you_can_not_edit));

        editTextItemQty.setEnabled(false);
        textInputQuantity.setEnabled(false);
        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(activity.getString(R.string.select_technicians));
        /*// FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        if (mResultData.getUserId() != 0) {
            String assignedUserFirstName = null;
            for (int i = 0; i < ((HomeActivityNew) activity).mUsersLists.size(); i++) {
                if (mResultData.getUserId() == ((HomeActivityNew) activity).mUsersLists.get(i).getId()) {
                    assignedUserFirstName = ((HomeActivityNew) activity).mUsersLists.get(i).getFirstName();
                }
            }
            FWLogger.logInfo(TAG, "Tech A : " + assignedUserFirstName);
            if (assignedUserFirstName != null)
                technicians.setSelection(((ArrayAdapter<String>) technicians.getAdapter()).getPosition(assignedUserFirstName));
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
                        textInputWages.setEnabled(false);
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        break;
                    case R.id.radiobuttonRate:
                        textInputWages.setEnabled(true);
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        break;
                    default:
                        break;
                }
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        //////////////////////////////////////////Defaults /////////////////////////////////////////////////////////////////////////////
        textViewDialogTitle.setText(R.string.edit_task);
        if (mResultData.getName() != null) {
            editTextTaskTitle.setText(mResultData.getName());
        }
        if (mResultData.getCustomerName() != null) {
            autoCompleteCustName.setText(mResultData.getCustomerName());
        }
        if (mResultData.getContactNo() != null) {
            editTextCustNumber.setText(mResultData.getContactNo());
        }
        if (mResultData.getLocationDesc() != null) {
            editTextLandmark.setText(mResultData.getLocationDesc());
        }
        if (mResultData.getFullAddress() != null) {
            editTextAddress.setText(mResultData.getFullAddress());
        }
        if (mResultData.getDescription() != null)
            editTextTaskDesc.setText(mResultData.getDescription());
        if (mResultData.getTaskTypeId() != 0) {
            if (mResultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (mResultData.getTaskTypeId() == 2)
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
            else segmentedGroupTaskType.check(R.id.radiobuttonOther);
        }
        if (mResultData.getLatitude() != null)
            mTempLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mTempLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getLocationName() != null) mTempLocName = mResultData.getLocationName();
        FWLogger.logInfo(TAG, " " + mResultData.getTaskDate());
        if (mResultData.getAudioFilePath() != null && !mResultData.getAudioFilePath().isEmpty())
            audioFilePath = mResultData.getAudioFilePath();

        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
//                editTextWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setEnabled(false);
            //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            segmentedGroupRate.check(R.id.radiobuttonAMC);
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            segmentedGroupRate.check(R.id.radiobuttonRate);
            editTextWages.setText("" + mResultData.getWagesPerHours());
            textInputWages.setEnabled(true);
        }

        // FOR UPDATE RECORDING :::
        editRecAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditAudio = "EditREC";
                /*AppLog.logString("Start Recording");
                enableButtons(true);*/
//                recAudioStop.setVisibility(View.VISIBLE);
//                recAudio.setVisibility(View.GONE);
                editRecAudio.setEnabled(false);
                editRecAudio.setAlpha(.5f);
                editRecAudioStop.setEnabled(true);
                editRecAudioStop.setAlpha(1.0f);
                editRecAudioPlay.setEnabled(false);
                editRecAudioPlay.setAlpha(.5f);
                editChronometerTimer.start();
                editChronometer.setFormat("Time (%s)");
                editChronometer.setBase(SystemClock.elapsedRealtime());
                editChronometer.start();
                startRecording();
            }
        });

        editRecAudioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                recAudio.setVisibility(View.VISIBLE);
//                recAudioStop.setVisibility(View.GONE);
                editRecAudio.setEnabled(true);
                editRecAudio.setAlpha(1.0f);
                editRecAudioStop.setEnabled(false);
                editRecAudioStop.setAlpha(.5f);
                editRecAudioPlay.setEnabled(true);
                editRecAudioPlay.setAlpha(1.0f);
                editChronometerTimer.cancel();
                editChronometer.stop();
                stopRecording();
            }
        });

        editRecAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (audioFilePath != null && !audioFilePath.isEmpty()) {
                        FWLogger.logInfo(TAG, "getFilename() : " + audioFilePath);

                        if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {
                            if (audioFilePath != null && !audioFilePath.isEmpty()) {
                                if (mEditAudio.equalsIgnoreCase("EditREC")) {
                                    File file = new File(audioFilePath);
                                    if (file.exists()) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            Uri contentUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            intent.setDataAndType(contentUri, "audio/*");
                                            activity.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.setDataAndType(Uri.fromFile(file), "audio");
                                            activity.startActivity(intent);
                                        }
                                    } else {
                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                                        builder.setTitle("");
                                        builder.setMessage("File does not exist");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        });
                                        builder.create().show();
                                    }
                                } else {
                                    Uri uri = Uri.parse(audioFilePath);
                                    Intent viewMediaIntent = new Intent();
                                    viewMediaIntent.setAction(Intent.ACTION_VIEW);
                                    viewMediaIntent.setDataAndType(uri, "audio/*");
                                    viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    activity.startActivity(viewMediaIntent);
                                }
                            } else {
                                Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            ((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                        }
                    } else
                        Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonUpdateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = editTextTimePicker.getText().toString();
                mEditAudio = "";
                if (isEmpty(editTextTaskTitle)) {
                    editTextTaskTitle.setError(activity.getString(R.string.please_enter_task_name));
                } else if (!isEmpty(editTextTaskTitle) && Character.isWhitespace(editTextTaskTitle.getText().toString().charAt(0)) || editTextTaskTitle.getText().toString().trim().isEmpty()) {
                    editTextTaskTitle.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(autoCompleteCustName) && (Character.isWhitespace(autoCompleteCustName.getText().toString().charAt(0)) || autoCompleteCustName.getText().toString().trim().isEmpty())) {
                    autoCompleteCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextAddress)) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextLandmark)) {
                    if (!isEmpty(editTextLandmark) && (Character.isWhitespace(editTextLandmark.getText().toString().charAt(0)) || editTextLandmark.getText().toString().trim().isEmpty())) {
                        editTextLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                    } else {
                        editTextLandmark.setError(activity.getString(R.string.please_enter_landmark));
                    }
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else if (!isEmpty(editTextTaskDesc) && (Character.isWhitespace(editTextTaskDesc.getText().toString().charAt(0)) || editTextTaskDesc.getText().toString().trim().isEmpty())) {
                    editTextTaskDesc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    if (!isEmpty(editTextCustNumber)) { //This will validate only if user enter the customer no
                        if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(editTextCustNumber)) {
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(editTextCustNumber)) { //FOR NRI
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }

                        /*else if (isValidPhone(editTextCustNumber)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }*/
                    }
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {//This will validate only if user select rate
                        if (isEmpty(editTextWages)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_rate));
                            return;
                        } else if ((Integer.valueOf(editTextWages.getText().toString()) == 0)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_valid_amount));
                            return;
                        }
                    }
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() == 0) {
                            ((TextView) items.getSelectedView()).setError(activity.getString(R.string.quantity_is_0));
                            arrayAdapterItem.notifyDataSetChanged();
                            return;
                        }
                        if (isEmpty(editTextItemQty) && editTextItemQty.isEnabled() == true) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        } else if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    } else {
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    }

                    UpdateTask.ResultData updateTask = new UpdateTask.ResultData();
                    UsersList.ResultData usersLists = null;
                    //START :  BY MANISH
                    for (int i = 0; i < ((HomeActivityNew) activity).mUsersLists.size(); i++) {
                        if (((HomeActivityNew) activity).mUsersLists.get(i).getFirstName().equalsIgnoreCase(technicians.getSelectedItem().toString())) {
                            usersLists = ((HomeActivityNew) activity).mUsersLists.get(i);
                        }
                    }
                    // END :BELOW LIN COMMENTED BY MANISH FOR : REASSIGN TASK : UNABLE TO GET
                    //UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    FWLogger.logInfo(TAG, "tasktype : " + tasktype);
                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        updateTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        updateTask.setCustomerDetailsid(0);
                    }
                    updateTask.setId(mResultData.getId());
                    updateTask.setName(editTextTaskTitle.getText().toString());
                    updateTask.setCustomerName(autoCompleteCustName.getText().toString());
                    updateTask.setContactNo(editTextCustNumber.getText().toString());
                    updateTask.setTaskStatus(4);
                    updateTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    updateTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    updateTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + updateTask.getTime());
                    updateTask.setLocationId(0);
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        updateTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        updateTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        updateTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }
                    updateTask.setUserId(usersLists.getId());
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                            updateTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                        }
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                            updateTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        }
                    }
                    updateTask.setIsActive(true);
                    updateTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    updateTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) updateTask.setLocName(mTempLocName);//short address
                    if (mTempLat != 0) updateTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) updateTask.setLongitude(String.valueOf(mTempLong));//long
                    if (editTextLandmark.getText().toString() != null)
                        updateTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        updateTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        updateTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    updateTask.setLocIsActive(true);
                    updateTask.setIsSuccessful(true);
                    updateTask.setIsModelError(true);
                    updateTask.setDescription(editTextTaskDesc.getText().toString());

                    // UPDATE EXISTING AUDIO FILE BASE64 with Existing One
                    try {
                        if (audioData == null || audioData.isEmpty()) {
                            audioData = null;
                            updateTask.setAudioFilePath("");
//                            addTask.setBase64AudioString(audioData);
                        } else {
                            updateTask.setAudioFilePath("");
                            updateTask.setBase64AudioString(audioData);
                        }
                        // UPDATE MULTI ITEM DATA
                       /* try {
                            List<UpdateTask.MultipleItemAssigned> itemAssignedList = new ArrayList<UpdateTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < finalPostArray.size(); i++) {
                                UpdateTask.MultipleItemAssigned multipleItemAssignedpost = new UpdateTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            updateTask.setMultipleItemAssigned(itemAssignedList);

                        } catch (Exception e) {
                            e.getMessage();
                        }*/

                        try {
                            List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                            itemAssigneds = mResultData.getMultipleItemAssigned();

                            List<UpdateTask.MultipleItemAssigned> itemAssignedList = new ArrayList<UpdateTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < itemAssigneds.size(); i++) {
                                UpdateTask.MultipleItemAssigned multipleItemAssignedpost = new UpdateTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(itemAssigneds.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(itemAssigneds.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(itemAssigneds.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            updateTask.setMultipleItemAssigned(itemAssignedList);

                        } catch (Exception e) {
                            e.getMessage();
                        }

                        ((HomeActivityNew) activity).updateTask(updateTask);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    // Clear file details of recorded audio
                    clearFileFromMem();

                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditAudio = "";
                arrayAdapterItem.remove(activity.getString(R.string.none));
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                audioData = null;
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonCancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditAudio = "";
                arrayAdapterItem.remove(activity.getString(R.string.none));
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                audioData = null;
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                editTextAddress.setError(null);
                editTextLandmark.setError(null);
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }

/*
    public void reAssignTaskDialog(Activity
                                           activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, TasksList.ResultData
                                           mResultData,
                                   List<UsersList.ResultData> usersLists, List<CustomerList.ResultData> customerList) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        TextInputLayout textInputTaskTitle = dialog.findViewById(R.id.textInput_task_title);
        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
//        TextInputLayout textInputCustomerName = dialog.findViewById(R.id.textInput_customer_name);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputAddress = dialog.findViewById(R.id.textInput_task_address);
        TextInputLayout textInputAddressDesc = dialog.findViewById(R.id.textInput_address_desc);
        TextInputLayout textInputQuantity = dialog.findViewById(R.id.textInput_quantity);

        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_task_item_quantity);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        TextView textViewDialogTitle = dialog.findViewById(R.id.textView_dialog_title);

       */
/* Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        Spinner items = dialog.findViewById(R.id.spin_items);*//*

        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner items = (SearchableSpinner) dialog.findViewById(R.id.spin_items);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button saveTask = dialog.findViewById(R.id.button_add);
        Button cancelTask = dialog.findViewById(R.id.button_cancel);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);

        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);

        RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);

        // FOR EDIT : MULTI ITEM
        try {
            if (mResultData.getMultipleItemAssigned().size() > 0) {
                List<TasksList.MultipleItemAssigned> itemAssignedList = new ArrayList<TasksList.MultipleItemAssigned>();
                itemAssignedList = mResultData.getMultipleItemAssigned();
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemRejectedAdpter = new AddItemsEditAdapter(this, itemAssignedList, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemRejectedAdpter);
            } else {
                ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
                AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
                multipleItemAssigned.setItemId(0);
                multipleItemAssigned.setItemQuantity(0);
                multipleItemAssigned.setItemName("");
                multipleItemAssignedArrayList.add(multipleItemAssigned);

                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList*/
    /*addItemList*//*
, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        howto.setText(R.string.hreassigntask);
        saveTask.setText(R.string.re_assign);

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;

        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });

        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddTaskActivity.class);
                activity.startActivity(send);
            }
        });

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName()
                        + " Number : " + customerList.get(position).getMobileNumber()
                        + " ID : " + customerList.get(position).getCustomerDetailsid()
                        + " Description : " + customerList.get(position).getDescription());
                editTextCustNumber.setText(customerList.get(position).getMobileNumber());
                editTextAddress.setText(customerList.get(position).getAddress());
                editTextLandmark.setText(customerList.get(position).getDescription());
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //textInputDatePicker.setEnabled(false);

        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (itemsNameList.size() == 0) {
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        textInputQuantity.setEnabled(false);
        itemsNameList.add(0, activity.getString(R.string.none));
        itemsNameList.add(activity.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(activity.getString(R.string.select_technicians));
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);

                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        textInputWages.setEnabled(false);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        break;
                    case R.id.radiobuttonRate:
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        textInputWages.setEnabled(true);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                        break;
                    default:
                        break;
                }
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                */
    /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*//*

                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        //////////////////////////////////////////Defaults /////////////////////////////////////////////////////////////////////////////
        textViewDialogTitle.setText(activity.getString(R.string.re_assign));
        if (mResultData.getName() != null) {
            editTextTaskTitle.setText(mResultData.getName());
            editTextTaskTitle.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            textInputTaskTitle.setEnabled(false);
        }
        if (mResultData.getCustomerName() != null) {
            autoCompleteCustName.setText(mResultData.getCustomerName());
            autoCompleteCustName.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //autoCompleteCustName.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        }
        if (mResultData.getContactNo() != null) {
            editTextCustNumber.setText(mResultData.getContactNo());
            textInputCustomerNumber.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //editTextCustNumber.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        }
        if (mResultData.getLocationDesc() != null) {
            editTextLandmark.setText(mResultData.getLocationDesc());
            textInputAddressDesc.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //editTextLandmark.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        }
        if (mResultData.getFullAddress() != null) {
            editTextAddress.setText(mResultData.getFullAddress());
            textInputAddress.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //editTextAddress.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        }
        if (mResultData.getDescription() != null)
            editTextTaskDesc.setText(mResultData.getDescription());
        if (mResultData.getTaskTypeId() != 0) {
            if (mResultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (mResultData.getTaskTypeId() == 2)
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
            else
                segmentedGroupTaskType.check(R.id.radiobuttonOther);
        }
        if (mResultData.getLatitude() != null)
            mTempLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mTempLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getLocationName() != null) mTempLocName = mResultData.getLocationName();
        FWLogger.logInfo(TAG, " " + mResultData.getTaskDate());

        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
//            editTextWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            editTextWages.setText("" + mResultData.getWagesPerHours());
        }
        textInputWages.setEnabled(false);
        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        segmentedGroupRate.setClickable(false);
        radioButtonAMC.setEnabled(false);
        radioButtonRate.setEnabled(false);
        segmentedGroupRate.requestFocus();

        */
/*if (mResultData.getItemId() != 0) {
            //items.setSelection(mResultData.getTaskTypeId() - 1);
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
            items.setEnabled(false);
            items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            editTextItemQty.setText("" + mResultData.getQuantity());
            editTextItemQty.setEnabled(false);
            textInputQuantity.setEnabled(false);
            editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            //TODO Add item quantity and disable it
        }*//*

        if (mResultData.getItemId() != 0) {
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
            items.setEnabled(false);
            //items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            editTextItemQty.setText("" + mResultData.getQuantity());
            editTextItemQty.setEnabled(false);
            textInputQuantity.setEnabled(false);
            //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            //TODO Add item quantity and disable it
        } else {
            items.setEnabled(true);
            editTextItemQty.setEnabled(true);
            textInputQuantity.setEnabled(true);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        saveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = editTextTimePicker.getText().toString();

                if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else {

                    ReassignTask.ResultData resultDataReassignTask = new ReassignTask.ResultData();

                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime + "Date " + editTextDatePicker.getText().toString());
                    resultDataReassignTask.setId(mResultData.getId());
                    resultDataReassignTask.setTaskDate(editTextDatePicker.getText().toString());
                    resultDataReassignTask.setTime(selectedTime);
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    FWLogger.logInfo(TAG, "tasktype : " + tasktype);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    resultDataReassignTask.setTaskType(tasktype + 1);
                    resultDataReassignTask.setTaskStatus(TaskStatus.IN_ACTIVE);
                    int techUserID = 0;
                    // COMMENTED BY MANISH FOR  TECH SEQUENCE
                   */
/* for (UsersList.ResultData user : usersLists) {
                        if (user.getFirstName().equalsIgnoreCase(userNameList.get(technicians.getSelectedItemPosition()))) {
                            techUserID = user.getId();
                        }
                    }*//*

                    // ADDED BY MANISH
                    try {
                        for (int i = 0; i < userNameList.size(); i++) {
                            if (usersLists.get(i).getFirstName().equalsIgnoreCase(technicians.getSelectedItem().toString())) {
                                techUserID = usersLists.get(i).getId();
                            }
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }

                    if (mResultData.getItemId() == 0) {
                        if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item))
                                && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                            if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                                resultDataReassignTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                            }
                            if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                                resultDataReassignTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                            }
                        }
                    }

                    resultDataReassignTask.setUserId(techUserID);
                    resultDataReassignTask.setIsActive(true);

                    // POST MULTI ITEM :
                    try {
                        List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                        itemAssigneds = mResultData.getMultipleItemAssigned();

                        List<ReassignTask.MultipleItemAssigned> itemAssignedList = new ArrayList<ReassignTask.MultipleItemAssigned>();

                        for (int i = 0; i < itemAssigneds.size(); i++) {
                            ReassignTask.MultipleItemAssigned multipleItemAssignedpost = new ReassignTask.MultipleItemAssigned();
                            multipleItemAssignedpost.setItemId(itemAssigneds.get(i).getItemId());
                            multipleItemAssignedpost.setItemQuantity(itemAssigneds.get(i).getItemQuantity());
                            multipleItemAssignedpost.setItemName(itemAssigneds.get(i).getItemName());
                            itemAssignedList.add(multipleItemAssignedpost);
                        }
                        resultDataReassignTask.setMultipleItemAssigned(itemAssignedList);

                    } catch (Exception e) {
                        e.getMessage();
                    }

                    // DURING REASSIGN PASS ACTUAL TASK ID
                    */
    /*resultDataReassignTask.setOnHoldTaskId(mResultData.getId());*//*

                    //resultDataReassignTask.setOnHoldTaskId(0); // CFT : DEFAULT

                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    customerObj = null;
                    dialog.dismiss();

                    ((HomeActivityNew) activity).reAssignAsyncTask(resultDataReassignTask);
                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                editTextAddress.setError(null);
                editTextLandmark.setError(null);
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }
*/

    public void reAssignTaskDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, TasksList.ResultData mResultData, List<CustomerList.ResultData> customerList) {  //List<UsersList.ResultData> usersLists,
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputQuantity = dialog.findViewById(R.id.textInput_quantity);

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_task_item_quantity);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        TextView textViewDialogTitle = dialog.findViewById(R.id.textView_dialog_title);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

       /* Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        Spinner items = dialog.findViewById(R.id.spin_items);*/
        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner items = (SearchableSpinner) dialog.findViewById(R.id.spin_items);
        SearchableSpinner quotelist = (SearchableSpinner) dialog.findViewById(R.id.spin_quote);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button buttonUpdateTask = dialog.findViewById(R.id.button_add);
        Button buttonCancelTask = dialog.findViewById(R.id.button_cancel);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);
        howto.setText(R.string.hreassigntask);

        try {
            if (mResultData.getMultipleItemAssigned().size() > 0) {
                List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                itemAssigneds = mResultData.getMultipleItemAssigned();
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsEditAdapter = new AddItemsEditAdapter(this, itemAssigneds, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsEditAdapter);
            } else {
                ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
                AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
                multipleItemAssigned.setItemId(0);
                multipleItemAssigned.setItemQuantity(0);
                multipleItemAssigned.setItemName("");
                multipleItemAssignedArrayList.add(multipleItemAssigned);
                //
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsAdapter);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }
       /* ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);

        mLayoutManager = new LinearLayoutManager(mContext);
        addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList*//*addItemList*//*, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView_Item_list.setLayoutManager(mLayoutManager);
        recyclerView_Item_list.setAdapter(addItemsAdapter);*/

        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        LinearLayout quote = dialog.findViewById(R.id.quote);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });
        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddTaskActivity.class);
                activity.startActivity(send);
            }
        });

        // FOR UPDATE RECORDING :::
        editRecAudio = (ImageView) dialog.findViewById(R.id.recAudio);
        editRecAudioStop = (ImageView) dialog.findViewById(R.id.imageView_stop_audio);
        editRecAudioPlay = (ImageView) dialog.findViewById(R.id.imageView_play_audio);
        editChronometer = (Chronometer) dialog.findViewById(R.id.chronometer);
        editChronometerTimer = new CounterForEditChronometer(31000, 1000); // 30 SEC TIMER INITALIZATION FOR AUDIO REC
        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        /*RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);*/

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        editRecAudioStop.setEnabled(false);
        editRecAudioStop.setAlpha(.5f);


        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerList.get(position).getMobileNumber());
                editTextAddress.setText(customerList.get(position).getAddress());
                editTextLandmark.setText(customerList.get(position).getDescription());
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        /////////////////////////////////////////Time picker on Edit Task form/////////////////////////////////////////////////////////
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
//        editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //textInputDatePicker.setEnabled(false);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        buttonUpdateTask.setText(R.string.re_assign);

        ArrayList<String> mQuoteBindList = new ArrayList<>();
        List<QuoteBindListDTO.ResultData> listOfQutoeBind = ((HomeActivityNew) activity).mQuoteBindListResultData;

        for (QuoteBindListDTO.ResultData serviceResultList : listOfQutoeBind) {
            mQuoteBindList.add(serviceResultList.getQuoteName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mQuoteBindList);
        mQuoteBindList.clear();
        mQuoteBindList.addAll(set);

        ArrayAdapter<String> arrayAdapterQuoteBindList = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mQuoteBindList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }

        };
        arrayAdapterQuoteBindList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quotelist.setAdapter(arrayAdapterQuoteBindList);
        quotelist.setSelection(arrayAdapterQuoteBindList.getCount());
        quotelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfQutoeBind.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfQutoeBind.get(i).getQuoteName())) {
                        quoteId = listOfQutoeBind.get(i).getId();
                        editTextTaskTitle.setText(listOfQutoeBind.get(i).getQuoteName());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (itemsNameList.size() == 0) {
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        itemsNameList.add(0, activity.getString(R.string.none));//Add this to select none
        itemsNameList.add(activity.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());
        if (mResultData.getItemId() != 0 && mResultData.getItemName() != null) {
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
        }
        items.setEnabled(false);
        items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        if (mResultData.getQuantity() != 0) editTextItemQty.setText("" + mResultData.getQuantity());
        else
            textInputQuantity.setHint(activity.getResources().getString(R.string.you_can_not_edit));

        editTextItemQty.setEnabled(false);
        textInputQuantity.setEnabled(false);
        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(activity.getString(R.string.select_technicians));
        /*// FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        if (mResultData.getUserId() != 0) {
            String assignedUserFirstName = null;
            for (int i = 0; i < ((HomeActivityNew) activity).mUsersLists.size(); i++) {
                if (mResultData.getUserId() == ((HomeActivityNew) activity).mUsersLists.get(i).getId()) {
                    assignedUserFirstName = ((HomeActivityNew) activity).mUsersLists.get(i).getFirstName();
                }
            }
            FWLogger.logInfo(TAG, "Tech A : " + assignedUserFirstName);
            if (assignedUserFirstName != null)
                technicians.setSelection(((ArrayAdapter<String>) technicians.getAdapter()).getPosition(assignedUserFirstName));
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
                        textInputWages.setEnabled(false);
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        break;
                    case R.id.radiobuttonRate:
                        textInputWages.setEnabled(true);
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        break;
                    default:
                        break;
                }
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        //////////////////////////////////////////Defaults /////////////////////////////////////////////////////////////////////////////
        textViewDialogTitle.setText(R.string.re_assign);
        if (mResultData.getName() != null) {
            editTextTaskTitle.setText(mResultData.getName());
        }
        if (mResultData.getCustomerName() != null) {
            autoCompleteCustName.setText(mResultData.getCustomerName());
        }
        if (mResultData.getContactNo() != null) {
            editTextCustNumber.setText(mResultData.getContactNo());
        }
        if (mResultData.getLocationDesc() != null) {
            editTextLandmark.setText(mResultData.getLocationDesc());
        }
        if (mResultData.getFullAddress() != null) {
            editTextAddress.setText(mResultData.getFullAddress());
        }
        if (mResultData.getDescription() != null)
            editTextTaskDesc.setText(mResultData.getDescription());
        if (mResultData.getTaskTypeId() != 0) {
            if (mResultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (mResultData.getTaskTypeId() == 2)
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
            else segmentedGroupTaskType.check(R.id.radiobuttonOther);
        }
        if (mResultData.getLatitude() != null)
            mTempLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mTempLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getLocationName() != null) mTempLocName = mResultData.getLocationName();
        FWLogger.logInfo(TAG, " " + mResultData.getTaskDate());
        if (mResultData.getAudioFilePath() != null && !mResultData.getAudioFilePath().isEmpty())
            audioFilePath = mResultData.getAudioFilePath();

        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
//                editTextWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setEnabled(false);
            //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            segmentedGroupRate.check(R.id.radiobuttonAMC);
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            segmentedGroupRate.check(R.id.radiobuttonRate);
            editTextWages.setText("" + mResultData.getWagesPerHours());
            textInputWages.setEnabled(true);
        }

        // FOR UPDATE RECORDING :::
        editRecAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditAudio = "EditREC";
                /*AppLog.logString("Start Recording");
                enableButtons(true);*/
//                recAudioStop.setVisibility(View.VISIBLE);
//                recAudio.setVisibility(View.GONE);
                editRecAudio.setEnabled(false);
                editRecAudio.setAlpha(.5f);
                editRecAudioStop.setEnabled(true);
                editRecAudioStop.setAlpha(1.0f);
                editRecAudioPlay.setEnabled(false);
                editRecAudioPlay.setAlpha(.5f);
                editChronometerTimer.start();
                editChronometer.setFormat("Time (%s)");
                editChronometer.setBase(SystemClock.elapsedRealtime());
                editChronometer.start();
                startRecording();
            }
        });

        editRecAudioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                recAudio.setVisibility(View.VISIBLE);
//                recAudioStop.setVisibility(View.GONE);
                editRecAudio.setEnabled(true);
                editRecAudio.setAlpha(1.0f);
                editRecAudioStop.setEnabled(false);
                editRecAudioStop.setAlpha(.5f);
                editRecAudioPlay.setEnabled(true);
                editRecAudioPlay.setAlpha(1.0f);
                editChronometerTimer.cancel();
                editChronometer.stop();
                stopRecording();
            }
        });

        editRecAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (audioFilePath != null && !audioFilePath.isEmpty()) {
                        FWLogger.logInfo(TAG, "getFilename() : " + audioFilePath);

                        if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {
                            if (audioFilePath != null && !audioFilePath.isEmpty()) {
                                if (mEditAudio.equalsIgnoreCase("EditREC")) {
                                    File file = new File(audioFilePath);
                                    if (file.exists()) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            Uri contentUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            intent.setDataAndType(contentUri, "audio/*");
                                            activity.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.setDataAndType(Uri.fromFile(file), "audio");
                                            activity.startActivity(intent);
                                        }
                                    } else {
                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                                        builder.setTitle("");
                                        builder.setMessage("File does not exist");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        });
                                        builder.create().show();
                                    }
                                } else {
                                    Uri uri = Uri.parse(audioFilePath);
                                    Intent viewMediaIntent = new Intent();
                                    viewMediaIntent.setAction(Intent.ACTION_VIEW);
                                    viewMediaIntent.setDataAndType(uri, "audio/*");
                                    viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    activity.startActivity(viewMediaIntent);
                                }
                            } else {
                                Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            ((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                        }
                    } else
                        Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonUpdateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = editTextTimePicker.getText().toString();
                mEditAudio = "";
                if (isEmpty(editTextTaskTitle)) {
                    editTextTaskTitle.setError(activity.getString(R.string.please_enter_task_name));
                } else if (!isEmpty(editTextTaskTitle) && Character.isWhitespace(editTextTaskTitle.getText().toString().charAt(0)) || editTextTaskTitle.getText().toString().trim().isEmpty()) {
                    editTextTaskTitle.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(autoCompleteCustName) && (Character.isWhitespace(autoCompleteCustName.getText().toString().charAt(0)) || autoCompleteCustName.getText().toString().trim().isEmpty())) {
                    autoCompleteCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextAddress)) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextLandmark)) {
                    if (!isEmpty(editTextLandmark) && (Character.isWhitespace(editTextLandmark.getText().toString().charAt(0)) || editTextLandmark.getText().toString().trim().isEmpty())) {
                        editTextLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                    } else {
                        editTextLandmark.setError(activity.getString(R.string.please_enter_landmark));
                    }
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else if (!isEmpty(editTextTaskDesc) && (Character.isWhitespace(editTextTaskDesc.getText().toString().charAt(0)) || editTextTaskDesc.getText().toString().trim().isEmpty())) {
                    editTextTaskDesc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    if (!isEmpty(editTextCustNumber)) { //This will validate only if user enter the customer no
                        if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(editTextCustNumber)) {
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(editTextCustNumber)) { //FOR NRI
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }

                        /*else if (isValidPhone(editTextCustNumber)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }*/
                    }
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {//This will validate only if user select rate
                        if (isEmpty(editTextWages)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_rate));
                            return;
                        } else if ((Integer.valueOf(editTextWages.getText().toString()) == 0)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_valid_amount));
                            return;
                        }
                    }
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() == 0) {
                            ((TextView) items.getSelectedView()).setError(activity.getString(R.string.quantity_is_0));
                            arrayAdapterItem.notifyDataSetChanged();
                            return;
                        }
                        if (isEmpty(editTextItemQty) && editTextItemQty.isEnabled() == true) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        } else if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    } else {
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    }

                    UpdateTask.ResultData updateTask = new UpdateTask.ResultData();
                    UsersList.ResultData usersLists = null;
                    //START :  BY MANISH
                    for (int i = 0; i < ((HomeActivityNew) activity).mUsersLists.size(); i++) {
                        if (((HomeActivityNew) activity).mUsersLists.get(i).getFirstName().equalsIgnoreCase(technicians.getSelectedItem().toString())) {
                            usersLists = ((HomeActivityNew) activity).mUsersLists.get(i);
                        }
                    }
                    // END :BELOW LIN COMMENTED BY MANISH FOR : REASSIGN TASK : UNABLE TO GET
                    //UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    FWLogger.logInfo(TAG, "tasktype : " + tasktype);
                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        updateTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        updateTask.setCustomerDetailsid(0);
                    }
                    updateTask.setId(mResultData.getId());
                    updateTask.setName(editTextTaskTitle.getText().toString());
                    updateTask.setCustomerName(autoCompleteCustName.getText().toString());
                    updateTask.setContactNo(editTextCustNumber.getText().toString());
                    updateTask.setTaskStatus(4);
                    updateTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    updateTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    updateTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + updateTask.getTime());
                    updateTask.setLocationId(0);
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        updateTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        updateTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        updateTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }
                    updateTask.setUserId(usersLists.getId());
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                            updateTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                        }
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                            updateTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        }
                    }
                    updateTask.setIsActive(true);
                    updateTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    updateTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) updateTask.setLocName(mTempLocName);//short address
                    if (mTempLat != 0) updateTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) updateTask.setLongitude(String.valueOf(mTempLong));//long
                    if (editTextLandmark.getText().toString() != null)
                        updateTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        updateTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        updateTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    updateTask.setLocIsActive(true);
                    updateTask.setIsSuccessful(true);
                    updateTask.setIsModelError(true);
                    updateTask.setDescription(editTextTaskDesc.getText().toString());

                    // UPDATE EXISTING AUDIO FILE BASE64 with Existing One
                    try {
                        if (audioData == null || audioData.isEmpty()) {
                            audioData = null;
                            updateTask.setAudioFilePath("");
//                            addTask.setBase64AudioString(audioData);
                        } else {
                            updateTask.setAudioFilePath("");
                            updateTask.setBase64AudioString(audioData);
                        }
                        // UPDATE MULTI ITEM DATA
                       /* try {
                            List<UpdateTask.MultipleItemAssigned> itemAssignedList = new ArrayList<UpdateTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < finalPostArray.size(); i++) {
                                UpdateTask.MultipleItemAssigned multipleItemAssignedpost = new UpdateTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            updateTask.setMultipleItemAssigned(itemAssignedList);

                        } catch (Exception e) {
                            e.getMessage();
                        }*/

                        try {
                            List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                            itemAssigneds = mResultData.getMultipleItemAssigned();

                            List<UpdateTask.MultipleItemAssigned> itemAssignedList = new ArrayList<UpdateTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < itemAssigneds.size(); i++) {
                                UpdateTask.MultipleItemAssigned multipleItemAssignedpost = new UpdateTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(itemAssigneds.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(itemAssigneds.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(itemAssigneds.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            updateTask.setMultipleItemAssigned(itemAssignedList);

                        } catch (Exception e) {
                            e.getMessage();
                        }

                        ((HomeActivityNew) activity).updateTask(updateTask);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    // Clear file details of recorded audio
                    clearFileFromMem();

                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditAudio = "";
                arrayAdapterItem.remove(activity.getString(R.string.none));
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                audioData = null;
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonCancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditAudio = "";
                arrayAdapterItem.remove(activity.getString(R.string.none));
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                audioData = null;
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                editTextAddress.setError(null);
                editTextLandmark.setError(null);
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }

    public void reAssignCompletedTaskDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, TasksList.ResultData mResultData, List<UsersList.ResultData> usersLists, List<CustomerList.ResultData> customerList) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputQuantity = dialog.findViewById(R.id.textInput_quantity);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_task_item_quantity);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        TextView textViewDialogTitle = dialog.findViewById(R.id.textView_dialog_title);

        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner items = (SearchableSpinner) dialog.findViewById(R.id.spin_items);
        SearchableSpinner quotelist = (SearchableSpinner) dialog.findViewById(R.id.spin_quote);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button saveTask = dialog.findViewById(R.id.button_add);
        Button cancelTask = dialog.findViewById(R.id.button_cancel);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);

        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        LinearLayout quote = dialog.findViewById(R.id.quote);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);

        //
        ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);

        mLayoutManager = new LinearLayoutManager(mContext);
        addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList/*addItemList*/, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView_Item_list.setLayoutManager(mLayoutManager);
        recyclerView_Item_list.setAdapter(addItemsAdapter);

        howto.setText(R.string.hreassigntask);
        saveTask.setText(R.string.re_assign);

        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddTaskActivity.class);
                activity.startActivity(send);
            }
        });

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;

        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerList.get(position).getMobileNumber());
                editTextAddress.setText(customerList.get(position).getAddress());
                editTextLandmark.setText(customerList.get(position).getDescription());
            }
        });
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //textInputDatePicker.setEnabled(false);

        ArrayList<String> mQuoteBindList = new ArrayList<>();
        List<QuoteBindListDTO.ResultData> listOfQutoeBind = ((HomeActivityNew) activity).mQuoteBindListResultData;

        for (QuoteBindListDTO.ResultData serviceResultList : listOfQutoeBind) {
            mQuoteBindList.add(serviceResultList.getQuoteName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mQuoteBindList);
        mQuoteBindList.clear();
        mQuoteBindList.addAll(set);

        ArrayAdapter<String> arrayAdapterQuoteBindList = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mQuoteBindList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }

        };
        arrayAdapterQuoteBindList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quotelist.setAdapter(arrayAdapterQuoteBindList);
        quotelist.setSelection(arrayAdapterQuoteBindList.getCount());
        quotelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfQutoeBind.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfQutoeBind.get(i).getQuoteName())) {
                        quoteId = listOfQutoeBind.get(i).getId();
                        editTextTaskTitle.setText(listOfQutoeBind.get(i).getQuoteName());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (itemsNameList.size() == 0) {
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        itemsNameList.add(0, activity.getString(R.string.none));
        itemsNameList.add(activity.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(activity.getString(R.string.select_technicians));
       /* LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setEnabled(false);
            //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            segmentedGroupRate.check(R.id.radiobuttonAMC);
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            segmentedGroupRate.check(R.id.radiobuttonRate);
            editTextWages.setText("" + mResultData.getWagesPerHours());
            textInputWages.setEnabled(true);
        }

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
//                        editTextWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        textInputWages.setEnabled(false);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        break;
                    case R.id.radiobuttonRate:
//                        editTextWages.setHint(activity.getString(R.string.please_enter_amount));
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        textInputWages.setEnabled(true);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                        break;
                    default:
                        break;
                }
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        //////////////////////////////////////////Defaults /////////////////////////////////////////////////////////////////////////////
        textViewDialogTitle.setText(activity.getString(R.string.re_assign));

        if (mResultData.getName() != null) {
            editTextTaskTitle.setText(mResultData.getName());
        }
        if (mResultData.getCustomerName() != null) {
            autoCompleteCustName.setText(mResultData.getCustomerName());
        }
        if (mResultData.getContactNo() != null) {
            editTextCustNumber.setText(mResultData.getContactNo());
        }
        if (mResultData.getLocationDesc() != null) {
            editTextLandmark.setText(mResultData.getLocationDesc());
        }
        if (mResultData.getFullAddress() != null) {
            editTextAddress.setText(mResultData.getFullAddress());
        }
        if (mResultData.getDescription() != null)
            editTextTaskDesc.setText(mResultData.getDescription());

        if (mResultData.getTaskTypeId() != 0) {
            if (mResultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (mResultData.getTaskTypeId() == 2)
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
            else segmentedGroupTaskType.check(R.id.radiobuttonOther);
        }
        if (mResultData.getLatitude() != null)
            mTempLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mTempLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getLocationName() != null) mTempLocName = mResultData.getLocationName();
        FWLogger.logInfo(TAG, " " + mResultData.getTaskDate());
        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            editTextWages.setText("" + mResultData.getWagesPerHours());
        }

        textInputWages.setEnabled(false);
        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
//        segmentedGroupRate.setTintColor(R.color.light_gray);
        segmentedGroupRate.setEnabled(false);
        radioButtonAMC.setEnabled(false);
        radioButtonRate.setEnabled(false);
        segmentedGroupRate.requestFocus();

        if (mResultData.getItemId() != 0) {
            //items.setSelection(mResultData.getTaskTypeId() - 1);
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
            items.setEnabled(false);
            //items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            editTextItemQty.setText("" + mResultData.getQuantity());
            editTextItemQty.setEnabled(false);
            textInputQuantity.setEnabled(false);
            //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            //TODO Add item quantity and disable it
        } else {
            items.setEnabled(true);
            editTextItemQty.setEnabled(true);
            textInputQuantity.setEnabled(true);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        saveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = editTextTimePicker.getText().toString();

                FWLogger.logInfo(TAG, "CHECK Time : " + DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime));

                if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else {

                    AddTask.ResultData updateTask = new AddTask.ResultData();
                    UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        updateTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        updateTask.setCustomerDetailsid(0);
                    }
                    updateTask.setId(0);
                    updateTask.setName(editTextTaskTitle.getText().toString());
                    updateTask.setCustomerName(autoCompleteCustName.getText().toString());
                    updateTask.setContactNo(editTextCustNumber.getText().toString());
                    updateTask.setTaskStatus(4);
                    updateTask.setCustomerDetailsid(mResultData.getCustomerDetailsid());
                    updateTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    updateTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    updateTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + updateTask.getTime());
                    FWLogger.logInfo(TAG, "ITEMs SIZE : " + ((HomeActivityNew) activity).mItemsLists.size() + " position : " + mResultData.getItemId());
//                    FWLogger.logInfo(TAG, " Item Quantity : "+((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition()-1).getUnAssignedQuantity());

                    updateTask.setLocationId(updateTask.getLocationId());
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        updateTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        updateTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        updateTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }
                    updateTask.setUserId(usersLists.getId());
                    if (mResultData.getItemId() == 0) {
                        if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                            if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                                updateTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                            }
                            if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                                updateTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                            }
                        }
                    }
                    updateTask.setIsActive(true);
                    updateTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    updateTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) updateTask.setLocName(mTempLocName);//short address
                    if (mTempLat != 0) updateTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) updateTask.setLongitude(String.valueOf(mTempLong));//long
                    if (editTextLandmark.getText().toString() != null)
                        updateTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        updateTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        updateTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    updateTask.setLocIsActive(true);
                    updateTask.setIsSuccessful(true);
                    updateTask.setIsModelError(true);
                    updateTask.setDescription(editTextTaskDesc.getText().toString());

                    try {
                        List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();
                        //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                        for (int i = 0; i < finalPostArray.size(); i++) {
                            AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                            multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                            multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                            multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                            itemAssignedList.add(multipleItemAssignedpost);
                        }

                        updateTask.setMultipleItemAssigned(itemAssignedList);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    // DURING UPDATE TASK : 0
                    updateTask.setOnHoldTaskId(mResultData.getId()); // CFT : Completed

                    ((HomeActivityNew) activity).saveAddTask(updateTask);
                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                editTextAddress.setError(null);
                editTextLandmark.setError(null);
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }


    public void reAssignOnholdTaskDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, TasksList.ResultData mResultData, List<UsersList.ResultData> usersLists, List<CustomerList.ResultData> customerList) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputQuantity = dialog.findViewById(R.id.textInput_quantity);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_task_item_quantity);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        TextView textViewDialogTitle = dialog.findViewById(R.id.textView_dialog_title);

        SearchableSpinner technicians = dialog.findViewById(R.id.spin_technicians);
        SearchableSpinner items = dialog.findViewById(R.id.spin_items);
        SearchableSpinner quotelist = (SearchableSpinner) dialog.findViewById(R.id.spin_quote);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button saveTask = dialog.findViewById(R.id.button_add);
        Button cancelTask = dialog.findViewById(R.id.button_cancel);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);

        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        LinearLayout quote = dialog.findViewById(R.id.quote);

        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);

        //
        try {
            if (mResultData.getMultipleItemAssigned().size() > 0) {
                List<TasksList.MultipleItemAssigned> itemAssignedList = new ArrayList<TasksList.MultipleItemAssigned>();
                itemAssignedList = mResultData.getMultipleItemAssigned();
                mLayoutManager = new LinearLayoutManager(mContext);
                addItemRejectedAdpter = new AddItemsEditAdapter(this, itemAssignedList, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemRejectedAdpter);
            } else {
                ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
                AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
                multipleItemAssigned.setItemId(0);
                multipleItemAssigned.setItemQuantity(0);
                multipleItemAssigned.setItemName("");
                multipleItemAssignedArrayList.add(multipleItemAssigned);

                mLayoutManager = new LinearLayoutManager(mContext);
                addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList/*addItemList*/, mContext, itemsNameList);
                mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView_Item_list.setLayoutManager(mLayoutManager);
                recyclerView_Item_list.setAdapter(addItemsAdapter);
            }

        } catch (Exception ex) {
            ex.getMessage();
        }


        howto.setText(R.string.hreassigntask);
        saveTask.setText(R.string.re_assign);

        howto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddTaskActivity.class);
                activity.startActivity(send);
            }
        });

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;

        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerList.get(position).getMobileNumber());
                editTextAddress.setText(customerList.get(position).getAddress());
                editTextLandmark.setText(customerList.get(position).getDescription());
            }
        });
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //textInputDatePicker.setEnabled(false);

        ArrayList<String> mQuoteBindList = new ArrayList<>();
        List<QuoteBindListDTO.ResultData> listOfQutoeBind = ((HomeActivityNew) activity).mQuoteBindListResultData;

        for (QuoteBindListDTO.ResultData serviceResultList : listOfQutoeBind) {
            mQuoteBindList.add(serviceResultList.getQuoteName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mQuoteBindList);
        mQuoteBindList.clear();
        mQuoteBindList.addAll(set);

        ArrayAdapter<String> arrayAdapterQuoteBindList = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mQuoteBindList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }

        };
        arrayAdapterQuoteBindList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quotelist.setAdapter(arrayAdapterQuoteBindList);
        quotelist.setSelection(arrayAdapterQuoteBindList.getCount());
        quotelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfQutoeBind.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfQutoeBind.get(i).getQuoteName())) {
                        quoteId = listOfQutoeBind.get(i).getId();
                        editTextTaskTitle.setText(listOfQutoeBind.get(i).getQuoteName());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (itemsNameList.size() == 0) {
            items.setVisibility(View.GONE);
            editTextItemQty.setVisibility(View.GONE);
        }
        itemsNameList.add(0, activity.getString(R.string.none));
        itemsNameList.add(activity.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());

        items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    editTextItemQty.setText("");
                    textInputQuantity.setEnabled(false);
                    editTextItemQty.setEnabled(false);
                    //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                } else {
                    if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                    } else {
                        textInputQuantity.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(activity.getString(R.string.select_technicians));
       /* LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        quote.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
            textInputWages.setEnabled(false);
            //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            segmentedGroupRate.check(R.id.radiobuttonAMC);
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            segmentedGroupRate.check(R.id.radiobuttonRate);
            editTextWages.setText("" + mResultData.getWagesPerHours());
            textInputWages.setEnabled(true);
        }

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
//                        editTextWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        textInputWages.setEnabled(false);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                        break;
                    case R.id.radiobuttonRate:
//                        editTextWages.setHint(activity.getString(R.string.please_enter_amount));
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        textInputWages.setEnabled(true);
                        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_dialog));
                        break;
                    default:
                        break;
                }
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        //////////////////////////////////////////Defaults /////////////////////////////////////////////////////////////////////////////
        textViewDialogTitle.setText(activity.getString(R.string.re_assign));

        if (mResultData.getName() != null) {
            editTextTaskTitle.setText(mResultData.getName());
        }
        if (mResultData.getCustomerName() != null) {
            autoCompleteCustName.setText(mResultData.getCustomerName());
        }
        if (mResultData.getContactNo() != null) {
            editTextCustNumber.setText(mResultData.getContactNo());
        }
        if (mResultData.getLocationDesc() != null) {
            editTextLandmark.setText(mResultData.getLocationDesc());
        }
        if (mResultData.getFullAddress() != null) {
            editTextAddress.setText(mResultData.getFullAddress());
        }
        if (mResultData.getDescription() != null)
            editTextTaskDesc.setText(mResultData.getDescription());

        if (mResultData.getTaskTypeId() != 0) {
            if (mResultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (mResultData.getTaskTypeId() == 2)
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
            else segmentedGroupTaskType.check(R.id.radiobuttonOther);
        }
        if (mResultData.getLatitude() != null)
            mTempLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mTempLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getLocationName() != null) mTempLocName = mResultData.getLocationName();
        FWLogger.logInfo(TAG, " " + mResultData.getTaskDate());
        if (mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
            editTextWages.setText("");
            textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
        } else if (mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
            editTextWages.setText("" + mResultData.getWagesPerHours());
        }

        textInputWages.setEnabled(false);
        //editTextWages.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
//        segmentedGroupRate.setTintColor(R.color.light_gray);
        segmentedGroupRate.setEnabled(false);
        radioButtonAMC.setEnabled(false);
        radioButtonRate.setEnabled(false);
        segmentedGroupRate.requestFocus();

        if (mResultData.getItemId() != 0) {
            //items.setSelection(mResultData.getTaskTypeId() - 1);
            items.setSelection(((ArrayAdapter<String>) items.getAdapter()).getPosition(mResultData.getItemName()));
            items.setEnabled(false);
            //items.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            editTextItemQty.setText("" + mResultData.getQuantity());
            editTextItemQty.setEnabled(false);
            textInputQuantity.setEnabled(false);
            //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            //TODO Add item quantity and disable it
        } else {
            items.setEnabled(true);
            editTextItemQty.setEnabled(true);
            textInputQuantity.setEnabled(true);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        saveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = editTextTimePicker.getText().toString();

                FWLogger.logInfo(TAG, "CHECK Time : " + DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime));

                if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else {

                    AddTask.ResultData updateTask = new AddTask.ResultData();
                    UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        updateTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        updateTask.setCustomerDetailsid(0);
                    }
                    updateTask.setId(0);
                    updateTask.setName(editTextTaskTitle.getText().toString());
                    updateTask.setCustomerName(autoCompleteCustName.getText().toString());
                    updateTask.setContactNo(editTextCustNumber.getText().toString());
                    updateTask.setTaskStatus(4);
                    updateTask.setCustomerDetailsid(mResultData.getCustomerDetailsid());
                    updateTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    updateTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    updateTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + updateTask.getTime());
                    FWLogger.logInfo(TAG, "ITEMs SIZE : " + ((HomeActivityNew) activity).mItemsLists.size() + " position : " + mResultData.getItemId());
//                    FWLogger.logInfo(TAG, " Item Quantity : "+((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition()-1).getUnAssignedQuantity());

                    updateTask.setLocationId(updateTask.getLocationId());
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        updateTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        updateTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        updateTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        updateTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }
                    updateTask.setUserId(usersLists.getId());
                    if (mResultData.getItemId() == 0) {
                        if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                            if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                                updateTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                            }
                            if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                                updateTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                            }
                        }
                    }
                    updateTask.setIsActive(true);
                    updateTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    updateTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) updateTask.setLocName(mTempLocName);//short address
                    if (mTempLat != 0) updateTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) updateTask.setLongitude(String.valueOf(mTempLong));//long
                    if (editTextLandmark.getText().toString() != null)
                        updateTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        updateTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        updateTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    updateTask.setLocIsActive(true);
                    updateTask.setIsSuccessful(true);
                    updateTask.setIsModelError(true);
                    updateTask.setDescription(editTextTaskDesc.getText().toString());

                   /* try {
                        List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();
                        //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                        for (int i = 0; i < finalPostArray.size(); i++) {
                            AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                            multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                            multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                            multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                            itemAssignedList.add(multipleItemAssignedpost);
                        }

                        updateTask.setMultipleItemAssigned(itemAssignedList);
                    } catch (Exception e) {
                        e.getMessage();
                    }*/
                    // POST MULTI ITEM :
                    try {
                        List<TasksList.MultipleItemAssigned> itemAssigneds = new ArrayList<TasksList.MultipleItemAssigned>();
                        itemAssigneds = mResultData.getMultipleItemAssigned();
                        if (itemAssigneds.size() > 0) {
                            List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();

                            for (int i = 0; i < itemAssigneds.size(); i++) {
                                AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(itemAssigneds.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(itemAssigneds.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(itemAssigneds.get(i).getItemName());
                                itemAssignedList.add(multipleItemAssignedpost);
                            }
                            updateTask.setMultipleItemAssigned(itemAssignedList);
                        } else {
                            List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();

                            for (int i = 0; i < finalPostArray.size(); i++) {
                                AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());
                                itemAssignedList.add(multipleItemAssignedpost);
                            }
                            updateTask.setMultipleItemAssigned(itemAssignedList);
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }

                    // DURING UPDATE TASK : 0
                    updateTask.setOnHoldTaskId(mResultData.getId()); // CFT : Completed

                    ((HomeActivityNew) activity).saveAddTask(updateTask);
                    arrayAdapterItem.remove(activity.getString(R.string.none));
                    arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        cancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                arrayAdapterItem.remove(activity.getString(R.string.none));
                customerObj = null;
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                editTextAddress.setError(null);
                editTextLandmark.setError(null);
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }


    public void acceptTaskDialog(Activity activity, TasksList.ResultData data, int taskStatus) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_accept_task);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView textViewTaskName, textViewDescription, textViewCustName, textViewCustAddress, textViewLandmark, textViewDistance, textViewTime, textViewAcceptTitle;
        ImageView imageViewCall, imageViewAMC, imageViewCancel;
        Button buttonAccept, buttonReject;
        TextInputLayout rejeReasonLayout;
        EditText edtRejectNote;

        textViewTaskName = dialog.findViewById(R.id.textView_task_name);
        textViewDescription = dialog.findViewById(R.id.textView_description);
        textViewCustName = dialog.findViewById(R.id.textView_cust_name);
        textViewCustAddress = dialog.findViewById(R.id.textView_cust_address);
        textViewLandmark = dialog.findViewById(R.id.textView_landmark);
        rejeReasonLayout = dialog.findViewById(R.id.rejeReasonLayout);
        edtRejectNote = dialog.findViewById(R.id.edtRejectNote);

//        textViewDistance = dialog.findViewById(R.id.textView_distance);
//        textViewTime = dialog.findViewById(R.id.textView_time);
        textViewAcceptTitle = dialog.findViewById(R.id.textView_accept_title);

        imageViewCall = dialog.findViewById(R.id.imageView_calll);
        imageViewAMC = dialog.findViewById(R.id.imageView_amc);
        imageViewCancel = dialog.findViewById(R.id.imageView_cancel);

        try {
            LatLng mCoordinateOrigin = new LatLng(SharedPrefManager.getInstance(activity.getApplicationContext()).getUserLocation().getLatitude(), SharedPrefManager.getInstance(activity.getApplicationContext()).getUserLocation().getLongitude());
            LatLng mCoordinateDestination = new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()));

            MapUtils.getPathBetweenTwoLocation(activity, mCoordinateOrigin, mCoordinateDestination);
        } catch (Exception ex) {
            ex.getMessage();
        }

        /*LatLng sourceLatLng = new LatLng(Double.parseDouble(data.getTechLatitude()), Double.parseDouble(data.getTechLongitude()));
        LatLng destinationLatLng = new LatLng(Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()));

        double distance = SphericalUtil.computeDistanceBetween(sourceLatLng, destinationLatLng);
        FWLogger.logInfo(TAG, "Distance : "+distance);*/

        /*int distanceInKm = 0;

        if(data.getTechLatitude() == null && data.getTechLongitude() == null) {
            if (SharedPrefManager.getInstance(activity).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                double latitude = SharedPrefManager.getInstance(activity).getUserLocation().getLatitude();
                double longitude = SharedPrefManager.getInstance(activity).getUserLocation().getLongitude();
                distanceInKm = MapUtils.calculateDistanceInKilometer(latitude, longitude,
                        Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()));
            }
        } else {
            distanceInKm = MapUtils.calculateDistanceInKilometer(Double.parseDouble(data.getTechLatitude()), Double.parseDouble(data.getTechLongitude()),
                    Double.parseDouble(data.getLatitude()), Double.parseDouble(data.getLongitude()));
        }
        FWLogger.logInfo(TAG, "Distance In KM : " + distanceInKm);*/

        /*int speed=40;
        float time = distanceInKm/speed;
        FWLogger.logInfo(TAG, "Time Required : "+time);*/

//        textViewDistance.setText(distanceInKm + " km");
        if (data.getPaymentMode() != null) {
            if (data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING))
                imageViewAMC.setVisibility(View.VISIBLE);
            else imageViewAMC.setVisibility(View.INVISIBLE);
        }

        if (data.getLocationDesc() != null && !data.getLocationDesc().isEmpty())
            textViewLandmark.setText(data.getLocationDesc());
        else textViewLandmark.setText("");

        buttonAccept = dialog.findViewById(R.id.button_accept);
        buttonReject = dialog.findViewById(R.id.button_reject);

        textViewTaskName.setText(data.getName());

        if (taskStatus == TaskStatus.IN_ACTIVE) {
            textViewAcceptTitle.setText(activity.getResources().getString(R.string.accept_task));
            buttonAccept.setText(activity.getResources().getString(R.string.accept));
        } else if (taskStatus == TaskStatus.ON_GOING) {
            textViewAcceptTitle.setText(activity.getResources().getString(R.string.cont_task));
            buttonAccept.setText(activity.getResources().getString(R.string.continue_task));
        }

        if (data.getDescription() != null && !data.getDescription().isEmpty()) {
            textViewDescription.setVisibility(View.VISIBLE);
            textViewDescription.setText(data.getDescription());
        } else {
            textViewDescription.setVisibility(View.GONE);
        }
        if (data.getCustomerName() != null && !data.getCustomerName().isEmpty())
            textViewCustName.setText(data.getCustomerName());
        else textViewCustName.setText(activity.getString(R.string.na));

        textViewCustAddress.setText(data.getFullAddress());

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (SharedPrefManager.getInstance(activity).getTeleCMIModuleFlag().equalsIgnoreCase("true") || SharedPrefManager.getInstance(activity).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                        TelCMI(activity, data.getContactNo());
                    } else {
                        if (data.getContactNo() != null && !data.getContactNo().isEmpty()) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + data.getContactNo()));
                            activity.startActivity(intent);
                        } else {
                            Toast.makeText(activity, activity.getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.getMessage();
                }


            }
        });

        imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.getTaskStatusId() == TaskStatus.IN_ACTIVE && (data.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name()))) {
                    //this task is inactive then show dialog and then change the tab
                    int validity = isTaskDateValid(data);
                    FWLogger.logInfo(TAG, "validity : " + validity);
                    if (validity == 999) {
                        Toast.makeText(activity, R.string.date_format_mismatched, Toast.LENGTH_SHORT).show();
                    } else if (validity == 0) {//currentDate is equal to TaskDate
                        if (verifyGPSAndInternetAndPermission(activity)) {
//                            TaskDialog taskDialog = TaskDialog.getInstance();
//                            taskDialog.taskConfirmationDialog(this, ((MainTaskFragmentNew) fragment).mTakName);
                            ((HomeActivityNew) activity).mTaskListData = data;
                            ((HomeActivityNew) activity).updateTaskStatus(TaskStatus.ON_GOING, TaskState.NOT_STARTED, "");
                            dialog.dismiss();
                        }
                    } else if (validity == -1) {//currentDate is before TaskDate
                        Toast.makeText(activity, R.string.you_cant_accept_task_before_task_date, Toast.LENGTH_SHORT).show();
                    } /*else if (validity == 1) {//currentDate is after TaskDate
                        Toast.makeText(activity, R.string.task_expired, Toast.LENGTH_SHORT).show();
                    } */ /*else {
                        Toast.makeText(activity, R.string.something_went_wrong + "...!", Toast.LENGTH_SHORT).show();
                    }*/
                    // START
                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String strCurDate = df.format(c);
                    String strTaskDate[] = data.getTaskDate().split("T");
                    try {
                        Date date1; // Task Date
                        Date date2; // Current Date
                        SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
                        date1 = dates.parse(strTaskDate[0]);
                        date2 = dates.parse(strCurDate);
                        long difference = Math.abs(date1.getTime() - date2.getTime());
                        //long differenceDates = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
                        long differenceDates = difference / (24 * 60 * 60 * 1000);
                        //String dayDifference = Long.toString(differenceDates);

                        if (differenceDates > 3 && date1.compareTo(date2) < 0) { // date 1 occurs after date 2
                            Toast.makeText(activity, "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                        } else if (date1.compareTo(date2) > 0) { // FUTURE DATE
                            Toast.makeText(activity, "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                        } else if (differenceDates == 0 || differenceDates == 1 || differenceDates == 2 || differenceDates == 3 && date1.compareTo(date2) <= 0 && differenceDates <= 3) { //FOR PAST DATE AND CURRENT DATE ACCEPT:: IF task date is before than current
                            ((HomeActivityNew) activity).mTaskListData = data;
                            ((HomeActivityNew) activity).updateTaskStatus(TaskStatus.ON_GOING, TaskState.NOT_STARTED, "");
                            dialog.dismiss();
                        } else if (differenceDates < 3 && date1.compareTo(date2) < 0) {
                            Toast.makeText(activity, "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                        } else if (differenceDates >= 1) {
                            Toast.makeText(activity, "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                        } else if (differenceDates <= 1) {
                            Toast.makeText(activity, "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //END

                    /*else{
                        Toast.makeText(activity, R.string.something_went_wrong + "...!", Toast.LENGTH_SHORT).show();
                    }*/
                } /*else {
                    ((HomeActivityNew) activity).navigateToTaskNavigation(data);
                }*/

                dialog.dismiss();
            }
        });

        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rejeReasonLayout.setVisibility(View.VISIBLE);
                if (edtRejectNote.getText().toString().isEmpty()) {
                    edtRejectNote.setError(activity.getString(R.string.enter_reject_reason));
                } else {
                    ((HomeActivityNew) activity).mTaskListData = data;
                    ((HomeActivityNew) activity).updateTaskStatus(TaskStatus.REJECTED, TaskState.NOT_STARTED, edtRejectNote.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    public void captureBeforeTaskPhotoDialog(Activity activity, TasksList.ResultData data, Fragment fragment) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_before_task_photo);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonUpload, buttonSkip;
        buttonUpload = dialog.findViewById(R.id.button_upload);
        buttonSkip = dialog.findViewById(R.id.button_skip);
        EditText editTextNote = dialog.findViewById(R.id.editText_note);
        mImageViewBeforeTask = dialog.findViewById(R.id.imageView_before_task);
        mImageViewBeforeTask2 = dialog.findViewById(R.id.imageView_before_task2);
        mImageViewBeforeTask3 = dialog.findViewById(R.id.imageView_before_task3);

        mImageViewBeforeTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CountdownTechFragmentNew) fragment).pickImage(CountdownTechFragmentNew.mBeforeTaskImage, 1, null);
            }
        });

        mImageViewBeforeTask2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CountdownTechFragmentNew) fragment).pickImage(CountdownTechFragmentNew.mBeforeTaskImage2, 2, null);
            }
        });

        mImageViewBeforeTask3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CountdownTechFragmentNew) fragment).pickImage(CountdownTechFragmentNew.mBeforeTaskImage3, 3, null);
            }
        });

        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if (CountdownTechFragmentNew.mEncodedBaseString != null && !CountdownTechFragmentNew.mEncodedBaseString.isEmpty() && CountdownTechFragmentNew.mEncodedBaseString4 != null && !CountdownTechFragmentNew.mEncodedBaseString4.isEmpty() && CountdownTechFragmentNew.mEncodedBaseString5 != null && !CountdownTechFragmentNew.mEncodedBaseString5.isEmpty()) {
                ((CountdownTechFragmentNew) fragment).addPhotoBeforeTask(editTextNote.getText().toString());
                ((CountdownTechFragmentNew) fragment).updatestatusfromTaskDialog();
                ((CountdownTechFragmentNew) fragment).startTimer();
                dialog.dismiss();
                /*} else {
                    Toast.makeText(activity, "Please Add/Capture Image", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((HomeActivityNew) activity).mTaskListData = data;
//                ((HomeActivityNew) activity).updateTaskStatus(TaskStatus.REJECTED, TaskState.NOT_STARTED);
                CountdownTechFragmentNew.mEncodedBaseString = null;
                ((CountdownTechFragmentNew) fragment).updatestatusfromTaskDialog();
                ((CountdownTechFragmentNew) fragment).startTimer();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onHoldDialog(Activity activity, Fragment fragment, TasksList.ResultData mTaskListResultData) {

        ///////////////////////////////////////Behaviour of Dialog/////////////////////////////////////////////////////////
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_on_hold);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        //Button
        Button buttonOnhold = dialog.findViewById(R.id.button_onhold);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        //ImageView
        photo1 = dialog.findViewById(R.id.imageview_photo1);
        photo2 = dialog.findViewById(R.id.imageview_photo2);
        photo3 = dialog.findViewById(R.id.imageview_photo3);

        //TextInputLayout
        //TextInputLayout onHoldMaterial = dialog.findViewById(R.id.textInput_onhold_note);

        EditText onHoldNote = dialog.findViewById(R.id.edittext_onhold_note);


        buttonOnhold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(activity, "On Hold Screen Call", Toast.LENGTH_SHORT).show();
                String strOnHoldNote = onHoldNote.getText().toString();
                if (strOnHoldNote.isEmpty()) {
                    onHoldNote.setError("Please Enter the Reason!!");
                } else {
                    updateTask(activity, mTaskListResultData, strOnHoldNote);
                    dialog.dismiss();
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        photo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission(activity);
                String fromOnHold = "fromOnHold_1";
                ((CountdownTechFragmentNew) fragment).pickImage(fromOnHold, 1, null);
            }
        });
        photo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission(activity);
                String fromOnHold = "fromOnHold_2";
                ((CountdownTechFragmentNew) fragment).pickImage(fromOnHold, 1, null);
            }
        });
        photo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission(activity);
                String fromOnHold = "fromOnHold_3";
                ((CountdownTechFragmentNew) fragment).pickImage(fromOnHold, 1, null);
            }
        });
        dialog.show();
    }

    private boolean checkCameraPermission(Activity activity) {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return cameraFlag;
    }


    private boolean verifyGPSAndInternetAndPermission(Activity activity) {
        HomeActivityNew homeActivity = ((HomeActivityNew) activity);
        if (homeActivity.isInternetConnectionAvailable()) {
            FWLogger.logInfo(TAG, "isInternetConnectionAvailable ");
            //Go To Next step
            //Step 3 : verify and check all permissions are enabled
            if (homeActivity.checkAllPermissionEnabled()) {
                FWLogger.logInfo(TAG, "checkAllPermissionEnabled ");
                //Go To Next step
                //Step 4 : verify and check GPS enabled
                if (homeActivity.isGPSEnabled()) {
                    FWLogger.logInfo(TAG, "isGPSEnabled ");
                    //Go To Next step
                    //Step 5 :  Start requesting location service and Check attendance API call
                    homeActivity.startRequestingLocationService();
                    return true;
                } else {
                    FWLogger.logInfo(TAG, "GPS is not enabled request to enable");
                    //else show non cancelable dialog to enable GPS and on GPS enable then continue with first step from onActivityResult
                    if (!AlertDialog.isAlertDialogShowing) {
                        ServiceDialog serviceDialog = ServiceDialog.getInstance();
                        serviceDialog.showGPSEnableDialog(homeActivity);
                    }
                    return false;
                }
            } else {
                //else request to enable permissions and on enable permission continue with first step
                FWLogger.logInfo(TAG, "Permission is not enabled request to enable");
                homeActivity.mListener = homeActivity;
                homeActivity.requestToEnableAllPermission(homeActivity.mListener, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION);
                return false;
            }
        } else {
            FWLogger.logInfo(TAG, "Internet Connection not Available ");
            //else show non cancelable dialog to refresh internet connection and on connection enabled continue with first step
            ServiceDialog serviceDialog = ServiceDialog.getInstance();
            serviceDialog.showRefreshInternetConnectionDialog(homeActivity);//this is recursive call
            return false;
        }
    }

    private int isTaskDateValid(TasksList.ResultData data) {
        int ret = 999;
        if (!data.getTaskDate().equalsIgnoreCase("")) {
            //Current date should not exceed (Greater than) Task Date
            Date dateOfTask = DateUtils.stringToDateFormatter(data.getTaskDate(), "yyyy-MM-dd");
            String dateMillis = DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd");
            FWLogger.logInfo(TAG, "dateMillis : " + dateMillis);
            Date date = DateUtils.stringToDateFormatter(dateMillis, "yyyy-MM-dd");
            FWLogger.logInfo(TAG, "date : " + date);
            ret = date.compareTo(dateOfTask);
        } else {
            ret = 999;
        }
        return ret;
    }

    // WAV FILE REC METHODS : START
    private String getFilename() {
//        String filePath = Environment.getExternalStorageDirectory().getPath();
//        String filePath = mContext.getExternalFilesDir(null).getAbsolutePath();
        File filePath = mContext.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File file = new File(filePath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        audioFilePath = file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV;
        return audioFilePath;
    }

    private String getTempFilename() {
//        String filePath = Environment.getExternalStorageDirectory().getPath();
//        String filePath = mContext.getExternalFilesDir(null).getAbsolutePath();
        File filePath = mContext.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File file = new File(filePath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filePath, AUDIO_RECORDER_TEMP_FILE);

        if (tempFile.exists()) tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    private void startRecording() {
        try {
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);
        } catch (Exception ex) {
            ex.getMessage();
        }

        int i = recorder.getState();
        if (i == 1) recorder.startRecording();

        isRecording = true;

        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {
                writeAudioDataToFile();
            }
        }, "AudioRecorder Thread");

        recordingThread.start();
    }

    private void writeAudioDataToFile() {
        byte data[] = new byte[bufferSize];
        String filename = getTempFilename();
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int read = 0;

        if (null != os) {
            while (isRecording) {
                read = recorder.read(data, 0, bufferSize);

                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    try {
                        os.write(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopRecording() {
        if (null != recorder) {
            isRecording = false;

            int i = recorder.getState();
            if (i == 1) recorder.stop();
            recorder.release();

            recorder = null;
            recordingThread = null;
        }

        FWLogger.logInfo(TAG, "getTempFilename() : " + getTempFilename());
        copyWaveFile(getTempFilename(), getFilename());
        deleteTempFile();
    }

    private void deleteTempFile() {
        File file = new File(getTempFilename());

        if (file.exists()) file.delete();
    }

    // For delete (Purge) audio files from device
    private void deleteWavFilesFromPath() {
        try {
            File filePath = mContext.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
            File dir = new File(filePath + "/" + AUDIO_RECORDER_FOLDER);
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void copyWaveFile(String inFilename, String outFilename) {
        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 2;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            FWLogger.logInfo(TAG, "File size: " + totalDataLen);
            recFilePath = inFilename;

            new WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);

            while (true) {
                try {
                    if (!(in.read(data) != -1)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                out.write(data);
            }
            audioData = encodeToBase64(outFilename);
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String encodeToBase64(final String url) {
        try {
            File file = new File(url);
            byte[] bFile = new byte[(int) file.length()];
            FileInputStream inputStream = new FileInputStream(url);
            inputStream.read(bFile);
            inputStream.close();
            return Base64.encodeToString(bFile, Base64.NO_WRAP);

        } catch (IOException e) {
            FWLogger.logInfo(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    public void onMultipleMaterialPost(ArrayList<AddTask.MultipleItemAssigned> SODArray) {
        finalPostArray = SODArray;
    }

    @Override
    public void onReassignItemPostListener(ArrayList<TasksList.MultipleItemAssigned> SODArray) {
        reassignMultiItemList = SODArray;
    }

    public class CounterForChronometer extends CountDownTimer {
        public CounterForChronometer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            FWLogger.logInfo(TAG, "Timer Completed..");
            stopRecording();
            chronometer.stop();
            recAudio.setEnabled(true);
            recAudio.setAlpha(1.0f);
            recAudioStop.setEnabled(false);
            recAudioStop.setAlpha(.5f);
            recAudioPlay.setEnabled(true);
            recAudioPlay.setAlpha(1.0f);
//            recAudio.setVisibility(View.VISIBLE);
//            recAudioStop.setVisibility(View.GONE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            FWLogger.logInfo(TAG, "Timer  : " + (millisUntilFinished / 1000));
        }
    }

    public class CounterForEditChronometer extends CountDownTimer {
        public CounterForEditChronometer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            FWLogger.logInfo(TAG, "Timer Completed..");
            stopRecording();
            editChronometer.stop();
            editRecAudio.setEnabled(true);
            editRecAudio.setAlpha(1.0f);
            editRecAudioStop.setEnabled(false);
            editRecAudioStop.setAlpha(.5f);
            editRecAudioPlay.setEnabled(true);
            editRecAudioPlay.setAlpha(1.0f);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            FWLogger.logInfo(TAG, "Timer  : " + (millisUntilFinished / 1000));
        }
    }
    // WAV FILE REC METHODS : END

    // POST ONHOLD STATUS
    public void updateTask(Activity activity, TasksList.ResultData resultData, String strOnHoldNote) {
        try {
            if (Connectivity.isNetworkAvailableRetro(activity)) {
                //
                CommonFunction.showProgressDialog(activity);
                OnHoldTaskDetails onHoldTaskDetails = new OnHoldTaskDetails();

                onHoldTaskDetails.setId(0);
                onHoldTaskDetails.setTaskId(resultData.getId());
                onHoldTaskDetails.setUserId(resultData.getUserId());
                onHoldTaskDetails.setOnHoldNotes(strOnHoldNote);
                onHoldTaskDetails.setPick1(CountdownTechFragmentNew.mEncodedBaseString1);
                onHoldTaskDetails.setPick2(CountdownTechFragmentNew.mEncodedBaseString2);
                onHoldTaskDetails.setPick3(CountdownTechFragmentNew.mEncodedBaseString3);
                onHoldTaskDetails.setCreatedBy(resultData.getCreatedBy());
                onHoldTaskDetails.setCreatedDate(resultData.getCreatedDate());
                onHoldTaskDetails.setUpdatedBy(resultData.getUpdatedBy());
                onHoldTaskDetails.setUpdatedDate(resultData.getUpdatedDate());
                onHoldTaskDetails.setTaskStatus(5);


                Call<OnHoldTaskDetails> call = RetrofitClient.getInstance(mContext).getMyApi().postOnHoldTaskStatus(onHoldTaskDetails, "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<OnHoldTaskDetails>() {
                    @Override
                    public void onResponse(Call<OnHoldTaskDetails> call, Response<OnHoldTaskDetails> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(activity);
                                OnHoldTaskDetails onHoldTaskDetails = response.body();
                                if (onHoldTaskDetails != null) {
                                    CountdownTechFragmentNew.mEncodedBaseString1 = "";
                                    CountdownTechFragmentNew.mEncodedBaseString2 = "";
                                    CountdownTechFragmentNew.mEncodedBaseString3 = "";
                                    Toast.makeText(activity, "Status Updated Successfully.", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(activity, HomeActivityNew.class);
                                    activity.startActivity(intent);
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<OnHoldTaskDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in OnHoldTaskDetails API:");
                    }
                });
            } else {
                CommonFunction.hideProgressDialog(activity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(activity);
            FWLogger.logInfo(TAG, "Exception in OnHoldTaskDetails API:");
            ex.getMessage();
        }

    }

    public void addTaskFromQuoteDialog(Activity activity, ArrayList<String> itemsNameList, ArrayList<String> userNameList, boolean onEnquiry, EnquiryList.ResultData enquiry, int techPosition, int amcServiceDetailsId, boolean isAMC, AMCDetails.ResultData AMCResultData, List<CustomerList.ResultData> customerList, List<QuotationDetailsDTO.ResultData> mQuoteList, int quoteId) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
//        Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_task_from_quote);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        TextInputLayout textInputDatePicker = dialog.findViewById(R.id.textInput_date_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
//        TextInputLayout textInputCustomerName = dialog.findViewById(R.id.textInput_customer_name);
        TextInputLayout textInputCustomerNumber = dialog.findViewById(R.id.textInput_customer_number);
        TextInputLayout textInputTaskDescription = dialog.findViewById(R.id.textInput_task_description);
        TextInputLayout textInputWages = dialog.findViewById(R.id.textInput_wages);
        TextInputLayout textInputAddress = dialog.findViewById(R.id.textInput_task_address);
        TextInputLayout textInputAddressDesc = dialog.findViewById(R.id.textInput_address_desc);
        RecyclerView recyclerView_Item_list = dialog.findViewById(R.id.recyclerView_item_list);

        mContext = activity;

        TextView howto = dialog.findViewById(R.id.textView_how_to_add);
        EditText editTextTaskTitle = dialog.findViewById(R.id.edittext_task_name);
        EditText editTextDatePicker = dialog.findViewById(R.id.edittext_date_picker);
//        EditText editTextCustName = dialog.findViewById(R.id.edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextAddress = dialog.findViewById(R.id.edittext_task_address);
        EditText editTextLandmark = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextTaskDesc = dialog.findViewById(R.id.edittext_task_description);
        EditText editTextWages = dialog.findViewById(R.id.edittext_task_wages);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);

        ImageView closeAddTask = dialog.findViewById(R.id.closeAddTask);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);
        /*Spinner technicians = dialog.findViewById(R.id.spin_technicians);*/

        SearchableSpinner technicians = (SearchableSpinner) dialog.findViewById(R.id.spin_technicians);

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        Button buttonSaveTask = dialog.findViewById(R.id.button_add);
        Button buttonCancelTask = dialog.findViewById(R.id.button_cancel);

        ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);

        mLayoutManager = new LinearLayoutManager(mContext);
        addItemsAdapter = new AddItemsAdapter(this, multipleItemAssignedArrayList/*addItemList*/, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView_Item_list.setLayoutManager(mLayoutManager);
        recyclerView_Item_list.setAdapter(addItemsAdapter);

        SegmentedGroup segmentedGroupTaskType = dialog.findViewById(R.id.segmentedGroupType);
        SegmentedGroup segmentedGroupSelection = dialog.findViewById(R.id.segmentedGroupSelection);
        LinearLayout moredetails = dialog.findViewById(R.id.linear_AddMoreDetails);
        LinearLayout itemss = dialog.findViewById(R.id.items);
        //
        LinearLayout lnlQuotation = dialog.findViewById(R.id.lnlQuotation);
        TextView lblQuoteNameNum = dialog.findViewById(R.id.lblQuoteNameNum);

        SegmentedGroup segmentedGroupRate = dialog.findViewById(R.id.segmentedGroupRate);
        RadioButton radioButtonRate = dialog.findViewById(R.id.radiobuttonRate);
        RadioButton radioButtonAMC = dialog.findViewById(R.id.radiobuttonAMC);

        chronometer = (Chronometer) dialog.findViewById(R.id.chronometer);
        recAudio = (ImageView) dialog.findViewById(R.id.recAudio);
        recAudioStop = (ImageView) dialog.findViewById(R.id.imageView_stop_audio);
        recAudioPlay = (ImageView) dialog.findViewById(R.id.imageView_play_audio);
        chronometerTimer = new CounterForChronometer(31000, 1000); // 30 SEC TIMER INITALIZATION FOR AUDIO REC
        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

//        recAudio.setEnabled(false);
//        recAudio.setAlpha(.5f);
        recAudioStop.setEnabled(false);
        recAudioStop.setAlpha(.5f);
        recAudioPlay.setEnabled(false);
        recAudioPlay.setAlpha(.5f);

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        mAutoCompleteCustName = autoCompleteCustName;
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        try {
            if (mQuoteList.size() > 0) {
                autoCompleteCustName.setText(mQuoteList.get(0).getCustomer().getCustomerName());
                editTextCustNumber.setText(mQuoteList.get(0).getCustomer().getMobileNumber());
                editTextAddress.setText(mQuoteList.get(0).getCustomer().getLocationList().getAddress());
                editTextLandmark.setText(mQuoteList.get(0).getCustomer().getLocationList().getDescription());
                editTextTaskTitle.setText(mQuoteList.get(0).getQuoteName());
                lblQuoteNameNum.setText("#" + mQuoteList.get(0).getId() + " " + mQuoteList.get(0).getQuoteName());
                //editTextLandmark.setText(mQuoteList.get(0).getCustomer().getLocationList().get());
                mTempLat = Double.parseDouble(mQuoteList.get(0).getCustomer().getLocationList().getLatitude());
                mTempLong = Double.parseDouble(mQuoteList.get(0).getCustomer().getLocationList().getLongitude());
            }

        } catch (Exception ex) {
            ex.getMessage();
        }

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentTask();
            }
        });

        howto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent send = new Intent(mContext.getApplicationContext(), YouTubeAddTaskActivity.class);
                mContext.startActivity(send);
            }
        });


        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                editTextCustNumber.setText(customerObj.getMobileNumber());
                editTextAddress.setText(customerObj.getAddress());
                editTextLandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextTaskTitle.clearFocus();
                editTextAddress.clearFocus();
                editTextLandmark.clearFocus();
                return false;
            }
        });

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputDatePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));

        /////////////////////////////////////////Time picker on Add Task form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /* userNameList.add(activity.getResources().getString(R.string.select_technicians));
        LinkedList<String> linkedList = new LinkedList<>(userNameList);
        userNameList.clear();
        userNameList.addAll(linkedList);*/
        userNameList.add(activity.getString(R.string.select_technicians));
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        if (techPosition != -1) {
            technicians.setSelection(techPosition);
            technicians.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //technicians.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        } else technicians.setSelection(arrayAdapterTech.getCount());

        //////////////////////////////AMC//////////////////////////////////
        if (isAMC) {
            if (AMCResultData.getAMCName() != null) {
                editTextTaskTitle.setText(AMCResultData.getAMCName());
            }
            if (AMCResultData.getProductDetail().getCustomerDetailInfoDto().getCustomerName() != null) {
                autoCompleteCustName.setText(AMCResultData.getProductDetail().getCustomerDetailInfoDto().getCustomerName());
                autoCompleteCustName.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                // autoCompleteCustName.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerDetailInfoDto().getMobileNumber() != null) {
                editTextCustNumber.setText("" + AMCResultData.getProductDetail().getCustomerDetailInfoDto().getMobileNumber());
                textInputCustomerNumber.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                //editTextCustNumber.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getAddress() != null) {
                editTextAddress.setText(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getAddress());
                textInputAddress.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                //editTextAddress.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getDescription() != null) {
                editTextLandmark.setText(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getDescription());
            }
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getName() != null)
                mTempLocName = AMCResultData.getProductDetail().getCustomerLocationInfoDto().getName();

            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLatitude() != null)
                mTempLat = Double.parseDouble(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLatitude());
            if (AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLongitude() != null)
                mTempLong = Double.parseDouble(AMCResultData.getProductDetail().getCustomerLocationInfoDto().getLongitude());
        }
        ////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (onEnquiry) {
            if (enquiry != null) {
                //Verify all temp place
                //need to verify task type position (may mismatch with new task type list )
                //Need to verify the pre-fill date
                //Need to verify pre-fill the time
                if (enquiry.getServiceName() != null)
                    editTextTaskTitle.setText(enquiry.getServiceName());
                if (enquiry.getCustomerName() != null) {
                    autoCompleteCustName.setText(enquiry.getCustomerName());
                    autoCompleteCustName.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // autoCompleteCustName.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getMobileNumber() != null) {
                    editTextCustNumber.setText(enquiry.getMobileNumber());
                    textInputCustomerNumber.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextCustNumber.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getLocDescription() != null) {
                    editTextLandmark.setText(enquiry.getLocDescription());
                    textInputAddressDesc.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextLandmark.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getAddress() != null) {
                    editTextAddress.setText(enquiry.getAddress());
                    textInputAddress.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
                    // editTextAddress.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                }
                if (enquiry.getTechnicalProblem() != null)
                    editTextTaskDesc.setText(enquiry.getTechnicalProblem());
                if (enquiry.getTaskTypeId() != 0) {
                    if (enquiry.getTaskTypeId() == 1)
                        segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
                    else if (enquiry.getTaskTypeId() == 2) {
                        segmentedGroupTaskType.check(R.id.radiobuttonToday);
                        textInputTimePicker.setEnabled(true);
                    } else {
                        segmentedGroupTaskType.check(R.id.radiobuttonOther);
                        textInputDatePicker.setEnabled(true);
                        textInputTimePicker.setEnabled(true);
                    }
                }
                if (enquiry.getLatitude() != null)
                    mTempLat = Double.parseDouble(enquiry.getLatitude());
                if (enquiry.getLongitude() != null)
                    mTempLong = Double.parseDouble(enquiry.getLongitude());
                if (enquiry.getLocName() != null) mTempLocName = enquiry.getLocName();
                if (enquiry.getPrefDate() != null) {
                    editTextDatePicker.setText(enquiry.getPrefDate());
                }
            }
        }

        //--------------To set default Urgent date time--------------
        int currentTimePlusOne = getCurrentTimePlusOne(editTextDatePicker);
        //----------------------------------------------------------

        segmentedGroupSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonCustomer:

                        autoCompleteCustName.setVisibility(View.VISIBLE);
                        textInputCustomerNumber.setVisibility(View.VISIBLE);
                        phonebutton.setVisibility(View.VISIBLE);
                        itemss.setVisibility(View.GONE);
                        lnlQuotation.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonItems:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.VISIBLE);
                        lnlQuotation.setVisibility(View.GONE);
                        moredetails.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;

                    case R.id.radiobuttonQuotation:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        lnlQuotation.setVisibility(View.VISIBLE);
                        moredetails.setVisibility(View.GONE);
                        textInputTaskDescription.setVisibility(View.GONE);
                        break;


                    case R.id.radiobuttonInstructions:

                        autoCompleteCustName.setVisibility(View.GONE);
                        textInputCustomerNumber.setVisibility(View.GONE);
                        phonebutton.setVisibility(View.GONE);
                        itemss.setVisibility(View.GONE);
                        lnlQuotation.setVisibility(View.GONE);
                        moredetails.setVisibility(View.VISIBLE);
                        textInputTaskDescription.setVisibility(View.VISIBLE);
                        break;

                    default:
                        // Nothing to do
                        break;
                }
            }
        });

        //if (isAMC) radioButtonRate.setClickable(false);
        radioButtonRate.setChecked(true);
        try {
            textInputWages.setHint(activity.getString(R.string.please_enter_amount));
            textInputWages.setEnabled(true);
            int amtint = (int) mQuoteList.get(0).getGrandTotalAmount();
            editTextWages.setText(String.valueOf(amtint));
        } catch (Exception e) {
            e.getMessage();
        }

        ///////////////////////////////////////////////////////////////////////////////////////
        segmentedGroupRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAMC:
                        editTextWages.setText("");
                        textInputWages.setEnabled(false);
                        textInputWages.setHint(activity.getString(R.string.not_applicable_in_amc_mode));
                        break;
                    case R.id.radiobuttonRate:
                        editTextWages.setHint("");
                        textInputWages.setEnabled(true);
                        try {
                            int amtint = (int) mQuoteList.get(0).getGrandTotalAmount();
                            editTextWages.setText(String.valueOf(amtint));
                        } catch (Exception e) {
                            e.getMessage();
                        }
                        textInputWages.setHint(activity.getString(R.string.please_enter_amount));
                        break;
                    default:
                        // Nothing to do
                }
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedTime = editTextTimePicker.getText().toString();

                if (isEmpty(editTextTaskTitle)) {
                    editTextTaskTitle.setError(activity.getString(R.string.please_enter_task_name));
                } else if (!isEmpty(editTextTaskTitle) && Character.isWhitespace(editTextTaskTitle.getText().toString().charAt(0)) || editTextTaskTitle.getText().toString().trim().isEmpty()) {
                    editTextTaskTitle.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(autoCompleteCustName) && (Character.isWhitespace(autoCompleteCustName.getText().toString().charAt(0)) || autoCompleteCustName.getText().toString().trim().isEmpty())) {
                    autoCompleteCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (technicians.getSelectedItem() == activity.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (isEmpty(editTextAddress)) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextLandmark)) {
                    editTextLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextLandmark) && (Character.isWhitespace(editTextLandmark.getText().toString().charAt(0)) || editTextLandmark.getText().toString().trim().isEmpty())) {
                    editTextLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextTimePicker)) {
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_time));
                } else if (!DateUtils.isFutureDate(editTextDatePicker.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                } else if (!isEmpty(editTextTaskDesc) && (Character.isWhitespace(editTextTaskDesc.getText().toString().charAt(0)) || editTextTaskDesc.getText().toString().trim().isEmpty())) {
                    editTextTaskDesc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (mTempLat == 0 || mTempLong == 0) {
                    editTextAddress.setError(activity.getString(R.string.please_enter_address));
                    Toast.makeText(activity, activity.getResources().getString(R.string.please_enter_address_correctly), Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmpty(editTextCustNumber)) { //This will validate only if user enter the customer no
                        if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(editTextCustNumber)) {
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(editTextCustNumber)) { // NRI
                                editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }

                       /* else if (isValidPhone(editTextCustNumber)) {
                            editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }*/
                    }
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {//This will validate only if user select rate
                        if (isEmpty(editTextWages)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_rate));
                            return;
                        } else if ((Integer.valueOf(editTextWages.getText().toString()) == 0)) {
                            editTextWages.setError(activity.getString(R.string.please_enter_valid_amount));
                            return;
                        }
                    }
                    // COMMENTED BY MANISH : TO PASS MULTI ITEM ARRAY
                   /* if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() == 0) {
                            ((TextView) items.getSelectedView()).setError(activity.getString(R.string.quantity_is_0));
                            Toast.makeText(activity, activity.getString(R.string.quantity_is_0), Toast.LENGTH_SHORT).show();
                            arrayAdapterItem.notifyDataSetChanged();
                            return;
                        }
                        if (isEmpty(editTextItemQty)) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        } else if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    } else {
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) == 0) {
                            editTextItemQty.setError(activity.getString(R.string.please_enter_valid_quantity));
                            return;
                        }
                    }*/

                    AddTask.ResultData addTask = new AddTask.ResultData();
                    UsersList.ResultData usersLists = ((HomeActivityNew) activity).mUsersLists.get(technicians.getSelectedItemPosition());
                    int tasktype = segmentedGroupTaskType.indexOfChild(dialog.findViewById(segmentedGroupTaskType.getCheckedRadioButtonId()));
                    FWLogger.logInfo(TAG, "tasktype : " + tasktype);
                    if (onEnquiry) addTask.setId(enquiry.getEnquiryId());
                    else addTask.setId(0);

                    if (customerObj != null && !autoCompleteCustName.getText().toString().isEmpty()) {
                        addTask.setCustomerDetailsid(customerObj.getCustomerDetailsid());
                    } else {
                        addTask.setCustomerDetailsid(0);
                    }

                    addTask.setName(editTextTaskTitle.getText().toString());
                    addTask.setCustomerName(autoCompleteCustName.getText().toString());
                    addTask.setContactNo(editTextCustNumber.getText().toString());
                    addTask.setTaskStatus(4);
                    addTask.setTaskType(tasktype + 1);
                    FWLogger.logInfo(TAG, String.valueOf(tasktype));
                    addTask.setTaskDate(editTextDatePicker.getText().toString());
                    FWLogger.logInfo(TAG, "selectedTime " + selectedTime);
                    addTask.setTime(selectedTime);//TODO Need to work on this requires function
                    FWLogger.logInfo(TAG, "selectedTime get " + addTask.getTime());
                    addTask.setLocationId(0);
                    if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonRate) {
                        addTask.setPaymentMode(Constant.PaymentMode.RATE_STRING);
                        addTask.setPaymentModeId(Constant.PaymentMode.RATE_ID);//2 for Rate
                        addTask.setWagesPerHour(Integer.valueOf(editTextWages.getText().toString()));
                    } else if (segmentedGroupRate.getCheckedRadioButtonId() == R.id.radiobuttonAMC) {
                        addTask.setPaymentMode(Constant.PaymentMode.AMC_STRING);
                        addTask.setPaymentModeId(Constant.PaymentMode.AMC_ID);//1 for AMC
                    }

                    addTask.setUserId(usersLists.getId());
                    // COMMENTED BY MANISH
                    /*if (!itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.select_item)) && !itemsNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(activity.getString(R.string.none))) {//Validate Only when user select item
                        if ((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getUnAssignedQuantity() != 0) {
                            addTask.setItemId((((HomeActivityNew) activity).mItemsLists.get(items.getSelectedItemPosition() - 1)).getId());
                        }
                        if (!isEmpty(editTextItemQty) && Integer.valueOf(editTextItemQty.getText().toString()) != 0) {
                            addTask.setItemQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        }
                    }*/
                    addTask.setItemId(0);
                    //addTask.setItemQuantity(0);

                    addTask.setIsActive(true);
                    addTask.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    addTask.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    if (mTempLocName != null) addTask.setLocName(mTempLocName);//short address
                   /* if (mTempLat != 0) addTask.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) addTask.setLongitude(String.valueOf(mTempLong));//long*/
                    if (mTempLat != 0 && mTempLong != 0) {
                        addTask.setLatitude(String.valueOf(mTempLat));//lat from ADD QUOTE
                        addTask.setLongitude(String.valueOf(mTempLong));//long from ADD QUOTE
                    }

                    if (editTextLandmark.getText().toString() != null)
                        addTask.setLocDescription(editTextLandmark.getText().toString());//address description
                    if (editTextAddress.getText().toString() != null)
                        addTask.setAddress(editTextAddress.getText().toString());//full address
                    if (mTempLong != 0 && mTempLat != 0)
                        addTask.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));//Pin code or plus code

                    addTask.setLocIsActive(true);
                    addTask.setIsSuccessful(true);
                    addTask.setIsModelError(true);
                    addTask.setDescription(editTextTaskDesc.getText().toString());
                    if (amcServiceDetailsId != 0)
                        addTask.setAMCServiceDetailsId(amcServiceDetailsId);

                    if (onEnquiry) addTask.setCustomerDetailsid(enquiry.getCustomerDetailsid());
                    try {
                        if (audioData == null || audioData.isEmpty()) {
                            audioData = null;
                            addTask.setAudioFilePath("");
//                            addTask.setBase64AudioString(audioData);
                        } else {
                            addTask.setAudioFilePath("");
                            addTask.setBase64AudioString(audioData);
                        }

                        try {
                            List<AddTask.MultipleItemAssigned> itemAssignedList = new ArrayList<AddTask.MultipleItemAssigned>();
                            //AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();

                            for (int i = 0; i < finalPostArray.size(); i++) {
                                AddTask.MultipleItemAssigned multipleItemAssignedpost = new AddTask.MultipleItemAssigned();
                                multipleItemAssignedpost.setItemId(finalPostArray.get(i).getItemId());
                                multipleItemAssignedpost.setItemQuantity(finalPostArray.get(i).getItemQuantity());
                                multipleItemAssignedpost.setItemName(finalPostArray.get(i).getItemName());

                                itemAssignedList.add(multipleItemAssignedpost);
                            }

                            addTask.setMultipleItemAssigned(itemAssignedList);

                            // DURING ADD TASK : 0
                            addTask.setOnHoldTaskId(0); // CFT : Fresh
                            addTask.setQuotationId(quoteId);


                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        ((HomeActivityNew) activity).saveAddTask(addTask);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    // Clear file details of recorded audio
                    clearFileFromMem();

                    //arrayAdapterItem.remove(activity.getString(R.string.none));
                    //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                    arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                    customerObj = null;
                    dialog.dismiss();
                }
            }


            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });


        closeAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //arrayAdapterItem.remove(activity.getString(R.string.none));
                //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                if (null != recorder) {
                    isRecording = false;
                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        buttonCancelTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //arrayAdapterItem.remove(activity.getString(R.string.none));
                //arrayAdapterItem.remove(activity.getString(R.string.select_item));
                arrayAdapterTech.remove(activity.getString(R.string.select_technicians));
                customerObj = null;
                audioFilePath = "";
                if (null != recorder) {
                    isRecording = false;

                    int i = recorder.getState();
                    if (i == 1) recorder.stop();
                    recorder.release();

                    recorder = null;
                    recordingThread = null;
                }
                dialog.dismiss();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                if (place != null) {
                    editTextAddress.setError(null);
                    editTextLandmark.setError(null);
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        // AUDIO REC EVENT :
        recAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*AppLog.logString("Start Recording");
                enableButtons(true);*/
//                recAudioStop.setVisibility(View.VISIBLE);
                recAudioStop.setEnabled(true);
                recAudioStop.setAlpha(1.0f);
                recAudio.setEnabled(false);
                recAudio.setAlpha(.5f);
                recAudioPlay.setEnabled(false);
                recAudioPlay.setAlpha(.5f);
//                recAudio.setVisibility(View.GONE);
                chronometerTimer.start();
                chronometer.setFormat("Time (%s)");
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                startRecording();
            }
        });

        recAudioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                recAudio.setVisibility(View.VISIBLE);
//                recAudioStop.setVisibility(View.GONE);
                recAudio.setEnabled(true);
                recAudio.setAlpha(1.0f);
                recAudioStop.setEnabled(false);
                recAudioStop.setAlpha(.5f);
                recAudioPlay.setEnabled(true);
                recAudioPlay.setAlpha(1.0f);

                chronometerTimer.cancel();
                chronometer.stop();
                stopRecording();

            }
        });

        recAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FWLogger.logInfo(TAG, "getFilename() : " + audioFilePath);

                    /*if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {
                        if (audioFilePath != null && !audioFilePath.isEmpty()) {
                            Uri uri = Uri.parse(audioFilePath);
                            Intent viewMediaIntent = new Intent();
                            viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
                            viewMediaIntent.setDataAndType(uri, "audio/*");
                            viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            activity.startActivity(viewMediaIntent);
                        } else {
                            Toast.makeText(activity, "Audio File not found!!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                    }*/

                    /*if (((HomeActivityNew) activity).checkAllPermissionEnabled()) {*/
                    if (((HomeActivityNew) activity).AudioPermission()) {
                        File file = new File(audioFilePath);
                        if (file.exists()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                Uri contentUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.setDataAndType(contentUri, "audio/*");
                                activity.startActivity(intent);
                            } else {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "audio");
                                activity.startActivity(intent);
                            }
                        } else {
                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
                            builder.setTitle("");
                            builder.setMessage("File does not exist");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                            builder.create().show();
                        }
                    } else {
                        //((HomeActivityNew) activity).requestToEnableAllPermission(((HomeActivityNew) activity), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
                        ((HomeActivityNew) activity).AudioPermission();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        dialog.show();
    }


    /*
        private void TelCMI(Activity activity, String custContactnum) {
            try {
                String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
                List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
                ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
                ArrayList<Connect> connectArrayList = new ArrayList<>();

                TeleCMICall teleCMICall = new TeleCMICall();
                teleCMICall.setAppid(2222293);
                teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
                teleCMICall.setFrom(Long.valueOf("918037222880"));
                teleCMICall.setTo(Long.valueOf(Long.valueOf(techMobile)));// TECH NUMBER
                //
                Pcmo pcmo1 = new Pcmo();
                pcmo1.setAction("record");

                Pcmo pcmo = new Pcmo();
                //pcmo.setAction("record");
                pcmo.setAction("bridge");
                pcmo.setDuration(100);
                pcmo.setTimeout(20);
                pcmo.setFrom(Long.valueOf("8037222880"));// OLDER SPAM NUMBER : 8037222876
                pcmo.setLoop(2);
                teleCMICall.setPcmo(pcmoArrayList);
                //
                Connect connect = new Connect();
                connect.setType("pstn");
                connect.setNumber(Long.valueOf(custContactnum));// CUST NUM
                connectArrayList.add(connect);

                pcmo.setConnect(connectArrayList);
                pcmoArrayList.add(pcmo1);
                pcmoArrayList.add(pcmo);
                //
                teleCMICallArrayList.add(teleCMICall);

                ((HomeActivityNew) activity).postData(teleCMICallArrayList);
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    */
    private void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918035234087"));
            teleCMICall.setTo(Long.valueOf(Long.valueOf("91" + techMobile)));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");

            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("918035234087"));// OLDER SPAM NUMBER : 8037222876
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf("91" + custContactnum));// CUST NUM
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

}
