package com.corefield.fieldweb.FieldWeb.Admin;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;


/**
 * Fragment Controller for HelpAndSupportFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Help and Support
 */
public class HelpAndSupportFragment extends Fragment implements View.OnClickListener {
    protected static String TAG = HelpAndSupportFragment.class.getSimpleName();
    private View mRootView;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_help_support, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.fieldweb));
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.VISIBLE);
        //((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        TextView textViewHealthSafety = mRootView.findViewById(R.id.textView_health_safety);
        TextView textViewTermsCondition = mRootView.findViewById(R.id.textView_terms_condition);
        TextView textViewRateUs = mRootView.findViewById(R.id.textView_rate_us);
        ImageView imageViewCall = mRootView.findViewById(R.id.imageView_call);
        ImageView imageViewMail = mRootView.findViewById(R.id.imageView_email);
        ImageView imageViewLocation = mRootView.findViewById(R.id.imageView_location);

        if (SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            //textViewHealthSafety.setVisibility(View.GONE);
        } else {
            //textViewHealthSafety.setVisibility(View.VISIBLE);
        }

        textViewRateUs.setOnClickListener(this);
        textViewTermsCondition.setOnClickListener(this);
        textViewHealthSafety.setOnClickListener(this);
        imageViewCall.setOnClickListener(this);
        imageViewMail.setOnClickListener(this);
        imageViewLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        switch (v.getId()) {
            case R.id.textView_rate_us:
                CommonDialog commonDialog = CommonDialog.getInstance();
                commonDialog.rateUsDialog(getActivity());
                break;

           /* case R.id.textView_health_safety:
                ((HomeActivityNew) getActivity()).navigateToHealthAndSafetyActivity();
                break;*/

            case R.id.textView_terms_condition:
                loadFragment(new PrivacyPolicyFragment());
                break;

            case R.id.imageView_call:
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.CONTACT_SUPPORT, bundle);

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "+91 9344906855")); // 8178690938
                startActivity(intent);
                break;

            case R.id.imageView_email:
                Intent intentEmail = new Intent(Intent.ACTION_SENDTO);
                intentEmail.setData(Uri.parse("mailto:info@fieldweb.co.in"));
                intentEmail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentEmail);
                break;

            case R.id.imageView_location:
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 28.355560f, 76.947730f);
                Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intentMap);
                break;

            default:
                break;
        }
    }

    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("TYPE", "terms");
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }

}
