package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.corefield.fieldweb.DTO.AMC.AddAMC;
import com.corefield.fieldweb.DTO.AMC.ReminderModeList;
import com.corefield.fieldweb.DTO.AMC.ServiceOccurrenceList;
import com.corefield.fieldweb.FieldWeb.AMC.AMCListFragment;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddAMCActivity;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TimeSlot;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.core.content.ContextCompat;

import org.w3c.dom.Text;

import info.hoang8f.android.segmented.SegmentedGroup;

public class AMCDialog {
    public static boolean mSelectedWarranty = true;
    public static final String TAG = AMCDialog.class.getSimpleName();
    String mTempLocName = "";
    double mTempLat = 0, mTempLong = 0;
    Place mTempPlace = null;
    private static AMCDialog mAMCDialog;
    private String mName, mNumber;
    EditText mEditTextCustName, mEditTextCustNumber;
    Context mContext;


    /**
     * The private constructor for the AMCDialog Singleton class
     */
    private AMCDialog() {
    }

    public static AMCDialog getInstance() {
        //instantiate a new AMCDialog if we didn't instantiate one yet
        if (mAMCDialog == null) {
            mAMCDialog = new AMCDialog();
        }
        return mAMCDialog;
    }

    public void addEditAMC(Activity activity, ArrayList<String> serviceOccTypeList, ArrayList<String> reminderTypeList, int servicePosition, int reminderPosition, int noOfServicePosition) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_amc_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        mContext = activity;

        ScrollView scrollView = dialog.findViewById(R.id.scroll_view);
        SearchableSpinner serviceOccurrence = (SearchableSpinner) dialog.findViewById(R.id.spinner_services_occurrence);
        Spinner reminderMode = dialog.findViewById(R.id.spinner_set_reminder_mode);
        Spinner noOfServices = dialog.findViewById(R.id.spinner_no_of_services);
        EditText editTextActivationDate = dialog.findViewById(R.id.editText_activation_date);
        EditText editTextAmcName = dialog.findViewById(R.id.editText_amc_name);

        EditText editTextCustomerName = dialog.findViewById(R.id.editText_customer_name);
        mEditTextCustName = editTextCustomerName;
        EditText editTextCustomerNumber = dialog.findViewById(R.id.editText_customer_number);
        mEditTextCustNumber = editTextCustomerNumber;

        EditText editTextCustomerEmail = dialog.findViewById(R.id.editText_customer_email);
        EditText editTextCustomerAddress = dialog.findViewById(R.id.editText_customer_address);
        EditText editTextAddressDesc = dialog.findViewById(R.id.edittext_task_address_desc);
        EditText editTextPinCode = dialog.findViewById(R.id.editText_pin_code);
        EditText editTextProductBrand = dialog.findViewById(R.id.editText_product_brand);
        EditText editTextProductName = dialog.findViewById(R.id.editText_product_name);
        EditText editTextProductSerialNo = dialog.findViewById(R.id.editText_product_serial_no);

        TextInputLayout textInputAmount = dialog.findViewById(R.id.textInput_amount);
        EditText editTextAmount = dialog.findViewById(R.id.editText_amount);
        EditText timePicker = dialog.findViewById(R.id.edittext_time);
        TextView hAddAMCVideo = dialog.findViewById(R.id.textView_how_to_add_AMC);

        EditText editTextNote = dialog.findViewById(R.id.editText_note);
        Button buttonSave = dialog.findViewById(R.id.button_save);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        ImageView closeAddAMC = dialog.findViewById(R.id.closeAddAMC);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        SegmentedGroup segmentedGroupWarranty = dialog.findViewById(R.id.segmentedGroupForWarranty);
        /*RadioButton radioButtonNo = dialog.findViewById(R.id.radio_button_no);
        RadioButton radioButtonYes = dialog.findViewById(R.id.radio_button_yes);*/

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).getContactDetailsIntentAMC();
            }
        });
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextAmcName.clearFocus();
                editTextCustomerName.clearFocus();
                editTextCustomerNumber.clearFocus();
                editTextCustomerEmail.clearFocus();
                editTextCustomerAddress.clearFocus();
                editTextAddressDesc.clearFocus();
                editTextPinCode.clearFocus();
                editTextProductBrand.clearFocus();
                editTextProductName.clearFocus();
                editTextProductSerialNo.clearFocus();
                editTextActivationDate.clearFocus();
                return false;
            }
        });

        hAddAMCVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddAMCActivity.class);
                activity.startActivity(send);
            }
        });

        int checkedWarrantyButtonID = segmentedGroupWarranty.getCheckedRadioButtonId();

        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, timePicker);
            }
        });

        editTextCustomerAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, AMCListFragment.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                if (place != null) {
                    editTextCustomerAddress.setError(null);
                    editTextAddressDesc.setError(null);
                    editTextCustomerAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

        segmentedGroupWarranty.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button_yes:
                        mSelectedWarranty = true;
                        textInputAmount.setVisibility(View.GONE);
                        break;
                    case R.id.radio_button_no:
                        mSelectedWarranty = false;
                        textInputAmount.setVisibility(View.VISIBLE);
                        break;
                    default:
                        // Nothing to do
                }
            }
        });

        ArrayAdapter<String> arrayAdapterNoOfService = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, TimeSlot.getServiceOccurrenceArrayList(activity)) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterNoOfService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        noOfServices.setAdapter(arrayAdapterNoOfService);
        if (noOfServicePosition != -1) {
            noOfServices.setSelection(noOfServicePosition);
            noOfServices.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //noOfServices.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        } else noOfServices.setSelection(arrayAdapterNoOfService.getCount());


        serviceOccTypeList.add(activity.getString(R.string.service_occurrence));
        ArrayAdapter<String> arrayAdapterService = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, serviceOccTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceOccurrence.setAdapter(arrayAdapterService);
        if (servicePosition != -1) {
            serviceOccurrence.setSelection(servicePosition);
            serviceOccurrence.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //serviceOccurrence.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        } else serviceOccurrence.setSelection(arrayAdapterService.getCount());

        //To set reminder spinner data
        reminderTypeList.add(activity.getString(R.string.set_reminder_mode));
        ArrayAdapter<String> arrayAdapterReminder = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, reminderTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterReminder.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reminderMode.setAdapter(arrayAdapterReminder);
        if (reminderPosition != -1) {
            reminderMode.setSelection(reminderPosition);
            reminderMode.setEnabled(false);//Don't allow to edit as this may results in duplicate record in the database
            //reminderMode.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        } else reminderMode.setSelection(arrayAdapterReminder.getCount());

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceOccTypeList.remove(activity.getString(R.string.service_occurrence));
                reminderTypeList.remove(activity.getString(R.string.set_reminder_mode));
                dialog.dismiss();
            }
        });

        closeAddAMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceOccTypeList.remove(activity.getString(R.string.service_occurrence));
                reminderTypeList.remove(activity.getString(R.string.set_reminder_mode));
                dialog.dismiss();
            }
        });

        editTextActivationDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePickerToAcceptBackMonth(activity, editTextActivationDate);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedTime = timePicker.getText().toString();

                if (isEmpty(editTextAmcName)) {
                    editTextAmcName.setError(activity.getString(R.string.please_enter_amc_name));
                } else if (!isEmpty(editTextAmcName) && (Character.isWhitespace(editTextAmcName.getText().toString().charAt(0)) || editTextAmcName.getText().toString().trim().isEmpty())) {
                    editTextAmcName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCustomerName)) {
                    editTextCustomerName.setError(activity.getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(editTextCustomerName) && (Character.isWhitespace(editTextCustomerName.getText().toString().charAt(0)) || editTextCustomerName.getText().toString().trim().isEmpty())) {
                    editTextCustomerName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCustomerNumber)) {
                    editTextCustomerNumber.setError(activity.getString(R.string.please_enter_cust_number));
                } else if (!editTextCustomerEmail.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(editTextCustomerEmail.getText().toString()).matches() == false) {
                    editTextCustomerEmail.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else if (isEmpty(editTextCustomerAddress)) {
                    editTextCustomerAddress.setError(activity.getString(R.string.please_enter_cust_address));
                } else if (isEmpty(editTextAddressDesc)) {
                    editTextAddressDesc.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextAddressDesc) && (Character.isWhitespace(editTextAddressDesc.getText().toString().charAt(0)) || editTextAddressDesc.getText().toString().trim().isEmpty())) {
                    editTextAddressDesc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextProductBrand)) {
                    editTextProductBrand.setError(activity.getString(R.string.please_enter_prod_brand));
                } else if (!isEmpty(editTextProductBrand) && (Character.isWhitespace(editTextProductBrand.getText().toString().charAt(0)) || editTextProductBrand.getText().toString().trim().isEmpty())) {
                    editTextProductBrand.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextProductName)) {
                    editTextProductName.setError(activity.getString(R.string.please_enter_prod_name));
                } else if (!isEmpty(editTextProductName) && (Character.isWhitespace(editTextProductName.getText().toString().charAt(0)) || editTextProductName.getText().toString().trim().isEmpty())) {
                    editTextProductName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextProductSerialNo)) {
                    editTextProductSerialNo.setError(activity.getString(R.string.please_enter_prod_serial_number));
                } else if (editTextProductSerialNo.getText().toString().contains(" ")) {
                    editTextProductSerialNo.setError(activity.getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextActivationDate)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextActivationDate.setError(activity.getString(R.string.please_enter_date));
                } else if (timePicker.getText().toString().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.time, activity))) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    timePicker.setError(activity.getString(R.string.please_enter_time));
                } /*else if (!DateUtils.isFutureDate(editTextActivationDate.getText().toString(), selectedTime)) { //This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextActivationDate.setError(activity.getString(R.string.please_enter_valid_date_time));
                    timePicker.setError(activity.getString(R.string.please_enter_valid_date_time));
                }*/ else if (noOfServices.getSelectedItem() == activity.getString(R.string.no_of_services)) {
                    ((TextView) noOfServices.getSelectedView()).setError(activity.getString(R.string.please_select_no_of_services));
                } else if (reminderMode.getSelectedItem() == activity.getString(R.string.set_reminder_mode)) {
                    ((TextView) reminderMode.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                    Toast.makeText(activity, activity.getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
                } else if (serviceOccurrence.getSelectedItem() == activity.getString(R.string.service_occurrence)) {
                    ((TextView) serviceOccurrence.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                    Toast.makeText(activity, activity.getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
                } else if (mSelectedWarranty == false && isEmpty(editTextAmount)) {
                    editTextAmount.setError(activity.getString(R.string.please_enter_amount));
                } else if (!isEmpty(editTextNote) && (Character.isWhitespace(editTextNote.getText().toString().charAt(0)) || editTextNote.getText().toString().trim().isEmpty())) {
                    editTextNote.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    if (!isEmpty(editTextCustomerNumber)) {
                        if ((Long.valueOf(editTextCustomerNumber.getText().toString()) == 0)) {
                            editTextCustomerNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(editTextCustomerNumber)) {
                                editTextCustomerNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(editTextCustomerNumber)) { //FOR NRI
                                editTextCustomerNumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }
                        /*else if (isValidPhone(editTextCustomerNumber)) {
                            editTextCustomerNumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }*/
                    }

                    ServiceOccurrenceList.ResultData serviceOccResult = ((HomeActivityNew) activity).mServiceOccurrenceResultList.get(serviceOccurrence.getSelectedItemPosition());
                    ReminderModeList.ResultData reminderModeResult = ((HomeActivityNew) activity).mReminderModeResultList.get(reminderMode.getSelectedItemPosition());

                    AddAMC addAMC = new AddAMC();
                    AddAMC.CustomerDetail customerDetails = new AddAMC.CustomerDetail();
                    AddAMC.Location location = new AddAMC.Location();
                    AddAMC.ProductDetail productDetail = new AddAMC.ProductDetail();

                    addAMC.setUserId(SharedPrefManager.getInstance(activity).getUserId());
                    addAMC.setAMCName(editTextAmcName.getText().toString());
                    addAMC.setServiceOccuranceId(serviceOccResult.getServiceOccuranceId());

                    FWLogger.logInfo(TAG, "AMC Date : " + editTextActivationDate.getText().toString());

                    addAMC.setActivationDate(editTextActivationDate.getText().toString());
                    addAMC.setActivationTime(selectedTime);
                    if (editTextAmount != null && !editTextAmount.getText().toString().isEmpty())
                        addAMC.setAMCAmount(Integer.parseInt(editTextAmount.getText().toString()));

                    addAMC.setTotalServices(noOfServices.getSelectedItemPosition() + 1);
                    addAMC.setAMCSetReminderId(reminderModeResult.getAMCSetReminderId());
                    addAMC.setAMCNotes(editTextNote.getText().toString());

                    productDetail.setProductName(editTextProductName.getText().toString());
                    productDetail.setProductBrand(editTextProductBrand.getText().toString());
                    productDetail.setProductSerialNo(editTextProductSerialNo.getText().toString());
                    productDetail.setUnderWarranty(mSelectedWarranty);
                    addAMC.setProductDetail(productDetail);

                    customerDetails.setCustomerName(editTextCustomerName.getText().toString());
                    customerDetails.setMobileNumber(editTextCustomerNumber.getText().toString());
                    String emailId = "";
                    if (editTextCustomerEmail.getText() != null)
                        emailId = editTextCustomerEmail.getText().toString();
                    else emailId = "";
                    customerDetails.setEmailId(emailId);
                    customerDetails.setIsActive(true);

                    productDetail.setCustomerDetail(customerDetails);

                    location.setAddress(editTextCustomerAddress.getText().toString());
                    if (editTextAddressDesc.getText().toString() != null)
                        location.setDescription(editTextAddressDesc.getText().toString());

                    if (mTempLocName != null) location.setName(mTempLocName);//short address
                    location.setPinCode(editTextPinCode.getText().toString());
                    location.setIsActive(true);

                    if (mTempLat != 0) location.setLatitude(String.valueOf(mTempLat));//lat
                    if (mTempLong != 0) location.setLongitude(String.valueOf(mTempLong));//long

                    productDetail.setLocation(location);

                    ((HomeActivityNew) activity).addAMCAsyncTask(addAMC);

                    arrayAdapterService.remove(activity.getString(R.string.service_occurrence));
                    arrayAdapterNoOfService.remove("No of Services");
                    arrayAdapterReminder.remove(activity.getString(R.string.set_reminder_mode));
                    mSelectedWarranty = true;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                /*return phone.length() != 10;*/
                return phone.length() <= 6;
            }
        });

        dialog.show();
    }

    public void getContactData(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        //Toast.makeText(mContext, mName + "/" + mNumber, Toast.LENGTH_SHORT).show();
        //if(!mName.isEmpty() && !mNumber.isEmpty()){
        mEditTextCustName.setText(mName);
        mEditTextCustNumber.setText(mNumber);
        //}
    }
}
