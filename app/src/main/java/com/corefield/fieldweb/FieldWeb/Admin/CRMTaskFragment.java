package com.corefield.fieldweb.FieldWeb.Admin;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CRMAMCListAdapter;
import com.corefield.fieldweb.Adapter.CRMTasksListAdapter;
import com.corefield.fieldweb.Adapter.CustomerListAdapterNew;
import com.corefield.fieldweb.Adapter.DatesListAdapter;
import com.corefield.fieldweb.Adapter.EnquiryListAdapterNew;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.AMC.AMCTypeList;
import com.corefield.fieldweb.DTO.CRMTask.CRMAMCkList;
import com.corefield.fieldweb.DTO.CRMTask.CRMTaskList;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.DatesDto;
import com.corefield.fieldweb.DTO.Enquiry.AddEnquiry;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateEnquiryAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Listener.RecyclerTouchListenerUpdateItem;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for CRMFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Enquiry and Customer list
 */
public class CRMTaskFragment extends Fragment implements OnTaskCompleteListener, View.OnClickListener, RecyclerTouchListener, RecyclerTouchListenerUpdateItem, DatePickerDialog.OnDateSetListener {
    protected static String TAG = CRMTaskFragment.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewList;
    private RecyclerView mcrmRecyclerViewList;
    //private RecyclerView mAmcRecyclerViewList;
    private Button mButtonEnquiries, mButtonCustomers;
    private List<CRMTaskList.ResultData> mCRMTaskList;
    private List<CRMAMCkList.ResultData> mCRMAmcList;
    private EnquiryListAdapterNew mEnquiryListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLinearLayoutManager;
    private LinearLayoutManager mLinearLayoutManagerAmc;
    private SearchView mSearchView;
    CRMTasksListAdapter crmtasksListAdapter;
    CRMAMCListAdapter crmamcListAdapter;
    //
    public List<DatesDto> mDatesLists = null;
    DatesListAdapter mDatesListAdapter;
    public int mSelectedDate;

    TextView txtTaskCount, txtAmcCount;

    // PAGE INDEX LOGIC
    private static final int PAGE_START = 1;
    private int mCurrentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private boolean isLoadFirstTime = false;
    LinearLayout enquiryCustListLayout;
    private List<CustomerList.ResultData> mCustomerList;
    private CustomerListAdapterNew mCustomerListAdapter;
    private String mSelection = "", currentMonthName = "", mCustName = "";
    Bundle mBundle;
    private int custId;
    private UpdateEnquiryAsyncTask mUpdateEnquiryAsyncTask;
    private List<EnquiryList.ResultData> mEnquiryLists;
    CardView taskCardview, amcCardView;
    ImageView imgTaskNxt, imgNxt2, noResultImg;
    private Calendar mCal;
    private int mYear, mMonth, mDay;
    LinearLayout linear_user_header;
    Spinner mTaskTypeSpinner, mTaskStatusSpinner;
    EditText mEditTextYearMonth;
    private int mTaskStatusId = 0, mTaskTypeId = 0, mTypeCheck = 0, mStatusCheck = 0;
    private List<AMCTypeList.ResultData> mAMCTypeLists = null;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_crm_task, container, false);
        //NOTE: Log GA event
        mBundle = new Bundle();
        mBundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        mBundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        mBundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        mBundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        mBundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);


        mCal = Calendar.getInstance();
        mYear = mCal.get(Calendar.YEAR);
        mMonth = mCal.get(Calendar.MONTH);
        mDay = mCal.get(Calendar.DAY_OF_MONTH);
        currentMonthName = DateUtils.getMonthName(mMonth);
        mSelectedDate = mDay;

        inIT();
        return mRootView;
    }

    private void inIT() {
        mRecyclerViewList = mRootView.findViewById(R.id.recycler_list_view);
        mcrmRecyclerViewList = mRootView.findViewById(R.id.recycler_list_viewcrm);
        //mAmcRecyclerViewList = mRootView.findViewById(R.id.recycler_list_viewAmc);

        mButtonEnquiries = mRootView.findViewById(R.id.button_enquiries);
        mButtonCustomers = mRootView.findViewById(R.id.button_customers);
        mSearchView = mRootView.findViewById(R.id.edittext_search);

        enquiryCustListLayout = mRootView.findViewById(R.id.enquiryCustListLayout);
        linear_user_header = mRootView.findViewById(R.id.linear_user_header);
        mTaskTypeSpinner = mRootView.findViewById(R.id.spin_task_type);
        mTaskStatusSpinner = mRootView.findViewById(R.id.spin_task_status);
        mEditTextYearMonth = mRootView.findViewById(R.id.editText_year_month);

        imgTaskNxt = mRootView.findViewById(R.id.imgTaskNxt);
        imgNxt2 = mRootView.findViewById(R.id.imgNxt2);

        txtTaskCount = mRootView.findViewById(R.id.txtTaskCount);
        txtAmcCount = mRootView.findViewById(R.id.txtAmcCount);

        taskCardview = mRootView.findViewById(R.id.taskCardview);
        amcCardView = mRootView.findViewById(R.id.amcCardView);

        noResultImg = mRootView.findViewById(R.id.noResultImg);

        //taskCardview.setOnClickListener(this);
        //amcCardView.setOnClickListener(this);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                custId = getBundle.getInt("CustID");
                mCustName = getBundle.getString("CustName");
                //textCustName.setVisibility(View.VISIBLE);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(mCustName);
                //textCustName.setText(mCustName);
                enquiryCustListLayout.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mcrmRecyclerViewList.addOnScrollListener(new PaginationScrollListener(mLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        List<String> typeArray = new ArrayList<>();
        typeArray.add("Type");
        typeArray.add(Constant.TaskCategories.Urgent.name());
        typeArray.add(Constant.TaskCategories.Today.name());
        typeArray.add(Constant.TaskCategories.Schedule.name());

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, typeArray) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount();
            }
        };
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTaskTypeSpinner.setAdapter(typeAdapter);

        mTaskTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++mTypeCheck > 1) {
                    if (mCRMTaskList != null) clearTaskList();
                    mTaskTypeId = position;
                    mSearchView.setQuery("", false);

                    getCRMTaskList(mTaskStatusId, mTaskTypeId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> statusArray = new ArrayList<>();
        statusArray.add("Status");
        statusArray.add(Constant.TaskCategories.Completed.name());
        statusArray.add(Constant.TaskCategories.Rejected.name());
        statusArray.add(Constant.TaskCategories.Ongoing.name());
        statusArray.add(Constant.TaskCategories.InActive.name());
        statusArray.add(Constant.TaskCategories.OnHold.name());

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, statusArray) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(getResources().getColor(R.color.colorPrimaryDark));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount();
            }
        };
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTaskStatusSpinner.setAdapter(statusAdapter);

        mTaskStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++mStatusCheck > 1) {
                    if (mCRMTaskList != null) clearTaskList();
                    mTaskStatusId = position;
                    mSearchView.setQuery("", false);
                    getCRMTaskList(mTaskStatusId, mTaskTypeId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, ((HomeActivityNew) getActivity()).currentYear, ((HomeActivityNew) getActivity()).currentMonth, ((HomeActivityNew) getActivity()).currentDay) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int day = getContext().getResources().getIdentifier("android:id/day", null, null);
                if (day != 0) {
                    View dayPicker = findViewById(day);
                    if (dayPicker != null) {
                        //Set Day view visibility Off/Gone
                        dayPicker.setVisibility(View.GONE);
                    }
                }
            }
        };
        mCal.add(Calendar.DATE, 0);
        datePickerDialog.getDatePicker().setMaxDate(mCal.getTimeInMillis());
        mCal.add(Calendar.MONTH, -2);
        datePickerDialog.getDatePicker().setMinDate(mCal.getTimeInMillis());

        mEditTextYearMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        //
        mSelection = "TASK";
        mCurrentPage = PAGE_START;

        getCRMTaskList(0, 0);
        //mcrmRecyclerViewList.setVisibility(View.VISIBLE);
        //
        linear_user_header.setVisibility(View.VISIBLE);
        mEditTextYearMonth.setText(currentMonthName + " " + mYear);
        mEditTextYearMonth.setVisibility(View.VISIBLE);

        //}
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        if (mSelection.equalsIgnoreCase("TASK")) {
            getCRMTaskList(0, 0);
        } /*else if (mSelection.equalsIgnoreCase("AMC")) {
            getCRMAMCList();
        }*/

    }


    private void getCRMTaskList(int mTaskStatusId, int mTaskTypeId) {
       /* mGetCRMTaskListAsyncTask = new GetCRMTaskListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mGetCRMTaskListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), "", mTaskStatusId, mTaskTypeId, mCurrentPage, mMonth + 1, mYear, custId);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userId = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<CRMTaskList> call = RetrofitClient.getInstance(getContext()).getMyApi().getCRMTaskList(userId, "", mTaskStatusId, mTaskTypeId, mCurrentPage, mMonth + 1, mYear, custId);
                call.enqueue(new retrofit2.Callback<CRMTaskList>() {
                    @Override
                    public void onResponse(Call<CRMTaskList> call, Response<CRMTaskList> response) {
                        try {
                            if (response.code() == 200) {
                                CRMTaskList crmTaskList = response.body();
                                if (crmTaskList.getCode().equalsIgnoreCase("200") && crmTaskList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + crmTaskList.getMessage());
                                    if (crmTaskList != null) {
                                        mCRMTaskList = new ArrayList<>();
                                        mCRMTaskList = crmTaskList.getResultData();
                                        if (mCRMTaskList.size() > 0) {
                                            setData();
                                        } else {
                                            noResultImg.setVisibility(View.VISIBLE);
                                            noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                        }

                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CRMTaskList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in CRMGetAMCList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in CRMGetAMCList API:");
            ex.getMessage();
        }

    }

    private void getEnquiryList() {
        /*mGetEnquiryListAsyncTask = new GetEnquiryListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mGetEnquiryListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<EnquiryList> call = RetrofitClient.getInstance(getContext()).getMyApi().getEnquiryList(userID,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<EnquiryList>() {
                    @Override
                    public void onResponse(Call<EnquiryList> call, Response<EnquiryList> response) {
                        try {
                            if (response.code() == 200) {
                                EnquiryList enquiryList = response.body();
                                if (enquiryList.getCode().equalsIgnoreCase("200") && enquiryList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + enquiryList.getMessage());
                                    if (enquiryList != null) {
                                        mEnquiryLists = new ArrayList<>();
                                        mEnquiryLists = enquiryList.getResultData();
                                        setData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EnquiryList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllEnquiryListForMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllEnquiryListForMobile API:");
            ex.getMessage();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;
            case R.id.button_enquiries:
                mSelection = "ENQ";
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
                mSearchView.setQueryHint("Search by Enq no., Customer name...");
                mButtonEnquiries.setBackground(getResources().getDrawable(R.drawable.bg_curve_border_selected));
                mButtonEnquiries.setTextColor(getResources().getColor(R.color.black));
                mButtonCustomers.setBackground(getResources().getDrawable(R.drawable.bg_curve_border));
                mButtonCustomers.setTextColor(getResources().getColor(R.color.White));
                getEnquiryList();
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);
                break;

            case R.id.button_customers:
                mSelection = "CUST";
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
                mSearchView.setQueryHint("Search by Customer name...");
                mButtonCustomers.setBackground(getResources().getDrawable(R.drawable.bg_curve_border_selected));
                mButtonCustomers.setTextColor(getResources().getColor(R.color.black));
                mButtonEnquiries.setBackground(getResources().getDrawable(R.drawable.bg_curve_border));
                mButtonEnquiries.setTextColor(getResources().getColor(R.color.White));
                /*mGetCustomerListAsyncTask = new GetCustomerListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
                mGetCustomerListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
                getCustomerList();
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.CUSTOMER_LIST, bundle);
                break;

            case R.id.taskCardview:
                break;

            case R.id.amcCardView:

                break;

            default:
                break;
        }
    }


    public void saveUpdateEnquiry(AddEnquiry.ResultData addEnquiry) {
        mUpdateEnquiryAsyncTask = new UpdateEnquiryAsyncTask(getActivity(), BaseAsyncTask.Priority.LOW, this);
        mUpdateEnquiryAsyncTask.execute(addEnquiry);
    }

    public void updateEnquiry(EnquiryList.ResultData resultData) {
        /*Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_add_enquiry));
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);*/

        EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
        enquiryDialog.updateEnquiryDialog(getActivity(), ((HomeActivityNew) getActivity()).mServiceTypeResultData, resultData, this);
    }

    public void getCustomerList() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<CustomerList> call = RetrofitClient.getInstance(getContext()).getMyApi().getCustomerList(userID);
                call.enqueue(new retrofit2.Callback<CustomerList>() {
                    @Override
                    public void onResponse(Call<CustomerList> call, Response<CustomerList> response) {
                        try {
                            if (response.code() == 200) {
                                CustomerList customerList = response.body();
                                if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                                    if (customerList != null) {
                                        mCustomerList = new ArrayList<>();
                                        mCustomerList = customerList.getResultData();
                                        setData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(CRMTaskList.class.getSimpleName())) {
            Gson gson = new Gson();
            CRMTaskList crmTaskList = gson.fromJson(urlConnectionResponse.resultData, CRMTaskList.class);
            if (crmTaskList.getCode().equalsIgnoreCase("200") && crmTaskList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + crmTaskList.getMessage());
                if (crmTaskList != null) {
                    mCRMTaskList = new ArrayList<>();
                    mCRMTaskList = crmTaskList.getResultData();
                    if (mCRMTaskList.size() > 0) {
                        // txtTaskCount.setText(String.valueOf(mCRMTaskList.size()));
                        setData();
                    } else {
                        //Toast.makeText(getActivity(), "Record not available!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        } else if (classType.equalsIgnoreCase(CustomerList.class.getSimpleName())) {
            Gson gson = new Gson();
            CustomerList customerList = gson.fromJson(urlConnectionResponse.resultData, CustomerList.class);
            if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                if (customerList != null) {
                    mCustomerList = new ArrayList<>();
                    mCustomerList = customerList.getResultData();
                    setData();
                }
            }
        } else if (classType.equalsIgnoreCase(EnquiryList.class.getSimpleName())) {
            Gson gson = new Gson();
            EnquiryList enquiryList = gson.fromJson(urlConnectionResponse.resultData, EnquiryList.class);
            if (enquiryList.getCode().equalsIgnoreCase("200") && enquiryList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + enquiryList.getMessage());
                if (enquiryList != null) {
                    mEnquiryLists = new ArrayList<>();
                    mEnquiryLists = enquiryList.getResultData();
                    setData();
                }
            }
        } else if (classType.equalsIgnoreCase(AddEnquiry.class.getSimpleName())) {
            Gson gson = new Gson();
            AddEnquiry addEnquiry = gson.fromJson(urlConnectionResponse.resultData, AddEnquiry.class);
            //NOTE: Log GA event
            /*Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, addEnquiry.getResultData().getMobileNumber());
            FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_ENQ, bundle);*/

            //Reload Enquiry list to reflect updated details
            // getEnquiryList();
            Toast.makeText(getActivity(), addEnquiry.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (classType.equalsIgnoreCase(CRMAMCkList.class.getSimpleName())) {
            Gson gson = new Gson();
            CRMAMCkList crmamCkList = gson.fromJson(urlConnectionResponse.resultData, CRMAMCkList.class);
            if (crmamCkList.getCode().equalsIgnoreCase("200") && crmamCkList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + crmamCkList.getMessage());
                if (crmamCkList != null) {
                    mCRMAmcList = new ArrayList<>();
                    mCRMAmcList = crmamCkList.getResultData();
                    if (mCRMAmcList.size() > 0) {
                        txtAmcCount.setText(String.valueOf(mCRMAmcList.size()));
                        setData();
                    } else {
                        //Toast.makeText(getActivity(), "Record not available!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }

    private void setData() {
        // TASK
        if (mSelection.equalsIgnoreCase("TASK")) {
            if (mCRMTaskList != null) {
                // txtTaskCount.setText(String.valueOf(mCRMTaskList.size()));
                mcrmRecyclerViewList.setVisibility(View.VISIBLE);
                crmtasksListAdapter = new CRMTasksListAdapter(getContext(), mCRMTaskList, this);
                crmtasksListAdapter.setClickListener(this);
                mcrmRecyclerViewList.setLayoutManager(mLinearLayoutManager);
                mcrmRecyclerViewList.setAdapter(crmtasksListAdapter);
            }
        }
        // AMC
        /*else if (mSelection.equalsIgnoreCase("AMC")) {
            if (mCRMAmcList != null) {
                txtAmcCount.setText(String.valueOf(mCRMAmcList.size()));
                mAmcRecyclerViewList.setVisibility(View.VISIBLE);
                crmamcListAdapter = new CRMAMCListAdapter(getContext(), mCRMAmcList, this);
                crmamcListAdapter.setClickListener(this);
                mAmcRecyclerViewList.setLayoutManager(mLinearLayoutManagerAmc);
                mAmcRecyclerViewList.setAdapter(crmamcListAdapter);
            }
        }*/
    }


    @Override
    public void onClick(View view, int position) {
        /*if (view.getId() == R.id.enquiry_card_view) {
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_DETAILS, bundle);

            EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
            enquiryDialog.enquiryDetailsDialog(getActivity(), mEnquiryListAdapter.getItem(position));

        } else if (view.getId() == R.id.imageView_call) {
            if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mCustomerListAdapter.getItem(position).getMobileNumber()));
                startActivity(intent);
            } else {
                Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.imageView_message) {
            if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                try {
                    String number = mCustomerListAdapter.getItem(position).getMobileNumber();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                } catch (Exception e) {
                    Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
            }
        }*/
        /*if(mEnquiryListAdapter != null)
            resultData = mEnquiryListAdapter.getItem(position);*/

        switch (view.getId()) {
            case R.id.enquiry_card_view:
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_DETAILS, bundle);

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.enquiryDetailsDialog(getActivity(), mEnquiryListAdapter.getItem(position));
                break;

            case R.id.imageView_call:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mCustomerListAdapter.getItem(position).getMobileNumber()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    try {
                        String number = mCustomerListAdapter.getItem(position).getMobileNumber();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }

    }

    @Override
    public void onUpdateClick(View view, int position) {

    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mDay = dayOfMonth;
        mMonth = month;
        mYear = year;
        FWLogger.logInfo(TAG, "mMonth : " + mMonth + " " + mYear);

       /* if (mTaskTypeId == 0 && mTaskStatusId == 0 && mMonth == ((HomeActivityNew) getActivity()).currentMonth)
            isAllData = false;
        else
            isAllData = true;

        if (mTasksLists != null && getContext() != null) clearTaskList();*/

        if (mCRMTaskList != null && getContext() != null) clearTaskList();

        currentMonthName = DateUtils.getMonthName(mMonth);
        mEditTextYearMonth.setText(currentMonthName + " " + mYear);

        getCRMTaskList(mTaskStatusId, mTaskTypeId);
    }

    public void clearTaskList() {

        try {
            int size = mCRMTaskList.size();
            mCRMTaskList.clear();
            crmtasksListAdapter.notifyItemRangeRemoved(0, size);

            // For loading data after selecting task type from spinner
            isLoadFirstTime = true;
            mCurrentPage = PAGE_START;
            isLoading = false;
            isLastPage = false;
            // mSearchParam = "";

            crmtasksListAdapter = new CRMTasksListAdapter(getActivity(), mCRMTaskList, this);
            mcrmRecyclerViewList.setAdapter(crmtasksListAdapter);
            crmtasksListAdapter.setClickListener(this);
            mcrmRecyclerViewList.setVisibility(View.GONE);
            crmtasksListAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

}
