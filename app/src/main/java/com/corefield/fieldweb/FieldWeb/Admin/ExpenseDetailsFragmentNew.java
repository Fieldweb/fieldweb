package com.corefield.fieldweb.FieldWeb.Admin;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.ExpenseListAdapterNew;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.DatesDto;
import com.corefield.fieldweb.DTO.Expenditure.AddExpense;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseDetails;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ExpenditureDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for ExpenseDetailsFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for Expenditure Details  that show date wise expense of technician
 * @implNote This Fragment class is used for to show Expense List of Technician
 */

public class ExpenseDetailsFragmentNew extends Fragment implements OnTaskCompleteListener, RecyclerTouchListener,
        BaseActivity.RequestPermissionsUpdateListener, DatePickerDialog.OnDateSetListener {

    protected static String TAG = ExpenseDetailsFragmentNew.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewExpenseList;
    private int mPosition;
    private ExpenseListAdapterNew mExpenseListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ExpenseDetails.ResultData mExpenseDetails = null;
    private List<ExpenseDetails.ExpenseList> mExpenseLists = null;
    private String mDate, mDateView;
    private int mUserId = 0;
    RecyclerTouchListener recyclerTouchListener;

    private TextView mCreditedAmount, mOpeningAmount, mReturn, mRemainingAmount, mEarnedAmount, mTotalExpense, mTechFullName, mExpenseDate;

    private ExpenseTechList.ResultData mResultData;
//    private DatesListAdapter mDatesListAdapter;

    private int mAmount = 0;
    private String mWhichImage = "";
    private Uri mImageUri;
    private int mImageNo;
    private ExpenseDetailsFragmentNew.ImageUpdateListener mImageUpdateListener;
    private String mCapturedImagePath = "";
    public static String mExpenseImage = "Expense", mExpenseEncodedBaseString = null;
    private ImageView mImageViewAddExpense;
    private CardView mCardViewNA;
    private Calendar mCal;
    private int mYear, mMonth, mDay;
    String currentMonthName = "";
    EditText mEditTextYearMonth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.expense_details_new, container, false);

        mCal = Calendar.getInstance();
        mYear = mCal.get(Calendar.YEAR);
        mMonth = mCal.get(Calendar.MONTH);
        mDay = mCal.get(Calendar.DAY_OF_MONTH);

        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            mResultData = (ExpenseTechList.ResultData) getBundle.getSerializable("expenseResult");
            if (mResultData != null)
                mUserId = mResultData.getUserId();
            else
                mUserId = SharedPrefManager.getInstance(getContext()).getUserId();
        }
        mUserId = SharedPrefManager.getInstance(getContext()).getUserId();
        inIt();
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.VISIBLE);
       /* ((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.INVISIBLE);*/

//      ((HomeActivityNew) getActivity()).getMonthYearList();

        currentMonthName = DateUtils.getMonthName(mMonth);

        /*((HomeActivityNew) getActivity()).mEditTextYearMonth.setText(mDay + " " + currentMonthName + " " + mYear);*/

        mEditTextYearMonth.setText(mDay + " " + currentMonthName + " " + mYear);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, ((HomeActivityNew) getActivity()).currentYear, ((HomeActivityNew) getActivity()).currentMonth, ((HomeActivityNew) getActivity()).currentDay) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                /*int day = getContext().getResources().getIdentifier("android:id/day", null, null);
                if (day != 0) {
                    View dayPicker = findViewById(day);
                    if (dayPicker != null) {
                        //Set Day view visibility Off/Gone
                        dayPicker.setVisibility(View.VISIBLE);
                    }
                }*/
            }
        };

        mCal.add(Calendar.DATE, 0);
        datePickerDialog.getDatePicker().setMaxDate(mCal.getTimeInMillis());
        mCal.add(Calendar.MONTH, -2);
        datePickerDialog.getDatePicker().setMinDate(mCal.getTimeInMillis());


        /*((HomeActivityNew) getActivity()).mEditTextYearMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });*/
        mEditTextYearMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        /*((HomeActivityNew) getActivity()).mSpinnerMonthYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "onItemSelected called : " + position);
                String selectedMonth = parent.getItemAtPosition(position).toString();
                FWLogger.logInfo(TAG, "selectedMonth called : " + selectedMonth);
                ((HomeActivityNew) getActivity()).mSelectedMonth = ((HomeActivityNew) getActivity()).monthList.get(position);
                FWLogger.logInfo(TAG, "Month Number : " + ((HomeActivityNew) getActivity()).mSelectedMonth);
                getDatesList(mCal, ((HomeActivityNew) getActivity()).mSelectedMonth, ((HomeActivityNew) getActivity()).mSelectedDate, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        return mRootView;
    }

    private void inIt() {
        mRecyclerViewExpenseList = mRootView.findViewById(R.id.recycler_expense_list);
        mImageViewAddExpense = mRootView.findViewById(R.id.imageView_add_expense);
        mCardViewNA = mRootView.findViewById(R.id.cardView_na);

        mEditTextYearMonth = mRootView.findViewById(R.id.editText_year_month);
        recyclerTouchListener = this;

        setDates();

        mTechFullName = mRootView.findViewById(R.id.textView_tech_name);
        mExpenseDate = mRootView.findViewById(R.id.textView_date);
        mCreditedAmount = mRootView.findViewById(R.id.expense_credited_amount);
        mOpeningAmount = mRootView.findViewById(R.id.expense_opening_balance);
        mReturn = mRootView.findViewById(R.id.expense_return_amount);
        mRemainingAmount = mRootView.findViewById(R.id.expense_remaining_balance);
        mEarnedAmount = mRootView.findViewById(R.id.expense_earned_amount);
        mTotalExpense = mRootView.findViewById(R.id.expense_total_expenses_amount);

        callAPI();

//        FWLogger.logInfo(TAG, "Selected Month Pos : "+((HomeActivityNew) getActivity()).mSpinnerMonthYear.getSelectedItemPosition());
//        getDatesList(mCal, ((HomeActivityNew) getActivity()).mSelectedMonth, ((HomeActivityNew) getActivity()).mSelectedDate, ((HomeActivityNew) getActivity()).mSpinnerMonthYear.getSelectedItemPosition());

        getDatesList(mCal, mMonth, ((HomeActivityNew) getActivity()).mSelectedDate);

        if (SharedPrefManager.getInstance(getContext()).getUserGroup() != null && SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            mImageViewAddExpense.setVisibility(View.VISIBLE);
        } else {
            mImageViewAddExpense.setVisibility(View.GONE);
        }

        mImageViewAddExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission();
                openAddExpDialog();
            }
        });

        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
        bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount); //To equalize event column
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.EXP_DETAIL, bundle);
    }

    private void setDates() {
//        mDate = ((HomeActivityNew) getActivity()).mSelectedYear + "-" + ((HomeActivityNew) getActivity()).mSelectedMonth + "-" + ((HomeActivityNew) getActivity()).mSelectedDate;
        int monthCount = mMonth + 1;
        mDate = mYear + "-" + monthCount + "-" + ((HomeActivityNew) getActivity()).mSelectedDate;
        FWLogger.logInfo(TAG, "DATE : " + mDate);

        mDateView = ((HomeActivityNew) getActivity()).mSelectedDate + "-" + monthCount + "-" + mYear;
        FWLogger.logInfo(TAG, "DATE_VIEW : " + mDateView);

    }

    private void callAPI() {
       /* mExpenseDetailsAsyncTask = new ExpenseDetailsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mExpenseDetailsAsyncTask.execute(mUserId, mDate);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<ExpenseDetails> call = RetrofitClient.getInstance(getContext()).getMyApi().getExpenditureDetails(mUserId, mDate);
                call.enqueue(new retrofit2.Callback<ExpenseDetails>() {
                    @Override
                    public void onResponse(Call<ExpenseDetails> call, Response<ExpenseDetails> response) {
                        try {
                            if (response.code() == 200) {
                                FWLogger.logInfo(TAG, "List added Successfully");
                                ExpenseDetails expenseDetails = response.body();
                                mExpenseLists = new ArrayList<>();
                                mExpenseDetails = expenseDetails.getResultData();
                                //mExpenseDate.setText(mDateView);
                                currentMonthName = DateUtils.getMonthName(mMonth);
                                ///////////////////////
                                if (mExpenseDetails != null) {
                                    mTechFullName.setText(expenseDetails.getResultData().getFullName());
                                    mCreditedAmount.setText(String.valueOf(expenseDetails.getResultData().getCreditedAmonut()));
                                    mOpeningAmount.setText(String.valueOf(expenseDetails.getResultData().getOpeningBalance()));
                                    mEarnedAmount.setText(String.valueOf(expenseDetails.getResultData().getEarnedAmount()));
                                    mReturn.setText(String.valueOf(expenseDetails.getResultData().getReturnAmount()));
                                    mTotalExpense.setText(String.valueOf(expenseDetails.getResultData().getExpenses()));
                                    mRemainingAmount.setText(String.valueOf(expenseDetails.getResultData().getRemainingBalance()));
                                } else {
                                    mCreditedAmount.setText("0");
                                    mOpeningAmount.setText("0");
                                    mEarnedAmount.setText("0");
                                    mReturn.setText("0");
                                    mTotalExpense.setText("0");
                                }

                                ///////////////////
                                mExpenseLists = expenseDetails.getResultData().getExpenseList();
                                if (mExpenseLists != null && mExpenseLists.size() != 0) {
                                    mCardViewNA.setVisibility(View.GONE);
                                    mRecyclerViewExpenseList.setVisibility(View.VISIBLE);
                                    mLayoutManager = new LinearLayoutManager(getActivity());
                                    mRecyclerViewExpenseList.setLayoutManager(mLayoutManager);
                                    mExpenseListAdapter = new ExpenseListAdapterNew(getActivity(), mPosition, mExpenseLists);
                                    mExpenseListAdapter.setClickListener(recyclerTouchListener);
                                    mRecyclerViewExpenseList.setAdapter(mExpenseListAdapter);
                                } else {
                                    mCardViewNA.setVisibility(View.VISIBLE);
                                    mRecyclerViewExpenseList.setVisibility(View.GONE);
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ExpenseDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetTechnicianExpenditure? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTechnicianExpenditure? API:");
            ex.getMessage();
        }
    }

    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            },
                            ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }


    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        if (classType.equalsIgnoreCase(ExpenseDetails.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "List added Successfully");
            Gson gson = new Gson();
            mExpenseLists = new ArrayList<>();
            ExpenseDetails expenseDetails = gson.fromJson(urlConnectionResponse.resultData, ExpenseDetails.class);
            mExpenseDetails = expenseDetails.getResultData();
            mExpenseDate.setText(mDateView);

            currentMonthName = DateUtils.getMonthName(mMonth);
            ///////////////////////
            if (mExpenseDetails != null) {
                mTechFullName.setText(expenseDetails.getResultData().getFullName());
                mCreditedAmount.setText(String.valueOf(expenseDetails.getResultData().getCreditedAmonut()));
                mOpeningAmount.setText(String.valueOf(expenseDetails.getResultData().getOpeningBalance()));
                mEarnedAmount.setText(String.valueOf(expenseDetails.getResultData().getEarnedAmount()));
                mReturn.setText(String.valueOf(expenseDetails.getResultData().getReturnAmount()));
                mTotalExpense.setText(String.valueOf(expenseDetails.getResultData().getExpenses()));
                mRemainingAmount.setText(String.valueOf(expenseDetails.getResultData().getRemainingBalance()));
            } else {
                mCreditedAmount.setText("0");
                mOpeningAmount.setText("0");
                mEarnedAmount.setText("0");
                mReturn.setText("0");
                mTotalExpense.setText("0");
            }

            ///////////////////
            mExpenseLists = expenseDetails.getResultData().getExpenseList();
            if (mExpenseLists != null && mExpenseLists.size() != 0) {
                mCardViewNA.setVisibility(View.GONE);
                mRecyclerViewExpenseList.setVisibility(View.VISIBLE);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerViewExpenseList.setLayoutManager(mLayoutManager);
                mExpenseListAdapter = new ExpenseListAdapterNew(getActivity(), mPosition, mExpenseLists);
                mExpenseListAdapter.setClickListener(this);
                mRecyclerViewExpenseList.setAdapter(mExpenseListAdapter);
            } else {
                mCardViewNA.setVisibility(View.VISIBLE);
                mRecyclerViewExpenseList.setVisibility(View.GONE);
            }

        } else if (classType.equalsIgnoreCase(AddExpense.class.getSimpleName())) {
            Gson gson = new Gson();
            AddExpense addExpense = gson.fromJson(urlConnectionResponse.resultData, AddExpense.class);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
            bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.EXPENSE, bundle);
            if (addExpense != null) {
                if (addExpense.getCode().equalsIgnoreCase("200") && addExpense.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.expense_added_successfully, getContext()))) {
                    Toast.makeText(getContext(), getString(R.string.expense_added_successfully), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, "Expense added successfully.");
                    callAPI();
                } else {
                    Toast.makeText(getContext(), addExpense.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onClick(View view, int position) {
        if (view.getId() == R.id.expense_photo) {
            FWLogger.logInfo("ExpensePhoto : ", mExpenseLists.get(position).getExpensePhoto());
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.EXP_NAME, mExpenseLists.get(position).getExpenseName());
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.EXP_PHOTO, bundle);

            CommonDialog commonDialog = CommonDialog.getInstance();
            commonDialog.showImageInFullScreenMode(getActivity(), mExpenseLists.get(position).getExpensePhoto(), getResources().getString(R.string.expense_photo));
        } else {
            FWLogger.logInfo("position : ", "" + position);
            DatesDto datesDto = ((HomeActivityNew) getActivity()).mDatesLists.get(position);
            ((HomeActivityNew) getActivity()).mSelectedDate = datesDto.getDate();

            setDates();

            callAPI();
        }
    }

    public void getDatesList(Calendar cal, int currentMonth, int currentDay) {  //, int selectedMonthPos
//        cal.set(Calendar.MONTH, currentMonth - 1);
        cal.set(Calendar.MONTH, currentMonth);
        cal.set(Calendar.DAY_OF_MONTH, currentDay);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        FWLogger.logInfo(TAG, "Max Days : " + maxDay + " currentMonth : " + currentMonth);
        SimpleDateFormat df = new SimpleDateFormat("dd");
        if (((HomeActivityNew) getActivity()).mDatesLists != null)
            ((HomeActivityNew) getActivity()).mDatesLists.clear();

//        mDatesListAdapter = null;
        /*((HomeActivityNew) getActivity()).mDatesLists = new ArrayList<>();
        for (int i = 0; i < maxDay; i++) {
            DatesDto datesDto = new DatesDto();
            cal.set(Calendar.DAY_OF_MONTH, i + 1);
            if(mMonth == ((HomeActivityNew) getActivity()).currentMonth) {  //selectedMonthPos == 0
                if(DateUtils.getCurrentDateOnly() >= i) {
                    datesDto.setDate(Integer.parseInt(df.format(cal.getTime())));
                    datesDto.setDay(new SimpleDateFormat("EE", Locale.ENGLISH).format(cal.getTime()));
                    ((HomeActivityNew) getActivity()).mDatesLists.add(datesDto);
                }
            } else {
                datesDto.setDate(Integer.parseInt(df.format(cal.getTime())));
                datesDto.setDay(new SimpleDateFormat("EE", Locale.ENGLISH).format(cal.getTime()));
                ((HomeActivityNew) getActivity()).mDatesLists.add(datesDto);
            }
        }

        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setLayoutManager(mLayoutManager);
        mDatesListAdapter = new DatesListAdapter(getContext(), ((HomeActivityNew) getActivity()).mDatesLists, DateUtils.getCurrentDateOnly());
        mDatesListAdapter.setClickListener(this);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setAdapter(mDatesListAdapter);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.scrollToPosition(((HomeActivityNew) getActivity()).mSelectedDate-1);
        mDatesListAdapter.notifyDataSetChanged();*/

        setDates();
        callAPI();
    }

    public void addExpense(String expenseName, int amount) {
        mAmount = amount;
       /* addExpenseAsyncTask = new AddExpenseAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        addExpenseAsyncTask.execute(amount, expenseName, mUserId, mExpenseEncodedBaseString);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<AddExpense> call = RetrofitClient.getInstance(getContext()).getMyApi().addExpense(amount, expenseName, mUserId, mExpenseEncodedBaseString,
                        "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<AddExpense>() {
                    @Override
                    public void onResponse(Call<AddExpense> call, Response<AddExpense> response) {
                        try {
                            if (response.code() == 200) {
                                AddExpense addExpense = response.body();
                                Bundle bundle = new Bundle();
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
                                bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
                                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.EXPENSE, bundle);
                                if (addExpense != null) {
                                    if (addExpense.getCode().equalsIgnoreCase("200") && addExpense.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.expense_added_successfully, getContext()))) {
                                        Toast.makeText(getContext(), getString(R.string.expense_added_successfully), Toast.LENGTH_LONG).show();
                                        FWLogger.logInfo(TAG, "Expense added successfully.");
                                        callAPI();
                                    } else {
                                        Toast.makeText(getContext(), addExpense.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddExpense> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddExpense API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddExpense API:");
            ex.getMessage();
        }

    }

    public void openAddExpDialog() {
        ExpenditureDialog expenditureDialog = ExpenditureDialog.getInstance();
        expenditureDialog.addExpenseDialog(getContext(), this);
    }

    public void pickImage(String whichImage, int imageNo, ExpenseDetailsFragmentNew.ImageUpdateListener imageUpdateListener) {
        mWhichImage = whichImage;
        mImageNo = imageNo;
        mImageUpdateListener = imageUpdateListener;
        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        builder.setTitle(getResources().getString(R.string.select_option));
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(getActivity().getApplicationContext())) {
                        Toast.makeText(getActivity().getApplicationContext(),
                                getResources().getString(R.string.camera_not_supported),
                                Toast.LENGTH_LONG).show();
                    }
                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(ExpenseDetailsFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {

                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(ExpenseDetailsFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            //photo = new File(Environment.getExternalStorageDirectory(), getFileName() + ".jpg");
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            photo = File.createTempFile(getFileName(), ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCapturedImagePath = photo.getAbsolutePath();
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mImageUri = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider",
                photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private String getFileName() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        String imageFileName = timeStamp + "_" + mWhichImage + "_" + mImageNo + "_";
        return imageFileName;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            FWLogger.logInfo(TAG, "Camera");
            try {
                getActivity().getContentResolver().notifyChange(mImageUri, null);
                Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), mImageUri, mCapturedImagePath);
                //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                String filePath = CameraUtils.getFilePathFromURI(mImageUri, getContext());
                //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);

                if (mWhichImage.equalsIgnoreCase(mExpenseImage)) {
                    setExpensePhoto(bitmap);
                } else {
                    mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                }
            } catch (Exception e) {
                Toast.makeText(getActivity(), getResources().getString(R.string.failed_to_load), Toast.LENGTH_SHORT)
                        .show();
                FWLogger.logInfo("Camera", e.toString());
            }

        } else if (requestCode == BaseActivity.PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                FWLogger.logInfo(TAG, "Gallery");
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), selectedImage, null);
                    // bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                    String filePath = CameraUtils.getFilePathFromURI(selectedImage, getContext());
                    FWLogger.logInfo("filePath : ", filePath);
                    // bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                    bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                    if (mWhichImage.equalsIgnoreCase(mExpenseImage)) {
                        setExpensePhoto(bitmap);
                    } else {
                        mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(),
                        "Canceled", Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.sorry_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void setExpensePhoto(Bitmap bitmap) {
        ExpenditureDialog.mImageViewExpense.setBackground(null);
        mExpenseEncodedBaseString = ImageUtils.ConvertBitmapToString(bitmap);
        FWLogger.logInfo(TAG, "Expense_Base64 : " + mExpenseEncodedBaseString);
        ExpenditureDialog.mImageViewExpense.setImageBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                captureImage();
            } else if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String stringOfDate = month + 1 + "/" + year;
        FWLogger.logInfo(TAG, "stringOfDate : " + stringOfDate);
        ((HomeActivityNew) getActivity()).mSelectedDate = dayOfMonth;
//        mDay = dayOfMonth;
//        ((HomeActivityNew) getActivity()).mSelectedDate = dayOfMonth;
        mMonth = month;
        mYear = year;
        currentMonthName = DateUtils.getMonthName(mMonth);
        /*((HomeActivityNew) getActivity()).mEditTextYearMonth.setText(((HomeActivityNew) getActivity()).mSelectedDate + " " + currentMonthName + " " + mYear);*/
        mEditTextYearMonth.setText(((HomeActivityNew) getActivity()).mSelectedDate + " " + currentMonthName + " " + mYear);
        getDatesList(mCal, mMonth, ((HomeActivityNew) getActivity()).mSelectedDate);
    }


    public interface ImageUpdateListener {
        void onImageCaptured(Bitmap bitmap, String whichImage, int imageNo);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        ((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.GONE);
        //((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.GONE);
        super.onDestroyView();
    }
}
