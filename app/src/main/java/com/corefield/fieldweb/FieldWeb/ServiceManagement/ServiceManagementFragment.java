package com.corefield.fieldweb.FieldWeb.ServiceManagement;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.ServiceTypeListAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerItemInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show item inventory where Owner can add item and Assign Item
 */
public class ServiceManagementFragment extends Fragment implements View.OnClickListener, RecyclerTouchListener {
    protected static String TAG = ServiceManagementFragment.class.getSimpleName();
    public List<ServiceTypeListDTO.ResultData> mServiceList = null;
    public List<UsersList.ResultData> mUsersLists = null;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    private View mRootView;
    ImageView noResultImg;
    private String mSearchParam = "";

    private RecyclerView mRecyclerViewItemsList;
    private ServiceTypeListAdapter mServiceTypeListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean mAllItemApiCall = false, mItemListApiCall = false, mUserListApiCall = false;
    private Button mButtonInventory, mButtonIssuedItems, mAddService;
    private String mSelection = "";
    private SearchView mSearchView;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;
    //
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START;
    private boolean isLoadFirstTime = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.service_management_fragment, container, false);
        mButtonInventory = mRootView.findViewById(R.id.button_inventory);
        mButtonIssuedItems = mRootView.findViewById(R.id.button_issued_item);
        mAddService = mRootView.findViewById(R.id.plus_service);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        noResultImg = mRootView.findViewById(R.id.noResultImg);
        recyclerTouchListener = this;
        fragment = this;

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        getServiceTypeList(recyclerTouchListener, fragment, mSearchParam);
        mRecyclerViewItemsList = mRootView.findViewById(R.id.recycler_item_list);

        FGALoadEvent();
        mSelection = "INV";
        mButtonInventory.setOnClickListener(this);
        mButtonIssuedItems.setOnClickListener(this);
        mAddService.setOnClickListener(this);

        mAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                    addUpdateServiceDialog.addService(getContext(), activity);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

/*
        mRecyclerViewItemsList.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
*/


        return mRootView;
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        //getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
        getServiceTypeList(recyclerTouchListener, fragment, mSearchParam);
    }

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }


    public void getServiceTypeList(RecyclerTouchListener recyclerTouchListener, Fragment fragment, String mSearchParam) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<ServiceTypeListDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getServiceTypeList(userID, mSearchParam, mCurrentPage,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<ServiceTypeListDTO>() {
                    @Override
                    public void onResponse(Call<ServiceTypeListDTO> call, Response<ServiceTypeListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                FWLogger.logInfo(TAG, "Get Services list");
                                mServiceList = new ArrayList<>();
                                ServiceTypeListDTO serviceTypeListDTO = response.body();
                                mServiceList = serviceTypeListDTO.getResultData();
                                if (mServiceList.size() > 0) {
                                    mRecyclerViewItemsList.setVisibility(View.VISIBLE);
                                    mItemsNameList = new ArrayList<>();
                                    for (ServiceTypeListDTO.ResultData resultData : mServiceList) {
                                        mItemsNameList.add(resultData.getServiceName());
                                        FWLogger.logInfo(TAG, resultData.getServiceName());
                                    }
                                    mLayoutManager = new LinearLayoutManager(getActivity());
                                    mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
                                    mRecyclerViewItemsList.setItemAnimator(new DefaultItemAnimator());
                                    mServiceTypeListAdapter = new ServiceTypeListAdapter(getActivity(), mServiceList, recyclerTouchListener, fragment);
                                    mRecyclerViewItemsList.setAdapter(mServiceTypeListAdapter);
                                    mServiceTypeListAdapter.setClickListener(recyclerTouchListener);

                                    setSearchFilter();
                                } else {
                                    mRecyclerViewItemsList.setVisibility(View.GONE);
                                    noResultImg.setVisibility(View.VISIBLE);
                                    noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceTypeListDTO> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in GetServiceTypeList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
            ex.getMessage();
        }
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        switch (v.getId()) {
            case R.id.button_inventory:
                break;

            case R.id.button_issued_item:
                break;

            default:
                break;
        }
    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ITEM_INVENTORY, bundle);
    }

    private void refresh() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        ServiceManagementFragment myFragment = new ServiceManagementFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }


    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;

            case R.id.image_item_update:

                break;

            case R.id.image_issue_item_update:
                break;

            default:
                break;
        }
    }

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);

        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mSelection.equalsIgnoreCase("INV")) {
                    // filter recycler view when query submitted
                    if (mServiceTypeListAdapter != null)
                        mServiceTypeListAdapter.getFilter().filter(query);
                    mSearchParam = query;
                    mockingNetworkDelay(query, 0, 0);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mSelection.equalsIgnoreCase("INV")) {
                    // filter recycler view when query submitted
                    if (mServiceTypeListAdapter != null)
                        mServiceTypeListAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    private void mockingNetworkDelay(String searchParam, int typeId, int statusId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getServiceTypeList(recyclerTouchListener, fragment, mSearchParam);
            }
        }, 1000);
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }

}
