package com.corefield.fieldweb.FieldWeb;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


/**
 * Activity for Base Activity
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Activity  class act as Base Class for all activity
 */
public class BaseActivity extends AppCompatActivity {

    public static final String PERMISSION_REQUEST_ACCESS_LOCATION = "location permission";
    public static final String PERMISSION_REQUEST_ACCESS_CAMERA = "camera permission";
    public static final String PERMISSION_REQUEST_ACCESS_GALLERY = "gallery permission";
    public static final String PERMISSION_REQUEST_REC_AUDIO = "Rec Audio permission";
    //    public static final String PERMISSION_REQUEST_PHONE_CALL = "phone call";
//    public static final String PERMISSION_REQUEST_SEND_SMS = "send sms";
    public static final String PERMISSION_REQUEST_ACCESS_ALL = "all permission";
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PICK_IMAGE_GALLERY = 2;
    private static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private static final int REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING = 1001;
    private static final int REQ_CODE_VERSION_UPDATE = 530;
    private static final int DAYS_FOR_FLEXIBLE_UPDATE = 1;
    private static final int HIGH_PRIORITY_UPDATE = 3;
    private String TAG = BaseActivity.class.getSimpleName();
    private BaseActivity.RequestPermissionsUpdateListener mListener;
    private String PERMISSION_REQUESTED_FOR = "";
    private boolean isDialogShowing = false;
    private AppUpdateManager mAppUpdateManager;
    private InstallStateUpdatedListener mInstallStateUpdatedListener;
    int checkBackgroundLocation;
    //FakeAppUpdateManager mFakeAppUpdateManager = null;

//    String[] permissionsRequired = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE,
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_BACKGROUND_LOCATION};

    public void setRequestPermissionsUpdateListener(BaseActivity.RequestPermissionsUpdateListener listener) {
        this.mListener = listener;
    }

    public boolean checkAllPermissionEnabled() {
        boolean flag = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //final int checkCamera = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.CAMERA);
            // final int checkStorage = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int checkFineLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int checkCoarseLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                flag = true;
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                checkBackgroundLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                if (/*checkCamera == PackageManager.PERMISSION_GRANTED && checkStorage == PackageManager.PERMISSION_GRANTED
                        &&*/ checkFineLocation == PackageManager.PERMISSION_GRANTED && checkCoarseLocation == PackageManager.PERMISSION_GRANTED && checkBackgroundLocation == PackageManager.PERMISSION_GRANTED) {  //&& checkPhoneCall == PackageManager.PERMISSION_GRANTED && checkSendSMS == PackageManager.PERMISSION_GRANTED

                    flag = true;
                }
            } else {
                if (/*checkCamera == PackageManager.PERMISSION_GRANTED && checkStorage == PackageManager.PERMISSION_GRANTED
                        &&*/ checkFineLocation == PackageManager.PERMISSION_GRANTED && checkCoarseLocation == PackageManager.PERMISSION_GRANTED) {  //&& checkPhoneCall == PackageManager.PERMISSION_GRANTED && checkSendSMS == PackageManager.PERMISSION_GRANTED

                    flag = true;
                }
            }
        } else {
            flag = true;
        }
        return flag;
    }

    public boolean requestToEnableAllPermission(BaseActivity.RequestPermissionsUpdateListener listener, String permissionRequestedFeature) {
        //NOTE: Log GA event
        /*Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID,SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME,BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE,SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM,FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(BaseActivity.this).logEvent(FirebaseGoogleAnalytics.Event.ENABLE_PERMISSION, bundle);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PERMISSION_REQUESTED_FOR = permissionRequestedFeature;
            // final int checkCamera = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.CAMERA);
            //  final int checkStorage = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int checkFineLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int checkCoarseLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                checkBackgroundLocation = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                if (/*checkCamera != PackageManager.PERMISSION_GRANTED || checkStorage != PackageManager.PERMISSION_GRANTED
                        ||*/ checkFineLocation != PackageManager.PERMISSION_GRANTED || checkCoarseLocation != PackageManager.PERMISSION_GRANTED || checkBackgroundLocation != PackageManager.PERMISSION_GRANTED) {  //|| checkPhoneCall != PackageManager.PERMISSION_GRANTED || checkSendSMS != PackageManager.PERMISSION_GRANTED
                    setRequestPermissionsUpdateListener(listener);
                    //Check and set permission above Android 10
                    checkAndSetPermissions();
                    return false;
                } else {
                    // All permission granted
                    //if user grant permission from outside
                    // continue with the screen
                    return true;
                }
            } else {
                if (/*checkCamera != PackageManager.PERMISSION_GRANTED || checkStorage != PackageManager.PERMISSION_GRANTED
                        ||*/ checkFineLocation != PackageManager.PERMISSION_GRANTED || checkCoarseLocation != PackageManager.PERMISSION_GRANTED) {  //|| checkPhoneCall != PackageManager.PERMISSION_GRANTED || checkSendSMS != PackageManager.PERMISSION_GRANTED
                    setRequestPermissionsUpdateListener(listener);
                    //Check and set permission Below Android 10
                    checkAndPerSetBelowAndroidQ();
                    return false;
                } else {
                    // All permission granted
                    //if user grant permission from outside
                    // continue with the screen
                    return true;
                }
            }
        } else {
            // device is not  M or  M>
            // continue with the screen
            return true;
        }
    }

    private void checkAndPerSetBelowAndroidQ() {
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CAMERA)) {
            requestPermissionBelowAndroidQ();
        } else*/
        if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissionBelowAndroidQ();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissionBelowAndroidQ();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestPermissionBelowAndroidQ();
        } else {
            requestPermissionBelowAndroidQ();
        }
    }

    private void checkAndSetPermissions() {
       /* if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CAMERA)) {
            requestRunTomePermissions();
        } else *//*if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestRunTomePermissions();
        } else*/
        if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestRunTomePermissions();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestRunTomePermissions();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
            requestRunTomePermissions();
        } else {
            requestRunTomePermissions();
        }
    }

    private void requestPermissionBelowAndroidQ() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                    /*  Manifest.permission.CAMERA,*/
                    /* Manifest.permission.WRITE_EXTERNAL_STORAGE,*/
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    /* Manifest.permission.RECORD_AUDIO*/}, REQUEST_CODE_PERMISSIONS);
        }
    }

    private void requestRunTomePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                    /* Manifest.permission.CAMERA,*/
                    /* Manifest.permission.WRITE_EXTERNAL_STORAGE,*/
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    /* Manifest.permission.RECORD_AUDIO*/}, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FWLogger.logInfo(TAG, "onCreate");
        //setContentView(R.layout.activity_main);
        //Note : Uncomment below line's of code to simulate the fake app update behaviour in Internal Test Track by Google play store
        //mFakeAppUpdateManager = new FakeAppUpdateManager(this);
        //Update Available
        //mFakeAppUpdateManager.setUpdateAvailable(4);
        //mFakeAppUpdateManager.setTotalBytesToDownload(400000);
        //mFakeAppUpdateManager.setPartiallyAllowedUpdateType(AppUpdateType.IMMEDIATE);

        //Flexible Update
        //mFakeAppUpdateManager.setPartiallyAllowedUpdateType(AppUpdateType.FLEXIBLE);
        //mFakeAppUpdateManager.setClientVersionStalenessDays(1);
        //mFakeAppUpdateManager.setUpdatePriority(2);

        //IMMEDIATE Update
        //mFakeAppUpdateManager.setPartiallyAllowedUpdateType(AppUpdateType.IMMEDIATE);
        //mFakeAppUpdateManager.setClientVersionStalenessDays(4);
        //mFakeAppUpdateManager.setUpdatePriority(5);

        checkForAppUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "onResume");
        checkNewAppVersionState();
    }

    @Override
    protected void onPause() {
        FWLogger.logInfo(TAG, "onPause");
        super.onPause();
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(BaseActivity.this,
                            permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[1]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[2]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[3]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[4]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[5]) == PackageManager.PERMISSION_GRANTED) {
                        //Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(this, "100: Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case 101: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(BaseActivity.this,
                            permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[1]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[2]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[3]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[4]) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(BaseActivity.this,
                                    permissionsRequired[5]) == PackageManager.PERMISSION_GRANTED) {
                    }
                }
//                else {
//                    Toast.makeText(this, "101 : Permission Denied", Toast.LENGTH_SHORT).show();
//                }
                return;
            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                boolean userAllowed = true;
                for (final int result : grantResults) {
                    userAllowed &= result == PackageManager.PERMISSION_GRANTED;
                }
                if (userAllowed) {
                    // All permissions granted
                    try {
                        mListener.onRequestPermissionsUpdate(true, PERMISSION_REQUESTED_FOR);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
               /* else {
                    if (!isDialogShowing) {
                        isDialogShowing = true;
                        android.app.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                        alertDialogBuilder.setTitle(R.string.permissions_required)
                                *//*  .setMessage("You have denied permissions to some features which are required by this app. " +
                                          "Below is the list of features which should be enabled.\n * Camera\n * Storage\n * Location \nPlease go to settings and enable permissions.")*//*
                                .setMessage("You have denied permissions to some features which are required by this app. " +
                                        "Below is the list of features which should be enabled. Location \nPlease go to settings and enable permissions.")
                                .setPositiveButton(R.string.open_settings, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Code to go to setting app
                                        //or continue with the screen
                                        isDialogShowing = false;
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.setData(Uri.fromParts("package", getPackageName(), null));
                                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivityForResult(intent, REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING);
                                    }
                                })
                                .setCancelable(false)
                                .create()
                                .show();
                    }
                }*/
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        FWLogger.logInfo(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING) {
            if (checkAllPermissionEnabled()) {
                mListener.onRequestPermissionsUpdate(true, PERMISSION_REQUESTED_FOR);
            }
        } else if (requestCode == REQ_CODE_VERSION_UPDATE) {
            FWLogger.logInfo(TAG, "REQ_CODE_VERSION_UPDATE");
            if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                FWLogger.logInfo(TAG, "Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                unregisterInstallStateUpdListener();
                finish();
            } else if (resultCode == RESULT_OK) {
                //NOTE: Log GA event
                /*Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID,SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                bundle.putInt(FirebaseGoogleAnalytics.Param.VERSION_CODE, BuildConfig.VERSION_CODE);
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME,BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE,SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM,FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(BaseActivity.this).logEvent(FirebaseGoogleAnalytics.Event.APP_UPDATE_CONF, bundle);*/

                // Make it true to know user first time opens the app after version update
                SharedPrefManager.getInstance(getApplicationContext()).setFirstTime(true);
            }
        }
    }

    @Override
    protected void onDestroy() {
        FWLogger.logInfo(TAG, "onDestroy");
        super.onDestroy();
        unregisterInstallStateUpdListener();
    }

    private void checkForAppUpdate() {
        FWLogger.logInfo(TAG, "checkForAppUpdate");
        // Creates instance of the manager.
        mAppUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();

        // Create a listener to track request state updates.
        mInstallStateUpdatedListener = new InstallStateUpdatedListener() {
            @Override
            public void onStateUpdate(InstallState installState) {
                // Show module progress, log state, or install the update.
                if (installState.installStatus() == InstallStatus.DOWNLOADED) {
                    FWLogger.logInfo(TAG, "onStateUpdate checkForAppUpdate");
                    // After the update is downloaded, show a notification
                    // and request user confirmation to restart the app.
                    popupSnackbarForCompleteUpdateAndUnregister();
                }
            }
        };

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                FWLogger.logInfo(TAG, "addOnSuccessListener checkForAppUpdate");
                //NOTE: Log GA event
                /*Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID,SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                bundle.putInt(FirebaseGoogleAnalytics.Param.VERSION_CODE, BuildConfig.VERSION_CODE);
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME,BuildConfig.VERSION_NAME);
                bundle.putBoolean(FirebaseGoogleAnalytics.Param.UPDATE_AVAILABLE,appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE );
                bundle.putInt(FirebaseGoogleAnalytics.Param.AVAILABLE_VERSION_CODE,appUpdateInfo.availableVersionCode());
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE,SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM,FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(BaseActivity.this).logEvent(FirebaseGoogleAnalytics.Event.APP_UPDATE_RECEIVED, bundle);*/
                // Request the update.
                //NOTE: Currently we are not interested in showing the FLEXIBLE update flow.we will implement this once we have achieved the stable build/development
                /*if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
                        && appUpdateInfo.updatePriority() < HIGH_PRIORITY_UPDATE
                        && (appUpdateInfo.clientVersionStalenessDays() != null
                        && appUpdateInfo.clientVersionStalenessDays() > DAYS_FOR_FLEXIBLE_UPDATE)) {
                    // Before starting an update, register a listener for updates.
                    mAppUpdateManager.registerListener(mInstallStateUpdatedListener);
                    // Start an update.
                    startAppUpdateFlexible(appUpdateInfo);
                } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                        && appUpdateInfo.updatePriority() >= HIGH_PRIORITY_UPDATE
                        && (appUpdateInfo.clientVersionStalenessDays() != null
                        && appUpdateInfo.clientVersionStalenessDays() > DAYS_FOR_FLEXIBLE_UPDATE)) {
                    // Start an update.
                    startAppUpdateImmediate(appUpdateInfo);
                }*/
                // Start immediate update.
                startAppUpdateImmediate(appUpdateInfo);
            }
        });
    }

    private void startAppUpdateImmediate(AppUpdateInfo appUpdateInfo) {
        FWLogger.logInfo(TAG, "startAppUpdateImmediate");
        try {
            // COMMENTED BY MANISH : AS APP IS GETTING LOGOUT AFTER NEW RELEASE
            //SharedPrefManager.getInstance(this).logout(false);
            mAppUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void startAppUpdateFlexible(AppUpdateInfo appUpdateInfo) {
        FWLogger.logInfo(TAG, "startAppUpdateFlexible");
        try {
            //SharedPrefManager.getInstance(this).logout(false);
            mAppUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            unregisterInstallStateUpdListener();
        }
    }

    /**
     * Displays the snackbar notification and call to action.
     * Needed only for Flexible app update
     */
    private void popupSnackbarForCompleteUpdateAndUnregister() {
        FWLogger.logInfo(TAG, "popupSnackbarForCompleteUpdateAndUnregister");
        Snackbar snackbar = Snackbar.make(new View(this), getString(R.string.update_downloaded), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.restart, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAppUpdateManager.completeUpdate();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();

        unregisterInstallStateUpdListener();
    }

    /**
     * Checks that the update is not stalled during 'onResume()'.
     * However, you should execute this check at all app entry points.
     */
    private void checkNewAppVersionState() {
        FWLogger.logInfo(TAG, "checkNewAppVersionState");
        mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {
            //FLEXIBLE:
            // If the update is downloaded but not installed,
            // notify the user to complete the update.
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                FWLogger.logInfo(TAG, "addOnSuccessListener checkNewAppVersionState DOWNLOADED");
                popupSnackbarForCompleteUpdateAndUnregister();
            }

            //IMMEDIATE:
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                FWLogger.logInfo(TAG, "addOnSuccessListener checkNewAppVersionState");
                // If an in-app update is already running, resume the update.
                startAppUpdateImmediate(appUpdateInfo);
            }
        });

    }

    /**
     * Needed only for FLEXIBLE update
     */
    private void unregisterInstallStateUpdListener() {
        FWLogger.logInfo(TAG, "unregisterInstallStateUpdListener");
        if (mAppUpdateManager != null && mInstallStateUpdatedListener != null)
            mAppUpdateManager.unregisterListener(mInstallStateUpdatedListener);
    }

    public interface RequestPermissionsUpdateListener {
        void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature);
    }

    public static String getLocaleStringResource(Locale requestedLocale, int resourceId, Context context) {
        String result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // use latest api
            Configuration config = new Configuration(context.getResources().getConfiguration());
            config.setLocale(requestedLocale);
            result = context.createConfigurationContext(config).getText(resourceId).toString();
        } else { // support older android versions
            Resources resources = context.getResources();
            Configuration conf = resources.getConfiguration();
            Locale savedLocale = conf.locale;
            conf.locale = requestedLocale;
            resources.updateConfiguration(conf, null);

            // retrieve resources from desired locale
            result = resources.getString(resourceId);

            // restore original locale
            conf.locale = savedLocale;
            resources.updateConfiguration(conf, null);
        }

        return result;
    }

    /////////////////////////////////Not in Use//////////////////////////////////////////////////

    //Oncreate{

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }
    */
    // If a notification message is tapped, any data accompanying the notification
    // message is available in the intent extras. In this sample the launcher
    // intent is fired when the notification is tapped, so any accompanying data would
    // be handled here. If you want a different intent fired, set the click_action
    // field of the notification message to the desired intent. The launcher intent
    // is used when no click_action is specified.
    //
    // Handle possible data accompanying notification message.
    // [START handle_data_extras]
       /* if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }*/
    // [END handle_data_extras]
    //displayFirebaseRegId();
    //}


    // Fetches reg id from shared preferences
    // and displays on the screen
    //private void displayFirebaseRegId() {
    //  String regId = SharedPrefManager.getInstance(BaseActivity.this).getRegFCMID();
    //Log.e(TAG, "Firebase reg id: " + regId);
    //}

}