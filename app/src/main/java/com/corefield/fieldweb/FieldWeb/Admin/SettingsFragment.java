package com.corefield.fieldweb.FieldWeb.Admin;


import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog;
import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;
import com.corefield.fieldweb.FieldWeb.Home.TechDashboardFragmentNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

/**
 * Fragment Controller for HelpAndSupportFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Help and Support
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {
    protected static String TAG = SettingsFragment.class.getSimpleName();
    private View mRootView;

    ImageButton arrow;
    LinearLayout hiddenView;
    RelativeLayout cardView;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_settings, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.settings));

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {

        cardView = mRootView.findViewById(R.id.base_cardview);
        ImageButton arrow = mRootView.findViewById(R.id.arrow_button);
        hiddenView = mRootView.findViewById(R.id.hidden_view);

        CardView cardViewAppTour = mRootView.findViewById(R.id.cardView_AppTour);
        CardView cardViewRateUs = mRootView.findViewById(R.id.card_RateUs);
        CardView cardViewShareFeedback = mRootView.findViewById(R.id.card_ShareFeedback);

        CardView cardViewChangeLang = mRootView.findViewById(R.id.cardView_change_lang);
        CardView cardViewDeleteAccount = mRootView.findViewById(R.id.cardViewDeleteAccount);
        CardView cardViewTermsConditions = mRootView.findViewById(R.id.cardViewTermsConditions);
        CardView cardView_privacy_policy = mRootView.findViewById(R.id.cardView_privacy_policy);
        CardView cardView_refund_policy = mRootView.findViewById(R.id.cardView_refund_policy);
        CardView cardView_about = mRootView.findViewById(R.id.cardView_about);
        CardView cardViewInviteFriends = mRootView.findViewById(R.id.cardView_invite_friends);

        ImageView imageViewCall = mRootView.findViewById(R.id.imageView_call);
        ImageView imageViewMail = mRootView.findViewById(R.id.imageView_email);
        ImageView imageViewLocation = mRootView.findViewById(R.id.imageView_location);

        cardViewAppTour.setOnClickListener(this);
        cardViewRateUs.setOnClickListener(this);
        cardViewShareFeedback.setOnClickListener(this);

        cardViewChangeLang.setOnClickListener(this);
        cardViewDeleteAccount.setOnClickListener(this);
        cardViewTermsConditions.setOnClickListener(this);
        cardView_privacy_policy.setOnClickListener(this);
        cardView_refund_policy.setOnClickListener(this);
        cardView_about.setOnClickListener(this);
        cardViewInviteFriends.setOnClickListener(this);

        arrow.setOnClickListener(this);
        imageViewCall.setOnClickListener(this);
        imageViewMail.setOnClickListener(this);
        imageViewLocation.setOnClickListener(this);

        //
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            cardViewInviteFriends.setVisibility(View.VISIBLE);
        } else {
            cardViewInviteFriends.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

        switch (v.getId()) {
            case R.id.cardView_invite_friends:
                inviteFriendForOwner();
                break;
           /* case R.id.textView_change_pass:
                bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.change_password));
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                PasswordDialog passwordDialog = PasswordDialog.getInstance();
                passwordDialog.changePasswordDialog(getActivity(), false, SharedPrefManager.getInstance(getContext()).getUserId());
                break;*/
            case R.id.arrow_button:
                if (hiddenView.getVisibility() == View.VISIBLE) {
                    // The transition of the hiddenView is carried out by the TransitionManager class.
                    // Here we use an object of the AutoTransition Class to create a default transition
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    hiddenView.setVisibility(View.GONE);
                    //arrow.setImageResource(R.drawable.ic_down_arrow);
                }

                // If the CardView is not expanded, set its visibility to
                // visible and change the expand more icon to expand less.
                else {
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    hiddenView.setVisibility(View.VISIBLE);
                    //arrow.setImageResource(R.drawable.ic_up_arrow);
                }
                break;

            case R.id.cardView_AppTour:
                HomeActivityNew.onDemandAppTourFlag = true;
                if (SharedPrefManager.getInstance(getActivity().getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                    ((HomeActivityNew) getActivity()).loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
                } else {
                    ((HomeActivityNew) getActivity()).loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
                }
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.fieldweb);
                break;

            case R.id.card_RateUs:
                ((HomeActivityNew) getActivity()).PlayStoreRatingDialog(getActivity());
                break;

            case R.id.card_ShareFeedback:
                loadFragment(new SuggestionFeedbackFragment());
                break;

            case R.id.cardView_change_lang:
                bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_change_language));
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                UserDialog userDialogL = UserDialog.getInstance();
                userDialogL.changeLanguageDialog(getActivity());
                break;

            case R.id.cardViewDeleteAccount:
                bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.delete_account));
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).DeleteOwnerSelfDialog(getActivity());
                break;

            case R.id.cardViewTermsConditions:
                loadFragment(new TermsAndConditionsFragment());
                break;

            case R.id.cardView_privacy_policy:
                loadFragment(new PrivacyPolicyFragment());
                break;

            case R.id.cardView_refund_policy:
                loadFragment(new RefundPolicyFragment());
                break;

            case R.id.cardView_about:
                loadFragment(new AboutUsFragment());
                break;

            case R.id.imageView_call:
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.CONTACT_SUPPORT, bundle);

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "+91 9344906855"));
                startActivity(intent);
                break;

            case R.id.imageView_email:
                Intent intentEmail = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
//                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
//                intentEmail.putExtra(Intent.EXTRA_TEXT, "Body of email");
                intentEmail.setData(Uri.parse("mailto:info@fieldweb.co.in")); // or just "mailto:" for blank
                intentEmail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intentEmail);
                break;

            case R.id.imageView_location:
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 28.355560f, 76.947730f);
                Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intentMap);
                break;

            default:
                break;
        }
    }

    private void inviteFriendForOwner() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Namaste \uD83D\uDE4F,\n" +
                    "I am using India's #1 field business management app - \uD83D\uDCA5 FieldWeb \n\n" +
                    "Benefits of using FieldWeb: \n\n" +
                    "• Revenue increased \uD83D\uDCC8 by 75% \n" +
                    "• Keeps data Safe \uD83D\uDEE1 and Secure. \n" +
                    "• Tracks fieldworker's \uD83D\uDC68\u200D\uD83D\uDD27 activity. \n" +
                    "• Assign task/job to fieldworkers \uD83D\uDC68\u200D\uD83D\uDD27. \n" +
                    "• Sends service reminder to customers \uD83D\uDC68\u200D\uD83D\uDC68\u200D\uD83D\uDC67. \n\n" +
                    "Download Now: \n\n " +
                    "https://bit.ly/3MZiwEJ");
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }
}
