package com.corefield.fieldweb.FieldWeb.Admin;


import static com.facebook.FacebookSdk.getApplicationContext;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

/**
 * Fragment Controller for AddonsFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to Addons +
 */
public class AddonsFragment extends Fragment implements View.OnClickListener {
    protected static String TAG = AddonsFragment.class.getSimpleName();
    private View mRootView;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_addons, container, false);
        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.addons));

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        CardView cardViewcustomer = mRootView.findViewById(R.id.card_customer_app);
        CardView cardViewfieldfinz = mRootView.findViewById(R.id.card_owner_tech_ff);
        CardView cardViewfieldstaffing = mRootView.findViewById(R.id.card_owner_fs);
        CardView cardViewfieldtrade = mRootView.findViewById(R.id.card_owner_tech_ft);
        CardView cardViewfieldfixit = mRootView.findViewById(R.id.card_owner_fixit);
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {

            cardViewcustomer.setOnClickListener(this);
            cardViewfieldfinz.setOnClickListener(this);
            cardViewfieldstaffing.setOnClickListener(this);
            cardViewfieldtrade.setOnClickListener(this);
            cardViewfieldfixit.setOnClickListener(this);

        } else {
            cardViewfieldfinz.setOnClickListener(this);
            cardViewfieldtrade.setOnClickListener(this);

            cardViewcustomer.setVisibility(View.GONE);
            cardViewfieldstaffing.setVisibility(View.GONE);
            cardViewfieldfixit.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        switch (v.getId()) {
           /* case R.id.textView_change_pass:
                bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.change_password));
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                PasswordDialog passwordDialog = PasswordDialog.getInstance();
                passwordDialog.changePasswordDialog(getActivity(), false, SharedPrefManager.getInstance(getContext()).getUserId());
                break;*/
            case R.id.card_customer_app:
                loadFragment(new CustomerAppFragment());
                break;
            case R.id.card_owner_tech_ff:
                loadFragment(new FieldFinzFragment());
                break;
            case R.id.card_owner_fs:
                loadFragment(new FieldStaffingFragment());
                break;
            case R.id.card_owner_tech_ft:
                loadFragment(new FieldTradesFragment());
                //Toast.makeText(getApplicationContext(), "Coming Soon....", Toast.LENGTH_SHORT).show();
                break;
            case R.id.card_owner_fixit:
                loadFragment(new FieldWebFixitFragment());
                break;
            default:
                break;
        }
    }


    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        //args.putString("TYPE", "terms");
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }
}