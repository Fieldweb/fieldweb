package com.corefield.fieldweb.FieldWeb.Admin;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.UsersListAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Delete.DeleteOwnerTechnician;
import com.corefield.fieldweb.DTO.User.TechnicianList;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for TechnicianListFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used show  technician list (owner login)
 */
public class TechnicianListFragment extends Fragment implements OnTaskCompleteListener, RecyclerTouchListener, View.OnClickListener {
    protected static String TAG = TechnicianListFragment.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewUsersList;
    private ImageView mImageViewDownload;
    private int mPosition;
    private UsersListAdapter mUsersListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<UsersList.ResultData> mUsersLists = null;
    private TechnicianList mTechnicianList;
    private static final String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.INTERNET};

    private static final String[] PERMISSIONS_TIRAMISU = {android.Manifest.permission.READ_MEDIA_VIDEO, android.Manifest.permission.READ_MEDIA_IMAGES, android.Manifest.permission.READ_MEDIA_AUDIO};
    private SearchView mSearchView;
    public static Context mcontext;
    Button plustech;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_employee_list, container, false);

        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_LIST, bundle);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        recyclerTouchListener = this;
        fragment = this;

        mImageViewDownload = mRootView.findViewById(R.id.imageView_download);
        plustech = mRootView.findViewById(R.id.plus_tech);
        //mImageViewDownload.setVisibility(View.VISIBLE);
        mcontext = getContext();
        mImageViewDownload.setOnClickListener(this);
        plustech.setOnClickListener(this);
        mRecyclerViewUsersList = mRootView.findViewById(R.id.recycler_user_list);
        getTechnicianList();
        return mRootView;
    }

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);
        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mUsersListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mUsersListAdapter.getFilter().filter(query);
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    private void getTechnicianList() {
       /* mUsersListAsyncTask = new UsersListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUsersListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<UsersList> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getAllUsersList(userID);
                call.enqueue(new retrofit2.Callback<UsersList>() {
                    @Override
                    public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                FWLogger.logInfo(TAG, "List added Successfully");
                                UsersList usersList = response.body();
                                mUsersLists = new ArrayList<>();
                                mUsersLists = usersList.getResultData();
                                if (mUsersLists.size() != 0) {
                                    mImageViewDownload.setVisibility(View.VISIBLE);
                                    mLayoutManager = new LinearLayoutManager(getActivity());
                                    mRecyclerViewUsersList.setLayoutManager(mLayoutManager);
                                    mUsersListAdapter = new UsersListAdapter(getActivity(), mPosition, mUsersLists, recyclerTouchListener, fragment);
                                    mUsersListAdapter.setClickListener(recyclerTouchListener);
                                    mRecyclerViewUsersList.setAdapter(mUsersListAdapter);
                                    setSearchFilter();
                                    // SHOW ONLY FIRST TIME::
                                    if (SharedPrefManager.getInstance(getActivity().getApplicationContext()).isFirstTimeAddMoreTechPopup()) {
                                        AddTech_CreateTask_OnceDialog(getActivity());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UsersList> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in AllUsersList? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(getActivity());
            FWLogger.logInfo(TAG, "Exception in AllUsersList? API:");
            ex.getMessage();
        }
    }

    private void getDownloadTechList() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<TechnicianList> call = RetrofitClient.getInstance(getContext()).getMyApi().getDownloadTechList(userID);
                call.enqueue(new retrofit2.Callback<TechnicianList>() {
                    @Override
                    public void onResponse(Call<TechnicianList> call, Response<TechnicianList> response) {
                        try {
                            if (response.code() == 200) {
                                TechnicianList mTechnicianList = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.DOWNLOAD);
                                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_LIST, bundle);
                                if (mTechnicianList.getCode().equalsIgnoreCase("200") && mTechnicianList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "TechList download Successfully");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                        if (!hasPermissions(getContext(), PERMISSIONS_TIRAMISU)) {
                                            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                            Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                            t.show();
                                        } else {
                                            DownloadFile.downloadFile(mTechnicianList.getResultData().getTechFilePath(), getActivity());
                                        }
                                    } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                                        if (!hasPermissions(getContext(), PERMISSIONS)) {
                                            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                            Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                            t.show();
                                        } else {
                                            DownloadFile.downloadFile(mTechnicianList.getResultData().getTechFilePath(), getActivity());
                                        }
                                    }
                                   /* if (!hasPermissions(getContext(), PERMISSIONS)) {
                                        Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                        Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                        t.show();
                                    } else {
                                        DownloadFile.downloadFile(mTechnicianList.getResultData().getTechFilePath(), getActivity());
                                    }*/
                                } else {
                                    Toast.makeText(getActivity(), mTechnicianList.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TechnicianList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllTechnicianListDownloadForMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllTechnicianListDownloadForMobile API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(UsersList.class.getSimpleName())) {
                FWLogger.logInfo(TAG, "List added Successfully");
                Gson gson = new Gson();
                mUsersLists = new ArrayList<>();
                UsersList usersList = gson.fromJson(urlConnectionResponse.resultData, UsersList.class);
                mUsersLists = usersList.getResultData();
                if (mUsersLists.size() != 0) {
                    mImageViewDownload.setVisibility(View.VISIBLE);
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mRecyclerViewUsersList.setLayoutManager(mLayoutManager);
                    mUsersListAdapter = new UsersListAdapter(getActivity(), mPosition, mUsersLists, this, this);
                    mUsersListAdapter.setClickListener(this);
                    mRecyclerViewUsersList.setAdapter(mUsersListAdapter);
                    setSearchFilter();
                    // SHOW ONLY FIRST TIME::
                    if (SharedPrefManager.getInstance(getActivity().getApplicationContext()).isFirstTimeAddMoreTechPopup()) {
                        AddTech_CreateTask_OnceDialog(getActivity());
                    }

                }

            } else if (classType.equalsIgnoreCase(TechnicianList.class.getSimpleName())) {
                Gson gson = new Gson();
                mTechnicianList = gson.fromJson(urlConnectionResponse.resultData, TechnicianList.class);
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.DOWNLOAD);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_LIST, bundle);
                if (mTechnicianList.getCode().equalsIgnoreCase("200") && mTechnicianList.getMessage().equalsIgnoreCase("success")) {
                    FWLogger.logInfo(TAG, "TechList download Successfully");
                    if (!hasPermissions(getContext(), PERMISSIONS)) {
                        Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                        Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                        t.show();
                    } else {
                        DownloadFile.downloadFile(mTechnicianList.getResultData().getTechFilePath(), getActivity());
                    }
                } else {
                    Toast.makeText(getActivity(), mTechnicianList.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else if (classType.equalsIgnoreCase(DeleteOwnerTechnician.class.getSimpleName())) {
                Gson gson = new Gson();
                DeleteOwnerTechnician mDeleteTech = gson.fromJson(urlConnectionResponse.resultData, DeleteOwnerTechnician.class);
                if (mDeleteTech.getCode().equalsIgnoreCase("200") && mDeleteTech.getMessage().equalsIgnoreCase("Your account has been deleted.")) {
                    FWLogger.logInfo(TAG, "account has been deleted.");
                    refresh();
                } else {
                    Toast.makeText(mcontext, "Unable to delete Technician Account", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void DeleteTechAccount(int userID, Context mcontext, Activity mActivity) {
        /*mDeleteSelf_TechAccAsyncTask = new DeleteSelf_TechAccAsyncTask(mcontext, BaseAsyncTask.Priority.LOW, this);
        mDeleteSelf_TechAccAsyncTask.execute(userID);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(mActivity)) {
                Call<DeleteOwnerTechnician> call = RetrofitClient.getInstance(mcontext).getMyApi().getDeleteOwnAccount(userID);
                call.enqueue(new retrofit2.Callback<DeleteOwnerTechnician>() {
                    @Override
                    public void onResponse(Call<DeleteOwnerTechnician> call, Response<DeleteOwnerTechnician> response) {
                        try {
                            if (response.code() == 200) {
                                DeleteOwnerTechnician mDeleteTech = response.body();
                                if (mDeleteTech.getCode().equalsIgnoreCase("200") && mDeleteTech.getMessage().equalsIgnoreCase("Your account has been deleted.")) {
                                    FWLogger.logInfo(TAG, "account has been deleted.");
                                    refresh();
                                } else {
                                    Toast.makeText(mcontext, "Unable to delete Technician Account", Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteOwnerTechnician> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteLoginCredentials? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mActivity);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteLoginCredentials? API:");
            ex.getMessage();
        }

    }

    private void refresh() {
        Toast.makeText(mcontext, "Technician Account Deleted Successfully", Toast.LENGTH_SHORT).show();
        ((HomeActivityNew) mcontext).loadFragment(new TechnicianListFragment(), R.id.navigation_home);
    }

    /*private void downloadFile(TechnicianList.ResultData resultData) {
        try {
            final ProgressDialog pd = new ProgressDialog(getContext());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
//            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean downloadResponse = HttpDownloadUtility.downloadFile(resultData.getTechFilePath(), storageDir.getAbsolutePath());
                        if (downloadResponse) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.file_download_successfully, Toast.LENGTH_SHORT).show();
                                }
                            });
                            String filename = resultData.getTechFilePath().substring(resultData.getTechFilePath().lastIndexOf("/") + 1);
                            File file = new File(storageDir, filename);
                            Uri uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", file);
                            // Open file with user selected app
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(uri, "text/plain");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.sownload_failed, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        pd.dismiss();
                    } catch (IOException ex) {
                        pd.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onClick(View view, int position) {
        UsersList.ResultData userDetails = mUsersListAdapter.getItem(position);
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;

            case R.id.imageView_call:
                if (userDetails.getContactNo() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + userDetails.getContactNo()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (userDetails.getContactNo() != null) {
                    try {
                        String number = userDetails.getContactNo();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_trackHistory:
                if (!userDetails.getAttendance().equalsIgnoreCase("Absent")) {
                    loadTechGMapFragment(userDetails.getId());
                } else {
                    Toast.makeText(getActivity(), R.string.technician_status, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.relative_tech_details:
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.TECH_CLICKED);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_LIST, bundle);
                UserDialog userDialog = UserDialog.getInstance();
                userDialog.technicianDetailsDialog(getContext(), userDetails, getActivity());
                break;

            default:
                break;
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void loadTechGMapFragment(int techUserID) {

        new TechnicianGMapFragment(techUserID).show(getActivity().getSupportFragmentManager(), null);
    }

    public void DeleteTechnicianDialog(Context context, UsersList.ResultData userDetails, Activity mActivity) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_tech_account);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button btnDeleteTech, btnNotYet;
        TextView textView_message2;

        btnDeleteTech = dialog.findViewById(R.id.btnDeleteTech);
        btnNotYet = dialog.findViewById(R.id.btnNotYet);
        textView_message2 = dialog.findViewById(R.id.textView_message2);

        try {
            if (userDetails.getFirstName() != null && !userDetails.getFirstName().equalsIgnoreCase("null")) {
                textView_message2.setText(userDetails.getFirstName() + " " + userDetails.getLastName());
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        btnDeleteTech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteTechAccount(userDetails.getId(), context, mActivity);
                dialog.dismiss();
            }
        });

        btnNotYet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void AddTech_CreateTask_OnceDialog(Activity activity) {
        try {
            SharedPrefManager.getInstance(getActivity().getApplicationContext()).setisFirstTimeAddMoreTechPopup(false);
            FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.first_time_only);
            dialog.getWindow().setGravity(Gravity.BOTTOM);

            Button addMoreTech, createTask;
            ImageButton closeAddTech;

            addMoreTech = dialog.findViewById(R.id.addMoreTech);
            createTask = dialog.findViewById(R.id.createTask);
            closeAddTech = dialog.findViewById(R.id.closeAddTech);

            addMoreTech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivityNew) activity).loadFragment(new AddBulkTechFragment(), R.id.navigation_home);
                    dialog.dismiss();
                }
            });

            createTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivityNew) activity).addTask();
                    dialog.dismiss();
                }
            });

            closeAddTech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_download:
                getDownloadTechList();
                break;
            case R.id.plus_tech:
                ((HomeActivityNew) mcontext).loadFragment(new AddBulkTechFragment(), R.id.navigation_home);
                break;
        }
    }
}