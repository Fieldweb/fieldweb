package com.corefield.fieldweb.FieldWeb.Home;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.corefield.fieldweb.Adapter.ImageSliderAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.AMC.AMCDashboardCount;
import com.corefield.fieldweb.DTO.Attendance.OwnDateAttendance;
import com.corefield.fieldweb.DTO.Passbook.EarningDashboard;
import com.corefield.fieldweb.DTO.Passbook.MonthlyPassbook;
import com.corefield.fieldweb.DTO.Task.TaskStatusCount;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.AMC.AMCListFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.Admin.TechnicianListFragment;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Passbook.HomePassbookFragmentNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.gson.Gson;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerDashboardFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show Owner Dashboard component
 */

public class OwnerDashboardFragment extends Fragment implements OnTaskCompleteListener {
    protected static String TAG = OwnerDashboardFragment.class.getSimpleName();
    public List<TaskStatusCount.ResultData> mTaskStatusCount = null;
    private View mRootView;
    private TextView mTextViewAbsent, mTextViewPresent, mTextViewIdle, mTextViewEarned, mTextViewDataFor,
            textProfilePercctage, mCreateTask;
    public TextView textProfileEdit, profileCompletion;
    private TextView mTextViewTotalTask;
    private TextView mTextViewComplete, mTextViewUrgent, mTextViewInactive, mTextViewReject, mTextViewOnHold;
    private TextView mTextViewAMCStatus, mTextViewAMCCompleted, mTextViewAMCUpcoming, mTextViewAMCRenewal, mTextViewAMCReject;
    private FitChart mFitChart;
    private RoundCornerProgressBar mRoundCornerProgressBar;
    Resources mResources;
    public List<MonthlyPassbook.MonthlyALlDataList> mMonthlyPassbook = null;
    private String selectedFilter = "";
    private RoundCornerProgressBar mprogress_ProfileOwner;
    private UserDetails.ResultData mUserDetails;
    private float mTotalProfilePercentage;
    private Activity mActivity;
    List<UsersList.ResultData> userListResponseData;
    private int mfirstName, mlastName, mConactNum, mAddress, mEmailid, mDOB,
            mCompanyName, mCompAddress, mCompanyCity, mCompGSTPAN, mCompContactNum, mCompWebsite, mProfileImg, mCompLogo;

    public RelativeLayout mRelativeHeaderLayout;
    public LinearLayout mLinearHeaderLayout;
    public RecyclerView mDatesRecyclerView;
    public Toolbar mToolbar;
    TextView Earnings, mAttendance, txShowingDataFor;
    ImageView mEarnings, mAMC, mAttend;
    ViewPager mPager;
    int currentPage = 0;

    Integer pic[] =
            {
                    R.drawable.slider_customerapp,
                    R.drawable.slider_fieldwebfixit,
                    R.drawable.slider_fieldfinz,
                    R.drawable.slider_fieldstaffing,
                    R.drawable.slider_fieldstrade,
            };

    //change here

    ArrayList<Integer> picArray = new ArrayList<Integer>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.owner_home_fragment_new, container, false);
        inIt();
        jadu();

        return mRootView;
    }

    public void jadu() {
        //change here
        for (int i = 0; i < pic.length; i++)
            //change here
            picArray.add(pic[i]);
        mPager = (ViewPager) mRootView.findViewById(R.id.pager);
        mPager.setAdapter(new ImageSliderAdapter(getContext(), picArray));

        final Handler handler = new Handler();//android.os

        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == pic.length) {
                    currentPage = 0;
                }

                mPager.setCurrentItem(currentPage++, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 4000, 4000);
    }

   /* public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("TYPE", "terms");
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }*/

    private void inIt() {
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setVisibility(View.VISIBLE);

        mActivity = getActivity();
        if (getContext() != null)
            mResources = getContext().getResources();

        mFitChart = (FitChart) mRootView.findViewById(R.id.fitChart);
        profileCompletion = (TextView) mRootView.findViewById(R.id.profileCompletion);

        mTextViewAbsent = mRootView.findViewById(R.id.textView_absent);
        mTextViewPresent = mRootView.findViewById(R.id.textView_present);
        mTextViewIdle = mRootView.findViewById(R.id.textView_idle);
        mCreateTask = mRootView.findViewById(R.id.textView_create_task);
        mTextViewEarned = mRootView.findViewById(R.id.textView_earned);
        mTextViewComplete = mRootView.findViewById(R.id.textView_complete);
        mTextViewUrgent = mRootView.findViewById(R.id.textView_urgent);
        mTextViewInactive = mRootView.findViewById(R.id.textView_inactive);
        mTextViewReject = mRootView.findViewById(R.id.textView_reject);
        mTextViewOnHold = mRootView.findViewById(R.id.textView_onhold);
        mTextViewDataFor = mRootView.findViewById(R.id.textView_data_for);

        mTextViewAMCStatus = mRootView.findViewById(R.id.textView_amc_status);
        mTextViewAMCCompleted = mRootView.findViewById(R.id.textView_amc_completed);
        mTextViewAMCUpcoming = mRootView.findViewById(R.id.textView_amc_upcoming);
        mTextViewAMCRenewal = mRootView.findViewById(R.id.textView_amc_renewal);
        mTextViewAMCReject = mRootView.findViewById(R.id.textView_amc_reject);

        txShowingDataFor = mRootView.findViewById(R.id.txShowingDataFor);

        //mRoundCornerProgressBar = mRootView.findViewById(R.id.progress_attendance);

        mprogress_ProfileOwner = mRootView.findViewById(R.id.mprogress_ProfileOwner);
        textProfilePercctage = mRootView.findViewById(R.id.textProfilePercctage);
        textProfileEdit = mRootView.findViewById(R.id.textProfileEdit);

        mTextViewTotalTask = mRootView.findViewById(R.id.textView_total_task);

        mEarnings = mRootView.findViewById(R.id.info_earning);
        Earnings = mRootView.findViewById(R.id.textView_earning);
        mAMC = mRootView.findViewById(R.id.info_amc);
        mAttendance = mRootView.findViewById(R.id.attendance_click);
        mAttend = mRootView.findViewById(R.id.info_attendance);

        mLinearHeaderLayout = mRootView.findViewById(R.id.linear_task_list_header);
        mDatesRecyclerView = mRootView.findViewById(R.id.recycler_dates);
        mToolbar = mRootView.findViewById(R.id.toolbar);

       /* Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String currentDate = mYear + "-" + (mMonth + 1) + "-" + mDay;*/

        selectedFilter = ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.getSelectedItem().toString();

        try {
            txShowingDataFor.setText(selectedFilter);
        } catch (Exception ex) {
            ex.getMessage();
        }

        callStatusCountApi();
        callAMCStatusCountApi();
        callMonthlyEarningApi();

        /*mOwnerAttendanceAsyncTask = new OwnerAttendanceAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnerAttendanceAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), currentDate);*/

        getAttendanceMonthlyForAdmin();

        /*new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        FWLogger.logInfo(TAG, "onItemSelected called : " + position);
                        String selectedTaskType = parent.getItemAtPosition(position).toString();
                        FWLogger.logInfo(TAG, "selected called : " + selectedTaskType);
                        if (((HomeActivityNew) getActivity()).monthList != null)
                            ((HomeActivityNew) getActivity()).mSelectedMonth = ((HomeActivityNew) getActivity()).monthList.get(position);
                        FWLogger.logInfo(TAG, "Month Number : " + ((HomeActivityNew) getActivity()).mSelectedMonth);
                        String[] mons = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
                        ((HomeActivityNew) getActivity()).mSelectedMonthName = mons[((HomeActivityNew) getActivity()).mSelectedMonth - 1];
                        FWLogger.logInfo(TAG, "Month Name : " + ((HomeActivityNew) getActivity()).mSelectedMonthName);

                        callStatusCountApi();
                        callMonthlyEarningApi();
                        callAMCStatusCountApi();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        });*/

        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Day Filter called : " + position);
                selectedFilter = parent.getItemAtPosition(position).toString();
                try {
                    txShowingDataFor.setText(selectedFilter);
                } catch (Exception ex) {
                    ex.getMessage();
                }

//                Toast.makeText(getActivity(), "Selected : "+selectedFilter, Toast.LENGTH_SHORT).show();
                mTextViewDataFor.setText(DateUtils.getSelectedDate(selectedFilter));
//                mTextViewDataFor.setText(selectedFilter);
                callStatusCountApi();
                callAMCStatusCountApi();
                callMonthlyEarningApi();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mTextViewDataFor.setText(DateUtils.getSelectedDate(selectedFilter));

        mTextViewAMCStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.amc);
                ((HomeActivityNew) getActivity()).loadFragment(new AMCListFragment(), R.id.navigation_home);
            }
        });

        mAMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.amc);
                ((HomeActivityNew) getActivity()).loadFragment(new AMCListFragment(), R.id.navigation_home);
            }
        });

        Earnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.passbook);

                ((HomeActivityNew) getActivity()).loadFragment(new HomePassbookFragmentNew(), R.id.navigation_home);
            }
        });

        mEarnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.passbook);

                ((HomeActivityNew) getActivity()).loadFragment(new HomePassbookFragmentNew(), R.id.navigation_home);
            }
        });

        mAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*((HomeActivityNew) getActivity()).loadFragment(new OwnerAttendanceFragment(), R.id.navigation_home);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.attendance);*/
                ((HomeActivityNew) getActivity()).loadFragment(new TechnicianListFragment(), R.id.navigation_home);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.attendance);
            }
        });
        mAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   ((HomeActivityNew) getActivity()).loadFragment(new OwnerAttendanceFragment(), R.id.navigation_home);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.attendance);*/
                ((HomeActivityNew) getActivity()).loadFragment(new TechnicianListFragment(), R.id.navigation_home);
                ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.attendance);
            }
        });

        mCreateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) getActivity()).addTask();
            }
        });

        textProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ProfileFragmentNew profileFragmentNew = new ProfileFragmentNew();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, profileFragmentNew);
                    transaction.commit();
                } catch (Exception ex) {
                    ex.getMessage();
                }

            }
        });
        // OWNER PROFILE TOTAL % CALCULATION : START
        try {
            getProfileDetails();
        } catch (Exception e) {
            e.getMessage();
        }

        app_launched(getActivity());
    }


    public void getAttendanceMonthlyForAdmin() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String currentDate = mYear + "-" + (mMonth + 1) + "-" + mDay;

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<OwnDateAttendance> call = RetrofitClient.getInstance(getContext()).getMyApi().getAttendanceMonthlyForAdmin(SharedPrefManager.getInstance(getContext()).getUserId(), currentDate);
                call.enqueue(new retrofit2.Callback<OwnDateAttendance>() {
                    @Override
                    public void onResponse(Call<OwnDateAttendance> call, Response<OwnDateAttendance> response) {
                        try {
                            if (response.code() == 200) {
                                OwnDateAttendance ownDateAttendance = response.body();
                                FWLogger.logInfo(TAG, "Today Total Attendance" + ownDateAttendance.getMessage());
                                mTextViewPresent.setText(String.valueOf(ownDateAttendance.getResultData().getPresentCount()));
                                mTextViewAbsent.setText(String.valueOf(ownDateAttendance.getResultData().getAbsentCount()));
                                mTextViewIdle.setText(String.valueOf(ownDateAttendance.getResultData().getIdleCount()));

                                int totalAttendance = 0;
                                totalAttendance = ownDateAttendance.getResultData().getPresentCount() + ownDateAttendance.getResultData().getAbsentCount() + ownDateAttendance.getResultData().getIdleCount();
            /*mRoundCornerProgressBar.setMax(totalAttendance);
            mRoundCornerProgressBar.setProgress(ownDateAttendance.getResultData().getPresentCount());
            mRoundCornerProgressBar.setProgressColor(getResources().getColor(R.color.green));
            mRoundCornerProgressBar.setSecondaryProgress(ownDateAttendance.getResultData().getPresentCount() + ownDateAttendance.getResultData().getIdleCount());
            mRoundCornerProgressBar.setSecondaryProgressColor(getResources().getColor(R.color.orange));*/

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<OwnDateAttendance> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAttendanceMonthlyForAdmin? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAttendanceMonthlyForAdmin? API:");
            ex.getMessage();
        }
    }
    /*public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.commit();
    }*/


    public void getProfileDetails() {
       /* mProfileDetailsAsyncTask = new GetProfileDetailsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mProfileDetailsAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<UserDetails> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getProfileDetails(userID);
                call.enqueue(new retrofit2.Callback<UserDetails>() {
                    @Override
                    public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                        try {
                            if (response.code() == 200) {
                                UserDetails userDetails = response.body();
                                //mUserDetails = new UserDetails.ResultData();
                                mUserDetails = userDetails.getResultData();
                                try {
                                    mTotalProfilePercentage = Float.parseFloat(mUserDetails.getProgressBarPercentage());
                                    try {
                                        if (mTotalProfilePercentage != 100) {
                                            int totalProfileCompletion = 0;
                                            totalProfileCompletion = 100;
                                            mprogress_ProfileOwner.setMax(totalProfileCompletion);
                                            mprogress_ProfileOwner.setProgress(mTotalProfilePercentage);
                                            textProfilePercctage.setText(mTotalProfilePercentage + "%");
                                            textProfileEdit.setVisibility(View.VISIBLE);
                                        } else {
                                            mprogress_ProfileOwner.setVisibility(View.GONE);
                                            textProfilePercctage.setVisibility(View.GONE);
                                            profileCompletion.setVisibility(View.GONE);
                                        }
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }

                                    if (HomeActivityNew.onDemandAppTourFlag) {
                                        // OnDemandOwnerAppTour(getActivity());
                                        ((HomeActivityNew) getActivity()).OwnerAppTour();
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
            ex.getMessage();
        }
    }

    private void callMonthlyEarningApi() {
       /* mGetEarningsAsyncTask = new GetEarningsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mGetEarningsAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<EarningDashboard> call = RetrofitClient.getInstance(getContext()).getMyApi().getPassbookDetailsForDashboard(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());
                call.enqueue(new retrofit2.Callback<EarningDashboard>() {
                    @Override
                    public void onResponse(Call<EarningDashboard> call, Response<EarningDashboard> response) {
                        try {
                            if (response.code() == 200) {
                                EarningDashboard earningDashboard = response.body();
//            FWLogger.logInfo(TAG, "amcDashboardCount MSG " + amcDashboardCount.getMessage());
                                if (earningDashboard.getCode().equalsIgnoreCase("200") && earningDashboard.getMessage().equalsIgnoreCase("success")) {
                                    mTextViewEarned.setText(String.valueOf(earningDashboard.getResultData().getEarningAmount()));
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EarningDashboard> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetPassbookDetailsForDashboard? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetPassbookDetailsForDashboard? API:");
            ex.getMessage();
        }
    }

    /*public void callStatusCountApi() {
        mOwnHomeTaskStatusCountAsyncTask = new OwnHomeTaskStatusCountAsyncTaskNew(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnHomeTaskStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), ((HomeActivityNew) getActivity()).mSelectedMonth, ((HomeActivityNew) getActivity()).mSelectedYear);
    }*/
    public void callStatusCountApi() {
       /* mOwnHomeTaskStatusCountAsyncTask = new OwnHomeTaskStatusCountAsyncTaskNew(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnHomeTaskStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<TaskStatusCount> call = RetrofitClient.getInstance(getContext()).getMyApi().getTaskStatusCount(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());
                call.enqueue(new retrofit2.Callback<TaskStatusCount>() {
                    @Override
                    public void onResponse(Call<TaskStatusCount> call, Response<TaskStatusCount> response) {
                        try {
                            if (response.code() == 200) {
                                TaskStatusCount taskStatusCount = response.body();
                                mTaskStatusCount = taskStatusCount.getResultData();
                                FWLogger.logInfo(TAG, "TaskStatus Count " + taskStatusCount.getMessage());

                                Collection<FitChartValue> values = new ArrayList<>();

                                int totalTaskCount = 0;
                                String taskType = "";
                                if (taskStatusCount.getCode().equalsIgnoreCase("200") && taskStatusCount.getMessage().equalsIgnoreCase("success")) {
                                    mTextViewComplete.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.green)));

                                    mTextViewReject.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                                    mTextViewUrgent.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                                    mTextViewInactive.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                                    mTextViewOnHold.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.onhold)));

                                    mFitChart.setMinValue(0f);
                                    for (int i = 0; i < mTaskStatusCount.size(); i++) {
                                        totalTaskCount = totalTaskCount + mTaskStatusCount.get(i).getTaskcount();
                                        taskType = mTaskStatusCount.get(i).getName();
                                        switch (taskType) {
                                            case "Completed":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewComplete.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewComplete.setText("" + mTaskStatusCount.get(i).getTaskcount());

                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.green)));
                                                break;

                                            case "Rejected":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewReject.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewReject.setText("" + mTaskStatusCount.get(i).getTaskcount());

                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.colorPrimaryDark)));
                                                break;

                                            case "Ongoing":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewUrgent.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewUrgent.setText("" + mTaskStatusCount.get(i).getTaskcount());

                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.orange)));
                                                break;

                                            case "InActive":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewInactive.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewInactive.setText("" + mTaskStatusCount.get(i).getTaskcount());

                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.light_gray)));
                                                break;

                                            case "OnHold":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewOnHold.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewOnHold.setText("" + mTaskStatusCount.get(i).getTaskcount());

                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.onhold)));
                                                break;


                                            default:
                                                break;
                                        }

                                    }
                                    mFitChart.setMaxValue(totalTaskCount);
                                    mFitChart.setValues(values);
                                    mTextViewTotalTask.setText("" + totalTaskCount);
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TaskStatusCount> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetTaskDataForDashboard? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTaskDataForDashboard? API:");
            ex.getMessage();
        }
    }

    /*public void callAMCStatusCountApi() {
        mOwnHomeAMCStatusCountAsyncTask = new OwnHomeAMCStatusCountAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnHomeAMCStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), ((HomeActivityNew) getActivity()).mSelectedMonth + "-" + ((HomeActivityNew) getActivity()).mSelectedDate + "-" + ((HomeActivityNew) getActivity()).mSelectedYear);
    }*/
    public void callAMCStatusCountApi() {
        /*mOwnHomeAMCStatusCountAsyncTask = new OwnHomeAMCStatusCountAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnHomeAMCStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<AMCDashboardCount> call = RetrofitClient.getInstance(getContext()).getMyApi().getAMCDashboardCountDetails(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());
                call.enqueue(new retrofit2.Callback<AMCDashboardCount>() {
                    @Override
                    public void onResponse(Call<AMCDashboardCount> call, Response<AMCDashboardCount> response) {
                        try {
                            if (response.code() == 200) {
                                AMCDashboardCount amcDashboardCount = response.body();
                                FWLogger.logInfo(TAG, "amcDashboardCount MSG " + amcDashboardCount.getMessage());
                                if (amcDashboardCount.getCode().equalsIgnoreCase("200") && amcDashboardCount.getMessage().equalsIgnoreCase("Success Request")) {
                                    mTextViewAMCCompleted.setText("" + amcDashboardCount.getResultData().getTotalCompleted());
                                    mTextViewAMCUpcoming.setText("" + amcDashboardCount.getResultData().getTotalUpcomming());
                                    mTextViewAMCRenewal.setText("" + amcDashboardCount.getResultData().getTotalRenewal());
                                    mTextViewAMCReject.setText("" + amcDashboardCount.getResultData().getTotalExpired());
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AMCDashboardCount> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AMCDashboardDetailsV2WithFilter? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AMCDashboardDetailsV2WithFilter? API:");
            ex.getMessage();
        }
    }

    public void app_launched(Context mContext) {

        int DAYS_UNTIL_PROMPT = 3;//Min number of days
        int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch +
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                ((HomeActivityNew) getActivity()).PlayStoreRatingDialog(getActivity());
            }
        }

        editor.commit();
    }


    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {

        if (classType.equalsIgnoreCase(TaskStatusCount.class.getSimpleName())) {
            Gson gson = new Gson();
            TaskStatusCount taskStatusCount = gson.fromJson(urlConnectionResponse.resultData, TaskStatusCount.class);
            mTaskStatusCount = taskStatusCount.getResultData();
            FWLogger.logInfo(TAG, "TaskStatus Count " + taskStatusCount.getMessage());

            Collection<FitChartValue> values = new ArrayList<>();

            int totalTaskCount = 0;
            String taskType = "";
            if (taskStatusCount.getCode().equalsIgnoreCase("200") && taskStatusCount.getMessage().equalsIgnoreCase("success")) {
                mTextViewComplete.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.green)));

                mTextViewReject.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                mTextViewUrgent.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                mTextViewInactive.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.colorPrimaryDark)));

                mFitChart.setMinValue(0f);
                for (int i = 0; i < mTaskStatusCount.size(); i++) {
                    totalTaskCount = totalTaskCount + mTaskStatusCount.get(i).getTaskcount();
                    taskType = mTaskStatusCount.get(i).getName();
                    switch (taskType) {
                        case "Completed":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewComplete.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewComplete.setText("" + mTaskStatusCount.get(i).getTaskcount());

                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.green)));
                            break;

                        case "Rejected":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewReject.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewReject.setText("" + mTaskStatusCount.get(i).getTaskcount());

                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.colorPrimaryDark)));
                            break;

                        case "Ongoing":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewUrgent.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewUrgent.setText("" + mTaskStatusCount.get(i).getTaskcount());

                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.orange)));
                            break;

                        case "InActive":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewInactive.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewInactive.setText("" + mTaskStatusCount.get(i).getTaskcount());

                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.light_gray)));
                            break;

                        default:
                            break;
                    }

                }
                mFitChart.setMaxValue(totalTaskCount);
                mFitChart.setValues(values);
                mTextViewTotalTask.setText("" + totalTaskCount);
            }

        } else if (classType.equalsIgnoreCase(AMCDashboardCount.class.getSimpleName())) {
            Gson gson = new Gson();
            AMCDashboardCount amcDashboardCount = gson.fromJson(urlConnectionResponse.resultData, AMCDashboardCount.class);
            FWLogger.logInfo(TAG, "amcDashboardCount MSG " + amcDashboardCount.getMessage());
            if (amcDashboardCount.getCode().equalsIgnoreCase("200") && amcDashboardCount.getMessage().equalsIgnoreCase("Success Request")) {
                mTextViewAMCCompleted.setText("" + amcDashboardCount.getResultData().getTotalCompleted());
                mTextViewAMCUpcoming.setText("" + amcDashboardCount.getResultData().getTotalUpcomming());
                mTextViewAMCRenewal.setText("" + amcDashboardCount.getResultData().getTotalRenewal());
                mTextViewAMCReject.setText("" + amcDashboardCount.getResultData().getTotalExpired());
            }

        } else if (classType.equalsIgnoreCase(EarningDashboard.class.getSimpleName())) {
            Gson gson = new Gson();
            EarningDashboard earningDashboard = gson.fromJson(urlConnectionResponse.resultData, EarningDashboard.class);
//            FWLogger.logInfo(TAG, "amcDashboardCount MSG " + amcDashboardCount.getMessage());
            if (earningDashboard.getCode().equalsIgnoreCase("200") && earningDashboard.getMessage().equalsIgnoreCase("success")) {
                mTextViewEarned.setText(String.valueOf(earningDashboard.getResultData().getEarningAmount()));
            }

        } else if (classType.equalsIgnoreCase(OwnDateAttendance.class.getSimpleName())) {
            Gson gson = new Gson();
            OwnDateAttendance ownDateAttendance = gson.fromJson(urlConnectionResponse.resultData, OwnDateAttendance.class);

            FWLogger.logInfo(TAG, "Today Total Attendance" + ownDateAttendance.getMessage());

            mTextViewPresent.setText(String.valueOf(ownDateAttendance.getResultData().getPresentCount()));
            mTextViewAbsent.setText(String.valueOf(ownDateAttendance.getResultData().getAbsentCount()));
            mTextViewIdle.setText(String.valueOf(ownDateAttendance.getResultData().getIdleCount()));

            int totalAttendance = 0;
            totalAttendance = ownDateAttendance.getResultData().getPresentCount() + ownDateAttendance.getResultData().getAbsentCount() + ownDateAttendance.getResultData().getIdleCount();
            /*mRoundCornerProgressBar.setMax(totalAttendance);
            mRoundCornerProgressBar.setProgress(ownDateAttendance.getResultData().getPresentCount());
            mRoundCornerProgressBar.setProgressColor(getResources().getColor(R.color.green));
            mRoundCornerProgressBar.setSecondaryProgress(ownDateAttendance.getResultData().getPresentCount() + ownDateAttendance.getResultData().getIdleCount());
            mRoundCornerProgressBar.setSecondaryProgressColor(getResources().getColor(R.color.orange));*/
        }
        // FOR PROFILE PERCENTAGE COUNT
        else if (classType.equalsIgnoreCase(UserDetails.class.getSimpleName())) {
            Gson gson = new Gson();
            UserDetails userDetails = gson.fromJson(urlConnectionResponse.resultData, UserDetails.class);
            //mUserDetails = new UserDetails.ResultData();
            mUserDetails = userDetails.getResultData();
            try {
                mTotalProfilePercentage = Float.parseFloat(mUserDetails.getProgressBarPercentage());
                int totalProfileCompletion = 0;
                totalProfileCompletion = 100;
                mprogress_ProfileOwner.setMax(totalProfileCompletion);
                mprogress_ProfileOwner.setProgress(mTotalProfilePercentage);
                textProfilePercctage.setText(mTotalProfilePercentage + "%");
              /*  if (!SharedPrefManager.getInstance(getApplicationContext()).isAppTourCompleted()) {
                    //OwnerAppTour_Skippable();
                }*/
                if (HomeActivityNew.onDemandAppTourFlag) {
                    // OnDemandOwnerAppTour(getActivity());
                    ((HomeActivityNew) getActivity()).OwnerAppTour();
                }
            } catch (Exception e) {
                e.getMessage();
            }

           /* try {
                if (mUserDetails.getFirstName() != null && !mUserDetails.getFirstName().isEmpty() ||
                        mUserDetails.getLastName() != null && !mUserDetails.getLastName().isEmpty()) {
                    mfirstName = 5;
                    mlastName = 5;
                } else {
                    mfirstName = 0;
                    mlastName = 0;
                }
                if (mUserDetails.getContactNo() != null && !mUserDetails.getContactNo().isEmpty()) {
                    mConactNum = 5;
                } else {
                    mConactNum = 0;
                }
                if (mUserDetails.getAddress() != null && !mUserDetails.getAddress().isEmpty()) {
                    mAddress = 5;
                } else {
                    mAddress = 0;
                }
                if (mUserDetails.getEmail() != null && !mUserDetails.getEmail().isEmpty()) {
                    mEmailid = 5;
                } else {
                    mEmailid = 0;
                }
                if (mUserDetails.getDOB() != null && !mUserDetails.getDOB().isEmpty()) {
                    mDOB = 5;
                } else {
                    mDOB = 0;
                }
                if (mUserDetails.getCompanyName() != null && !mUserDetails.getCompanyName().isEmpty()) {
                    mCompanyName = 10;
                } else {
                    mCompanyName = 0;
                }
                if (mUserDetails.getCompanyAddress() != null && !mUserDetails.getCompanyAddress().isEmpty()) {
                    mCompAddress = 5;
                } else {
                    mCompAddress = 0;
                }
                if (mUserDetails.getCompanyCity() != null && !mUserDetails.getCompanyCity().isEmpty()) {
                    mCompanyCity = 5;
                } else {
                    mCompanyCity = 0;
                }
                if (mUserDetails.getCompanyGSTorPanNo() != null && !mUserDetails.getCompanyGSTorPanNo().isEmpty()) {
                    mCompGSTPAN = 10;
                } else {
                    mCompGSTPAN = 0;
                }
                if (String.valueOf(mUserDetails.getCompanyContactNo()) != null && !String.valueOf(mUserDetails.getCompanyContactNo()).isEmpty()) {
                    mCompContactNum = 10;
                } else {
                    mCompContactNum = 0;
                }
                if (mUserDetails.getCompanyWebsite() != null && !mUserDetails.getCompanyWebsite().isEmpty()) {
                    mCompWebsite = 10;
                } else {
                    mCompWebsite = 0;
                }
                if (mUserDetails.getPhoto() != null && !mUserDetails.getPhoto().isEmpty()) {
                    mProfileImg = 10;
                } else {
                    mProfileImg = 0;
                }
                if (mUserDetails.getCompanyLogo() != null && !mUserDetails.getCompanyLogo().isEmpty()) {
                    mCompLogo = 10;
                } else {
                    mCompLogo = 0;
                }

                mTotalProfilePercentage = mfirstName + mlastName + mConactNum + mAddress + mEmailid + mDOB +
                        mCompanyName + mCompAddress + mCompanyCity + mCompGSTPAN + mCompContactNum + mCompWebsite + mProfileImg + mCompLogo;

                int totalProfileCompletion = 0;
                totalProfileCompletion = 100;
                mprogress_ProfileOwner.setMax(totalProfileCompletion);
                mprogress_ProfileOwner.setProgress(mTotalProfilePercentage);
                textProfilePercctage.setText(mTotalProfilePercentage + "%");

            } catch (Exception e) {
                e.getMessage();
            }*/
        }

        /*else if (classType.equalsIgnoreCase(MonthlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            MonthlyPassbook monthlyPassbook = gson.fromJson(urlConnectionResponse.resultData, MonthlyPassbook.class);
            mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
            for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                if (((HomeActivityNew) getActivity()).mSelectedMonthName.equalsIgnoreCase(mMonthlyPassbook.get(i).getMonth())) {
                    mTextViewEarned.setText(String.valueOf(mMonthlyPassbook.get(i).getEarning()));
                }
            }
        }*/
    }


    @Override
    public void onResume() {
        super.onResume();
        /*if(((HomeActivityNew) getActivity()).spinnerMonthYear != null) {
            ((HomeActivityNew) getActivity()).spinnerMonthYear.post(new Runnable() {
                @Override
                public void run() {
                ((HomeActivityNew) getActivity()).spinnerMonthYear.setSelection(0);
//                    ((HomeActivityNew) getActivity()).getMonthYearList();
//                spinnerAdapter.notifyDataSetChanged();
                }
            });
        }*/
    }

    public void OwnerAppTour_Skippable(Activity mActivity) {
        try {
            TapTarget target1 = TapTarget.forView(mActivity.findViewById(R.id.profileCompletion/*textProfileEdit*/),
                    mActivity.getString(R.string.profile),
                    mActivity.getString(R.string.complete_ur_profile))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.black)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.transperant_black)
                    .dimColor(R.color.black).targetRadius(50);

            TapTargetSequence sequence = new TapTargetSequence(mActivity);
            sequence.targets(target1);
            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    sequence.cancel();
                    SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                    //CALL ADD TECH IF NUMBER OF TECH = 0
                    ((HomeActivityNew) mActivity).AddTechDialog(mActivity);

                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void OwnerAppTour_NonSkippable(Activity mActivity) {
        try {
            TapTarget target1 = TapTarget.forView(mActivity.findViewById(R.id.profileCompletion/*textProfileEdit*/),
                    mActivity.getString(R.string.profile),
                    mActivity.getString(R.string.complete_ur_profile))
                    .drawShadow(true)
                    .cancelable(false)
                    .targetCircleColor(R.color.black)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.transperant_black)
                    .dimColor(R.color.black).targetRadius(50);

            TapTargetSequence sequence = new TapTargetSequence(mActivity);
            sequence.targets(target1);
            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    sequence.cancel();
                    HomeActivityNew.onDemandAppTourFlag = false;
                    //((HomeActivityNew) mActivity).AppGuideForOwner_Skippable();
                    //SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setVisibility(View.GONE);
        super.onDestroyView();
    }
}
