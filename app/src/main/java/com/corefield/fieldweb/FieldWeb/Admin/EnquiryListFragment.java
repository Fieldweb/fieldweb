package com.corefield.fieldweb.FieldWeb.Admin;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CustomerListAdapterNew;
import com.corefield.fieldweb.Adapter.EnquiryListAdapterNew;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Enquiry.AddEnquiry;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateEnquiryAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeGetLinkActivity;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Listener.RecyclerTouchListenerUpdateItem;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for CRMFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Enquiry and Customer list
 */
public class EnquiryListFragment extends Fragment implements OnTaskCompleteListener, View.OnClickListener, RecyclerTouchListener, RecyclerTouchListenerUpdateItem {
    protected static String TAG = EnquiryListFragment.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewList;
    private Button mButtonEnquiries, mButtonCustomers;
    private List<EnquiryList.ResultData> mEnquiryLists;
    private EnquiryListAdapterNew mEnquiryListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SearchView mSearchView;

    private List<CustomerList.ResultData> mCustomerList;
    private CustomerListAdapterNew mCustomerListAdapter;
    private String mSelection = "";
    Bundle mBundle;
    EnquiryList.ResultData resultData = null;
    private UpdateEnquiryAsyncTask mUpdateEnquiryAsyncTask;
    Button plusenq;
    ImageView mGetLink, noResultImg;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.enquiry_list_fragment, container, false);
        //NOTE: Log GA event
        mBundle = new Bundle();
        mBundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        mBundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        mBundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        mBundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        mBundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        inIT();
        return mRootView;
    }

    private void inIT() {
        mRecyclerViewList = mRootView.findViewById(R.id.recycler_list_view);
        mButtonEnquiries = mRootView.findViewById(R.id.button_enquiries);
        mButtonCustomers = mRootView.findViewById(R.id.button_customers);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        plusenq = mRootView.findViewById(R.id.plus_enq);
        mGetLink = mRootView.findViewById(R.id.getlink);

        noResultImg = mRootView.findViewById(R.id.noResultImg);

        mSelection = "ENQ";
        mSearchView.setQueryHint("Search by Enq no., Customer name...");
        getEnquiryList();

        /*mButtonEnquiries.setOnClickListener(this);
        mButtonCustomers.setOnClickListener(this);*/
        plusenq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) getActivity()).addEnquiry();
            }
        });
        mGetLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getEnquiryLinkDialog(getActivity());
            }
        });
    }

    private void getEnquiryList() {
        /*mGetEnquiryListAsyncTask = new GetEnquiryListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mGetEnquiryListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<EnquiryList> call = RetrofitClient.getInstance(getContext()).getMyApi().getEnquiryList(userID,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<EnquiryList>() {
                    @Override
                    public void onResponse(Call<EnquiryList> call, Response<EnquiryList> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                EnquiryList enquiryList = response.body();
                                if (enquiryList.getCode().equalsIgnoreCase("200") && enquiryList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + enquiryList.getMessage());
                                    if (enquiryList != null) {
                                        mEnquiryLists = new ArrayList<>();
                                        mEnquiryLists = enquiryList.getResultData();
                                        if (mEnquiryLists.size() > 0) {
                                            setData();
                                        } else {
                                            noResultImg.setVisibility(View.VISIBLE);
                                            noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EnquiryList> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in GetAllEnquiryListForMobile? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllEnquiryListForMobile? API:");
            ex.getMessage();
        }
        plusenq.setVisibility(View.VISIBLE);
        mGetLink.setVisibility(View.VISIBLE);
    }

    public void getEnquiryLinkDialog(Activity activity) {
        try {
            Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_get_enquiry_form_link);
            dialog.getWindow().setGravity(Gravity.BOTTOM);

            Button btn_getEnquiryFormLink = dialog.findViewById(R.id.btn_getEnquiryFormLink);
            Button btn_QR = dialog.findViewById(R.id.btn_getEnquiryFormQR);
            Button button_cancel = dialog.findViewById(R.id.button_cancel);
            TextView haddenq = dialog.findViewById(R.id.textView_how_to_get_link);


            haddenq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent send = new Intent(activity.getApplicationContext(), YouTubeGetLinkActivity.class);
                    activity.startActivity(send);
                }
            });

            btn_getEnquiryFormLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String getUserID = String.valueOf(SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                        if (getUserID != null) {
                            String[] iVal = getUserID.split("");
                            String encryptedVal = enryptUserID(iVal);
                            String strgetFormLink = URLConstant.Base.GET_LINK + encryptedVal;
                            // String strgetFormLink = URLConstant.Base.GET_LINK;
                            if (!strgetFormLink.isEmpty()) {
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "FieldWeb : Enquiry Form Link");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, strgetFormLink);
                                activity.startActivity(Intent.createChooser(sharingIntent, "Share"));
                            }
                        }
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            });

            btn_QR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String getUserID = String.valueOf(SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                        if (getUserID != null) {
                            String[] iVal = getUserID.split("");
                            String encryptedVal = enryptUserID(iVal);
                            String strgetFormLink = "https://quickchart.io/qr?text=" + URLConstant.Base.GET_LINK + encryptedVal + "=&centerImageUrl=https://play-lh.googleusercontent.com/y7A-hlJPYg9k_b1eSAkZlsIyxKIwjIkuDXK5k4CKblLhzTGPSr42algKOaNECMOdJ84&size=500";
                            // String strgetFormLink = URLConstant.Base.GET_LINK;

                            if (!strgetFormLink.isEmpty()) {
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "FieldWeb : Enquiry Form Link");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, strgetFormLink);
                                DownloadFile.downloadFile(strgetFormLink, getActivity());
                                //activity.startActivity(Intent.createChooser(sharingIntent, "Share"));
                            }
                        }
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            });

            button_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private String enryptUserID(String[] userID) {
        String encrypted = "";
        /*String sourceStr = userID;
        try {
            encrypted = AESUtils.encrypt(sourceStr);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
        String EncUID = "";
        for (int i = 0; i < userID.length; i++) {
            EncUID += Encrypt(userID[i]);
        }
        return EncUID;
    }

    public String Encrypt(String UseridInput) {
        String EncString = "";
        switch (UseridInput) {
            case "0":
                EncString = "AZ=";
                break;


            case "1":
                EncString = "BY/";
                break;


            case "2":
                EncString = "CX=";
                break;


            case "3":
                EncString = "DW/";
                break;


            case "4":
                EncString = "EV=";
                break;


            case "5":
                EncString = "FU/";
                break;


            case "6":
                EncString = "G-=";
                break;


            case "7":
                EncString = "/=/";
                break;


            case "8":
                EncString = "=/=";
                break;


            case "9":
                EncString = "A=/";
                break;
        }
        return EncString;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public void saveUpdateEnquiry(AddEnquiry.ResultData addEnquiry) {
        mUpdateEnquiryAsyncTask = new UpdateEnquiryAsyncTask(getActivity(), BaseAsyncTask.Priority.LOW, this);
        mUpdateEnquiryAsyncTask.execute(addEnquiry);
    }

    public void updateEnquiry(EnquiryList.ResultData resultData) {
        /*Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_add_enquiry));
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);*/

        EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
        enquiryDialog.updateEnquiryDialog(getActivity(), ((HomeActivityNew) getActivity()).mServiceTypeResultData, resultData, this);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(EnquiryList.class.getSimpleName())) {
            Gson gson = new Gson();
            EnquiryList enquiryList = gson.fromJson(urlConnectionResponse.resultData, EnquiryList.class);
            if (enquiryList.getCode().equalsIgnoreCase("200") && enquiryList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + enquiryList.getMessage());
                if (enquiryList != null) {
                    mEnquiryLists = new ArrayList<>();
                    mEnquiryLists = enquiryList.getResultData();
                    setData();
                }
            }
        } else if (classType.equalsIgnoreCase(CustomerList.class.getSimpleName())) {
            Gson gson = new Gson();
            CustomerList customerList = gson.fromJson(urlConnectionResponse.resultData, CustomerList.class);
            if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                if (customerList != null) {
                    mCustomerList = new ArrayList<>();
                    mCustomerList = customerList.getResultData();
                    setData();
                }
            }
        } else if (classType.equalsIgnoreCase(AddEnquiry.class.getSimpleName())) {
            Gson gson = new Gson();
            AddEnquiry addEnquiry = gson.fromJson(urlConnectionResponse.resultData, AddEnquiry.class);
            //NOTE: Log GA event
            /*Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, addEnquiry.getResultData().getMobileNumber());
            FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_ENQ, bundle);*/

            //Reload Enquiry list to reflect updated details
            getEnquiryList();
            Toast.makeText(getActivity(), addEnquiry.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        if (mSelection.equalsIgnoreCase("ENQ")) {
            mRecyclerViewList.setVisibility(View.VISIBLE);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewList.setLayoutManager(mLayoutManager);
            mEnquiryListAdapter = new EnquiryListAdapterNew(mEnquiryLists, getContext(), this);
            if (mEnquiryLists != null && mEnquiryListAdapter != null)
                mEnquiryListAdapter.setClickListener(this);
            mRecyclerViewList.setAdapter(mEnquiryListAdapter);
        } else {
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewList.setLayoutManager(mLayoutManager);
            mCustomerListAdapter = new CustomerListAdapterNew(mCustomerList, getContext(), this);
//            mCustomerListAdapter.setClickListener(this);
            mRecyclerViewList.setAdapter(mCustomerListAdapter);
        }
        setSearchFilter();
    }

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);

        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mSelection.equalsIgnoreCase("ENQ")) {
                    // filter recycler view when text is changed
                    if (mEnquiryLists != null && mEnquiryListAdapter != null)
                        mEnquiryListAdapter.getFilter().filter(query);
                } else {
                    if (mCustomerListAdapter != null)
                        mCustomerListAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mSelection.equalsIgnoreCase("ENQ")) {
                    // filter recycler view when text is changed
                    if (mEnquiryLists != null && mEnquiryListAdapter != null)
                        mEnquiryListAdapter.getFilter().filter(query);
                } else {
                    if (mCustomerListAdapter != null)
                        mCustomerListAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.enquiry_card_view:
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_DETAILS, bundle);

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.enquiryDetailsDialog(getActivity(), mEnquiryListAdapter.getItem(position));
                break;

            case R.id.imageView_call:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mCustomerListAdapter.getItem(position).getMobileNumber()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    try {
                        String number = mCustomerListAdapter.getItem(position).getMobileNumber();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_call_cust:
                if (mEnquiryLists != null && mEnquiryListAdapter != null) {
                    resultData = mEnquiryListAdapter.getItem(position);
                    if (resultData.getMobileNumber() != null && !resultData.getMobileNumber().isEmpty()) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + resultData.getMobileNumber()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.imageView_edit:
                if (mEnquiryLists != null && mEnquiryListAdapter != null) {
                    resultData = mEnquiryListAdapter.getItem(position);
//                ((HomeActivityNew) getActivity()).addEnquiry();
//                ((HomeActivityNew) getActivity()).updateEnquiry(resultData);
                    getEnquiryServiceType();
                    /*updateEnquiry(resultData);*/
                }
                break;

            default:
                break;
        }

    }

    @Override
    public void onUpdateClick(View view, int position) {

    }

    public void getEnquiryServiceType() {
        /*mGetServiceTypeAsyncTask = new GetServiceTypeAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetServiceTypeAsyncTask.execute();*/
        try {
            CommonFunction.hideProgressDialog(getActivity());
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<EnquiryServiceTypeDTO> call = RetrofitClient.getInstance(getActivity()).getMyApi().getEnquiryServiceList(userID,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<EnquiryServiceTypeDTO>() {
                    @Override
                    public void onResponse(Call<EnquiryServiceTypeDTO> call, Response<EnquiryServiceTypeDTO> response) {
                        try {
                            if (response.code() == 200) {
                                EnquiryServiceTypeDTO serviceType = response.body();
                                FWLogger.logInfo(TAG, "Received services type Successfully");
                                ((HomeActivityNew) getActivity()).mServiceTypeResultData = new ArrayList<>();
                                ((HomeActivityNew) getActivity()).mServiceTypeResultData = serviceType.getResultData();
                                updateEnquiry(resultData);
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EnquiryServiceTypeDTO> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "Service Type API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in Service Type API :");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Service Type API :");
            ex.getMessage();
        }
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }
}
