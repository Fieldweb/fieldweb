package com.corefield.fieldweb.FieldWeb.Task;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.TasksListAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks
 *
 * @author CoreField
 * @version 1.4.0
 * @implNote This Fragment class is used to show list of task according to task type and status
 */
@SuppressLint("ValidFragment")
public class MainTaskFragmentNew extends Fragment implements OnTaskCompleteListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = MainTaskFragmentNew.class.getSimpleName();
    private View mRootView;
    private List<TasksList.ResultData> mTasksLists;
    private TasksListAdapter mTasksListAdapter;
    private RecyclerView mTaskListRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerTouchListener mRecyclerTouchListener;
    private String mUserGroup = "";
    private int mTaskTypeId = 0, mTaskStatusId = 0;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START;
    int mTypeCheck = 0, mStatusCheck = 0, mMonthYearCheck = 0;
    private boolean isLoadFirstTime = false;
    private String mSearchParam = "";
    private TextView mTextViewRefresh;
    private boolean mSearchFilter = false;
    TasksList.ResultData mResultData;
    SearchView mSearchView;
    private boolean isAllData = false;
    private Calendar mCal;
    private int mYear, mMonth, mDay;
    public static Context mcontext;
    Button plustask;
    String currentMonthName = "";
    ImageView noResultImg;

    @SuppressLint("ValidFragment")
    public MainTaskFragmentNew() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onCreateView");
        mRootView = inflater.inflate(R.layout.main_task_fragment, container, false);
        FWLogger.logInfo(TAG, "Loaded successfully");
        mUserGroup = SharedPrefManager.getInstance(getContext()).getUserGroup();
        mTaskListRecyclerView = mRootView.findViewById(R.id.recycler_taskList);

        noResultImg = mRootView.findViewById(R.id.noResultImg);

        mCal = Calendar.getInstance();
        mYear = mCal.get(Calendar.YEAR);
        mMonth = mCal.get(Calendar.MONTH);
        mDay = mCal.get(Calendar.DAY_OF_MONTH);

        //((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);


        mTaskListRecyclerView.setVisibility(View.VISIBLE);
        mTasksListAdapter = new TasksListAdapter(getContext(), mTasksLists);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mTaskListRecyclerView.setLayoutManager(mLayoutManager);
        mTaskListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mTaskListRecyclerView.setAdapter(mTasksListAdapter);


//        ((HomeActivityNew) getActivity()).getMonthYearList();
//
//        ((HomeActivityNew) getActivity()).spinnerMonthYear.setSelection(0, false);

        currentMonthName = DateUtils.getMonthName(mMonth);

        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setText(currentMonthName + " " + mYear);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, ((HomeActivityNew) getActivity()).currentYear, ((HomeActivityNew) getActivity()).currentMonth, ((HomeActivityNew) getActivity()).currentDay) {
            //DatePickerDialog dpd = new DatePickerDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT,this,year, month, day){
            // DatePickerDialog dpd = new DatePickerDialog(getActivity(), AlertDialog.THEME_TRADITIONAL,this,year, month, day){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int day = getContext().getResources().getIdentifier("android:id/day", null, null);
                if (day != 0) {
                    View dayPicker = findViewById(day);
                    if (dayPicker != null) {
                        //Set Day view visibility Off/Gone
                        dayPicker.setVisibility(View.GONE);
                    }
                }
            }
        };
        mCal.add(Calendar.DATE, 0);
        datePickerDialog.getDatePicker().setMaxDate(mCal.getTimeInMillis());
        mCal.add(Calendar.MONTH, -2);
        datePickerDialog.getDatePicker().setMinDate(mCal.getTimeInMillis());

        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        /*new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {*/
        /*((HomeActivityNew) getActivity()).mSpinnerMonthYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++mMonthYearCheck > 1) {
                    FWLogger.logInfo(TAG, "mSpinnerMonthYear_Count");
                    if (mTasksLists != null && getContext() != null) clearTaskList();
                    FWLogger.logInfo(TAG, "onItemSelected called : " + position);
                    String selectedMonthYear = parent.getItemAtPosition(position).toString();
                    FWLogger.logInfo(TAG, "selectedTaskType called : " + selectedMonthYear);
                    ((HomeActivityNew) getActivity()).mSelectedMonth = ((HomeActivityNew) getActivity()).monthList.get(position);
                    FWLogger.logInfo(TAG, "Month Number : " + ((HomeActivityNew) getActivity()).mSelectedMonth + "  " + selectedMonthYear.split(" ")[1]);
                    ((HomeActivityNew) getActivity()).mSelectedYear = Integer.parseInt(selectedMonthYear.split(" ")[1]);

                    if (mTaskTypeId == 0 && mTaskStatusId == 0 && ((HomeActivityNew) getActivity()).mSelectedMonth == ((HomeActivityNew) getActivity()).currentMonth)
                        isAllData = false;
                    else
                        isAllData = true;

                    getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/
            /*}
        });*/

        mSearchView = mRootView.findViewById(R.id.searchView_CustomerNo);
        plustask = mRootView.findViewById(R.id.plus_task);
        mSearchView.setInputType(InputType.TYPE_CLASS_NUMBER);

        if (mUserGroup.equalsIgnoreCase("Owner")) {
            plustask.setVisibility(View.VISIBLE);
        } else if (mUserGroup.equalsIgnoreCase("Technician")) {
            plustask.setVisibility(View.GONE);
        }

        List<String> typeArray = new ArrayList<>();
        typeArray.add("Type");
        typeArray.add(Constant.TaskCategories.Urgent.name());
        typeArray.add(Constant.TaskCategories.Today.name());
        typeArray.add(Constant.TaskCategories.Schedule.name());

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, typeArray) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.WHITE);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.WHITE);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount();
            }
        };
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((HomeActivityNew) getActivity()).mTaskTypeSpinner.setAdapter(typeAdapter);

        ((HomeActivityNew) getActivity()).mTaskTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++mTypeCheck > 1) {
                    if (mTasksLists != null) clearTaskList();
                    mTaskTypeId = position;
                    mSearchView.setQuery("", false);
                    mTextViewRefresh.setVisibility(View.GONE);
                    mSearchParam = "";
                    if (mTaskTypeId == 0 && mTaskStatusId == 0 && ((HomeActivityNew) getActivity()).mSelectedMonth == ((HomeActivityNew) getActivity()).currentMonth)
                        isAllData = false;
                    else
                        isAllData = true;

                    getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> statusArray = new ArrayList<>();
        statusArray.add("Status");
        statusArray.add(Constant.TaskCategories.Completed.name());
        statusArray.add(Constant.TaskCategories.Rejected.name());
        statusArray.add(Constant.TaskCategories.Ongoing.name());
        statusArray.add(Constant.TaskCategories.InActive.name());
        statusArray.add(Constant.TaskCategories.OnHold.name());

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, statusArray) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.WHITE);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.WHITE);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount();
            }
        };
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((HomeActivityNew) getActivity()).mTaskStatusSpinner.setAdapter(statusAdapter);

        ((HomeActivityNew) getActivity()).mTaskStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++mStatusCheck > 1) {
                    if (mTasksLists != null) clearTaskList();
                    mTaskStatusId = position;
                    mSearchView.setQuery("", false);
                    mTextViewRefresh.setVisibility(View.GONE);
                    mSearchParam = "";

                    if (mTaskTypeId == 0 && mTaskStatusId == 0 && ((HomeActivityNew) getActivity()).mSelectedMonth == ((HomeActivityNew) getActivity()).currentMonth)
                        isAllData = false;
                    else
                        isAllData = true;
                    getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mTaskListRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        mSearchParam = "";
        mockingNetworkDelay(mSearchParam, mTaskTypeId, mTaskStatusId);

        AutoCompleteTextView search_text = (AutoCompleteTextView) mSearchView.findViewById(mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        search_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.list_title));
        search_text.setGravity(Gravity.CENTER_VERTICAL);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(10);
        search_text.setFilters(filterArray);
        mSearchView.setGravity(Gravity.CENTER_VERTICAL);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
            }
        });
        setSearchFilter();

        plustask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) getActivity()).addTask();
            }
        });

        mTextViewRefresh = mRootView.findViewById(R.id.textView_refresh);
        mTextViewRefresh.setVisibility(View.GONE);
        mTextViewRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextViewRefresh.setVisibility(View.GONE);
                if (mTasksLists != null) clearTaskList();
                mSearchParam = "";
                mSearchView.setQuery("", false);
                mockingNetworkDelay(mSearchParam, 0, 0);
            }
        });

        mRecyclerTouchListener = new RecyclerTouchListener() {
            @Override
            public void onClick(View view, int position) {
                //NOTE: Log GA event
                Bundle GABundle = new Bundle();
                GABundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTasksListAdapter.getItem(position).getId());
                GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_TYPE, mTasksListAdapter.getItem(position).getTaskType());
                GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, mTasksListAdapter.getItem(position).getTaskStatus());
                GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, mTasksListAdapter.getItem(position).getTaskState());
                GABundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                GABundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                GABundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                GABundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TASK_CLICK, GABundle);
                if (view.getId() == R.id.textView_reassign_from_list) {
                    if (mTasksListAdapter.getItem(position).getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name())) {
                        ((HomeActivityNew) getActivity()).openUpdateTaskDialog(mTasksListAdapter.getItem(position));
                    } else if (mTasksListAdapter.getItem(position).getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.Completed.name())) {
                        ((HomeActivityNew) getActivity()).openReAssignCompletedTaskDialog(mTasksListAdapter.getItem(position));
                    } else if (mTasksListAdapter.getItem(position).getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.OnHold.name())) {
                        ((HomeActivityNew) getActivity()).openReAssignOnHoldTaskDialog(mTasksListAdapter.getItem(position));
                    } else {
                        ((HomeActivityNew) getActivity()).addTaskWithReassignDefaults(mTasksListAdapter.getItem(position));
                    }
                } else if (mUserGroup.equalsIgnoreCase("Owner")) {
                    if (mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Completed.name())) {
                        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
                        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
                        TaskDetailsFragmentNew taskDetailsFragment = new TaskDetailsFragmentNew();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("downloadReport", true);
                        bundle.putBoolean("allowToReassign", true);
                        bundle.putSerializable("taskResult", mTasksListAdapter.getItem(position));
                        taskDetailsFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskDetailsFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else if (mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Rejected.name())) {
                        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
                        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
                        TaskDetailsFragmentNew taskDetailsFragment = new TaskDetailsFragmentNew();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("allowToReassign", true);
                        bundle.putSerializable("taskResult", mTasksListAdapter.getItem(position));
                        taskDetailsFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskDetailsFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
                        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
                        OwnerTaskTrackingFragmentNew ownerTaskTrackingFragment = new OwnerTaskTrackingFragmentNew();
                        TasksList.ResultData data = mTasksListAdapter.getItem(position);
                        Bundle bundle = new Bundle();
                        if (data != null) {
                            bundle.putInt("TaskId", data.getId());
                            bundle.putString("Task Status", data.getTaskStatus());
                            bundle.putInt("TaskStatusId", data.getTaskStatusId());
                            bundle.putString("Task Type", data.getTaskType());
                            bundle.putInt("TaskTypeId", data.getTaskTypeId());
                            bundle.putString("Task Name", data.getName());
                            bundle.putString("Task Assignee", data.getAssignedTo());
                            bundle.putInt("Task Amount", data.getWagesPerHours());
                            bundle.putString("Image", "a");
                            bundle.putString("FieldLat", data.getLatitude());
                            bundle.putString("FieldLong", data.getLongitude());
                            bundle.putString("Photo", data.getPhoto());
                            //fake location
                            bundle.putString("TechLat", data.getTechLatitude());
                            bundle.putString("TechLong", data.getTechLongitude());
                            bundle.putString("ContactNo", data.getContactNo());
                            bundle.putString("CustomerName", data.getCustomerName());
                            bundle.putInt("TaskState", data.getTaskState());
                            bundle.putInt("PaymentModeId", data.getPaymentModeId());
                            bundle.putString("FullAddress", data.getFullAddress());
                            bundle.putString("LocDescription", data.getLocationDesc());
                            bundle.putInt("UserId", data.getUserId());
                            // ON HOLD DATA
                            if (data.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.onhold))) {
                                bundle.putString("OnHoldNotes", data.getOnHoldTaskDtos().getOnHoldNotes());
                                bundle.putString("CreatedDate", data.getOnHoldTaskDtos().getCreatedDate());
                                bundle.putString("Pick1", data.getOnHoldTaskDtos().getPick1());
                                bundle.putString("Pick2", data.getOnHoldTaskDtos().getPick2());
                                bundle.putString("Pick3", data.getOnHoldTaskDtos().getPick3());
                                bundle.putSerializable("taskResult", mTasksListAdapter.getItem(position));
                            }
                            if (data.getMultipleItemAssigned().size() > 0) {
                                List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
                                multipleItemAssignedArrayList = data.getMultipleItemAssigned();
                                bundle.putSerializable("MultiItemResult", (Serializable) multipleItemAssignedArrayList);
                            }

                        }
                        bundle.putBoolean("navigateToTaskTab", true);
                        ownerTaskTrackingFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, ownerTaskTrackingFragment);
                        //NOTE : Do not add to back stack as we are navigating from back press in OwnerTaskTrackingFragment
                        //transaction.isAddToBackStackAllowed();
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                } else if (mUserGroup.equalsIgnoreCase("Technician")) {
                    plustask.setVisibility(View.GONE);
                    mResultData = mTasksListAdapter.getItem(position);
                    Bundle bundle = new Bundle();
                    if (mResultData != null) {
                        bundle.putString("Task Status", mResultData.getTaskStatus());
                        bundle.putString("Task Name", mResultData.getName());
                        bundle.putString("Task Assignee", mResultData.getAssignedTo());
                        bundle.putInt("Task Amount", mResultData.getWagesPerHours());
                        bundle.putString("Image", "a");
                        bundle.putString("FieldLat", mResultData.getLatitude());
                        bundle.putString("FieldLong", mResultData.getLongitude());
                        bundle.putString("TaskDescription", mResultData.getDescription()); // task desc
                        bundle.putString("AddressDesc", mResultData.getLocationDesc()); // Address desc
                        bundle.putString("ShortAddress", mResultData.getLocationName()); //TODO error
                        bundle.putString("ContactNo", mResultData.getContactNo());
                        bundle.putString("CustomerName", mResultData.getCustomerName());
                        bundle.putInt("TaskId", mResultData.getId());
                        bundle.putInt("TaskStatusId", mResultData.getTaskStatusId());
                        bundle.putLong("StartDate", mResultData.getStartDate());
                        bundle.putLong("EndDate", mResultData.getEndDate());
                        bundle.putInt("TaskState", mResultData.getTaskState());
                        bundle.putString("FullAddress", mResultData.getFullAddress());
                        bundle.putString("PaymentMode", mResultData.getPaymentMode());
                        bundle.putInt("PaymentModeId", mResultData.getPaymentModeId());
                        bundle.putString("TaskDate", mResultData.getTaskDate());
                        bundle.putInt("OwnerId", mResultData.getOwnerId());
                        bundle.putString("CustomerEmailId", mResultData.getCustomerEmailId());

                        // FOR MULTIPLE ITEM
                       /* try {
                            ArrayList<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
                            if (mResultData.getMultipleItemAssigned().size() > 0) {
                                for (int i = 0; i < mResultData.getMultipleItemAssigned().size(); i++) {
                                    TasksList.MultipleItemAssigned multipleItemAssigned = new TasksList.MultipleItemAssigned();
                                    multipleItemAssigned.setItemId(mResultData.getMultipleItemAssigned().get(i).getItemId());
                                    multipleItemAssigned.setItemQuantity(mResultData.getMultipleItemAssigned().get(i).getItemQuantity());
                                    multipleItemAssigned.setItemName(mResultData.getMultipleItemAssigned().get(i).getItemName());
                                    multipleItemAssignedArrayList.add(multipleItemAssigned);
                                }
                                bundle.putSerializable("MultipleItemAssigned", multipleItemAssignedArrayList);
                            }

                        } catch (Exception ex) {
                            ex.getMessage();
                        }*/

                        FWLogger.logInfo("PaymentMode : ", " " + mResultData.getPaymentMode());
                        FWLogger.logInfo("PaymentModeId : ", " " + mResultData.getPaymentModeId());
                    }
                    //Check for PNR(Payment Not Received)
                    FWLogger.logInfo(TAG, "getPaymentNotReceived  = " + mResultData.getPaymentNotReceived());

                    //NOTE :
                    // If task is  the open TechTaskTrackingFragment
                    // TASKSTATE 0 : Task not stated then open TechTaskTrackingFragment
                    // TASKSTATE 1 : Task started but not ended then open TechTaskTrackingFragment
                    // TASKSTATE 2 : Task Ended but payment not rec then open TechPaymentReceviedFragment directly
                    // TASKSTATE 3 : Show the pop  that task has been already completed

                    if (mResultData.getTaskState() == TaskState.NOT_STARTED || mResultData.getTaskState() == TaskState.STARTED_NOT_ENDED) {
                        if (!mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Rejected.name())) {
                            if (mResultData.getTaskStatusId() == TaskStatus.IN_ACTIVE && mResultData.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name())) {
                                /*if (DateUtils.compareTo(mResultData.getTaskDate()) != 1) {
                                    ((HomeActivityNew) getActivity()).setupAlarm(900000);
                                    TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                    taskDialogNew.acceptTaskDialog(getActivity(), mResultData, TaskStatus.IN_ACTIVE);
                                } else
                                    Toast.makeText(getActivity(), R.string.task_expired, Toast.LENGTH_SHORT).show();*/
                                // START
                                Date c = Calendar.getInstance().getTime();
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                String strCurDate = df.format(c);
                                String strTaskDate[] = mResultData.getTaskDate().split("T");
                                try {
                                    Date date1; // Task Date
                                    Date date2; // Current Date
                                    SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
                                    date1 = dates.parse(strTaskDate[0]);
                                    date2 = dates.parse(strCurDate);
                                    long difference = Math.abs(date1.getTime() - date2.getTime());
                                    //long differenceDates = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
                                    long differenceDates = difference / (24 * 60 * 60 * 1000);
                                    //String dayDifference = Long.toString(differenceDates);

                                    if (differenceDates > 3 && date1.compareTo(date2) < 0) { // date 1 occurs after date 2
                                        Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                    } else if (date1.compareTo(date2) > 0) { // FUTURE DATE
                                        Toast.makeText(getActivity(), "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                                    } else if (differenceDates == 0 || differenceDates == 1 || differenceDates == 2 || differenceDates == 3
                                            && date1.compareTo(date2) <= 0 && differenceDates <= 3) { //FOR PAST DATE AND CURRENT DATE ACCEPT:: IF task date is before than current
                                        ((HomeActivityNew) getActivity()).setupAlarm(900000);
                                        TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                        taskDialogNew.acceptTaskDialog(getActivity(), mResultData, TaskStatus.IN_ACTIVE);
                                    } else if (differenceDates < 3 && date1.compareTo(date2) < 0) {
                                        Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                    } else if (differenceDates >= 1) {
                                        Toast.makeText(getActivity(), "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                                    } else if (differenceDates <= 1) {
                                        Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //END
                            } else if (mResultData.getTaskStatusId() == TaskStatus.ON_GOING && mResultData.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.Ongoing.name())) {
                                ((HomeActivityNew) getActivity()).navigateToTaskNavigation(mResultData);
                            }
                        }
                    }

                    /*if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
                        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        taskClosureFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                        transaction.commit();
                    }
                    if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        techPaymentReceivedFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        //FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        //NOTE : Do not add to back stack as we are navigating from back press in TechTaskTrackingFragment and TechPaymentReceivedFragment
                        transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
                        transaction.commit();
                    }*/
                    if (mResultData.getPaymentMode() != null && mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getTaskStatusId() == TaskStatus.ON_GOING && mResultData.isTaskClosureStatus() == true
                            && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        techPaymentReceivedFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        //FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        //NOTE : Do not add to back stack as we are navigating from back press in TechTaskTrackingFragment and TechPaymentReceivedFragment
                        transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
                        transaction.commit();
                    } else if (mResultData.getPaymentMode() != null && mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getTaskStatusId() == TaskStatus.ON_GOING && mResultData.isTaskClosureStatus() == false
                            && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        taskClosureFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                        transaction.commit();
                    }
                    if (mResultData.getPaymentMode() != null && mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
                        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        taskClosureFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                        transaction.commit();
                    }
                    if (mResultData.getTaskState() == TaskState.PAYMENT_RECEIVED && !mResultData.isTaskClosureStatus() && !mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Completed.name())) {
                        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                        bundle.putSerializable("taskResult", mResultData);
                        taskClosureFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                        transaction.commit();
                    } else if (mResultData.getTaskState() == TaskState.PAYMENT_RECEIVED && mResultData.isTaskClosureStatus()) {
                        Toast.makeText(getContext(), R.string.this_task_completed_and_payment_is_received, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        mTasksListAdapter.setClickListener(mRecyclerTouchListener);

        return mRootView;
    }

    private String getMonthName() {
        String[] mons = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        String currentMonthName = mons[mMonth];
        return currentMonthName;
    }

    private void mockingNetworkDelay(String searchParam, int typeId, int statusId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getTaskList(searchParam, typeId, statusId, isAllData);
            }
        }, 1000);
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
    }

    protected void getTaskList(String searchParam, int taskTypeId, int statusId, boolean isAllData) {
        FWLogger.logInfo(TAG, "Count Task List");
        if (getContext() != null) {
            /*mGetTaskListSearchByParamAsyncTask = new GetTaskListSearchByParamAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, MainTaskFragmentNew.this);
            mGetTaskListSearchByParamAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), searchParam, taskTypeId, mCurrentPage, statusId, mMonth + 1, mYear, isAllData);*/

            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    CommonFunction.showProgressDialog(getActivity());
                    Call<TasksList> call = RetrofitClient.getInstance(getContext()).getMyApi().getTaskListSearch(SharedPrefManager.getInstance(getContext()).getUserId(),
                            searchParam, statusId, taskTypeId, mCurrentPage, mMonth + 1, mYear, isAllData);
                    call.enqueue(new retrofit2.Callback<TasksList>() {
                        @Override
                        public void onResponse(Call<TasksList> call, Response<TasksList> response) {
                            try {
                                if (response.code() == 200) {
                                    TasksList tasksList = response.body();
                                    if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                                        if (tasksList != null) {
                                            CommonFunction.hideProgressDialog(getActivity());
                                            FWLogger.logInfo(TAG, "success");
                                            if (mSearchFilter && tasksList.getResultData().size() == 0) {
                                                mTasksListAdapter.removeLoadingFooter();
                                                //Toast.makeText(getContext(), R.string.no_record_found, Toast.LENGTH_LONG).show();
                                                mTaskListRecyclerView.setVisibility(View.GONE);
                                                noResultImg.setVisibility(View.VISIBLE);
                                                noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                                mTextViewRefresh.setVisibility(View.VISIBLE);
                                            } else {
                                                setResults(tasksList);
                                            }
                                            mSearchFilter = false;
                                        }
                                    } else {
                                        Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
                                    }

                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<TasksList> call, Throwable throwable) {
                            CommonFunction.hideProgressDialog(getActivity());
                            FWLogger.logInfo(TAG, "Exception in GetTaskListSearchByParamV2New? API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in GetTaskListSearchByParamV2New? API:");
                ex.getMessage();
            }


        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(TasksList.class.getSimpleName())) {
                Gson gson = new Gson();
                TasksList tasksList = gson.fromJson(urlConnectionResponse.resultData, TasksList.class);

                if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                    if (tasksList != null) {
                        FWLogger.logInfo(TAG, "success");
                        if (mSearchFilter && tasksList.getResultData().size() == 0) {
                            mTasksListAdapter.removeLoadingFooter();
                            Toast.makeText(getContext(), R.string.no_record_found, Toast.LENGTH_LONG).show();
                            mTextViewRefresh.setVisibility(View.VISIBLE);
                        } else {
                            setResults(tasksList);
                        }
                        mSearchFilter = false;
                    }
                } else {
                    Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void setResults(TasksList tasksList) {
        mTasksLists = new ArrayList<>();
        mTasksLists = tasksList.getResultData();
        if (isLoadFirstTime) {
            if (mTasksLists == null || mTasksLists.isEmpty() || mTasksLists.size() <= 0) {
                mTaskListRecyclerView.setVisibility(View.GONE);
                noResultImg.setVisibility(View.VISIBLE);
                noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                isLoading = false;
                isLastPage = true;
            } else {
                mTaskListRecyclerView.setVisibility(View.VISIBLE);
                mTasksListAdapter.addAll(mTasksLists);
                mTasksListAdapter.notifyDataSetChanged();
                mTasksListAdapter.addLoadingFooter();
            }
        } else {
            if (mTasksLists == null || mTasksLists.isEmpty() || mTasksLists.size() <= 0) {
                mTasksListAdapter.removeLoadingFooter();
                isLoading = false;
                isLastPage = true;
            } else {
                mTasksListAdapter.removeLoadingFooter();
                isLoading = false;
                mTasksListAdapter.addAll(mTasksLists);
                mTasksListAdapter.notifyDataSetChanged();
                mTasksListAdapter.addLoadingFooter();
            }
        }
    }

    public void clearTaskList() {
        int size = mTasksLists.size();
        mTasksLists.clear();
        mTasksListAdapter.notifyItemRangeRemoved(0, size);

        // For loading data after selecting task type from spinner
        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        mSearchParam = "";

        mTasksListAdapter = new TasksListAdapter(getContext(), mTasksLists);
        mTaskListRecyclerView.setAdapter(mTasksListAdapter);
        mTasksListAdapter.setClickListener(mRecyclerTouchListener);
        mTasksListAdapter.notifyDataSetChanged();
    }

    private void setSearchFilter() {
        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null && isValidPhone(query)) {
                    if (mTasksLists != null) clearTaskList();
                    mTextViewRefresh.setVisibility(View.GONE);
                    mSearchParam = query;
                    mSearchFilter = true;
                    mockingNetworkDelay(query, 0, 0);
                } else {
                    Toast.makeText(getContext(), R.string.please_enter_valid_contact_number, Toast.LENGTH_SHORT).show();
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }

        });
    }

    private boolean isValidPhone(String phone) {
        /*return phone.length() == 10;*/
        return phone.length() >= 7;
    }

    @Override
    public void onResume() {
        FWLogger.logInfo(TAG, "OnResume");
        super.onResume();
        /*if(((HomeActivityNew) getActivity()).spinnerMonthYear != null) {
            ((HomeActivityNew) getActivity()).spinnerMonthYear.post(new Runnable() {
                @Override
                public void run() {
                ((HomeActivityNew) getActivity()).spinnerMonthYear.setSelection(0);
//                    ((HomeActivityNew) getActivity()).getMonthYearList();
//                spinnerAdapter.notifyDataSetChanged();
                }
            });
        }*/
    }

    @Override
    public void onAttach(Context context) {
        FWLogger.logInfo(TAG, "onAttach");
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        FWLogger.logInfo(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onPause() {
        FWLogger.logInfo(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mDay = dayOfMonth;
        mMonth = month;
        mYear = year;
        FWLogger.logInfo(TAG, "mMonth : " + mMonth + " " + mYear);

        if (mTaskTypeId == 0 && mTaskStatusId == 0 && mMonth == ((HomeActivityNew) getActivity()).currentMonth)
            isAllData = false;
        else
            isAllData = true;

        if (mTasksLists != null && getContext() != null) clearTaskList();

        currentMonthName = DateUtils.getMonthName(mMonth);
        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setText(currentMonthName + " " + mYear);

        getTaskList(mSearchParam, mTaskTypeId, mTaskStatusId, isAllData);
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        ((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.VISIBLE);
        mSearchView.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).textViewUsername.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.GONE);
//        ((HomeActivityNew) getActivity()).getMonthYearList();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        FWLogger.logInfo(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        FWLogger.logInfo(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onStop() {
        FWLogger.logInfo(TAG, "onStop");
        super.onStop();
    }
}