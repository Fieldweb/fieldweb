package com.corefield.fieldweb.FieldWeb.AssetManagement;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.FollowUpLeadHistoryListAdapter;
import com.corefield.fieldweb.Adapter.ProductFeatureListAdapter;
import com.corefield.fieldweb.Adapter.ProductHistoryListAdapter;
import com.corefield.fieldweb.DTO.LeadManagement.DeleteLeadDetailsDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadDetailsDTO;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.AssetManagement.QRCodeScanner.qrcodescanner.QrCodeActivity;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.LeadManagement.AddEditLeadDialog;
import com.corefield.fieldweb.FieldWeb.LeadManagement.LeadManagementFragment;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks Details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show completed task details
 */
@SuppressLint("ValidFragment")
public class AssetDetailsFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, BaseActivity.RequestPermissionsUpdateListener {

    public static String TAG = AssetDetailsFragment.class.getSimpleName();
    private String mFieldImageURL = "", mFieldImageURL1 = "", mFieldImageURL2 = "", mImageBeforeTaskURL = "", mImageBeforeTaskURL1 = "", mImageBeforeTaskURL2 = "";
    private View mRootView;
    private int leadID;
    public List<LeadDetailsDTO.ResultData> mLeadList = null;
    private TextView mtxtLeadStatus, mtxtCustName, mtxtStartDateTime, mtxtCustNo, mtxtCustAddress, mtxtLandmark, mtxtServiceDetails, mtxtNotes, mattchedPhotoLabel;
    private SupportMapFragment mMapFragment;
    private ImageView mbtnDeletLeadDetails, mbtnCallCustno;
    private GoogleMap mGoogleMap;
    private Marker mMarkerDestination;
    private double mDestLat = 0, mDestLong = 0;
    private RecyclerView mproductFeatureList, mproductHistoryList;
    private LinearLayoutManager mLayoutManager;
    Fragment fragment;
    private ProductFeatureListAdapter productFeatureListAdapter;
    private ImageView leadImg1, leadImg2, leadImg3;
    LinearLayout leadLnlLayout;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    public static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private ProductHistoryListAdapter productHistoryListAdapter;

    @SuppressLint("ValidFragment")
    public AssetDetailsFragment() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.asset_details_fragment, container, false);

        inIt();

        //checkStoragePermission();

        return mRootView;
    }

    private void inIt() {

        mtxtCustName = mRootView.findViewById(R.id.txtCustName);
        mtxtStartDateTime = mRootView.findViewById(R.id.txtStartDateTime);
        mtxtCustNo = mRootView.findViewById(R.id.txtCustNo);
        mtxtCustAddress = mRootView.findViewById(R.id.txtCustAddress);
        mtxtLandmark = mRootView.findViewById(R.id.txtLandmark);
        mtxtServiceDetails = mRootView.findViewById(R.id.txtServiceDetails);
        mtxtNotes = mRootView.findViewById(R.id.txtNotes);
        mtxtLeadStatus = mRootView.findViewById(R.id.txtLeadStatus);
        mproductFeatureList = mRootView.findViewById(R.id.productFeatureList);
        mbtnDeletLeadDetails = mRootView.findViewById(R.id.btnDeletLeadDetails);
        mproductHistoryList = mRootView.findViewById(R.id.productHistoryList);
        //
        leadLnlLayout = mRootView.findViewById(R.id.leadLnlLayout);
        leadImg1 = mRootView.findViewById(R.id.leadImg1);
        leadImg2 = mRootView.findViewById(R.id.leadImg2);
        leadImg3 = mRootView.findViewById(R.id.leadImg3);
        mattchedPhotoLabel = mRootView.findViewById(R.id.attchedPhotoLabel);
        mbtnCallCustno = mRootView.findViewById(R.id.btnCallCustno);

        mbtnDeletLeadDetails.setOnClickListener(this);
        mbtnCallCustno.setOnClickListener(this);


        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.task_details_map);
        mMapFragment.getMapAsync(this);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                leadID = getBundle.getInt("leadID");
                getLeadDetails(leadID);
                fragment = this;
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        checkCameraPermission();

    }

    private void getLeadDetails(int leadId) {
        try {
            if (leadId != 0) {
                if (getContext() == null) FWLogger.logInfo(TAG, "getContext is null");
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    CommonFunction.showProgressDialog(getActivity());
                    int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    Call<LeadDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getLeadDetailsById(userID, leadId,
                            "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<LeadDetailsDTO>() {
                        @Override
                        public void onResponse(Call<LeadDetailsDTO> call, Response<LeadDetailsDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    CommonFunction.hideProgressDialog(getActivity());
                                    FWLogger.logInfo(TAG, "Get Lead Details by Id");
                                    mLeadList = new ArrayList<>();
                                    LeadDetailsDTO leadDetailsDTO = response.body();
                                    mLeadList = leadDetailsDTO.getResultData();
                                    if (mLeadList.size() > 0) {
                                        setData(mLeadList);
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<LeadDetailsDTO> call, Throwable throwable) {
                            CommonFunction.hideProgressDialog(getActivity());
                            FWLogger.logInfo(TAG, "Exception in GetLeadListByUserIdandLeadId API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetLeadListByUserIdandLeadId API:");
            ex.getMessage();
        }

    }

    private void setData(List<LeadDetailsDTO.ResultData> mLeadList) {
        try {
            for (int i = 0; i < mLeadList.size(); i++) {
                mtxtCustName.setText(mLeadList.get(i).getCustomerName());
                String formattedDate = DateUtils.convertDateFormat(mLeadList.get(i).getLeadDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
                String formattedTime = DateUtils.timeFormatConversion(mLeadList.get(i).getLeadTime(), "HH:mm:ss", "hh:mm aa");
                if (formattedDate != null && formattedTime != null)
                    mtxtStartDateTime.setText(formattedDate + "  " + formattedTime);

                try {
                    if (SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("true") ||
                            SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                        mtxtCustNo.setText("XXXXXXXXXX");
                    } else {
                        mtxtCustNo.setText(mLeadList.get(i).getMobileNumber());
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }


                //mtxtCustAddress.setText(mLeadList.get(i).getAddress());
                //mtxtLandmark.setText(mLeadList.get(i).getLocDescription());
                //mtxtServiceDetails.setText(mLeadList.get(i).getServiceName());
                if (mLeadList.get(i).getDescription() != null) {
                    mtxtNotes.setText(mLeadList.get(i).getDescription());
                } else {
                    mtxtNotes.setText("-NA-");
                }


                try {
                    List<LeadDetailsDTO.LeadStatusLogObj> followUpHistoryList = new ArrayList<LeadDetailsDTO.LeadStatusLogObj>();
                    followUpHistoryList = mLeadList.get(i).getLeadStatusLogObj();
                    if (followUpHistoryList.size() > 0) {
                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mproductFeatureList.setLayoutManager(mLayoutManager);
                        productFeatureListAdapter = new ProductFeatureListAdapter(getActivity(), followUpHistoryList);
                        mproductFeatureList.setAdapter(productFeatureListAdapter);
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }

                //
                try {
                    List<LeadDetailsDTO.LeadStatusLogObj> followUpHistoryList = new ArrayList<LeadDetailsDTO.LeadStatusLogObj>();
                    followUpHistoryList = mLeadList.get(i).getLeadStatusLogObj();
                    if (followUpHistoryList.size() > 0) {
                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mproductHistoryList.setLayoutManager(mLayoutManager);
                        productHistoryListAdapter = new ProductHistoryListAdapter(getActivity(), followUpHistoryList);
                        mproductHistoryList.setAdapter(productHistoryListAdapter);
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }

                switch (mLeadList.get(i).getLeadStatus()) {
                    case "InActive":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.light_gray));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "Assigned":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.task_ongoing_dark));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "In discussion":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.quantum_lightblue));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "Called":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.blue));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "Dormant":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.dot_dark_screen1));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "Quote Sent":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.dot_light_screen1));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;

                    case "Converted":
                        mtxtLeadStatus.setTextColor(getActivity().getResources().getColor(R.color.green));
                        mtxtLeadStatus.setText(mLeadList.get(i).getLeadStatus());
                        break;
                    default:
                        break;
                }
            }
            updateGoogleMapUI();
            setImages();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.btnScanQr:
                try {
                    Intent i = new Intent(getActivity(), QrCodeActivity.class);
                    startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                } catch (Exception ex) {
                    ex.getMessage();
                }
                break;*/

            case R.id.btnDeletLeadDetails:
                try {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.updateCustomer(getApplicationContext(), activity);
                } catch (Exception ex) {
                    ex.getMessage();
                }
                break;

            case R.id.btnCallCustno:
                try {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.updateProductStatus(getApplicationContext(), activity);
                } catch (Exception ex) {
                    ex.getMessage();
                }
                break;

            default:
                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        FWLogger.logInfo(TAG, "updateGoogleMapUI called");
        if (mGoogleMap != null) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = mGoogleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                getContext(), R.raw.style_json));

                if (!success) {
                    FWLogger.logInfo(TAG, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(TAG, "Can't find style. Error: ", e);
            }

            MapsInitializer.initialize(getContext());
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

            //Check permission
            if (((BaseActivity) getActivity()).checkAllPermissionEnabled()) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                if (((BaseActivity) getActivity()).requestToEnableAllPermission(this, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }

            mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
            // mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

            //Update map
            // updateGoogleMapUI();

        }
    }

    private synchronized void updateGoogleMapUI() {
        try {
            if (mLeadList.get(0).getLatitude() != null && mLeadList.get(0).getLongitude() != null) {
                mDestLat = Double.parseDouble(mLeadList.get(0).getLatitude());
                mDestLong = Double.parseDouble(mLeadList.get(0).getLongitude());
                //Destination marker
                mMarkerDestination = mGoogleMap.addMarker(new MarkerOptions()
                        .anchor(0.0f, 1.0f)
                        .position(new LatLng(mDestLat, mDestLong))
                        .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_fieldweb_location))
                        .title(mLeadList.get(0).getCustomerName())
                        .snippet(mLeadList.get(0).getServiceName()));
                //Show info window forcefully to know about task
                mMarkerDestination.showInfoWindow();

                //Animate camera
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mMarkerDestination.getPosition(), 10F);
                mGoogleMap.animateCamera(cameraUpdate);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void deleteLeadDetailsDialog(Activity activity, int leadID) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_leaddetails_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button deleteLead, buttonCancel;

        deleteLead = dialog.findViewById(R.id.deleteLead);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        deleteLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeleteLeadDetails(leadID);
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void getDeleteLeadDetails(int leadID) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<DeleteLeadDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getDeleteLead(userID, leadID,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<DeleteLeadDetailsDTO>() {
                    @Override
                    public void onResponse(Call<DeleteLeadDetailsDTO> call, Response<DeleteLeadDetailsDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                if (response.code() == 200) {
                                    DeleteLeadDetailsDTO deleteLeadDetailsDTO = response.body();
                                    if (deleteLeadDetailsDTO != null) {
                                        if (deleteLeadDetailsDTO.getCode().equalsIgnoreCase("200")) {
                                            Toast.makeText(getContext(), deleteLeadDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getContext(), deleteLeadDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                        setRefresh();
                                    } else {
                                        Toast.makeText(getContext(), R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteLeadDetailsDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteILead API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteILead API:");
            ex.getMessage();
        }
    }

    private void setRefresh() {
        clearBackStackEntries();
        LeadManagementFragment myFragment = new LeadManagementFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, myFragment);
        transaction.commit();
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String
            permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /*private void downloadFile(String path) {
        try {
            final ProgressDialog pd = new ProgressDialog(getContext());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean downloadResponse = HttpDownloadUtility.downloadFile(path, storageDir.getAbsolutePath());
                        if (downloadResponse) {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.file_download_successfully, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            String filename = path.substring(path.lastIndexOf("/") + 1);
                            File file = new File(storageDir, filename);
                            Uri uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", file);
                            // Open file with user selected app
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(uri, "application/pdf");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.sownload_failed, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        pd.dismiss();
                    } catch (IOException ex) {
                        pd.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean checkStoragePermission() {
        boolean storageFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkWriteStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                final int checkReadStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if (checkWriteStorage != PackageManager.PERMISSION_GRANTED && checkReadStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            },
                            ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    storageFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return storageFlag;
    }

    private void setImages() {
        try {
            if (mLeadList.get(0).getPhotoPath().equalsIgnoreCase("NA") &&
                    (mLeadList.get(0).getPhotoPath1().equalsIgnoreCase("NA") &&
                            (mLeadList.get(0).getPhotoPath2().equalsIgnoreCase("NA")))) {
                leadLnlLayout.setVisibility(View.GONE);
                mattchedPhotoLabel.setVisibility(View.GONE);
            } else {
                leadLnlLayout.setVisibility(View.VISIBLE);
                mattchedPhotoLabel.setVisibility(View.VISIBLE);
                //
                try {
                    Picasso.get().load(mLeadList.get(0).getPhotoPath()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(leadImg1, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            leadImg1.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
                //2
                try {
                    Picasso.get().load(mLeadList.get(0).getPhotoPath1()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(leadImg2, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            leadImg2.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
                //3
                try {
                    Picasso.get().load(mLeadList.get(0).getPhotoPath2()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(leadImg3, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            leadImg3.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            //Log.d(LOGTAG, "COULD NOT GET A GOOD RESULT.");
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            final String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            //Log.d(LOGTAG, "Have scan result in your app activity :" + result);
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Toast.makeText(getActivity(), "Result:" + result, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

        }
    }

    public boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            },
                            REQUEST_CODE_PERMISSIONS);
                } else {
                    if (cameraFlag == false) {
                        requestPermissions(new String[]{
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                },
                                REQUEST_CODE_PERMISSIONS);
                    } else {
                        cameraFlag = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return cameraFlag;
    }


    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    //AppCompatActivity activity = (AppCompatActivity) getApplicationContext();
                    LeadManagementFragment myFragment = new LeadManagementFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    //((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}