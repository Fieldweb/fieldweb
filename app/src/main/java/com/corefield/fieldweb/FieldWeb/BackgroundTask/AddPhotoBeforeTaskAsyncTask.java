package com.corefield.fieldweb.FieldWeb.BackgroundTask;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Expenditure.AddExpense;
import com.corefield.fieldweb.DTO.Task.AddPhotoBeforeTask;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Async Task for Add Expense
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Async Task class is used to call - add attendance (Mark Attendance) API
 */
public class AddPhotoBeforeTaskAsyncTask extends BaseAsyncTask {

    protected static String TAG = AddPhotoBeforeTaskAsyncTask.class.getSimpleName();

    public AddPhotoBeforeTaskAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                String imageName = (String) objects[0];
                String note = (String) objects[1];
                int userId = (int) objects[2];
                String taskPhoto = (String) objects[3];
                int taskId = (int) objects[4];
//                amount = (int) objects[0];
//                expenseName = (String) objects[1];

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("DeviceInfoImageName", imageName);
                    jsonObject.put("DeviceInfoNotes", note);
                    jsonObject.put("CreatedBy", userId);
                    jsonObject.put("DeviceInfoImagePath", taskPhoto);
                    jsonObject.put("TaskId", taskId);
                } catch (JSONException e) {
                    jsonObject = null;
                    e.printStackTrace();
                }
                FWLogger.logInfo(TAG, "jsonObject = " + jsonObject.toString());


                if (jsonObject != null) {
                    URLConnectionRequest URLConnectionRequest = new URLConnectionRequest();
                    if (jsonObject != null && URLConnectionRequest != null) {
                        //No need to pass data to request builder as Gson gives data in string JSON format
                        Request request = RequestBuilder.buildRequest(mContext, URLConstant.Task.ADD_PHOTO_BEFORE_TASK, jsonObject);
                        mUrlConnectionResponse = URLConnectionRequest.sendPostRequest(request.getmURL(), request.getmHeader(), request.getmData().toString(), request.getToken());
                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }

            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null && mUrlConnectionResponse != null) {
            if (mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (status) {
                    if (onTaskCompleteListener != null)
                        onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, AddPhotoBeforeTask.class.getSimpleName());
                } else {
                    //Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
