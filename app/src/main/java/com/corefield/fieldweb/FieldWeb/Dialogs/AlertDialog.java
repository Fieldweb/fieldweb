package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.LoginTouchlessActivity;
import com.corefield.fieldweb.LocationService.GoogleLocAlarmReceiver;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * AlertDialog for various dialog functionality
 *
 * @author CoreField
 * @version 1.1
 * @implNote This AlertDialog class act as Base Class for all Dialog Functions
 */
public class AlertDialog extends AppCompatActivity {
    public static final String TAG = AlertDialog.class.getSimpleName();
    public static boolean isAlertDialogShowing = false;

    public static void exitAlertDialog(Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);

        builder.setTitle(R.string.exit_alert);
        builder.setIcon(R.drawable.ic_alert_black_24dp);
        builder.setMessage(R.string.do_you_want_exit_app);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                activity.finishAffinity();
            }
        });

        builder.setNeutralButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }

    public static void confirmationAlertDialog(Activity activity, String type) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonLogout, buttonCancel;

        buttonLogout = dialog.findViewById(R.id.button_logout);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase(activity.getResources().getString(R.string.title_logout))) {
                    try {
                        ((HomeActivityNew) activity).unRegisterBroadcast();
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                    if (SharedPrefManager.getInstance(activity).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                        SharedPrefManager.getInstance(activity).logoutTechnician(false);
                    } else {
                        SharedPrefManager.getInstance(activity).logout(false);
                    }
                    SharedPrefManager.getInstance(activity).clearPreferredLanguage();
                    Intent intent = new Intent(activity, LoginTouchlessActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                    dialog.dismiss();
                    activity.finish();
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void checkOutDialog(Activity activity, String type) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView textViewMessage;
        Button buttonCheckOut, buttonCancel;

        textViewMessage = dialog.findViewById(R.id.textView_message);
        textViewMessage.setText(R.string.do_you_want_checkout);

        buttonCheckOut = dialog.findViewById(R.id.button_logout);
        buttonCancel = dialog.findViewById(R.id.button_cancel);
        buttonCheckOut.setText(R.string.check_out);

        buttonCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase(activity.getString(R.string.checkout))) {
                    if (SharedPrefManager.getInstance(activity).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                        double latitude = SharedPrefManager.getInstance(activity).getUserLocation().getLatitude();
                        double longitude = SharedPrefManager.getInstance(activity).getUserLocation().getLongitude();
                        String place = MapUtils.getFullAddressFromLatLong(activity, latitude, longitude);
                        try {
                            ((HomeActivityNew) activity).unRegisterBroadcast();
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                        dialog.dismiss();
                        // STOP TRAVELLED PATH ALARM
                        ((HomeActivityNew) activity).cancelAlarm();
                        ((HomeActivityNew) activity).checkOut(latitude, longitude, place);
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public static void showMaintenanceDialog(Context context, String message) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_maintenance);
        TextView textViewExit = dialog.findViewById(R.id.exit);
        TextView textViewMessage = dialog.findViewById(R.id.message_to_show_text_view);
        TextView textViewSupportPhoneNo = dialog.findViewById(R.id.textView_support_phone_no);
        ImageButton imageButtonCancelMaintenance = dialog.findViewById(R.id.image_button_cancel_maintenance);
        textViewMessage.setText(message);

        textViewExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
                //System.exit(0);
                //SharedPrefManager.getInstance(context).logout();
                Activity activity = (Activity) context;
                activity.finishAffinity();
            }
        });

        textViewSupportPhoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01244309497"));
                if (context != null) context.startActivity(intent);
            }
        });

        imageButtonCancelMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
                Activity activity = (Activity) context;
                activity.finishAffinity();
                //System.exit(0);
            }
        });
        isAlertDialogShowing = true;
        dialog.show();
    }

    public static void showTokenExpiredDialog(Context context, String message) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_token_expire);
        TextView textViewReLogin = dialog.findViewById(R.id.textView_re_login);
        TextView textViewMessage = dialog.findViewById(R.id.message_to_show_text_view);
        TextView textViewSupportPhoneNo = dialog.findViewById(R.id.textView_support_phone_no);
        ImageButton imageButtonCancelMaintenance = dialog.findViewById(R.id.image_button_cancel_maintenance);
        textViewMessage.setText(message);

        textViewReLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                //System.exit(0);
                //SharedPrefManager.getInstance(context).logout();
                Activity activity = (Activity) context;
                if (activity instanceof LoginTouchlessActivity) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(activity, LoginTouchlessActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            activity.overridePendingTransition(0, 0);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    });
                }
            }
        });

        textViewSupportPhoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:01244309497"));
                if (context != null) context.startActivity(intent);
            }
        });

        imageButtonCancelMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
                Activity activity = (Activity) context;
                activity.finishAffinity();
                //System.exit(0);
            }
        });
        isAlertDialogShowing = true;
        dialog.show();
    }

    public static void showInvalidAndroidInstanceDialog(Context context) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_android_instance_expire);
        TextView textViewReLogin = dialog.findViewById(R.id.textView_re_login_android_expire);
        ImageButton imageButtonCancelMaintenance = dialog.findViewById(R.id.image_button_cancel_maintenance);

        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(context).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(context).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseGoogleAnalytics.Event.DUPLICATE_LOGIN, bundle);

        textViewReLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                //System.exit(0);
                //SharedPrefManager.getInstance(context).logout();
                Activity activity = (Activity) context;
                if (activity instanceof LoginTouchlessActivity) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    Intent intent = new Intent(activity, LoginTouchlessActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                }

            }
        });

        imageButtonCancelMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
                Activity activity = (Activity) context;
                activity.finishAffinity();
                //System.exit(0);
            }
        });
        isAlertDialogShowing = true;
        dialog.show();
    }

}