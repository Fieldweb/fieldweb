package com.corefield.fieldweb.FieldWeb.Task;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.Task.UpdateTaskStatus;
import com.corefield.fieldweb.DTO.TeleCMI.Connect;
import com.corefield.fieldweb.DTO.TeleCMI.Pcmo;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Fragment Controller for technician task tracking
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show technician tracking on google map (tech Login)
 */

public class StartTaskTrackingFragmentNew extends Fragment implements OnMapReadyCallback, HomeActivityNew.UpdateLocation, View.OnClickListener, OnTaskCompleteListener {
    private static String TAG = StartTaskTrackingFragmentNew.class.getSimpleName();
    protected double mFieldLat = 0, mFieldLong = 0, mTechLat = 0, mTechLong = 0;
    protected String mTaskAssignee = "", mTaskDate = "";
    protected String mTaskName = "", mTaskShortAddress = "", mTaskAddressDesc = "", mTaskDescription = "", mTaskStatus = "", mTaskType = "",
            mCustomerName = "", mCustomerContNo = "", mTaskFullAddress = "", mPaymentMode = "";
    protected int mTaskId = 0, mTaskStatusId = 0, mTaskAmount = 0, mTaskState = 0, mPaymentModeId = 0;
    protected long mStartDate = 0, mEndDate = 0, mElapsedTime = 0;
    private View mRootView;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private LinearLayout mMapLinearLayout;
    private LatLng mCoordinateDestination;
    private Marker mMarkerTech;
    private boolean isFirstTime = true;
    private boolean isTaskEnd = false, isTaskStart = false, isTaskCountdown = false, isTaskRejected = false, isCompleted = false;
    private LatLng mCoordinateOrigin = null;
    private int mOwnerId = 0;
    private String mCustomerEmail = "";
    private TasksList.ResultData mResultData;
    private TextView textViewTaskName, textViewCustomerName, textViewTaskType, textViewDescription,
            textViewMobileNo, textViewDistance, textViewAddress, textView_landmark;
    private ImageView imageViewCall, imageViewArrow;
    private Button buttonStartNavigation, buttonYes, buttonNo, btnSkipNav;
    private LinearLayout linearInstruction, linearAddress, linearLandmark, linearDestination;
    private List<LatLng> path = new ArrayList<>();
    private static Polyline mPolyline = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.start_task_tracking_frag, container, false);

        //TODO Implement shared pref ,update api, get api,logic to get latest timer from api or pref,update UI for all values
        ((HomeActivityNew) getActivity()).mUpdateLocation = this;
        ((HomeActivityNew) getActivity()).hideHeader();

        Bundle getBundle = this.getArguments();
        if (getBundle != null)
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

        inIt();

        return mRootView;
    }

    private void inIt() {
        //initialize all views
        mMapLinearLayout = mRootView.findViewById(R.id.map_linear_layout);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.start_task_map);
        textViewTaskName = mRootView.findViewById(R.id.textView_task_name);
        textViewCustomerName = mRootView.findViewById(R.id.textView_cust_name);
        textViewTaskType = mRootView.findViewById(R.id.textview_task_type);
        textViewDistance = mRootView.findViewById(R.id.textView_distance);
        buttonStartNavigation = mRootView.findViewById(R.id.button_start_navigation);
        btnSkipNav = mRootView.findViewById(R.id.btnSkipNav);
        linearInstruction = mRootView.findViewById(R.id.linear_instruction);
        linearDestination = mRootView.findViewById(R.id.linear_destination);
        linearAddress = mRootView.findViewById(R.id.linear_address);
        linearLandmark = mRootView.findViewById(R.id.linear_landmark);
        textViewDescription = mRootView.findViewById(R.id.textView_description);
        textViewMobileNo = mRootView.findViewById(R.id.textView_mobile_no);
        textViewAddress = mRootView.findViewById(R.id.textView_address);
        textView_landmark = mRootView.findViewById(R.id.textView_landmark);
        imageViewCall = mRootView.findViewById(R.id.imageView_call);
        imageViewArrow = mRootView.findViewById(R.id.imageView_arrow);
        buttonYes = mRootView.findViewById(R.id.button_yes);
        buttonNo = mRootView.findViewById(R.id.button_no);

        buttonStartNavigation.setOnClickListener(this);
        btnSkipNav.setOnClickListener(this);
        imageViewCall.setOnClickListener(this);
        imageViewArrow.setOnClickListener(this);
        buttonYes.setOnClickListener(this);
        buttonNo.setOnClickListener(this);
        getArgu();

        //Google Map
        initializeGoogleMap();
    }

    private void getArgu() {
        if (mResultData.getTaskDate() != null)
            mTaskDate = mResultData.getTaskDate();
        if (mResultData.getLatitude() != null)
            mFieldLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mFieldLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getName() != null)
            mTaskName = mResultData.getName();
        if (mResultData.getAssignedTo() != null)
            mTaskAssignee = mResultData.getAssignedTo();
        if (mResultData.getLocationName() != null)
            mTaskShortAddress = mResultData.getLocationName();
        if (mResultData.getDescription() != null && !mResultData.getDescription().isEmpty())
            mTaskDescription = mResultData.getDescription();
        else
            mTaskDescription = getString(R.string.na);

        if (mResultData.getLocationDesc() != null) {
            mTaskAddressDesc = mResultData.getLocationDesc();
            textView_landmark.setText(mTaskAddressDesc);
        } else {
            mTaskAddressDesc = getString(R.string.na);
        }

        if (mResultData.getTaskStatus() != null)
            mTaskStatus = mResultData.getTaskStatus();
        if (mResultData.getTaskType() != null)
            mTaskType = mResultData.getTaskType();
        if (mResultData.getContactNo() != null && !mResultData.getContactNo().isEmpty())
            mCustomerContNo = mResultData.getContactNo();
        else
            mCustomerContNo = getString(R.string.na);

        if (mResultData.getCustomerName() != null && !mResultData.getCustomerName().isEmpty())
            mCustomerName = mResultData.getCustomerName();
        else
            mCustomerName = getString(R.string.na);
        if (mResultData.getId() != 0) mTaskId = mResultData.getId();
        if (mResultData.getTaskStatusId() != 0)
            mTaskStatusId = mResultData.getTaskStatusId();

        if (mResultData.getFullAddress() != null)
            textViewAddress.setText(mResultData.getFullAddress());

        mTaskState = mResultData.getTaskState(); ////// TODO check

        if (mResultData.getStartDate() != 0)
            mStartDate = mResultData.getStartDate();
        if (mResultData.getEndDate() != 0) mEndDate = mResultData.getEndDate();

        if (mResultData.getFullAddress() != null)
            mTaskFullAddress = mResultData.getFullAddress();

        if (mResultData.getOwnerId() != 0) mOwnerId = mResultData.getOwnerId();

        if (getArguments().getString("FieldLat") != null && getArguments().getString("FieldLong") != null) {
            String address = "";
            address = MapUtils.getFullAddressFromLatLong(getContext(), mFieldLat, mFieldLong);
            if (!address.equalsIgnoreCase("")) {
                mTaskFullAddress = address;
                FWLogger.logInfo(TAG, "Address from lat long " + mTaskFullAddress);
            }
        }


        /*int distanceInKm = 0;

        if(mResultData.getTechLatitude() == null && mResultData.getTechLongitude() == null) {
            if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                double latitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLatitude();
                double longitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLongitude();
                distanceInKm = MapUtils.calculateDistanceInKilometer(latitude, longitude,
                        Double.parseDouble(mResultData.getLatitude()), Double.parseDouble(mResultData.getLongitude()));
            }
        } else {
            distanceInKm = MapUtils.calculateDistanceInKilometer(Double.parseDouble(mResultData.getTechLatitude()), Double.parseDouble(mResultData.getTechLongitude()),
                    Double.parseDouble(mResultData.getLatitude()), Double.parseDouble(mResultData.getLongitude()));
        }
        FWLogger.logInfo(TAG, "Distance In KM : " + distanceInKm);*/

        /*mPaymentModeId = getArguments().getInt("PaymentModeId");
        if (getArguments().getString("PaymentMode") != null) {
            mPaymentMode = getArguments().getString("PaymentMode");
        }*/

        textViewTaskName.setText(mTaskName);
        textViewCustomerName.setText(mCustomerName);
        textViewTaskType.setText(mTaskType);
        if (mTaskType != null && !mTaskType.isEmpty()) {
            switch (mTaskType) {
                case "Urgent":
                    textViewTaskType.setTextColor(getResources().getColor(R.color.task_urgent));
                    break;
                case "Today":
                    textViewTaskType.setTextColor(getResources().getColor(R.color.orange));
                    break;
                case "Schedule":
                    textViewTaskType.setTextColor(getResources().getColor(R.color.task_schedule));
                    break;
                default:
                    break;
            }
        }
        textViewDescription.setText(mTaskDescription);
        try {
            if (SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("true") ||
                    SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                textViewMobileNo.setText("XXXXXXXXXX"); // Changes Done by Manav to hide customer mobile number
            } else {
                textViewMobileNo.setText(mCustomerContNo);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        //
//        textViewDistance.setText(distanceInKm+" km");
    }

    public double CalculationByDistance(double initialLat, double initialLong,
                                        double finalLat, double finalLong) {
        int R = 6371; // km (Earth radius)
        double dLat = toRadians(finalLat - initialLat);
        double dLon = toRadians(finalLong - initialLong);
        initialLat = toRadians(initialLat);
        finalLat = toRadians(finalLat);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(initialLat) * Math.cos(finalLat);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    public double toRadians(double deg) {
        return deg * (Math.PI / 180);
    }

    private void initializeGoogleMap() {
        mMapFragment.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        FWLogger.logInfo(TAG, "onMapReady - start");
        mGoogleMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                FWLogger.logInfo(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        MapsInitializer.initialize(getContext());
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mCoordinateDestination = new LatLng(mFieldLat, mFieldLong);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.addMarker(new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(mCoordinateDestination)
                .icon(MapUtils.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_fieldweb_location))
                .title(mTaskName)
                .snippet(mCustomerName))
                .showInfoWindow();

        if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING) {
            FWLogger.logInfo(TAG, "Task is ongoing");
            //Call this here to resolve first time loading of map if no client is connected to fused api
            if (mGoogleMap != null && mRootView != null) {
                FWLogger.logInfo(TAG, "Map is not null");
                mTechLat = SharedPrefManager.getInstance(getContext().getApplicationContext()).getUserLocation().getLatitude();
                mTechLong = SharedPrefManager.getInstance(getContext().getApplicationContext()).getUserLocation().getLongitude();
                mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
                MapUtils.updateMarkerOnMap(true, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
                /*Projection projection = mGoogleMap.getProjection();
                LatLng markerPosition = mMarkerTech.getPosition();
                Point markerPoint = projection.toScreenLocation(markerPosition);
                Point targetPoint = new Point(markerPoint.x, markerPoint.y - mMapLinearLayout.getHeight() / 2);
                LatLng targetPosition = projection.fromScreenLocation(targetPoint);
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(targetPosition), 1000, null);*/
                /*CameraPosition cameraPosition = new CameraPosition.Builder().target(mCoordinateDestination).zoom(13.0F).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
            }

            if (getActivity() != null) {
                FWLogger.logInfo(TAG, "onMapReady - requesting location service ");
                if (SharedPrefManager.getInstance(getContext().getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                    ((HomeActivityNew) getActivity()).startRequestingLocationService();
                }
            }


            path = MapUtils.getPathBetweenTwoLocation(getContext(), mCoordinateOrigin, mCoordinateDestination);

            /*if(path != null) {
             *//*PolylineOptions opts = new PolylineOptions().addAll(path).color(getContext().getResources().getColor(R.color.black)).width(6);
                mPolyline = mGoogleMap.addPolyline(opts);*//*
                MapUtils.getBearing(mCoordinateOrigin, mCoordinateDestination);
            }*/
        }
        /*double distance = SphericalUtil.computeDistanceBetween(mCoordinateOrigin, mCoordinateDestination);
        Location locationA = new Location("point A");
        locationA.setLatitude(mCoordinateOrigin.latitude);
        locationA.setLongitude(mCoordinateOrigin.longitude);
        Location locationB = new Location("point B");
        locationB.setLatitude(mCoordinateDestination.latitude);
        locationB.setLongitude(mCoordinateDestination.longitude);
        double distanceSecond = locationA.distanceTo(locationB);
        FWLogger.logInfo(TAG, "Distance : "+distance+" distanceSecond : "+distanceSecond);

        double latitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLatitude();
        double longitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLongitude();
        double destinationLat = Double.parseDouble(mResultData.getLatitude());
        double destinationLong = Double.parseDouble(mResultData.getLongitude());
        double calDistance = CalculationByDistance(latitude, longitude,destinationLat, destinationLong);
        FWLogger.logInfo(TAG, "Cal_Distance : "+calDistance);*/

        FWLogger.logInfo(TAG, "onMapReady - end");
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationUpdate(double latitude, double longitude) {
        FWLogger.logInfo(TAG, "onLocationUpdate - " + latitude + " - " + longitude);
        mTechLat = latitude;
        mTechLong = longitude;
        mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
        //Here no need to check permission this listener will gets call only is user has enable permission from GPSTracker
        if (mGoogleMap != null && mRootView != null && mCoordinateOrigin != null) {
            if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING) {
                FWLogger.logInfo(TAG, "onLocationUpdate Task is ongoing");
                MapUtils.updateMarkerOnMap(false, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
            }
        }
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        switch (v.getId()) {
            case R.id.button_start_navigation:
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ON_START_NAV, bundle);
                if (mFieldLat != 0 && mFieldLong != 0)
                    startNavigation();
                else
                    Toast.makeText(getContext(), R.string.google_map_unable_find_destination, Toast.LENGTH_SHORT).show();
                break;

            case R.id.imageView_call:
                if (mCustomerContNo != null) {
                    try {
                        if (SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("true") ||
                                SharedPrefManager.getInstance(getApplicationContext()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                            TelCMI(getActivity(), mCustomerContNo);
                        } else {
                            if (mCustomerContNo != null && !mCustomerContNo.isEmpty() && !mCustomerContNo.equalsIgnoreCase(getString(R.string.na))) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + mCustomerContNo));
                                startActivity(intent);
                            } else {
                                Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }


                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_arrow:
                /*if(imageViewArrow.getSo == getActivity().getResources().getDrawable(R.drawable.ic_down_arrow))
                    imageViewArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_up_arrow));
                else
                    imageViewArrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_down_arrow));*/
                if (linearInstruction.getVisibility() == View.VISIBLE) {
                    imageViewArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_down_arrow));
                    linearAddress.setVisibility(View.GONE);
                    linearLandmark.setVisibility(View.GONE);
                    linearInstruction.setVisibility(View.GONE);
                } else {
                    imageViewArrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_up_arrow));
                    linearAddress.setVisibility(View.VISIBLE);
                    linearLandmark.setVisibility(View.VISIBLE);
                    linearInstruction.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.button_yes:
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.REACHED, bundle);
                ((HomeActivityNew) getActivity()).navigateToStartTaskCountDown(mResultData);
                break;

            case R.id.btnSkipNav:
                try {
                    FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.NOT_REACHED, bundle);
                    buttonStartNavigation.setVisibility(View.GONE);
                    btnSkipNav.setVisibility(View.GONE);
                    linearAddress.setVisibility(View.VISIBLE);
                    linearLandmark.setVisibility(View.VISIBLE);
                    linearInstruction.setVisibility(View.VISIBLE);
                    linearDestination.setVisibility(View.VISIBLE);
                } catch (Exception ex) {
                    ex.getMessage();
                }

                break;

            case R.id.button_no:
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.NOT_REACHED, bundle);
                buttonStartNavigation.setVisibility(View.VISIBLE);
                btnSkipNav.setVisibility(View.VISIBLE);
                linearDestination.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private void startNavigation() {
        try {
            Uri navigationIntentUri = Uri.parse("google.navigation:q=" + mFieldLat + "," + mFieldLong);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            try {
                startActivity(mapIntent);
                buttonStartNavigation.setVisibility(View.GONE);
                linearInstruction.setVisibility(View.VISIBLE);
                linearDestination.setVisibility(View.VISIBLE);
            } catch (ActivityNotFoundException ex) {
                try {
                    Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + mFieldLat + "," + mFieldLong));
                    startActivity(unrestrictedIntent);
                    buttonStartNavigation.setVisibility(View.GONE);
                    linearInstruction.setVisibility(View.VISIBLE);
                    linearDestination.setVisibility(View.VISIBLE);
                } catch (ActivityNotFoundException innerEx) {
                    Toast.makeText(getContext(), R.string.please_install_maps_application, Toast.LENGTH_LONG).show();
                }
            }

        } catch (Exception e) {
            Toast.makeText(getContext(), R.string.something_went_wrong_navigate, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(UpdateTaskStatus.class.getSimpleName())) {
                Gson gson = new Gson();
                UpdateTaskStatus updateTaskStatus = gson.fromJson(urlConnectionResponse.resultData, UpdateTaskStatus.class);
                if (updateTaskStatus != null) {
                    if (updateTaskStatus.getCode().equalsIgnoreCase("200") && updateTaskStatus.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, getContext()))) {
                        Toast.makeText(getContext(), getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                        if (isTaskStart) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.START_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.NOT_STARTED;
                            mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
                            MapUtils.updateMarkerOnMap(true, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
                            isTaskStart = false;
                        } else if (isTaskCountdown) {
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.STARTED_NOT_ENDED;
                            isTaskCountdown = false;
                        } else if (isTaskEnd) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.ENDED_NO_PAYMENT);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.END_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.ENDED_NO_PAYMENT;
                            isTaskEnd = false;
                            navigateToTaskClosure();
                            /*if (mPaymentMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mPaymentModeId == Constant.PaymentMode.RATE_ID) {
                                navigateToPaymentScreen();
                            } else {
                                navigateToTaskClosure();
                            }*/
                        } else if (isTaskRejected) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Rejected.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.REJECT_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Rejected.name();
                            mTaskStatusId = TaskStatus.REJECTED;
                            mTaskState = TaskState.NOT_STARTED;
                            isTaskRejected = false;
                            ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                        } else if (isCompleted) {
                            mTaskStatus = Constant.TaskCategories.Completed.name();
                            mTaskStatusId = TaskStatus.COMPLETED;
                            mTaskState = TaskState.PAYMENT_RECEIVED;
                            isTaskRejected = false;
                            ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                        }
                        //add for reject
                    } else {
                        Toast.makeText(getContext(), updateTaskStatus.getMessage(), Toast.LENGTH_LONG).show();
                        ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                        //Note Check ? This will work only if list get refreshed on back press other wise add if else aging for rejected
                    }
                }

            }
        }
    }

    private void navigateToTaskClosure() {
        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", mResultData);
        taskClosureFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.relative_task_closure, taskClosureFragment);
        transaction.commit();
    }

    private void navigateToPaymentScreen() {
        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", mResultData);
        techPaymentReceivedFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.relative_payment_received, techPaymentReceivedFragment);
        transaction.commit();
    }

    /*private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }*/

    private void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918035234087"));
            teleCMICall.setTo(Long.valueOf("91"+techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("918035234087"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf("91"+custContactnum));
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


/*
    private void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918037222880"));
            teleCMICall.setTo(Long.valueOf(techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            //pcmo.setAction("record");
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("8037222880"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf(custContactnum));
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
*/

    @Override
    public void onStop() {
        FWLogger.logInfo(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onResume() {
        FWLogger.logInfo(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        FWLogger.logInfo(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivityNew) getActivity()).showHeader();
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        super.onDestroyView();
//        inItAllVariables();
        MapUtils.clearMapAttr();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (((HomeActivityNew) getActivity()).mUpdateLocation != null)
            ((HomeActivityNew) getActivity()).mUpdateLocation = null;
    }
}
