package com.corefield.fieldweb.FieldWeb;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.corefield.fieldweb.FieldWeb.Database.DatabaseHandler;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.LocaleUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;

/**
 * Activity for Splash Activity
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Activity  class is used for intro function before all the views
 */
public class SplashActivity extends AppCompatActivity {

    private Handler mHandler;
    private String TAG = SplashActivity.class.getSimpleName();
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setClickable(false);
        if (SharedPrefManager.getInstance(getApplicationContext()).isFirstTime()) {
            SharedPrefManager.getInstance(this).removeUserId();
            SharedPrefManager.getInstance(getApplicationContext()).setFirstTime(false);
        }
        if (!SharedPrefManager.getInstance(getApplicationContext()).isPermissionAccepted()) {
            SharedPrefManager.getInstance(getApplicationContext()).setPermissionEnabled(false);
        }

        try {
            DatabaseHandler db = new DatabaseHandler(this);
            db.copyDataBase(this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        // FOR LANGUAGE UPDATE
        if (SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage() != null) {  //&& !SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage().isEmpty()
            LocaleUtils.setLocale(SplashActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
        } else {
            SharedPrefManager.getInstance(getApplicationContext()).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ENGLISH, 0);
            LocaleUtils.setLocale(SplashActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
        }

        if (getIntent().getExtras() != null) {
            //mean there is new notification message then goto message list screen
            FWLogger.logInfo(TAG, "Intent got extras: ");
            if (getIntent().getExtras().getString("dataMessage") != null) {
                FWLogger.logInfo(TAG, "Intent Push notification data message background : " + getIntent().getExtras().getString("dataMessage"));
                Intent intent = new Intent(SplashActivity.this, LoginTouchlessActivity.class);
                intent.putExtra("NewNotification", "NewNotification");
                //startActivity(intent);
                //finish();
            } else {
                /*Intent intent = new Intent(SplashActivity.this, LoginTouchlessActivity.class);
                startActivity(intent);
                finish();*/
                if (!SharedPrefManager.getInstance(getApplicationContext()).isPermissionAccepted()) {
                    Intent intent = new Intent(SplashActivity.this, RuntimePermissionActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    if (SharedPrefManager.getInstance(this).isLoggedIn() &&
                            SharedPrefManager.getInstance(getApplicationContext()).isOTPVerified()) {
                        Intent intent = new Intent(SplashActivity.this, HomeActivityNew.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, LoginTouchlessActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }
            }
        } else {
            try {

                Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash);
                videoView.setVideoURI(video);
                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        startNextActivity();
                    }
                });
                videoView.start();
            } catch (Exception ex) {
                startNextActivity();
            }
        }
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            public void run() {
                if (!SharedPrefManager.getInstance(getApplicationContext()).isPermissionAccepted()) {
                    Intent intent = new Intent(SplashActivity.this, RuntimePermissionActivity.class);
                    startActivity(intent);
                    finish();
                } /*else {
                        Intent intent = new Intent(SplashActivity.this, LoginTouchlessActivity.class);
                        startActivity(intent);
                        finish();
                    }*/ else if (SharedPrefManager.getInstance(SplashActivity.this).isLoggedIn() &&
                        SharedPrefManager.getInstance(getApplicationContext()).isOTPVerified()) {
                    Intent intent = new Intent(SplashActivity.this, HomeActivityNew.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, LoginTouchlessActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }
    private void startNextActivity() {
        finish();
    }
}



