package com.corefield.fieldweb.FieldWeb.Admin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.AddTechListAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.User.AddBulkTech;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddTechnicianActivity;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * //
 * Created by CFS on 5/7/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Admin
 * Version : 1.3.1
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This fragment is used to submit the technicians in bulk
 * //
 **/
public class AddBulkTechFragment extends Fragment implements
        View.OnClickListener, OnTaskCompleteListener, RecyclerTouchListener {

    //Variables
    private String TAG = AddBulkTechFragment.class.getSimpleName();

    //View's
    private View mRootView;
    private RecyclerView mRecyclerViewBulkTechList;
    private LinearLayoutManager mLayoutManager;

    //AddBulkTech List DTO
    private AddBulkTech.ResultData mAddBulkTechValidation = new AddBulkTech.ResultData();
    private List<AddBulkTech.ResultData> mTechnicianLists = new ArrayList<>();
    public TextView mTextViewValidate, mTextViewSubmit, mTextviewHowToAddTech;

    private AddTechListAdapter mAddTechListAdapter;
    AddBulkTechFragment addBulkTechFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.add_bulk_tech_fragment, container, false);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.title_add_tech));
        inIt();
        return mRootView;
    }

    private void inIt() {
        mRecyclerViewBulkTechList = mRootView.findViewById(R.id.recyclerView_tech_list);
        //DeviceList
        mAddBulkTechValidation.setTempTechSrNo(0);
        mTechnicianLists.add(mAddBulkTechValidation);
        mAddTechListAdapter = new AddTechListAdapter(mTechnicianLists, getContext(), this, "");
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewBulkTechList.setLayoutManager(mLayoutManager);
        mRecyclerViewBulkTechList.setAdapter(mAddTechListAdapter);
        mTextViewValidate = mRootView.findViewById(R.id.textView_validate);
        mTextViewSubmit = mRootView.findViewById(R.id.textView_submit);
        mTextviewHowToAddTech = mRootView.findViewById(R.id.textView_how_to_add_tech);
        mTextViewValidate.setOnClickListener(this);
        mTextViewSubmit.setOnClickListener(this);
        mTextviewHowToAddTech.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_validate:
                boolean isValidateSuccess = false;
                addBulkTechFragment = this;

                for (AddBulkTech.ResultData resultData : mAddTechListAdapter.mTechLists) {
                    if (resultData.getFirstName() == null || resultData.getFirstName().isEmpty()
                            || resultData.getFirstName().equalsIgnoreCase("")
                            /* && resultData.getUserName() == null || resultData.getUserName().isEmpty()*/
                            /*  && resultData.getContactNo() == null || resultData.getContactNo().isEmpty()*/
                            && resultData.getLastName() == null || resultData.getLastName().isEmpty()) {
                        isValidateSuccess = false;
                        Toast.makeText(getContext(), getResources().getString(R.string.invalid_data), Toast.LENGTH_LONG).show();
                    } else {
                        isValidateSuccess = true;
                    }
                }

                if (isValidateSuccess) {
                   /* mAddBulkTechValidationAsyncTask = new AddBulkTechValidationAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
                    mAddBulkTechValidationAsyncTask.execute(mAddTechListAdapter.mTechLists);*/
                    try {
                        if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                            Call<AddBulkTech> call = RetrofitClient.getInstance(getContext()).getMyApi().addBulkTechValidation(mAddTechListAdapter.mTechLists,
                                    "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                            call.enqueue(new retrofit2.Callback<AddBulkTech>() {
                                @Override
                                public void onResponse(Call<AddBulkTech> call, Response<AddBulkTech> response) {
                                    try {
                                        if (response.code() == 200) {
                                            AddBulkTech addBulkTech = response.body();
                                            if (addBulkTech != null) {
                                                FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                                                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, getContext()))) {
                                                    Toast.makeText(getContext(), getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                                                    AddTechSubmit();
                                                } else {
                                                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, getContext()))) {
                                                        //NOTE: Log GA event
                                                        Bundle bundle = new Bundle();
                                                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                                        bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                                                        Toast.makeText(getContext(), getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                                                        ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
                                                    } else {
                                                        if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, getContext()))) {
                                                            Toast.makeText(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                                                        } else {
                                                            Toast.makeText(getContext(), addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                                                        }
                                                        //disableSubmit();
                                                        mTechnicianLists = addBulkTech.getResultData();
                                                        if (mTechnicianLists != null) {
                                                            mAddTechListAdapter = new AddTechListAdapter(mTechnicianLists, getContext(), addBulkTechFragment, addBulkTech.getMessage());
                                                            mLayoutManager = new LinearLayoutManager(getContext());
                                                            mRecyclerViewBulkTechList.setLayoutManager(mLayoutManager);
                                                            mRecyclerViewBulkTechList.setAdapter(mAddTechListAdapter);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } catch (
                                            Exception e) {
                                        e.getMessage();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddBulkTech> call, Throwable throwable) {
                                    FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                                }
                            });
                        } else {
                            ServiceDialog serviceDialog = ServiceDialog.getInstance();
                            serviceDialog.noConnectionDialogRetro(getActivity());
                        }

                    } catch (Exception ex) {
                        FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                        ex.getMessage();
                    }

                    View view = getView().getRootView();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                break;

            case R.id.textView_submit:

                AddTechValidate();
               /* if (mAddTechListAdapter.mTechLists.size() > 0) {
                    // COMMENTED BY MANISH : AS USERNAME IS NOT MADATE FOR MOBILE NUMBER
                   *//* for (AddBulkTech.ResultData resultData : mAddTechListAdapter.mTechLists) {
                        if (resultData.getUserName() == null || resultData.getUserName().isEmpty()
                                *//**//*&& resultData.getContactNo() == null || resultData.getContactNo().isEmpty()*//**//*) {
                            mAddTechListAdapter.mTechLists.remove(resultData);
                        }
                    }*//*
                    mAddBulkTechAsyncTask = new AddBulkTechAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
                    mAddBulkTechAsyncTask.execute(mAddTechListAdapter.mTechLists);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
                }*/
                break;

            case R.id.textView_how_to_add_tech:
                Intent send = new Intent(getContext().getApplicationContext(), YouTubeAddTechnicianActivity.class);
                getContext().startActivity(send);
                break;
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(AddBulkTech.class.getSimpleName())) {
                Gson gson = new Gson();
                AddBulkTech addBulkTech = gson.fromJson(urlConnectionResponse.resultData, AddBulkTech.class);
                if (addBulkTech != null) {
                    FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, getContext()))) {
                        Toast.makeText(getContext(), getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                       /* disableValidate();
                        enableSubmit();*/
                        AddTechSubmit();
                    } else {
                        if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, getContext()))) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                            Toast.makeText(getContext(), getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                            ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
                        } else {
                            if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, getContext()))) {
                                Toast.makeText(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getContext(), addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            //disableSubmit();
                            mTechnicianLists = addBulkTech.getResultData();
                            if (mTechnicianLists != null) {
                                mAddTechListAdapter = new AddTechListAdapter(mTechnicianLists, getContext(), this, addBulkTech.getMessage());
                                mLayoutManager = new LinearLayoutManager(getContext());
                                mRecyclerViewBulkTechList.setLayoutManager(mLayoutManager);
                                mRecyclerViewBulkTechList.setAdapter(mAddTechListAdapter);
                            }
                        }
                    }
                }
            }
        }
    }

    public void disableSubmit() {
        mTextViewSubmit.setEnabled(false);
        mTextViewSubmit.setBackgroundResource(R.drawable.rounded_button_gray);
    }

    public void enableSubmit() {
        mTextViewSubmit.setEnabled(true);
        mTextViewSubmit.setBackgroundResource(R.drawable.rounded_button);
    }

    public void enableValidate() {
        mTextViewValidate.setEnabled(true);
        mTextViewValidate.setBackgroundResource(R.drawable.rounded_button);
    }

    public void disableValidate() {
        mTextViewValidate.setEnabled(false);
        mTextViewValidate.setBackgroundResource(R.drawable.rounded_button_gray);
    }

    @Override
    public void onClick(View view, int position) {

    }

    private void AddTechValidate() {
        try {
            boolean isValidateSuccess = false;

            for (AddBulkTech.ResultData resultData : mAddTechListAdapter.mTechLists) {
                if (resultData.getFirstName() == null || resultData.getFirstName().isEmpty()
                        || resultData.getFirstName().equalsIgnoreCase("")
                        /* && resultData.getUserName() == null || resultData.getUserName().isEmpty()*/
                        /*  && resultData.getContactNo() == null || resultData.getContactNo().isEmpty()*/
                        && resultData.getLastName() == null || resultData.getLastName().isEmpty()) {
                    isValidateSuccess = false;
                    Toast.makeText(getContext(), getResources().getString(R.string.invalid_data), Toast.LENGTH_LONG).show();
                } else {
                    isValidateSuccess = true;
                }
            }

            if (isValidateSuccess) {
               /* mAddBulkTechValidationAsyncTask = new AddBulkTechValidationAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
                mAddBulkTechValidationAsyncTask.execute(mAddTechListAdapter.mTechLists);*/
                try {
                    if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                        Call<AddBulkTech> call = RetrofitClient.getInstance(getContext()).getMyApi().addBulkTechValidation(mAddTechListAdapter.mTechLists,
                                "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                        call.enqueue(new retrofit2.Callback<AddBulkTech>() {
                            @Override
                            public void onResponse(Call<AddBulkTech> call, Response<AddBulkTech> response) {
                                try {
                                    if (response.code() == 200) {
                                        AddBulkTech addBulkTech = response.body();
                                        if (addBulkTech != null) {
                                            FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                                            if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, getContext()))) {
                                                Toast.makeText(getContext(), getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                                                AddTechSubmit();
                                            } else {
                                                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, getContext()))) {
                                                    //NOTE: Log GA event
                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                    FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                                                    Toast.makeText(getContext(), getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                                                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
                                                } else {
                                                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, getContext()))) {
                                                        Toast.makeText(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(getContext(), addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                                                    }
                                                    //disableSubmit();
                                                    mTechnicianLists = addBulkTech.getResultData();
                                                    if (mTechnicianLists != null) {
                                                        mAddTechListAdapter = new AddTechListAdapter(mTechnicianLists, getContext(), addBulkTechFragment, addBulkTech.getMessage());
                                                        mLayoutManager = new LinearLayoutManager(getContext());
                                                        mRecyclerViewBulkTechList.setLayoutManager(mLayoutManager);
                                                        mRecyclerViewBulkTechList.setAdapter(mAddTechListAdapter);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } catch (
                                        Exception e) {
                                    e.getMessage();
                                }
                            }

                            @Override
                            public void onFailure(Call<AddBulkTech> call, Throwable throwable) {
                                FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                            }
                        });
                    } else {
                        ServiceDialog serviceDialog = ServiceDialog.getInstance();
                        serviceDialog.noConnectionDialogRetro(getActivity());
                    }

                } catch (Exception ex) {
                    FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                    ex.getMessage();
                }


            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void AddTechSubmit() {
        try {
            if (mAddTechListAdapter.mTechLists.size() > 0) {
                addBulkTechFragment = this;

               /* mAddBulkTechAsyncTask = new AddBulkTechAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
                mAddBulkTechAsyncTask.execute(mAddTechListAdapter.mTechLists);*/
                try {
                    if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                        Call<AddBulkTech> call = RetrofitClient.getInstance(getContext()).getMyApi().addBulkTech(mAddTechListAdapter.mTechLists,
                                "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                        call.enqueue(new retrofit2.Callback<AddBulkTech>() {
                            @Override
                            public void onResponse(Call<AddBulkTech> call, Response<AddBulkTech> response) {
                                try {
                                    if (response.code() == 200) {
                                        AddBulkTech addBulkTech = response.body();
                                        if (addBulkTech != null) {
                                            FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                                            if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, getContext()))) {
                                                Toast.makeText(getContext(), getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                       /* disableValidate();
                        enableSubmit();*/
                                                AddTechSubmit();
                                            } else {
                                                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, getContext()))) {
                                                    //NOTE: Log GA event
                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                    FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                                                    Toast.makeText(getContext(), getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                                                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
                                                } else {
                                                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, getContext()))) {
                                                        Toast.makeText(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(getContext(), addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                                                    }
                                                    //disableSubmit();
                                                    mTechnicianLists = addBulkTech.getResultData();
                                                    if (mTechnicianLists != null) {
                                                        mAddTechListAdapter = new AddTechListAdapter(mTechnicianLists, getContext(), addBulkTechFragment, addBulkTech.getMessage());
                                                        mLayoutManager = new LinearLayoutManager(getContext());
                                                        mRecyclerViewBulkTechList.setLayoutManager(mLayoutManager);
                                                        mRecyclerViewBulkTechList.setAdapter(mAddTechListAdapter);
                                                    }
                                                }
                                            }
                                        }


                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                            }

                            @Override
                            public void onFailure(Call<AddBulkTech> call, Throwable throwable) {
                                FWLogger.logInfo(TAG, "Exception in AddBulkUsersForMobile API:");
                            }
                        });
                    } else {
                        ServiceDialog serviceDialog = ServiceDialog.getInstance();
                        serviceDialog.noConnectionDialogRetro(getActivity());
                    }

                } catch (Exception ex) {
                    FWLogger.logInfo(TAG, "Exception in AddBulkUsersForMobile API:");
                    ex.getMessage();
                }

            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
