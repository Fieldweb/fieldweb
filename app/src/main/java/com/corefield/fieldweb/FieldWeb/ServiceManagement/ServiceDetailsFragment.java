package com.corefield.fieldweb.FieldWeb.ServiceManagement;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.corefield.fieldweb.Adapter.ServiceSliderAdapter;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Response;

public class ServiceDetailsFragment extends Fragment {

    View mRootView;
    TextView mServiceName, mServicePrice, mServiceContact, mServiceDescription;
    List<EnquiryServiceTypeDTO.ResultData> enquiryServiceTypeDTOList = null;
    ArrayList<Integer> picArray = new ArrayList<Integer>();
    private String serviceName = "", contactNo = "", desrciption = "";
    Button btnBooknow;
    int servicePrice, serviceID;
    ViewPager mPager;
    CircleIndicator circleIndicator;
    int currentPage = 0;
    ImageView editservice, deleteservice;
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    Integer pic[] =
            {
                    R.drawable.service1,
                    R.drawable.service2,
                    R.drawable.service3,
            };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_service_details, container, false);
        inIt();
        jadu();
        return mRootView;
    }

    public void jadu() {
        //change here
        for (int i = 0; i < pic.length; i++)
            //change here
            picArray.add(pic[i]);
        mPager = (ViewPager) mRootView.findViewById(R.id.pager);
        mPager.setAdapter(new ServiceSliderAdapter(getContext(), picArray));

        circleIndicator = (CircleIndicator) mRootView.findViewById(R.id.indicator);
        circleIndicator.setViewPager(mPager);

        final Handler handler = new Handler();//android.os

        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == pic.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 4000, 4000);
    }

    private void inIt() {
        mServiceName = mRootView.findViewById(R.id.servicename);
        mServicePrice = mRootView.findViewById(R.id.serviceprice);
        mServiceContact = mRootView.findViewById(R.id.servicecontact);
        mServiceDescription = mRootView.findViewById(R.id.servicedescription);
        btnBooknow = mRootView.findViewById(R.id.btnBooknow);
        editservice = mRootView.findViewById(R.id.editservice);
        deleteservice = mRootView.findViewById(R.id.deleteservice);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                serviceID = getBundle.getInt("serviceID");
                serviceName = getBundle.getString("serviceName");
                servicePrice = getBundle.getInt("servicePrice");
                contactNo = getBundle.getString("contactNo");
                desrciption = getBundle.getString("description");

                mServiceName.setText(serviceName);
                mServicePrice.setText("₹" + " " + String.valueOf(servicePrice));
                mServiceContact.setText(contactNo);
                mServiceDescription.setText(desrciption);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        btnBooknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ((HomeActivityNew) getActivity()).addEnquiry();
                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.addEnquiryDialogForService(getActivity(), serviceID, serviceName);
            }
        });

        editservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AppCompatActivity activity = (AppCompatActivity) getContext();
                    AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                    addUpdateServiceDialog.EditService(getContext(), activity, serviceID, serviceName, servicePrice, contactNo, desrciption);
                    //setRefresh();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        deleteservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AppCompatActivity activity = (AppCompatActivity) getContext();
                    deleteServiceAlertDialog(activity);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });
    }

    public void getDeleteService() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<ServiceTypeListDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getDeleteService(userID, serviceID,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<ServiceTypeListDTO>() {
                    @Override
                    public void onResponse(Call<ServiceTypeListDTO> call, Response<ServiceTypeListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                if (response.code() == 200) {
                                    CommonFunction.hideProgressDialog(getActivity());
                                    ServiceTypeListDTO serviceTypeListDTO = response.body();
                                    if (serviceTypeListDTO != null) {
                                        if (serviceTypeListDTO.getCode().equalsIgnoreCase("200")) {
                                            Toast.makeText(getContext(), serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getContext(), serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                        setRefresh();
                                    } else {
                                        Toast.makeText(getContext(), R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceTypeListDTO> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in  API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in  API:");
            ex.getMessage();
        }
    }

    private void setRefresh() {
        ServiceManagementFragment myFragment = new ServiceManagementFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }

    private void deleteServiceAlertDialog(Activity activity) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_service_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button deleteservice, buttonCancel;

        deleteservice = dialog.findViewById(R.id.deleteservice);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        deleteservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeleteService();
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}
