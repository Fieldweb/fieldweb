package com.corefield.fieldweb.FieldWeb.AssetManagement;

import static com.corefield.fieldweb.Util.CommonFunction.isEmpty;
import static com.corefield.fieldweb.Util.CommonFunction.isValidPhone;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.AddQuoteServiceAdapter;
import com.corefield.fieldweb.Adapter.CustomerAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.LeadManagement.LeadManagementFragment;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddEditAssetDialog implements AddQuoteServiceAdapter.MultipleServicePostListener {
    private static AddEditAssetDialog mItemDialog;
    public ImageView mImageview1, mImageview2, mImageview3;
    private String mWhichImage = "", mBaseStringFromLead1 = "", mBaseStringFromLead2 = "", mBaseStringFromLead3 = "",
            imageFileName1 = "", imageFileName2 = "", imageFileName3 = "";
    private CountdownTechFragmentNew.ImageUpdateListener mImageUpdateListener;
    private String mCapturedImagePath = "";
    public static final int PICK_IMAGE_GALLERY = 2;
    public static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    private Uri mImageUri;
    Context mContext;
    String mTempLocName = "";
    double mTempLat = 0, mTempLong = 0;
    Place mTempPlace = null;
    private int leadStatusId, serviceId;
    LinearLayoutManager mLayoutManager;
    AddQuoteServiceAdapter addServiceAdapter;
    CustomerList.ResultData customerObj = null;
    ArrayList<EnquiryServiceTypeDTO.ResultData> finalPostArray = new ArrayList<EnquiryServiceTypeDTO.ResultData>();

    private AddEditAssetDialog() {
    }

    public static AddEditAssetDialog getInstance() {
        //instantiate a new ExpenditureDialog if we didn't instantiate one yet
        if (mItemDialog == null) {
            mItemDialog = new AddEditAssetDialog();
        }
        return mItemDialog;
    }

    // Add Asset
    public void addProduct(Context context, Activity mActivity, List<EnquiryServiceTypeDTO.ResultData> serviceNameList, List<CustomerList.ResultData> customerList) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_addeditasset);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        ImageView mcloseProductForm = dialog.findViewById(R.id.closeProductForm);
        Button mbtnAddProduct = dialog.findViewById(R.id.btnAddProduct);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        TextView title = dialog.findViewById(R.id.textView_dialog_title);
        title.setText("Add Product");

        EditText productName = dialog.findViewById(R.id.productName);
        EditText serialno = dialog.findViewById(R.id.edittext_serial_number);
        EditText modelno = dialog.findViewById(R.id.edittext_mobile_number);
        EditText brand = dialog.findViewById(R.id.edittext_brand);
        EditText price = dialog.findViewById(R.id.edittext_price);
        EditText producttype = dialog.findViewById(R.id.edittext_product_type);
        EditText date = dialog.findViewById(R.id.edittext_date);
        EditText custMobileno = dialog.findViewById(R.id.edittext_customer_number);
        EditText productAddress = dialog.findViewById(R.id.productAddress);
        EditText productLandmark = dialog.findViewById(R.id.productLandmark);
        EditText productDesc = dialog.findViewById(R.id.productDesc);
        RecyclerView productfeature = dialog.findViewById(R.id.recyclerView_product_feature);

        mImageview1 = dialog.findViewById(R.id.imageview_photo1);
        mImageview2 = dialog.findViewById(R.id.imageview_photo2);
        mImageview3 = dialog.findViewById(R.id.imageview_photo3);


        mContext = context;
        FGALoadEvent(mContext);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateUtils.datePicker(mActivity, date);
            }
        });


        mcloseProductForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        mImageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct1";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct2";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct3";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        productAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productAddress.setError(null);
                // Initialize Places.
                Places.initialize(mActivity.getApplicationContext(), mActivity.getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        CustomerAdapter adapter = new CustomerAdapter(mActivity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName()
                        + " Number : " + customerList.get(position).getMobileNumber()
                        + " ID : " + customerList.get(position).getCustomerDetailsid()
                        + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                custMobileno.setText(customerObj.getMobileNumber());
                productAddress.setText(customerObj.getAddress());
                productLandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });


        mbtnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(productName)) {
                    productName.setError(mContext.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(productName) && (Character.isWhitespace(productName.getText().toString().charAt(0)) || productName.getText().toString().trim().isEmpty())) {
                    productName.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(serialno)) {
                    serialno.setError("Please Enter Serial Number");
                } else if (isEmpty(modelno)) {
                    modelno.setError("Please Enter Model Number");
                } else if (isEmpty(brand)) {
                    brand.setError("Please enter Product Brand");
                } else if (isEmpty(price)) {
                    price.setError("Please Enter Price");
                } else if (isEmpty(producttype)) {
                    producttype.setError("Please Enter Product Type");
                } else if (isEmpty(productDesc)) {
                    productDesc.setError("Please Enter Product Description");
                } else if (isEmpty(date)) {
                    date.setError(mContext.getResources().getString(R.string.please_enter_date));
                } else if (isEmpty(custMobileno)) {
                    custMobileno.setError(mContext.getResources().getString(R.string.please_enter_cust_number));
                } else if (isValidPhone(custMobileno)) {
                    custMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(custMobileno.getText().toString()) == 0)) {
                    custMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(productAddress)) {
                    productAddress.setError(mContext.getString(R.string.please_enter_address));
                } else if (isEmpty(productLandmark)) {
                    productLandmark.setError(mContext.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(productLandmark) && (Character.isWhitespace(productLandmark.getText().toString().charAt(0)) || productLandmark.getText().toString().trim().isEmpty())) {
                    productLandmark.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } /*else if (isEmpty(leadNotes)) {
                    leadNotes.setError(context.getString(R.string.please_enter_notes));
                }*/ else {
                    dialog.dismiss();
                }
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    productAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }
            }
        });

       /* ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);*/
        ArrayList<EnquiryServiceTypeDTO.ResultData> multipleServiceArrayList = new ArrayList<EnquiryServiceTypeDTO.ResultData>();
        EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
        resultData.setId(0);
        resultData.setServiceName("");
        multipleServiceArrayList.add(resultData);

        mLayoutManager = new LinearLayoutManager(mContext);
        addServiceAdapter = new AddQuoteServiceAdapter(this, multipleServiceArrayList, mContext, serviceNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        productfeature.setLayoutManager(mLayoutManager);
        productfeature.setAdapter(addServiceAdapter);

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });
    }

    //Edit Asset
    public void editProduct(Context context, Activity mActivity, List<EnquiryServiceTypeDTO.ResultData> serviceNameList, List<CustomerList.ResultData> customerList) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_addeditasset);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        ImageView mcloseProductForm = dialog.findViewById(R.id.closeProductForm);
        Button mbtnAddProduct = dialog.findViewById(R.id.btnAddProduct);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        TextView title = dialog.findViewById(R.id.textView_dialog_title);
        title.setText("Edit Product");

        EditText productName = dialog.findViewById(R.id.productName);
        EditText serialno = dialog.findViewById(R.id.edittext_serial_number);
        EditText modelno = dialog.findViewById(R.id.edittext_mobile_number);
        EditText brand = dialog.findViewById(R.id.edittext_brand);
        EditText price = dialog.findViewById(R.id.edittext_price);
        EditText producttype = dialog.findViewById(R.id.edittext_product_type);
        EditText date = dialog.findViewById(R.id.edittext_date);
        EditText custMobileno = dialog.findViewById(R.id.edittext_customer_number);
        EditText productAddress = dialog.findViewById(R.id.productAddress);
        EditText productLandmark = dialog.findViewById(R.id.productLandmark);
        EditText productDesc = dialog.findViewById(R.id.productDesc);
        RecyclerView productfeature = dialog.findViewById(R.id.recyclerView_product_feature);

        mImageview1 = dialog.findViewById(R.id.imageview_photo1);
        mImageview2 = dialog.findViewById(R.id.imageview_photo2);
        mImageview3 = dialog.findViewById(R.id.imageview_photo3);


        mContext = context;
        FGALoadEvent(mContext);


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateUtils.datePicker(mActivity, date);
            }
        });

        mcloseProductForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        mImageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct1";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct2";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddProduct3";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        productAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productAddress.setError(null);
                // Initialize Places.
                Places.initialize(mActivity.getApplicationContext(), mActivity.getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        /*AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        CustomerAdapter adapter = new CustomerAdapter(mActivity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);*/

        /*autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName()
                        + " Number : " + customerList.get(position).getMobileNumber()
                        + " ID : " + customerList.get(position).getCustomerDetailsid()
                        + " Description : " + customerList.get(position).getDescription());
                customerObj = customerList.get(position);
                custMobileno.setText(customerObj.getMobileNumber());
                productAddress.setText(customerObj.getAddress());
                productLandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });*/


        mbtnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(productName)) {
                    productName.setError(mContext.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(productName) && (Character.isWhitespace(productName.getText().toString().charAt(0)) || productName.getText().toString().trim().isEmpty())) {
                    productName.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(serialno)) {
                    serialno.setError("Please Enter Serial Number");
                } else if (isEmpty(modelno)) {
                    modelno.setError("Please Enter Model Number");
                } else if (isEmpty(brand)) {
                    brand.setError("Please enter Product Brand");
                } else if (isEmpty(price)) {
                    price.setError("Please Enter Price");
                } else if (isEmpty(producttype)) {
                    producttype.setError("Please Enter Product Type");
                } else if (isEmpty(productDesc)) {
                    productDesc.setError("Please Enter Product Description");
                } else if (isEmpty(custMobileno)) {
                    custMobileno.setError(mContext.getResources().getString(R.string.please_enter_cust_number));
                } else if (isValidPhone(custMobileno)) {
                    custMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(custMobileno.getText().toString()) == 0)) {
                    custMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(productAddress)) {
                    productAddress.setError(mContext.getString(R.string.please_enter_address));
                } else if (isEmpty(productLandmark)) {
                    productLandmark.setError(mContext.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(productLandmark) && (Character.isWhitespace(productLandmark.getText().toString().charAt(0)) || productLandmark.getText().toString().trim().isEmpty())) {
                    productLandmark.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } /*else if (isEmpty(leadNotes)) {
                    leadNotes.setError(context.getString(R.string.please_enter_notes));
                }*/ else {
                    dialog.dismiss();
                }
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    productAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /*ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned = new AddTask.MultipleItemAssigned();
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemQuantity(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssignedArrayList.add(multipleItemAssigned);*/
        ArrayList<EnquiryServiceTypeDTO.ResultData> multipleServiceArrayList = new ArrayList<EnquiryServiceTypeDTO.ResultData>();
        EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
        resultData.setId(0);
        resultData.setServiceName("");
        multipleServiceArrayList.add(resultData);

        mLayoutManager = new LinearLayoutManager(mContext);
        addServiceAdapter = new AddQuoteServiceAdapter(this, multipleServiceArrayList, mContext, serviceNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        productfeature.setLayoutManager(mLayoutManager);
        productfeature.setAdapter(addServiceAdapter);

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });
    }

    // Delete Asset
    public void deleteProduct(Context context, Activity mActivity) {
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_asset_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        Button no, yes;

        no = dialog.findViewById(R.id.button_cancel);
        yes = dialog.findViewById(R.id.deleteProduct);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    // Update Product Status
    public void updateProductStatus(Context context, Activity mActivity) {
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_asset_status);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        ImageView close;
        Button cancel;

        close = dialog.findViewById(R.id.closeLeadForm);
        cancel = dialog.findViewById(R.id.button_cancel);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    // Update Customer Status
    public void updateCustomer(Context context, Activity mActivity) {
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_asset_customer);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        ImageView close;
        Button cancel;

        close = dialog.findViewById(R.id.closeLeadForm);
        cancel = dialog.findViewById(R.id.button_cancel);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void refresh(Context mContext) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        LeadManagementFragment myFragment = new LeadManagementFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }

    public void setPhotoUpdateForProduct1(Bitmap bitmap) {
        mImageview1.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName1 = timeStamp + "_" + "AddUpdateProduct1" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead1 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview1.setImageBitmap(bitmap);
    }

    //
    public void setPhotoUpdateForProduct2(Bitmap bitmap) {
        mImageview2.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName2 = timeStamp + "_" + "AddUpdateProduct2" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead2 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview2.setImageBitmap(bitmap);
    }

    public void setPhotoUpdateForProduct3(Bitmap bitmap) {
        mImageview3.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName3 = timeStamp + "_" + "AddUpdateProduct3" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead3 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview3.setImageBitmap(bitmap);
    }

    private void FGALoadEvent(Context mContext) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(mContext).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(mContext).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(mContext).logEvent(FirebaseGoogleAnalytics.Event.ADD_LEAD, bundle);
    }

    @Override
    public void onMultipleServicePost(ArrayList<EnquiryServiceTypeDTO.ResultData> SODArray) {
        finalPostArray = SODArray;
    }
}
