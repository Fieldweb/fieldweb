package com.corefield.fieldweb.FieldWeb.Task;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.TaskCompletedMultitemListAdapter;
import com.corefield.fieldweb.Adapter.TaskOngoingMultitemListAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.AttendanceCheck;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.User.GetLiveLocation;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.GetLiveLocationAsyncTask;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Owner Task Tracking
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show the current tech location with Google Map to Owner login
 */

public class OwnerTaskTrackingFragmentNew extends Fragment implements OnMapReadyCallback, BaseActivity.RequestPermissionsUpdateListener, OnTaskCompleteListener, ValueEventListener, View.OnClickListener {
    private static String TAG = OwnerTaskTrackingFragmentNew.class.getSimpleName();
    private View mRootView;
    private TextView mTaskStatusTextView, mTaskNameTextView, mTaskAssigneeTextView, mTaskAmountTextView, mTextViewLandmark,
            mTextViewFullTaskAddress, mTextViewCustomerName, mTextViewCostumerContNo, mTextViewTechName, mTextViewTaskId, mTextViewTaskType,
            mtext_onHoldDateTime, mtext_onHoldReason, mTextonholdLabel;
    private ImageView mImageViewCall, mImageViewMessage, onHoldImg1, onHoldImg2, onHoldImg3, mImageViewReassign;
    LinearLayout onHoldLnlLayout;
    private CircleImageView mAssigneeCircleImageView;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mMapFragment;
    private double mFieldLat = 0, mFieldLong = 0, mTechLat = 0, mTechLong = 0;
    private String mTakName = "", mTaskAssignee = "", mFullAddress = "", mTaskStatus = "", mCustomerName = "", mTaskType = "";
    private int mUserId = 0, mTaskId = 0, mTaskTypeId = 0, mPaymentModeId = 0;
    private GetLiveLocationAsyncTask mGetLiveLocationAsyncTask = null;
    private Handler mHandler = new Handler();
    private LinearLayout mLinearLayoutTechAbsent;
    private ImageView mRiderImageView;
    private DatabaseReference mDatabaseReference;
    private LatLng mCoordinateDestination = null;
    private LatLng mCoordinateOrigin = null;
    private boolean mMapLoaded = false;
    private int mTaskStatusId = 0;
    private String contactNo;
    private TableRow tableRowEarnedAmt;
    private Marker mMarkerDestination;
    OnMapReadyCallback onMapReadyCallback;
    TableRow tblRowOnHoldOne, tblRowOnHoldTwo;
    private TasksList.ResultData mResultData;
    //private TasksList.MultipleItemAssigned multipleItemAssigned;
    List<TasksList.MultipleItemAssigned> multipleItemAssigned;
    private RecyclerView multiItemOngoingTaskDetls;
    //
    TaskOngoingMultitemListAdapter taskOngoingMultitemListAdapter;
    private LinearLayoutManager mLayoutManager;

    //////////////////////////////// Animate Rider ///////////////////////////////////////
    Runnable mHandlerTechRider = new Runnable() {
        @Override
        public void run() {
            animateTechRider();
            mHandler.postDelayed(mHandlerTechRider, 1000);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.owner_task_tracking_fragment_new, container, false);

        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);

        inIt();
        manageBackPress();
        return mRootView;
    }

    private void inIt() {
        mTakName = getArguments().getString("Task Name");
        mTaskAssignee = getArguments().getString("Task Assignee");

        onMapReadyCallback = this;

        if (getArguments().getString("FieldLat") != null)
            mFieldLat = Double.parseDouble(getArguments().getString("FieldLat"));
        if (getArguments().getString("FieldLong") != null)
            mFieldLong = Double.parseDouble(getArguments().getString("FieldLong"));
        if (getArguments().getString("TechLat") != null)
            mTechLat = Double.parseDouble(getArguments().getString("TechLat"));
        if (getArguments().getString("TechLong") != null)
            mTechLong = Double.parseDouble(getArguments().getString("TechLong"));
        if (getArguments().getInt("UserId") != 0)
            mUserId = getArguments().getInt("UserId");
        if (getArguments().getString("Task Status") != null)
            mTaskStatus = getArguments().getString("Task Status");
        if (getArguments().getInt("TaskStatusId") != 0)
            mTaskStatusId = getArguments().getInt("TaskStatusId");

        if (getArguments().getString("Task Type") != null)
            mTaskType = getArguments().getString("Task Type");
        if (getArguments().getInt("TaskTypeId") != 0)
            mTaskTypeId = getArguments().getInt("TaskTypeId");
        if (getArguments().getInt("PaymentModeId") != 0)
            mPaymentModeId = getArguments().getInt("PaymentModeId");

        if (getArguments().getInt("TaskId") != 0)
            mTaskId = getArguments().getInt("TaskId");
        if (getArguments().getString("CustomerName") != null)
            mCustomerName = getArguments().getString("CustomerName");

        if (getArguments().getString("ContactNo") != null && !getArguments().getString("ContactNo").isEmpty() && !getArguments().getString("ContactNo").equalsIgnoreCase(""))
            contactNo = getArguments().getString("ContactNo");

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("" + mUserId);
        mDatabaseReference.addValueEventListener(this);

        mTextViewLandmark = mRootView.findViewById(R.id.textview_owner_task_cust_landmark);
        mTextViewFullTaskAddress = mRootView.findViewById(R.id.textview_owner_task_cust_address);
        mTextViewCostumerContNo = mRootView.findViewById(R.id.textview_owner_task_cust_contact);
        mTextViewCustomerName = mRootView.findViewById(R.id.textview_owner_task_cust_name);
        mTaskStatusTextView = mRootView.findViewById(R.id.textview_task_status);
        mTaskAssigneeTextView = mRootView.findViewById(R.id.textview_task_assignee_name);
        mTaskNameTextView = mRootView.findViewById(R.id.textview_task_name);
        mTextViewTechName = mRootView.findViewById(R.id.textview_tech_name_absent);
        mRiderImageView = mRootView.findViewById(R.id.imageview_rider);
        mTextViewTaskId = mRootView.findViewById(R.id.task_id);
        mImageViewCall = mRootView.findViewById(R.id.imageView_call);
        mImageViewMessage = mRootView.findViewById(R.id.imageView_message);
        mTextViewTaskType = mRootView.findViewById(R.id.task_details_task_type);
        tableRowEarnedAmt = mRootView.findViewById(R.id.table_earned_amt);

        mLinearLayoutTechAbsent = mRootView.findViewById(R.id.linear_tech_absent_task_view);

        mTaskAmountTextView = mRootView.findViewById(R.id.textview_earning_amount);
        mAssigneeCircleImageView = mRootView.findViewById(R.id.technician_image);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mImageViewReassign = mRootView.findViewById(R.id.imageView_reassign);
        tblRowOnHoldOne = mRootView.findViewById(R.id.tblRowOnHoldOne);
        tblRowOnHoldTwo = mRootView.findViewById(R.id.tblRowOnHoldTwo);
        mtext_onHoldDateTime = mRootView.findViewById(R.id.text_onHoldDateTime);
        mtext_onHoldReason = mRootView.findViewById(R.id.text_onHoldReason);
        onHoldLnlLayout = mRootView.findViewById(R.id.onHoldLnlLayout);
        onHoldImg1 = mRootView.findViewById(R.id.onHoldImg1);
        onHoldImg2 = mRootView.findViewById(R.id.onHoldImg2);
        onHoldImg3 = mRootView.findViewById(R.id.onHoldImg3);
        mTextonholdLabel = mRootView.findViewById(R.id.onholdLabel);
        //
        multiItemOngoingTaskDetls = mRootView.findViewById(R.id.multiItemOngoingTaskDetls);

        if (mPaymentModeId == 1)
            tableRowEarnedAmt.setVisibility(View.GONE);
        else
            tableRowEarnedAmt.setVisibility(View.VISIBLE);

        /*Picasso.get().load(getArguments().getString("Photo")).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mAssigneeCircleImageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                mAssigneeCircleImageView.setImageResource(R.drawable.profile_icon);
            }
        });*/
        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {

                multipleItemAssigned = (List<TasksList.MultipleItemAssigned>) getBundle.getSerializable("MultiItemResult");
            }

        } catch (Exception ex) {
            ex.getMessage();
        }

        if (getArguments().getString("Task Status") != null) {
            mTaskStatusTextView.setText(getArguments().getString("Task Status"));
            if (getArguments().getString("Task Status").equalsIgnoreCase(getActivity().getResources().getString(R.string.inactive)))
                mTaskStatusTextView.setTextColor(getActivity().getResources().getColor(R.color.light_gray));
            else if (getArguments().getString("Task Status").equalsIgnoreCase(getActivity().getResources().getString(R.string.ongoing)))
                mTaskStatusTextView.setTextColor(getActivity().getResources().getColor(R.color.orange));
            else if (getArguments().getString("Task Status").equalsIgnoreCase(getActivity().getResources().getString(R.string.onhold))) {
                mTaskStatusTextView.setTextColor(getActivity().getResources().getColor(R.color.onhold));
                tblRowOnHoldOne.setVisibility(View.VISIBLE);
                tblRowOnHoldTwo.setVisibility(View.VISIBLE);
                mImageViewReassign.setVisibility(View.VISIBLE);
                mTextonholdLabel.setVisibility(View.VISIBLE);
                Bundle getBundle = this.getArguments();
                if (getBundle != null) {
                    mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");
                }
            }
        } else mTaskStatusTextView.setText(getResources().getString(R.string.na));

        mTaskAssigneeTextView.setText(mTaskAssignee);
        mTextViewTechName.setText(mTaskAssignee);
        mTaskNameTextView.setText(mTakName);
        mTextViewTaskType.setText(mTaskType);
        if (mTaskType != null && !mTaskType.isEmpty()) {
            switch (mTaskType) {
                case "Urgent":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_urgent));
                    break;
                case "Today":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.orange));
                    break;
                case "Schedule":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_schedule));
                    break;
                default:
                    break;
            }
        }

        mTaskAmountTextView.setText(String.valueOf(getArguments().getInt("Task Amount")));
        String address = "";
        address = MapUtils.getFullAddressFromLatLong(getContext(), mFieldLat, mFieldLong);
        if (!address.equalsIgnoreCase("")) {
            mTextViewFullTaskAddress.setText(address);
            FWLogger.logInfo(TAG, "Address from lat long " + address);
        } else {
            if (getArguments().getString("FullAddress") != null)
                mTextViewFullTaskAddress.setText(getArguments().getString("FullAddress"));
            FWLogger.logInfo(TAG, "Address from Full add- " + getArguments().getString("FullAddress"));
        }

        if (getArguments().getString("ContactNo") == null || getArguments().getString("ContactNo").isEmpty() || getArguments().getString("ContactNo").equalsIgnoreCase(""))
            mTextViewCostumerContNo.setText(getResources().getString(R.string.na));
        else
            mTextViewCostumerContNo.setText(getArguments().getString("ContactNo"));

        if (getArguments().getString("CustomerName") == null || getArguments().getString("CustomerName").isEmpty() || getArguments().getString("CustomerName").equalsIgnoreCase(""))
            mTextViewCustomerName.setText(getResources().getString(R.string.na));
        else
            mTextViewCustomerName.setText(getArguments().getString("CustomerName"));

        if (getArguments().getString("LocDescription") == null || getArguments().getString("LocDescription").isEmpty() || getArguments().getString("LocDescription").equalsIgnoreCase(""))
            mTextViewLandmark.setText(getResources().getString(R.string.na));
        else
            mTextViewLandmark.setText(getArguments().getString("LocDescription"));

        mTextViewTaskId.setText("[" + mTaskId + "]");

        try {
            if (getArguments().getString("OnHoldNotes") == null || getArguments().getString("OnHoldNotes").isEmpty() || getArguments().getString("OnHoldNotes").equalsIgnoreCase(""))
                mtext_onHoldReason.setText(getResources().getString(R.string.na));
            else
                mtext_onHoldReason.setText(getArguments().getString("OnHoldNotes"));
        } catch (Exception ex) {
            ex.getMessage();
        }

        try {
            if (getArguments().getString("CreatedDate") == null || getArguments().getString("CreatedDate").isEmpty() || getArguments().getString("CreatedDate").equalsIgnoreCase(""))
                mtext_onHoldDateTime.setText(getResources().getString(R.string.na));
            else
                setFormatDate(getArguments().getString("CreatedDate"));
        } catch (Exception ex) {
            ex.getMessage();
        }
        try {
            if (getArguments().getString("Pick1") == null || getArguments().getString("Pick1").isEmpty() || getArguments().getString("Pick1").equalsIgnoreCase("") ||
                    (getArguments().getString("Pick2") == null || getArguments().getString("Pick2").isEmpty() || getArguments().getString("Pick2").equalsIgnoreCase("") ||
                            (getArguments().getString("Pick3") == null || getArguments().getString("Pick3").isEmpty() || getArguments().getString("Pick3").equalsIgnoreCase("")))) {
                onHoldLnlLayout.setVisibility(View.GONE);
            } else {
                onHoldLnlLayout.setVisibility(View.VISIBLE);
                //
                try {
                    Picasso.get().load(getArguments().getString("Pick1")).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(onHoldImg1, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            onHoldImg1.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
                //2
                try {
                    Picasso.get().load(getArguments().getString("Pick2")).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(onHoldImg2, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            onHoldImg2.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
                //3
                try {
                    Picasso.get().load(getArguments().getString("Pick3")).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(onHoldImg3, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            onHoldImg3.setImageResource(R.drawable.profile_icon);
                        }
                    });
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        //
        mImageViewReassign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResultData != null && getActivity() != null) {
                    if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.onhold))) {
                        ((HomeActivityNew) getActivity()).openReAssignCompletedTaskDialog(mResultData);
                    } /*else {
                        ((HomeActivityNew) getActivity()).addTaskWithReassignDefaults(mResultData);
                    }*/
                } else {
                    Toast.makeText(getContext(), R.string.something_wrong_please_refresh, Toast.LENGTH_SHORT).show();
                }
            }
        });

        // MULTI ITEM LIST
        try {
            List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
            multipleItemAssignedArrayList = multipleItemAssigned;
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiItemOngoingTaskDetls.setLayoutManager(mLayoutManager);
                taskOngoingMultitemListAdapter = new TaskOngoingMultitemListAdapter(getActivity(), multipleItemAssignedArrayList);
                multiItemOngoingTaskDetls.setAdapter(taskOngoingMultitemListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        isTechnicianPresent();

        mImageViewCall.setOnClickListener(this);
        mImageViewMessage.setOnClickListener(this);
    }

    private void setFormatDate(String dateString) {
        String formattedDate = DateUtils.convertDateFormat(dateString, "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy h:mm a");
        mtext_onHoldDateTime.setText(String.valueOf(formattedDate));
    }

    private void animateTechRider() {
        mRiderImageView.setRotation(mRiderImageView.getRotation() + 10);
    }

    private void isTechnicianPresent() {
       /* mAttendanceCheckAsyncTask = new AttendanceCheckAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mAttendanceCheckAsyncTask.execute(mUserId);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<AttendanceCheck> call = RetrofitClient.getInstance(getContext()).getMyApi().getAttendanceCheck(mUserId);
                call.enqueue(new retrofit2.Callback<AttendanceCheck>() {
                    @Override
                    public void onResponse(Call<AttendanceCheck> call, Response<AttendanceCheck> response) {
                        try {
                            FWLogger.logInfo(TAG, "AttendanceCheck");
                            if (response.code() == 200) {
                                AttendanceCheck attendanceCheck = response.body();
                                if (attendanceCheck != null) {
                                    if (attendanceCheck.getCode().equalsIgnoreCase("200") && attendanceCheck.getMessage().equalsIgnoreCase("Attendance already added.")) {
                                        FWLogger.logInfo(TAG, "Present");
                                        //If present then call the map sync
                                        mMapFragment.getMapAsync(onMapReadyCallback);
                                    } else {
                                        FWLogger.logInfo(TAG, "Absent");
                                        mLinearLayoutTechAbsent.setVisibility(View.VISIBLE);
                                        startTechRiderAnimate();
                                        //If absent then do not call the map sync
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AttendanceCheck> call, Throwable throwable) {
                        //Toast.makeText(getContext(), "GetTodayAttandanceIsExist? API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "GetTodayAttandanceIsExist? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "GetTodayAttandanceIsExist? API:");
            ex.getMessage();
        }
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        try {

            FWLogger.logInfo(TAG, "onDataChange");
            String databaseLatitudeString = dataSnapshot.child("lat").getValue().toString(); //.substring(1, dataSnapshot.child("lat").getValue().toString().length()-1);
            String databaseLongitudeString = dataSnapshot.child("lng").getValue().toString(); //.substring(1, dataSnapshot.child("lng").getValue().toString().length()-1);
            FWLogger.logInfo(TAG, "Lat : " + databaseLatitudeString);
            FWLogger.logInfo(TAG, "Long : " + databaseLongitudeString);
            mTechLat = Double.parseDouble(databaseLatitudeString);
            mTechLong = Double.parseDouble(databaseLongitudeString);
            mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
            if (mMapLoaded && mCoordinateOrigin != null && mGoogleMap != null) {
                if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING)
                    MapUtils.updateMarkerOnMap(false, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                FWLogger.logInfo(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        MapsInitializer.initialize(getContext());
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        //Check permission
        if (((BaseActivity) getActivity()).checkAllPermissionEnabled()) {
            mGoogleMap.setMyLocationEnabled(true);
        } else {
            if (((BaseActivity) getActivity()).requestToEnableAllPermission(this, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mCoordinateDestination = new LatLng(mFieldLat, mFieldLong);
        //Destination marker
        mMarkerDestination = mGoogleMap.addMarker(new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(mCoordinateDestination)
                .icon(MapUtils.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_fieldweb_location))
                .title(mTakName)
                .snippet(mCustomerName));
        mMarkerDestination.showInfoWindow();

        //Update map for the first time
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMapLoaded = true;
                mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
                if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING)
                    MapUtils.updateMarkerOnMap(true, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
            }
        });
        //Once google map fully loaded add recursive call to get most recent place of technician

        //Animate camera
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mMarkerDestination.getPosition(), 12F);
        mGoogleMap.animateCamera(cameraUpdate);
    }


    private void startDropMarkerAnimation(final Marker marker) {
        final LatLng target = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mGoogleMap.getProjection();
        Point targetPoint = proj.toScreenLocation(target);
        final long duration = (long) (200 + (targetPoint.y * 0.6));
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.y = 0;
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later == 60 frames per second
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    void startTechRiderAnimate() {
        FWLogger.logInfo(TAG, "startTechRiderAnimate called");
        mHandlerTechRider.run();
    }

    void stopTechRiderAnimate() {
        FWLogger.logInfo(TAG, "stopTechRiderAnimate called");
        if (mHandler != null) mHandler.removeCallbacks(mHandlerTechRider);
    }
//////////////////////////////// Animate Rider ///////////////////////////////////////

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String
            permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(GetLiveLocation.class.getSimpleName())) {
            Gson gson = new Gson();
            GetLiveLocation getLiveLocation = gson.fromJson(urlConnectionResponse.resultData, GetLiveLocation.class);
            //Don't show anything on screen
            if (getLiveLocation != null) {
                if (getLiveLocation.getCode().equalsIgnoreCase("200") && getLiveLocation.getMessage().equalsIgnoreCase("success")) {
                    FWLogger.logInfo(TAG, "Message : " + getLiveLocation.getMessage());
                    if (getLiveLocation.getResultData().get(0) != null) {
                        mTechLat = Double.parseDouble(getLiveLocation.getResultData().get(0).getLatitude());
                        mTechLong = Double.parseDouble(getLiveLocation.getResultData().get(0).getLongitude());
                        FWLogger.logInfo(TAG, "Lat :  " + mTechLat + "Long : " + mTechLong);
                    }
                } else {
                    FWLogger.logInfo(TAG, "Message : " + getLiveLocation.getMessage());
                }
            }
            //Cancel and make it null to avoid congestion of network call
            if (mGetLiveLocationAsyncTask != null) {
                mGetLiveLocationAsyncTask.cancel(true);
                mGetLiveLocationAsyncTask = null;
            }
        }
        if (classType.equalsIgnoreCase(AttendanceCheck.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "AttendanceCheck");
            Gson gson = new Gson();
            AttendanceCheck attendanceCheck = gson.fromJson(urlConnectionResponse.resultData, AttendanceCheck.class);

            if (attendanceCheck != null) {
                if (attendanceCheck.getCode().equalsIgnoreCase("200") && attendanceCheck.getMessage().equalsIgnoreCase("Attendance already added.")) {
                    FWLogger.logInfo(TAG, "Present");
                    //If present then call the map sync
                    mMapFragment.getMapAsync(this);
                } else {
                    FWLogger.logInfo(TAG, "Absent");
                    mLinearLayoutTechAbsent.setVisibility(View.VISIBLE);
                    startTechRiderAnimate();
                    //If absent then do not call the map sync
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.textView_call:
            case R.id.imageView_call:
                if (contactNo != null && !contactNo.isEmpty()) {
//                    if (((BaseActivity) getActivity()).requestToEnableAllPermission(this, BaseActivity.PERMISSION_REQUEST_PHONE_CALL)) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + contactNo));
                    startActivity(intent);
//                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (contactNo != null && !contactNo.isEmpty()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", contactNo, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onDestroyView() {
        stopTechRiderAnimate();
        mMapLoaded = false;
        if (mGetLiveLocationAsyncTask != null) {
            mGetLiveLocationAsyncTask.cancel(true);
            mGetLiveLocationAsyncTask = null;
        }
        MapUtils.clearMapAttr();
        super.onDestroyView();
    }
}
