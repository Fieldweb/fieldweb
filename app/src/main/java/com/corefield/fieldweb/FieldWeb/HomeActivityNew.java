package com.corefield.fieldweb.FieldWeb;

import static com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog.itemListFromWhere;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.AddItemsAdapter;
import com.corefield.fieldweb.Adapter.AddTechListAdapter;
import com.corefield.fieldweb.Adapter.DatesListAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.AMC.AMCDetails;
import com.corefield.fieldweb.DTO.AMC.AddAMC;
import com.corefield.fieldweb.DTO.AMC.ReminderModeList;
import com.corefield.fieldweb.DTO.AMC.ServiceOccurrenceList;
import com.corefield.fieldweb.DTO.Account.QuoteBindListDTO;
import com.corefield.fieldweb.DTO.Account.SaveQuotationDTO;
import com.corefield.fieldweb.DTO.Attendance.CheckOut;
import com.corefield.fieldweb.DTO.AttendanceCheck;
import com.corefield.fieldweb.DTO.ChangePassword;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.DatesDto;
import com.corefield.fieldweb.DTO.Delete.DeleteOwnerTechnician;
import com.corefield.fieldweb.DTO.Enquiry.AddEnquiry;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.DTO.Enquiry.ReferenceType;
import com.corefield.fieldweb.DTO.Enquiry.ServiceType;
import com.corefield.fieldweb.DTO.Expenditure.AddCredit;
import com.corefield.fieldweb.DTO.Expenditure.DeductBalance;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.DTO.Item.AddItem;
import com.corefield.fieldweb.DTO.Item.AddDeductUsedItem;
import com.corefield.fieldweb.DTO.Item.IssueItem;
import com.corefield.fieldweb.DTO.Item.ItemUnitType;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Item.UnAssignItem;
import com.corefield.fieldweb.DTO.LeadManagement.LeadStatusListDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.ReassignTask;
import com.corefield.fieldweb.DTO.Task.SendReport;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.Task.UpdateTask;
import com.corefield.fieldweb.DTO.Task.UpdateTaskStatus;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.DTO.User.AddAttendance;
import com.corefield.fieldweb.DTO.User.AddBulkTech;
import com.corefield.fieldweb.DTO.User.AddTech;
import com.corefield.fieldweb.DTO.User.CompanyDetails;
import com.corefield.fieldweb.DTO.User.LanguageList;
import com.corefield.fieldweb.DTO.User.LocationUpdateToServer;
import com.corefield.fieldweb.DTO.User.UpdateUser;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.DTO.UserDisclaimer.AcceptDisclaimer;
import com.corefield.fieldweb.DTO.UserDisclaimer.GetUserDisclaimer;
import com.corefield.fieldweb.FieldWeb.AMC.AMCListFragment;
import com.corefield.fieldweb.FieldWeb.Accounts.AccountsTabHost;
import com.corefield.fieldweb.FieldWeb.Admin.AddonsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.CRMFragmentTabHost;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerAttendanceFragment;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerItemInventoryFragmentNew;
import com.corefield.fieldweb.FieldWeb.Admin.PassbookExpenditureTabHost;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.AssetManagement.AddEditAssetDialog;
import com.corefield.fieldweb.FieldWeb.AssetManagement.AssetManagementFragment;
import com.corefield.fieldweb.FieldWeb.AssetManagement.QRCodeScanner.qrcodescanner.QrCodeActivity;
import com.corefield.fieldweb.FieldWeb.LeadManagement.AddEditLeadDialog;
import com.corefield.fieldweb.FieldWeb.LeadManagement.LeadManagementFragment;
import com.corefield.fieldweb.FieldWeb.ServiceManagement.ServiceManagementFragment;
import com.corefield.fieldweb.FieldWeb.Admin.SettingsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.TechAttendanceFragment;
import com.corefield.fieldweb.FieldWeb.Admin.TechItemsInventoryFragmentNew;
import com.corefield.fieldweb.FieldWeb.Admin.TechnicianListFragment;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddLiveLocationAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.SendEmailAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.TeleCMIAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateProfilePicAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.AMCDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddBalanceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.AlertDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.DeductBalanceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.ExpenditureDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog;
import com.corefield.fieldweb.FieldWeb.HealthAndSafety.HealthAndSafetyActivity;
import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;
import com.corefield.fieldweb.FieldWeb.Home.TechDashboardFragmentNew;
import com.corefield.fieldweb.FieldWeb.Intercom.Settings;
import com.corefield.fieldweb.FieldWeb.Notification.NotificationFragment;
import com.corefield.fieldweb.FieldWeb.OpenAI.OpenAIFragment;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.MainTaskFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.StartTaskTrackingFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskDetailsFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TechPaymentReceivedFragmentNew;
import com.corefield.fieldweb.FieldWeb.TechRouteTaskMap.TechnTaskRouteMapFragment;
import com.corefield.fieldweb.LocationService.GoogleLocAlarmReceiver;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Services.AlarmReceiver;
import com.corefield.fieldweb.Util.BadgeDrawable;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.EndDrawerToggle;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.GoogleMapUtils.LocationUpdatesService;
import com.corefield.fieldweb.Util.GoogleMapUtils.LocationUtils;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.facebook.appevents.AppEventsLogger;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import de.hdodenhof.circleimageview.CircleImageView;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;
import io.intercom.android.sdk.push.IntercomPushClient;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Activity for HomeActivityNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Activity  class is tha main Activity where all the fragment are used
 */
public class HomeActivityNew extends BaseActivity implements OnTaskCompleteListener, BaseActivity.RequestPermissionsUpdateListener {  //RecyclerTouchListener

    public static final int AUTOCOMPLETE_REQUEST_CODE = 23487;
    public static final int ALARM_REQUEST_CODE = 1253;
    public final int REQUEST_CODE_PERMISSIONS = 0x1;
    private String fromItemDialog = "";
    public Bitmap itemDialogBitmap;
    public static String fromPassbook = "";
    public static boolean isOwner = false; // Disable owner to edit tech profile
    protected static String TAG = HomeActivityNew.class.getSimpleName();
    public static int LOCATION_REQUEST_CODE = 1;
    //    public ConstraintLayout mBottomConstraint;
    public CoordinatorLayout mBottomCoordinator;
    public BottomNavigationView mBottomNavigationView;
    public UpdateLocation mUpdateLocation;
    TeleCMIAsyncTask teleCMIAsyncTask;
    public Toolbar mToolbar;
    //    private UpdateEnquiryAsyncTask mUpdateEnquiryAsyncTask;
    public List<ItemUnitType.ResultData> mitemUnitTypeList = null;
    private UpdateProfilePicAsyncTask mUpdateProfilePicAsyncTask;
    private AddLiveLocationAsyncTask mSendLocationToServerAsyncTask;
    private SendEmailAsyncTask mSendEmailAsyncTask;
    AddBulkTech.ResultData techObj = null;
    private String fromWhere = "";
    private static final int REQUEST_CODE_QR_SCAN = 101;

    public RequestPermissionsUpdateListener mListener;
    private BroadcastReceiver mNotificationBroadcastReceiver;
    private IntentFilter batteryIntentFilter;

    private MenuItem mNotificationBadge; // This should be global as we access through allover class
    private CompanyDetails mCompanyDetails = null;
    // The BroadcastReceiver used to listen from broadcasts from the service.
    private LocationUpdateReceiver mLocationUpdateReceiver;
    // A reference to the service used to get location updates.
    private LocationUpdatesService mLocationUpdatesService = null;
    // Tracks the bound state of the service.
    public boolean mBound = false;
    public static String OwnerAppTourFlag = "";
    public static boolean onDemandAppTourFlag = false;
    boolean isValidateSuccess = false;
    TextView mTextViewValidate, mTextViewSubmit;
    List<AddBulkTech.ResultData> mTechLists = null;
    //private boolean isAttendanceMarked = false;
    //static String tempAttdMarkedLoccation = "";

    private static final int MAX_FILE_SIZE = 6 * 1024 * 1024; // 6MB
    private String selectedFileName;

    // INTERCOME FILES : START
    private Settings settings;
    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    private String mTaskCreatedBy = "";
    private boolean isCompanyClicked = false;
    private RecyclerView.LayoutManager mLayoutManager;
    public RecyclerView mDatesRecyclerView;
    public DatesListAdapter mDatesListAdapter;
    public List<DatesDto> mDatesLists = null;

    public Spinner mSpinnerAMCMonthYear = null, mSpinnerDaysFilter = null, mSpinnerAMCType = null; //mSpinnerMonthYear = null,
    public EditText mEditTextYearMonth;
    public TextView mTextViewSpace;
    public LinearLayout mLinearHeaderLayout;
    public RelativeLayout mRelativeHeaderLayout;
    public TextView textViewUsername, textViewDrawerName, textViewMobile;
    public LinearLayout linearRating;
    public RelativeLayout relativeLogout;
    public Spinner mTaskTypeSpinner, mTaskStatusSpinner;
    //    public SearchView mSearchView;
    public FloatingActionButton mFabOptions;
    public CircleImageView mProfileCircleImageView;
    public int mSelectedDate;
    public int mSelectedMonth;
    public int mSelectedYear;
    public String mSelectedMonthName;
    public List<Integer> monthList = null;
    NavigationView navigationView;
    //ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;
    UserDetails.ResultData mUserDetails;
    private EndDrawerToggle drawerToggle;
    public TasksList.ResultData mTaskListData = new TasksList.ResultData();
    //
    public ArrayList<String> mSpineerLeadStatusList;
    public List<LeadStatusListDTO.ResultData> mLeadStatusList = null;
    //
    private static final int PICK_CONTACT_REQUEST_TASK = 108;
    private static final int PICK_CONTACT_REQUEST_AMC = 109;
    private static final int PICK_CONTACT_REQUEST_LEAD = 110;
    private static final int PICK_CONTACT_REQUEST_ENQUIRY = 111;
    private static final int PICK_CONTACT_REQUEST_ENQUIRYFORSERVICE = 112;
    private static final int PICK_CONTACT_REQUEST_ADDUPDATE_CUSTPRI = 113;
    private static final int PICK_CONTACT_REQUEST_ADDUPDATE_CUSTSEC = 114;
    private static final int PICK_CONTACT_REQUEST_ADDTECH = 115;

    private Uri mImageUri;
    private String mCapturedImagePath = "";
    private String mImageEncodeBaseString;

    public boolean isTaskEnd = false, isTaskClosure = false, isTaskStart = false, isTaskCountdown = false, isTaskRejected = false, isCompleted = false;
    private LatLng mCoordinateOrigin = null;
    protected double mFieldLat = 0, mFieldLong = 0, mTechLat = 0, mTechLong = 0;
    private String mTaskStatus = "", mPaymentMode = "";
    private boolean isGPSStatus = false;
    private int mTaskStatusId = 0, mTaskState = 0, mPaymentModeId = 0;

    public List<CustomerList.ResultData> mCustomerList;
    private List<ExpenseTechList.ResultData> mExpenseTechList = null;
    public List<ServiceOccurrenceList.ResultData> mServiceOccurrenceResultList;
    public List<ReminderModeList.ResultData> mReminderModeResultList;
    public ArrayList<String> mServiceOccurrenceTypeList;
    public ArrayList<String> mReminderModeList;
    public List<ItemsList.ResultData> mItemsLists = null;
    public List<UsersList.ResultData> mUsersLists = null;
    /*public List<ServiceType.ResultData> mServiceTypeResultData = null;*/
    public List<EnquiryServiceTypeDTO.ResultData> mServiceTypeResultData = null;
    public List<ReferenceType.ResultData> mReferenceTypeResultData = null;
    public List<ServiceType.ResultData> mServiceTypeResultDataProfile = null;
    //    public List<AMCsList.ResultData> mAMCListResultData = null;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    public ArrayList<String> mServiceTypeList;
    public ArrayList<String> mReferenceTypeList;
    private int mAmount = 0;
    public boolean isAMCOpen = false;
    public Calendar cal;
    public int currentYear, currentMonth, currentDay, lastMonth, secondLastMonth;
    private boolean isGPSEnabled = false;
    private boolean isGPSEnabledByUser = false;
    LocationManager gpsManager = null;
    public List<QuoteBindListDTO.ResultData> mQuoteBindListResultData = null;
    public ArrayList<String> mQuoteBindList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);
        gpsManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mExpenseTechList = new ArrayList<>();
        settings = new Settings(getApplicationContext());
//        monthList = new ArrayList<Integer>();
        FWLogger.logInfo(TAG, "Activity life cycle onCreate");
        mBottomNavigationView = findViewById(R.id.navigation);
//        mBottomConstraint = findViewById(R.id.layout_constraint);
        mBottomCoordinator = findViewById(R.id.layout_coordinator);
        mBottomNavigationView.setBackground(null);
        monthList = new ArrayList<Integer>();

        mFabOptions = findViewById(R.id.fab_options);
        mFabOptions.bringToFront();
        navigationView = (NavigationView) findViewById(R.id.navview);
        navigationView.setItemIconTintList(null);
        View header = navigationView.getHeaderView(0);
        drawerLayout = findViewById(R.id.drawer);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.dashboard);
        setSupportActionBar(mToolbar);

        textViewDrawerName = header.findViewById(R.id.textView_name);
        textViewMobile = header.findViewById(R.id.textView_mobile);
        mProfileCircleImageView = header.findViewById(R.id.imageView_profile);
        linearRating = header.findViewById(R.id.linear_rating);
        relativeLogout = header.findViewById(R.id.relative_logout);
        initNavigationDrawer();

        mDatesRecyclerView = findViewById(R.id.recycler_dates);

        /*mFabOptions.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(HomeActivityNew.this, "FAB Touched!", Toast.LENGTH_SHORT).show();
                return false;
            }
        });*/

        /*mFabOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(HomeActivityNew.this, "FAB Clicked!", Toast.LENGTH_SHORT).show();
                if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.addOptionsDialog(HomeActivityNew.this);
                }
            }
        });*/

        relativeLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                FWLogger.logInfo(TAG, "USerId : " + SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_logout));
                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                AlertDialog.confirmationAlertDialog(HomeActivityNew.this, getResources().getString(R.string.title_logout));
            }
        });

        mProfileCircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                isProfileImgClicked = true;
                getUSerDetails();

                drawerLayout.closeDrawer(Gravity.LEFT);
                ProfileFragmentNew profileFragmentNew = new ProfileFragmentNew();
                Bundle bundle = new Bundle();
                bundle.putSerializable("profileData", mUserDetails);
                bundle.putString("screen", "home");
                profileFragmentNew.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_fragment_container, profileFragmentNew);
                transaction.commit();
                mBottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);

                /*UserDialog userDialog = UserDialog.getInstance();
                userDialog.editProfileNewDialog(HomeActivityNew.this, mUserDetails);*/
            }
        });

        // START INTIALIZE INTERCOM
        initializeInterCom();
        onCreateInitialiseIntercom();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.i(TAG, "New token " + newToken);
                intercomPushClient.sendTokenToIntercom(HomeActivityNew.this.getApplication(), newToken);

            }
        });
        // END INTIALIZE INTERCOM

        Menu nav_Menu = navigationView.getMenu();
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            linearRating.setVisibility(View.GONE);
            //relativeLogout.setVisibility(View.GONE);
            hideOwnerNavigationItem();
            nav_Menu.findItem(R.id.item_inventory).setTitle(R.string.item_inventory);
        } else {
            linearRating.setVisibility(View.VISIBLE);
            relativeLogout.setVisibility(View.VISIBLE);
            hideTechNavigationItem();
            nav_Menu.findItem(R.id.item_inventory).setTitle(R.string.issued_item);
//            nav_Menu.findItem(R.id.add_options).setVisible(false);
        }

        cal = Calendar.getInstance();
        currentYear = cal.get(Calendar.YEAR);
//        int lastYear = cal.get(Calendar.YEAR) -1;
        currentMonth = cal.get(Calendar.MONTH) + 1;
        currentDay = cal.get(Calendar.DAY_OF_MONTH);
        mSelectedDate = currentDay;
        FWLogger.logInfo(TAG, "currentYear : " + currentYear + " currentMonth : " + currentMonth + " currentDay : " + currentDay); //"LastYear : "+lastYear+

        getDatesList(cal, currentMonth, currentDay);

        mLinearHeaderLayout = findViewById(R.id.linear_task_list_header);
        mRelativeHeaderLayout = findViewById(R.id.relative_header);
        mTaskTypeSpinner = findViewById(R.id.spin_task_type);
        mTaskStatusSpinner = findViewById(R.id.spin_task_status);

//        mSpinnerMonthYear = findViewById(R.id.spin_year_month);
        mEditTextYearMonth = findViewById(R.id.editText_year_month);
        mSpinnerDaysFilter = findViewById(R.id.spin_days_filter);
        mSpinnerAMCMonthYear = findViewById(R.id.spin_amc_year_month);
        mSpinnerAMCType = findViewById(R.id.spin_amc_type);
        mTextViewSpace = findViewById(R.id.textView_space);
        textViewUsername = findViewById(R.id.textView_username);

        //To change spinner on dashboard
//        mSpinnerMonthYear.setVisibility(View.VISIBLE);

        lastMonth = cal.get(Calendar.MONTH);
        secondLastMonth = cal.get(Calendar.MONTH) - 1;
        FWLogger.logInfo(TAG, "currentMonth : " + currentMonth + "  lastMonth : " + lastMonth + "  secondLastMonth : " + secondLastMonth);
        monthList.add(currentMonth);
        monthList.add(lastMonth);
        monthList.add(secondLastMonth);

        getDaysFilterList();

//        getMonthYearList(); //cal, currentYear, currentMonth

        //when an item is selected from menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

                switch (item.getItemId()) {
                    /*case R.id.dashboard:
//                        Toast.makeText(getApplicationContext(),"dashboard",Toast.LENGTH_SHORT).show();
                        mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                        mToolbar.setTitle(R.string.fieldweb);
                        clearBackStackEntries();
                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
                        } else {
                            loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
                        }
                        //close drawer
                        drawerLayout.closeDrawer(GravityCompat.END);
                        break;*/

                    case R.id.item_inventory:
//                        Toast.makeText(getApplicationContext(),"item_inventory",Toast.LENGTH_SHORT).show();
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            mToolbar.setTitle(R.string.item_inventory);
                            loadFragment(new OwnerItemInventoryFragmentNew(), R.id.navigation_home);
                            // loadFragment(new ItemInventoryTabHost(), R.id.navigation_home);
                        } else {
                            mToolbar.setTitle(R.string.issued_item_list);
                            loadFragment(new TechItemsInventoryFragmentNew(), R.id.navigation_home);
                        }

                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.technician_list:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.amc);
                        HomeActivityNew.isOwner = false;
                        loadFragment(new AMCListFragment(), R.id.navigation_home);
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.attendance:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.attendance);
                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            loadFragment(new OwnerAttendanceFragment(), R.id.navigation_home);
                        } else {
                            loadFragment(new TechAttendanceFragment(), R.id.navigation_crm);
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.passbook:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        /*mToolbar.setTitle(R.string.passbook);*/
                        mToolbar.setTitle(R.string.fieldweb);
                        /*loadFragment(new HomePassbookFragment(), R.id.navigation_home);*/
                        /*loadFragment(new HomePassbookFragmentNew(), R.id.navigation_home);*/
                        loadFragment(new PassbookExpenditureTabHost(), R.id.navigation_home);
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.expenditure:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        /*mToolbar.setTitle(R.string.expenditure);*/
                        mToolbar.setTitle(R.string.fieldweb);
                       /* if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            loadFragment(new ExpenseTechnicianListFragment(), R.id.navigation_home);
                        } else {
                            loadFragment(new ExpenseDetailsFragment(), R.id.navigation_home);
                        }*/
                        loadFragment(new PassbookExpenditureTabHost(), R.id.navigation_home);
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.services:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            mToolbar.setTitle(R.string.service_management);
                            loadFragment(new ServiceManagementFragment(), R.id.navigation_home);
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.accounts:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            mToolbar.setTitle("Accounts");
                            loadFragment(new AccountsTabHost(), R.id.navigation_home);
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.leadmanagement:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            mToolbar.setTitle("Leads");
                            loadFragment(new LeadManagementFragment(), R.id.navigation_home);
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                   /* case R.id.assetmanagement:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            mToolbar.setTitle("Assets");
                            loadFragment(new AssetManagementFragment(), R.id.navigation_home);
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;*/

                   /* case R.id.leadform:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                            mToolbar.setTitle("Leads");
                            try {
                                checkCameraPermission();
                              *//*  //AppCompatActivity activity = (AppCompatActivity) view.getContext();
                                AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                                addEditLeadDialog.addLead(HomeActivityNew.this, HomeActivityNew.this);*//*
                                mToolbar.setTitle("Leads");
                                loadFragment(new LeadsFragmentForTechnician(), R.id.navigation_home);
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;*/

                    case R.id.subscription:
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    /*case R.id.features:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);
                        mToolbar.setTitle(R.string.features);
                        loadFragment(new UpcomingFeaturesFragment(), R.id.navigation_home);
                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;*/
                    case R.id.fieldwebai:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.AI);
                        loadFragment(new OpenAIFragment(), R.id.navigation_home);
                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    case R.id.setting:
//                        Toast.makeText(getApplicationContext(),"setting",Toast.LENGTH_SHORT).show();
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.settings);
                        loadFragment(new SettingsFragment(), R.id.navigation_home);
                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;

                    /*case R.id.about:

                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.about);
                        loadFragment(new AboutFragment(), R.id.navigation_home);
                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);*/

                      /*  mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                        mToolbar.setTitle(R.string.dashboard);
                        clearBackStackEntries();
                        onDemandAppTourFlag = true;
                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
                        } else {
                            loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
                        }
                        drawerLayout.closeDrawer(GravityCompat.END);*/

                    //break;


                   /* case R.id.PlayStoreRating:
                        try {
                            PlayStoreRatingDialog(HomeActivityNew.this);
                            drawerLayout.closeDrawer(GravityCompat.END);
                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        break;*/

                    /*case R.id.help_support:
                        mRelativeHeaderLayout.setVisibility(View.GONE);
                        mLinearHeaderLayout.setVisibility(View.GONE);
                        mDatesRecyclerView.setVisibility(View.GONE);

                        mToolbar.setTitle(R.string.help_and_support);
                        *//* loadFragment(new HelpAndSupportFragment(), R.id.navigation_home);*//*
                        RegisterUserFor_Intercom();
//                        Toast.makeText(getApplicationContext(),"help_support",Toast.LENGTH_SHORT).show();
                        //close drawer
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        break;*/

                    case R.id.check_out:
                        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_check_out));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                        AlertDialog.checkOutDialog(HomeActivityNew.this, getResources().getString(R.string.checkout));
                        break;

                 /*   case R.id.invite:
                        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                            inviteFriendForOwner();
                        }
                        break;*/

                 /*   case R.id.delete_account:
                        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.delete_account));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                        DeleteOwnerSelfDialog(HomeActivityNew.this);
                        drawerLayout.closeDrawer(GravityCompat.END);
                        break;
*/
                   /* case R.id.log_out:
                        FWLogger.logInfo(TAG, "USerId : " + SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_logout));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                        AlertDialog.confirmationAlertDialog(HomeActivityNew.this, getResources().getString(R.string.title_logout));
//                        Toast.makeText(getApplicationContext(),"log_out",Toast.LENGTH_SHORT).show();
                        //close drawer
                        drawerLayout.closeDrawer(GravityCompat.END);
                        break;*/
                }

                return true;
            }
        });

        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            //getItemList();
            getUserList();
            getItemUnitType(); // ADDED BY MANISH : NEW API FOR UNIT : ADD ITEM
            getQuoteBindList();
            //getServiceType();
            //getReferenceType();
            //getCompanyDetails();
            checkDisclaimer();// Call API for check disclaimer is accepted or not
            getTechnicianList();
            //serviceOccurrenceTask();
            //reminderModeTask();
            getCustomerList();
            getLeadStatusList();
//            mBottomNavigationView.getMenu().removeItem(R.id.navigation_passbook);
        } else if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            //first register receiver to avoid crash on service update (Message broadcast) and then verify google play sever and start service
//            registerReceiver();//Old code
            batteryIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            mLocationUpdateReceiver = new LocationUpdateReceiver();
            LocalBroadcastManager.getInstance(this).registerReceiver(mLocationUpdateReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));

            isGPSEnabled = gpsManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            getGPSStatus();
            try {
                HomeActivityNew.this.registerReceiver(batteryBroadcastReceiver, batteryIntentFilter);
            } catch (Exception ex) {
                ex.getMessage();
            }
//            mBottomNavigationView.getMenu().removeItem(R.id.navigation_amc);
        }
        getUSerDetails();
        selectDashboardFragment();

        mBottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mToolbar.setOnMenuItemClickListener(mOnMenuItemClickListener);
        setNotificationBroadcastReceiver();
        // FB SETUP
        AppEventsLogger logger = AppEventsLogger.newLogger(this);

        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            if (!SharedPrefManager.getInstance(getApplicationContext()).isAppTourCompleted()) {
                OwnerAppTourSkippable();
            }
        }
    }

    private void inviteFriendForOwner() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Namaste \uD83D\uDE4F,\n" + "I am using *India's #1* field business management app - \uD83D\uDCA5 *FieldWeb* \n\n" + "Benefits of using FieldWeb: \n\n" + "• Revenue increased \uD83D\uDCC8 by 75% \n" + "• Keeps data Safe \uD83D\uDEE1 and Secure. \n" + "• Tracks fieldworker's \uD83D\uDC68\u200D\uD83D\uDD27 activity. \n" + "• Assign task/job to fieldworkers \uD83D\uDC68\u200D\uD83D\uDD27. \n" + "• Sends service reminder to customers \uD83D\uDC68\u200D\uD83D\uDC68\u200D\uD83D\uDC67. \n\n" + "Download Now: \n\n " + "https://bit.ly/3MZiwEJ");
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void getGPSStatus() {
        // CALL TO LISTEN GPS ON/OFF
        IntentFilter filter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_PROVIDER_CHANGED);
        HomeActivityNew.this.registerReceiver(GPSBroadcastReceiver, filter);
    }

    /**
     * Interface to update location on tracking screen
     */
    public interface UpdateLocation {
        void onLocationUpdate(double latitude, double longitude);
    }

    /**
     * Monitors the state of the connection to the service.
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mLocationUpdatesService = binder.getService();
            mBound = true;
//            FWLogger.logInfo(TAG, "CHECK : "+SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
                verifyLocationSettings();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mLocationUpdatesService = null;
            mBound = false;
        }
    };

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            FWLogger.logInfo(TAG, "onReceive: " + LocationUtils.getLocationText(location));
            if (location != null) {
                FWLogger.logInfo(TAG, "Got The location : " + LocationUtils.getLocationText(location));
                double latitude = 0;
                double longitude = 0;
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if (latitude != 0 && longitude != 0) {
                    FWLogger.logInfo(TAG, getString(R.string.msg_location_service_started) + "\n Home_Location_Latitude : " + latitude + "\n Home_Location_Longitude: " + longitude);
                    //setupAlarm(900000); // 5 MIN
                    //mMsgView.setText(getString(R.string.msg_location_service_started) + "\n Latitude : " + latitude + "\n Longitude: " + longitude);
                    if (mUpdateLocation != null)
                        mUpdateLocation.onLocationUpdate(latitude, longitude);
                    //Check for Null pointer exception (if log out this can be a null)
                    if (SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup() != null) {
                        FWLogger.logInfo(TAG, "User : " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                        //This API was called for store data on FW server
                        /*if (SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
                            sendLocationToServer(latitude, longitude);*/
                    }
                }
            }
        }
    }

    /**
     * Create option menu
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        mNotificationBadge = menu.findItem(R.id.notification_toolbar);

        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            /*MenuItem addOptions = menu.findItem(R.id.add_options);
            addOptions.setVisible(false);*/
            if (mFabOptions.isShown()) mFabOptions.hide();

            menu.findItem(R.id.trackTask).setVisible(true);

            mBottomNavigationView.getMenu().removeItem(R.id.placeholder);

            mBottomNavigationView.getMenu().findItem(R.id.navigation_tech).setIcon(getResources().getDrawable(R.drawable.ic_book));
            mBottomNavigationView.getMenu().findItem(R.id.navigation_tech).setTitle(getString(R.string.title_passbook));

            mBottomNavigationView.getMenu().findItem(R.id.navigation_crm).setIcon(getResources().getDrawable(R.drawable.ic_calendar));
            mBottomNavigationView.getMenu().findItem(R.id.navigation_crm).setTitle(getString(R.string.attendance));
        } else {
            /*MenuItem addOptions = menu.findItem(R.id.add_options);
            addOptions.setVisible(true);*/
            if (!mFabOptions.isShown()) mFabOptions.show();

            mBottomNavigationView.getMenu().findItem(R.id.navigation_tech).setIcon(getResources().getDrawable(R.drawable.ic_tech));
            mBottomNavigationView.getMenu().findItem(R.id.navigation_tech).setTitle(getString(R.string.tech));
        }

//        mBottomNavigationView.getMenu().findItem(R.id.placeholder).setEnabled(false);
        //drawerToggle.setToggleOnMenu(menu);

        /*if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            addTask.setVisible(false);
            addTech.setVisible(false);
            addEnq.setVisible(false);
//            passbook.setVisible(false);
            changeCompanyName.setVisible(false);
            changeCompanyName.setEnabled(false);
            if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyEnabled()) {
                healthAndSafety.setVisible(true);
            } else {
                healthAndSafety.setVisible(false);
            }
            mBottomNavigationView.getMenu().findItem(R.id.navigation_amc).setTitle(getString(R.string.title_passbook));
        } else {
            mBottomNavigationView.getMenu().findItem(R.id.navigation_amc).setTitle(getString(R.string.title_amc));
            healthAndSafety.setVisible(false);
            checkOut.setVisible(false);
        }*/
        return true;
    }

    /**
     * Listener for Toolbar item (@link OnMenuItemClickListener)
     */
    private Toolbar.OnMenuItemClickListener mOnMenuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            switch (item.getItemId()) {
                /*case R.id.add_options:
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        CommonDialog commonDialog = CommonDialog.getInstance();
                        commonDialog.addOptionsDialog(HomeActivityNew.this);
                    }
                    return true;*/
                case R.id.notification_toolbar:
                    clearBackStackEntries();
                    mRelativeHeaderLayout.setVisibility(View.GONE);
                    mLinearHeaderLayout.setVisibility(View.GONE);
                    mDatesRecyclerView.setVisibility(View.GONE);

                    mToolbar.setTitle(R.string.title_notification);
                    bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_notification));
                    FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                    addFragment(new NotificationFragment());//Zero means layout not belongs to navigation bottom but should lean into the fragment container
                    return true;

                /*case R.id.assetqr:
                    clearBackStackEntries();
                    try {
                        Intent i = new Intent(HomeActivityNew.this, QrCodeActivity.class);
                        startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                    return true;*/

                case R.id.addons:
                    clearBackStackEntries();
                    /*mRelativeHeaderLayout.setVisibility(View.GONE);
                    mLinearHeaderLayout.setVisibility(View.GONE)*/
                    mDatesRecyclerView.setVisibility(View.GONE);
                    mToolbar.setTitle(R.string.addons);
                    addFragment(new AddonsFragment());
                    return true;

                case R.id.trackTask:
                    clearBackStackEntries();
                    try {
                        if (TechDashboardFragmentNew.isDataAvailable) {
                            mDatesRecyclerView.setVisibility(View.GONE);
                            mToolbar.setTitle(R.string.trackTask);
                            addFragment(new TechnTaskRouteMapFragment());
                        } else {
                            TechDashboardFragmentNew.isDataAvailable = false;
                            Toast.makeText(HomeActivityNew.this, "There is no Task assigned for you today!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                    return true;

               /* case R.id.help:
                    clearBackStackEntries();
                    mRelativeHeaderLayout.setVisibility(View.GONE);
                    mLinearHeaderLayout.setVisibility(View.GONE);
                    mDatesRecyclerView.setVisibility(View.GONE);
                    mToolbar.setTitle(R.string.help_and_support);
                    RegisterUserFor_Intercom();
                    return true;*/
                case R.id.help:
                    clearBackStackEntries();
                    RegisterUserFor_Intercom();
                    return true;
            }
            return false;
        }
    };

    /**
     * Listener for BottomNavigationView (Bottom tab)
     */
    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mToolbar.setTitle(R.string.fieldweb);
                    mDatesRecyclerView.setVisibility(View.GONE);
                    mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                    bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.dashboard));
                    FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                    clearBackStackEntries();
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
                    } else {
                        loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
                    }
                    return true;
                case R.id.navigation_task:
                    //mToolbar.setTitle(R.string.tasks);
                    bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.tasks));
                    FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                    clearBackStackEntries();
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        getItemList();
                        if (mUsersNameList.size() == 0) {
                            AddTechDialog(HomeActivityNew.this);
                        } else {
                            mToolbar.setTitle(R.string.tasks);
                            mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                            mDatesRecyclerView.setVisibility(View.GONE);
                            mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                            loadFragment(new MainTaskFragmentNew(), R.id.navigation_task);
                        }
                    } else {
                        mToolbar.setTitle(R.string.tasks);
                        mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                        mDatesRecyclerView.setVisibility(View.GONE);
                        mRelativeHeaderLayout.setVisibility(View.VISIBLE);
                        loadFragment(new MainTaskFragmentNew(), R.id.navigation_task);
                    }
                    return true;

                case R.id.placeholder:
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        // IF TECH SIZE = 0 SHOW ADD TECH POPUP
                        if (mUsersNameList.size() == 0) {
                            AddTechDialog(HomeActivityNew.this);
                        } else {
                            CommonDialog commonDialog = CommonDialog.getInstance();
                            commonDialog.addOptionsDialog(HomeActivityNew.this);
                        }
                    }
                    return true;
                case R.id.navigation_crm:
                   /* mRelativeHeaderLayout.setVisibility(View.GONE);
                    mLinearHeaderLayout.setVisibility(View.GONE);
                    mDatesRecyclerView.setVisibility(View.GONE);*/

                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                        mToolbar.setTitle(getString(R.string.attendance));
                        bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.attendance));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                        loadFragment(new TechAttendanceFragment(), R.id.navigation_crm);
                        clearBackStackEntries();
                    } else {
                        /*mToolbar.setTitle(R.string.crm);
                        bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.crm));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                        clearBackStackEntries();
                        loadFragment(new CRMFragment(), R.id.navigation_crm);*/
                        if (mUsersNameList.size() == 0) {
                            AddTechDialog(HomeActivityNew.this);
                        } else {
                            getItemList();
                            mToolbar.setTitle(R.string.crm);
                            bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.crm));
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                            clearBackStackEntries();
                            /*loadFragment(new CRMFragment(), R.id.navigation_crm);*/
                            loadFragment(new CRMFragmentTabHost(), R.id.navigation_crm);
                        }
                    }
                    return true;
                /*case R.id.navigation_admin:
                    mToolbar.setTitle(R.string.title_admin);
                    bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.title_admin));
                    FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                    clearBackStackEntries();
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        loadFragment(new OwnerAdminFragment(), R.id.navigation_admin);
                    } else {
                        loadFragment(new TechAdminFragment(), R.id.navigation_admin);
                    }
                    return true;*/
                case R.id.navigation_tech:
                   /* mRelativeHeaderLayout.setVisibility(View.GONE);
                    mLinearHeaderLayout.setVisibility(View.GONE);
                    mDatesRecyclerView.setVisibility(View.GONE);*/

                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                        mToolbar.setTitle(R.string.passbook);
                        fromPassbook = "Tech_Passbook";
                        bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.passbook));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                        if (item.getTitle().equals(getString(R.string.title_amc))) {
                            item.setTitle(R.string.passbook);
//                            bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.passbook));
//                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                        }
                        clearBackStackEntries();
                        /*loadFragment(new HomePassbookFragment(), R.id.navigation_amc);*/
                        // loadFragment(new HomePassbookFragmentNew(), R.id.navigation_tech);
                        loadFragment(new PassbookExpenditureTabHost(), R.id.navigation_home);
                        return true;
                    } else {
                        /*isAMCOpen = true;
                        mToolbar.setTitle(R.string.title_amc);
                        bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.title_amc));
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                        clearBackStackEntries();
//                        loadFragment(new AMCDashboardFragment(), R.id.navigation_amc);
                        loadFragment(new AMCListFragment(), R.id.navigation_amc);*/
                        if (mUsersNameList.size() == 0) {
                            AddTechDialog(HomeActivityNew.this);
                        } else {
                            isAMCOpen = true;
                            mToolbar.setTitle(R.string.technician_list);
                            bundle.putString(FirebaseGoogleAnalytics.Param.BOTTOM_MENU, getString(R.string.tech));
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.BOTTOM_NAV_MENU_CLICK, bundle);
                            clearBackStackEntries();
                            loadFragment(new TechnicianListFragment(), R.id.navigation_tech);
                        }
                        return true;
                    }
            }
            return false;
        }
    };

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }*/


    public void initNavigationDrawer() {
       /* drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerToggle = new EndDrawerToggle(drawerLayout,
                mToolbar,
                R.string.open,
                R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);*/
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);


    }

    /*public void getMonthYearList() {  //Calendar cal, int currentYear, int currentMonth
        List<String> monthYearList = new ArrayList<String>();
        String[] mons = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        String currentMonthName = mons[currentMonth - 1];
        String lastMonthName = mons[lastMonth - 1];
        String secondLastMonthName = mons[secondLastMonth - 1];

        monthYearList.add(currentMonthName + " " + currentYear);
        monthYearList.add(lastMonthName + " " + currentYear);
        monthYearList.add(secondLastMonthName + " " + currentYear);

        ArrayAdapter<String> arrayAdapterMonthYearType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthYearList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.WHITE);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(0)); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.WHITE);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount(); // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterMonthYearType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMonthYear.setAdapter(arrayAdapterMonthYearType);
        mSpinnerMonthYear.setSelection(0);  //arrayAdapterMonthYearType.getCount()
        mSelectedMonth = currentMonth;
        mSelectedYear = currentYear;
        mSelectedMonthName = currentMonthName;
//        arrayAdapterMonthYearType.notifyDataSetChanged();
    }*/

    public void getDaysFilterList() {  //Calendar cal, int currentYear, int currentMonth
        List<String> daysFilterList = new ArrayList<String>();
        String[] mons = new DateFormatSymbols(Locale.ENGLISH).getMonths();
        String currentMonthName = mons[currentMonth - 1];

        daysFilterList.add(getString(R.string.today));
        daysFilterList.add(getString(R.string.week));
        daysFilterList.add(getString(R.string.month));
        daysFilterList.add(getString(R.string.year));

        ArrayAdapter<String> arrayAdapterMonthYearType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, daysFilterList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.WHITE);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(0)); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.WHITE);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount(); // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterMonthYearType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDaysFilter.setAdapter(arrayAdapterMonthYearType);
        mSpinnerDaysFilter.setSelection(0);  //arrayAdapterMonthYearType.getCount()
    }

    public void getDatesList(Calendar cal, int currentMonth, int currentDay) {
        cal.set(Calendar.MONTH, currentMonth - 1);
        cal.set(Calendar.DAY_OF_MONTH, currentDay);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        FWLogger.logInfo(TAG, "Max Days : " + maxDay);
        SimpleDateFormat df = new SimpleDateFormat("dd");
        if (mDatesLists != null) mDatesLists.clear();

        mDatesListAdapter = null;
        mDatesLists = new ArrayList<>();
        for (int i = 0; i < maxDay; i++) {
            DatesDto datesDto = new DatesDto();
            cal.set(Calendar.DAY_OF_MONTH, i + 1);
//            FWLogger.logInfo(TAG,"Test : " + df.format(cal.getTime()) + "  Days : "+new SimpleDateFormat("EE", Locale.ENGLISH).format(cal.getTime()));
            datesDto.setDate(Integer.parseInt(df.format(cal.getTime())));
            datesDto.setDay(new SimpleDateFormat("EE", Locale.ENGLISH).format(cal.getTime()));
            mDatesLists.add(datesDto);
        }

        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager = new LinearLayoutManager(HomeActivityNew.this, LinearLayoutManager.HORIZONTAL, false);
        mDatesRecyclerView.setLayoutManager(mLayoutManager);
        mDatesListAdapter = new DatesListAdapter(this, mDatesLists, DateUtils.getCurrentDateOnly());
//        mDatesListAdapter.setClickListener(this);
        mDatesRecyclerView.setAdapter(mDatesListAdapter);
        mDatesRecyclerView.scrollToPosition(mSelectedDate - 1);
//        mDatesRecyclerView.scrollToPosition(20);
        mDatesListAdapter.notifyDataSetChanged();
    }

    private void hideTechNavigationItem() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.technician_list).setVisible(false);
        nav_Menu.findItem(R.id.subscription).setVisible(false);
        nav_Menu.findItem(R.id.passbook).setVisible(false);
        //nav_Menu.findItem(R.id.invite).setVisible(false);
        nav_Menu.findItem(R.id.services).setVisible(false);
        nav_Menu.findItem(R.id.accounts).setVisible(false);
        nav_Menu.findItem(R.id.leadmanagement).setVisible(false);
        //nav_Menu.findItem(R.id.assetmanagement).setVisible(false);
        //nav_Menu.findItem(R.id.log_out).setVisible(false);
    }

    private void hideOwnerNavigationItem() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.check_out).setVisible(false);
        nav_Menu.findItem(R.id.expenditure).setVisible(false);
        /*nav_Menu.findItem(R.id.leadform).setVisible(false);*/
    }

    private void setNotificationBroadcastReceiver() {
        mNotificationBroadcastReceiver = new BroadcastReceiver() {//NOTE Commented may use in latter time for known issue
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constant.FCM.UNREAD_FOREGROUND)) {
                    FWLogger.logInfo(TAG, "notification onReceive");
                    updatedBadgeCountOnForegroundNotification();
                    if (intent.getExtras() != null) {
                        String updateBadge = intent.getExtras().getString(Constant.FCM.UPDATE_BADGE);
                        //Also update the notification list only if the notification fragment is in foreground
                        if (updateBadge != null) {
                            if (updateBadge.equalsIgnoreCase(Constant.FCM.UPDATE_BADGE)) {
                                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
                                if (currentFragment != null && currentFragment instanceof NotificationFragment) {
                                    //DO STUFF
                                    FWLogger.logInfo(TAG, "notificationFragment is found in foreground");
                                    if (((NotificationFragment) currentFragment).mNewNotificationListener != null) {
                                        FWLogger.logInfo(TAG, "mNewNotificationListener is not null");
                                        ((NotificationFragment) currentFragment).mNewNotificationListener.onNewNotification();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        FWLogger.logInfo(TAG, "Activity life cycle onStart");
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        if (!mBound) {
            FWLogger.logInfo(TAG, "Activity life cycle Service to bind");
            bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection, Context.BIND_AUTO_CREATE);

        }
        // Everything else might be called multiple times
    }

    @Override
    protected void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "Activity life cycle onResume");
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationBroadcastReceiver, new IntentFilter(Constant.FCM.UNREAD_FOREGROUND));
    }

    public void verifyLocationSettings() {
        //Step 1 : Verify Google play service
        if (isGooglePlayServicesAvailable()) {
            FWLogger.logInfo(TAG, "verifyGooglePlayService ");
            //Go To Next step
            //Step 2 : Verify Internet connection
            if (isInternetConnectionAvailable()) {
                FWLogger.logInfo(TAG, "isInternetConnectionAvailable ");
                //Go To Next step
                //Step 3 : verify and check all permissions are enabled
                // if (checkAllPermissionEnabled()) {
                FWLogger.logInfo(TAG, "checkAllPermissionEnabled ");
                //Go To Next step
                //Step 4 : verify and check GPS enabled
                if (isGPSEnabled()) {
                    FWLogger.logInfo(TAG, "isGPSEnabled ");
                    isGPSStatus = true;
                    //Go To Next step and check owner /tech to start with location service/attendance
                    //Step 5 :  Start requesting location service and Check attendance API call
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                        startRequestingLocationService();
                        //if(!isAttendanceMarked)checkAttendance();//API call to check attendance
                        checkAttendance();//API call to check attendance
                        app_launched(HomeActivityNew.this); // APP RATER FOR TECH
                    }
                } else {
                    isGPSStatus = false;
                    FWLogger.logInfo(TAG, "GPS is not enabled request to enable");
                    //else show non cancelable dialog to enable GPS and on GPS enable then continue with first step from onActivityResult
                    if (!AlertDialog.isAlertDialogShowing) {
                        ServiceDialog serviceDialog = ServiceDialog.getInstance();
                        serviceDialog.showGPSEnableDialog(this);
                    }
                }
               /* } else {
                    isGPSStatus = false;
                    //else request to enable permissions and on enable permission continue with first step
                    FWLogger.logInfo(TAG, "Permission is not enabled request to enable");
                    mListener = this;
                    requestToEnableAllPermission(mListener, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION);
                }*/
            } else {
                FWLogger.logInfo(TAG, "Internet Connection not Available ");
                //else show non cancelable dialog to refresh internet connection and on connection enabled continue with first step
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.showRefreshInternetConnectionDialog(this);//this is recursive call
            }
        } else {
            FWLogger.logInfo(TAG, "Google Play Service not available ");
            //Toast.makeText(getApplicationContext(), R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
            //else show non cancelable dialog and exit user from app, on adding google play service continue with first step
            ServiceDialog serviceDialog = ServiceDialog.getInstance();
            serviceDialog.showNoGooglePlayServiceAvailableDialog(this);
        }
    }

    public boolean isInternetConnectionAvailable() {
        FWLogger.logInfo(TAG, "verifyInternetConnection ");
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean isGPSEnabled() {
        FWLogger.logInfo(TAG, "isLocationEnabled");
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    /**
     * Start requesting Location Monitor Service
     */
    public void startRequestingLocationService() {
        FWLogger.logInfo(TAG, "start Requesting LocationService ");
        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.
        if (mLocationUpdatesService != null) mLocationUpdatesService.requestLocationUpdates();
    }

    public void loadProfileFragment(UsersList.ResultData mUserDetails) {
        ProfileFragmentNew profileFragmentNew = new ProfileFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("profileData", mUserDetails);
        bundle.putString("screen", "techList");
        profileFragmentNew.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, profileFragmentNew);
        transaction.commit();
    }

    public void loadFragment(Fragment fragment, int resId) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putInt("Id", SharedPrefManager.getInstance(this).getUserId());
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.commit();
        mBottomNavigationView.getMenu().findItem(resId).setChecked(true);
    }

    public void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putInt("Id", SharedPrefManager.getInstance(this).getUserId());
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        /*transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);*/
        transaction.commit();
        mBottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);
    }

    public void navigateToTab(int resId) {
        clearBackStackEntries();
        mBottomNavigationView.setSelectedItemId(resId);
        mBottomNavigationView.getMenu().findItem(resId).setChecked(true);
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void navigateToHealthAndSafetyActivity() {
        Intent intent = new Intent(HomeActivityNew.this, HealthAndSafetyActivity.class);
        intent.putExtra("isFromOptionMenu", true);
        startActivity(intent);
    }

    /**
     * Update badge count of notification when notification arrives in foreground
     */
    private void updatedBadgeCountOnForegroundNotification() {
//        BitmapDrawable iconBitmap = (BitmapDrawable) mNotificationBadge.getIcon();
//        LayerDrawable icon = new LayerDrawable(new Drawable [] { iconBitmap });
        LayerDrawable icon = (LayerDrawable) mNotificationBadge.getIcon();
        BadgeDrawable badge;
        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(HomeActivityNew.this);
        }
        badge.setCount(String.valueOf(Constant.UNREAD_COUNT));
        icon.invalidateSelf();
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
        icon.invalidateSelf();
    }

    /**
     * For technician send(update) most recent location to server
     *
     * @param techRecentLat
     * @param techRecentLong
     */
    private void sendLocationToServer(double techRecentLat, double techRecentLong) {
        if (mSendLocationToServerAsyncTask == null) {
            mSendLocationToServerAsyncTask = new AddLiveLocationAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
            mSendLocationToServerAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), techRecentLat, techRecentLong);
        }
    }

    public void app_launched(Context mContext) {

        int DAYS_UNTIL_PROMPT = 3;//Min number of days
        int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                PlayStoreRatingDialog(HomeActivityNew.this);
            }
        }

        editor.commit();
    }


    private void selectDashboardFragment() {
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {

            if (getIntent().getExtras() != null) {
                if (getIntent().getExtras().getString("NewNotification") != null) {
                    clearBackStackEntries();
                    mToolbar.setTitle("Notification");
                    //TODO Issue of bottom navigation crash
                    loadFragment(new NotificationFragment(), R.id.notification_toolbar);//Zero means layout not belongs to navigation bottom but should lean into the fragment container
                } else {
                    loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
                }
            } else {
                mBottomNavigationView.post(new Runnable() {
                    @Override
                    public void run() {
                        mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                    }
                });
                loadFragment(new OwnerDashboardFragment(), R.id.navigation_home);
            }

        } else if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {

            if (getIntent().getExtras() != null) {
                if (getIntent().getExtras().getString("NewNotification") != null) {
                    clearBackStackEntries();
                    mToolbar.setTitle("Notification");
                    //TODO Issue of bottom navigation crash
                    loadFragment(new NotificationFragment(), R.id.notification_toolbar);//Zero means layout not belongs to navigation bottom but should lean into the fragment container
                } else {
                    loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
                }
            } else {
                mBottomNavigationView.post(new Runnable() {
                    @Override
                    public void run() {
                        mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                    }
                });
                loadFragment(new TechDashboardFragmentNew(), R.id.navigation_home);
            }

        }
    }

    private void logoutAndNavigateToLogin() {
        SharedPrefManager.getInstance(HomeActivityNew.this).logout(false);
        Intent intent = new Intent(HomeActivityNew.this, LoginTouchlessActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void addTaskNew(int amcServiceDetailsId, boolean isAMC, AMCDetails.ResultData AMCResultData) {
        try {
            CommonFunction.showProgressDialog(HomeActivityNew.this);
            getItemList();
            getCustomerList();
            if (mUsersNameList != null && mUsersNameList.size() > 0) {
                AudioPermission();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        CommonFunction.hideProgressDialog(HomeActivityNew.this);
                        TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                        mTaskCreatedBy = FirebaseGoogleAnalytics.Defaults.AMC;
                        taskDialog.addTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, false, null, -1, amcServiceDetailsId, isAMC, AMCResultData, mCustomerList);
                    }
                }, 500);
            } else {
                CommonFunction.hideProgressDialog(HomeActivityNew.this);
                Toast.makeText(this, R.string.you_do_not_have_tech_add_at_least_one, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void addItem() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
        bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ADD);
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CLICK, bundle);

        checkCameraPermission();
        ItemDialog itemDialog = ItemDialog.getInstance();
        itemDialog.addItemDialog(HomeActivityNew.this, mitemUnitTypeList); //, ((OwnerItemInventoryFragment) fragment)
    }

    public void editProfile() {
        UserDialog userDialog = UserDialog.getInstance();
        userDialog.editProfileNewDialog(HomeActivityNew.this, mUserDetails);
    }

    public void addAMCDialog(ArrayList<String> mServiceOccurrenceTypeList, ArrayList<String> mReminderModeList) {
        AMCDialog amcDialog = AMCDialog.getInstance();
        amcDialog.addEditAMC(HomeActivityNew.this, mServiceOccurrenceTypeList, mReminderModeList, -1, -1, -1); //new AMCListFragment(),
    }

    public void assignItem() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
        bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ISSUE);
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CLICK, bundle);
        ItemDialog itemDialog = ItemDialog.getInstance();
        itemDialog.assignItemDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList);
    }

    public void addTask(int amcServiceDetailsId, boolean isAMC, AMCDetails.ResultData AMCResultData) {
        if (mUsersNameList != null && mUsersNameList.size() > 0) {
            getItemList();
            getCustomerList();
            AudioPermission();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                    mTaskCreatedBy = FirebaseGoogleAnalytics.Defaults.AMC;
                    taskDialog.addTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, false, null, -1, amcServiceDetailsId, isAMC, AMCResultData, mCustomerList);
                }
            }, 500);
        } else {
            Toast.makeText(this, R.string.you_do_not_have_tech_add_at_least_one, Toast.LENGTH_SHORT).show();
        }
    }

    public void addTask(int techID, int position) {
        getItemList();
        getCustomerList();
        AudioPermission();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                mTaskCreatedBy = FirebaseGoogleAnalytics.Defaults.TASK;
                taskDialog.addTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, false, null, position, 0, false, null, mCustomerList);
            }
        }, 500);
    }

    public void addTaskWithEnquiryDefaults(EnquiryList.ResultData enquiry) {
        if (mUsersNameList != null && mUsersNameList.size() > 0) {
            AudioPermission();
            TaskDialogNew taskDialog = TaskDialogNew.getInstance();
            mTaskCreatedBy = FirebaseGoogleAnalytics.Defaults.ENQ;
            taskDialog.addTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, true, enquiry, -1, 0, false, null, mCustomerList);
        } else {
            Toast.makeText(this, R.string.you_do_not_have_tech_add_at_least_one, Toast.LENGTH_SHORT).show();
        }
    }

    public void addTaskWithReassignDefaults(TasksList.ResultData mResultData) {
        mTaskCreatedBy = FirebaseGoogleAnalytics.Defaults.REASSIGN;
        TaskDialogNew taskDialog = TaskDialogNew.getInstance();
        //taskDialog.reAssignTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, mResultData, mUsersLists, mCustomerList);
        taskDialog.reAssignTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, mResultData, mCustomerList);  //mUsersLists
    }

    public void addTask() {
        //getItemList();
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_add_task));
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
        if (mUsersNameList.size() == 0) {
            Toast.makeText(HomeActivityNew.this, R.string.you_dont_have_tech, Toast.LENGTH_SHORT).show();
        } else {
            addTaskNew(0, false, null);
        }
    }

    public void addDeductBalance() {
        getTechnicianList();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ExpenditureDialog expenditureDialog = ExpenditureDialog.getInstance();
                expenditureDialog.addDeductBalance(HomeActivityNew.this, mExpenseTechList);
            }
        }, 700);
    }

    public void addBalance(String techName, int techId, int remainingBalance) {
        //getTechnicianList();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                AddBalanceDialog addBalanceDialog = AddBalanceDialog.getInstance();
                addBalanceDialog.addDeductBalance(HomeActivityNew.this, techName, techId, remainingBalance/*mExpenseTechList*/);
            }
        }, 700);
    }

    public void DeductBalance(String techName, int techId, int remainingBalance) {
        //getTechnicianList();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                DeductBalanceDialog deductBalanceDialog = DeductBalanceDialog.getInstance();
                deductBalanceDialog.addDeductBalance(HomeActivityNew.this, techName, techId, remainingBalance/*mExpenseTechList*/);
            }
        }, 700);
    }

    public void addEnquiry() {
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_add_enquiry));
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);

        EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
//        enquiryDialog.addEnquiryDialog(HomeActivityNew.this, mServiceTypeList, mReferenceTypeList);
        /*enquiryDialog.addEnquiryDialog(HomeActivityNew.this, mServiceTypeResultData, mReferenceTypeList);*/
        enquiryDialog.addEnquiryDialog(HomeActivityNew.this, mServiceTypeResultData);


        /*EnquiryDialogFragment enquiryDialogFragment = new EnquiryDialogFragment(mServiceTypeList);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putInt("Id", SharedPrefManager.getInstance(this).getUserId());
        enquiryDialogFragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, enquiryDialogFragment);
        transaction.commit();
        mBottomNavigationView.getMenu().findItem(R.id.navigation_admin).setChecked(true);*/
    }

    /*public void updateEnquiry(EnquiryList.ResultData resultData) {
     *//*Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        bundle.putString(FirebaseGoogleAnalytics.Param.TOOLBAR_MENU, getString(R.string.title_add_enquiry));
        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);*//*

        EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
        enquiryDialog.updateEnquiryDialog(HomeActivityNew.this, mServiceTypeResultData, resultData, this);
    }*/


    public void getCustomerList() {
        /*mGetCustomerListAsyncTask = new GetCustomerListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetCustomerListAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<CustomerList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getCustomerList(userID);
                call.enqueue(new retrofit2.Callback<CustomerList>() {
                    @Override
                    public void onResponse(Call<CustomerList> call, Response<CustomerList> response) {
                        try {
                            if (response.code() == 200) {
                                CustomerList customerList = response.body();
                                if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                                    if (customerList != null) {
                                        mCustomerList = new ArrayList<>();
                                        mCustomerList = customerList.getResultData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
            ex.getMessage();
        }
    }

    public void serviceOccurrenceTask() {
        /*mGetServiceOccurrenceAsyncTask = new GetServiceOccurrenceAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetServiceOccurrenceAsyncTask.execute();*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ServiceOccurrenceList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getServiceOccurrenceTask();
                call.enqueue(new retrofit2.Callback<ServiceOccurrenceList>() {
                    @Override
                    public void onResponse(Call<ServiceOccurrenceList> call, Response<ServiceOccurrenceList> response) {
                        try {
                            if (response.code() == 200) {
                                ServiceOccurrenceList serviceOccurrenceList = response.body();
                                FWLogger.logInfo(TAG, "Service Occurrence List Received Successfully");
                                mServiceOccurrenceResultList = new ArrayList<>();
                                mServiceOccurrenceResultList = serviceOccurrenceList.getResultData();
                                mServiceOccurrenceTypeList = new ArrayList<>();
                                for (ServiceOccurrenceList.ResultData resultData : mServiceOccurrenceResultList) {
                                    mServiceOccurrenceTypeList.add(resultData.getServiceOccuranceType());
                                }
                                reminderModeTask();

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceOccurrenceList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAMCServiceOccuranceType API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAMCServiceOccuranceType API:");
            ex.getMessage();
        }

    }

    public void reminderModeTask() {
      /*  mGetReminderModeAsyncTask = new GetReminderModeAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetReminderModeAsyncTask.execute();*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ReminderModeList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getReminderModeTask();
                call.enqueue(new retrofit2.Callback<ReminderModeList>() {
                    @Override
                    public void onResponse(Call<ReminderModeList> call, Response<ReminderModeList> response) {
                        try {
                            if (response.code() == 200) {
                                ReminderModeList reminderModeList = response.body();
                                FWLogger.logInfo(TAG, "Reminder Mode List Received Successfully");
                                mReminderModeList = new ArrayList<>();
                                mReminderModeResultList = reminderModeList.getResultData();
                                for (ReminderModeList.ResultData resultData : mReminderModeResultList) {
                                    mReminderModeList.add(resultData.getAMCSetReminderType());
                                }
                                addAMCDialog(mServiceOccurrenceTypeList, mReminderModeList);
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ReminderModeList> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "GetAMCSetReminders API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in GetAMCSetReminders API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAMCSetReminders API:");
            ex.getMessage();
        }

    }

    public void addCredit(Integer amount, int mUserId, String addDeduct_Description) {
        mAmount = amount;
       /* mAddCreditAsyncTask = new AddCreditAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddCreditAsyncTask.execute(amount, mUserId, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), addDeduct_Description);
*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddCredit> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().addCredit(amount, mUserId, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), addDeduct_Description, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddCredit>() {
                    @Override
                    public void onResponse(Call<AddCredit> call, Response<AddCredit> response) {
                        try {
                            if (response.code() == 200) {
                                AddCredit addCredit = response.body();
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
                                bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_BAL, bundle);
                                if (addCredit != null) {
                                    if (addCredit.getCode().equalsIgnoreCase("200") && addCredit.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.credit_added_successfully, HomeActivityNew.this))) {
//                    Toast.makeText(HomeActivityNew.this, getString(R.string.credit_added_successfully), Toast.LENGTH_LONG).show();
                                        Toast.makeText(HomeActivityNew.this, "Amount credited successfully", Toast.LENGTH_LONG).show();
                                        FWLogger.logInfo(TAG, "Credit added successfully.");
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, addCredit.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCredit> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddCredit API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddCredit API:");
            ex.getMessage();
        }

    }

    public void deductBalance(int amount, int mUserId, String addDeduct_Description) {
        mAmount = amount;
       /* mDeductBalanceAsyncTask = new DeductBalanceAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mDeductBalanceAsyncTask.execute(amount, mUserId, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), addDeduct_Description);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<DeductBalance> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().deductBalance(amount, mUserId, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), addDeduct_Description, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<DeductBalance>() {
                    @Override
                    public void onResponse(Call<DeductBalance> call, Response<DeductBalance> response) {
                        try {
                            if (response.code() == 200) {
                                DeductBalance deductBalance = response.body();
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
                                bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.DEDUCT_BAL, bundle);
                                if (deductBalance != null) {
                                    if (deductBalance.getCode().equalsIgnoreCase("200") && deductBalance.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.amount_deducted_successfully, HomeActivityNew.this))) {
                                        Toast.makeText(HomeActivityNew.this, getString(R.string.amount_deducted_successfully), Toast.LENGTH_LONG).show();
                                        FWLogger.logInfo(TAG, "Amount deducted successfully.");
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, deductBalance.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeductBalance> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddDeduction API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddDeduction API:");
            ex.getMessage();
        }

    }

    public void addAMCAsyncTask(AddAMC addAMC) {
       /* mAddAMCDetailsAsyncTask = new AddAMCDetailsAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddAMCDetailsAsyncTask.execute(addAMC);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddAMC> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().addAMCDetails(addAMC, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddAMC>() {
                    @Override
                    public void onResponse(Call<AddAMC> call, Response<AddAMC> response) {
                        try {
                            if (response.code() == 200) {
                                AddAMC addAMC = response.body();
                                FWLogger.logInfo(TAG, "Inside Home AMC response");
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_AMC, bundle);
                                if (addAMC != null) {
                                    if (addAMC.getCode().equalsIgnoreCase("200")) {
                                        FWLogger.logInfo(TAG, "" + addAMC.getMessage());
                                        Toast.makeText(HomeActivityNew.this, addAMC.getMessage(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        FWLogger.logInfo(TAG, "" + addAMC.getMessage());
                                        Toast.makeText(HomeActivityNew.this, addAMC.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddAMC> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddAMC API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddAMC API:");
            ex.getMessage();
        }


    }

    public void getTechnicianList() {
      /*  mExpenseTechListAsyncTask = new ExpenseTechListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mExpenseTechListAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<ExpenseTechList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getTechnicianList(userID);
                call.enqueue(new retrofit2.Callback<ExpenseTechList>() {
                    @Override
                    public void onResponse(Call<ExpenseTechList> call, Response<ExpenseTechList> response) {
                        try {
                            if (response.code() == 200) {
                                ExpenseTechList expenseTechList = response.body();
                                FWLogger.logInfo(TAG, "Tech List added Successfully");
                                mExpenseTechList = new ArrayList<>();
                                mExpenseTechList = expenseTechList.getResultData();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ExpenseTechList> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "Tech List API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in GetTechnicianList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTechnicianList API:");
            ex.getMessage();
        }
    }

    private void updatePhoto() {
        String fileName = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId() + ".jpg";
        FWLogger.logInfo(TAG, fileName);
        mUpdateProfilePicAsyncTask = new UpdateProfilePicAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUpdateProfilePicAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), mImageEncodeBaseString, fileName);
    }

    public void updateUser(UpdateUser.ResultData updateUser) {
       /* mUpdateUserAsyncTask = new UpdateUserAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUpdateUserAsyncTask.execute(updateUser);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<UpdateUser> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().updateUser(updateUser, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateUser>() {
                    @Override
                    public void onResponse(Call<UpdateUser> call, Response<UpdateUser> response) {
                        try {
                            if (response.code() == 200) {
                                UpdateUser updateUser = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.EDIT);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.CONFIRM_PROFILE, bundle);
                                FWLogger.logInfo(TAG, "Updated user Name " + updateUser.getResultData().getFirstName());
                                if (updateUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.profile_updated_successfully, HomeActivityNew.this))) {
                                    Toast.makeText(HomeActivityNew.this, getString(R.string.profile_updated_successfully), Toast.LENGTH_SHORT).show();
                                    getUSerDetails();
                                } else {
                                    Toast.makeText(HomeActivityNew.this, updateUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateUser> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateUser API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateUser API:");
            ex.getMessage();
        }

    }

    public void saveIssuedItem(IssueItem.ResultData issueItem) {
       /* mIssueItemAsyncTask = new IssueItemAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mIssueItemAsyncTask.execute(issueItem);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<IssueItem> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().saveIssuedItem(issueItem, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<IssueItem>() {
                    @Override
                    public void onResponse(Call<IssueItem> call, Response<IssueItem> response) {
                        try {
                            if (response.code() == 200) {
                                IssueItem issueItem = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, issueItem.getResultData().getQuantity());
                                bundle.putInt(FirebaseGoogleAnalytics.Param.TECH_ID, issueItem.getResultData().getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ISSUE);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
                                Toast.makeText(HomeActivityNew.this, issueItem.getMessage(), Toast.LENGTH_SHORT).show();
                                FWLogger.logInfo(TAG, "Issue Items to  = " + issueItem.getResultData().getUserId());
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<IssueItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AssignItem API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AssignItem API:");
            ex.getMessage();
        }

    }

    public void saveReturnItem(int userId, int itemId, int quantity, int itemIssuedID, int taskId, int createdBy, String notes) {
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<UnAssignItem> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().saveReturnItem(userId, itemId, quantity, itemIssuedID, taskId, createdBy, notes, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<UnAssignItem>() {
                    @Override
                    public void onResponse(Call<UnAssignItem> call, Response<UnAssignItem> response) {
                        try {
                            if (response.code() == 200) {
                                UnAssignItem returnItem1 = response.body();
                                //NOTE: Log GA event
                                Toast.makeText(HomeActivityNew.this, returnItem1.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UnAssignItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
            ex.getMessage();
        }

    }


    public void saveAddItem(AddItem.Resultdata addItem) {
       /* mAddItemAsyncTask = new AddItemAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddItemAsyncTask.execute(addItem);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddItem> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().saveAddItem(addItem, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddItem>() {
                    @Override
                    public void onResponse(Call<AddItem> call, Response<AddItem> response) {
                        try {
                            if (response.code() == 200) {
                                AddItem addItem = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, addItem.getResultdata().getQuantity());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ADD);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
                                if (addItem.getCode().equalsIgnoreCase("200") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_added_successfully, HomeActivityNew.this)))
                                    Toast.makeText(HomeActivityNew.this, getString(R.string.item_added_successfully), Toast.LENGTH_SHORT).show();
                                else if (addItem.getCode().equalsIgnoreCase("400") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_name_already_exist, HomeActivityNew.this)))
                                    Toast.makeText(HomeActivityNew.this, getString(R.string.item_name_already_exist), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(HomeActivityNew.this, addItem.getMessage(), Toast.LENGTH_SHORT).show();
                                getItemList();
                                FWLogger.logInfo(TAG, "Added Successful = " + addItem.getResultdata().getName());

                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddItem API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddItem API:");
            ex.getMessage();
        }
    }

    public void addUsedItems(AddDeductUsedItem addUsedItem) {
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddDeductUsedItem> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().addUsedItem(addUsedItem, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddDeductUsedItem>() {
                    @Override
                    public void onResponse(Call<AddDeductUsedItem> call, Response<AddDeductUsedItem> response) {
                        try {
                            if (response.code() == 200) {
                                AddDeductUsedItem addDeductUsedItem = response.body();
                                Toast.makeText(HomeActivityNew.this, addDeductUsedItem.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddDeductUsedItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddUsedItem API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddUsedItem API:");
            ex.getMessage();
        }

    }

    public void deductUsedItems(AddDeductUsedItem addUsedItem) {
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddDeductUsedItem> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().deductUsedItem(addUsedItem, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddDeductUsedItem>() {
                    @Override
                    public void onResponse(Call<AddDeductUsedItem> call, Response<AddDeductUsedItem> response) {
                        try {
                            if (response.code() == 200) {
                                AddDeductUsedItem addDeductUsedItem = response.body();
                                Toast.makeText(HomeActivityNew.this, addDeductUsedItem.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddDeductUsedItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddUsedItem API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddUsedItem API:");
            ex.getMessage();
        }

    }


    public void getUSerDetails() {
       /* mUserDetailsAsyncTask = new UserDetailsAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUserDetailsAsyncTask.execute(SharedPrefManager.getInstance(this).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<UserDetails> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getUserDetails(userID);
                call.enqueue(new retrofit2.Callback<UserDetails>() {
                    @Override
                    public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                        try {
                            if (response.code() == 200) {
                                UserDetails userDetails = response.body();
                                UserDetails.ResultData userDetailsResultData = userDetails.getResultData();
                                mUserDetails = userDetailsResultData;
                                //Update UI
                                if (userDetailsResultData != null) {
                                    try {
                                        String languageType = SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage();
                                        if (mUserDetails.getIsTeleCmiEnabled() != null || !(mUserDetails.getIsTeleCmiEnabled() == null)) {
                                            SharedPrefManager.getInstance(HomeActivityNew.this).setTeleCMIModuleFlag(mUserDetails.getIsTeleCmiEnabled());
                                        } else {
                                            SharedPrefManager.getInstance(HomeActivityNew.this).setTeleCMIModuleFlag("false");
                                        }
                                        if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_ENGLISH)) {
                                            textViewUsername.setText("Hello, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_HINDI)) {
                                            textViewUsername.setText("नमस्ते, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_PUNJABI)) {
                                            textViewUsername.setText("ਸੁਆਗਤ ਹੈ, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_BENGALI)) {
                                            textViewUsername.setText("হ্যালো, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_MARATHI)) {
                                            textViewUsername.setText("नमस्कार, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_KANNADA)) {
                                            textViewUsername.setText("ನಮಸ್ಕಾರ, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_TAMIL)) {
                                            textViewUsername.setText("வணக்கம், " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_TELUGU)) {
                                            textViewUsername.setText("హలో, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_MALAYALAM)) {
                                            textViewUsername.setText("മലയാളം, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_ARABIC)) {
                                            textViewUsername.setText("مرحبا, " + userDetailsResultData.getFirstName());
                                            textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                                        }
                                        // 1= INIDA && OTHER THAN 1 MEANS NRI
                                        if (SharedPrefManager.getInstance(HomeActivityNew.this).getCountryDetailsID() == 1) {
                                            if (userDetailsResultData.getContactNo() != null) {
                                                textViewMobile.setText("+91 " + userDetailsResultData.getContactNo());
                                                SharedPrefManager.getInstance(HomeActivityNew.this).setMOBILE_OR_EMAIL(userDetailsResultData.getContactNo());
                                                SharedPrefManager.getInstance(HomeActivityNew.this).setUserFirstName(userDetailsResultData.getFirstName());
                                            }
                                        } else {
                                            // NRI
                                            textViewMobile.setText(userDetailsResultData.getEmail());
                                            SharedPrefManager.getInstance(HomeActivityNew.this).setMOBILE_OR_EMAIL(userDetailsResultData.getEmail());
                                            SharedPrefManager.getInstance(HomeActivityNew.this).setUserFirstName(userDetailsResultData.getFirstName());
                                        }
                                    } catch (Exception e) {
                                        e.getMessage();
                                    }
                                    Picasso.get().load(userDetailsResultData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mProfileCircleImageView, new Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onError(Exception e) {
                                            mProfileCircleImageView.setImageResource(R.drawable.profile_icon);
                                        }
                                    });
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetails> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "GetUsersByUserId API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in GetUsersByUserId API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetUsersByUserId API:");
            ex.getMessage();
        }
    }

    private void checkAttendance() {
        /*mAttendanceCheckAsyncTask = new AttendanceCheckAsyncTask((HomeActivityNew.this), BaseAsyncTask.Priority.LOW, this);
        mAttendanceCheckAsyncTask.execute(SharedPrefManager.getInstance(this).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<AttendanceCheck> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getAttendanceCheck(userID);
                call.enqueue(new retrofit2.Callback<AttendanceCheck>() {
                    @Override
                    public void onResponse(Call<AttendanceCheck> call, Response<AttendanceCheck> response) {
                        try {
                            if (response.code() == 200) {
                                AttendanceCheck attendanceCheck = response.body();
                                FWLogger.logInfo(TAG, "AttendanceCheck");
                                if (attendanceCheck != null) {
                                    if (attendanceCheck.getCode().equalsIgnoreCase("200") && attendanceCheck.getMessage().equalsIgnoreCase("Attendance already added.")) {
                                        FWLogger.logInfo(TAG, "Attendance already added.");
                                        // Call API to check disclaimer is accepted or not
                                        checkDisclaimer();
                                    } else {
                                        FWLogger.logInfo(TAG, "Open Attendance Mark Dialog ");
                                        Toast.makeText(HomeActivityNew.this, R.string.please_check_in_first, Toast.LENGTH_SHORT).show();
                                        UserDialog userDialog = UserDialog.getInstance();
                                        userDialog.showAttendanceMarkDialog(HomeActivityNew.this);
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "AttendanceCheck API failure");
                                    verifyLocationSettings();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AttendanceCheck> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetTodayAttandanceIsExist API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTodayAttandanceIsExist API:");
            ex.getMessage();
        }

    }

    private void checkDisclaimer() {
       /* mUserDisclaimerAsyncTask = new GetUserDisclaimerAsyncTask((HomeActivityNew.this), BaseAsyncTask.Priority.LOW, this);
        mUserDisclaimerAsyncTask.execute(SharedPrefManager.getInstance(this).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<GetUserDisclaimer> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getUserDisclaimer(userID);
                call.enqueue(new retrofit2.Callback<GetUserDisclaimer>() {
                    @Override
                    public void onResponse(Call<GetUserDisclaimer> call, Response<GetUserDisclaimer> response) {
                        try {
                            if (response.code() == 200) {
                                GetUserDisclaimer getUserDisclaimer = response.body();
                                FWLogger.logInfo(TAG, "Disclaimer check");
                                if (getUserDisclaimer != null) {
                                    if (getUserDisclaimer.getCode().equalsIgnoreCase("200") && getUserDisclaimer.getMessage().equalsIgnoreCase("successfully.") && getUserDisclaimer.getResultData().getIsAccepted()) {
                                        // Disclaimer is accepted then load fragments according to user role
                                        FWLogger.logInfo(TAG, "Disclaimer is already accepted");
                                    } else {
                                        // Disclaimer is not accepted then show dialog to accept disclaimer
                                        FWLogger.logInfo(TAG, "Disclaimer is not accepted");
                                        UserDialog userDialog = UserDialog.getInstance();
                                        userDialog.disclaimerDialog(HomeActivityNew.this, getUserDisclaimer.getResultData().getDisclaimerHtml(), getUserDisclaimer.getResultData().getDCN());
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "GetUserDisclaimer API failure");
                                    // Give specific call : Call API for check disclaimer is accepted or not
                                    checkDisclaimer();
                                }


                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetUserDisclaimer> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetUserDisclaimer API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetUserDisclaimer API:");
            ex.getMessage();
        }
    }

    public void acceptDisclaimer(AcceptDisclaimer.ResultData acceptResultData) {
        /*mAcceptDisclaimerAsyncTask = new AcceptDisclaimerAsyncTask((HomeActivityNew.this), BaseAsyncTask.Priority.LOW, this);
        mAcceptDisclaimerAsyncTask.execute(acceptResultData);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AcceptDisclaimer> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().acceptDisclaimer(acceptResultData, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AcceptDisclaimer>() {
                    @Override
                    public void onResponse(Call<AcceptDisclaimer> call, Response<AcceptDisclaimer> response) {
                        try {
                            if (response.code() == 200) {
                                AcceptDisclaimer acceptDisclaimer = response.body();
                                FWLogger.logInfo(TAG, "Accept Disclaimer");
                                if (acceptDisclaimer != null) {
                                    if (acceptDisclaimer.getCode().equalsIgnoreCase("200") && acceptDisclaimer.getMessage().equalsIgnoreCase("successfully.") && acceptDisclaimer.getResultData().getIsAccepted()) {
                                        // Disclaimer is accepted then load fragments according to user role
                                        FWLogger.logInfo(TAG, "Accept Disclaimer successfully");
                                        //selectDashboardFragment();
                                    } else {
                                        // Give specific call : Call API for check disclaimer is accepted or not
                                        FWLogger.logInfo(TAG, "Unable to Accept Disclaimer");
                                        checkDisclaimer();
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "AcceptDisclaimer API failure");
                                    // Give specific call : Call API for check disclaimer is accepted or not
                                    checkDisclaimer();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AcceptDisclaimer> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AcceptUserDisclaimer API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AcceptUserDisclaimer API:");
            ex.getMessage();
        }

    }

    public void markAttendance(AddAttendance.ResultData addAttendance) {
       /* mAddAttendanceAsyncTask = new AddAttendanceAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddAttendanceAsyncTask.execute(addAttendance);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddAttendance> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().postAttendance(addAttendance, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddAttendance>() {
                    @Override
                    public void onResponse(Call<AddAttendance> call, Response<AddAttendance> response) {
                        try {
                            if (response.code() == 200) {
                                AddAttendance addAttendance = response.body();
                                if (addAttendance != null) {
                                    if (addAttendance.getCode().equalsIgnoreCase("200") && addAttendance.getMessage().equalsIgnoreCase("Attendance added successfully.")) {
                                        Toast.makeText(HomeActivityNew.this, R.string.check_in_successfully, Toast.LENGTH_SHORT).show();
                                        FWLogger.logInfo(TAG, "date" + addAttendance.getResultData().getAttendanceDate());
                                        // Call API to check disclaimer is accepted or not
                                        //isAttendanceMarked = true;
                                        setupAlarm(900000); // 15 MIN into MILLISEC
                                        checkDisclaimer();
                                    } else if (addAttendance.getCode().equalsIgnoreCase("200") && addAttendance.getMessage().equalsIgnoreCase("Attendance already added.")) {
//                    Toast.makeText(this, R.string.check_in_successfully, Toast.LENGTH_SHORT).show();
                                        FWLogger.logInfo(TAG, "date" + addAttendance.getResultData().getAttendanceDate());
                                        // Call API to check disclaimer is accepted or not
                                        //isAttendanceMarked = true;
                                        checkDisclaimer();
                                    } else {
                                        FWLogger.logInfo(TAG, "Attendance not added ");
                                        //Go back to first step of flow and verify everything again
                                        Toast.makeText(HomeActivityNew.this, addAttendance.getMessage(), Toast.LENGTH_SHORT).show();
                                        //isAttendanceMarked = false;
                                        verifyLocationSettings();
                                    }
                                } else {
                                    // On API failure
                                    FWLogger.logInfo(TAG, "AddAttendance API failure");
                                    //isAttendanceMarked = false;
                                    verifyLocationSettings();
                                }


                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddAttendance> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddAttendance API:");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddAttendance API:");
            ex.getMessage();
        }

    }

    public void getItemList() {
        /*mItemsListAsyncTask = new ItemsListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mItemsListAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<ItemsList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getItemList(userID);
                call.enqueue(new retrofit2.Callback<ItemsList>() {
                    @Override
                    public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                        try {
                            if (response.code() == 200) {
                                ItemsList itemsList = response.body();
                                mItemsLists = new ArrayList<>();
                                mItemsLists = itemsList.getResultData();
                                mItemsNameList = new ArrayList<>();

                                for (ItemsList.ResultData resultData : mItemsLists) {
                                    mItemsNameList.add(resultData.getName());
                                }
                                if (itemListFromWhere.equalsIgnoreCase("dialog")) {
                                    itemListFromWhere = "";
                                    assignItem();
                                } else if (itemListFromWhere.equalsIgnoreCase("cDialog")) {
                                    itemListFromWhere = "";
                                    addTask();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ItemsList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
            ex.getMessage();
        }
    }

    private void getItemUnitType() {
       /* mGetItemUnitListAsyncTask = new GetItemUnitListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetItemUnitListAsyncTask.execute();*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ItemUnitType> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getItemUnitType();
                call.enqueue(new retrofit2.Callback<ItemUnitType>() {
                    @Override
                    public void onResponse(Call<ItemUnitType> call, Response<ItemUnitType> response) {
                        try {
                            if (response.code() == 200) {
                                ItemUnitType itemUnitType = response.body();
                                FWLogger.logInfo(TAG, "Get ITEM UNIT List Successful");
                                if (itemUnitType != null) {
                                    if (itemUnitType.getCode().equalsIgnoreCase("200") && itemUnitType.getMessage().equalsIgnoreCase("successfully.")) {
                                        mitemUnitTypeList = new ArrayList<>();
                                        mitemUnitTypeList = itemUnitType.getResultData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ItemUnitType> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "ItemUnitType API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in ItemUnitType API :");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in ItemUnitType API :");
            ex.getMessage();
        }

    }

    public void getUserList() {
       /* mUsersListAsyncTask = new UsersListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUsersListAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                CommonFunction.showProgressDialog(HomeActivityNew.this);
                int userID = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                Call<UsersList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getAllUsersList(userID);
                call.enqueue(new retrofit2.Callback<UsersList>() {
                    @Override
                    public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(HomeActivityNew.this);
                                UsersList usersList = response.body();
                                FWLogger.logInfo(TAG, "Received Users Name Successfully");
                                mUsersLists = new ArrayList<>();
                                mUsersLists = usersList.getResultData();
                                mUsersNameList = new ArrayList<>();

                                for (UsersList.ResultData resultData : mUsersLists) {
                                    mUsersNameList.add(resultData.getFirstName());
                                }
                                if (fromWhere.equalsIgnoreCase("FIRST_ADD_TECH")) {
                                    fromWhere = "";
                                    SharedPrefManager.getInstance(getApplicationContext()).setisFirstTimeAddMoreTechPopup(true);
                                    loadFragment(new TechnicianListFragment(), R.id.navigation_home);
                                }
                                getServiceType();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UsersList> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(HomeActivityNew.this);
                        FWLogger.logInfo(TAG, "Exception in AllUsersList API:");

                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            FWLogger.logInfo(TAG, "Exception in AllUsersList API:");
            ex.getMessage();
        }
    }

/*
    public void getServiceType() {
        */
/*mGetServiceTypeAsyncTask = new GetServiceTypeAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetServiceTypeAsyncTask.execute();*//*

        try {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ServiceType> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getServiceType();
                call.enqueue(new retrofit2.Callback<ServiceType>() {
                    @Override
                    public void onResponse(Call<ServiceType> call, Response<ServiceType> response) {
                        try {
                            if (response.code() == 200) {
                                ServiceType serviceType = response.body();
                                FWLogger.logInfo(TAG, "Received service type Successfully");
                                mServiceTypeResultData = new ArrayList<>();
                                mServiceTypeResultData = serviceType.getResultData();
                                mServiceTypeList = new ArrayList<>();
                                for (ServiceType.ResultData resultData : mServiceTypeResultData) {
                                    mServiceTypeList.add(resultData.getServiceName());
                                }
                                getReferenceType();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceType> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "Service Type API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in Service Type API :");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Service Type API :");
            ex.getMessage();
        }
    }
*/

    public void getServiceType() {
        try {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<EnquiryServiceTypeDTO> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getEnquiryServiceList(userID, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<EnquiryServiceTypeDTO>() {
                    @Override
                    public void onResponse(Call<EnquiryServiceTypeDTO> call, Response<EnquiryServiceTypeDTO> response) {
                        try {
                            if (response.code() == 200) {
                                EnquiryServiceTypeDTO serviceType = response.body();
                                FWLogger.logInfo(TAG, "Received services type Successfully");
                                mServiceTypeResultData = new ArrayList<>();
                                mServiceTypeResultData = serviceType.getResultData();
                                mServiceTypeList = new ArrayList<>();
                                for (EnquiryServiceTypeDTO.ResultData resultData : mServiceTypeResultData) {
                                    mServiceTypeList.add(resultData.getServiceName());
                                }
                                getReferenceType();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EnquiryServiceTypeDTO> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "Service Type API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in Service Type API :");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Service Type API :");
            ex.getMessage();
        }
    }

    //
    public void getQuoteBindList() {
        try {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<QuoteBindListDTO> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getQuoteBindList(userID, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<QuoteBindListDTO>() {
                    @Override
                    public void onResponse(Call<QuoteBindListDTO> call, Response<QuoteBindListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                QuoteBindListDTO quoteBindListDTO = response.body();
                                mQuoteBindListResultData = new ArrayList<>();
                                mQuoteBindListResultData = quoteBindListDTO.getResultData();
                                mQuoteBindList = new ArrayList<>();
                                for (QuoteBindListDTO.ResultData resultData : mQuoteBindListResultData) {
                                    mQuoteBindList.add(resultData.getQuoteName());
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<QuoteBindListDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in Service Type API :");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Service Type API :");
            ex.getMessage();
        }
    }


    public void getReferenceType() {
       /* mGetReferenceTypeAsyncTask = new GetReferenceTypeAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetReferenceTypeAsyncTask.execute();*/
        try {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ReferenceType> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getReferenceType();
                call.enqueue(new retrofit2.Callback<ReferenceType>() {
                    @Override
                    public void onResponse(Call<ReferenceType> call, Response<ReferenceType> response) {
                        try {
                            if (response.code() == 200) {
                                ReferenceType referenceType = response.body();
                                mReferenceTypeResultData = referenceType.getResultData();
                                mReferenceTypeList = new ArrayList<>();
                                for (ReferenceType.ResultData resultData : mReferenceTypeResultData) {
                                    mReferenceTypeList.add(resultData.getReferenceName());
                                }
                            }
                            getServiceTypeForProfileFragment();
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ReferenceType> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in Reference Type API :");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Reference Type API :");
            ex.getMessage();
        }
    }

    public void getServiceTypeForProfileFragment() {
        try {
            CommonFunction.hideProgressDialog(HomeActivityNew.this);
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ServiceType> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getServiceType();
                call.enqueue(new retrofit2.Callback<ServiceType>() {
                    @Override
                    public void onResponse(Call<ServiceType> call, Response<ServiceType> response) {
                        try {
                            if (response.code() == 200) {
                                ServiceType serviceType = response.body();
                                FWLogger.logInfo(TAG, "Received service type Successfully");
                                mServiceTypeResultDataProfile = new ArrayList<>();
                                mServiceTypeResultDataProfile = serviceType.getResultData();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceType> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "Service Type API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in Service Type API :");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in Service Type API :");
            ex.getMessage();
        }
    }


    public void getLeadStatusList() {
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<LeadStatusListDTO> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getLeadStatusList();
                call.enqueue(new retrofit2.Callback<LeadStatusListDTO>() {
                    @Override
                    public void onResponse(Call<LeadStatusListDTO> call, Response<LeadStatusListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                LeadStatusListDTO leadStatusListDTO = response.body();
                                FWLogger.logInfo(TAG, "Received Lead Status Successfully");
                                mLeadStatusList = new ArrayList<>();
                                mLeadStatusList = leadStatusListDTO.getResultData();

                                mSpineerLeadStatusList = new ArrayList<>();

                                for (LeadStatusListDTO.ResultData resultData : mLeadStatusList) {
                                    mSpineerLeadStatusList.add(resultData.getName());
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<LeadStatusListDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetLeadStatusList Type API :");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetLeadStatusList API :");
            ex.getMessage();
        }
    }


    public void saveAddTask(AddTask.ResultData addTask) {
       /* mAddTaskAsyncTask = new AddTaskAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddTaskAsyncTask.execute(addTask);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddTask> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().saveAddTask(addTask, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddTask>() {
                    @Override
                    public void onResponse(Call<AddTask> call, Response<AddTask> response) {
                        try {
                            if (response.code() == 200) {
                                AddTask addTask = response.body();
                                Toast.makeText(HomeActivityNew.this, addTask.getMessage(), Toast.LENGTH_SHORT).show();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
                                bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_ADDED, addTask.getMessage().equalsIgnoreCase("Task added successfully."));
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_TASK, bundle);
                                mTaskCreatedBy = "";
                                FWLogger.logInfo(TAG, "Added Task = " + addTask.getResultData());
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddTask> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddTaskWithAudio API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddTaskWithAudio API:");
            ex.getMessage();
        }


    }

    public void reAssignAsyncTask(ReassignTask.ResultData reassignTask) {
        /*mReassignTaskAsyncTask = new ReassignTaskAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mReassignTaskAsyncTask.execute(reassignTask);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<ReassignTask> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().reAssignTask(reassignTask, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<ReassignTask>() {
                    @Override
                    public void onResponse(Call<ReassignTask> call, Response<ReassignTask> response) {
                        try {
                            if (response.code() == 200) {
                                ReassignTask reassignTask = response.body();
                                FWLogger.logInfo(TAG, "Received reference type Successfully");
                                FWLogger.logInfo(TAG, "MSG : " + reassignTask.getMessage());
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
                                bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_REASSIGNED, reassignTask.getMessage().equalsIgnoreCase("Task Reassigned Successfully."));
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.REASSIGN_TASK, bundle);
                                if (reassignTask != null) {
                                    if (reassignTask.getCode().equalsIgnoreCase("200")) {
                                        Toast.makeText(HomeActivityNew.this, reassignTask.getMessage(), Toast.LENGTH_SHORT).show();
                                        //Update noti fragment if open or visible
                                        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
                                        if (currentFragment != null && currentFragment instanceof NotificationFragment) {
                                            //DO STUFF
                                            FWLogger.logInfo(TAG, "notificationFragment is found in foreground");
                                            if (((NotificationFragment) currentFragment).mNewNotificationListener != null) {
                                                FWLogger.logInfo(TAG, "mNewNotificationListener is not null");
                                                ((NotificationFragment) currentFragment).mNewNotificationListener.onNewNotification();
                                            }
                                        } else if (currentFragment != null && currentFragment instanceof TaskDetailsFragmentNew) {
                                            //First pop back stack immediately to get notification fragment to come out to foreground
                                            getSupportFragmentManager().popBackStackImmediate();
                                            currentFragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
                                            //DO STUFF
                                            FWLogger.logInfo(TAG, "notificationFragment is found in foreground");
                                            if (currentFragment instanceof TaskDetailsFragmentNew || currentFragment instanceof MainTaskFragmentNew) {

                                            } else if (((NotificationFragment) currentFragment).mNewNotificationListener != null) {
                                                FWLogger.logInfo(TAG, "mNewNotificationListener is not null");
                                                ((NotificationFragment) currentFragment).mNewNotificationListener.onNewNotification();
                                            }
                                        }
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, reassignTask.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ReassignTask> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in ReAssignTaskByTaskID API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in ReAssignTaskByTaskID API:");
            ex.getMessage();
        }

    }

    public void saveAddEnquiry(AddEnquiry.ResultData addEnquiry) {
       /* mAddEnquiryAsyncTask = new AddEnquiryAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mAddEnquiryAsyncTask.execute(addEnquiry);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<AddEnquiry> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().saveAddEnquiry(addEnquiry, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<AddEnquiry>() {
                    @Override
                    public void onResponse(Call<AddEnquiry> call, Response<AddEnquiry> response) {
                        try {
                            if (response.code() == 200) {
                                AddEnquiry addEnquiry = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, addEnquiry.getResultData().getMobileNumber());
                                FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_ENQ, bundle);

                                Toast.makeText(HomeActivityNew.this, addEnquiry.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddEnquiry> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddCustomerInquiry API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddCustomerInquiry API:");
            ex.getMessage();
        }
    }

    public void updateTask(UpdateTask.ResultData resultData) {
      /*  mUpdateTaskAsyncTask = new UpdateTaskAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUpdateTaskAsyncTask.execute(resultData);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<UpdateTask> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().updateTask(resultData, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateTask>() {
                    @Override
                    public void onResponse(Call<UpdateTask> call, Response<UpdateTask> response) {
                        try {
                            if (response.code() == 200) {
                                UpdateTask updateTask = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
                                bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_UPDATED, updateTask.getMessage().equalsIgnoreCase(getString(R.string.task_updated_successfully)));
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.UPDATE_TASK, bundle);
                                mTaskCreatedBy = "";
                                if (updateTask != null) {
                                    if (updateTask.getCode().equalsIgnoreCase("200") && updateTask.getMessage().equalsIgnoreCase(getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, HomeActivityNew.this))) {
                                        Toast.makeText(HomeActivityNew.this, getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                                        navigateToTab(R.id.navigation_task);
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, updateTask.getMessage(), Toast.LENGTH_LONG).show();
                    /*Toast toast = Toast.makeText(this,updateTask.getMessage(), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();*/
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "Updated Task = " + updateTask.getResultData());
                                    Toast.makeText(HomeActivityNew.this, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                }


                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateTask> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
            ex.getMessage();
        }

    }

    public void openReAssignCompletedTaskDialog(TasksList.ResultData mResultData) {
        getItemList();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                taskDialog.reAssignCompletedTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, mResultData, mUsersLists, mCustomerList);
            }
        }, 500);
    }

    public void openReAssignOnHoldTaskDialog(TasksList.ResultData mResultData) {
        getItemList();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                taskDialog.reAssignOnholdTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, mResultData, mUsersLists, mCustomerList);
            }
        }, 500);
    }

    public void openUpdateTaskDialog(TasksList.ResultData mResultData) {
        getItemList();
        AudioPermission();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                taskDialog.updateTaskDialog(HomeActivityNew.this, mItemsNameList, mUsersNameList, mResultData, mCustomerList);  //mUsersLists
            }
        }, 500);
    }

    public void checkOut(double lat, double lang, String place) {
        /*mCheckOutAsyncTask = new CheckOutAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mCheckOutAsyncTask.execute(lat, lang, place);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<CheckOut> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().checkOut(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), lat, lang, place, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<CheckOut>() {
                    @Override
                    public void onResponse(Call<CheckOut> call, Response<CheckOut> response) {
                        try {
                            if (response.code() == 200) {
                                CheckOut checkOut = response.body();
                                if (checkOut != null) {
                                    if (checkOut.getCode().equalsIgnoreCase("200") && checkOut.getMessage().equalsIgnoreCase("Check out successfully.")) {
                                        Toast.makeText(HomeActivityNew.this, checkOut.getMessage(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, checkOut.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                    //logoutAndNavigateToLogin(); // Commented By Manish : As per SAhil Requirement : 06-04-2022
                                    drawerLayout.closeDrawer(Gravity.LEFT); // ADDED BY MANISH 06-04-2022
                                } else {
                                    FWLogger.logInfo(TAG, "Check Out = " + checkOut.getMessage());
                                    Toast.makeText(HomeActivityNew.this, R.string.failed_to_checkout_try_again, Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckOut> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in CheckOutAttendance API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in CheckOutAttendance API:");
            ex.getMessage();
        }

    }

    public void updatePrefLanguage() {
       /* mUpdatePrefLanguageAsyncTask = new UpdatePrefLanguageAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUpdatePrefLanguageAsyncTask.execute();*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                int userId = SharedPrefManager.getInstance(HomeActivityNew.this).getUserId();
                String preferredLanguage = SharedPrefManager.getInstance(HomeActivityNew.this).getPreferredLanguage();
                Call<LanguageList> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getPreferredLanguage(userId, preferredLanguage);
                call.enqueue(new retrofit2.Callback<LanguageList>() {
                    @Override
                    public void onResponse(Call<LanguageList> call, Response<LanguageList> response) {
                        try {
                            if (response.code() == 200) {
                                LanguageList languageList = response.body();
                                FWLogger.logInfo(TAG, "languageList = " + languageList.getMessage());
                                if (languageList.getCode().equalsIgnoreCase("200") && languageList.getMessage().equalsIgnoreCase("User Preferred language updated successfully"))
                                    Toast.makeText(HomeActivityNew.this, languageList.getMessage(), Toast.LENGTH_LONG).show();
                                UserDialog.restartActivity(HomeActivityNew.this);
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<LanguageList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UserPreferredLanguage API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UserPreferredLanguage API:");
            ex.getMessage();
        }
    }

    public void sendEmailTask(int taskId, int ownerId, String email) {
        mSendEmailAsyncTask = new SendEmailAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mSendEmailAsyncTask.execute(taskId, ownerId, email);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(UserDetails.class.getSimpleName())) {
            Gson gson = new Gson();
            UserDetails userDetails = gson.fromJson(urlConnectionResponse.resultData, UserDetails.class);
            UserDetails.ResultData userDetailsResultData = userDetails.getResultData();
            mUserDetails = userDetailsResultData;
            //Update UI
            if (userDetailsResultData != null) {
//                if(mUserDetails.isRegistered())getItemList();
                try {
                    String languageType = SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage();
                    if (mUserDetails.getIsTeleCmiEnabled() != null || !(mUserDetails.getIsTeleCmiEnabled() == null)) {
                        SharedPrefManager.getInstance(HomeActivityNew.this).setTeleCMIModuleFlag(mUserDetails.getIsTeleCmiEnabled());
                    } else {
                        SharedPrefManager.getInstance(HomeActivityNew.this).setTeleCMIModuleFlag("false");
                    }
                    //SharedPrefManager.getInstance(HomeActivityNew.this).setTeleCMIModuleFlag(mUserDetails.getIsTeleCmiEnabled());
                   /* if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_ENGLISH)) {
                        textViewUsername.setText("Hello, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_HINDI)) {
                        textViewUsername.setText("नमस्ते, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    }*/
                    if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_ENGLISH)) {
                        textViewUsername.setText("Hello, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_HINDI)) {
                        textViewUsername.setText("नमस्ते, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_PUNJABI)) {
                        textViewUsername.setText("ਸੁਆਗਤ ਹੈ, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_BENGALI)) {
                        textViewUsername.setText("হ্যালো, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_MARATHI)) {
                        textViewUsername.setText("नमस्कार, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_KANNADA)) {
                        textViewUsername.setText("ನಮಸ್ಕಾರ, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_TAMIL)) {
                        textViewUsername.setText("வணக்கம், " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_TELUGU)) {
                        textViewUsername.setText("హలో, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_MALAYALAM)) {
                        textViewUsername.setText("മലയാളം, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    } else if (languageType.equalsIgnoreCase(Constant.PreferredLanguage.LOCALE_ARABIC)) {
                        textViewUsername.setText("مرحبا, " + userDetailsResultData.getFirstName());
                        textViewDrawerName.setText(userDetailsResultData.getFirstName() + " " + userDetailsResultData.getLastName());
                    }
                    // 1= INIDA && OTHER THAN 1 MEANS NRI
                    if (SharedPrefManager.getInstance(this).getCountryDetailsID() == 1) {
                        if (userDetailsResultData.getContactNo() != null) {
                            textViewMobile.setText("+91 " + userDetailsResultData.getContactNo());
                            SharedPrefManager.getInstance(HomeActivityNew.this).setMOBILE_OR_EMAIL(userDetailsResultData.getContactNo());
                            SharedPrefManager.getInstance(HomeActivityNew.this).setUserFirstName(userDetailsResultData.getFirstName());
                        }
                    } else {
                        // NRI
                        textViewMobile.setText(userDetailsResultData.getEmail());
                        SharedPrefManager.getInstance(HomeActivityNew.this).setMOBILE_OR_EMAIL(userDetailsResultData.getEmail());
                        SharedPrefManager.getInstance(HomeActivityNew.this).setUserFirstName(userDetailsResultData.getFirstName());
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
//                if(isProfileImgClicked) {
                Picasso.get().load(userDetailsResultData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mProfileCircleImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mProfileCircleImageView.setImageResource(R.drawable.profile_icon);
                    }
                });
//                }
            }
        } else if (classType.equalsIgnoreCase(CustomerList.class.getSimpleName())) {
            Gson gson = new Gson();
            CustomerList customerList = gson.fromJson(urlConnectionResponse.resultData, CustomerList.class);
            if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                if (customerList != null) {
                    mCustomerList = new ArrayList<>();
                    mCustomerList = customerList.getResultData();
                }
            }
        } else if (classType.equalsIgnoreCase(AddTech.class.getSimpleName())) {
            Gson gson = new Gson();
            AddTech addTech = gson.fromJson(urlConnectionResponse.resultData, AddTech.class);
            Toast.makeText(this, addTech.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (classType.equalsIgnoreCase(ServiceOccurrenceList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Service Occurrence List Received Successfully");
            Gson gson = new Gson();
            mServiceOccurrenceResultList = new ArrayList<>();
            ServiceOccurrenceList serviceList = gson.fromJson(urlConnectionResponse.resultData, ServiceOccurrenceList.class);
            mServiceOccurrenceResultList = serviceList.getResultData();
            mServiceOccurrenceTypeList = new ArrayList<>();
            for (ServiceOccurrenceList.ResultData resultData : mServiceOccurrenceResultList) {
                mServiceOccurrenceTypeList.add(resultData.getServiceOccuranceType());
            }
            reminderModeTask();
        } else if (classType.equalsIgnoreCase(ReminderModeList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Reminder Mode List Received Successfully");
            Gson gson = new Gson();
            mReminderModeList = new ArrayList<>();
            ReminderModeList reminderModeLists = gson.fromJson(urlConnectionResponse.resultData, ReminderModeList.class);
            mReminderModeResultList = reminderModeLists.getResultData();
            for (ReminderModeList.ResultData resultData : mReminderModeResultList) {
                mReminderModeList.add(resultData.getAMCSetReminderType());
            }
            addAMCDialog(mServiceOccurrenceTypeList, mReminderModeList);
        } else if (classType.equalsIgnoreCase(AddAMC.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Inside Home AMC response");
            Gson gson = new Gson();
            AddAMC addAMC = gson.fromJson(urlConnectionResponse.resultData, AddAMC.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_AMC, bundle);
            if (addAMC != null) {
                if (addAMC.getCode().equalsIgnoreCase("200")) {
                    FWLogger.logInfo(TAG, "" + addAMC.getMessage());
                    Toast.makeText(HomeActivityNew.this, addAMC.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    FWLogger.logInfo(TAG, "" + addAMC.getMessage());
                    Toast.makeText(HomeActivityNew.this, addAMC.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            /*try {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(AMCListFragment.this).attach(AMCListFragment.this).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        } else if (classType.equalsIgnoreCase(UsersList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received Users Name Successfully");
            Gson gson = new Gson();
            mUsersLists = new ArrayList<>();
            UsersList usersList = gson.fromJson(urlConnectionResponse.resultData, UsersList.class);
            mUsersLists = usersList.getResultData();
            mUsersNameList = new ArrayList<>();
            /*for (int i = 0; i < mUsersLists.size(); i++) {
                mUsersNameList.add(mUsersLists.get(i).getFirstName());
            }*/
            for (UsersList.ResultData resultData : mUsersLists) {
                mUsersNameList.add(resultData.getFirstName());
            }
            if (fromWhere.equalsIgnoreCase("FIRST_ADD_TECH")) {
                fromWhere = "";
                SharedPrefManager.getInstance(getApplicationContext()).setisFirstTimeAddMoreTechPopup(true);
                loadFragment(new TechnicianListFragment(), R.id.navigation_home);
            }
            getServiceType();
            // FWLogger.logInfo(TAG, usersLists.toString());
        } else if (classType.equalsIgnoreCase(ItemsList.class.getSimpleName())) {
            Gson gson = new Gson();
            mItemsLists = new ArrayList<>();
            ItemsList itemsLists = gson.fromJson(urlConnectionResponse.resultData, ItemsList.class);
            mItemsLists = itemsLists.getResultData();
            mItemsNameList = new ArrayList<>();
            /*for (int i = 0; i < mItemsLists.size(); i++) {
                mItemsNameList.add(mItemsLists.get(i).getName());
            }*/
            for (ItemsList.ResultData resultData : mItemsLists) {
                mItemsNameList.add(resultData.getName());
            }
            if (itemListFromWhere.equalsIgnoreCase("dialog")) {
                itemListFromWhere = "";
                assignItem();
            } else if (itemListFromWhere.equalsIgnoreCase("cDialog")) {
                itemListFromWhere = "";
                addTask();
            }
        } else if (classType.equalsIgnoreCase(ExpenseTechList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "List added Successfully");
            Gson gson = new Gson();
            mExpenseTechList = new ArrayList<>();
            ExpenseTechList expenseTechList = gson.fromJson(urlConnectionResponse.resultData, ExpenseTechList.class);
            mExpenseTechList = expenseTechList.getResultData();
        } else if (classType.equalsIgnoreCase(AddTask.class.getSimpleName())) {
            Gson gson = new Gson();
            AddTask addTask = gson.fromJson(urlConnectionResponse.resultData, AddTask.class);
            Toast.makeText(this, addTask.getMessage(), Toast.LENGTH_SHORT).show();
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
            bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_ADDED, addTask.getMessage().equalsIgnoreCase("Task added successfully."));
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_TASK, bundle);
            mTaskCreatedBy = "";
            FWLogger.logInfo(TAG, "Added Task = " + addTask.getResultData());
        } else if (classType.equalsIgnoreCase(UpdateTask.class.getSimpleName())) {
            Gson gson = new Gson();
            UpdateTask updateTask = gson.fromJson(urlConnectionResponse.resultData, UpdateTask.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
            bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_UPDATED, updateTask.getMessage().equalsIgnoreCase(getString(R.string.task_updated_successfully)));
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.UPDATE_TASK, bundle);
            mTaskCreatedBy = "";
            if (updateTask != null) {
                if (updateTask.getCode().equalsIgnoreCase("200") && updateTask.getMessage().equalsIgnoreCase(getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, HomeActivityNew.this))) {
                    Toast.makeText(this, getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                    navigateToTab(R.id.navigation_task);
                } else {
                    Toast.makeText(this, updateTask.getMessage(), Toast.LENGTH_LONG).show();
                    /*Toast toast = Toast.makeText(this,updateTask.getMessage(), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();*/
                }
            } else {
                FWLogger.logInfo(TAG, "Updated Task = " + updateTask.getResultData());
                Toast.makeText(this, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
            }
        } else if (classType.equalsIgnoreCase(AddAttendance.class.getSimpleName())) {
            Gson gson = new Gson();
            AddAttendance addAttendance = gson.fromJson(urlConnectionResponse.resultData, AddAttendance.class);
            if (addAttendance != null) {
                if (addAttendance.getCode().equalsIgnoreCase("200") && addAttendance.getMessage().equalsIgnoreCase("Attendance added successfully.")) {
                    Toast.makeText(this, R.string.check_in_successfully, Toast.LENGTH_SHORT).show();
                    FWLogger.logInfo(TAG, "date" + addAttendance.getResultData().getAttendanceDate());
                    // Call API to check disclaimer is accepted or not
                    //isAttendanceMarked = true;
                    setupAlarm(900000); // 15 MIN into MILLISEC
                    checkDisclaimer();
                } else if (addAttendance.getCode().equalsIgnoreCase("200") && addAttendance.getMessage().equalsIgnoreCase("Attendance already added.")) {
//                    Toast.makeText(this, R.string.check_in_successfully, Toast.LENGTH_SHORT).show();
                    FWLogger.logInfo(TAG, "date" + addAttendance.getResultData().getAttendanceDate());
                    // Call API to check disclaimer is accepted or not
                    //isAttendanceMarked = true;
                    checkDisclaimer();
                } else {
                    FWLogger.logInfo(TAG, "Attendance not added ");
                    //Go back to first step of flow and verify everything again
                    Toast.makeText(this, addAttendance.getMessage(), Toast.LENGTH_SHORT).show();
                    //isAttendanceMarked = false;
                    verifyLocationSettings();
                }
            } else {
                // On API failure
                FWLogger.logInfo(TAG, "AddAttendance API failure");
                //isAttendanceMarked = false;
                verifyLocationSettings();
            }
        } else if (classType.equalsIgnoreCase(AddItem.class.getSimpleName())) {
            Gson gson = new Gson();
            AddItem addItem = gson.fromJson(urlConnectionResponse.resultData, AddItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, addItem.getResultdata().getQuantity());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ADD);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            if (addItem.getCode().equalsIgnoreCase("200") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_added_successfully, HomeActivityNew.this)))
                Toast.makeText(HomeActivityNew.this, getString(R.string.item_added_successfully), Toast.LENGTH_SHORT).show();
            else if (addItem.getCode().equalsIgnoreCase("400") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_name_already_exist, HomeActivityNew.this)))
                Toast.makeText(HomeActivityNew.this, getString(R.string.item_name_already_exist), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(HomeActivityNew.this, addItem.getMessage(), Toast.LENGTH_SHORT).show();
//            callToApi();
            getItemList();
            FWLogger.logInfo(TAG, "Added Successful = " + addItem.getResultdata().getName());
        } else if (classType.equalsIgnoreCase(IssueItem.class.getSimpleName())) {
            Gson gson = new Gson();
            IssueItem issueItem = gson.fromJson(urlConnectionResponse.resultData, IssueItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, issueItem.getResultData().getQuantity());
            bundle.putInt(FirebaseGoogleAnalytics.Param.TECH_ID, issueItem.getResultData().getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ISSUE);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            Toast.makeText(HomeActivityNew.this, issueItem.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Issue Items to  = " + issueItem.getResultData().getUserId());
        }
        if (classType.equalsIgnoreCase(AddCredit.class.getSimpleName())) {
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

            Gson gson = new Gson();
            AddCredit addCredit = gson.fromJson(urlConnectionResponse.resultData, AddCredit.class);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
            bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_BAL, bundle);
            if (addCredit != null) {
                if (addCredit.getCode().equalsIgnoreCase("200") && addCredit.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.credit_added_successfully, HomeActivityNew.this))) {
//                    Toast.makeText(HomeActivityNew.this, getString(R.string.credit_added_successfully), Toast.LENGTH_LONG).show();
                    Toast.makeText(HomeActivityNew.this, "Amount credited successfully", Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, "Credit added successfully.");
                } else {
                    Toast.makeText(HomeActivityNew.this, addCredit.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (classType.equalsIgnoreCase(DeductBalance.class.getSimpleName())) {
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

            Gson gson = new Gson();
            DeductBalance deductBalance = gson.fromJson(urlConnectionResponse.resultData, DeductBalance.class);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.TRANSACTION);
            bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mAmount);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.DEDUCT_BAL, bundle);
            if (deductBalance != null) {
                if (deductBalance.getCode().equalsIgnoreCase("200") && deductBalance.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.amount_deducted_successfully, HomeActivityNew.this))) {
                    Toast.makeText(HomeActivityNew.this, getString(R.string.amount_deducted_successfully), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, "Amount deducted successfully.");
                } else {
                    Toast.makeText(HomeActivityNew.this, deductBalance.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (classType.equalsIgnoreCase(UpdateUser.class.getSimpleName())) {
            Gson gson = new Gson();
            UpdateUser updateUser = gson.fromJson(urlConnectionResponse.resultData, UpdateUser.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.EDIT);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.CONFIRM_PROFILE, bundle);
            FWLogger.logInfo(TAG, "Updated user Name " + updateUser.getResultData().getFirstName());
            if (updateUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.profile_updated_successfully, HomeActivityNew.this))) {
                Toast.makeText(HomeActivityNew.this, getString(R.string.profile_updated_successfully), Toast.LENGTH_SHORT).show();
                getUSerDetails();
            } else {
                Toast.makeText(HomeActivityNew.this, updateUser.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(UpdateTaskStatus.class.getSimpleName())) {
                Gson gson = new Gson();
                UpdateTaskStatus updateTaskStatus = gson.fromJson(urlConnectionResponse.resultData, UpdateTaskStatus.class);
                if (updateTaskStatus != null) {
                    if (updateTaskStatus.getCode().equalsIgnoreCase("200") && updateTaskStatus.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, HomeActivityNew.this))) {
                        Toast.makeText(HomeActivityNew.this, getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                        if (isTaskStart) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.START_TASK, bundle);

                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.NOT_STARTED;
                            mCoordinateOrigin = new LatLng(mTechLat, mTechLong);

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);

                            navigateToTaskNavigation(mTaskListData);

                            isTaskStart = false;
                        } else if (isTaskCountdown) {
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.STARTED_NOT_ENDED;

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);
                            isTaskCountdown = false;
                        } else if (isTaskEnd) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.ENDED_NO_PAYMENT);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.END_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.ENDED_NO_PAYMENT;

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);
                            isTaskEnd = false;
                            navigateToTaskClosure(mTaskListData);
                            /*if (mPaymentMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mPaymentModeId == Constant.PaymentMode.RATE_ID) {
                                navigateToPaymentScreen(mTaskListData);
                            } else {
                                navigateToTaskClosure(mTaskListData);
                            }*/
                        } else if (isTaskClosure) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.TASK_CLOSURE);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.END_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                            mTaskStatusId = TaskStatus.ON_GOING;
                            mTaskState = TaskState.TASK_CLOSURE;

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);
                            isTaskEnd = false;
                            navigateToPaymentScreen(mTaskListData);
                            /*if (mPaymentMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mPaymentModeId == Constant.PaymentMode.RATE_ID) {
                                navigateToPaymentScreen(mTaskListData);
                            } else {
                                navigateToTaskClosure(mTaskListData);
                            }*/
                        } else if (isTaskRejected) {
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Rejected.name());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.REJECT_TASK, bundle);
                            mTaskStatus = Constant.TaskCategories.Rejected.name();
                            mTaskStatusId = TaskStatus.REJECTED;
                            mTaskState = TaskState.NOT_STARTED;

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);

                            isTaskRejected = false;
                            navigateToTab(R.id.navigation_task);
                        } else if (isCompleted) {
                            mTaskStatus = Constant.TaskCategories.Completed.name();
                            mTaskStatusId = TaskStatus.COMPLETED;
                            mTaskState = TaskState.PAYMENT_RECEIVED;

                            mTaskListData.setId(mTaskListData.getId());
                            mTaskListData.setTaskStatus(mTaskStatus);
                            mTaskListData.setTaskStatusId(mTaskStatusId);
                            mTaskListData.setTaskState(mTaskState);

                            isTaskRejected = false;
                            navigateToTab(R.id.navigation_task);
                        }

                        /*else {
                            Toast.makeText(HomeActivityNew.this, updateTaskStatus.getMessage(), Toast.LENGTH_LONG).show();
//                    navigateToTab(R.id.navigation_task);
                            loadFragment(new MainTaskFragmentNew(), R.id.navigation_task);
                        }*/
                    } else {
                        Toast.makeText(HomeActivityNew.this, updateTaskStatus.getMessage(), Toast.LENGTH_LONG).show();
//                    navigateToTab(R.id.navigation_task);
                        loadFragment(new MainTaskFragmentNew(), R.id.navigation_task);
                        //Note Check ? This will work only if list get refreshed on back press other wise add if else aging for rejected
                    }
                }
            }
        }

        if (classType.equalsIgnoreCase(SendReport.class.getSimpleName())) {
            Gson gson = new Gson();
            SendReport sendReport = gson.fromJson(urlConnectionResponse.resultData, SendReport.class);
            if (sendReport != null) {
                FWLogger.logInfo(TAG, "Code : " + sendReport.getCode());
                FWLogger.logInfo(TAG, "Message : " + sendReport.getMessage());
                if (sendReport.getCode().equalsIgnoreCase("200") && sendReport.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.email_send_successfully, HomeActivityNew.this))) {
                    Toast.makeText(this, getString(R.string.email_send_successfully), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, sendReport.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            navigateToTab(R.id.navigation_task);
        } else if (classType.equalsIgnoreCase(ItemUnitType.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Get ITEM UNIT List Successful");
            Gson gson = new Gson();
            ItemUnitType itemUnitType = gson.fromJson(urlConnectionResponse.resultData, ItemUnitType.class);
            if (itemUnitType != null) {
                if (itemUnitType.getCode().equalsIgnoreCase("200") && itemUnitType.getMessage().equalsIgnoreCase("successfully.")) {
                    mitemUnitTypeList = new ArrayList<>();
                    mitemUnitTypeList = itemUnitType.getResultData();
                   /* for (ItemUnitType.ResultData resultData : mitemUnitTypeList) {
                        mItemsNameList.add(resultData.getName());
                    }*/
                }
            }
        }

        if (classType.equalsIgnoreCase(LanguageList.class.getSimpleName())) {
            Gson gson = new Gson();
            LanguageList languageList = gson.fromJson(urlConnectionResponse.resultData, LanguageList.class);
            FWLogger.logInfo(TAG, "languageList = " + languageList.getMessage());
            if (languageList.getCode().equalsIgnoreCase("200") && languageList.getMessage().equalsIgnoreCase("User Preferred language updated successfully"))
                Toast.makeText(this, languageList.getMessage(), Toast.LENGTH_LONG).show();
            UserDialog.restartActivity(HomeActivityNew.this);
        }
        if (classType.equalsIgnoreCase(AttendanceCheck.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "AttendanceCheck");
            Gson gson = new Gson();
            AttendanceCheck attendanceCheck = gson.fromJson(urlConnectionResponse.resultData, AttendanceCheck.class);
            if (attendanceCheck != null) {
                if (attendanceCheck.getCode().equalsIgnoreCase("200") && attendanceCheck.getMessage().equalsIgnoreCase("Attendance already added.")) {
                    FWLogger.logInfo(TAG, "Attendance already added.");
                    // Call API to check disclaimer is accepted or not
                    //isAttendanceMarked = true;
                    checkDisclaimer();
                } else {
                    //Go To next step i.e. show dialog to mark attendance
                    //isAttendanceMarked = false;
                    FWLogger.logInfo(TAG, "Open Attendance Mark Dialog ");
                    Toast.makeText(HomeActivityNew.this, R.string.please_check_in_first, Toast.LENGTH_SHORT).show();
                    UserDialog userDialog = UserDialog.getInstance();
                    userDialog.showAttendanceMarkDialog(HomeActivityNew.this);
                }
            } else {
                FWLogger.logInfo(TAG, "AttendanceCheck API failure");
                // On API failure
                //isAttendanceMarked = false;
                verifyLocationSettings();
            }
        } else if (classType.equalsIgnoreCase(GetUserDisclaimer.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Disclaimer check");
            Gson gson = new Gson();
            GetUserDisclaimer getUserDisclaimer = gson.fromJson(urlConnectionResponse.resultData, GetUserDisclaimer.class);
            if (getUserDisclaimer != null) {
                if (getUserDisclaimer.getCode().equalsIgnoreCase("200") && getUserDisclaimer.getMessage().equalsIgnoreCase("successfully.") && getUserDisclaimer.getResultData().getIsAccepted()) {
                    // Disclaimer is accepted then load fragments according to user role
                    FWLogger.logInfo(TAG, "Disclaimer is already accepted");
                    //selectDashboardFragment();
                } else {
                    // Disclaimer is not accepted then show dialog to accept disclaimer
                    FWLogger.logInfo(TAG, "Disclaimer is not accepted");
                    UserDialog userDialog = UserDialog.getInstance();
                    userDialog.disclaimerDialog(this, getUserDisclaimer.getResultData().getDisclaimerHtml(), getUserDisclaimer.getResultData().getDCN());
                }
            } else {
                FWLogger.logInfo(TAG, "GetUserDisclaimer API failure");
                // Give specific call : Call API for check disclaimer is accepted or not
                checkDisclaimer();
            }
        } else if (classType.equalsIgnoreCase(AcceptDisclaimer.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Accept Disclaimer");
            Gson gson = new Gson();
            AcceptDisclaimer acceptDisclaimer = gson.fromJson(urlConnectionResponse.resultData, AcceptDisclaimer.class);
            if (acceptDisclaimer != null) {
                if (acceptDisclaimer.getCode().equalsIgnoreCase("200") && acceptDisclaimer.getMessage().equalsIgnoreCase("successfully.") && acceptDisclaimer.getResultData().getIsAccepted()) {
                    // Disclaimer is accepted then load fragments according to user role
                    FWLogger.logInfo(TAG, "Accept Disclaimer successfully");
                    //selectDashboardFragment();
                } else {
                    // Give specific call : Call API for check disclaimer is accepted or not
                    FWLogger.logInfo(TAG, "Unable to Accept Disclaimer");
                    checkDisclaimer();
                }
            } else {
                FWLogger.logInfo(TAG, "AcceptDisclaimer API failure");
                // Give specific call : Call API for check disclaimer is accepted or not
                checkDisclaimer();
            }
        } else if (classType.equalsIgnoreCase(ChangePassword.class.getSimpleName())) {
            Gson gson = new Gson();
            ChangePassword changePassword = gson.fromJson(urlConnectionResponse.resultData, ChangePassword.class);

            if (changePassword != null) {
                if (changePassword.getCode().equalsIgnoreCase("200") && changePassword.getMessage().equalsIgnoreCase("Password updated successfully.")) {
                    Toast.makeText(this, changePassword.getMessage(), Toast.LENGTH_LONG).show();
                    SharedPrefManager.getInstance(HomeActivityNew.this).logout(false);
                    Intent intent = new Intent(HomeActivityNew.this, LoginTouchlessActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, changePassword.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (classType.equalsIgnoreCase(LocationUpdateToServer.class.getSimpleName())) {
            Gson gson = new Gson();
            LocationUpdateToServer locationUpdateToServer = gson.fromJson(urlConnectionResponse.resultData, LocationUpdateToServer.class);
            //Don't show anything on screen
            if (locationUpdateToServer != null) {
                if (locationUpdateToServer.getCode().equalsIgnoreCase("200") && locationUpdateToServer.getMessage().equalsIgnoreCase("Live location updated successfully.")) {
                    FWLogger.logInfo(TAG, "Message : " + locationUpdateToServer.getMessage());
                } else {
                    FWLogger.logInfo(TAG, "Message : " + locationUpdateToServer.getMessage());
                }
            }
            //Cancel and make it null to avoid congestion of network call
            if (mSendLocationToServerAsyncTask != null) {
                mSendLocationToServerAsyncTask.cancel(true);
                mSendLocationToServerAsyncTask = null;
            }
        }
        if (classType.equalsIgnoreCase(AddEnquiry.class.getSimpleName())) {
            Gson gson = new Gson();
            AddEnquiry addEnquiry = gson.fromJson(urlConnectionResponse.resultData, AddEnquiry.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, addEnquiry.getResultData().getMobileNumber());
            FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_ENQ, bundle);

            Toast.makeText(this, addEnquiry.getMessage(), Toast.LENGTH_SHORT).show();
        } /*else if (classType.equalsIgnoreCase(ServiceType.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received service type Successfully");
            Gson gson = new Gson();
            mServiceTypeResultData = new ArrayList<>();
            ServiceType serviceType = gson.fromJson(urlConnectionResponse.resultData, ServiceType.class);
            mServiceTypeResultData = serviceType.getResultData();
            mServiceTypeList = new ArrayList<>();
            *//*for (int i = 0; i < mServiceTypeResultData.size(); i++) {
                mServiceTypeList.add(mServiceTypeResultData.get(i).getServiceName());
            }*//*
            for (ServiceType.ResultData resultData : mServiceTypeResultData) {
                mServiceTypeList.add(resultData.getServiceName());
            }
            getReferenceType();
        }*/ else if (classType.equalsIgnoreCase(ReferenceType.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received reference type Successfully");
            Gson gson = new Gson();
            mReferenceTypeResultData = new ArrayList<>();
            ReferenceType referenceType = gson.fromJson(urlConnectionResponse.resultData, ReferenceType.class);
            mReferenceTypeResultData = referenceType.getResultData();
            mReferenceTypeList = new ArrayList<>();
            /*for (int i = 0; i < mReferenceTypeResultData.size(); i++) {
                mReferenceTypeList.add(mReferenceTypeResultData.get(i).getReferenceName());
            }*/
            for (ReferenceType.ResultData resultData : mReferenceTypeResultData) {
                mReferenceTypeList.add(resultData.getReferenceName());
            }
        } else if (classType.equalsIgnoreCase(ReassignTask.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received reference type Successfully");
            Gson gson = new Gson();
            ReassignTask reassignTask = gson.fromJson(urlConnectionResponse.resultData, ReassignTask.class);
            FWLogger.logInfo(TAG, "MSG : " + reassignTask.getMessage());
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_CREATED_BY, mTaskCreatedBy);
            bundle.putBoolean(FirebaseGoogleAnalytics.Param.TASK_REASSIGNED, reassignTask.getMessage().equalsIgnoreCase("Task Reassigned Successfully."));
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.REASSIGN_TASK, bundle);
            if (reassignTask != null) {
                if (reassignTask.getCode().equalsIgnoreCase("200")) {
                    Toast.makeText(this, reassignTask.getMessage(), Toast.LENGTH_SHORT).show();
                    //Update noti fragment if open or visible
                    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
                    if (currentFragment != null && currentFragment instanceof NotificationFragment) {
                        //DO STUFF
                        FWLogger.logInfo(TAG, "notificationFragment is found in foreground");
                        if (((NotificationFragment) currentFragment).mNewNotificationListener != null) {
                            FWLogger.logInfo(TAG, "mNewNotificationListener is not null");
                            ((NotificationFragment) currentFragment).mNewNotificationListener.onNewNotification();
                        }
                    } else if (currentFragment != null && currentFragment instanceof TaskDetailsFragmentNew) {
                        //First pop back stack immediately to get notification fragment to come out to foreground
                        getSupportFragmentManager().popBackStackImmediate();
                        currentFragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
                        //DO STUFF
                        FWLogger.logInfo(TAG, "notificationFragment is found in foreground");
                        if (currentFragment instanceof TaskDetailsFragmentNew || currentFragment instanceof MainTaskFragmentNew) {

                        } else if (((NotificationFragment) currentFragment).mNewNotificationListener != null) {
                            FWLogger.logInfo(TAG, "mNewNotificationListener is not null");
                            ((NotificationFragment) currentFragment).mNewNotificationListener.onNewNotification();
                        }
                    }
                } else {
                    Toast.makeText(this, reassignTask.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (classType.equalsIgnoreCase(CompanyDetails.class.getSimpleName())) {
            Gson gson = new Gson();
            CompanyDetails companyDetails = gson.fromJson(urlConnectionResponse.resultData, CompanyDetails.class);

            if (companyDetails != null) {
                if (companyDetails.getCode().equalsIgnoreCase("200")) {
                    FWLogger.logInfo(TAG, "" + companyDetails.getMessage());
                    mCompanyDetails = companyDetails;

                    /*if (isCompanyClicked) {
                        UserDialog userDialog = UserDialog.getInstance();
                        userDialog.changeCompanyNameDialog(HomeActivityNew.this, mCompanyDetails.getResultData());
                        isCompanyClicked = false;
                    }*/
                } else {
                    FWLogger.logInfo(TAG, "" + companyDetails.getMessage());
//                    Toast.makeText(HomeActivity.this, ""+companyDetails.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (classType.equalsIgnoreCase(CheckOut.class.getSimpleName())) {
            Gson gson = new Gson();
            CheckOut checkOut = gson.fromJson(urlConnectionResponse.resultData, CheckOut.class);
            if (checkOut != null) {
                if (checkOut.getCode().equalsIgnoreCase("200") && checkOut.getMessage().equalsIgnoreCase("Check out successfully.")) {
                    Toast.makeText(this, checkOut.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, checkOut.getMessage(), Toast.LENGTH_SHORT).show();
                }
                //logoutAndNavigateToLogin(); // Commented By Manish : As per SAhil Requirement : 06-04-2022
                drawerLayout.closeDrawer(Gravity.LEFT); // ADDED BY MANISH 06-04-2022
            } else {
                FWLogger.logInfo(TAG, "Check Out = " + checkOut.getMessage());
                Toast.makeText(this, R.string.failed_to_checkout_try_again, Toast.LENGTH_SHORT).show();
            }
        }
        // FOR NEW USER : ADD TECH RESPONSE::
        else if (classType.equalsIgnoreCase(AddBulkTech.class.getSimpleName())) {
            Gson gson = new Gson();
            AddBulkTech addBulkTech = gson.fromJson(urlConnectionResponse.resultData, AddBulkTech.class);
            if (addBulkTech != null) {
                FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, HomeActivityNew.this))) {
                    Toast.makeText(HomeActivityNew.this, getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                    disableValidate(mTextViewValidate);
                    enableSubmit(mTextViewSubmit);
                } else {
                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, HomeActivityNew.this))) {
                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                        bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                        Toast.makeText(HomeActivityNew.this, getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                        //((HomeActivityNew) HomeActivityNew.this).navigateToTab(R.id.navigation_home);
                        fromWhere = "FIRST_ADD_TECH";
                        getUserList();
                    } else {
                        if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, HomeActivityNew.this))) {
                            Toast.makeText(HomeActivityNew.this, getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(HomeActivityNew.this, addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        disableSubmit(mTextViewSubmit);
                    }
                }
            }
        }
        // FOR DELETE SELF ACC ::
        else if (classType.equalsIgnoreCase(DeleteOwnerTechnician.class.getSimpleName())) {
            Gson gson = new Gson();
            DeleteOwnerTechnician mDeleteTech = gson.fromJson(urlConnectionResponse.resultData, DeleteOwnerTechnician.class);
            if (mDeleteTech.getCode().equalsIgnoreCase("200") && mDeleteTech.getMessage().equalsIgnoreCase("Your account has been deleted.")) {
                FWLogger.logInfo(TAG, "Your account has been deleted.");
                Toast.makeText(HomeActivityNew.this, mDeleteTech.getMessage(), Toast.LENGTH_SHORT).show();
                logoutAndNavigateToLogin();
            } else {
                Toast.makeText(HomeActivityNew.this, "Unable to delete Account", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void updateTaskStatus(int taskStatus, int taskState, String rejectedTaskNotes) {
        FWLogger.logInfo(TAG, "Distance : " + MapUtils.totalDistance.split(" ")[0]);
      /*  mUpdateTaskStatusAsyncTask = new UpdateTaskStatusAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mUpdateTaskStatusAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(),
                mTaskListData.getId(), taskStatus, System.currentTimeMillis(), taskState, MapUtils.totalDistance.split(" ")[0], rejectedTaskNotes);*/

        updateTaskStatusRetro(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), mTaskListData.getId(), taskStatus, System.currentTimeMillis(), taskState, MapUtils.totalDistance.split(" ")[0], rejectedTaskNotes);

        mPaymentModeId = mTaskListData.getPaymentModeId(); //getArguments().getInt("PaymentModeId");
        if (mTaskListData.getPaymentMode() != null) {
            mPaymentMode = mTaskListData.getPaymentMode();
        }

        if (taskStatus == TaskStatus.ON_GOING && taskState == TaskState.NOT_STARTED)
            isTaskStart = true;
        else if (taskStatus == TaskStatus.ON_GOING && taskState == TaskState.STARTED_NOT_ENDED)
            isTaskCountdown = true;
        else if (taskStatus == TaskStatus.ON_GOING && taskState == TaskState.ENDED_NO_PAYMENT)
            isTaskEnd = true;
        else if (taskStatus == TaskStatus.REJECTED && taskState == TaskState.NOT_STARTED)
            isTaskRejected = true;
        else if (taskStatus == TaskStatus.COMPLETED && taskState == TaskState.PAYMENT_RECEIVED)
            isCompleted = true;
        else if (taskStatus == TaskStatus.ON_GOING && taskState == TaskState.TASK_CLOSURE && mPaymentMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mPaymentModeId == Constant.PaymentMode.RATE_ID) {
            isTaskClosure = true;
        }

        FWLogger.logInfo(TAG, "Updating Task ");
    }

    private void updateTaskStatusRetro(int userId, int taskId, int statusId, long time, int taskstate, String totalDistance, String rejectedTaskNotes) {
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<UpdateTaskStatus> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().updateTaskStatus(userId, taskId, statusId, time, taskstate, totalDistance, rejectedTaskNotes, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateTaskStatus>() {
                    @Override
                    public void onResponse(Call<UpdateTaskStatus> call, Response<UpdateTaskStatus> response) {
                        try {
                            if (response.code() == 200) {
                                UpdateTaskStatus updateTaskStatus = response.body();
                                if (updateTaskStatus != null) {
                                    if (updateTaskStatus.getCode().equalsIgnoreCase("200") && updateTaskStatus.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, HomeActivityNew.this))) {
                                        Toast.makeText(HomeActivityNew.this, getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                                        if (isTaskStart) {
                                            //NOTE: Log GA event
                                            Bundle bundle = new Bundle();
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.START_TASK, bundle);

                                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                                            mTaskStatusId = TaskStatus.ON_GOING;
                                            mTaskState = TaskState.NOT_STARTED;
                                            mCoordinateOrigin = new LatLng(mTechLat, mTechLong);

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);

                                            navigateToTaskNavigation(mTaskListData);

                                            isTaskStart = false;
                                        } else if (isTaskCountdown) {
                                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                                            mTaskStatusId = TaskStatus.ON_GOING;
                                            mTaskState = TaskState.STARTED_NOT_ENDED;

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);
                                            isTaskCountdown = false;
                                        } else if (isTaskEnd) {
                                            //NOTE: Log GA event
                                            Bundle bundle = new Bundle();
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.ENDED_NO_PAYMENT);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.END_TASK, bundle);
                                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                                            mTaskStatusId = TaskStatus.ON_GOING;
                                            mTaskState = TaskState.ENDED_NO_PAYMENT;

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);
                                            isTaskEnd = false;
                                            navigateToTaskClosure(mTaskListData);

                                        } else if (isTaskClosure) {
                                            //NOTE: Log GA event
                                            Bundle bundle = new Bundle();
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.TASK_CLOSURE);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Ongoing.name());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.END_TASK, bundle);
                                            mTaskStatus = Constant.TaskCategories.Ongoing.name();
                                            mTaskStatusId = TaskStatus.ON_GOING;
                                            mTaskState = TaskState.TASK_CLOSURE;

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);
                                            isTaskEnd = false;
                                            navigateToPaymentScreen(mTaskListData);
                                        } else if (isTaskRejected) {
                                            //NOTE: Log GA event
                                            Bundle bundle = new Bundle();
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskListData.getId());
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.NOT_STARTED);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Rejected.name());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                            FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.REJECT_TASK, bundle);
                                            mTaskStatus = Constant.TaskCategories.Rejected.name();
                                            mTaskStatusId = TaskStatus.REJECTED;
                                            mTaskState = TaskState.NOT_STARTED;

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);

                                            isTaskRejected = false;
                                            navigateToTab(R.id.navigation_task);
                                        } else if (isCompleted) {
                                            mTaskStatus = Constant.TaskCategories.Completed.name();
                                            mTaskStatusId = TaskStatus.COMPLETED;
                                            mTaskState = TaskState.PAYMENT_RECEIVED;

                                            mTaskListData.setId(mTaskListData.getId());
                                            mTaskListData.setTaskStatus(mTaskStatus);
                                            mTaskListData.setTaskStatusId(mTaskStatusId);
                                            mTaskListData.setTaskState(mTaskState);

                                            isTaskRejected = false;
                                            navigateToTab(R.id.navigation_task);
                                        }
                                    } else {
                                        Toast.makeText(HomeActivityNew.this, updateTaskStatus.getMessage(), Toast.LENGTH_LONG).show();
                                        loadFragment(new MainTaskFragmentNew(), R.id.navigation_task);
                                        //Note Check ? This will work only if list get refreshed on back press other wise add if else aging for rejected
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateTaskStatus> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateTaskStatus API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateTaskStatus API:");
            ex.getMessage();
        }


    }

    public void navigateToTaskNavigation(TasksList.ResultData resultData) {
        FWLogger.logInfo(TAG, "navigateToTaskNavigation");

        StartTaskTrackingFragmentNew startTaskTrackingFragmentNew = new StartTaskTrackingFragmentNew();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", resultData);
        startTaskTrackingFragmentNew.setArguments(bundle);
        transaction.replace(R.id.home_fragment_container, startTaskTrackingFragmentNew);
        transaction.commit();

        /*StartTaskTrackingFragmentNew startTaskTrackingFragmentNew = new StartTaskTrackingFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", resultData);
        startTaskTrackingFragmentNew.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, startTaskTrackingFragmentNew);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();*/
    }

    public void navigateToStartTaskCountDown(TasksList.ResultData resultData) {
        FWLogger.logInfo(TAG, "navigateToStartTaskCountDown");
        checkLocationPermission();
        checkCameraPermission();
        CountdownTechFragmentNew countdownTechFragmentNew = new CountdownTechFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", resultData);
        countdownTechFragmentNew.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, countdownTechFragmentNew);
//        transaction.isAddToBackStackAllowed();
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void navigateToTaskClosure(TasksList.ResultData resultData) {
        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", resultData);
        taskClosureFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
        transaction.commit();
    }


    private void navigateToPaymentScreen(TasksList.ResultData resultData) {
        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", resultData);
        techPaymentReceivedFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
        transaction.commit();
    }

    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, REQUEST_CODE_PERMISSIONS);
                } else {
                    if (cameraFlag == false) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, REQUEST_CODE_PERMISSIONS);
                    } else {
                        cameraFlag = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }

    private boolean checkLocationPermission() {
        boolean locationFlag = false;
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU) {
                final int checkFineLocation = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (checkFineLocation != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSIONS);
                } else {
                    if (locationFlag == false) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSIONS);
                    } else {
                        locationFlag = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return locationFlag;
    }


    @Override
    protected void onPause() {
        FWLogger.logInfo(TAG, "Activity life cycle onPause");
        super.onPause();
//        FWLogger.logInfo(TAG, "OnPause GPS Status : "+isGPSStatus);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationBroadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FWLogger.logInfo(TAG, "Activity life cycle onStop");
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            //
            FWLogger.logInfo(TAG, "onStop mBound true");
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    @Override
    public void onDestroy() {
        FWLogger.logInfo(TAG, "Activity life cycle onDestroy");

        CommonFunction.hideProgressDialog(HomeActivityNew.this);

        if (mSendLocationToServerAsyncTask != null) mSendLocationToServerAsyncTask.cancel(true);

        if (mSendEmailAsyncTask != null) mSendEmailAsyncTask.cancel(true);

        if (mUpdateProfilePicAsyncTask != null) mUpdateProfilePicAsyncTask.cancel(true);

        //Stop location sharing service to app server.........
        //stopService(new Intent(this, LocationService.class));
        //isAttendanceMarked =false;
        mLocationUpdatesService.removeLocationUpdates();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocationUpdateReceiver);
       /* if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            HomeActivityNew.this.unregisterReceiver(batteryBroadcastReceiver);
            HomeActivityNew.this.unregisterReceiver(GPSBroadcastReceiver);
        }*/
        //Ends................................................
        super.onDestroy();
    }

    public void unRegisterBroadcast() {
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            HomeActivityNew.this.unregisterReceiver(batteryBroadcastReceiver);
            HomeActivityNew.this.unregisterReceiver(GPSBroadcastReceiver);
        }
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                FWLogger.logInfo(TAG, "onRequestPermissionsUpdate");
                //Always continue form first step
                verifyLocationSettings();
            }
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                captureImage();
            } else if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
            }
        }
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        FWLogger.logInfo(TAG, "getBackStackEntryCount = " + count);
        if (count == 0) {
            //AlertDialog.exitAlertDialog(this);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.home_fragment_container);
            if (fragment instanceof OwnerDashboardFragment || fragment instanceof TechDashboardFragmentNew) {
                AlertDialog.exitAlertDialog(this);
            } else {
                navigateToTab(R.id.navigation_home);
            }
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void pickImage(String fromWhere) {
        checkCameraPermission();
        fromItemDialog = fromWhere;
        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(HomeActivityNew.this);
        builder.setTitle(getResources().getString(R.string.select_option));
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(HomeActivityNew.this.getApplicationContext())) {
                        Toast.makeText(HomeActivityNew.this.getApplicationContext(), R.string.device_doesnt_supprt_camera, Toast.LENGTH_LONG).show();
                    }
                    if (checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (requestToEnableAllPermission(mListener, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    HomeActivityNew.isOwner = true;// TODO check
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {

                    if (checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    } else if (requestToEnableAllPermission(mListener, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    }
                    HomeActivityNew.isOwner = true;// TODO check
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    HomeActivityNew.isOwner = true;  // TODO check
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File storageDir = HomeActivityNew.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photo = null;
        try {
            photo = File.createTempFile(System.currentTimeMillis() + "_profile", ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mCapturedImagePath = photo.getAbsolutePath();
        mImageUri = FileProvider.getUriForFile(HomeActivityNew.this, BuildConfig.APPLICATION_ID + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        HomeActivityNew.isOwner = true;// TODO check
        FWLogger.logInfo(TAG, "Home_requestCode : " + requestCode + "  resultCode : " + resultCode);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                FWLogger.logInfo(TAG, "Place : " + place.getName() + ", ID : " + place.getId() + ", Address : " + place.getAddress() + ", longitude : " + place.getLatLng().longitude + ", latitude : " + place.getLatLng().latitude);
                if (place != null) FWDialog.callback(place);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                FWLogger.logInfo(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                FWLogger.logInfo(TAG, "Request has canceled");
            }
        } else if (requestCode == LOCATION_REQUEST_CODE) {
            // Again open date picker so that user will not be able to open app/next screen
            FWLogger.logInfo(TAG, "GPS LOCATION_REQUEST_CODE");
            //Always continue form first step
            //verifyLocationSettings(); No need to call this from here as we already calling it from onStart
        } else if (requestCode == BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

            FWLogger.logInfo(TAG, "Camera");
            Uri selectedImage = mImageUri;
            HomeActivityNew.this.getContentResolver().notifyChange(selectedImage, null);
            try {
                Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(HomeActivityNew.this, mImageUri, mCapturedImagePath);
                //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                String filePath = CameraUtils.getFilePathFromURI(mImageUri, HomeActivityNew.this);
                //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
//                if(isProfileImgClicked) {

                bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);

                if (fromItemDialog.equalsIgnoreCase("ItemDialog")) {
                    itemDialogBitmap = bitmap;
                    ItemDialog itemDialog = ItemDialog.getInstance();
                    itemDialog.setPhoto(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("ItemDialogUpdate")) {
                    itemDialogBitmap = bitmap;
                    ItemDialog itemDialog = ItemDialog.getInstance();
                    itemDialog.setPhotoUpdate(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService1")) {
                    itemDialogBitmap = bitmap;
                    AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                    itemDialog.setPhotoUpdateForService1(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService2")) {
                    itemDialogBitmap = bitmap;
                    AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                    itemDialog.setPhotoUpdateForService2(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService3")) {
                    itemDialogBitmap = bitmap;
                    AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                    itemDialog.setPhotoUpdateForService3(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddLead1")) {
                    itemDialogBitmap = bitmap;
                    AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                    addEditLeadDialog.setPhotoUpdateForLead1(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddLead2")) {
                    itemDialogBitmap = bitmap;
                    AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                    addEditLeadDialog.setPhotoUpdateForLead2(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddLead3")) {
                    itemDialogBitmap = bitmap;
                    AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                    addEditLeadDialog.setPhotoUpdateForLead3(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct1")) {
                    itemDialogBitmap = bitmap;
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.setPhotoUpdateForProduct1(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct2")) {
                    itemDialogBitmap = bitmap;
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.setPhotoUpdateForProduct2(itemDialogBitmap);
                    fromItemDialog = "";
                } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct3")) {
                    itemDialogBitmap = bitmap;
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.setPhotoUpdateForProduct3(itemDialogBitmap);
                    fromItemDialog = "";
                } else {
                    mProfileCircleImageView.setImageBitmap(bitmap);
                    ConvertBitmapToString(bitmap);
                    updatePhoto();
                }
            } catch (Exception e) {
                try {
//                    if(isProfileImgClicked) {
                    mProfileCircleImageView.setImageResource(R.drawable.profile_icon);
//                    }
                } catch (Exception ex) {

                }
                Toast.makeText(HomeActivityNew.this, R.string.failed_to_load, Toast.LENGTH_SHORT).show();
                FWLogger.logInfo("Camera", e.toString());
            }

        } else if (requestCode == BaseActivity.PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                FWLogger.logInfo(TAG, "Gallery");
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(HomeActivityNew.this, selectedImage, null);
                    //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                    String filePath = CameraUtils.getFilePathFromURI(selectedImage, HomeActivityNew.this);
                    //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
//
                    bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);

                    if (fromItemDialog.equalsIgnoreCase("ItemDialog")) {
                        itemDialogBitmap = bitmap;
                        ItemDialog itemDialog = ItemDialog.getInstance();
                        itemDialog.setPhoto(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("ItemDialogUpdate")) {
                        itemDialogBitmap = bitmap;
                        ItemDialog itemDialog = ItemDialog.getInstance();
                        itemDialog.setPhotoUpdate(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService1")) {
                        itemDialogBitmap = bitmap;
                        AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                        itemDialog.setPhotoUpdateForService1(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService2")) {
                        itemDialogBitmap = bitmap;
                        AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                        itemDialog.setPhotoUpdateForService2(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddUpdateService3")) {
                        itemDialogBitmap = bitmap;
                        AddUpdateServiceDialog itemDialog = AddUpdateServiceDialog.getInstance();
                        itemDialog.setPhotoUpdateForService3(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddLead1")) {
                        itemDialogBitmap = bitmap;
                        AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                        addEditLeadDialog.setPhotoUpdateForLead1(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddLead2")) {
                        itemDialogBitmap = bitmap;
                        AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                        addEditLeadDialog.setPhotoUpdateForLead2(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddLead3")) {
                        itemDialogBitmap = bitmap;
                        AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                        addEditLeadDialog.setPhotoUpdateForLead3(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct1")) {
                        itemDialogBitmap = bitmap;
                        AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                        addEditAssetDialog.setPhotoUpdateForProduct1(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct2")) {
                        itemDialogBitmap = bitmap;
                        AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                        addEditAssetDialog.setPhotoUpdateForProduct2(itemDialogBitmap);
                        fromItemDialog = "";
                    } else if (fromItemDialog.equalsIgnoreCase("fromAddProduct3")) {
                        itemDialogBitmap = bitmap;
                        AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                        addEditAssetDialog.setPhotoUpdateForProduct3(itemDialogBitmap);
                        fromItemDialog = "";
                    } else {
                        mProfileCircleImageView.setImageBitmap(bitmap);
                        ConvertBitmapToString(bitmap);
                        updatePhoto();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(HomeActivityNew.this, "Cancelled", Toast.LENGTH_SHORT).show();
//                getActivity().onBackPressed();
            } else {
                Toast.makeText(HomeActivityNew.this, R.string.failed_to_load, Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == 999) { // FOR PDF WHICH IS UNDER 6MB of SIZE
            // Get the Uri of the selected file
           /* Uri uri = data.getData();
            String uriString = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            String displayName = null;

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = HomeActivityNew.this.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        try {
                            InputStream targetStream = new FileInputStream(path);
                        } catch (FileNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }
*/
            /*Uri uri = data.getData();
            File file = new File(uri.getPath());
            if (file.length() <= MAX_FILE_SIZE) {
                *//*if (file.exists() && file.length() <= MAX_FILE_SIZE) {*//*
                selectedFileName = file.getName();
                //openPdfFile(HomeActivityNew.this, file);
                //START :: INPUT STREAM OF PDF
                File initialFile = new File(uri.getPath());
                byte[] bFile = new byte[(int) file.length()];
                //InputStream targetStream = new FileInputStream(sPath);
                //FileInputStream targetStream = new FileInputStream(initialFile);
                InputStream targetStream = null;
                try {
                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File filee = new File(path, "abc.pdf");
                    path.mkdirs();
                    targetStream = new FileInputStream(filee);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                try {
                    targetStream.read(bFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    targetStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Base64.encodeToString(bFile, Base64.NO_WRAP);
                AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                addUpdateServiceDialog.setSelectedPDFName(selectedFileName, targetStream);
                *//*AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                addUpdateServiceDialog.setSelectedPDFName(selectedFileName, null);*//*
                //END :: INPUT STREAM OF PDF

            } else {
                Toast.makeText(HomeActivityNew.this, "Invalid PDF file or exceeds maximum size", Toast.LENGTH_SHORT).show();
            }*/
        } else if (requestCode == PICK_CONTACT_REQUEST_TASK && resultCode == RESULT_OK) { // FOR CONTACT NAME/NUMBER PICKER
            // Get the contact URI from the intent data
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsTask(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_AMC && resultCode == RESULT_OK) {
            // Get the contact URI from the intent data
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsAMC(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_LEAD && resultCode == RESULT_OK) {
            // Get the contact URI from the intent data
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsLead(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_ENQUIRY && resultCode == RESULT_OK) {
            // Get the contact URI from the intent data
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsEnquiry(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_ENQUIRYFORSERVICE && resultCode == RESULT_OK) {
            // Get the contact URI from the intent data
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsEnquiryForService(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_ADDUPDATE_CUSTPRI && resultCode == RESULT_OK) {
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactForAddUpdateCustPri(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_ADDUPDATE_CUSTSEC && resultCode == RESULT_OK) {
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactForAddUpdateCustSec(contactUri);
            }
        } else if (requestCode == PICK_CONTACT_REQUEST_ADDTECH && resultCode == RESULT_OK) {
            if (data != null) {
                Uri contactUri = data.getData();
                retrieveContactDetailsAddTech(contactUri);
            }
        }

    }


    private void retrieveContactDetailsTask(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Remove the "+91" prefix and spaces from the number
                //number = number.replace("+91", "").replace(" ", "");
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                taskDialogNew.getContactData(name, number);

            }
            cursor.close();
        }
    }

    private void retrieveContactDetailsAMC(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Remove the "+91" prefix and spaces from the number
                //number = number.replace("+91", "").replace(" ", "");
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                AMCDialog amcDialog = AMCDialog.getInstance();
                amcDialog.getContactData(name, number);

            }
            cursor.close();
        }
    }

    private void retrieveContactDetailsLead(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Remove the "+91" prefix and spaces from the number
                //number = number.replace("+91", "").replace(" ", "");
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                addEditLeadDialog.getContactData(name, number);

            }
            cursor.close();
        }
    }

    private void retrieveContactDetailsEnquiry(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Remove the "+91" prefix and spaces from the number
                //number = number.replace("+91", "").replace(" ", "");
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.getContactData(name, number);

            }
            cursor.close();
        }
    }

    private void retrieveContactDetailsEnquiryForService(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Remove the "+91" prefix and spaces from the number
                //number = number.replace("+91", "").replace(" ", "");
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.getContactData(name, number);

            }
            cursor.close();
        }
    }

    private void retrieveContactForAddUpdateCustPri(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.getContactDataForAddUpdateCustPri(name, number);
            }
            cursor.close();
        }
    }

    private void retrieveContactForAddUpdateCustSec(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.getContactDataForAddUpdateCustSec(name, number);
            }
            cursor.close();
        }
    }

    private void retrieveContactDetailsAddTech(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            if (nameIndex != -1 && numberIndex != -1) {
                String name = cursor.getString(nameIndex);
                String number = cursor.getString(numberIndex);

                // Split the full name by space
                String[] nameParts = name.split(" ");

                // Initialize first name and last name
                String firstName = "";
                String lastName = "";

                // If there are two or more name parts, set first name and last name
                if (nameParts.length >= 2) {
                    firstName = nameParts[0];
                    lastName = nameParts[nameParts.length - 1];
                } else if (nameParts.length == 1) {
                    // If only one part, consider it as the first name
                    firstName = nameParts[0];
                }
                // Remove the "+91" prefix and spaces from the number
                number = number.replace("+91", "").replace(" ", "").replace("-", "");

                AddTechListAdapter.getContactData(firstName, lastName, number);
            }
            cursor.close();
        }
    }


    public void getContactDetailsIntentTask() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_TASK);
    }

    public void getContactDetailsIntentAMC() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_AMC);
    }

    public void getContactDetailsIntentLead() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_LEAD);
    }

    public void getContactDetailsIntentEnquiry() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_ENQUIRY);
    }

    public void getContactDetailsIntentEnquiryForService() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_ENQUIRYFORSERVICE);
    }

    public void getContactDetailsIntentAddUpdateCustPri() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_ADDUPDATE_CUSTPRI);
    }

    public void getContactDetailsIntentAddUpdateCustSec() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_ADDUPDATE_CUSTSEC);
    }

    public void getContactDetailsIntentAddTech() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST_ADDTECH);
    }

    public void ConvertBitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mImageEncodeBaseString = Base64.encodeToString(b, Base64.DEFAULT);
        FWLogger.logInfo(TAG, mImageEncodeBaseString);
    }

    public void hideHeader() {
        mRelativeHeaderLayout.setVisibility(View.GONE);
        mDatesRecyclerView.setVisibility(View.GONE);
        mBottomNavigationView.setVisibility(View.GONE);
//        mBottomConstraint.setVisibility(View.GONE);
        mBottomCoordinator.setVisibility(View.GONE);
    }

    public void showHeader() {
        mRelativeHeaderLayout.setVisibility(View.VISIBLE);
        mDatesRecyclerView.setVisibility(View.VISIBLE);
        mBottomNavigationView.setVisibility(View.VISIBLE);
//        mBottomConstraint.setVisibility(View.VISIBLE);
        mBottomCoordinator.setVisibility(View.VISIBLE);
    }

    private BroadcastReceiver GPSBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {

                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                if (isGpsEnabled) {
                    //location is enabled
                    isGPSEnabledByUser = true;
                } else {
                    //location is disabled
                    isGPSEnabledByUser = false;
                }
            }
        }
    };

    private BroadcastReceiver batteryBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*deviceStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);*/
            /*if (deviceStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                //textview.setText(currentBatteryStatus + " = Charging at " + batteryLevel + " %");
                Toast.makeText(MainActivity.this, currentBatteryStatus + " = Charging at " + batteryLevel + " %", Toast.LENGTH_SHORT).show();
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_DISCHARGING) {
                //textview.setText(currentBatteryStatus + " = Discharging at " + batteryLevel + " %");
                Toast.makeText(MainActivity.this, currentBatteryStatus + " = Discharging at " + batteryLevel + " %", Toast.LENGTH_SHORT).show();
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_FULL) {
                //textview.setText(currentBatteryStatus + "= Battery Full at " + batteryLevel + " %");
                Toast.makeText(MainActivity.this, currentBatteryStatus + "= Battery Full at " + batteryLevel + " %", Toast.LENGTH_SHORT).show();
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                //textview.setText(currentBatteryStatus + " = Unknown at " + batteryLevel + " %");
                Toast.makeText(MainActivity.this, currentBatteryStatus + " = Unknown at " + batteryLevel + " %", Toast.LENGTH_SHORT).show();
            }
            if (deviceStatus == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
                //textview.setText(currentBatteryStatus + " = Not Charging at " + batteryLevel + " %");
                Toast.makeText(MainActivity.this, currentBatteryStatus + " = Not Charging at " + batteryLevel + " %", Toast.LENGTH_SHORT).show();
            }*/
            FWLogger.logInfo(TAG, "Inside_Broadcast_Call");

            //START GPS STATUS
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (isGpsEnabled) {
                //location is enabled
                isGPSEnabledByUser = true;
            } else {
                //location is disabled
                isGPSEnabledByUser = false;
            }

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int batteryLevel = (int) (((float) level / (float) scale) * 100.0f);
//            isGPSEnabled = gpsManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // CALL GPS SERVICE : startService(new Intent(this, GPSStatusUpdate.class));
            setupAlarm(900, batteryLevel, isGPSEnabledByUser); // 15min
        }
    };

    private void setupAlarm(int interval, int batteryStatus, boolean isGPSStatus) {
        try {
            FWLogger.logInfo(TAG, "Home_Battery_Status : " + batteryStatus + " Home_GPS_Status : " + isGPSStatus);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Intent i = new Intent(getBaseContext(), AlarmReceiver.class);
            i.putExtra("BatteryStatus", batteryStatus);
            i.putExtra("GPSStatus", isGPSStatus);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(HomeActivityNew.this, ALARM_REQUEST_CODE, i, PendingIntent.FLAG_MUTABLE);

            Calendar t = Calendar.getInstance();
            t.setTimeInMillis(System.currentTimeMillis());

//            int interval = seconds * 1000;
            // am.setRepeating(AlarmManager.RTC_WAKEUP, t.getTimeInMillis(), interval, pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC, t.getTimeInMillis(), interval, pendingIntent);

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Alarm_Exception" + ex.getMessage());
        }
    }


    public void OnDemandAppGuideForTech() {

        TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_task), getString(R.string.app_tour_task_title_for_tech), getString(R.string.app_tour_task_desc_for_tech)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        final View view = mToolbar.getChildAt(1);
        TapTarget target2 = TapTarget.forView(view, getString(R.string.manage_issued_item_expenditure_tech), getString(R.string.manage_issued_item_expenditure_tech_desc)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target3 = TapTarget.forToolbarMenuItem(mToolbar, R.id.addons, getString(R.string.addon_title), getString(R.string.addon_desc_for_tech)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);

        TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
        sequence.targets(target1, target2, target3);

        sequence.listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                onDemandAppTourFlag = false;
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {
                // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
            }
        });
        sequence.start();
    }

    public void AppGuideForTech_Skippable() {

        TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_task), getString(R.string.app_tour_task_title_for_tech), getString(R.string.app_tour_task_desc_for_tech)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        final View view = mToolbar.getChildAt(1);
        TapTarget target2 = TapTarget.forView(view, getString(R.string.manage_issued_item_expenditure_tech), getString(R.string.manage_issued_item_expenditure_tech_desc)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target3 = TapTarget.forToolbarMenuItem(mToolbar, R.id.addons, getString(R.string.addon_title), getString(R.string.addon_desc_for_tech)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);


        TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
        sequence.targets(target1, target2, target3);

        sequence.listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                //Toast.makeText(HomeActivityNew.this, "App Tour Completed Successfully.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {
                SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
            }
        });
        sequence.start();
    }


    public void AppGuideForOwner_Skippable() {

        TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_task), getString(R.string.app_tour_manage_task_title_owner), getString(R.string.app_tour_manage_task_desc_owner)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target2 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_crm), getString(R.string.manage_crm), getString(R.string.crm_desc)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target3 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_tech), getString(R.string.manage_amc), getString(R.string.manage_amc_desc)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        final View view = mToolbar.getChildAt(1);
        TapTarget target4 = TapTarget.forView(view, getString(R.string.manage_expense_assigned_items), getString(R.string.manage_expense_assigned_items_desc)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target5 = TapTarget.forToolbarMenuItem(mToolbar, R.id.addons, getString(R.string.addon_title), getString(R.string.addon_desc_for_owner)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);

        TapTarget target6 = TapTarget.forToolbarMenuItem(mToolbar, R.id.help, getString(R.string.help_and_support_tour), getString(R.string.help_and_support_tour)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);

        TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
        sequence.targets(target1, target2, target3, target4, target5, target6);

        sequence.listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {
                new OwnerDashboardFragment().OwnerAppTour_Skippable(HomeActivityNew.this);
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {
                // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
            }
        });
        sequence.start();
    }

    public void AppGuideForOwner_NonSkippable() {

        TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_task), getString(R.string.app_tour_manage_task_title_owner), getString(R.string.app_tour_manage_task_desc_owner)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target2 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_crm), getString(R.string.manage_crm), getString(R.string.crm_desc)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target3 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.navigation_tech), getString(R.string.manage_amc), getString(R.string.manage_amc_desc)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        final View view = mToolbar.getChildAt(1);
        TapTarget target4 = TapTarget.forView(view, getString(R.string.manage_expense_assigned_items), getString(R.string.manage_expense_assigned_items_desc)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

        TapTarget target5 = TapTarget.forToolbarMenuItem(mToolbar, R.id.addons, getString(R.string.addon_title), getString(R.string.addon_desc_for_owner)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);

        TapTarget target6 = TapTarget.forToolbarMenuItem(mToolbar, R.id.help, getString(R.string.help_and_support_tour), getString(R.string.help_and_support_tour)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(30);

        TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
        sequence.targets(target1, target2, target3, target4, target5, target6);

        sequence.listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {

                new OwnerDashboardFragment().OwnerAppTour_NonSkippable(HomeActivityNew.this);
                SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {
                // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
            }
        });
        sequence.start();
    }


    public void OwnerAppTourSkippable() {
        try {
            OwnerAppTourFlag = "SKIPABLE";
            TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.placeholder), getString(R.string.add_tech_create_task), getString(R.string.add_tech_create_task_desc)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

            TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
            sequence.targets(target1);

            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    //final View view = mToolbar.getChildAt(1);
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        CommonDialog commonDialog = CommonDialog.getInstance();
                        commonDialog.addOptionsDialog(HomeActivityNew.this);
                    }
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                    AddTechDialog(HomeActivityNew.this);
                }
            });
            sequence.start();

        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    public void OwnerAppTour() {
        try {
            OwnerAppTourFlag = "";
            TapTarget target1 = TapTarget.forView(HomeActivityNew.this.findViewById(R.id.placeholder), getString(R.string.add_tech_create_task), getString(R.string.add_tech_create_task_desc)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(60);

            TapTargetSequence sequence = new TapTargetSequence(HomeActivityNew.this);
            sequence.targets(target1);

            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    //final View view = mToolbar.getChildAt(1);
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup() != null && SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        CommonDialog commonDialog = CommonDialog.getInstance();
                        commonDialog.addOptionsDialog(HomeActivityNew.this);
                    }
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public boolean AudioPermission() {
        boolean audioFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkMicroPhone = ContextCompat.checkSelfPermission(HomeActivityNew.this, Manifest.permission.RECORD_AUDIO);
                if (checkMicroPhone != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    audioFlag = true;
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return audioFlag;
    }

    // PLAYSTORE IN-APP RATINGS-------
    public void PlayStoreRatingDialog(Activity activity) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.play_store_ratings);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonRateNow, buttonNoThanks, buttonRemindLater;

        buttonRateNow = dialog.findViewById(R.id.button_rateNow);
        buttonNoThanks = dialog.findViewById(R.id.button_NoThanks);
        buttonRemindLater = dialog.findViewById(R.id.button_RemindLatr);

        buttonRateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName();
                try {
                    SharedPreferences prefs = HomeActivityNew.this.getSharedPreferences("apprater", 0);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                    //
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                } catch (ActivityNotFoundException exception) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                dialog.dismiss();
            }
        });

        buttonNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = HomeActivityNew.this.getSharedPreferences("apprater", 0);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("dontshowagain", true);
                editor.commit();
                dialog.dismiss();
            }
        });

        buttonRemindLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // ADD TECH : ONLY ONCE IF TECH SIZE IS ZERO
/*
    public void AddTechDialog(Activity activity) {
        try {
            if (mUsersNameList.size() == 0) {
                FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.add_tech_only_once);
                dialog.getWindow().setGravity(Gravity.BOTTOM);

                TextInputLayout txtMobileLayout, txtEmailLayout;
                EditText mEditTextFirstName, mEditTextLastName, mEditTextPhoneNumber, meditTextEmailid;
                ImageButton closeAddTech;

                txtMobileLayout = dialog.findViewById(R.id.txtMobileLayout);
                txtEmailLayout = dialog.findViewById(R.id.txtEmailLayout);
                closeAddTech = dialog.findViewById(R.id.closeAddTech);
                mEditTextFirstName = dialog.findViewById(R.id.editText_first_name);
                mEditTextLastName = dialog.findViewById(R.id.editText_last_name);
                mEditTextPhoneNumber = dialog.findViewById(R.id.editText_phone_number);
                meditTextEmailid = dialog.findViewById(R.id.editText_Emailid);
                mTextViewValidate = dialog.findViewById(R.id.textView_validate);
                mTextViewSubmit = dialog.findViewById(R.id.textView_submit);

                try {
                    if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                        txtMobileLayout.setVisibility(View.VISIBLE);
                        txtEmailLayout.setVisibility(View.GONE);
                    } else {
                        txtEmailLayout.setVisibility(View.VISIBLE);
                        txtMobileLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

                closeAddTech.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                mEditTextFirstName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (s.toString().contains("@")) {
                                mEditTextFirstName.setText("");
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);
                    }
                });

                mEditTextLastName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (s.toString().contains("@")) {
                                mEditTextFirstName.setText("");
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);
                    }
                });

                // INDIAN -- Username blank
                // NRI --- Username = EMail
                mEditTextPhoneNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        enableValidate(mTextViewValidate);
                    }
                });

                meditTextEmailid.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);

                    }
                });

                mTextViewValidate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {

                                if (mEditTextFirstName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextFirstName.setError(getResources().getString(R.string.first_name));
                                } else if (mEditTextLastName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextLastName.setError(getResources().getString(R.string.last_name));
                                } else if (mEditTextPhoneNumber.getText().toString().isEmpty() || CommonFunction.isValidPhone(mEditTextPhoneNumber)) {
                                    isValidateSuccess = false;
                                    mEditTextPhoneNumber.setError(getResources().getString(R.string.mobile_number));
                                }
                                //Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.invalid_data), Toast.LENGTH_LONG).show();
                                else {
                                    isValidateSuccess = true;
                                    try {
                                        mTechLists = new ArrayList<>();
                                        techObj = new AddBulkTech.ResultData();
                                        techObj.setFirstName(mEditTextFirstName.getText().toString());
                                        techObj.setLastName(mEditTextLastName.getText().toString());
                                        techObj.setCreatedBy(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                        techObj.setUserCountryCode(SharedPrefManager.getInstance(HomeActivityNew.this).getCountryDetailsID());

                                        // i.e FOR INDIA : MOBILE NO.
                                        techObj.setContactNo(mEditTextPhoneNumber.getText().toString());
                                        techObj.setUserName("");

                                        mTechLists.add(0, techObj);
                                        AddTechValidate(mTechLists);
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }
                                }
                            } else {
                                if (mEditTextFirstName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextFirstName.setError(getResources().getString(R.string.first_name));
                                } else if (mEditTextLastName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextLastName.setError(getResources().getString(R.string.last_name));
                                } else if (meditTextEmailid.getText().toString().isEmpty() */
    /*|| CommonFunction.isValidPhone(mEditTextPhoneNumber)*//*
) {
                                    isValidateSuccess = false;
                                    meditTextEmailid.setError(getResources().getString(R.string.email_id));
                                } else {
                                    isValidateSuccess = true;
                                    try {
                                        mTechLists = new ArrayList<>();
                                        techObj = new AddBulkTech.ResultData();
                                        techObj.setFirstName(mEditTextFirstName.getText().toString());
                                        techObj.setLastName(mEditTextLastName.getText().toString());
                                        techObj.setCreatedBy(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                        techObj.setUserCountryCode(SharedPrefManager.getInstance(HomeActivityNew.this).getCountryDetailsID());

                                        techObj.setEmail(meditTextEmailid.getText().toString());
                                        techObj.setUserName(meditTextEmailid.getText().toString());

                                        mTechLists.add(0, techObj);
                                        AddTechValidate(mTechLists);
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }
                                }
                            }
                        } catch (
                                Exception ex) {
                            ex.getMessage();
                        }
                    }
                });

                mTextViewSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            AddTechSubmit(mTechLists);
                            dialog.dismiss();
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                    }
                });
                dialog.show();
            }
        } catch (
                Exception e) {
            e.getMessage();
        }

    }
*/

    public void AddTechDialog(Activity activity) {
        try {
            if (mUsersNameList.size() == 0) {
                FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.add_tech_only_once);
                dialog.getWindow().setGravity(Gravity.BOTTOM);

                TextInputLayout txtMobileLayout, txtEmailLayout;
                EditText mEditTextFirstName, mEditTextLastName, mEditTextPhoneNumber, meditTextEmailid;
                ImageButton closeAddTech;

                txtMobileLayout = dialog.findViewById(R.id.txtMobileLayout);
                txtEmailLayout = dialog.findViewById(R.id.txtEmailLayout);
                closeAddTech = dialog.findViewById(R.id.closeAddTech);
                mEditTextFirstName = dialog.findViewById(R.id.editText_first_name);
                mEditTextLastName = dialog.findViewById(R.id.editText_last_name);
                mEditTextPhoneNumber = dialog.findViewById(R.id.editText_phone_number);
                meditTextEmailid = dialog.findViewById(R.id.editText_Emailid);
                mTextViewValidate = dialog.findViewById(R.id.textView_validate);
                mTextViewSubmit = dialog.findViewById(R.id.textView_submit);

                try {
                    if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                        txtMobileLayout.setVisibility(View.VISIBLE);
                        txtEmailLayout.setVisibility(View.GONE);
                    } else {
                        txtEmailLayout.setVisibility(View.VISIBLE);
                        txtMobileLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

                closeAddTech.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                mEditTextFirstName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (s.toString().contains("@")) {
                                mEditTextFirstName.setText("");
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);
                    }
                });

                mEditTextLastName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (s.toString().contains("@")) {
                                mEditTextFirstName.setText("");
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);
                    }
                });

                // INDIAN -- Username blank
                // NRI --- Username = EMail
                mEditTextPhoneNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        enableValidate(mTextViewValidate);
                    }
                });

                meditTextEmailid.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        enableValidate(mTextViewValidate);

                    }
                });

                mTextViewValidate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {

                                if (mEditTextFirstName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextFirstName.setError(getResources().getString(R.string.first_name));
                                } else if (mEditTextLastName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextLastName.setError(getResources().getString(R.string.last_name));
                                } else if (mEditTextPhoneNumber.getText().toString().isEmpty() || CommonFunction.isValidPhone(mEditTextPhoneNumber)) {
                                    isValidateSuccess = false;
                                    mEditTextPhoneNumber.setError(getResources().getString(R.string.mobile_number));
                                }
                                //Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.invalid_data), Toast.LENGTH_LONG).show();
                                else {
                                    isValidateSuccess = true;
                                    try {
                                        mTechLists = new ArrayList<>();
                                        techObj = new AddBulkTech.ResultData();
                                        techObj.setFirstName(mEditTextFirstName.getText().toString());
                                        techObj.setLastName(mEditTextLastName.getText().toString());
                                        techObj.setCreatedBy(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                        techObj.setUserCountryCode(SharedPrefManager.getInstance(HomeActivityNew.this).getCountryDetailsID());

                                        // i.e FOR INDIA : MOBILE NO.
                                        techObj.setContactNo(mEditTextPhoneNumber.getText().toString());
                                        techObj.setUserName("");

                                        mTechLists.add(0, techObj);
                                        AddTechValidate(mTechLists);
                                        dialog.dismiss();
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }
                                }
                            } else {
                                if (mEditTextFirstName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextFirstName.setError(getResources().getString(R.string.first_name));
                                } else if (mEditTextLastName.getText().toString().isEmpty()) {
                                    isValidateSuccess = false;
                                    mEditTextLastName.setError(getResources().getString(R.string.last_name));
                                } else if (meditTextEmailid.getText().toString().isEmpty() /*|| CommonFunction.isValidPhone(mEditTextPhoneNumber)*/) {
                                    isValidateSuccess = false;
                                    meditTextEmailid.setError(getResources().getString(R.string.email_id));
                                } else {
                                    isValidateSuccess = true;
                                    try {
                                        mTechLists = new ArrayList<>();
                                        techObj = new AddBulkTech.ResultData();
                                        techObj.setFirstName(mEditTextFirstName.getText().toString());
                                        techObj.setLastName(mEditTextLastName.getText().toString());
                                        techObj.setCreatedBy(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                        techObj.setUserCountryCode(SharedPrefManager.getInstance(HomeActivityNew.this).getCountryDetailsID());

                                        techObj.setEmail(meditTextEmailid.getText().toString());
                                        techObj.setUserName(meditTextEmailid.getText().toString());

                                        mTechLists.add(0, techObj);
                                        AddTechValidate(mTechLists);
                                        dialog.dismiss();
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                    }
                });

                /*mTextViewSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            AddTechSubmit(mTechLists);
                            dialog.dismiss();
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                    }
                });*/
                dialog.show();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }


    public void DeleteOwnerSelfDialog(Context context) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_own_account);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button btnDeleteSelfacc, btnNotYet;

        btnDeleteSelfacc = dialog.findViewById(R.id.btnDeleteSelfacc);
        btnNotYet = dialog.findViewById(R.id.btnNotYet);

        btnDeleteSelfacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteOwnAccount(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId(), HomeActivityNew.this);
                dialog.dismiss();
            }
        });

        btnNotYet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void DeleteOwnAccount(int userID, Context mcontext) {
        /*mDeleteSelfAccAsyncTask = new DeleteSelf_TechAccAsyncTask(mcontext, BaseAsyncTask.Priority.LOW, this);
        mDeleteSelfAccAsyncTask.execute(userID);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                Call<DeleteOwnerTechnician> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().getDeleteOwnAccount(userID);
                call.enqueue(new retrofit2.Callback<DeleteOwnerTechnician>() {
                    @Override
                    public void onResponse(Call<DeleteOwnerTechnician> call, Response<DeleteOwnerTechnician> response) {
                        try {
                            if (response.code() == 200) {
                                DeleteOwnerTechnician mDeleteTech = response.body();

                                if (mDeleteTech.getCode().equalsIgnoreCase("200") && mDeleteTech.getMessage().equalsIgnoreCase("Your account has been deleted.")) {
                                    FWLogger.logInfo(TAG, "Your account has been deleted.");
                                    Toast.makeText(HomeActivityNew.this, mDeleteTech.getMessage(), Toast.LENGTH_SHORT).show();
                                    logoutAndNavigateToLogin();
                                } else {
                                    Toast.makeText(HomeActivityNew.this, "Unable to delete Account", Toast.LENGTH_SHORT).show();

                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteOwnerTechnician> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteLoginCredentials? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteLoginCredentials? API:");
            ex.getMessage();
        }

    }

    private void AddTechValidate(List<AddBulkTech.ResultData> mTechLists) {
        try {
            if (isValidateSuccess) {
                if (mTechLists.size() > 0) {
                    /*mAddBulkTechValidationAsyncTask = new AddBulkTechValidationAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
                    mAddBulkTechValidationAsyncTask.execute(mTechLists);*/

                    // ADD TECH VALIDATION
                    try {
                        if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                            Call<AddBulkTech> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().addBulkTechValidation(mTechLists, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                            call.enqueue(new retrofit2.Callback<AddBulkTech>() {
                                @Override
                                public void onResponse(Call<AddBulkTech> call, Response<AddBulkTech> response) {
                                    try {
                                        if (response.code() == 200) {
                                            AddBulkTech addBulkTech = response.body();
                                            if (addBulkTech != null) {
                                                FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                                                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, HomeActivityNew.this))) {
                                                    Toast.makeText(HomeActivityNew.this, getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                                                    disableValidate(mTextViewValidate);
                                                    enableSubmit(mTextViewSubmit);
                                                } else {
                                                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, HomeActivityNew.this))) {
                                                        //NOTE: Log GA event
                                                        Bundle bundle = new Bundle();
                                                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                                        bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                        FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                                                        Toast.makeText(HomeActivityNew.this, getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                                                        //((HomeActivityNew) HomeActivityNew.this).navigateToTab(R.id.navigation_home);
                                                        fromWhere = "FIRST_ADD_TECH";
                                                        getUserList();
                                                    } else {
                                                        if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, HomeActivityNew.this))) {
                                                            Toast.makeText(HomeActivityNew.this, getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                                                        } else {
                                                            Toast.makeText(HomeActivityNew.this, addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                                                        }
                                                        disableSubmit(mTextViewSubmit);
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.getMessage();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddBulkTech> call, Throwable throwable) {
                                    FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                                }
                            });
                        } else {
                            ServiceDialog serviceDialog = ServiceDialog.getInstance();
                            serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
                        }

                    } catch (Exception ex) {
                        FWLogger.logInfo(TAG, "Exception in AddBulkUsersValidation API:");
                        ex.getMessage();
                    }

                } else {
                    Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // ADD BULK TECH
        try {
            if (mTechLists.size() > 0) {
               /* mAddBulkTechAsyncTask = new AddBulkTechAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
                mAddBulkTechAsyncTask.execute(mTechLists);*/
                try {
                    if (Connectivity.isNetworkAvailableRetro(HomeActivityNew.this)) {
                        Call<AddBulkTech> call = RetrofitClient.getInstance(HomeActivityNew.this).getMyApi().addBulkTech(mTechLists, "Bearer " + SharedPrefManager.getInstance(HomeActivityNew.this).getUserToken());
                        call.enqueue(new retrofit2.Callback<AddBulkTech>() {
                            @Override
                            public void onResponse(Call<AddBulkTech> call, Response<AddBulkTech> response) {
                                try {
                                    if (response.code() == 200) {
                                        AddBulkTech addBulkTech = response.body();
                                        if (addBulkTech != null) {
                                            FWLogger.logInfo(TAG, "Result : " + addBulkTech.getCode() + "   " + addBulkTech.getMessage());
                                            if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_successfully, HomeActivityNew.this))) {
                                                Toast.makeText(HomeActivityNew.this, getString(R.string.validation_successfully), Toast.LENGTH_LONG).show();
                                                disableValidate(mTextViewValidate);
                                                enableSubmit(mTextViewSubmit);
                                            } else {
                                                if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.tech_added_successfully, HomeActivityNew.this))) {
                                                    //NOTE: Log GA event
                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());
                                                    bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_TECH, addBulkTech.getResultData().size());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(HomeActivityNew.this).getUserGroup());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                    FirebaseAnalytics.getInstance(HomeActivityNew.this).logEvent(FirebaseGoogleAnalytics.Event.ADD_BULK_TECH, bundle);
                                                    Toast.makeText(HomeActivityNew.this, getString(R.string.tech_added_successfully), Toast.LENGTH_LONG).show();
                                                    //((HomeActivityNew) HomeActivityNew.this).navigateToTab(R.id.navigation_home);
                                                    fromWhere = "FIRST_ADD_TECH";
                                                    getUserList();
                                                } else {
                                                    if (addBulkTech.getCode().equalsIgnoreCase("200") && addBulkTech.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, HomeActivityNew.this))) {
                                                        Toast.makeText(HomeActivityNew.this, getString(R.string.validation_failed), Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Toast.makeText(HomeActivityNew.this, addBulkTech.getMessage(), Toast.LENGTH_LONG).show();
                                                    }
                                                    disableSubmit(mTextViewSubmit);
                                                }
                                            }
                                        }

                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                            }

                            @Override
                            public void onFailure(Call<AddBulkTech> call, Throwable throwable) {
                                FWLogger.logInfo(TAG, "Exception in AddBulkUsersForMobile API:");
                            }
                        });
                    } else {
                        ServiceDialog serviceDialog = ServiceDialog.getInstance();
                        serviceDialog.noConnectionDialogRetro(HomeActivityNew.this);
                    }

                } catch (Exception ex) {
                    FWLogger.logInfo(TAG, "Exception in AddBulkUsersForMobile API:");
                    ex.getMessage();
                }

            } else {
                Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

    }


    /*private void AddTechValidate(List<AddBulkTech.ResultData> mTechLists) {
        try {
            if (isValidateSuccess) {
                if (mTechLists.size() > 0) {
                    mAddBulkTechValidationAsyncTask = new AddBulkTechValidationAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
                    mAddBulkTechValidationAsyncTask.execute(mTechLists);
                } else {
                    Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }*/

   /* private void AddTechSubmit(List<AddBulkTech.ResultData> mTechLists) {
        try {
            if (mTechLists.size() > 0) {
                mAddBulkTechAsyncTask = new AddBulkTechAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
                mAddBulkTechAsyncTask.execute(mTechLists);
            } else {
                Toast.makeText(HomeActivityNew.this, getResources().getString(R.string.can_not_add_tech), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }*/


    public void setupAlarm(int interval) {
        try {
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            Intent i = new Intent(getBaseContext(), GoogleLocAlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(HomeActivityNew.this, 1235, i, PendingIntent.FLAG_MUTABLE);

            Calendar t = Calendar.getInstance();
            t.setTimeInMillis(System.currentTimeMillis());

            am.setInexactRepeating(AlarmManager.RTC, t.getTimeInMillis(), interval, pendingIntent);

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void cancelAlarm() {
        try {
            Intent intent = new Intent(getBaseContext(), GoogleLocAlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1235, intent, PendingIntent.FLAG_MUTABLE);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    public void disableSubmit(TextView mTextViewSubmit) {
        mTextViewSubmit.setEnabled(false);
        mTextViewSubmit.setBackgroundResource(R.drawable.rounded_button_gray);
    }

    public void enableSubmit(TextView mTextViewSubmit) {
        mTextViewSubmit.setEnabled(true);
        mTextViewSubmit.setBackgroundResource(R.drawable.rounded_button);
    }

    public void enableValidate(TextView mTextViewValidate) {
        mTextViewValidate.setEnabled(true);
        mTextViewValidate.setBackgroundResource(R.drawable.rounded_button);
    }

    public void disableValidate(TextView mTextViewValidate) {
        mTextViewValidate.setEnabled(false);
        mTextViewValidate.setBackgroundResource(R.drawable.rounded_button_gray);
    }

    // START : INTERCOM START
    private void RegisterUserFor_Intercom() {
        try {
            //settings.setValue(Settings.LAST_EMAIL, email);
            String userID = SharedPrefManager.getInstance(getApplicationContext()).getMOBILE_OR_EMAIL();
            String userFirstName = SharedPrefManager.getInstance(getApplicationContext()).getUserFirstName();
            settings.setValue(Settings.LAST_USER_ID, userID);
            //
            Registration registration = new Registration();
            String data = userID;
            UserAttributes userAttributes;
           /* if (!name.equals("")) {
                userAttributes = new UserAttributes.Builder().withName(name).build();
                registration = registration.withUserAttributes(userAttributes);
            }*/
            /*if (!email.equals("")) registration = registration.withEmail(email);*/
            if (!userID.equals("") && !userID.contains("@")) {
                registration = registration.withUserId(userID);
                //
                userAttributes = new UserAttributes.Builder().withName(userFirstName).build();
                registration = registration.withUserAttributes(userAttributes);
                //
                data = userID;
            } else {
                registration = registration.withEmail(userID);
            }
            intercomCheckSecureMode(data);
            Intercom.client().registerIdentifiedUser(registration);

            // CALL SCREEN
            onClickMessengerShow();

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void initializeInterCom() {
        try {
            settings = new Settings(getApplicationContext());
            String app_id = getResources().getString(R.string.chat_support_app_id);/*settings.getValue(Settings.APP_ID);*/
            String sdk_api_key = getResources().getString(R.string.chat_support_key);/*settings.getValue(Settings.SDK_API_KEY);*/
            //Log.i(TAG, "Initialise Intercom. App ID: " + app_id + " / SDK API Key: " + sdk_api_key);
            Intercom.initialize(HomeActivityNew.this.getApplication(), sdk_api_key, app_id);
            Intercom.setLogLevel(Intercom.LogLevel.DEBUG);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void intercomCheckSecureMode(String data) {
        String secret = settings.getValue(Settings.SDK_SECURE_MODE_SECRET_KEY);
        Log.i(TAG, "Check secure mode. Data: " + data + " / Set Secure mode?: " + (!secret.equals("")));
        if (!secret.equals("")) {
            Intercom.client().setUserHash(generateHash(data, secret));
        }
    }

    public String generateHash(String message, String secret) {
        try {

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hash = (sha256_HMAC.doFinal(message.getBytes()));
            StringBuilder result = new StringBuilder();
            for (byte b : hash) {
                result.append(String.format("%02X", b));
            }
            return result.toString().toLowerCase(); // lower case needed for mobile
        } catch (Exception e) {
            System.out.println("Error");
        }
        return "";
    }

    public void onClickMessengerShow() {
        Intercom.client().displayMessenger();
    }

    public void onCreateInitialiseIntercom() {
        // main Intercom library initalisation done in the Custom Application file as per docs
        // https://docs.intercom.io/install-on-your-product-or-site/quick-install/install-intercom-on-your-android-app

        try {
            Intent intent = this.getIntent();
            if (intent != null) {
                String action = intent.getAction();
                String data = intent.getDataString();
                Bundle extras = intent.getExtras();
                if (extras == null) extras = new Bundle();
                Log.i(TAG, "Intent. action: " + action + " |data: " + data + "|extras:" + extras.toString());
            }

            Intercom.client().handlePushMessage();
        } catch (Exception e) {
            e.getMessage();
        }
    }
    // END : INTERCOM


    public void postData(List<TeleCMICall> teleCMICallArrayList) {
        try {
            teleCMIAsyncTask = new TeleCMIAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
            teleCMIAsyncTask.execute(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    //START OPEN PDF SOURCE

    public void openPdfFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select a PDF file"), 999);
    }

    private static void openPdfFile(Context context, File file) {
        Uri uri = Uri.fromFile(file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/pdf");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No PDF viewer app found", Toast.LENGTH_SHORT).show();
        }
    }

    /*public String getSelectedFileName() {
        return selectedFileName;
    }*/


    // END OPEN PDF SOURCE


}
