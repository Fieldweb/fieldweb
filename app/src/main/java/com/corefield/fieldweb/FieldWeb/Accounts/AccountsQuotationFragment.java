package com.corefield.fieldweb.FieldWeb.Accounts;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CRMTasksListAdapter;
import com.corefield.fieldweb.Adapter.InvoiceListAdapter;
import com.corefield.fieldweb.Adapter.QuotationListAdapter;
import com.corefield.fieldweb.Adapter.ServiceTypeListAdapter;
import com.corefield.fieldweb.Adapter.TasksListAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.AMC.AMCTypeList;
import com.corefield.fieldweb.DTO.Account.QuotationListDTO;
import com.corefield.fieldweb.DTO.Account.TaxDetails;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerItemInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show item inventory where Owner can add item and Assign Item
 */
public class AccountsQuotationFragment extends Fragment implements View.OnClickListener, RecyclerTouchListener {
    protected static String TAG = AccountsQuotationFragment.class.getSimpleName();
    public List<QuotationListDTO.ResultData> mQuotationList = null;
    public List<EnquiryServiceTypeDTO.ResultData> mEnquiryServiceList;
    public List<UsersList.ResultData> mUsersLists = null;
    private List mQuoteTypeLists = null;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    private View mRootView;
    ImageView noResultImg;
    private String mSearchParam = "";
    private boolean mSearchFilter = false;
    public List<CustomerList.ResultData> mCustomerList;
    private List<QuotationListDTO.ResultData> mQuoteList;

    private RecyclerView mRecyclerViewQuotationList;
    private QuotationListAdapter mquotationListAdapter;
    //private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager;
    private boolean mAllItemApiCall = false, mItemListApiCall = false, mUserListApiCall = false;
    private Button mAddQuote;
    private SearchView mSearchView;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;
    //
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START;
    private boolean isLoadFirstTime = false;
    private static final int MAX_FILE_SIZE = 6 * 1024 * 1024; // 6MB
    private static String selectedFileName;
    private List<TaxDetails.ResultData> mTaxDetailsArry;

    Spinner quoteSpinStatus;

    private int StatusId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.accounts_quotation_fragment, container, false);
        mAddQuote = mRootView.findViewById(R.id.plus_quote);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        noResultImg = mRootView.findViewById(R.id.noResultImg);
        quoteSpinStatus = mRootView.findViewById(R.id.spin_quote_type);
        recyclerTouchListener = this;
        fragment = this;

        mRecyclerViewQuotationList = mRootView.findViewById(R.id.recycler_quotation_list);

        mAddQuote.setOnClickListener(this);
        //getCustomerList();
        ((HomeActivityNew) getActivity()).getItemList();
        ((HomeActivityNew) getActivity()).getServiceType();
        isLoadFirstTime = true;
        //getQuotationList(mSearchParam, mCurrentPage, StatusId);
        getTaxDetails();

        // BY M
        AutoCompleteTextView search_text = (AutoCompleteTextView) mSearchView.findViewById(mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        search_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.list_title));
        search_text.setGravity(Gravity.CENTER_VERTICAL);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(10);
        search_text.setFilters(filterArray);
        mSearchView.setGravity(Gravity.CENTER_VERTICAL);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
            }
        });
        //
        setSearchFilter();

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewQuotationList.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //
        mRecyclerViewQuotationList.setLayoutManager(mLayoutManager);
        mRecyclerViewQuotationList.setItemAnimator(new DefaultItemAnimator());
        mquotationListAdapter = new QuotationListAdapter(getActivity(), mQuotationList, recyclerTouchListener);
        mRecyclerViewQuotationList.setAdapter(mquotationListAdapter);
        mquotationListAdapter.notifyDataSetChanged();
        mquotationListAdapter.setClickListener(recyclerTouchListener);
        //

        // SET SPINNER DATA
        String[] quoteValues = {"Select Status", "Not Assigned", "Assigned", "Task Completed", "Lost"};
        ArrayAdapter<String> quoteType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, quoteValues);
        quoteSpinStatus.setAdapter(quoteType);

        quoteSpinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Select Status")) {
                    if (mQuoteList != null) clearTaskList();
                    StatusId = 0;
                    mSearchView.setQuery("", false);
                    getQuotationList(mSearchParam, mCurrentPage, StatusId);
                } else if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Not Assigned")) {
                    if (mQuoteList != null) clearTaskList();
                    StatusId = 2;
                    mSearchView.setQuery("", false);
                    getQuotationList(mSearchParam, mCurrentPage, StatusId);
                } else if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Assigned")) {
                    if (mQuoteList != null) clearTaskList();
                    StatusId = 1;
                    mSearchView.setQuery("", false);
                    getQuotationList(mSearchParam, mCurrentPage, StatusId);
                } else if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Task Completed")) {
                    if (mQuoteList != null) clearTaskList();
                    StatusId = 5;
                    mSearchView.setQuery("", false);
                    getQuotationList(mSearchParam, mCurrentPage, StatusId);
                } else if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Lost")) {
                    if (mQuoteList != null) clearTaskList();
                    StatusId = 4;
                    mSearchView.setQuery("", false);
                    getQuotationList(mSearchParam, mCurrentPage, StatusId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mAddQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                    addUpdateServiceDialog.addQuote(getContext(), activity, ((HomeActivityNew) getActivity()).mCustomerList, ((HomeActivityNew) getActivity()).mServiceTypeResultData, ((HomeActivityNew) getActivity()).mItemsNameList, mTaxDetailsArry);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        return mRootView;
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        getQuotationList(mSearchParam, mCurrentPage, StatusId);
    }

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    public void clearTaskList() {

        int size = mQuoteList.size();
        mQuoteList.clear();
        mquotationListAdapter.notifyItemRangeRemoved(0, size);

        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        mSearchParam = "";

        mquotationListAdapter = new QuotationListAdapter(getActivity(), mQuoteList, recyclerTouchListener);
        mRecyclerViewQuotationList.setAdapter(mquotationListAdapter);
        mquotationListAdapter.notifyDataSetChanged();
        mquotationListAdapter.setClickListener(recyclerTouchListener);
    }


    public void getQuotationList(String mSearchParam, int mCurrentPage, int StatusId) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                //CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<QuotationListDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getQuotationList(userID, 10, mCurrentPage, StatusId, mSearchParam, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<QuotationListDTO>() {
                    @Override
                    public void onResponse(Call<QuotationListDTO> call, Response<QuotationListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                //CommonFunction.hideProgressDialog(getActivity());
                                mQuotationList = new ArrayList<>();
                                QuotationListDTO quotationListDTO = response.body();
                                mQuotationList = quotationListDTO.getResultData();
                                if (mSearchFilter && mQuotationList.size() == 0) {
                                    mquotationListAdapter.removeLoadingFooter();
                                    mRecyclerViewQuotationList.setVisibility(View.GONE);
                                    noResultImg.setVisibility(View.VISIBLE);
                                    noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                } else {
                                    setResults(quotationListDTO);
                                }
                                mSearchFilter = false;
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<QuotationListDTO> call, Throwable throwable) {
                        // CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in GetQuotationsSearchByParam API:");
                    }
                });
            } else {
                //CommonFunction.hideProgressDialog(getActivity());
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            // CommonFunction.hideProgressDialog(getActivity());
            FWLogger.logInfo(TAG, "Exception in GetQuotationsSearchByParam API:");
            ex.getMessage();
        }
    }

    public void getCustomerList() {
        /*mGetCustomerListAsyncTask = new GetCustomerListAsyncTask(HomeActivityNew.this, BaseAsyncTask.Priority.LOW, this);
        mGetCustomerListAsyncTask.execute(SharedPrefManager.getInstance(HomeActivityNew.this).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getActivity()).getUserId();
                Call<CustomerList> call = RetrofitClient.getInstance(getActivity()).getMyApi().getCustomerList(userID);
                call.enqueue(new retrofit2.Callback<CustomerList>() {
                    @Override
                    public void onResponse(Call<CustomerList> call, Response<CustomerList> response) {
                        try {
                            if (response.code() == 200) {
                                CustomerList customerList = response.body();
                                if (customerList.getCode().equalsIgnoreCase("200") && customerList.getMessage().equalsIgnoreCase("success")) {
                                    FWLogger.logInfo(TAG, "Check = " + customerList.getMessage());
                                    if (customerList != null) {
                                        mCustomerList = new ArrayList<>();
                                        mCustomerList = customerList.getResultData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllCustomerListForMobile API:");
            ex.getMessage();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
          /*  case R.id.button_inventory:
                break;

            case R.id.button_issued_item:
                break;*/

            default:
                break;
        }
    }

    private void refresh() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        AccountsQuotationFragment myFragment = new AccountsQuotationFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }


    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;

            case R.id.image_item_update:

                break;

            case R.id.image_issue_item_update:
                break;

            default:
                break;
        }
    }

    private void setSearchFilter() {
        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if (mquotationListAdapter != null) clearQuotList();
                mSearchParam = query;
                mSearchFilter = true;
                mockingNetworkDelay(query, 0, 0);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }


    private void mockingNetworkDelay(String searchParam, int typeId, int statusId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getQuotationList(mSearchParam, mCurrentPage, StatusId);
            }
        }, 1000);
    }

    private void setResults(QuotationListDTO tasksList) {
        mQuoteList = new ArrayList<>();
        mQuoteList = tasksList.getResultData();
        if (isLoadFirstTime) {
            if (mQuoteList == null || mQuoteList.isEmpty() || mQuoteList.size() <= 0) {
                mRecyclerViewQuotationList.setVisibility(View.GONE);
                noResultImg.setVisibility(View.VISIBLE);
                noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                isLoading = false;
                isLastPage = true;
            } else {
                mRecyclerViewQuotationList.setVisibility(View.VISIBLE);
                mquotationListAdapter.addAll(mQuoteList);
                mquotationListAdapter.notifyDataSetChanged();
                mquotationListAdapter.addLoadingFooter();
            }
        } else {
            if (mQuoteList == null || mQuoteList.isEmpty() || mQuoteList.size() <= 0) {
                mquotationListAdapter.removeLoadingFooter();
                isLoading = false;
                isLastPage = true;
            } else {
                mquotationListAdapter.removeLoadingFooter();
                isLoading = false;
                mquotationListAdapter.addAll(mQuoteList);
                mquotationListAdapter.notifyDataSetChanged();
                mquotationListAdapter.addLoadingFooter();
            }
        }
    }

    public void clearQuotList() {
        int size = mQuoteList.size();
        mQuoteList.clear();
        mquotationListAdapter.notifyItemRangeRemoved(0, size);

        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        mSearchParam = "";

        mquotationListAdapter = new QuotationListAdapter(getContext(), mQuotationList, recyclerTouchListener);
        mRecyclerViewQuotationList.setAdapter(mquotationListAdapter);
        mquotationListAdapter.setClickListener(recyclerTouchListener);
        mquotationListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            File file = new File(uri.getPath());

            if (file.exists() && file.length() <= MAX_FILE_SIZE) {
                selectedFileName = file.getName(); // Store the file name
                openPdfFile(getContext(), file);
            } else {
                Toast.makeText(getContext(), "Invalid PDF file or exceeds maximum size", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static void openPdfFile(Context context, File file) {
        Uri uri = Uri.fromFile(file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/pdf");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No PDF viewer app found", Toast.LENGTH_SHORT).show();
        }
    }

    public static String getSelectedFileName() {
        return selectedFileName;
    }

    // END OPEN PDF SOURCE

    // TAX DETAILS
    private void getTaxDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<TaxDetails> call = RetrofitClient.getInstance(getActivity()).getMyApi().getTaxDetails();
                call.enqueue(new retrofit2.Callback<TaxDetails>() {
                    @Override
                    public void onResponse(Call<TaxDetails> call, Response<TaxDetails> response) {
                        try {
                            if (response.code() == 200) {
                                TaxDetails taxDetails = response.body();
                                FWLogger.logInfo(TAG, "TaxDetails Successful");
                                if (taxDetails != null) {
                                    if (taxDetails.getCode().equalsIgnoreCase("200") && taxDetails.getMessage().equalsIgnoreCase("successfully.")) {
                                        mTaxDetailsArry = new ArrayList<>();
                                        mTaxDetailsArry = taxDetails.getResultData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TaxDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in gettaxlist API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetCountryList API:");
            ex.getMessage();
        }
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }

}
