package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

public class ServiceDialog {
    public boolean isAlertDialogShowing = false;
    private static ServiceDialog mServiceDialog;

    /**
     * The private constructor for the ServiceDialog Singleton class
     */
    private ServiceDialog() {
    }

    public static ServiceDialog getInstance() {
        //instantiate a new ServiceDialog if we didn't instantiate one yet
        if (mServiceDialog == null) {
            mServiceDialog = new ServiceDialog();
        }
        return mServiceDialog;
    }

    public void showNoGooglePlayServiceAvailableDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_alert_no_google_play_service);
        builder.setMessage(R.string.no_google_playservice_available);
        String positiveText = context.getString(R.string.close);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isAlertDialogShowing = false;
                        dialog.dismiss();
                        Activity activity = (Activity) context;
                        activity.finishAffinity();
                    }
                });

        AlertDialog dialog = builder.create();
        isAlertDialogShowing = true;
        dialog.show();
    }

    public void showRefreshInternetConnectionDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_alert_no_internet);
        builder.setMessage(R.string.msg_alert_no_internet);
        String positiveText = context.getString(R.string.btn_label_refresh);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(context).getUserId());
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(context).getUserGroup());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        FirebaseAnalytics.getInstance(context).logEvent(FirebaseGoogleAnalytics.Event.REFRESH_INTERNET, bundle);

                        //Block the Application Execution until user grants the permissions
                        if (((HomeActivityNew) context).isInternetConnectionAvailable()) {
                            ((HomeActivityNew) context).verifyLocationSettings();
                            isAlertDialogShowing = false;
                            dialog.dismiss();
                        } else showRefreshInternetConnectionDialog(context); //recursive call
                    }
                });

        AlertDialog dialog = builder.create();
        isAlertDialogShowing = true;
        dialog.show();
    }

    public void showGPSEnableDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_alert_enable_gps);
        builder.setMessage(R.string.title_alert_enable_gps_message);
        String positiveText = context.getString(R.string.yes);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(context).getUserId());
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(context).getUserGroup());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        FirebaseAnalytics.getInstance(context).logEvent(FirebaseGoogleAnalytics.Event.GPS_ENABLE, bundle);
                        dialogInterface.dismiss();
                        ((HomeActivityNew) context).startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), HomeActivityNew.LOCATION_REQUEST_CODE);
                        isAlertDialogShowing = false;
                    }
                });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        isAlertDialogShowing = true;
        dialog.show();
    }

    public void noConnectionDialog(Context context) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_no_connection);

        TextView textViewRefresh = dialog.findViewById(R.id.refresh);
        ImageButton imageButtonCancel = dialog.findViewById(R.id.image_button_cancel_no_connection);
        imageButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    isAlertDialogShowing = false;
                    dialog.dismiss();
                    Activity activity = (Activity) context;
                    activity.finishAffinity();
                }
            }
        });

        textViewRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Connectivity.isNetworkAvailable(context))) {
                    isAlertDialogShowing = false;
                    if (context instanceof HomeActivityNew)
                        ((HomeActivityNew) context).verifyLocationSettings();
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, R.string.please_connect_internet_and_try_again, Toast.LENGTH_SHORT).show();
                }
            }
        });
        isAlertDialogShowing = true;
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public void noConnectionDialogRetro(Context context) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_no_connection);

        TextView textViewRefresh = dialog.findViewById(R.id.refresh);
        ImageButton imageButtonCancel = dialog.findViewById(R.id.image_button_cancel_no_connection);
        imageButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    isAlertDialogShowing = false;
                    dialog.dismiss();
                    Activity activity = (Activity) context;
                    activity.finishAffinity();
                }
            }
        });

        textViewRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Connectivity.isNetworkAvailable(context))) {
                    isAlertDialogShowing = false;
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, R.string.please_connect_internet_and_try_again, Toast.LENGTH_SHORT).show();
                }
            }
        });
        isAlertDialogShowing = true;
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

}
