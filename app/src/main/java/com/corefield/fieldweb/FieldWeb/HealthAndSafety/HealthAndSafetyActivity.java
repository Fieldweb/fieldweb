package com.corefield.fieldweb.FieldWeb.HealthAndSafety;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.corefield.fieldweb.Adapter.ArogyaSetuSpinnerAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.HealthAndSaftey.AddHealthAndSafetyStatus;
import com.corefield.fieldweb.DTO.HealthAndSaftey.ArogyaSetuStatusList;
import com.corefield.fieldweb.DTO.HealthAndSaftey.BodyTempUnitType;
import com.corefield.fieldweb.DTO.HealthAndSaftey.GetHealthAndSafetyStatus;
import com.corefield.fieldweb.DTO.HealthAndSaftey.UpdateHealthAndSafetyStatus;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddHealthStatusAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.GetArogyaSetuStatusListAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.GetBodyTempUnitTypeAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.GetHealthAndSafetyStatusAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateHealthStatusAsyncTask;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.ViewUtils;
import com.google.gson.Gson;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

/**
 * //
 * Created by CFS on 5/18/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.HealthAndSafety
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This activity is used to dynamically show and add the health and safety status
 * (runs based on backend response of feature enable true=show, false = don't show)
 * //
 **/
public class HealthAndSafetyActivity extends Activity implements View.OnClickListener, OnTaskCompleteListener, AdapterView.OnItemSelectedListener {

    private String TAG = HealthAndSafetyActivity.class.getSimpleName();
    private boolean isFromOptionMenu;
    private TextView mTextViewTempUnit, mTextViewClose;
    private EditText mEditTextTemp;
    private ImageView mImageViewHealthAndSafety;
    private Button mButtonSubmit;
    private Spinner mSpinnerArogyaSetu;
    private GetBodyTempUnitTypeAsyncTask mGetBodyTempUnitTypeAsyncTask;
    private GetArogyaSetuStatusListAsyncTask mGetArogyaSetuStatusListAsyncTask;
    private AddHealthStatusAsyncTask mAddHealthStatusAsyncTask;
    private UpdateHealthStatusAsyncTask mUpdateHealthStatusAsyncTask;
    private GetHealthAndSafetyStatusAsyncTask mGetHealthAndSafetyStatusAsyncTask;
    private List<BodyTempUnitType.ResultData> mBodyTempUnitTypeList;
    private List<ArogyaSetuStatusList.ResultData> mArogyaSetuStatusList;
    private ArogyaSetuSpinnerAdapter mArogyaSetuSpinnerAdapter;
    private int mSelectedStatusId = 0;
    private String mSelctedStatus = "";
    private String mSelectStatus = "";
    private AddHealthAndSafetyStatus.ResultData mHealthAndSafetyStatus = new AddHealthAndSafetyStatus.ResultData();
    private UpdateHealthAndSafetyStatus.ResultData mUpdateHealthAndSafetyStatus = new UpdateHealthAndSafetyStatus.ResultData();
    private int mGetHASStatusId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_and_safety);
        if (getIntent().getExtras() != null) {
            isFromOptionMenu = getIntent().getExtras().getBoolean("isFromOptionMenu");
        }
        mSelectStatus = HealthAndSafetyActivity.this.getResources().getString(R.string.select_status);
        inIt();
        mEditTextTemp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                float tempValue = 0;
                if (s != null && !s.toString().equalsIgnoreCase("") && !s.equals("") && !isEmpty(mEditTextTemp)) {
                    try {
                        tempValue = Float.valueOf(s.toString());
                    } catch (Exception e) {
                        mEditTextTemp.setText("");
                        Toast.makeText(HealthAndSafetyActivity.this, getResources().getString(R.string.please_enter_valid_no), Toast.LENGTH_SHORT).show();
                    }
                }

                if (tempValue >= 96 && tempValue <= 100)
                    mEditTextTemp.setTextColor(ResourcesCompat.getColor(getResources(), R.color.task_done, null));
                else if (tempValue > 100)
                    mEditTextTemp.setTextColor(ResourcesCompat.getColor(getResources(), R.color.task_rejected, null));
                else
                    mEditTextTemp.setTextColor(ResourcesCompat.getColor(getResources(), R.color.light_gray, null));

            }
        });
        mSpinnerArogyaSetu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    ViewUtils.hideKeyboard(HealthAndSafetyActivity.this);
                return false;
            }
        });
    }

    private void inIt() {
        getGetBodyTempUnitType();
        getArogyaSetuStatusList();
        mImageViewHealthAndSafety = findViewById(R.id.imageView_HealthAndSafety);
        mTextViewClose = findViewById(R.id.textView_close_healthAndSafety);
        mTextViewTempUnit = findViewById(R.id.textView_body_temp_unit);
        mEditTextTemp = findViewById(R.id.edittext_body_temp);
        mButtonSubmit = findViewById(R.id.button_submit);
        mSpinnerArogyaSetu = findViewById(R.id.spinner_arogya_setu_status);
        mTextViewClose.setOnClickListener(this);
        mTextViewTempUnit.setOnClickListener(this);
        mButtonSubmit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_submit:
                if (isValidHealthStatus()) {
                    if (isFromOptionMenu) {
                        if (mUpdateHealthAndSafetyStatus != null) {
                            mUpdateHealthAndSafetyStatus.setId(mGetHASStatusId);
                            mUpdateHealthAndSafetyStatus.setUserId(SharedPrefManager.getInstance(HealthAndSafetyActivity.this).getUserId());
                            mUpdateHealthAndSafetyStatus.setAarogyaSetuStatusId(mSelectedStatusId);
                            mUpdateHealthAndSafetyStatus.setTemperature(Float.valueOf(mEditTextTemp.getText().toString()));
                            updateHealthStatus();
                        }
                    } else {
                        if (mHealthAndSafetyStatus != null) {
                            mHealthAndSafetyStatus.setUserId(SharedPrefManager.getInstance(HealthAndSafetyActivity.this).getUserId());
                            mHealthAndSafetyStatus.setAarogyaSetuStatusId(mSelectedStatusId);
                            mHealthAndSafetyStatus.setTemperature(Float.valueOf(mEditTextTemp.getText().toString()));
                            addHealthStatus();
                        }
                    }
                }
                break;
            case R.id.textView_close_healthAndSafety:
                closeHealthAndSafety();
                break;
            default:
                break;
        }
    }


    private boolean isValidHealthStatus() {
        if (mSelectedStatusId == 0 || mSelectedStatusId == 999) {
            ((TextView) mSpinnerArogyaSetu.getSelectedView()).setError(getString(R.string.please_select_arogyasetu_status));
            return false;
        } else if (isEmpty(mEditTextTemp)) {
            mEditTextTemp.setError(getString(R.string.please_enter_body_temperature));
            return false;
        } else if (!isEmpty(mEditTextTemp)) {
            float tempValue = Float.valueOf(mEditTextTemp.getText().toString());
            if(tempValue <= 87.8 || tempValue >= 107) {
                mEditTextTemp.setError(getString(R.string.please_enter_correct_value));
                return false;
            } else if (tempValue >= 87.8 && tempValue <= 95) {
                mEditTextTemp.setError(getString(R.string.you_might_be_suffering_from_hypothermia));
                return false;
            } else if (tempValue >= 99.5 && tempValue <= 104) {
                mEditTextTemp.setError(getString(R.string.you_might_be_suffering_from_hyperthermia));
                return false;
            } else if (tempValue >= 104 && tempValue <=  107) {
                mEditTextTemp.setError(getString(R.string.you_might_be_suffering_from_hyperpyrexia));
                return false;
            }
            return true;
        }
        return true;
    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);

    }

    private void navigateToHomeActivity() {
        if (!isFromOptionMenu) {
            Intent intent = new Intent(HealthAndSafetyActivity.this, HomeActivityNew.class);
            if (getIntent().getExtras() != null) {
                if (getIntent().getExtras().getString("NewNotification") != null) {
                    intent.putExtra("NewNotification", "NewNotification");
                }
            }
            startActivity(intent);
            finish();
            return;
        }
    }

    private void closeHealthAndSafety() {
        finish();
    }

    private void setHealthAndSafetyStatus(GetHealthAndSafetyStatus getHealthAndSafetyStatus) {
        if (mArogyaSetuSpinnerAdapter != null) {
            FWLogger.logInfo(TAG, "setHealthAndSafetyStatus ");
            int statusId = getHealthAndSafetyStatus.getResultData().get(getHealthAndSafetyStatus.getResultData().size() - 1).getAarogyaSetuStatusId();
            float temp = getHealthAndSafetyStatus.getResultData().get(getHealthAndSafetyStatus.getResultData().size() - 1).getTemperature();
            FWLogger.logInfo(TAG, "statusId = " + statusId);
            int index = 0;
            if (mArogyaSetuStatusList != null) {
                for (index = 0; index < mArogyaSetuStatusList.size(); index++) {
                    if (mArogyaSetuStatusList.get(index).getId() == statusId) break;
                }
            }
            FWLogger.logInfo(TAG, "Index = " + index);
            mSpinnerArogyaSetu.setSelection(index);
            mEditTextTemp.setText(String.format("%.2f", temp));
            mGetHASStatusId = getHealthAndSafetyStatus.getResultData().get(getHealthAndSafetyStatus.getResultData().size() - 1).getId();
        }
    }

    private void getGetBodyTempUnitType() {
        mGetBodyTempUnitTypeAsyncTask = new GetBodyTempUnitTypeAsyncTask(HealthAndSafetyActivity.this, BaseAsyncTask.Priority.LOW, this);
        mGetBodyTempUnitTypeAsyncTask.execute();
    }

    private void getArogyaSetuStatusList() {
        mGetArogyaSetuStatusListAsyncTask = new GetArogyaSetuStatusListAsyncTask(HealthAndSafetyActivity.this, BaseAsyncTask.Priority.LOW, this);
        mGetArogyaSetuStatusListAsyncTask.execute();
    }

    private void addHealthStatus() {
        mAddHealthStatusAsyncTask = new AddHealthStatusAsyncTask(HealthAndSafetyActivity.this, BaseAsyncTask.Priority.LOW/*, this*/);
        mAddHealthStatusAsyncTask.execute(mHealthAndSafetyStatus);
    }

    private void updateHealthStatus() {
        mUpdateHealthStatusAsyncTask = new UpdateHealthStatusAsyncTask(HealthAndSafetyActivity.this, BaseAsyncTask.Priority.LOW, this);
        mUpdateHealthStatusAsyncTask.execute(mUpdateHealthAndSafetyStatus);
    }

    private void getHealthAndSafetyStatus() {
        FWLogger.logInfo(TAG, "getHealthAndSafetyStatus async task call");
        mGetHealthAndSafetyStatusAsyncTask = new GetHealthAndSafetyStatusAsyncTask(HealthAndSafetyActivity.this, BaseAsyncTask.Priority.LOW, this);
        mGetHealthAndSafetyStatusAsyncTask.execute();
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        FWLogger.logInfo(TAG, "onTaskComplete");
        if (classType.equalsIgnoreCase(BodyTempUnitType.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "BodyTempUnitType");
            Gson gson = new Gson();
            BodyTempUnitType bodyTempUnitType = gson.fromJson(urlConnectionResponse.resultData, BodyTempUnitType.class);
            if (bodyTempUnitType != null) {
                if (bodyTempUnitType.getCode().equalsIgnoreCase("200") && bodyTempUnitType.getMessage().equalsIgnoreCase("successfully.")) {
                    mBodyTempUnitTypeList = bodyTempUnitType.getResultData();
                }
            }
        } else if (classType.equalsIgnoreCase(ArogyaSetuStatusList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "ArogyaSetuStatusList");
            Gson gson = new Gson();
            ArogyaSetuStatusList arogyaSetuStatusList = gson.fromJson(urlConnectionResponse.resultData, ArogyaSetuStatusList.class);
            if (arogyaSetuStatusList != null) {
                if (arogyaSetuStatusList.getCode().equalsIgnoreCase("200") && arogyaSetuStatusList.getMessage().equalsIgnoreCase("Successfully.")) {
                    mArogyaSetuStatusList = arogyaSetuStatusList.getResultData();
                    //Add this for hint
                    ArogyaSetuStatusList.ResultData hint = new ArogyaSetuStatusList.ResultData();
                    hint.setArogyaSetuStatus(mSelectStatus);
                    hint.setId(999);
                    mArogyaSetuStatusList.add(hint);
                    mArogyaSetuSpinnerAdapter = new ArogyaSetuSpinnerAdapter(HealthAndSafetyActivity.this, mArogyaSetuStatusList);
                    mSpinnerArogyaSetu.setAdapter(mArogyaSetuSpinnerAdapter);
                    mSpinnerArogyaSetu.setOnItemSelectedListener(this);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (isFromOptionMenu) getHealthAndSafetyStatus();
                            else mSpinnerArogyaSetu.setSelection(mArogyaSetuSpinnerAdapter.getCount());
                        }
                    });
                }
            }
        } else if (classType.equalsIgnoreCase(AddHealthAndSafetyStatus.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "AddHealthStatus");
            Gson gson = new Gson();
            AddHealthAndSafetyStatus healthAndSafetyStatus = gson.fromJson(urlConnectionResponse.resultData, AddHealthAndSafetyStatus.class);
            if (healthAndSafetyStatus != null) {
                if (healthAndSafetyStatus.getCode().equalsIgnoreCase("200") && healthAndSafetyStatus.getMessage().equalsIgnoreCase("Body Temperature added successfully.")) {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(true);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd"));
                    navigateToHomeActivity();
                } else {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                    Toast.makeText(HealthAndSafetyActivity.this, healthAndSafetyStatus.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (classType.equalsIgnoreCase(GetHealthAndSafetyStatus.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "GetHealthAndSafetyStatus");
            Gson gson = new Gson();
            GetHealthAndSafetyStatus getHealthAndSafetyStatus = gson.fromJson(urlConnectionResponse.resultData, GetHealthAndSafetyStatus.class);
            if (getHealthAndSafetyStatus != null) {
                if (getHealthAndSafetyStatus.getCode().equalsIgnoreCase("200") && getHealthAndSafetyStatus.getMessage().equalsIgnoreCase("Successfully.")) {
                    setHealthAndSafetyStatus(getHealthAndSafetyStatus);
                } else {
                    Toast.makeText(HealthAndSafetyActivity.this, getHealthAndSafetyStatus.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (classType.equalsIgnoreCase(UpdateHealthAndSafetyStatus.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "UpdateHealthAndSafetyStatus");
            Gson gson = new Gson();
            UpdateHealthAndSafetyStatus updateHealthAndSafetyStatus = gson.fromJson(urlConnectionResponse.resultData, UpdateHealthAndSafetyStatus.class);
            if (updateHealthAndSafetyStatus != null) {
                if (updateHealthAndSafetyStatus.getCode().equalsIgnoreCase("200") && updateHealthAndSafetyStatus.getMessage().equalsIgnoreCase("Successfully.")) {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(true);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd"));
                    closeHealthAndSafety();
                } else {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                    Toast.makeText(HealthAndSafetyActivity.this, updateHealthAndSafetyStatus.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mSelctedStatus = mArogyaSetuStatusList.get(position).getArogyaSetuStatus();
        mSelectedStatusId = mArogyaSetuStatusList.get(position).getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onDestroy() {

        if (mGetBodyTempUnitTypeAsyncTask != null) {
            mGetBodyTempUnitTypeAsyncTask.cancel(true);
        }
        if (mGetArogyaSetuStatusListAsyncTask != null) {
            mGetArogyaSetuStatusListAsyncTask.cancel(true);
        }
        if (mAddHealthStatusAsyncTask != null) {
            mAddHealthStatusAsyncTask.cancel(true);
        }
        if (mUpdateHealthStatusAsyncTask != null) {
            mUpdateHealthStatusAsyncTask.cancel(true);
        }
        if (mGetHealthAndSafetyStatusAsyncTask != null) {
            mGetHealthAndSafetyStatusAsyncTask.cancel(true);
        }
        super.onDestroy();
    }
}
