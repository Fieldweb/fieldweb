package com.corefield.fieldweb.FieldWeb;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.FWUser;
import com.corefield.fieldweb.DTO.HealthAndSaftey.HealthAndSafety;
import com.corefield.fieldweb.DTO.OTP;
import com.corefield.fieldweb.DTO.RegisterHere.TouchlessTempRegistration;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HealthAndSafety.HealthAndSafetyActivity;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Services.SmsBroadcastReceiver;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.ViewUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Response;

public class VerifyOTPActivity extends BaseActivity implements View.OnClickListener, OnOtpCompletionListener, OnTaskCompleteListener {

    private final String TAG = VerifyOTPActivity.class.getSimpleName();
    private ImageView backButton;
    private Button buttonVerify_otp;
    private OtpView mOtpView;
    public int counter = 0;
    private TextView mTextViewResendOTP, mTextViewTimer, mtxtOtpExpired;
    private int mOTP = 0;
    private static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private int mOTPEnteredByUser = 0;
    private String mUserGroup = "", intentUserOTP = "", MobileOrEmailid = "", FromWhichActivity = "";
    private int mOTPAttempt = 0, mTouchlessSignupId, mCountryDetailsId;
    private Handler mHandler;
    //private GetHealthAndSafetyFeatureAsyncTask mGetHealthAndSafetyFeatureAsyncTask;
    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FWLogger.logInfo(TAG, "onCreate");
        setContentView(R.layout.activity_mobile_no_verification_touchless);
        inIt();
    }


    private void inIt() {
        try {
            backButton = (ImageView) findViewById(R.id.backButton);
            buttonVerify_otp = (Button) findViewById(R.id.buttonVerify_otp);
            mOtpView = findViewById(R.id.otp_view);
            mTextViewResendOTP = findViewById(R.id.textview_resend_otp);
            mTextViewTimer = findViewById(R.id.textView_timer);
            mtxtOtpExpired = findViewById(R.id.txtOtpExpired);

            try {
                Intent iVal = getIntent();
                MobileOrEmailid = iVal.getStringExtra("UserID");
                intentUserOTP = iVal.getStringExtra("UserOTP");
                FromWhichActivity = iVal.getStringExtra("FromWhere");
                mOTP = Integer.parseInt(intentUserOTP);
                OTPExpired();
                SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(false);
                // FOR AUTOFILL OTP
                if (!MobileOrEmailid.contains("@")) {
                    startSmartUserConsent();
                    //new SmsBroadcastReceiver().setEditTextOTP(mOtpView);
                }
                //Toast.makeText(VerifyOTPActivity.this, "OTP IS::" + mOTP, Toast.LENGTH_LONG).show();
                if (FromWhichActivity.equalsIgnoreCase("FromRegisterHere")) {
                    mTouchlessSignupId = iVal.getIntExtra("TouchlessSignupId", 0);
                    mCountryDetailsId = iVal.getIntExtra("CountryDetailsId", 0);
                }
            } catch (Exception e) {
                e.getMessage();
            }

            backButton.setOnClickListener(this);
            buttonVerify_otp.setOnClickListener(this);
            mOtpView.setOtpCompletionListener(this);
            mTextViewResendOTP.setOnClickListener(this);

            mOtpView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (mtxtOtpExpired.getVisibility() == View.VISIBLE) {
                            mtxtOtpExpired.setVisibility(View.GONE);
                            mtxtOtpExpired.setText("");
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.getMessage();
        }
    }

   /* private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);
        } else {
            new SmsBroadcastReceiver().setEditTextOTP(mOtpView);
        }
    }*/


    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.buttonVerify_otp:
                FWLogger.logInfo(TAG, "Button verify OTP");
                verifyOtp();
                break;
            case R.id.backButton:
                SharedPrefManager.getInstance(VerifyOTPActivity.this).logout(false);
                Intent intent = new Intent(VerifyOTPActivity.this, LoginTouchlessActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.textview_resend_otp:
                FWLogger.logInfo(TAG, "Text Resend clicked");
                resendOtp();
                break;

            default:
                break;
        }
    }


    private void verifyOtp() {
        FWLogger.logInfo(TAG, "verifyOtp");
        if (mOTP != 0 && mOTPEnteredByUser != 0 && mUserGroup != null && !MobileOrEmailid.equalsIgnoreCase("")) {
            if (mOTP == mOTPEnteredByUser) {
                FWLogger.logInfo(TAG, "verifyOtp verified");
                //NOTE: Log GA event
                // SET OTP VERIFIED FLAG
                SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(true);
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                if (MobileOrEmailid.contains("@")) {
                    bundle.putString(FirebaseGoogleAnalytics.Param.EMAIL_ID, MobileOrEmailid);
                } else {
                    bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, MobileOrEmailid);
                }
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(VerifyOTPActivity.this).logEvent(FirebaseGoogleAnalytics.Event.OTP_SUBMIT, bundle);

                if (FromWhichActivity.equalsIgnoreCase("FromLogin")) {
                    //navigateToHomeActivity();
                    // navigateWelcomeSliderActivity();
                    if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                        navigateWelcomeSliderActivity();
                    } else {
                        navigateToHomeActivity();
                    }
                    //checkUserTypeForHealthAndSafetyFeature();
                } else {
                    // IT SHOULD REDIRECT IF CAME FROM SIGNUP
                    navigateToWelcomActivity();

                }
            } else {
                FWLogger.logInfo(TAG, "verifyOtp not verified");
                Toast.makeText(this, R.string.please_try_again, Toast.LENGTH_LONG).show();
                mOtpView.setError(getString(R.string.invalid_otp));
            }
        } else {
            FWLogger.logInfo(TAG, "verifyOtp not verified due to 1 or more null objects");
            Toast.makeText(this, R.string.please_try_again, Toast.LENGTH_LONG).show();
            SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(false);
            mOtpView.setError(getString(R.string.invalid_otp));
        }
    }

    private void VerifyUserWithOTPDetails() {
       /* mLoginAsyncTask = new TouchLessLoginAsyncTask(VerifyOTPActivity.this, BaseAsyncTask.Priority.LOW, VerifyOTPActivity.this);
        mLoginAsyncTask.execute(MobileOrEmailid, "", CommonFunction.getUniqueDeviceID(VerifyOTPActivity.this));
*/
        try {
            if (Connectivity.isNetworkAvailableRetro(VerifyOTPActivity.this)) {
                Call<FWUser> call = RetrofitClient.getInstance(VerifyOTPActivity.this).getMyApi().doTouchlessLogin(MobileOrEmailid,
                        "", CommonFunction.getUniqueDeviceID(VerifyOTPActivity.this),
                        "en");
                call.enqueue(new retrofit2.Callback<FWUser>() {
                    @Override
                    public void onResponse(Call<FWUser> call, Response<FWUser> response) {
                        try {
                            if (response.code() == 200) {
                                FWUser fwUser = response.body();
                                FWLogger.logInfo(TAG, "Login Successful");

                                if (fwUser != null) {
                                    if (fwUser.getCode().equalsIgnoreCase("200") && fwUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.login_successfully, VerifyOTPActivity.this))) {
                                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser, CommonFunction.getUniqueDeviceID(VerifyOTPActivity.this));
                                        mOTP = Integer.parseInt(fwUser.getResultdata().getOTP());
                                        //Toast.makeText(VerifyOTPActivity.this, "OTP RESENT : " + mOTP, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(VerifyOTPActivity.this, fwUser.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<FWUser> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UserLoginMobile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(VerifyOTPActivity.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UserLoginMobile API:");
            ex.getMessage();
        }
    }

    private void userSignUpGetOTP() {
       /* mSignupAsyncTask = new TouchLessSignUpAsyncTask(VerifyOTPActivity.this, BaseAsyncTask.Priority.LOW, VerifyOTPActivity.this);
        mSignupAsyncTask.execute(MobileOrEmailid, mCountryDetailsId);*/

        try {
            Call<TouchlessTempRegistration> call = RetrofitClient.getInstance(VerifyOTPActivity.this).getMyApi().doTouchlessSignUp(MobileOrEmailid, mCountryDetailsId);
            call.enqueue(new retrofit2.Callback<TouchlessTempRegistration>() {
                @Override
                public void onResponse(Call<TouchlessTempRegistration> call, Response<TouchlessTempRegistration> response) {
                    try {
                        if (response.code() == 200) {
                            TouchlessTempRegistration touchlessTempRegistration = response.body();
                            FWLogger.logInfo(TAG, "RESEND OTP RECEIVED FROM REG HERE : OTP RECEIVED Successful");
                            if (touchlessTempRegistration != null) {
                                if (touchlessTempRegistration.getCode().equalsIgnoreCase("200") && touchlessTempRegistration.getMessage().equalsIgnoreCase(VerifyOTPActivity.this.getString(R.string.otp_sent))) {
                                    mTouchlessSignupId = touchlessTempRegistration.getResultData().getTouchlessSignupId();
                                    mOTP = touchlessTempRegistration.getResultData().getOTP();
                                    //Toast.makeText(VerifyOTPActivity.this, "OTP RESENT : " + mOTP, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }

                @Override
                public void onFailure(Call<TouchlessTempRegistration> call, Throwable throwable) {
                    FWLogger.logInfo(TAG, "Exception in TouchlessTempRegistration API:");
                }
            });
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in TouchlessTempRegistration API:");
            ex.getMessage();
        }

    }


    private void navigateToWelcomActivity() {
        Intent intent = new Intent(VerifyOTPActivity.this, WelcomeActivity.class);
        intent.putExtra("TouchlessSignupId", mTouchlessSignupId);
        intent.putExtra("EmailOrContactNo", MobileOrEmailid);
        intent.putExtra("CountryDetailsId", mCountryDetailsId);
        startActivity(intent);
        finish();
    }


    private void navigateToHomeActivity() {
        Intent intent = new Intent(VerifyOTPActivity.this, HomeActivityNew.class);
        /*if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("NewNotification") != null) {
                intent.putExtra("NewNotification", "NewNotification");
            }
        }*/
        startActivity(intent);
        finish();
        return;
    }

    private void navigateWelcomeSliderActivity() {
        Intent intent = new Intent(VerifyOTPActivity.this, WelcomeSliderActivity.class);
        startActivity(intent);
        finish();
        return;
    }

   /* private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);
        }
    }*/

/*
    public void checkUserTypeForHealthAndSafetyFeature() {
        //If user type is technician then verify Health And Safety Feature is enabled or not (API call)
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyEnabled()) {
                int comp = DateUtils.compareTo(SharedPrefManager.getInstance(getApplicationContext()).getHealthSafetyDate());
                if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyLogged() && comp == 0) {
                    navigateToHomeActivity();// comp == 0 Today's date = logged date
                } else {
                    //checkHealthAndSafetyFeatureEnabled();
                }
            } else {
                //checkHealthAndSafetyFeatureEnabled();
            }
        } else {
            //If user type is owner then Send to Home Activity
            navigateToHomeActivity();
        }
    }
*/

   /* private void checkHealthAndSafetyFeatureEnabled() {
        mGetHealthAndSafetyFeatureAsyncTask = new GetHealthAndSafetyFeatureAsyncTask(VerifyOTPActivity.this, BaseAsyncTask.Priority.LOW, this);
        mGetHealthAndSafetyFeatureAsyncTask.execute();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPrefManager.getInstance(VerifyOTPActivity.this).logout(false);
        Intent intent = new Intent(VerifyOTPActivity.this, LoginTouchlessActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onOtpCompleted(String otp) {
        FWLogger.logInfo(TAG, "onOtpCompleted");
        mOTPEnteredByUser = Integer.valueOf(otp);
        ViewUtils.hideKeyboard(this);

    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        FWLogger.logInfo(TAG, "Login onTaskComplete");
        if (classType.equalsIgnoreCase(FWUser.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Login Successful");
            Gson gson = new Gson();
            FWUser fwUser = gson.fromJson(urlConnectionResponse.resultData, FWUser.class);

            if (fwUser != null) {
                if (fwUser.getCode().equalsIgnoreCase("200") && fwUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.login_successfully, VerifyOTPActivity.this))) {
                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser, CommonFunction.getUniqueDeviceID(VerifyOTPActivity.this));
                    mOTP = Integer.parseInt(fwUser.getResultdata().getOTP());
                    //Toast.makeText(VerifyOTPActivity.this, "OTP RESENT : " + mOTP, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, fwUser.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (classType.equalsIgnoreCase(HealthAndSafety.class.getSimpleName())) {
            Gson gson = new Gson();
            HealthAndSafety healthAndSafety = gson.fromJson(urlConnectionResponse.resultData, HealthAndSafety.class);
            if (healthAndSafety != null) {
                if (healthAndSafety.getResultData().getIsHealthFeaturesEnabled()) {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(true);
                    if (healthAndSafety.getResultData().getIsTodayHealthStatus().getIsHealthAndSafetyValuesAddedForToday()) {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(true);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd"));
                        //navigateToHomeActivity();
                        navigateWelcomeSliderActivity();
                    } else {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                        //navigateToHealthAndSafetyActivity();
                    }
                } else {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                    //navigateToHomeActivity();
                    navigateWelcomeSliderActivity();
                }
            } else {
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                Toast.makeText(this, getString(R.string.something_went_wrong) + "..!", Toast.LENGTH_LONG).show();
            }
        }
        // OTP TASK COMPLETED
        else if (classType.equalsIgnoreCase(OTP.class.getSimpleName())) {
            Gson gson = new Gson();
            OTP otp = gson.fromJson(urlConnectionResponse.resultData, OTP.class);
            if (otp != null) {
                FWLogger.logInfo(TAG, "onTaskComplete otp object not null");
                if (otp.getCode().equalsIgnoreCase("200")) {
                    String msg = otp.getMessage();
                    FWLogger.logInfo(TAG, "onTaskComplete : msg not null " + msg);
                    if (msg != null) {
                        if (msg.equalsIgnoreCase("Number is Already Register as Technician.")) {
//                            FWLogger.logInfo(TAG, "onTaskComplete 1");
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                            mOTP = 0;
                            //mUserGroup = "";
                        } else if (msg.equalsIgnoreCase("OTP generated successfully for tech.")) {
//                            FWLogger.logInfo(TAG, "onTaskComplete 2");
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                            // bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, mEditTextMobileNo.getText().toString());
                            if (MobileOrEmailid.contains("@")) {
                                bundle.putString(FirebaseGoogleAnalytics.Param.EMAIL_ID, MobileOrEmailid);
                            } else {
                                bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, MobileOrEmailid);
                            }
                            mOTPAttempt++;//increment this each time
                            bundle.putInt(FirebaseGoogleAnalytics.Param.OTP_ATTEMPT, mOTPAttempt);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            FirebaseAnalytics.getInstance(VerifyOTPActivity.this).logEvent(FirebaseGoogleAnalytics.Event.OTP_SENT, bundle);
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                            mOTP = otp.getResultData().getOtp();
                            //mUserGroup = "Tech";
                        } else if (msg.equalsIgnoreCase("Number is already register as Owner.")) {
//                            FWLogger.logInfo(TAG, "onTaskComplete 3");
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                            mOTP = 0;
                            // mUserGroup = "";
                        } else if (msg.equalsIgnoreCase("OTP generated successfully  for Owner.")) {
//                            FWLogger.logInfo(TAG, "onTaskComplete 4");
                            //NOTE: Log GA event
                            Bundle bundle = new Bundle();
                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                            //bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, mEditTextMobileNo.getText().toString());
                            if (MobileOrEmailid.contains("@")) {
                                bundle.putString(FirebaseGoogleAnalytics.Param.EMAIL_ID, MobileOrEmailid);
                            } else {
                                bundle.putString(FirebaseGoogleAnalytics.Param.MOBILE_NO, MobileOrEmailid);
                            }
                            mOTPAttempt++;//increment this each time
                            bundle.putInt(FirebaseGoogleAnalytics.Param.OTP_ATTEMPT, mOTPAttempt);
                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                            FirebaseAnalytics.getInstance(VerifyOTPActivity.this).logEvent(FirebaseGoogleAnalytics.Event.OTP_SENT, bundle);
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                            mOTP = otp.getResultData().getOtp();
                            //mUserGroup = "Own";
                        } else {
//                            FWLogger.logInfo(TAG, "onTaskComplete 5");
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                            mOTP = 0;
                            //mUserGroup = "";
                        }
                    }
                } else {
                    FWLogger.logInfo(TAG, "onTaskComplete not 200");
                    Toast.makeText(this, otp.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        } else if (classType.equalsIgnoreCase(TouchlessTempRegistration.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "RESEND OTP RECEIVED FROM REG HERE : OTP RECEIVED Successful");
            Gson gson = new Gson();
            TouchlessTempRegistration tSignUp = gson.fromJson(urlConnectionResponse.resultData, TouchlessTempRegistration.class);
            if (tSignUp != null) {
                if (tSignUp.getCode().equalsIgnoreCase("200") && tSignUp.getMessage().equalsIgnoreCase(VerifyOTPActivity.this.getString(R.string.otp_sent))) {
                    mTouchlessSignupId = tSignUp.getResultData().getTouchlessSignupId();
                    mOTP = tSignUp.getResultData().getOTP();
                    //Toast.makeText(VerifyOTPActivity.this, "OTP RESENT : " + mOTP, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void resendOtp() {
        //getOtp();
        mtxtOtpExpired.setVisibility(View.GONE);
        if (!MobileOrEmailid.contains("@")) {
            startSmartUserConsent();
        }
        if (FromWhichActivity.equalsIgnoreCase("FromRegisterHere")) {
            // FRM SIGNUP
            userSignUpGetOTP();
        } else {
            // FROM LOGIN
            VerifyUserWithOTPDetails();
        }
        OTPExpired();
        mTextViewResendOTP.setEnabled(false);
        mTextViewResendOTP.setTextColor(Color.GRAY);

        new CountDownTimer(60000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                mTextViewTimer.setText("" + String.format("0%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                mTextViewTimer.setText("");
            }
        }.start();

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                mTextViewResendOTP.setEnabled(true);
                mTextViewResendOTP.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        }, 60000);
    }

    private void navigateToHealthAndSafetyActivity() {
        Intent intent = new Intent(VerifyOTPActivity.this, HealthAndSafetyActivity.class);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("NewNotification") != null) {
                intent.putExtra("NewNotification", "NewNotification");
            }
        }
        intent.putExtra("isFromOptionMenu", false);
        startActivity(intent);
        finish();
        return;
    }

    private void OTPExpired() {
        try {
            new CountDownTimer(/*30000*/270000, 1000) { // 4.5 Minute : OTP WILL GET EXPIRED
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    mOTP = 0;
                    mtxtOtpExpired.setVisibility(View.VISIBLE);
                    mtxtOtpExpired.setText(R.string.otp_expired);
                }
            }.start();

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    // START : AUTO READ/SET OTP
    private void startSmartUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsUserConsent(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {

        Pattern otpPattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = otpPattern.matcher(message);
        if (matcher.find()) {
            mOtpView.setText(matcher.group(0));
            verifyOtp();
        }
    }

    // END

   /* private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        *//*Pattern pattern = Pattern.compile("(|^)\\d{6}");*//*
        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            mOtpView.setText(matcher.group(0));
        }
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                boolean userAllowed = true;
                for (final int result : grantResults) {
                    userAllowed &= result == PackageManager.PERMISSION_GRANTED;
                }
                if (userAllowed) {
                    // SMS permissions granted
                    try {
                        //new SmsBroadcastReceiver().setEditTextOTP(mOtpView);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                      /*  if (getPackageName().equalsIgnoreCase("com.corefield.fieldweb.qa") ||
                                getPackageName().equalsIgnoreCase("com.corefield.fieldweb")) {
                            startActivityForResult(intent, REQ_USER_CONSENT);
                        }*/
                        try {
                            //Intent intentt = getIntent();
                            //Intent forward = (Intent) intentt.getParcelableExtra("TEST");
                            ComponentName componentName
                                    = intent.resolveActivity(getPackageManager());

                            if (componentName.getPackageName().equals("com.google.android.gms") &&
                                    componentName.getClassName().equals("com.google.android.gms.auth.api.phone.ui.UserConsentPromptActivity")) {
                                startActivityForResult(intent, REQ_USER_CONSENT);
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure() {
                        //Toast.makeText(VerifyOTPActivity.this, "Under Failure", Toast.LENGTH_SHORT).show();
                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            registerBroadcastReceiver();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(smsBroadcastReceiver);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    // END : AUTO FILL OTP

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
