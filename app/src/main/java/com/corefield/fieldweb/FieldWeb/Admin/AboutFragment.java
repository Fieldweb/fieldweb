package com.corefield.fieldweb.FieldWeb.Admin;


import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog;
import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;
import com.corefield.fieldweb.FieldWeb.Home.TechDashboardFragmentNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

/**
 * Fragment Controller for HelpAndSupportFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Help and Support
 */
public class AboutFragment extends Fragment implements View.OnClickListener {
    protected static String TAG = AboutFragment.class.getSimpleName();
    private View mRootView;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_about, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.about));

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        TextView textViewTermsConditions = mRootView.findViewById(R.id.textViewTermsConditions);
        TextView textView_privacy_policy = mRootView.findViewById(R.id.textView_privacy_policy);
        TextView textView_about = mRootView.findViewById(R.id.textView_about);
        TextView textViewShareFeedback = mRootView.findViewById(R.id.textViewShareFeedback);

        textViewTermsConditions.setOnClickListener(this);
        textView_privacy_policy.setOnClickListener(this);
        textView_about.setOnClickListener(this);
        textViewShareFeedback.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

        switch (v.getId()) {

            case R.id.textViewTermsConditions:
                loadFragment(new PrivacyPolicyFragment());
                break;

            case R.id.textView_privacy_policy:
                loadFragment(new PrivacyPolicyFragment(), "privacy");
                break;

            case R.id.textView_about:
                loadFragment(new PrivacyPolicyFragment(), "about");
                break;

            case R.id.textViewShareFeedback:
                loadFragment(new SuggestionFeedbackFragment());
                break;

            default:
                break;
        }
    }

    private void loadFragment(Fragment fragment, String type) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("TYPE", type);
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("TYPE", "terms");
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }
}
