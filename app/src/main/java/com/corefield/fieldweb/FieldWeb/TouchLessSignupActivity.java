package com.corefield.fieldweb.FieldWeb;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CustomCountryAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.CountryList.GetCountryList;
import com.corefield.fieldweb.DTO.FWUser;
import com.corefield.fieldweb.DTO.Notification.RegisterDeviceId;
import com.corefield.fieldweb.DTO.RegisterHere.TouchlessTempRegistration;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeSplashWhatsappActivity;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.LocaleUtils;
import com.corefield.fieldweb.Util.RecyclerTouchListeners;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

public class TouchLessSignupActivity extends BaseActivity implements View.OnClickListener, OnTaskCompleteListener,
        GoogleApiClient.OnConnectionFailedListener, CustomCountryAdapter.CommonAdapterPositionListener {

    private final String TAG = TouchLessSignupActivity.class.getSimpleName();
    private ImageView countryImg/*spin_country*/;
    private TextView mTextViewMobileNOPrefix, mTextViewLogin, txtCountryName;
    private EditText mEditTextMobileNo, edt_Non_IndiaEmail;
    private Button mButtonGetOTP;
    private String googleDisplayName = "";
    private List<GetCountryList.ResultData> mCountrtyLists = null;
    //GOOGLE SIGN IN
    SignInButton signInButton;
    private GoogleApiClient googleApiClient;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;
    LinearLayout lnlForIndiaMobile, lnlGoogleSignIn, lnlSetData;
    private String isEmailOrMobile = "", isGoogleSignIN = "";
    ArrayList<Bitmap> bitmap_array;
    CustomCountryAdapter mcustomCountryAdapter = null;
    int mCommonAdapterPosition, mCountryDetailsId, mTouchlessSignupId, mOTP;
    ImageView video, whatsapp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.touchless_signup_activity);
        inIt();
    }

    private void inIt() {

        //spin_country = findViewById(R.id.spin_country);
        mTextViewMobileNOPrefix = (TextView) findViewById(R.id.textView_mobile_no_prefix);
        mEditTextMobileNo = findViewById(R.id.edittext_mobile_no_for_otp);
        edt_Non_IndiaEmail = findViewById(R.id.edt_Non_IndiaEmail);
        mButtonGetOTP = findViewById(R.id.button_get_otp);
        mTextViewLogin = findViewById(R.id.textView_Login);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        lnlForIndiaMobile = (LinearLayout) findViewById(R.id.lnlForIndiaMobile);
        lnlGoogleSignIn = (LinearLayout) findViewById(R.id.lnlGoogleSignIn);
        lnlSetData = (LinearLayout) findViewById(R.id.lnlSetData);
        countryImg = (ImageView) findViewById(R.id.countryImg);
        txtCountryName = (TextView) findViewById(R.id.txtCountryName);
        //
        whatsapp = (ImageView) findViewById(R.id.whatsapp);
        video = (ImageView) findViewById(R.id.video);

        // GOOGLE SIGN IN : START
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isGoogleSignIN = "Google";
                mGoogleSignInClient.signOut();
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, RC_SIGN_IN);
            }
        });
        // GOOGLE SIGN IN : END

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "+919354795965"; // replace with the phone number you want to message
                String message = "Hello, I am having trouble signing up in FieldWeb. Please help"; // replace with your custom message
                try {
                    // Encode the message to be URL-safe
                    String encodedMessage = URLEncoder.encode(message, "UTF-8");
                    // Create the WhatsApp intent with the phone number and message
                    Uri uri = Uri.parse("https://wa.me/" + phoneNumber + "/?text=" + encodedMessage);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(getApplicationContext(), YouTubeSplashWhatsappActivity.class);
                startActivity(send);
            }
        });

        // FOR LANGUAGE UPDATE
        try {
            if (SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage() != null) {  //&& !SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage().isEmpty()
                LocaleUtils.setLocale(TouchLessSignupActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
            } else {
                SharedPrefManager.getInstance(getApplicationContext()).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ENGLISH, 0);
                LocaleUtils.setLocale(TouchLessSignupActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // GET/SET MOBILE NUMBER FROM LOGIN ACTIVITY
        try {
            Intent iVal = getIntent();
            String loginMobileNum = iVal.getStringExtra("LoginMobNumber");
            if (!loginMobileNum.contains("@")) {
                mEditTextMobileNo.setText(loginMobileNum);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        GetCountryList();
        mButtonGetOTP.setOnClickListener(this);
        mTextViewLogin.setOnClickListener(this);

        lnlSetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    countryListPopUp();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
    }

    private void GetCountryList() {
        /*mCountryListAsyncTask = new GetCountryListAsyncTask(this, BaseAsyncTask.Priority.LOW, this);
        mCountryListAsyncTask.execute();*/
        try {
            if (Connectivity.isNetworkAvailableRetro(TouchLessSignupActivity.this)) {
                Call<GetCountryList> call = RetrofitClient.getInstance(TouchLessSignupActivity.this).getMyApi().getCountryList();
                call.enqueue(new retrofit2.Callback<GetCountryList>() {
                    @Override
                    public void onResponse(Call<GetCountryList> call, Response<GetCountryList> response) {
                        try {
                            if (response.code() == 200) {
                                GetCountryList countryList = response.body();
                                FWLogger.logInfo(TAG, "GetCountryList Successful");
                                if (countryList != null) {
                                    if (countryList.getCode().equalsIgnoreCase("200") && countryList.getMessage().equalsIgnoreCase("success")) {

                                        mCountrtyLists = new ArrayList<>();
                                        mCountrtyLists = countryList.getResultData();
                                        bitmap_array = new ArrayList<Bitmap>();

                                        new Thread() {
                                            public void run() {
                                                for (int i = 0; i < mCountrtyLists.size(); i++) {
                                                    Bitmap bit = CommonFunction.getBitmapFromURL(mCountrtyLists.get(i).getCountryFlag());
                                                    bitmap_array.add(bit);
                                                    // SET DEFAULT INDIA AND LAYOUT ACCORDINGLY ==
                                                    if (mCountrtyLists.get(i).getCountryCode().equalsIgnoreCase("91")) {
                                                        mCountryDetailsId = mCountrtyLists.get(i).getCountryDetailsId();
                                                        int finalI = i;
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                lnlForIndiaMobile.setVisibility(View.VISIBLE);
                                                                edt_Non_IndiaEmail.setVisibility(View.GONE);
                                                                lnlGoogleSignIn.setVisibility(View.GONE);
                                                                countryImg.setImageBitmap(bitmap_array.get(finalI));
                                                                txtCountryName.setText(mCountrtyLists.get(finalI).getCountryName());
                                                                mTextViewMobileNOPrefix.setText("+" + " " + mCountrtyLists.get(finalI).getCountryCode());
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        }.start();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetCountryList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetCountryList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(TouchLessSignupActivity.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetCountryList API:");
            ex.getMessage();
        }

    }

    private void userSignUpGetOTP(String strMobile_OR_Emailid) {
       /* mSignupAsyncTask = new TouchLessSignUpAsyncTask(TouchLessSignupActivity.this, BaseAsyncTask.Priority.LOW, TouchLessSignupActivity.this);
        mSignupAsyncTask.execute(strMobile_OR_Emailid, mCountryDetailsId);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(TouchLessSignupActivity.this)) {
                Call<TouchlessTempRegistration> call = RetrofitClient.getInstance(TouchLessSignupActivity.this).getMyApi().doTouchlessSignUp(strMobile_OR_Emailid, mCountryDetailsId);
                call.enqueue(new retrofit2.Callback<TouchlessTempRegistration>() {
                    @Override
                    public void onResponse(Call<TouchlessTempRegistration> call, Response<TouchlessTempRegistration> response) {
                        try {
                            if (response.code() == 200) {
                                TouchlessTempRegistration touchlessTempRegistration = response.body();
                                FWLogger.logInfo(TAG, "REGISTER HERE : OTP RECEIVED Successful");
                                if (touchlessTempRegistration != null) {
                                    if (touchlessTempRegistration.getCode().equalsIgnoreCase("200") && touchlessTempRegistration.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.otp_sent, TouchLessSignupActivity.this))) {
                                        mTouchlessSignupId = touchlessTempRegistration.getResultData().getTouchlessSignupId();
                                        if (isGoogleSignIN.equalsIgnoreCase("Google")) {
                                            navigateToWelcome();
                                        } else {
                                            mOTP = touchlessTempRegistration.getResultData().getOTP();
                                            navigateToVerifyOTP();
                                        }

                                    } else if (touchlessTempRegistration.getCode().equalsIgnoreCase("404") && touchlessTempRegistration.getMessage().equalsIgnoreCase("Credentails already exist")) {
                                        Toast.makeText(TouchLessSignupActivity.this, "Credentails already exist,Please login to proceed", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(TouchLessSignupActivity.this, LoginTouchlessActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TouchlessTempRegistration> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in TouchlessTempRegistration API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(TouchLessSignupActivity.this);
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in TouchlessTempRegistration API:");
            ex.getMessage();
        }

    }


    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.button_get_otp:
               /* if (validateMobileNumorEmailId()) {
                    Intent intent = new Intent(TouchLessSignupActivity.this, VerifyOTPActivity.class);
                    intent.putExtra("UserID", mEditTextMobileNo.getText().toString().trim());
                    intent.putExtra("FromWhere", "FromSignup");
                    startActivity(intent);
                    finish();
                }*/

                if (lnlForIndiaMobile.getVisibility() == View.VISIBLE) {
                    isEmailOrMobile = "";
                    isEmailOrMobile = "Mobile";
                    if (validateMobileNumber()) {
                        userSignUpGetOTP(mEditTextMobileNo.getText().toString());
                    }

                } else if (edt_Non_IndiaEmail.getVisibility() == View.VISIBLE) {
                    isEmailOrMobile = "";
                    isEmailOrMobile = "Emailid";
                    if (validateEmailId()) {
                        userSignUpGetOTP(edt_Non_IndiaEmail.getText().toString());
                    }
                }
                break;

            case R.id.textView_Login:
                Intent intent = new Intent(TouchLessSignupActivity.this, LoginTouchlessActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;

            default:
                break;

        }

    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {

        try {
            FWLogger.logInfo(TAG, "GetCountryList onTaskComplete");
            if (classType.equalsIgnoreCase(GetCountryList.class.getSimpleName())) {
                FWLogger.logInfo(TAG, "GetCountryList Successful");
                Gson gson = new Gson();
                GetCountryList countryList = gson.fromJson(urlConnectionResponse.resultData, GetCountryList.class);
                if (countryList != null) {
                    if (countryList.getCode().equalsIgnoreCase("200") && countryList.getMessage().equalsIgnoreCase("success")) {

                        mCountrtyLists = new ArrayList<>();
                        mCountrtyLists = countryList.getResultData();
                        bitmap_array = new ArrayList<Bitmap>();

                        new Thread() {
                            public void run() {
                                for (int i = 0; i < mCountrtyLists.size(); i++) {
                                    Bitmap bit = CommonFunction.getBitmapFromURL(mCountrtyLists.get(i).getCountryFlag());
                                    bitmap_array.add(bit);
                                    // SET DEFAULT INDIA AND LAYOUT ACCORDINGLY ==
                                    if (mCountrtyLists.get(i).getCountryCode().equalsIgnoreCase("91")) {
                                        mCountryDetailsId = mCountrtyLists.get(i).getCountryDetailsId();
                                        int finalI = i;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lnlForIndiaMobile.setVisibility(View.VISIBLE);
                                                edt_Non_IndiaEmail.setVisibility(View.GONE);
                                                lnlGoogleSignIn.setVisibility(View.GONE);
                                                countryImg.setImageBitmap(bitmap_array.get(finalI));
                                                txtCountryName.setText(mCountrtyLists.get(finalI).getCountryName());
                                                mTextViewMobileNOPrefix.setText("+" + " " + mCountrtyLists.get(finalI).getCountryCode());
                                            }
                                        });
                                    }
                                }
                            }
                        }.start();
                    }
                }

            } else if (classType.equalsIgnoreCase(TouchlessTempRegistration.class.getSimpleName())) {
                FWLogger.logInfo(TAG, "REGISTER HERE : OTP RECEIVED Successful");
                Gson gson = new Gson();
                TouchlessTempRegistration tSignUp = gson.fromJson(urlConnectionResponse.resultData, TouchlessTempRegistration.class);
                if (tSignUp != null) {
                    if (tSignUp.getCode().equalsIgnoreCase("200") && tSignUp.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.otp_sent, TouchLessSignupActivity.this))) {
                        mTouchlessSignupId = tSignUp.getResultData().getTouchlessSignupId();
                        if (isGoogleSignIN.equalsIgnoreCase("Google")) {
                            navigateToWelcome();
                        } else {
                            mOTP = tSignUp.getResultData().getOTP();
                            navigateToVerifyOTP();
                        }

                    } else if (tSignUp.getCode().equalsIgnoreCase("404") && tSignUp.getMessage().equalsIgnoreCase("Credentails already exist")) {
                        Toast.makeText(TouchLessSignupActivity.this, "Credentails already exist,Please login to proceed", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(TouchLessSignupActivity.this, LoginTouchlessActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
            } else if (classType.equalsIgnoreCase(FWUser.class.getSimpleName())) {
                FWLogger.logInfo(TAG, "Login Successful");
                Gson gson = new Gson();
                FWUser fwUser = gson.fromJson(urlConnectionResponse.resultData, FWUser.class);

                if (fwUser != null) {
                    if (fwUser.getCode().equalsIgnoreCase("200") && fwUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.login_successfully, TouchLessSignupActivity.this))) {
                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser, CommonFunction.getUniqueDeviceID(TouchLessSignupActivity.this));
                        //Toast.makeText(TouchLessSignupActivity.this, "L:: " + fwUser.getResultdata().getOTP(), Toast.LENGTH_LONG).show();
                        int uniqueId = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                        if (uniqueId != 0) {
                            FirebaseCrashlytics.getInstance().setUserId("" + uniqueId);
                        }

                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, fwUser.getResultdata().getUserid());
                        // bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, mEditTextUser.getText().toString());
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, fwUser.getResultdata().getUsergroupname());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID() != null) {
                            sendRegistrationToServer(SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID());
                        }
                        // }
                    } else {
                        Toast.makeText(this, fwUser.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (classType.equalsIgnoreCase(RegisterDeviceId.class.getSimpleName())) {
                Gson gson = new Gson();
                RegisterDeviceId registerDeviceId = gson.fromJson(urlConnectionResponse.resultData, RegisterDeviceId.class);
                FWLogger.logInfo(TAG, "Device Registration");
                if (registerDeviceId != null) {
                    if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                        FWLogger.logInfo(TAG, "Device Registration Completed.");
                    } else {
                        FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                    }
                }
                // SEND FOR OTP VERIFICATION
                navigateToVerifyOTP();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    // GOOGLE SIGN IN : START
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            edt_Non_IndiaEmail.setText(account.getEmail());
            googleDisplayName = account.getDisplayName();

            userSignUpGetOTP(edt_Non_IndiaEmail.getText().toString());

            //userName.setText(account.getDisplayName());
            //userEmail.setText(account.getEmail());
            //userId.setText(account.getId());
           /* try {
                Glide.with(this).load(account.getPhotoUrl()).into(profileImage);
            } catch (NullPointerException e) {
                Toast.makeText(getApplicationContext(), "image not found", Toast.LENGTH_LONG).show();
            }*/
        } else {
            Toast.makeText(getApplicationContext(), "Sign in cancel", Toast.LENGTH_LONG).show();
        }
    }

    private void navigateToWelcome() {
        try {
            Intent intent = new Intent(TouchLessSignupActivity.this, WelcomeActivity.class);
            intent.putExtra("EmailOrContactNo", edt_Non_IndiaEmail.getText().toString().trim());
            //intent.putExtra("UserOTP", String.valueOf(mOTP));
            intent.putExtra("TouchlessSignupId", mTouchlessSignupId);
            intent.putExtra("CountryDetailsId", mCountryDetailsId);
            intent.putExtra("DisplayName", googleDisplayName);
            intent.putExtra("FromWhere", "Google");
            startActivity(intent);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void gotoProfile() {
        // CHK THIS CLASS FOR FURTHER RESPONSE
        // Intent intent = new Intent(TouchLessSignupActivity.this, ProfileActivity.class);
        // startActivity(intent);
        try {
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
            if (opr.isDone()) {
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
    // GOOGLE SIGN IN : END

    private boolean validateMobileNumber() {
        FWLogger.logInfo(TAG, "validate MobileNo");

        if (CommonFunction.isEmpty(mEditTextMobileNo)) {
            mEditTextMobileNo.setError(getString(R.string.enter_your_mobile_email));
            return false;
        }
        if (CommonFunction.isValidPhone(mEditTextMobileNo)) {
            mEditTextMobileNo.setError(getString(R.string.please_enter_valid_number));
            return false;
        }

        return true;
    }

    private boolean validateEmailId() {
        FWLogger.logInfo(TAG, "validate Email ID");
        String input = edt_Non_IndiaEmail.getText().toString();

        if (CommonFunction.isEmpty(edt_Non_IndiaEmail)) {
            edt_Non_IndiaEmail.setError(getString(R.string.enter_your_email_id));
            return false;
        }
        if (!CommonFunction.isEmailValid(input)) {
            edt_Non_IndiaEmail.setError(getString(R.string.please_enter_valid_emailid));
            return false;
        }
        return true;
    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
       /* mRegisterDeviceAsyncTask = new RegisterDeviceAsyncTask(getApplicationContext(), BaseAsyncTask.Priority.LOW, this);
        mRegisterDeviceAsyncTask.execute(token);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(TouchLessSignupActivity.this)) {
                String ToInstanceDeviceId = token;
                String UserDeviceId = "Android Device";
                int UserId = SharedPrefManager.getInstance(TouchLessSignupActivity.this).getUserId();
                int CreatedBy = SharedPrefManager.getInstance(TouchLessSignupActivity.this).getUserId();
                Call<RegisterDeviceId> call = RetrofitClient.getInstance(TouchLessSignupActivity.this).getMyApi().
                        doSendRegistrationToServer(ToInstanceDeviceId, UserDeviceId, UserId, CreatedBy);
                call.enqueue(new retrofit2.Callback<RegisterDeviceId>() {
                    @Override
                    public void onResponse(Call<RegisterDeviceId> call, Response<RegisterDeviceId> response) {
                        try {
                            if (response.code() == 200) {
                                RegisterDeviceId registerDeviceId = response.body();
                                FWLogger.logInfo(TAG, "Device Registration");
                                if (registerDeviceId != null) {
                                    if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                                        FWLogger.logInfo(TAG, "Device Registration Completed.");
                                    } else {
                                        FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                                    }
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterDeviceId> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in RegisterDevice API:");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(TouchLessSignupActivity.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in RegisterDevice API:");
            ex.getMessage();
        }


    }

    private void navigateToVerifyOTP() {
        if (isEmailOrMobile.equalsIgnoreCase("Mobile")) {

            Intent intent = new Intent(TouchLessSignupActivity.this, VerifyOTPActivity.class);
            intent.putExtra("UserID", mEditTextMobileNo.getText().toString().trim());
            intent.putExtra("UserOTP", String.valueOf(mOTP));
            intent.putExtra("TouchlessSignupId", mTouchlessSignupId);
            intent.putExtra("CountryDetailsId", mCountryDetailsId);
            intent.putExtra("FromWhere", "FromRegisterHere");
            startActivity(intent);
            finish();
        } else if (isEmailOrMobile.equalsIgnoreCase("Emailid")) {

            Intent intent = new Intent(TouchLessSignupActivity.this, VerifyOTPActivity.class);
            intent.putExtra("UserID", edt_Non_IndiaEmail.getText().toString().trim());
            intent.putExtra("UserOTP", String.valueOf(mOTP));
            intent.putExtra("TouchlessSignupId", mTouchlessSignupId);
            intent.putExtra("CountryDetailsId", mCountryDetailsId);
            intent.putExtra("FromWhere", "FromRegisterHere");
            startActivity(intent);
            finish();

            finish();
        }
    }

    private void countryListPopUp() {
        FWDialog dialog = new FWDialog(TouchLessSignupActivity.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.country_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        EditText edtSearchCountry;
        RecyclerView lstCountry;
        LinearLayoutManager linearLayoutManager;

        edtSearchCountry = dialog.findViewById(R.id.edtSearchCountry);
        lstCountry = (RecyclerView) dialog.findViewById(R.id.lstCountry);
        //mcustomCountryAdapter.setClickListener(this);

        if (mCountrtyLists.size() > 0) {
            linearLayoutManager = new LinearLayoutManager(this);
            lstCountry.setLayoutManager(linearLayoutManager);
            mcustomCountryAdapter = new CustomCountryAdapter(TouchLessSignupActivity.this, this, mCountrtyLists, bitmap_array, this);
            lstCountry.setAdapter(mcustomCountryAdapter);

        }
        //
        edtSearchCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                mcustomCountryAdapter.getFilter().filter(cs.toString());
                mcustomCountryAdapter.notifyDataSetChanged();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable cs) {

            }
        });

        //
        lstCountry.addOnItemTouchListener(new RecyclerTouchListeners(TouchLessSignupActivity.this, lstCountry, new RecyclerTouchListeners.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                try {
                    //position = mCommonAdapterPosition;
                    //mcustomCountryAdapter.getItem(position).getCountryName();
                    //if (mCommonAdapterPosition == null)
                    if (!edtSearchCountry.getText().toString().isEmpty()) {
                        Picasso.get().load(mcustomCountryAdapter.getItem(position).getCountryFlag()).into(countryImg);
                        txtCountryName.setText(mcustomCountryAdapter.getItem(position).getCountryName());
                        mCountryDetailsId = mcustomCountryAdapter.getItem(position).getCountryDetailsId();
                        try {
                            if (mcustomCountryAdapter.getItem(position).getCountryCode().equalsIgnoreCase("91")) {
                                lnlForIndiaMobile.setVisibility(View.VISIBLE);
                                edt_Non_IndiaEmail.setVisibility(View.GONE);
                                lnlGoogleSignIn.setVisibility(View.GONE);
                                mTextViewMobileNOPrefix.setText("+" + mcustomCountryAdapter.getItem(position).getCountryCode());
                            } else {
                                mTextViewMobileNOPrefix.setText("+" + " " + mcustomCountryAdapter.getItem(position).getCountryCode());
                                lnlForIndiaMobile.setVisibility(View.GONE);
                                edt_Non_IndiaEmail.setVisibility(View.VISIBLE);
                                lnlGoogleSignIn.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    } else {
                        if (mCountrtyLists.size() > 0 && bitmap_array.size() > 0) {
                            //countryImg.setImageBitmap(bitmap_array.get(position));
                            Picasso.get().load(mCountrtyLists.get(position).getCountryFlag()).into(countryImg);
                            txtCountryName.setText(mCountrtyLists.get(position).getCountryName());
                            mCountryDetailsId = mCountrtyLists.get(position).getCountryDetailsId();
                            try {
                                if (mCountrtyLists.size() > 0) {
                                    if (mCountrtyLists.get(position).getCountryCode().equalsIgnoreCase("91")) {
                                        lnlForIndiaMobile.setVisibility(View.VISIBLE);
                                        edt_Non_IndiaEmail.setVisibility(View.GONE);
                                        lnlGoogleSignIn.setVisibility(View.GONE);
                                        mTextViewMobileNOPrefix.setText("+" + " " + mCountrtyLists.get(position).getCountryCode());
                                    } else {
                                        mTextViewMobileNOPrefix.setText("+" + " " + mCountrtyLists.get(position).getCountryCode());
                                        lnlForIndiaMobile.setVisibility(View.GONE);
                                        edt_Non_IndiaEmail.setVisibility(View.VISIBLE);
                                        lnlGoogleSignIn.setVisibility(View.VISIBLE);
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }
                    }
                    dialog.dismiss();
                } catch (Exception e) {
                    e.getMessage();
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        dialog.show();
    }

    @Override
    public void onDestroy() {
        FWLogger.logInfo(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onItemSelected(int position) {
        mCommonAdapterPosition = position;
    }
}
