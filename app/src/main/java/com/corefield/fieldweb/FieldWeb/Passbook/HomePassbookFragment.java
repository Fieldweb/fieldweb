package com.corefield.fieldweb.FieldWeb.Passbook;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Passbook.DailyPassbook;
import com.corefield.fieldweb.DTO.Passbook.MonthlyPassbook;
import com.corefield.fieldweb.DTO.Passbook.TodayPassbook;
import com.corefield.fieldweb.DTO.Passbook.WeeklyPassbook;
import com.corefield.fieldweb.DTO.Passbook.YearlyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.DailyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.MonthlyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.TodayPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.WeeklyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.YearlyPassbookAsyncTask;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Fragment Controller for Home Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class contain all the view pager adapter for daily/monthly/yearly/today/weekly passbook fragment class
 */
public class HomePassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = HomePassbookFragment.class.getSimpleName();
    public List<MonthlyPassbook.MonthlyALlDataList> mMonthlyPassbook = null;
    public List<YearlyPassbook.GetYearlyData> mYearlyPassbook = null;
    public List<WeeklyPassbook.WeeklyData> mWeeklyPassbook = null;
    private View mRootView;
    private TabLayout mTabLayoutPassbook;
    private ViewPager mViewPagerPassbook;
    private TextView mEarned, mCredit, mExpense, mReturn, mTitle;
    private List<DailyPassbook.GetCurrentWeekData> mDailyPassbook = null;
    private TodayPassbookAsyncTask mTodayPassbookAsyncTask;
    private DailyPassbookAsyncTask mDailyPassbookAsyncTask;
    private WeeklyPassbookAsyncTask mWeeklyPassbookAsyncTask;
    private MonthlyPassbookAsyncTask mMonthlyPassbookAsyncTask;
    private YearlyPassbookAsyncTask mYearlyPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.home_passbook_fragment, container, false);
        inIt();
        FGALoadEvent();
        return mRootView;
    }

    private void inIt() {
        mViewPagerPassbook = mRootView.findViewById(R.id.passbook_viewpager);
        mEarned = mRootView.findViewById(R.id.textview_passbook_earned);
        mCredit = mRootView.findViewById(R.id.textview_passbook_credit);
        mExpense = mRootView.findViewById(R.id.textview_passbook_expense);
        mReturn = mRootView.findViewById(R.id.textview_passbook_return);
        mTitle = mRootView.findViewById(R.id.textview_passbook_title);
        mTitle.setText(getResources().getString(R.string.today_bracket));
        apiCall(0); // First Default Api Call for Today passbook
        setupViewPagerPassbook(mViewPagerPassbook);
        mTabLayoutPassbook = mRootView.findViewById(R.id.passbook_tab);
        mTabLayoutPassbook.setupWithViewPager(mViewPagerPassbook);
        mViewPagerPassbook.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    apiCall(position);
                    mTitle.setText(getResources().getString(R.string.today_bracket));
                } else if (position == 1) {
                    apiCall(position);
                    mTitle.setText(R.string.current_week_bracket);
                } else if (position == 2) {
                    apiCall(position);
                    mTitle.setText(R.string.current_month_btacket);
                } else if (position == 3) {
                    apiCall(position);
                    mTitle.setText(R.string.current_year_bracket);
                } else if (position == 4) {
                    apiCall(position);
                    mTitle.setText(R.string.years_bracket);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.PASSBOOK, bundle);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void apiCall(int position) {
        if (position == 0) {
            mTodayPassbookAsyncTask = new TodayPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mTodayPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 1) {
            mDailyPassbookAsyncTask = new DailyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mDailyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 2) {
            mWeeklyPassbookAsyncTask = new WeeklyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mWeeklyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 3) {
            mMonthlyPassbookAsyncTask = new MonthlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mMonthlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 4) {
            mYearlyPassbookAsyncTask = new YearlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mYearlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        }
    }

    private void setupViewPagerPassbook(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new TodayPassbookFragment(), getResources().getString(R.string.today));
        adapter.addFragment(new DailyPassbookFragment(), getString(R.string.daily));
        adapter.addFragment(new WeeklyPassbookFragment(), getString(R.string.weekly));
        adapter.addFragment(new MonthlyPassbookFragment(), getString(R.string.monthly));
        adapter.addFragment(new YearlyPassbookFragment(), getString(R.string.yearly));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(TodayPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            TodayPassbook todayPassbook = gson.fromJson(urlConnectionResponse.resultData, TodayPassbook.class);
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(todayPassbook.getResultData().getEarningAmount()));
            mCredit.setText(String.valueOf(todayPassbook.getResultData().getCredit()));
            mExpense.setText(String.valueOf(todayPassbook.getResultData().getExpenses()));
            mReturn.setText(String.valueOf(todayPassbook.getResultData().getReturn()));
        } else if (classType.equalsIgnoreCase(DailyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            DailyPassbook dailyPassbook = gson.fromJson(urlConnectionResponse.resultData, DailyPassbook.class);
            mDailyPassbook = dailyPassbook.getResultData().getGetCurrentWeekData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + dailyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(dailyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(dailyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(dailyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(dailyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(WeeklyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            WeeklyPassbook weeklyPassbook = gson.fromJson(urlConnectionResponse.resultData, WeeklyPassbook.class);
            mWeeklyPassbook = weeklyPassbook.getResultData().getWeeklyData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + weeklyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(weeklyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(weeklyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(weeklyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(weeklyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(MonthlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            MonthlyPassbook monthlyPassbook = gson.fromJson(urlConnectionResponse.resultData, MonthlyPassbook.class);
            mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + monthlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(monthlyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(monthlyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(monthlyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(monthlyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(YearlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            YearlyPassbook yearlyPassbook = gson.fromJson(urlConnectionResponse.resultData, YearlyPassbook.class);
            mYearlyPassbook = yearlyPassbook.getResultData().getGetYearlyData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + yearlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(yearlyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(yearlyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(yearlyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(yearlyPassbook.getResultData().getTotalDeduction()));
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mTodayPassbookAsyncTask != null) {
            mTodayPassbookAsyncTask.cancel(true);
        }
        if (mDailyPassbookAsyncTask != null) {
            mDailyPassbookAsyncTask.cancel(true);
        }
        if (mWeeklyPassbookAsyncTask != null) {
            mWeeklyPassbookAsyncTask.cancel(true);
        }
        if (mMonthlyPassbookAsyncTask != null) {
            mMonthlyPassbookAsyncTask.cancel(true);
        }
        if (mYearlyPassbookAsyncTask != null) {
            mYearlyPassbookAsyncTask.cancel(true);
        }
        super.onDestroyView();

    }
}

/**
 * ViewPagerAdapter for Home Passbook fragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This ViewPagerAdapter class contain all the fragment class of daily/monthly/yearly/today/weekly passbook
 */
class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}