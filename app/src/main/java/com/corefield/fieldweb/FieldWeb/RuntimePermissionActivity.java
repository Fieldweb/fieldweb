package com.corefield.fieldweb.FieldWeb;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

public class RuntimePermissionActivity extends AppCompatActivity {

    Button btn_noThanks, btn_turnOn;
    public static final String PERMISSION_REQUEST_ACCESS_ALL = "all permission";
    private static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private String PERMISSION_REQUESTED_FOR = "";
    private boolean isDialogShowing = false;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int PICK_IMAGE_GALLERY = 2;
    private static final int REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING = 1001;
    private static final int REQ_CODE_VERSION_UPDATE = 530;
    int checkBackgroundLocation;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_runtime_permissions);

        btn_noThanks = (Button) findViewById(R.id.btn_noThanks);
        btn_turnOn = (Button) findViewById(R.id.btn_turnOn);

        btn_noThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btn_turnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkAllPermissionEnabled()) {
                    SharedPrefManager.getInstance(getApplicationContext()).setisFirstTimeAddMoreTechPopup(true);
                    requestToEnableAllPermission(PERMISSION_REQUEST_ACCESS_ALL);
                }
            }
        });

    }

    public boolean checkAllPermissionEnabled() {
        boolean flag = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //final int checkCamera = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.CAMERA);
            //final int checkStorage = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int checkFineLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int checkCoarseLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            //final int checkMicroPhone = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.RECORD_AUDIO);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                checkBackgroundLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                if (/*checkCamera == PackageManager.PERMISSION_GRANTED && checkStorage == PackageManager.PERMISSION_GRANTED
                        &&*/ checkFineLocation == PackageManager.PERMISSION_GRANTED && checkCoarseLocation == PackageManager.PERMISSION_GRANTED
                        && checkBackgroundLocation == PackageManager.PERMISSION_GRANTED /*&&
                        checkMicroPhone == PackageManager.PERMISSION_GRANTED*/) {  //&& checkPhoneCall == PackageManager.PERMISSION_GRANTED && checkSendSMS == PackageManager.PERMISSION_GRANTED

                    flag = true;
                    /* Intent intent = new Intent(RuntimePermissionActivity.this, WelcomeSliderActivity.class);*/
                    Intent intent = new Intent(RuntimePermissionActivity.this, LoginTouchlessActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else {
                if (/*checkCamera == PackageManager.PERMISSION_GRANTED && checkStorage == PackageManager.PERMISSION_GRANTED
                        &&*/ checkFineLocation == PackageManager.PERMISSION_GRANTED && checkCoarseLocation == PackageManager.PERMISSION_GRANTED /*&&
                        checkMicroPhone == PackageManager.PERMISSION_GRANTED*/) {  //&& checkPhoneCall == PackageManager.PERMISSION_GRANTED && checkSendSMS == PackageManager.PERMISSION_GRANTED

                    flag = true;
                    /*Intent intent = new Intent(RuntimePermissionActivity.this, WelcomeSliderActivity.class);*/
                    Intent intent = new Intent(RuntimePermissionActivity.this, LoginTouchlessActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        } else {
            flag = true;
            /*Intent intent = new Intent(RuntimePermissionActivity.this, WelcomeSliderActivity.class);*/
            Intent intent = new Intent(RuntimePermissionActivity.this, LoginTouchlessActivity.class);
            startActivity(intent);
            finish();

        }
        return flag;
    }


    public boolean requestToEnableAllPermission(String permissionRequestedFeature) {
        //NOTE: Log GA event
        /*Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID,SharedPrefManager.getInstance(getApplicationContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME,BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE,SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM,FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(RuntimePermissionActivity.this).logEvent(FirebaseGoogleAnalytics.Event.ENABLE_PERMISSION, bundle);*/

        // Only for android 12 & 13
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.S_V2 || Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU) {
            PERMISSION_REQUESTED_FOR = permissionRequestedFeature;
            final int chkTiramisuFineLoc = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int chkTiramisuCoarseLoc = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            final int chkTiramisuBackgroundLoc = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

            if (chkTiramisuCoarseLoc != PackageManager.PERMISSION_GRANTED || chkTiramisuFineLoc != PackageManager.PERMISSION_GRANTED
                    || chkTiramisuBackgroundLoc != PackageManager.PERMISSION_GRANTED)
                checkAndSetPermissionsTiramisu();
            return false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PERMISSION_REQUESTED_FOR = permissionRequestedFeature;
            //final int checkCamera = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.CAMERA);
            // final int checkStorage = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            final int checkFineLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
            final int checkCoarseLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            // final int checkMicroPhone = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.RECORD_AUDIO);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                checkBackgroundLocation = ContextCompat.checkSelfPermission(RuntimePermissionActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                if (/*checkCamera != PackageManager.PERMISSION_GRANTED || checkStorage != PackageManager.PERMISSION_GRANTED
                        ||*/ checkFineLocation != PackageManager.PERMISSION_GRANTED || checkCoarseLocation != PackageManager.PERMISSION_GRANTED
                        || checkBackgroundLocation != PackageManager.PERMISSION_GRANTED
                    /* || checkMicroPhone != PackageManager.PERMISSION_GRANTED*/) {  //|| checkPhoneCall != PackageManager.PERMISSION_GRANTED || checkSendSMS != PackageManager.PERMISSION_GRANTED
                    //Check and set permission above Android 10
                    checkAndSetPermissions();
                    return false;
                } else {
                    // All permission granted
                    //if user grant permission from outside
                    // continue with the screen
                    return true;
                }
            } else {
                if (/*checkCamera != PackageManager.PERMISSION_GRANTED || checkStorage != PackageManager.PERMISSION_GRANTED
                        ||*/ checkFineLocation != PackageManager.PERMISSION_GRANTED || checkCoarseLocation != PackageManager.PERMISSION_GRANTED
                    /*|| checkMicroPhone != PackageManager.PERMISSION_GRANTED*/
                ) {  //|| checkPhoneCall != PackageManager.PERMISSION_GRANTED || checkSendSMS != PackageManager.PERMISSION_GRANTED
                    //Check and set permission Below Android 10
                    checkAndPerSetBelowAndroidQ();
                    return false;
                } else {
                    // All permission granted
                    //if user grant permission from outside
                    // continue with the screen
                    return true;
                }
            }
        } else {
            // device is not  M or  M>
            // continue with the screen
            return true;
        }
    }

    private void checkAndPerSetBelowAndroidQ() {
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.CAMERA)) {
            requestPermissionBelowAndroidQ();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissionBelowAndroidQ();
        } else*/
        if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissionBelowAndroidQ();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestPermissionBelowAndroidQ();
        } else {
            requestPermissionBelowAndroidQ();
        }
    }

    private void requestPermissionBelowAndroidQ() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            /*Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,*/
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            /* Manifest.permission.RECORD_AUDIO*/
                    },
                    REQUEST_CODE_PERMISSIONS);
        }
    }


    private void checkAndSetPermissions() {
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.CAMERA)) {
            requestRunTimePermissions();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestRunTimePermissions();
        } else*/
        if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestRunTimePermissions();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestRunTimePermissions();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
            requestRunTimePermissions();
        } else {
            requestRunTimePermissions();
        }
    }

    private void checkAndSetPermissionsTiramisu() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            requestRunTimePermissionTiramisu();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && ActivityCompat.shouldShowRequestPermissionRationale(RuntimePermissionActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
            requestRunTimePermissionTiramisu();
        } else {
            requestRunTimePermissionTiramisu();
        }
    }

    private void requestRunTimePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                            /* Manifest.permission.CAMERA,*/
                            /* Manifest.permission.WRITE_EXTERNAL_STORAGE,*/
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            /* Manifest.permission.RECORD_AUDIO*/
//                            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                    },
                    REQUEST_CODE_PERMISSIONS);
        }
    }

    private void requestRunTimePermissionTiramisu() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU) {
            requestPermissions(new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                boolean userAllowed = true;
                for (final int result : grantResults) {
                    userAllowed &= result == PackageManager.PERMISSION_GRANTED;
                }
                if (userAllowed) {
                    // All permissions granted
                    try {
                        // SAVE FIRST INSTANCE IN SHARED PREF & DON'T CALL EVERYTIME
                        SharedPrefManager.getInstance(getApplicationContext()).setPermissionEnabled(true);

                        /*Intent intent = new Intent(RuntimePermissionActivity.this, WelcomeSliderActivity.class);*/
                        Intent intent = new Intent(RuntimePermissionActivity.this, LoginTouchlessActivity.class);
                        startActivity(intent);
                        finish();

                    } catch (Exception e) {
                        e.getMessage();
                    }
                } else {
                    if (!isDialogShowing) {
                        isDialogShowing = true;
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                        alertDialogBuilder.setTitle(R.string.permissions_required)
                                .setMessage("You have denied permissions to some features which are required by this app. " +
                                        "Below is the list of features which should be enabled.\n * Camera\n * Storage\n * Location \nPlease go to settings and enable permissions.")
                                .setPositiveButton(R.string.open_settings, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Code to go to setting app
                                        //or continue with the screen
                                        isDialogShowing = false;
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.setData(Uri.fromParts("package", getPackageName(), null));
                                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivityForResult(intent, REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING);
                                    }
                                })
                                .setCancelable(false)
                                .create()
                                .show();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //FWLogger.logInfo(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PERMISSON_ENABLE_FROM_SETTTING) {
            if (checkAllPermissionEnabled()) {
                // mListener.onRequestPermissionsUpdate(true, PERMISSION_REQUESTED_FOR);
            }
        } else if (requestCode == REQ_CODE_VERSION_UPDATE) {
            //FWLogger.logInfo(TAG, "REQ_CODE_VERSION_UPDATE");
            if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                //FWLogger.logInfo(TAG, "Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                //unregisterInstallStateUpdListener();
                finish();
            } else if (resultCode == RESULT_OK) {
                // Make it true to know user first time opens the app after version update
                SharedPrefManager.getInstance(getApplicationContext()).setFirstTime(true);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
