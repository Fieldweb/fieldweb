package com.corefield.fieldweb.FieldWeb.Passbook;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.MonthlyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.MonthlyPassbookAsyncTask;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;

/**
 * Fragment Controller for Monthly Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show passbook of monthly bar graph
 */
public class MonthlyPassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = MonthlyPassbookFragment.class.getSimpleName();
    public List<MonthlyPassbook.MonthlyALlDataList> mMonthlyPassbook = null;
    private View mRootView;
    private BarChart mBarChart;
    private MonthlyPassbookAsyncTask mMonthlyPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.passbook_graph_fragment, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {
        mBarChart = mRootView.findViewById(R.id.barchart);
        mBarChart.setDescription("");
        mBarChart.setMaxVisibleValueCount(50);
        mBarChart.setPinchZoom(false);
        mBarChart.animateX(2000);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisLeft().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawGridLines(false);
        mMonthlyPassbookAsyncTask = new MonthlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mMonthlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(MonthlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            MonthlyPassbook monthlyPassbook = gson.fromJson(urlConnectionResponse.resultData, MonthlyPassbook.class);
            mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + monthlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            monthlyPassbook.getResultData().getTotalEarned();
            monthlyPassbook.getResultData().getTotalCredit();
            monthlyPassbook.getResultData().getTotalExpenses();
            monthlyPassbook.getResultData().getTotalDeduction();
            monthlyPassbook.getResultData().getTotalOpening();
            /////////////////////////////////////////BAR CHART ////////////////////////////////////
            ArrayList<BarEntry> estimated = new ArrayList<>();

            for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                estimated.add(new BarEntry(mMonthlyPassbook.get(i).getEstimated(), i));
            }

            ArrayList<BarEntry> earned = new ArrayList<>();
            for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                earned.add(new BarEntry(mMonthlyPassbook.get(i).getEarning(), i));
            }

            ArrayList<BarEntry> expense = new ArrayList<>();
            for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                expense.add(new BarEntry(mMonthlyPassbook.get(i).getExpenses(), i));
            }

            XAxis x = mBarChart.getXAxis();
            x.setSpaceBetweenLabels(0);
            x.setXOffset(.5f);
            x.setYOffset(.5f);

            BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
//            expenseBar.setColors(new int[]{R.color.expense}, getContext());
            expenseBar.setColors(new int[]{R.color.orange}, getContext());

            BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
//            estimatedBar.setColors(new int[]{R.color.estimated}, getContext());
            estimatedBar.setColors(new int[]{R.color.green}, getContext());

            BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
//            earnedBar.setColors(new int[]{R.color.earned}, getContext());
            earnedBar.setColors(new int[]{R.color.blue}, getContext());

            ArrayList<String> labels = new ArrayList<String>();
            labels.add("Jan");
            labels.add("Feb");
            labels.add("Mar");
            labels.add("Apr");
            labels.add("May");
            labels.add("Jun");
            labels.add("July");
            labels.add("Aug");
            labels.add("Sep");
            labels.add("Oct");
            labels.add("Nov");
            labels.add("Dec");

            ArrayList<BarDataSet> dataSets = new ArrayList<>();
            dataSets.add(expenseBar);
            dataSets.add(estimatedBar);
            dataSets.add(earnedBar);
            BarData data = new BarData(labels, dataSets);
            mBarChart.setData(data);
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        if (mMonthlyPassbookAsyncTask != null) {
            mMonthlyPassbookAsyncTask.cancel(true);
        }

        super.onDestroyView();

    }
}
