package com.corefield.fieldweb.FieldWeb.BackgroundTask;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.TravelPathHistory.TravelledPathHistory;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.google.gson.Gson;

import org.json.JSONArray;

/**
 * Async Task for Add Technician
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Async Task class is used to call - Add Technician API
 */
public class TeleCMIAsyncTask extends BaseAsyncTask {

    protected static String TAG = TeleCMIAsyncTask.class.getSimpleName();

    public TeleCMIAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                Gson gson = new Gson();

                String data = gson.toJson(objects[0]);
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(data);
                    FWLogger.logInfo(TAG, jsonArray.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (jsonArray != null) {
                    URLConnectionRequest URLConnectionRequest = new URLConnectionRequest();
                    if (jsonArray != null && URLConnectionRequest != null) {
                        //No need to pass data to request builder as Gson gives data in string JSON format
                        Request request = RequestBuilder.TeleCMIbuildArrayRequest(mContext, "/ind_pcmo_make_call"/*"/pcmo_make_call"*/, jsonArray);
                        mUrlConnectionResponse = URLConnectionRequest.sendPostRequest(request.getmURL(), request.getmHeader(), request.getmArrayData().toString().substring(1, 260/*254*/), request.getToken());
                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }

            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null && mUrlConnectionResponse != null) {
            if (mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (status) {
                    if (onTaskCompleteListener != null)
                        onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, TravelledPathHistory.class.getSimpleName());
                } else {
                    //Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
