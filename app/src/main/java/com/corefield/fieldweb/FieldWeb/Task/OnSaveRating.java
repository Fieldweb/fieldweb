package com.corefield.fieldweb.FieldWeb.Task;

/**
 * //
 * Created by CFS on 6/2/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Task
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : Interface to get rating from signature view
 * //
 **/
public interface OnSaveRating {
    void onRating(float rating, String personName,String signedMobileNo);
}
