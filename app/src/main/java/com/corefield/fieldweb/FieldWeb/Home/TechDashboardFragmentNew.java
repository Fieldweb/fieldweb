package com.corefield.fieldweb.FieldWeb.Home;

import static android.content.Context.ALARM_SERVICE;
import static com.corefield.fieldweb.FieldWeb.Database.DatabaseHandler.TABLE_TRAVELLED_PATH_DATA;
import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.corefield.fieldweb.Adapter.TasksListAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.HealthAndSaftey.HealthAndSafety;
import com.corefield.fieldweb.DTO.Task.TaskStatusCount;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.TravelPathHistory.TravelledPathHistory;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddTravelledPathHistroyAsyncTask;
import com.corefield.fieldweb.FieldWeb.Database.DatabaseHandler;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.HealthAndSafety.HealthAndSafetyActivity;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TechPaymentReceivedFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.LocationService.GoogleLocAlarmReceiver;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.gson.Gson;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


/**
 * Fragment Controller for Technician Dashboard Fragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used for to show Technician  Dashboard component
 */

public class TechDashboardFragmentNew extends Fragment implements OnTaskCompleteListener {

    protected static String TAG = TechDashboardFragmentNew.class.getSimpleName();
    public List<TaskStatusCount.ResultData> mTaskStatusCount = null;
    public static boolean isDataAvailable = false;
    private View mRootView;
    private TextView mTextViewComplete, mTextViewOngoing, mTextViewInactive, mTextViewReject, mTextView_onhold, mTextViewDataFor, mTextViewTotalTask, textProfilePercctage, textProfileEdit, profileCompletion;
    private FitChart mFitChart;
    DatabaseHandler databaseHandler;
    ArrayList<TravelledPathHistory.ResultData> travelledPathHistories = null;
    AddTravelledPathHistroyAsyncTask addTravelledPathHistroyAsyncTask;
    private static final int PERMISSION_REQUEST_ACTIVITY_RECOGNITION = 1;

    public List<TasksList.ResultData> mTasksLists;
    private TasksListAdapter mTasksListAdapter;
    private RecyclerView mTaskListRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerTouchListener mRecyclerTouchListener;
    //private GetHealthAndSafetyFeatureAsyncTask mGetHealthAndSafetyFeatureAsyncTask;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START;
    private boolean isLoadFirstTime = false;
    private String mSearchParam = "";
    Resources mResources;
    private boolean isAllData = false;
    private String selectedFilter = "";
    private RoundCornerProgressBar mprogress_Profile;
    private UserDetails.ResultData mUserDetails;
    private float mTotalProfilePercentage;
    Activity mActivity;
    private int mfirstName, mlastName, mConactNum, mAddress, mEmailid, mDOB, mProfileImg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tech_home_fragment_new, container, false);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mSpinnerMonthYear.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mEditTextYearMonth.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setVisibility(View.VISIBLE);
        inIt();
        return mRootView;
    }

    private void inIt() {
        if (getContext() != null) mResources = getContext().getResources();

        databaseHandler = new DatabaseHandler(getActivity());

        mFitChart = (FitChart) mRootView.findViewById(R.id.fitChart);
        mTextViewComplete = mRootView.findViewById(R.id.textView_completed);
        mTextViewOngoing = mRootView.findViewById(R.id.textView_ongoing);
        mTextViewInactive = mRootView.findViewById(R.id.textView_inactive);
        mTextViewReject = mRootView.findViewById(R.id.textView_reject);
        mTextView_onhold = mRootView.findViewById(R.id.textView_onhold);
        mTextViewTotalTask = mRootView.findViewById(R.id.textView_total_task);
        mTextViewDataFor = mRootView.findViewById(R.id.textView_data_for);
        mprogress_Profile = mRootView.findViewById(R.id.mprogress_Profile);
        textProfilePercctage = mRootView.findViewById(R.id.textProfilePercctage);
        textProfileEdit = mRootView.findViewById(R.id.textProfileEdit);
        profileCompletion = mRootView.findViewById(R.id.profileCompletion);
        mActivity = getActivity();
//        callAPIForTaskStatusCount();

        mTaskListRecyclerView = mRootView.findViewById(R.id.recycler_taskList);
        mTasksListAdapter = new TasksListAdapter(getContext(), mTasksLists);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mTaskListRecyclerView.setLayoutManager(mLayoutManager);
        mTaskListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mTaskListRecyclerView.setAdapter(mTasksListAdapter);

        // START :
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted, request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACTIVITY_RECOGNITION}, PERMISSION_REQUEST_ACTIVITY_RECOGNITION);
            } else {
                // Permission has already been granted
                // Perform your task here
            }
        }
        //END


        mSearchParam = "";
        mockingNetworkDelay(mSearchParam, 1);

//        ((HomeActivityNew) getActivity()).getMonthYearList();

        selectedFilter = ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.getSelectedItem().toString();
        callAPIForTaskStatusCount();

        /*((HomeActivityNew) getActivity()).mSpinnerMonthYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "onItemSelected called : " + position);
                String selectedTaskType = parent.getItemAtPosition(position).toString();
                FWLogger.logInfo(TAG, "selected called : " + selectedTaskType);
                ((HomeActivityNew) getActivity()).mSelectedMonth = ((HomeActivityNew) getActivity()).monthList.get(position);
                FWLogger.logInfo(TAG, "Month Number : " + ((HomeActivityNew) getActivity()).mSelectedMonth);
                callAPIForTaskStatusCount();
                String[] mons = new DateFormatSymbols(Locale.ENGLISH).getMonths();
                ((HomeActivityNew) getActivity()).mSelectedMonthName = mons[((HomeActivityNew) getActivity()).mSelectedMonth - 1];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Day Filter called : " + position);
                selectedFilter = parent.getItemAtPosition(position).toString();
//                Toast.makeText(getActivity(), "Selected : "+selectedFilter, Toast.LENGTH_SHORT).show();
                mTextViewDataFor.setText(DateUtils.getSelectedDate(selectedFilter));
                callAPIForTaskStatusCount();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mTextViewDataFor.setText(DateUtils.getSelectedDate(selectedFilter));

        mTaskListRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        recyclerViewOperations();

        // PROFILE TOTAL % CALCULATION : START
        try {
            getProfileDetails();
        } catch (Exception e) {
            e.getMessage();
        }

        textProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ProfileFragmentNew profileFragmentNew = new ProfileFragmentNew();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, profileFragmentNew);
                    transaction.commit();
                } catch (Exception ex) {
                    ex.getMessage();
                }

            }
        });
        // PROFILE TOTAL % CALCULATION : END

        // START TRACKING DATA
        try {
            autoSyncTravelledPath();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void getProfileDetails() {
       /* mProfileDetailsAsyncTask = new GetProfileDetailsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mProfileDetailsAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<UserDetails> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getProfileDetails(userID);
                call.enqueue(new retrofit2.Callback<UserDetails>() {
                    @Override
                    public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                        try {
                            if (response.code() == 200) {
                                UserDetails userDetails = response.body();
                                //mUserDetails = new UserDetails.ResultData();
                                mUserDetails = userDetails.getResultData();
                                try {
                                    mTotalProfilePercentage = Float.parseFloat(mUserDetails.getProgressBarPercentage());

                                    try {
                                        if (mTotalProfilePercentage != 100) {
                                            int totalProfileCompletion = 0;
                                            totalProfileCompletion = 100;
                                            mprogress_Profile.setMax(totalProfileCompletion);
                                            mprogress_Profile.setProgress(mTotalProfilePercentage);
                                            textProfilePercctage.setText(mTotalProfilePercentage + "%");
                                            textProfileEdit.setVisibility(View.VISIBLE);
                                        } else {
                                            textProfilePercctage.setVisibility(View.GONE);
                                            mprogress_Profile.setVisibility(View.GONE);
                                            profileCompletion.setVisibility(View.GONE);
                                        }
                                    } catch (Exception ex) {
                                        ex.getMessage();
                                    }

                                    // FOR APP TOUR : TECH
                                    if (!SharedPrefManager.getInstance(getApplicationContext()).isAppTourCompleted()) {
                                        TechAppTour_Skippable();
                                    }
                                    // ON DEMAND APP TOUR WITH FLAG
                                    if (HomeActivityNew.onDemandAppTourFlag) {
                                        OnDemandTechAppTour(getActivity());
                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
            ex.getMessage();
        }
    }

    /*private void callAPIForTaskStatusCount() {
        mTechHomeTaskStatusCountAsyncTask = new TechHomeTaskStatusCountAsyncTaskNew(getContext(), BaseAsyncTask.Priority.LOW, this);
        mTechHomeTaskStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), ((HomeActivityNew) getActivity()).mSelectedDate, ((HomeActivityNew) getActivity()).mSelectedMonth, ((HomeActivityNew) getActivity()).mSelectedYear);
    }*/

    public void callAPIForTaskStatusCount() {
       /* mOwnHomeTaskStatusCountAsyncTask = new OwnHomeTaskStatusCountAsyncTaskNew(getContext(), BaseAsyncTask.Priority.LOW, this);
        mOwnHomeTaskStatusCountAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<TaskStatusCount> call = RetrofitClient.getInstance(getContext()).getMyApi().getTaskStatusCount(SharedPrefManager.getInstance(getContext()).getUserId(), selectedFilter.toLowerCase());
                call.enqueue(new retrofit2.Callback<TaskStatusCount>() {
                    @Override
                    public void onResponse(Call<TaskStatusCount> call, Response<TaskStatusCount> response) {
                        try {
                            if (response.code() == 200) {
                                TaskStatusCount taskStatusCount = response.body();
                                mTaskStatusCount = taskStatusCount.getResultData();
                                FWLogger.logInfo(TAG, "TaskStatus Count " + taskStatusCount.getMessage());

                                Collection<FitChartValue> values = new ArrayList<>();

                                int totalTaskCount = 0;
                                String taskType = "";
                                if (taskStatusCount.getCode().equalsIgnoreCase("200") && taskStatusCount.getMessage().equalsIgnoreCase("success")) {
                                    mTextViewComplete.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.green)));

                                    mTextViewReject.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                                    mTextViewOngoing.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                                    mTextViewInactive.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                                    mTextView_onhold.setText("00");
                                    values.add(new FitChartValue(0, mResources.getColor(R.color.onhold)));

                                    mFitChart.setMinValue(0f);
                                    for (int i = 0; i < mTaskStatusCount.size(); i++) {
                                        totalTaskCount = totalTaskCount + mTaskStatusCount.get(i).getTaskcount();
                                        taskType = mTaskStatusCount.get(i).getName();
                                        switch (taskType) {
                                            case "Completed":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewComplete.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewComplete.setText("" + mTaskStatusCount.get(i).getTaskcount());
                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.green)));
                                                break;

                                            case "Rejected":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewReject.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewReject.setText("" + mTaskStatusCount.get(i).getTaskcount());
                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.colorPrimaryDark)));
                                                break;

                                            case "Ongoing":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewOngoing.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewOngoing.setText("" + mTaskStatusCount.get(i).getTaskcount());
                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.orange)));
                                                break;

                                            case "InActive":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextViewInactive.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextViewInactive.setText("" + mTaskStatusCount.get(i).getTaskcount());
                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.light_gray)));
                                                break;

                                            case "OnHold":
                                                if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                                    mTextView_onhold.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                                                else
                                                    mTextView_onhold.setText("" + mTaskStatusCount.get(i).getTaskcount());
                                                values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.onhold)));
                                                break;

                                            default:
                                                break;
                                        }

                                    }
                                    mFitChart.setMaxValue(totalTaskCount);
                                    mFitChart.setValues(values);
                                    mTextViewTotalTask.setText("" + totalTaskCount);

                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TaskStatusCount> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetTaskDataForDashboard? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTaskDataForDashboard? API:");
            ex.getMessage();
        }
    }

    private void mockingNetworkDelay(String searchParam, int typeId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getTaskList(searchParam, typeId);
            }
        }, 1000);
    }

    /*private void checkHealthAndSafetyFeatureEnabled() {
        mGetHealthAndSafetyFeatureAsyncTask = new GetHealthAndSafetyFeatureAsyncTask(getActivity(), BaseAsyncTask.Priority.LOW, this);
        mGetHealthAndSafetyFeatureAsyncTask.execute();
    }*/

    protected void getTaskList(String searchParam, int taskTypeId) {
        FWLogger.logInfo(TAG, "getTaskList()");
        if (getContext() != null) {
           /* mGetTodayTaskListAsyncTask = new GetTodayTaskListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, TechDashboardFragmentNew.this);
            mGetTodayTaskListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), mCurrentPage, isAllData);*/

            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    Call<TasksList> call = RetrofitClient.getInstance(getContext()).getMyApi().getTodayTaskList(SharedPrefManager.getInstance(getContext()).getUserId(), mCurrentPage, isAllData);
                    call.enqueue(new retrofit2.Callback<TasksList>() {
                        @Override
                        public void onResponse(Call<TasksList> call, Response<TasksList> response) {
                            try {
                                if (response.code() == 200) {
                                    TasksList tasksList = response.body();
                                    if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                                        if (tasksList != null) {
                                            FWLogger.logInfo(TAG, "success");
                                            setResults(tasksList);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<TasksList> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in TodayTaskListNew? API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in TodayTaskListNew? API:");
                ex.getMessage();
            }

        }
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        getTaskList(mSearchParam, 1);
    }

    public void recyclerViewOperations() {
        mRecyclerTouchListener = new RecyclerTouchListener() {
            @Override
            public void onClick(View view, int position) {
                TasksList.ResultData data = mTasksListAdapter.getItem(position);
                Bundle bundle = new Bundle();
                if (data != null) {
                    bundle.putString("Task Status", data.getTaskStatus());
                    bundle.putString("Task Name", data.getName());
                    bundle.putString("Task Assignee", data.getAssignedTo());
                    bundle.putInt("Task Amount", data.getWagesPerHours());
                    bundle.putString("Image", "a");
                    bundle.putString("FieldLat", data.getLatitude());
                    bundle.putString("FieldLong", data.getLongitude());
                    bundle.putString("TaskDescription", data.getDescription()); // task desc
                    bundle.putString("AddressDesc", data.getLocationDesc()); // Address desc
                    bundle.putString("ShortAddress", data.getLocationName()); //TODO error
                    bundle.putString("ContactNo", data.getContactNo());
                    bundle.putString("CustomerName", data.getCustomerName());
                    bundle.putInt("TaskId", data.getId());
                    bundle.putInt("TaskStatusId", data.getTaskStatusId());
                    bundle.putLong("StartDate", data.getStartDate());
                    bundle.putLong("EndDate", data.getEndDate());
                    bundle.putInt("TaskState", data.getTaskState());
                    bundle.putString("FullAddress", data.getFullAddress());
                    bundle.putString("PaymentMode", data.getPaymentMode());
                    bundle.putInt("PaymentModeId", data.getPaymentModeId());
                    bundle.putString("TaskDate", data.getTaskDate());
                    bundle.putInt("OwnerId", data.getOwnerId());
                    bundle.putString("CustomerEmailId", data.getCustomerEmailId());
                    bundle.putString("AudioFilePath", data.getAudioFilePath());
                    //
                    try {
                        bundle.putString("DeviceInfoImagePath", data.getPreDeviceInfoDto().getDeviceInfoImagePath());
                        bundle.putString("DeviceInfoImagePath1", data.getPreDeviceInfoDto().getDeviceInfoImagePath1());
                        bundle.putString("DeviceInfoImagePath2", data.getPreDeviceInfoDto().getDeviceInfoImagePath2());
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    try {
                        ArrayList<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
                        if (data.getMultipleItemAssigned().size() > 0) {
                            for (int i = 0; i < data.getMultipleItemAssigned().size(); i++) {
                                TasksList.MultipleItemAssigned multipleItemAssigned = new TasksList.MultipleItemAssigned();
                                multipleItemAssigned.setItemId(data.getMultipleItemAssigned().get(i).getItemId());
                                multipleItemAssigned.setItemQuantity(data.getMultipleItemAssigned().get(i).getItemQuantity());
                                multipleItemAssigned.setItemName(data.getMultipleItemAssigned().get(i).getItemName());
                                multipleItemAssignedArrayList.add(multipleItemAssigned);
                            }
                            bundle.putSerializable("MultipleItemAssigned", multipleItemAssignedArrayList);
                        }

                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    FWLogger.logInfo("PaymentMode : ", " " + data.getPaymentMode());
                    FWLogger.logInfo("PaymentModeId : ", " " + data.getPaymentModeId());
                }
                //Check for PNR(Payment Not Received)
                FWLogger.logInfo(TAG, "getPaymentNotReceived  = " + data.getPaymentNotReceived());

                if (data.getTaskState() == TaskState.NOT_STARTED || data.getTaskState() == TaskState.STARTED_NOT_ENDED) {
                    if (!mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Rejected.name())) {
                        if (data.getTaskStatusId() == TaskStatus.IN_ACTIVE && data.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name())) {
                           /* if (DateUtils.compareTo(data.getTaskDate()) != 1) {
                                TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                taskDialogNew.acceptTaskDialog(getActivity(), data, TaskStatus.IN_ACTIVE);
                            } else
                                Toast.makeText(getActivity(), R.string.task_expired, Toast.LENGTH_SHORT).show();*/
                            // START
                            Date c = Calendar.getInstance().getTime();
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String strCurDate = df.format(c);
                            String strTaskDate[] = data.getTaskDate().split("T");
                            try {
                                Date date1; // Task Date
                                Date date2; // Current Date
                                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");
                                date1 = dates.parse(strTaskDate[0]);
                                date2 = dates.parse(strCurDate);
                                long difference = Math.abs(date1.getTime() - date2.getTime());
                                //long differenceDates = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
                                long differenceDates = difference / (24 * 60 * 60 * 1000);
                                //String dayDifference = Long.toString(differenceDates);

                                if (differenceDates > 3 && date1.compareTo(date2) < 0) { // date 1 occurs after date 2
                                    Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                } else if (date1.compareTo(date2) > 0) { // FUTURE DATE
                                    Toast.makeText(getActivity(), "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                                } else if (differenceDates == 0 || differenceDates == 1 || differenceDates == 2 || differenceDates == 3 && date1.compareTo(date2) <= 0 && differenceDates <= 3) { //FOR PAST DATE AND CURRENT DATE ACCEPT:: IF task date is before than current
                                    ((HomeActivityNew) getActivity()).setupAlarm(900000);
                                    TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                    taskDialogNew.acceptTaskDialog(getActivity(), data, TaskStatus.IN_ACTIVE);
                                } else if (differenceDates < 3 && date1.compareTo(date2) < 0) {
                                    Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                } else if (differenceDates >= 1) {
                                    Toast.makeText(getActivity(), "It's too Early to Accept the Task!", Toast.LENGTH_SHORT).show();
                                } else if (differenceDates <= 1) {
                                    Toast.makeText(getActivity(), "It's too Late to Accept the Task!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //END
                        } else if (data.getTaskStatusId() == TaskStatus.ON_GOING && data.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.Ongoing.name())) {
                            ((HomeActivityNew) getActivity()).navigateToTaskNavigation(data);
                        }
                    }
                }

                /*if (data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING) && data.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
                    TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    taskClosureFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                    transaction.commit();
                }
                if (data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && data.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                    TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    techPaymentReceivedFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
                    transaction.commit();
                }*/
                if (data.getPaymentMode() != null && data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getTaskStatusId() == TaskStatus.ON_GOING && data.isTaskClosureStatus() == true && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && data.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                    TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    techPaymentReceivedFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    //FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    //NOTE : Do not add to back stack as we are navigating from back press in TechTaskTrackingFragment and TechPaymentReceivedFragment
                    transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
                    transaction.commit();
                } else if (data.getPaymentMode() != null && data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getTaskStatusId() == TaskStatus.ON_GOING && data.isTaskClosureStatus() == false && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && data.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                    TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    taskClosureFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                    transaction.commit();
                }
                if (data.getPaymentMode() != null && data.getTaskState() == TaskState.ENDED_NO_PAYMENT && data.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.AMC_STRING) && data.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {
                    TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    taskClosureFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                    transaction.commit();
                }

                /*if (data.getTaskState() == TaskState.ENDED_NO_PAYMENT) {
                    TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    taskClosureFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                    transaction.commit();
                }*/

                if (data.getTaskState() == TaskState.PAYMENT_RECEIVED && !data.isTaskClosureStatus() && !mTasksListAdapter.getItem(position).getTaskStatus().equals(Constant.TaskCategories.Completed.name())) {
                    TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
                    bundle.putSerializable("taskResult", data);
                    taskClosureFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, taskClosureFragment);
                    transaction.commit();
                } else if (data.getTaskState() == TaskState.PAYMENT_RECEIVED && data.isTaskClosureStatus()) {
                    Toast.makeText(getContext(), R.string.this_task_completed_and_payment_is_received, Toast.LENGTH_SHORT).show();
                }
            }
        };

        mTasksListAdapter.setClickListener(mRecyclerTouchListener);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(TaskStatusCount.class.getSimpleName())) {
            Gson gson = new Gson();
            TaskStatusCount taskStatusCount = gson.fromJson(urlConnectionResponse.resultData, TaskStatusCount.class);
            mTaskStatusCount = taskStatusCount.getResultData();
            FWLogger.logInfo(TAG, "TaskStatus Count " + taskStatusCount.getMessage());

            Collection<FitChartValue> values = new ArrayList<>();

            int totalTaskCount = 0;
            String taskType = "";
            if (taskStatusCount.getCode().equalsIgnoreCase("200") && taskStatusCount.getMessage().equalsIgnoreCase("success")) {
                mTextViewComplete.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.green)));

                mTextViewReject.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                mTextViewOngoing.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                mTextViewInactive.setText("00");
                values.add(new FitChartValue(0, mResources.getColor(R.color.red)));

                mFitChart.setMinValue(0f);
                for (int i = 0; i < mTaskStatusCount.size(); i++) {
                    totalTaskCount = totalTaskCount + mTaskStatusCount.get(i).getTaskcount();
                    taskType = mTaskStatusCount.get(i).getName();
                    switch (taskType) {
                        case "Completed":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewComplete.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewComplete.setText("" + mTaskStatusCount.get(i).getTaskcount());
                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.green)));
                            break;

                        case "Rejected":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewReject.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewReject.setText("" + mTaskStatusCount.get(i).getTaskcount());
                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.colorPrimaryDark)));
                            break;

                        case "Ongoing":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewOngoing.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewOngoing.setText("" + mTaskStatusCount.get(i).getTaskcount());
                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.orange)));
                            break;

                        case "InActive":
                            if (mTaskStatusCount.get(i).getTaskcount() <= 9)
                                mTextViewInactive.setText("0" + mTaskStatusCount.get(i).getTaskcount());
                            else
                                mTextViewInactive.setText("" + mTaskStatusCount.get(i).getTaskcount());
                            values.add(new FitChartValue(mTaskStatusCount.get(i).getTaskcount(), mResources.getColor(R.color.light_gray)));
                            break;

                        default:
                            break;
                    }

                }
                mFitChart.setMaxValue(totalTaskCount);
                mFitChart.setValues(values);
                mTextViewTotalTask.setText("" + totalTaskCount);

            }

        } else if (classType.equalsIgnoreCase(TasksList.class.getSimpleName())) {
            Gson gson = new Gson();
            TasksList tasksList = gson.fromJson(urlConnectionResponse.resultData, TasksList.class);
            if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                if (tasksList != null) {
                    FWLogger.logInfo(TAG, "success");
                    setResults(tasksList);
                }
            } else {
                Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        // FOR PROFILE PERCENTAGE COUNT
        else if (classType.equalsIgnoreCase(UserDetails.class.getSimpleName())) {
            Gson gson = new Gson();
            UserDetails userDetails = gson.fromJson(urlConnectionResponse.resultData, UserDetails.class);
            //mUserDetails = new UserDetails.ResultData();
            mUserDetails = userDetails.getResultData();
            try {
                mTotalProfilePercentage = Float.parseFloat(mUserDetails.getProgressBarPercentage());
                int totalProfileCompletion = 0;
                totalProfileCompletion = 100;
                mprogress_Profile.setMax(totalProfileCompletion);
                mprogress_Profile.setProgress(mTotalProfilePercentage);
                textProfilePercctage.setText(mTotalProfilePercentage + "%");

                //CHECK HEALTH SAFETY ENABLED OR NOT
               /* if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyEnabled()) {
                    int comp = DateUtils.compareTo(SharedPrefManager.getInstance(getApplicationContext()).getHealthSafetyDate());
                    if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyLogged() && comp == 0) {
                        // comp == 0 Today's date = logged date
                    } else {
                        checkHealthAndSafetyFeatureEnabled();
                    }
                } else {
                    checkHealthAndSafetyFeatureEnabled();
                }*/

                // FOR APP TOUR : TECH
                if (!SharedPrefManager.getInstance(getApplicationContext()).isAppTourCompleted()) {
                    TechAppTour_Skippable();
                }
                // ON DEMAND APP TOUR WITH FLAG
                if (HomeActivityNew.onDemandAppTourFlag) {
                    OnDemandTechAppTour(getActivity());
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        // FOR HEALTH AND SAFETY
        else if (classType.equalsIgnoreCase(HealthAndSafety.class.getSimpleName())) {
            Gson gson = new Gson();
            HealthAndSafety healthAndSafety = gson.fromJson(urlConnectionResponse.resultData, HealthAndSafety.class);
            if (healthAndSafety != null) {
                if (healthAndSafety.getResultData().getIsHealthFeaturesEnabled()) {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(true);
                    if (healthAndSafety.getResultData().getIsTodayHealthStatus().getIsHealthAndSafetyValuesAddedForToday()) {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(true);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd"));
                    } else {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                        //navigateToHealthAndSafetyActivity();
                    }
                } else {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                }
            } else {
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                Toast.makeText(getActivity(), getString(R.string.something_went_wrong) + "..!", Toast.LENGTH_LONG).show();
            }
        } else if (classType.equalsIgnoreCase(TravelledPathHistory.class.getSimpleName())) {
            Gson gson = new Gson();
            TravelledPathHistory mTravelledPathHistory = gson.fromJson(urlConnectionResponse.resultData, TravelledPathHistory.class);

            if (mTravelledPathHistory != null) {
                if (mTravelledPathHistory.getCode().equalsIgnoreCase("200") && mTravelledPathHistory.getMessage().equalsIgnoreCase("Path travel saved successfully.")) {
                    databaseHandler.deleteTable(TABLE_TRAVELLED_PATH_DATA);
                    Log.i(TAG, "Travelled Data Posted Successfully.: ");
                } else {
                    Toast.makeText(getActivity(), getString(R.string.something_went_wrong) + "..!", Toast.LENGTH_LONG).show();

                }

            }
        }
    }

    private void navigateToHealthAndSafetyActivity() {
        Intent intent = new Intent(getActivity(), HealthAndSafetyActivity.class);
        if (getActivity().getIntent().getExtras() != null) {
            if (getActivity().getIntent().getExtras().getString("NewNotification") != null) {
                intent.putExtra("NewNotification", "NewNotification");
            }
        }
        intent.putExtra("isFromOptionMenu", false);
        startActivity(intent);
        getActivity().finish();
        return;
    }

    private void setResults(TasksList tasksList) {

        List<TasksList.ResultData> mTempTasklist = new ArrayList<>();
        mTasksLists = new ArrayList<TasksList.ResultData>();
        mTempTasklist = tasksList.getResultData();
        // SHOW ONLY TODAY'S TASK
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String compDate = formatter.format(date) + "T00:00:00";
            for (int i = 0; i < mTempTasklist.size(); i++) {
                if (compDate.equalsIgnoreCase(mTempTasklist.get(i).getTaskDate())) {
                    mTasksLists.addAll(Collections.singleton(mTempTasklist.get(i)));
                    isDataAvailable = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        // END
        if (isLoadFirstTime) {
            if (mTasksLists == null || mTasksLists.isEmpty() || mTasksLists.size() <= 0) {
                isLoading = false;
                isLastPage = true;
            } else {
                mTasksListAdapter.addAll(mTasksLists);
                mTasksListAdapter.notifyDataSetChanged();
                mTasksListAdapter.addLoadingFooter();
            }
        } else {
            if (mTasksLists == null || mTasksLists.isEmpty() || mTasksLists.size() <= 0) {
                mTasksListAdapter.removeLoadingFooter();
                isLoading = false;
                isLastPage = true;
            } else {
                mTasksListAdapter.removeLoadingFooter();
                isLoading = false;
                mTasksListAdapter.addAll(mTasksLists);
                mTasksListAdapter.notifyDataSetChanged();
                mTasksListAdapter.addLoadingFooter();
            }
        }
    }

    public void OnDemandTechAppTour(Activity activity) {
        try {
            TapTarget target1 = TapTarget.forView(activity.findViewById(R.id.profileCompletion/*textProfileEdit*/), activity.getString(R.string.profile), activity.getString(R.string.complete_ur_profile)).drawShadow(true).cancelable(false).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(50);

            TapTargetSequence sequence = new TapTargetSequence(activity);
            sequence.targets(target1);
            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    sequence.cancel();
                    ((HomeActivityNew) activity).OnDemandAppGuideForTech();
                    // SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    public void TechAppTour_Skippable() {
        try {
            TapTarget target1 = TapTarget.forView(mRootView.findViewById(R.id.profileCompletion/*textProfileEdit*/), getString(R.string.profile), getString(R.string.complete_ur_profile)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(60);

            TapTargetSequence sequence = new TapTargetSequence(mActivity);
            sequence.targets(target1);
            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    sequence.cancel();
                    ((HomeActivityNew) mActivity).AppGuideForTech_Skippable();
                    SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    private void TechnicianAppTour() {
        try {
            TapTarget target1 = TapTarget.forView(mRootView.findViewById(R.id.profileCompletion/*textProfileEdit*/), getString(R.string.profile), getString(R.string.app_tour_2_title)).drawShadow(true).cancelable(true).targetCircleColor(R.color.black).transparentTarget(true).outerCircleColor(R.color.transperant_black).dimColor(R.color.black).targetRadius(60);

            TapTargetSequence sequence = new TapTargetSequence(getActivity());
            sequence.targets(target1);
            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    // ((HomeActivityNew) getActivity()).AppGuideForTechnician();
                    // SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void autoSyncTravelledPath() {
        try {
            travelledPathHistories = new ArrayList<TravelledPathHistory.ResultData>();
            travelledPathHistories = databaseHandler.getTravelledPathData();

            if (travelledPathHistories.size() > 0) {
                addTravelledPathHistroyAsyncTask = new AddTravelledPathHistroyAsyncTask(getActivity(), BaseAsyncTask.Priority.LOW, this);
                addTravelledPathHistroyAsyncTask.execute(travelledPathHistories);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void setupAlarm(int interval) {
        try {
            AlarmManager am = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
            Intent i = new Intent(getActivity().getBaseContext(), GoogleLocAlarmReceiver.class);

            if (Build.VERSION.SDK_INT >= 31) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1235, i, PendingIntent.FLAG_MUTABLE);
                Calendar t = Calendar.getInstance();
                t.setTimeInMillis(System.currentTimeMillis());
                am.setInexactRepeating(AlarmManager.RTC, t.getTimeInMillis(), interval, pendingIntent);
            } else {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1235, i, PendingIntent.FLAG_UPDATE_CURRENT);
                Calendar t = Calendar.getInstance();
                t.setTimeInMillis(System.currentTimeMillis());
                am.setInexactRepeating(AlarmManager.RTC, t.getTimeInMillis(), interval, pendingIntent);
            }

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_ACTIVITY_RECOGNITION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted
                // Perform your task here
            } else {
                // Permission has been denied
                // Handle the denial
            }
        }
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        ((HomeActivityNew) getActivity()).mSpinnerDaysFilter.setVisibility(View.GONE);
//        ((HomeActivityNew) getActivity()).getMonthYearList(((HomeActivityNew) getActivity()).cal, ((HomeActivityNew) getActivity()).currentYear, ((HomeActivityNew) getActivity()).currentMonth);
        super.onDestroyView();
    }
}