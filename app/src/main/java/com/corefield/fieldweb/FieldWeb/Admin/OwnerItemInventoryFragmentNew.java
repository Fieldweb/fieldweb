package com.corefield.fieldweb.FieldWeb.Admin;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.ItemIssueListAdapterNew;
import com.corefield.fieldweb.Adapter.ItemListAdapterNew;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Item.AddItem;
import com.corefield.fieldweb.DTO.Item.AllItemIssueList;
import com.corefield.fieldweb.DTO.Item.DeleteItemPortal;
import com.corefield.fieldweb.DTO.Item.EditItem;
import com.corefield.fieldweb.DTO.Item.IssueItem;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Item.UnAssignItem;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UnAssignItemAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateItemAsyncTask;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerItemInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show item inventory where Owner can add item and Assign Item
 */
public class OwnerItemInventoryFragmentNew extends Fragment implements OnTaskCompleteListener, View.OnClickListener, RecyclerTouchListener {  //, RecyclerTouchListenerUpdateItem
    protected static String TAG = OwnerItemInventoryFragmentNew.class.getSimpleName();
    public List<ItemsList.ResultData> mItemsLists = null;
    public List<UsersList.ResultData> mUsersLists = null;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    private View mRootView;
    ImageView noResultImg;

    private ItemIssueListAdapterNew mItemIssueListAdapter;
    ///////////////////////////////////////////////////////////
    private RecyclerView mRecyclerViewItemsList;
    private ItemListAdapterNew mItemsListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int mPosition;
    private List<AllItemIssueList.ResultData> mItemIssueLists = null;
    private int mItemID = 0;
    private int mItemAssignedUserId = 0;
    private int mQuantity = 0;
    //    private boolean mAPILock = false;
    private UnAssignItemAsyncTask mUnAssignItemAsyncTask;
    private UpdateItemAsyncTask mUpdateItemAsyncTask;
    private boolean mAllItemApiCall = false, mItemListApiCall = false, mUserListApiCall = false;

    private Button mButtonInventory, mButtonIssuedItems, mItem;
    private String mSelection = "";
    private SearchView mSearchView;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.owner_item_inventory_fragment_new, container, false);
        mButtonInventory = mRootView.findViewById(R.id.button_inventory);
        mButtonIssuedItems = mRootView.findViewById(R.id.button_issued_item);
        mItem = mRootView.findViewById(R.id.plus_item);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        noResultImg = mRootView.findViewById(R.id.noResultImg);
        recyclerTouchListener = this;
        fragment = this;

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        callToInventoryApi();
        mRecyclerViewItemsList = mRootView.findViewById(R.id.recycler_item_list);

        FGALoadEvent();
        mSelection = "INV";
        mButtonInventory.setOnClickListener(this);
        mButtonIssuedItems.setOnClickListener(this);
        mItem.setOnClickListener(this);

        mItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((HomeActivityNew) getActivity()).addItem();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    public void callToInventoryApi() {
        /*mItemsListAsyncTask = new ItemsListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mItemsListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        getAllItemListAssignAndUnAssignV2(recyclerTouchListener, fragment);
    }

    public void callToIssuedItemApi() {
        /*mAllItemIssueListAsyncTask = new AllItemIssueListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mAllItemIssueListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        getAllAssignedItemsListForAdmin(recyclerTouchListener);
    }

    //----------------------------------------------RETROFIT FUNCTIONS---------------------------------------------//

    public void getAllItemListAssignAndUnAssignV2(RecyclerTouchListener recyclerTouchListener, Fragment fragment) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<ItemsList> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getItemList(userID);
                call.enqueue(new retrofit2.Callback<ItemsList>() {
                    @Override
                    public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                FWLogger.logInfo(TAG, "Get Item list");
                                mItemsLists = new ArrayList<>();
                                ItemsList itemsLists = response.body();
                                mItemsLists = itemsLists.getResultData();
                                if (mItemsLists.size() > 0) {
                                    mRecyclerViewItemsList.setVisibility(View.VISIBLE);
                                    mItemsNameList = new ArrayList<>();
                                    for (ItemsList.ResultData resultData : mItemsLists) {
                                        mItemsNameList.add(resultData.getName());
                                        FWLogger.logInfo(TAG, resultData.getName());
                                    }
                                    mLayoutManager = new LinearLayoutManager(getActivity());
                                    mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
                                    mItemsListAdapter = new ItemListAdapterNew(getActivity(), mPosition, mItemsLists, recyclerTouchListener, fragment);
                                    mRecyclerViewItemsList.setAdapter(mItemsListAdapter);
                                    mItemListApiCall = true;
                                    setSearchFilter();
                                } else {
                                    mRecyclerViewItemsList.setVisibility(View.GONE);
                                    noResultImg.setVisibility(View.VISIBLE);
                                    noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                }


                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ItemsList> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
            ex.getMessage();
        }
    }

    public void getAllAssignedItemsListForAdmin(RecyclerTouchListener recyclerTouchListener) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<AllItemIssueList> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getAllItemIssueList(userID);
                call.enqueue(new retrofit2.Callback<AllItemIssueList>() {
                    @Override
                    public void onResponse(Call<AllItemIssueList> call, Response<AllItemIssueList> response) {
                        try {
                            if (response.code() == 200) {
                                FWLogger.logInfo(TAG, "Get Item Issue");
                                AllItemIssueList allItemIssueLists = response.body();
                                mItemIssueLists = new ArrayList<>();
                                mItemIssueLists = allItemIssueLists.getResultData();
                                mItemIssueListAdapter = new ItemIssueListAdapterNew(getActivity(), mPosition, mItemIssueLists, recyclerTouchListener);
                                mLayoutManager = new LinearLayoutManager(getActivity());
                                mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
                                mItemIssueListAdapter.setClickListener(recyclerTouchListener);
                                mRecyclerViewItemsList.setAdapter(mItemIssueListAdapter);

                                mAllItemApiCall = true;
                                setSearchFilter();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AllItemIssueList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAllAssignedItemsListForAdmin? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllAssignedItemsListForAdmin? API:");
            ex.getMessage();
        }
    }

    public void deleteItem(int itemId) {
        /*mDeleteItemAsyncTask = new DeleteItemAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mDeleteItemAsyncTask.execute(itemId);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<DeleteItemPortal> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getDeleteItemPortal(itemId);
                call.enqueue(new retrofit2.Callback<DeleteItemPortal>() {
                    @Override
                    public void onResponse(Call<DeleteItemPortal> call, Response<DeleteItemPortal> response) {
                        try {
                            if (response.code() == 200) {
                                DeleteItemPortal deleteItem = response.body();
                                Toast.makeText(getContext(), deleteItem.getMessage(), Toast.LENGTH_SHORT).show();
                                FWLogger.logInfo(TAG, "Massage  = " + deleteItem.getMessage());
                                if (deleteItem.getMessage().equalsIgnoreCase("Item deleted successfully.")) {
                                    refresh();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteItemPortal> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteItemPortal? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteItemPortal? API:");
            ex.getMessage();
        }
    }

    //----------------------------------------END OF RETROFIT FUNCTIONS-------------------------------------------//
    @Override
    public void onClick(View v) {
        ItemDialog itemDialog = ItemDialog.getInstance();
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        switch (v.getId()) {
            case R.id.button_inventory:
//                Toast.makeText(getContext(), "Inventory Clicked", Toast.LENGTH_SHORT).show();
                mSelection = "INV";
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
                mButtonInventory.setBackground(getResources().getDrawable(R.drawable.bg_curve_border_selected));
                mButtonInventory.setTextColor(getResources().getColor(R.color.black));
                mButtonIssuedItems.setBackground(getResources().getDrawable(R.drawable.bg_curve_border));
                mButtonIssuedItems.setTextColor(getResources().getColor(R.color.White));
                callToInventoryApi();
                break;

            case R.id.button_issued_item:
                mSelection = "ISSUE";
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
                mButtonIssuedItems.setBackground(getResources().getDrawable(R.drawable.bg_curve_border_selected));
                mButtonIssuedItems.setTextColor(getResources().getColor(R.color.black));
                mButtonInventory.setBackground(getResources().getDrawable(R.drawable.bg_curve_border));
                mButtonInventory.setTextColor(getResources().getColor(R.color.White));
                callToIssuedItemApi();
                break;

            default:
                break;
        }
    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ITEM_INVENTORY, bundle);
    }


    public void unAssign(int quantity) {
        FWLogger.logInfo(TAG, "Item ID on " + mItemID);
        mQuantity = quantity;
        mUnAssignItemAsyncTask = new UnAssignItemAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUnAssignItemAsyncTask.execute(mItemAssignedUserId, mItemID, quantity);
    }

    public void updateItem(/*int quantity*/EditItem.ResultData editItem) {
        FWLogger.logInfo(TAG, "Item ID on " + mItemID);
        /*mUpdateItemAsyncTask = new UpdateItemAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUpdateItemAsyncTask.execute(*//*SharedPrefManager.getInstance(getContext()).getUserId(), mItemID, quantity*//*editItem);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<EditItem> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().updateItem(editItem,
                        "Bearer " + SharedPrefManager.getInstance(getApplicationContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<EditItem>() {
                    @Override
                    public void onResponse(Call<EditItem> call, Response<EditItem> response) {
                        try {
                            if (response.code() == 200) {
                                EditItem editItem = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_ID, editItem.getResultData().getId());
                                bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, editItem.getResultData().getQuantity());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.UPDATE);
                                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
                                if (editItem.getCode().equalsIgnoreCase("200") && editItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_updated_successfully, getContext()))) {
                                    Toast.makeText(getContext(), getString(R.string.item_updated_successfully), Toast.LENGTH_SHORT).show();
                                    callToInventoryApi();
                                } else {
                                    Toast.makeText(getContext(), editItem.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<EditItem> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateEditItemByItemId API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateEditItemByItemId API:");
            ex.getMessage();
        }

    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(IssueItem.class.getSimpleName())) {
            Gson gson = new Gson();
            IssueItem issueItem = gson.fromJson(urlConnectionResponse.resultData, IssueItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, issueItem.getResultData().getQuantity());
            bundle.putInt(FirebaseGoogleAnalytics.Param.TECH_ID, issueItem.getResultData().getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ISSUE);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            Toast.makeText(getContext(), issueItem.getMessage(), Toast.LENGTH_SHORT).show();

            callToInventoryApi();
            FWLogger.logInfo(TAG, "Issue Items to  = " + issueItem.getResultData().getUserId());
        } else if (classType.equalsIgnoreCase(AddItem.class.getSimpleName())) {
            Gson gson = new Gson();
            AddItem addItem = gson.fromJson(urlConnectionResponse.resultData, AddItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, addItem.getResultdata().getQuantity());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.ADD);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            if (addItem.getCode().equalsIgnoreCase("200") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_added_successfully, getContext())))
                Toast.makeText(getContext(), getString(R.string.item_added_successfully), Toast.LENGTH_SHORT).show();
            else if (addItem.getCode().equalsIgnoreCase("400") && addItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_name_already_exist, getContext())))
                Toast.makeText(getContext(), getString(R.string.item_name_already_exist), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getContext(), addItem.getMessage(), Toast.LENGTH_SHORT).show();

            callToInventoryApi();
            ((HomeActivityNew) getActivity()).getItemList();
            FWLogger.logInfo(TAG, "Added Successful = " + addItem.getResultdata().getName());
        } else if (classType.equalsIgnoreCase(AllItemIssueList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Get Item Issue");
            Gson gson = new Gson();
            mItemIssueLists = new ArrayList<>();
            AllItemIssueList allItemIssueLists = gson.fromJson(urlConnectionResponse.resultData, AllItemIssueList.class);
            mItemIssueLists = allItemIssueLists.getResultData();
            mItemIssueListAdapter = new ItemIssueListAdapterNew(getActivity(), mPosition, mItemIssueLists, this);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
            mItemIssueListAdapter.setClickListener(this);
            mRecyclerViewItemsList.setAdapter(mItemIssueListAdapter);

            mAllItemApiCall = true;
            setSearchFilter();
        } else if (classType.equalsIgnoreCase(ItemsList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Get Item list");
            Gson gson = new Gson();
            mItemsLists = new ArrayList<>();
            ItemsList itemsLists = gson.fromJson(urlConnectionResponse.resultData, ItemsList.class);
            mItemsLists = itemsLists.getResultData();

            mItemsNameList = new ArrayList<>();
            for (ItemsList.ResultData resultData : mItemsLists) {
                mItemsNameList.add(resultData.getName());
                FWLogger.logInfo(TAG, resultData.getName());
            }
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
            mItemsListAdapter = new ItemListAdapterNew(getActivity(), mPosition, mItemsLists, this, this);
            mRecyclerViewItemsList.setAdapter(mItemsListAdapter);
            mItemListApiCall = true;
            setSearchFilter();
        } else if (classType.equalsIgnoreCase(UsersList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received Users Name Successfully");
            Gson gson = new Gson();
            mUsersLists = new ArrayList<>();
            UsersList usersList = gson.fromJson(urlConnectionResponse.resultData, UsersList.class);
            mUsersLists = usersList.getResultData();
            mUsersNameList = new ArrayList<>();
            for (UsersList.ResultData resultData : mUsersLists) {
                mUsersNameList.add(resultData.getFirstName());
            }
            mUserListApiCall = true;
        } else if (classType.equalsIgnoreCase(UnAssignItem.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Received Users Name Successfully");
            Gson gson = new Gson();
            UnAssignItem unAssignItem = gson.fromJson(urlConnectionResponse.resultData, UnAssignItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_ID, mItemID);
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, mQuantity);
            bundle.putInt(FirebaseGoogleAnalytics.Param.TECH_ID, mItemAssignedUserId);
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.UNASSIGN);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            if (unAssignItem != null) {
                if (unAssignItem.getCode().equalsIgnoreCase("200") && unAssignItem.getMessage().equalsIgnoreCase("Quantity updated Successfully.")) {
                    Toast.makeText(getContext(), unAssignItem.getMessage(), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, "Item UnAssign successfully.");
                    ///////////////
                    mUserListApiCall = true;
                    callToIssuedItemApi();
                    //////////////
                } else {
                    Toast.makeText(getContext(), unAssignItem.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        } else if (classType.equalsIgnoreCase(EditItem.class.getSimpleName())) {
            Gson gson = new Gson();
            EditItem editItem = gson.fromJson(urlConnectionResponse.resultData, EditItem.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_ID, editItem.getResultData().getId());
            bundle.putInt(FirebaseGoogleAnalytics.Param.ITEM_QTY, editItem.getResultData().getQuantity());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.UPDATE);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CONFIRMATION, bundle);
            if (editItem.getCode().equalsIgnoreCase("200") && editItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_updated_successfully, getContext()))) {
                Toast.makeText(getContext(), getString(R.string.item_updated_successfully), Toast.LENGTH_SHORT).show();
                callToInventoryApi();
            } else {
                Toast.makeText(getContext(), editItem.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (classType.equalsIgnoreCase(DeleteItemPortal.class.getSimpleName())) {
            Gson gson = new Gson();
            DeleteItemPortal deleteItem = gson.fromJson(urlConnectionResponse.resultData, DeleteItemPortal.class);
            Toast.makeText(getContext(), deleteItem.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Massage  = " + deleteItem.getMessage());
            if (deleteItem.getMessage().equalsIgnoreCase("Item deleted successfully.")) {
                refresh();
            }
        }

        if (mAllItemApiCall && mItemListApiCall && mUserListApiCall) {
            mAllItemApiCall = false;
            mItemListApiCall = false;
            mUserListApiCall = false;
        }
    }

    private void refresh() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        OwnerItemInventoryFragmentNew myFragment = new OwnerItemInventoryFragmentNew();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;

            case R.id.image_item_update:
                ItemsList.ResultData itemLists = mItemsListAdapter.getItem(position);
                mItemID = itemLists.getId();
                FWLogger.logInfo(TAG, "Item ID on click =  " + itemLists.getId());
                FWLogger.logInfo(TAG, "user ID  =  " + itemLists.getUserId());
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.UPDATE);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CLICK, bundle);
                ItemDialog itemDialog = ItemDialog.getInstance();
                itemDialog.updateItemDialog(getContext(), this, itemLists.getName(), String.valueOf(itemLists.getQuantity()), mItemID);
                break;

            case R.id.image_issue_item_update:
                AllItemIssueList.ResultData allItemIssueLists = mItemIssueListAdapter.getItem(position);
                mItemID = allItemIssueLists.getId();
                mItemAssignedUserId = allItemIssueLists.getUserId();
                FWLogger.logInfo(TAG, "Item ID on click =  " + allItemIssueLists.getId());
                FWLogger.logInfo(TAG, "user ID  =  " + allItemIssueLists.getUserId());

                //NOTE: Log GA event
                Bundle issueBundle = new Bundle();
                issueBundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                issueBundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                issueBundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                issueBundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                issueBundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                issueBundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
                issueBundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.UNASSIGN);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.INVENTORY_CLICK, issueBundle);

                ItemDialog issueItemDialog = ItemDialog.getInstance();
                issueItemDialog.unAssignItemDialog(getContext(), this, allItemIssueLists.getName(), allItemIssueLists.getAssignedTo(), String.valueOf(allItemIssueLists.getQuantity()));
                break;

            default:
                break;
        }
    }

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);

        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mSelection.equalsIgnoreCase("INV")) {
                    // filter recycler view when query submitted
                    if (mItemsListAdapter != null)
                        mItemsListAdapter.getFilter().filter(query);
                } else {
                    if (mItemIssueListAdapter != null)
                        mItemIssueListAdapter.getFilter().filter(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mSelection.equalsIgnoreCase("INV")) {
                    // filter recycler view when query submitted
                    if (mItemsListAdapter != null)
                        mItemsListAdapter.getFilter().filter(query);
                } else {
                    if (mItemIssueListAdapter != null)
                        mItemIssueListAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        if (mUnAssignItemAsyncTask != null) {
            mUnAssignItemAsyncTask.cancel(true);
        }
        if (mUpdateItemAsyncTask != null) {
            mUpdateItemAsyncTask.cancel(true);
        }
        super.onDestroyView();
    }

}
