package com.corefield.fieldweb.FieldWeb.Task;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListViewAdapter;
import com.corefield.fieldweb.Adapter.MultipleItemListAdapter;
import com.corefield.fieldweb.Adapter.NotesViewAdapter;
import com.corefield.fieldweb.Adapter.TaskCompletedMultitemListAdapter;
import com.corefield.fieldweb.Adapter.TaskOngoingMultitemListAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Task.DownloadReport;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.RoundedTransformation;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.BuildConfig;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks Details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show completed task details
 */
@SuppressLint("ValidFragment")
public class TaskDetailsFragmentNew extends Fragment implements View.OnClickListener, OnMapReadyCallback, OnTaskCompleteListener, BaseActivity.RequestPermissionsUpdateListener {

    public static String TAG = TaskDetailsFragmentNew.class.getSimpleName();
    private String mFieldImageURL = "", mFieldImageURL1 = "", mFieldImageURL2 = "", mImageBeforeTaskURL = "", mImageBeforeTaskURL1 = "", mImageBeforeTaskURL2 = "";
    private View mRootView;
    private TasksList.ResultData mResultData;
    private TextView mTextViewTaskType, mTextViewTaskStatus, mTextViewEstimatedAmount, mTextViewEarnedAmount, mTextViewStartDateTime, mTextViewEndDateTime, mTextViewTechnicianName, mTextViewItemName, mTextViewItemQuantity, mTextViewCustomerName, mTextViewCustomerNo, mTextViewLandmark, mTextViewAddress, mTextViewTaskName, mTextViewWorkMode, mTextViewSignedBy, mTextViewRating, mTextViewSignedMobileNo, mTextViewTaskID, mTextViewEstimatedRs, mTextViewEarnedRs;
    private ImageView mImageViewCall, mImageViewDownload, mImageViewReassign, mImageViewMessage;
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private Marker mMarkerDestination;
    private double mDestLat = 0, mDestLong = 0;
    private RecyclerView mRecyclerViewNote, mRecyclerViewDeviceList, multiItemCompltedTaskDetls;
    private NotesViewAdapter mNotesViewAdapter;
    private DeviceListViewAdapter mDeviceListViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<TaskClosure.TechnicalNotedto> mTechnicalNoteDTOs = new ArrayList<>();
    private List<TaskClosure.DeviceInfoList> mDeviceInfoLists = new ArrayList<>();
    private ImageView mImageViewSignature, mImageViewFieldPhoto, mImageViewFieldPhoto2, mImageViewFieldPhoto3, mImageViewPhotoBeforeTask, mImageViewPhotoBeforeTask1, mImageViewPhotoBeforeTask2;
    private RatingBar mRatingBar;
    private LinearLayout mLinearNotesList, mLinearDeviceList;
    private LinearLayout mTaskClosureWrapper, mLinearLayoutRatingWrapper;
    private static final String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.INTERNET};

    private static final String[] PERMISSIONS_TIRAMISU = {android.Manifest.permission.READ_MEDIA_VIDEO, android.Manifest.permission.READ_MEDIA_IMAGES, android.Manifest.permission.READ_MEDIA_AUDIO};
    TaskCompletedMultitemListAdapter taskCompletedMultitemListAdapter;
    TaskOngoingMultitemListAdapter taskRejectedMultitemListAdapter;

    @SuppressLint("ValidFragment")
    public TaskDetailsFragmentNew() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.dialog_task_details, container, false);
        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

            ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
            ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);

            inIt();
            getTaskClosureDetail(mResultData.getId());
            //setData();
            manageBackPress();
            checkStoragePermission();
        }
        return mRootView;
    }

    private void inIt() {
        try {
            ((HomeActivityNew) getActivity()).getItemList();
        } catch (Exception e) {
            e.getMessage();
        }
        if (getArguments() != null) {
            if (getArguments().getBoolean("allowToReassign")) {
                mImageViewReassign = mRootView.findViewById(R.id.imageView_reassign);
                mImageViewReassign.setVisibility(View.VISIBLE);
                mImageViewReassign.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mResultData != null && getActivity() != null) {
                            if (mResultData.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.Completed.name())) {
                                ((HomeActivityNew) getActivity()).openReAssignCompletedTaskDialog(mResultData);
                            } else {
                                ((HomeActivityNew) getActivity()).addTaskWithReassignDefaults(mResultData);
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.something_wrong_please_refresh, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            if (getArguments().getBoolean("downloadReport")) {
                mImageViewDownload = mRootView.findViewById(R.id.imageView_download);
                mImageViewDownload.setVisibility(View.VISIBLE);
                mImageViewDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDownloadReport(mResultData.getId());
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mResultData.getId());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, mResultData.getTaskStatus());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.GENERATE_INVOICE, bundle);
                    }
                });
            }
        }
        mTextViewTaskType = mRootView.findViewById(R.id.task_details_task_type);
        mTextViewTaskStatus = mRootView.findViewById(R.id.task_details_task_status);
        mTextViewEstimatedAmount = mRootView.findViewById(R.id.task_details_task_rate);
        mTextViewEarnedAmount = mRootView.findViewById(R.id.task_details_task_earned);
        mTextViewStartDateTime = mRootView.findViewById(R.id.task_details_task_start_date_time);
        mTextViewEndDateTime = mRootView.findViewById(R.id.task_details_task_end_date_time);
        mTextViewTechnicianName = mRootView.findViewById(R.id.task_details_tech_name);
        mTextViewItemName = mRootView.findViewById(R.id.task_details_item_name);
        mTextViewItemQuantity = mRootView.findViewById(R.id.task_details_item_quantity);
        mTextViewCustomerName = mRootView.findViewById(R.id.task_details_customer_name);
        mTextViewCustomerNo = mRootView.findViewById(R.id.task_details_customer_number);
        mTextViewLandmark = mRootView.findViewById(R.id.task_details_landmark);
        mTextViewAddress = mRootView.findViewById(R.id.task_details_address);
        mTextViewTaskName = mRootView.findViewById(R.id.task_details_task_name);
        mImageViewCall = mRootView.findViewById(R.id.imageView_call);
        mImageViewMessage = mRootView.findViewById(R.id.imageView_message);
        mTaskClosureWrapper = mRootView.findViewById(R.id.linear_layout_task_closure_wrapper);
        mLinearLayoutRatingWrapper = mRootView.findViewById(R.id.linear_layout_rating_wrapper);
        mTextViewWorkMode = mRootView.findViewById(R.id.textView_work_mode);
        mImageViewSignature = mRootView.findViewById(R.id.customer_signature);
        mRatingBar = mRootView.findViewById(R.id.ratingbar_task_closure_view);
        mTextViewRating = mRootView.findViewById(R.id.textView_rating);
        mImageViewPhotoBeforeTask = mRootView.findViewById(R.id.imageView_before_task);
        mImageViewPhotoBeforeTask1 = mRootView.findViewById(R.id.imageview_before_task2);
        mImageViewPhotoBeforeTask2 = mRootView.findViewById(R.id.imageview_before_task3);

        mImageViewFieldPhoto = mRootView.findViewById(R.id.imageView_field_photo);
        mImageViewFieldPhoto2 = mRootView.findViewById(R.id.imageView_field_photo2);
        mImageViewFieldPhoto3 = mRootView.findViewById(R.id.imageView_field_photo3);

        mTextViewSignedBy = mRootView.findViewById(R.id.textView_signed_by);
        mTextViewSignedMobileNo = mRootView.findViewById(R.id.textview_signed_mobile_no);
        mTextViewTaskID = mRootView.findViewById(R.id.task_id);
        mTextViewEstimatedRs = mRootView.findViewById(R.id.textView_estimated_rupees);
        mTextViewEarnedRs = mRootView.findViewById(R.id.textView_earned_rupees);

        mLinearNotesList = mRootView.findViewById(R.id.linear_notes_list);
        mLinearDeviceList = mRootView.findViewById(R.id.linear_device_list);

        mLinearNotesList.setVisibility(View.GONE);
        mLinearDeviceList.setVisibility(View.GONE);

        mRecyclerViewNote = mRootView.findViewById(R.id.recyclerView_note_view);
        mRecyclerViewDeviceList = mRootView.findViewById(R.id.recyclerView_device_view);
        multiItemCompltedTaskDetls = mRootView.findViewById(R.id.multiItemCompltedTaskDetls);

        mImageViewFieldPhoto.setOnClickListener(this);
        mImageViewFieldPhoto2.setOnClickListener(this);
        mImageViewFieldPhoto3.setOnClickListener(this);
        mImageViewPhotoBeforeTask.setOnClickListener(this);
        mImageViewPhotoBeforeTask1.setOnClickListener(this);
        mImageViewPhotoBeforeTask2.setOnClickListener(this);
        mImageViewCall.setOnClickListener(this);
        mImageViewMessage.setOnClickListener(this);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.task_details_map);
    }

    private void getTaskClosureDetail(int taskId) {
        if (taskId != 0) {
            if (getContext() == null) FWLogger.logInfo(TAG, "getContext is null");
           /* mGetTaskClosureDetails = new GetTaskClosureDetailsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
            mGetTaskClosureDetails.execute(taskId, SharedPrefManager.getInstance(getContext()).getUserId());*/

            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    Call<TaskClosure> call = RetrofitClient.getInstance(getContext()).getMyApi().getTaskClosureDetail(SharedPrefManager.getInstance(getContext()).getUserId(), taskId, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<TaskClosure>() {
                        @Override
                        public void onResponse(Call<TaskClosure> call, Response<TaskClosure> response) {
                            try {
                                if (response.code() == 200) {
                                    TaskClosure taskClosureDetails = response.body();
                                    if (taskClosureDetails.getCode().equalsIgnoreCase("200") && taskClosureDetails.getMessage().equalsIgnoreCase("Success")) {
                                        FWLogger.logInfo(TAG, "200");
                                        if (taskClosureDetails.getResultData() != null) {
                                            setTaskClosureToView(taskClosureDetails);
                                        } else {
                                            mLinearLayoutRatingWrapper.setVisibility(View.GONE);
                                            mTaskClosureWrapper.setVisibility(View.GONE);
                                        }
                                    } else {
                                        mLinearLayoutRatingWrapper.setVisibility(View.GONE);
                                        mTaskClosureWrapper.setVisibility(View.GONE);
                                        Toast.makeText(getContext(), taskClosureDetails.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                    setData(taskClosureDetails);
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<TaskClosure> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in GeTaskClosureList? API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in GeTaskClosureList? API:");
                ex.getMessage();
            }
        }
    }

    private void getDownloadReport(int taskId) {
        if (taskId != 0) {
            /*mDownloadReportAsyncTask = new DownloadReportAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
            mDownloadReportAsyncTask.execute(taskId, SharedPrefManager.getInstance(getContext()).getUserId());*/

            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    int userId = SharedPrefManager.getInstance(getContext()).getUserId();
                    Call<DownloadReport> call = RetrofitClient.getInstance(getContext()).getMyApi().getDownloadTaskReport(userId, taskId);
                    call.enqueue(new retrofit2.Callback<DownloadReport>() {
                        @Override
                        public void onResponse(Call<DownloadReport> call, Response<DownloadReport> response) {
                            try {
                                if (response.code() == 200) {
                                    DownloadReport downloadReport = response.body();
                                    if (downloadReport != null) {
                                        FWLogger.logInfo(TAG, "Code : " + downloadReport.getCode());
                                        FWLogger.logInfo(TAG, "Message : " + downloadReport.getMessage());
                                        if (downloadReport.getCode().equalsIgnoreCase("200")) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                                if (!hasPermissions(getContext(), PERMISSIONS_TIRAMISU)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(downloadReport.getMessage(), getActivity());
                                                }
                                            } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                                                if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(downloadReport.getMessage(), getActivity());
                                                }
                                            }
                                        } else {
                                            Toast.makeText(getContext(), downloadReport.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                            /*else if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                t.show();
                                            } else {
                                                DownloadFile.downloadFile(downloadReport.getMessage(), getActivity());
                                            }*/
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<DownloadReport> call, Throwable throwable) {
                            //Toast.makeText(getApplicationContext(), "GenerateReportPDF API : An error has occured", Toast.LENGTH_LONG).show();
                            FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                ex.getMessage();
            }
        }

    }

    private void setData(TaskClosure taskClosureDetails) {
        if (mResultData != null && mResultData.getTaskType() != null)
            mTextViewTaskType.setText(mResultData.getTaskType());
        else mTextViewTaskType.setText(getResources().getString(R.string.na));

        if (mResultData.getTaskType() != null && !mResultData.getTaskType().isEmpty()) {
            switch (mResultData.getTaskType()) {
                case "Urgent":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_urgent));
                    break;
                case "Today":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.orange));
                    break;
                case "Schedule":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_schedule));
                    break;
                default:
                    break;
            }
        }

        if (mResultData != null && mResultData.getTaskStatus() != null) {
            mTextViewTaskStatus.setText(mResultData.getTaskStatus());
            if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.inactive)))
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.light_gray));
            else if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.rejected)))
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.red));
            else if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.ongoing)))
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.orange));
            else if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.completed)))
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.green));
            else if (mResultData.getTaskStatus().equalsIgnoreCase(getActivity().getResources().getString(R.string.onhold)))
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.onhold));

        } else mTextViewTaskStatus.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getWagesPerHours() != 0) {
            mTextViewEstimatedRs.setVisibility(View.VISIBLE);
            mTextViewEstimatedAmount.setText("" + mResultData.getWagesPerHours());
        } else {
            mTextViewEstimatedRs.setVisibility(View.GONE);
            mTextViewEstimatedAmount.setText(getResources().getString(R.string.na));
        }
        if (mResultData != null && mResultData.getPaymentModeId() != Constant.PaymentMode.AMC_ID) {
            mTextViewEarnedRs.setVisibility(View.VISIBLE);
            if (mResultData.getEarningAmount() != 0)
                mTextViewEarnedAmount.setText("" + mResultData.getEarningAmount());
        } else {
            mTextViewEarnedRs.setVisibility(View.GONE);
            mTextViewEarnedAmount.setText(getResources().getString(R.string.na));
        }
        if (mResultData != null && mResultData.getStartDate() != 0)
            mTextViewStartDateTime.setText(DateUtils.getDate(mResultData.getStartDate(), "dd-MM-yyyy hh:mm aa"));
        else mTextViewStartDateTime.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getEndDate() != 0)
            mTextViewEndDateTime.setText(DateUtils.getDate(mResultData.getEndDate(), "dd-MM-yyyy hh:mm aa"));
        else mTextViewEndDateTime.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getAssignedTo() != null)
            mTextViewTechnicianName.setText(mResultData.getAssignedTo());
        else mTextViewTechnicianName.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getItemName() != null)
            mTextViewItemName.setText(mResultData.getItemName());
        else mTextViewItemName.setText(getResources().getString(R.string.na));
        /*if (mResultData != null && mResultData.getQuantity() != 0)
            mTextViewItemQuantity.setText("" + mResultData.getQuantity());
        else mTextViewItemQuantity.setText(getResources().getString(R.string.na));*/
        if (mResultData != null) {
            if (mResultData.getCustomerName() == null || mResultData.getCustomerName().isEmpty() || mResultData.getCustomerName().equalsIgnoreCase(""))
                mTextViewCustomerName.setText(getResources().getString(R.string.na));
            else mTextViewCustomerName.setText(mResultData.getCustomerName());
        }
        if (mResultData != null) {
            if (mResultData.getContactNo() == null || mResultData.getContactNo().isEmpty() || mResultData.getContactNo().equalsIgnoreCase(""))
                mTextViewCustomerNo.setText(getResources().getString(R.string.na));
            else mTextViewCustomerNo.setText(mResultData.getContactNo());
        }
        if (mResultData != null && mResultData.getLocationDesc() != null)
            mTextViewLandmark.setText(mResultData.getLocationDesc());
        else mTextViewLandmark.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getFullAddress() != null)
            mTextViewAddress.setText(mResultData.getFullAddress());
        else mTextViewAddress.setText(getResources().getString(R.string.na));
        if (mResultData != null && mResultData.getName() != null)
            mTextViewTaskName.setText(mResultData.getName());
        else mTextViewTaskName.setText(getResources().getString(R.string.na));

        if (mResultData.getLatitude() != null)
            mDestLat = Double.parseDouble(mResultData.getLatitude());
        if (mResultData.getLongitude() != null)
            mDestLong = Double.parseDouble(mResultData.getLongitude());
        if (mResultData.getId() != 0) mTextViewTaskID.setText("[" + mResultData.getId() + "]");
        mMapFragment.getMapAsync(this);

        // MULTI ITEM LIST FOR CLOSURE : MULTI ITEM
        try {
           /* List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
            multipleItemAssignedArrayList = mResultData.getMultipleItemAssigned();
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiItemCompltedTaskDetls.setLayoutManager(mLayoutManager);
                taskCompletedMultitemListAdapter = new TaskCompletedMultitemListAdapter(getActivity(), multipleItemAssignedArrayList);
                multiItemCompltedTaskDetls.setAdapter(taskCompletedMultitemListAdapter);*/
            List<TaskClosure.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TaskClosure.MultipleItemAssigned>();
            multipleItemAssignedArrayList = taskClosureDetails.getResultData().getMultipleItemAssigned();
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiItemCompltedTaskDetls.setLayoutManager(mLayoutManager);
                taskCompletedMultitemListAdapter = new TaskCompletedMultitemListAdapter(getActivity(), multipleItemAssignedArrayList);
                multiItemCompltedTaskDetls.setAdapter(taskCompletedMultitemListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR TASK REJECTED DETAILS : MULTI ITEM
        try {
            List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
            multipleItemAssignedArrayList = mResultData.getMultipleItemAssigned();
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiItemCompltedTaskDetls.setLayoutManager(mLayoutManager);
                taskRejectedMultitemListAdapter = new TaskOngoingMultitemListAdapter(getActivity(), multipleItemAssignedArrayList);
                multiItemCompltedTaskDetls.setAdapter(taskRejectedMultitemListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.textView_call:
            case R.id.imageView_call:
                if (mResultData.getContactNo() != null && !mResultData.getContactNo().isEmpty()) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mResultData.getContactNo()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (mResultData.getContactNo() != null && !mResultData.getContactNo().isEmpty()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mResultData.getContactNo(), null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_field_photo:
                if (mFieldImageURL != null && !mFieldImageURL.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageView_field_photo2:
                if (mFieldImageURL1 != null && !mFieldImageURL1.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mFieldImageURL1, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageView_field_photo3:
                if (mFieldImageURL2 != null && !mFieldImageURL2.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mFieldImageURL2, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;

            // FOR BEFORE PHOTO
            case R.id.imageView_before_task:
                if (mImageBeforeTaskURL != null && !mImageBeforeTaskURL.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mImageBeforeTaskURL, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageview_before_task2:
                if (mImageBeforeTaskURL1 != null && !mImageBeforeTaskURL1.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mImageBeforeTaskURL1, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageview_before_task3:
                if (mImageBeforeTaskURL2 != null && !mImageBeforeTaskURL2.equalsIgnoreCase("")) {
                    CommonDialog commonDialog = CommonDialog.getInstance();
                    commonDialog.showImageInFullScreenMode(getActivity(), mImageBeforeTaskURL2, getContext().getResources().getString(R.string.field_photo));
//                AlertDialog.showImageInFullScreenMode(getContext(), mFieldImageURL, getContext().getResources().getString(R.string.field_photo));
                } else
                    Toast.makeText(getContext(), R.string.image_is_not_available, Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        FWLogger.logInfo(TAG, "updateGoogleMapUI called");
        if (mGoogleMap != null) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));

                if (!success) {
                    FWLogger.logInfo(TAG, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(TAG, "Can't find style. Error: ", e);
            }

            MapsInitializer.initialize(getContext());
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

            //Check permission
            if (((BaseActivity) getActivity()).checkAllPermissionEnabled()) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                if (((BaseActivity) getActivity()).requestToEnableAllPermission(this, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }

            mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
            // mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

            //Update map
            updateGoogleMapUI();

        }
    }

    private synchronized void updateGoogleMapUI() {
        if (mResultData.getLatitude() != null && mResultData.getLongitude() != null) {
            //Destination marker
            mMarkerDestination = mGoogleMap.addMarker(new MarkerOptions().anchor(0.0f, 1.0f).position(new LatLng(mDestLat, mDestLong)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_fieldweb_location)).title(mResultData.getName()).snippet(mResultData.getCustomerName()));
            //Show info window forcefully to know about task
            mMarkerDestination.showInfoWindow();

            //Animate camera
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mMarkerDestination.getPosition(), 10F);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(TaskClosure.class.getSimpleName())) {
                Gson gson = new Gson();
                TaskClosure taskClosureDetails = gson.fromJson(urlConnectionResponse.resultData, TaskClosure.class);
                if (taskClosureDetails.getCode().equalsIgnoreCase("200") && taskClosureDetails.getMessage().equalsIgnoreCase("Success")) {
                    FWLogger.logInfo(TAG, "200");
                    if (taskClosureDetails.getResultData() != null) {
                        setTaskClosureToView(taskClosureDetails);
                    } else {
                        mLinearLayoutRatingWrapper.setVisibility(View.GONE);
                        mTaskClosureWrapper.setVisibility(View.GONE);
                    }
                } else {
                    mLinearLayoutRatingWrapper.setVisibility(View.GONE);
                    mTaskClosureWrapper.setVisibility(View.GONE);
                    Toast.makeText(getContext(), taskClosureDetails.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else if (classType.equalsIgnoreCase(DownloadReport.class.getSimpleName())) {
                Gson gson = new Gson();
                DownloadReport downloadReport = gson.fromJson(urlConnectionResponse.resultData, DownloadReport.class);
                if (downloadReport != null) {
                    FWLogger.logInfo(TAG, "Code : " + downloadReport.getCode());
                    FWLogger.logInfo(TAG, "Message : " + downloadReport.getMessage());
                    if (downloadReport.getCode().equalsIgnoreCase("200")) {
                        if (!hasPermissions(getContext(), PERMISSIONS)) {
                            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                            Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                            t.show();
                        } else {
                            DownloadFile.downloadFile(downloadReport.getMessage(), getActivity());
                        }
                    } else {
                        Toast.makeText(getContext(), downloadReport.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
//                navigateToTab(R.id.navigation_task);
            }
        }
    }

    private void setTaskClosureToView(TaskClosure taskClosureDetails) {
        // After Get Response
        mTextViewWorkMode.setText(taskClosureDetails.getResultData().getWorkModeType());
        mRatingBar.setRating((float) taskClosureDetails.getResultData().getRatingBar());
        mTextViewRating.setText("" + (float) mRatingBar.getRating());
        mRatingBar.setFocusable(false);
        mRatingBar.setIsIndicator(true);
        mTextViewSignedBy.setText(taskClosureDetails.getResultData().getSignedBy());
        try {
            mFieldImageURL = taskClosureDetails.getResultData().getFieldPhoto();
            mFieldImageURL1 = taskClosureDetails.getResultData().getFieldPhoto1();
            mFieldImageURL2 = taskClosureDetails.getResultData().getFieldPhoto2();
        } catch (Exception ex) {
            ex.getMessage();
        }

        if (taskClosureDetails.getResultData().getPreDeviceInfoDto() != null) {
            if (taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath() != null && !taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath().isEmpty())
                mImageBeforeTaskURL = taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath();
            {
                try {
                    Picasso.get().load(mImageBeforeTaskURL).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                            .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewPhotoBeforeTask, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError(Exception e) {
                                    mImageViewPhotoBeforeTask.setImageResource(R.drawable.field_location_upload_photo);
                                }
                            });
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
            // BEFORE IMG2
            if (taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath() != null && !taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath().isEmpty())
                mImageBeforeTaskURL1 = taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath1();
            {
                try {
                    Picasso.get().load(mImageBeforeTaskURL1).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                            .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewPhotoBeforeTask1, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError(Exception e) {
                                    mImageViewPhotoBeforeTask1.setImageResource(R.drawable.field_location_upload_photo);
                                }
                            });
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            // BEFORE IMG3

            if (taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath() != null && !taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath().isEmpty())
                mImageBeforeTaskURL2 = taskClosureDetails.getResultData().getPreDeviceInfoDto().getDeviceInfoImagePath2();
            {
                try {
                    Picasso.get().load(mImageBeforeTaskURL2).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                            .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewPhotoBeforeTask2, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError(Exception e) {
                                    mImageViewPhotoBeforeTask2.setImageResource(R.drawable.field_location_upload_photo);
                                }
                            });
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        } else {
            mImageViewPhotoBeforeTask.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_no_image));
        }
        // BEFORE IMG2

        if (taskClosureDetails.getResultData().getMobileNo() == 0)
            mTextViewSignedMobileNo.setText(getResources().getString(R.string.na));
        else mTextViewSignedMobileNo.setText("" + taskClosureDetails.getResultData().getMobileNo());

        // FIELD PHOTO : 1/2/3
        try {
            Picasso.get().load(mFieldImageURL).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewFieldPhoto, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewFieldPhoto.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }
        // 2
        try {
            Picasso.get().load(mFieldImageURL1).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewFieldPhoto2, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewFieldPhoto2.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }

        //3
        try {
            Picasso.get().load(mFieldImageURL2).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit().centerCrop()
//                .resizeDimen(R.dimen.img_width, R.dimen.img_height)
//                .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewFieldPhoto3, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewFieldPhoto3.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }

        Picasso.get().load(taskClosureDetails.getResultData().getCustomerSignatureImage()).placeholder(R.drawable.sign_bg).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewSignature, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                mImageViewSignature.setImageResource(R.drawable.sign_bg);
            }
        });

        //Notes View
        if (taskClosureDetails.getResultData().getTechnicalNotedto() != null) {
            mTechnicalNoteDTOs = taskClosureDetails.getResultData().getTechnicalNotedto();
            if (mTechnicalNoteDTOs.size() > 0) {
                mLinearNotesList.setVisibility(View.VISIBLE);
                mNotesViewAdapter = new NotesViewAdapter(mTechnicalNoteDTOs, getContext());
                mLayoutManager = new LinearLayoutManager(getContext());
                mRecyclerViewNote.setLayoutManager(mLayoutManager);
                mRecyclerViewNote.setAdapter(mNotesViewAdapter);
            }
        }

        // Device List View
        if (taskClosureDetails.getResultData().getDeviceInfoList() != null) {
            mDeviceInfoLists = taskClosureDetails.getResultData().getDeviceInfoList();
            if (mDeviceInfoLists.size() > 0) {
                mLinearDeviceList.setVisibility(View.VISIBLE);
                mDeviceListViewAdapter = new DeviceListViewAdapter(mDeviceInfoLists, getActivity());
                mLayoutManager = new LinearLayoutManager(getContext());
                mRecyclerViewDeviceList.setLayoutManager(mLayoutManager);
                mRecyclerViewDeviceList.setAdapter(mDeviceListViewAdapter);
            }
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /*private void downloadFile(String path) {
        try {
            final ProgressDialog pd = new ProgressDialog(getContext());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean downloadResponse = HttpDownloadUtility.downloadFile(path, storageDir.getAbsolutePath());
                        if (downloadResponse) {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.file_download_successfully, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            String filename = path.substring(path.lastIndexOf("/") + 1);
                            File file = new File(storageDir, filename);
                            Uri uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", file);
                            // Open file with user selected app
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(uri, "application/pdf");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.sownload_failed, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        pd.dismiss();
                    } catch (IOException ex) {
                        pd.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean checkStoragePermission() {
        boolean storageFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    storageFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkWriteStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                final int checkReadStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if (checkWriteStorage != PackageManager.PERMISSION_GRANTED && checkReadStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    storageFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return storageFlag;
    }


    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}