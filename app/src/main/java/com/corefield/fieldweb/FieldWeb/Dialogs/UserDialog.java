package com.corefield.fieldweb.FieldWeb.Dialogs;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.corefield.fieldweb.Adapter.LanguageListAdapter;
import com.corefield.fieldweb.DTO.Attendance.TechMonthlyAttendance;
import com.corefield.fieldweb.DTO.User.AddAttendance;
import com.corefield.fieldweb.DTO.User.CompanyDetails;
import com.corefield.fieldweb.DTO.User.UpdateUser;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.DTO.UserDisclaimer.AcceptDisclaimer;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerAttendanceFragment;
import com.corefield.fieldweb.FieldWeb.Admin.TechnicianListFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.LocaleUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserDialog {
    public boolean isAlertDialogShowing = false;
    public final String TAG = UserDialog.class.getSimpleName();
    static Place mTempPlace = null;
    private int mSelectMonth, mSelectedYear;
    private static UserDialog mUserDialog;
    Calendar mCalendar;

    /**
     * The private constructor for the UserDialog Singleton class
     */
    private UserDialog() {
    }

    public static UserDialog getInstance() {
        //instantiate a new UserDialog if we didn't instantiate one yet
        if (mUserDialog == null) {
            mUserDialog = new UserDialog();
        }
        return mUserDialog;
    }

    public void disclaimerDialog(Activity activity, String disclaimerHtml, String DCN) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
//        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_disclaimer);
        CheckBox checkBoxAgree = dialog.findViewById(R.id.disclaimer_agree_radio_button);
        TextView textViewContinue = dialog.findViewById(R.id.disclaimer_continue_textview);
        TextView textViewDisclaimer = dialog.findViewById(R.id.textView_disclaimer);
        String convertedHtml = String.valueOf(Html.fromHtml(disclaimerHtml));
        textViewDisclaimer.setText(Html.fromHtml(convertedHtml));
        textViewContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxAgree.isChecked()) {
                    isAlertDialogShowing = false;
                    SharedPrefManager.getInstance(activity).setDisclaimer(Constant.UserDisclaimer.AGREE);
                    AcceptDisclaimer.ResultData acceptDisclaimerResultData = new AcceptDisclaimer.ResultData();
                    acceptDisclaimerResultData.setUserId(SharedPrefManager.getInstance(activity).getUserId());
                    acceptDisclaimerResultData.setIsAccepted(true);
                    acceptDisclaimerResultData.setDCN(DCN);
                    ((HomeActivityNew) activity).acceptDisclaimer(acceptDisclaimerResultData);
                    dialog.dismiss();
                } else {
                    Toast.makeText(activity, activity.getString(R.string.click_on_agree), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.setCancelable(false);
        isAlertDialogShowing = true;
        dialog.show();
    }

    public void showAttendanceMarkDialog(Context context) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_check_in);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView textViewDate = dialog.findViewById(R.id.textView_date);
        Button buttonCheckIn = dialog.findViewById(R.id.button_check_in);

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String currentDate = sdf.format(c.getTime());
        FWLogger.logInfo(TAG, "Current date : " + currentDate);
        FWLogger.logInfo(TAG, "date : " + mYear + "-" + (mMonth + 1) + "-" + mDay);

        textViewDate.setText(currentDate);
        buttonCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddAttendance.ResultData addAttendance = new AddAttendance.ResultData();
                addAttendance.setAttendanceDate(mYear + "-" + (mMonth + 1) + "-" + mDay);
                addAttendance.setAttendanceTypeId(2);
                addAttendance.setCreatedBy(SharedPrefManager.getInstance(context).getUserId());
                addAttendance.setIsModelError(true);
                addAttendance.setIsSuccessful(true);
                addAttendance.setUserId(SharedPrefManager.getInstance(context).getUserId());
                addAttendance.setUpdatedBy(SharedPrefManager.getInstance(context).getUserId());
                double latitude = SharedPrefManager.getInstance(context).getUserLocation().getLatitude();
                double longitude = SharedPrefManager.getInstance(context).getUserLocation().getLongitude();
                addAttendance.setLatitude(String.valueOf(latitude));
                addAttendance.setLongitude(String.valueOf(longitude));
                addAttendance.setAttendanceMarkedPlace(MapUtils.getFullAddressFromLatLong(context, latitude, longitude));
                FWLogger.logInfo(TAG, "Attd. lat: " + addAttendance.getLatitude());
                FWLogger.logInfo(TAG, "Attd. long: " + addAttendance.getLongitude());
                FWLogger.logInfo(TAG, "Attd. location: " + addAttendance.getAttendanceMarkedPlace());
                FWLogger.logInfo(TAG, "Attd. date: " + addAttendance.getAttendanceDate());
                ((HomeActivityNew) context).markAttendance(addAttendance);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

/*
    public void changeLanguageDialog(Activity activity) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_change_language);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        RecyclerView rvLanguageList = dialog.findViewById(R.id.recycler_languageList);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        ArrayList<String> languageList = new ArrayList<>();
        languageList.add("English");
        languageList.add("हिन्दी");

        rvLanguageList.setLayoutManager(new LinearLayoutManager(activity));
        LanguageListAdapter languageListAdapter = new LanguageListAdapter(activity, languageList);
        rvLanguageList.setAdapter(languageListAdapter);

        languageListAdapter.setClickListener(new LanguageListAdapter.LanguageClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FWLogger.logInfo(TAG, "Lang Position : " + position);
                switch (languageListAdapter.getItem(position)) {
                    case "English":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ENGLISH, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "हिन्दी":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_HINDI, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;
                    default:
                        break;
                }

                dialog.dismiss();

                if (activity.getClass().getName() == HomeActivityNew.class.getName())
                    ((HomeActivityNew) activity).updatePrefLanguage();
                else
                    UserDialog.restartActivity(activity);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
*/

    public void changeLanguageDialog(Activity activity) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_change_language);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        RecyclerView rvLanguageList = dialog.findViewById(R.id.recycler_languageList);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        ArrayList<String> languageList = new ArrayList<>();
        languageList.add("English");
        languageList.add("हिन्दी");
        languageList.add("ਪੰਜਾਬੀ");
        languageList.add("বাংলা");
        languageList.add("मराठी");
        languageList.add("ಕನ್ನಡ");
        languageList.add("தமிழ்");
        languageList.add("తెలుగు");
        languageList.add("മലയാളം");
        languageList.add("عربي");

        rvLanguageList.setLayoutManager(new LinearLayoutManager(activity));
        LanguageListAdapter languageListAdapter = new LanguageListAdapter(activity, languageList);
        rvLanguageList.setAdapter(languageListAdapter);

        languageListAdapter.setClickListener(new LanguageListAdapter.LanguageClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FWLogger.logInfo(TAG, "Lang Position : " + position);
                switch (languageListAdapter.getItem(position)) {
                    case "English":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ENGLISH, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "हिन्दी":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_HINDI, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "ਪੰਜਾਬੀ":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_PUNJABI, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "বাংলা":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_BENGALI, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "मराठी":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_MARATHI, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "ಕನ್ನಡ":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_KANNADA, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "தமிழ்":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_TAMIL, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "తెలుగు":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_TELUGU, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "മലയാളം":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_MALAYALAM, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    case "عربي":
                        SharedPrefManager.getInstance(activity).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ARABIC, position);
                        LocaleUtils.setLocale(activity, SharedPrefManager.getInstance(activity).getPreferredLanguage());
                        break;

                    default:
                        break;
                }

                dialog.dismiss();
                SharedPrefManager.getInstance(activity.getApplicationContext()).setisFirstTimeChangeLang(false);
                if (activity.getClass().getName() == HomeActivityNew.class.getName()) {
                    ((HomeActivityNew) activity).updatePrefLanguage();
                    UserDialog.restartActivity(activity);
                } else
                    UserDialog.restartActivity(activity);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void restartActivity(Activity activity) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Intent intent = activity.getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                activity.overridePendingTransition(0, 0);
                activity.finish();

                activity.overridePendingTransition(0, 0);
                activity.startActivity(intent);
            }
        });

        if (activity.getClass().getName() == HomeActivityNew.class.getName())
            ((HomeActivityNew) activity).mBottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);
    }

    public void editProfileNewDialog(Activity activity, UserDetails.ResultData userDetails) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.fragment_edit_profile_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        EditText editTextFirstName, editTextLastName, editTextContact, editTextAddress, editTextEmail,
                editTextCompanyAddress, editTextGSTPanNo, editTextCompanyContact,
                editTextCompanyWebsite, editTextReferralCode;

        CircleImageView imageViewProfile;
        Button buttonSave, buttonCancel;
        ImageView imageViewCompanyLogo;

        editTextFirstName = dialog.findViewById(R.id.editText_firstname);
        editTextLastName = dialog.findViewById(R.id.editText_lastname);
        editTextContact = dialog.findViewById(R.id.editText_contact);
        editTextAddress = dialog.findViewById(R.id.editText_address);
        editTextEmail = dialog.findViewById(R.id.editText_email);
        editTextCompanyAddress = dialog.findViewById(R.id.editText_company_address);
        editTextGSTPanNo = dialog.findViewById(R.id.editText_gst_or_pan_no);
        editTextCompanyContact = dialog.findViewById(R.id.editText_company_cont_no);
        editTextCompanyWebsite = dialog.findViewById(R.id.editText_company_website);
        editTextReferralCode = dialog.findViewById(R.id.editText_referral_code);
        imageViewProfile = dialog.findViewById(R.id.imageView_user_profile);
        imageViewCompanyLogo = dialog.findViewById(R.id.signup_screen_company_image);

        buttonSave = dialog.findViewById(R.id.button_save);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        Picasso.get().load(userDetails.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageViewProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                imageViewProfile.setImageResource(R.drawable.profile_icon);
            }
        });

        Picasso.get().load(userDetails.getCompanyLogo()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageViewCompanyLogo, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
//                imageViewCompanyLogo.setImageResource(R.drawable.profile_icon);
            }
        });

        editTextFirstName.setText(userDetails.getFirstName());
        editTextLastName.setText(userDetails.getLastName());
        editTextContact.setText(userDetails.getContactNo());
        editTextAddress.setText(userDetails.getAddress());
        editTextEmail.setText(userDetails.getEmail());
        editTextCompanyAddress.setText(userDetails.getCompanyAddress());
        editTextGSTPanNo.setText(userDetails.getCompanyGSTorPanNo());
        editTextCompanyContact.setText("" + userDetails.getCompanyContactNo());
        editTextCompanyWebsite.setText(userDetails.getCompanyWebsite());
        editTextReferralCode.setText("" + userDetails.getReferralCodesId());

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivityNew) activity).pickImage("");
            }
        });

        imageViewCompanyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivityNew) activity).pickImage("");
            }
        });

        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editTextFirstName)) {
                    editTextFirstName.setError(activity.getResources().getString(R.string.please_enter_first_name));
                } else if (editTextFirstName.getText().toString().contains(" ") || editTextFirstName.getText().toString().trim().isEmpty()) {
                    editTextFirstName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextLastName)) {
                    editTextLastName.setError(activity.getResources().getString(R.string.please_enter_last_name));
                } else if (editTextLastName.getText().toString().contains(" ") || editTextLastName.getText().toString().trim().isEmpty()) {
                    editTextLastName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextContact)) {
                    editTextContact.setError(activity.getString(R.string.please_enter_contact_number));
                } else if (isValidPhone(editTextContact)) {
                    editTextContact.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(editTextContact.getText().toString()) == 0)) {
                    editTextContact.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(editTextCompanyAddress)) {
                    editTextCompanyAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (editTextEmail.getText().toString() != null && !editTextEmail.getText().toString().isEmpty()
                        && Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches() == false) {
                    editTextEmail.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else {
                    UpdateUser.ResultData updateUser = new UpdateUser.ResultData();
                    updateUser.setFirstName(editTextFirstName.getText().toString());
                    updateUser.setLastName(editTextLastName.getText().toString());
                    updateUser.setContactNo(editTextContact.getText().toString());
                    updateUser.setEmail(editTextEmail.getText().toString());
                    updateUser.setAddress(editTextAddress.getText().toString());
                    updateUser.setCompanyAddress(editTextCompanyAddress.getText().toString());
                    updateUser.setCompanyGSTorPanNo(editTextGSTPanNo.getText().toString());
                    updateUser.setCompanyContactNo(Integer.parseInt(editTextCompanyContact.getText().toString()));
                    updateUser.setCompanyWebsite(editTextCompanyWebsite.getText().toString());
                    updateUser.setReferralCodesId(Integer.parseInt(editTextReferralCode.getText().toString()));

                    updateUser.setUserId(userDetails.getId());
                    updateUser.setUpdatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    updateUser.setIsSuccessful(true);
                    updateUser.setIsModelError(true);
                    ((HomeActivityNew) activity).updateUser(updateUser);
                    dialog.dismiss();
                }
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }
                if (place != null) {
                    editTextAddress.setText(place.getAddress());
                    mTempPlace = place;
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void technicianDetailsDialog(Context context, UsersList.ResultData userDetails, Activity mActivity) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_employee_details);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView textViewTechName = dialog.findViewById(R.id.textView_tech_name);
        TextView textViewMobile = dialog.findViewById(R.id.textView_mobile_no);
        TextView textViewRole = dialog.findViewById(R.id.textView_role);
        TextView textViewUserName = dialog.findViewById(R.id.textView_username);
        TextView textViewAddress = dialog.findViewById(R.id.textView_address);
        ImageView imageViewEdit = dialog.findViewById(R.id.imageView_edit);
        ImageView deleteTechAccount = dialog.findViewById(R.id.deleteTechAccount);

        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        if (userDetails.getFirstName() != null && !userDetails.getFirstName().equalsIgnoreCase("null"))
            textViewTechName.setText(userDetails.getFirstName() + " " + userDetails.getLastName());
        else
            textViewTechName.setText(context.getResources().getString(R.string.na));

        if (userDetails.getContactNo() != null && !userDetails.getContactNo().equalsIgnoreCase("null"))
            textViewMobile.setText(userDetails.getContactNo());
        else
            textViewMobile.setText(context.getResources().getString(R.string.na));

        if (userDetails.getRole() != null && !userDetails.getRole().equalsIgnoreCase("null"))
            textViewRole.setText(userDetails.getRole());
        else
            textViewRole.setText(context.getResources().getString(R.string.na));

        if (userDetails.getUserName() != null && !userDetails.getUserName().equalsIgnoreCase("null"))
            textViewUserName.setText(userDetails.getUserName());
        else
            textViewUserName.setText(context.getResources().getString(R.string.na));

        if (userDetails.getAddress() != null && !userDetails.getAddress().equalsIgnoreCase("null"))
            textViewAddress.setText(userDetails.getAddress());
        else
            textViewAddress.setText(context.getResources().getString(R.string.na));

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((HomeActivityNew) context).loadProfileFragment(userDetails);
            }
        });

        deleteTechAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new TechnicianListFragment().DeleteTechnicianDialog(context, userDetails, mActivity);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


/*
    public void showTechAttendanceDialog(Context context, Fragment fragment, UsersList.ResultData resultData, List<TechMonthlyAttendance.ResultData> mTechAttend) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_tech_attendance);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonClose = dialog.findViewById(R.id.button_close);
        ImageView imageViewProfile = dialog.findViewById(R.id.technician_image);
        TextView textViewName = dialog.findViewById(R.id.technician_name);
        TextView textViewOccupation = dialog.findViewById(R.id.technician_occupation);
        ImageView imageViewCall = dialog.findViewById(R.id.imageView_call);
        CalendarView mCalendarView = dialog.findViewById(R.id.calendar_attendance);

        TextView txtDate = dialog.findViewById(R.id.txtDate);
        TextView txtCheckIn = dialog.findViewById(R.id.txtCheckIn);
        TextView txtCheckOut = dialog.findViewById(R.id.txtCheckOut);

        ((OwnerAttendanceFragment) fragment).getTechAttendance(resultData.getId(), mCalendarView);

        textViewName.setText(resultData.getFirstName() + " " + resultData.getLastName());
        textViewOccupation.setText(resultData.getRole());

        mCalendar = Calendar.getInstance();
        int mYear = mCalendar.get(Calendar.YEAR);
        int mMonth = mCalendar.get(Calendar.MONTH);
//        FWLogger.logInfo(TAG, "mMonth_Dialog : "+mMonth);
        int mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
        mSelectMonth = mMonth;
        mSelectedYear = mYear;
//        FWLogger.logInfo(TAG, "mSelectMonth_Dialog : "+mSelectMonth);
        mCalendarView.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                if (mSelectMonth == 0 || (mSelectMonth == 1 && mSelectedYear == mYear)) {
                    mSelectMonth = 12;
                    mSelectedYear = mSelectedYear - 1;
                } else if (mSelectMonth > 0 && mSelectMonth <= 12) {
                    mSelectMonth = mSelectMonth - 1;
                }
                ((OwnerAttendanceFragment) fragment).mDate = mSelectedYear + "-" + mSelectMonth + "-" + ((HomeActivityNew) context).mSelectedDate;
                ((OwnerAttendanceFragment) fragment).getTechAttendance(resultData.getId(), mCalendarView);
            }
        });

        mCalendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
//                mSelectMonth = mSelectMonth + 1;
                if (mSelectMonth == 12) {
                    mSelectMonth = 1;
                    mSelectedYear = mSelectedYear + 1;
                } else if (mSelectMonth < 12 && mSelectMonth > 0) {
                    mSelectMonth = mSelectMonth + 1;
                }
                ((OwnerAttendanceFragment) fragment).mDate = mSelectedYear + "-" + mSelectMonth + "-" + ((HomeActivityNew) context).mSelectedDate;
                ((OwnerAttendanceFragment) fragment).getTechAttendance(resultData.getId(), mCalendarView);
            }
        });

        Picasso.get().load(resultData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageViewProfile, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError(Exception e) {
                imageViewProfile.setImageResource(R.drawable.profile_icon);
            }
        });

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultData.getContactNo() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + resultData.getContactNo()));
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OwnerAttendanceFragment) fragment).setDates();
                dialog.dismiss();
            }
        });


        mCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                try {
                    java.util.Date date = new Date(eventDay.getCalendar().getTime().toString());
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String format = formatter.format(date);
                    System.out.println(format);
                    String finalD = format + "T00:00:00";
                    if (mTechAttend.size() > 0) {
                        for (int i = 0; i < mTechAttend.size(); i++) {
                            if (finalD.contains(mTechAttend.get(i).getDate())) {
                                try {
                                    if (mTechAttend.get(i).getCheckIn() == null) {
                                        txtDate.setText("Date: " + " " + "-NA-");
                                        txtCheckIn.setText("CheckIn  : " + " " + "-NA-");
                                    } else {
                                        txtDate.setText("Date : " + " " + mTechAttend.get(i).getCheckIn().split("T")[0]);
                                        txtCheckIn.setText("CheckIn   : " + " " + mTechAttend.get(i).getCheckIn().split("T")[1].split("[.]", 0)[0]);
                                    }
                                    if (mTechAttend.get(i).getCheckOut() == null) {
                                        txtCheckOut.setText("CheckOut : " + " " + "-NA-");
                                    } else {
                                        txtDate.setText("Date : " + " " + mTechAttend.get(i).getCheckOut().split("T")[0]);
                                        txtCheckOut.setText("CheckOut : " + " " + mTechAttend.get(i).getCheckOut().split("T")[1].split("[.]", 0)[0]);
                                    }
                                } catch (Exception ex) {
                                    ex.getMessage();
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });


        dialog.show();
    }
*/


}
