package com.corefield.fieldweb.FieldWeb.Dialogs;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.Admin.AddBulkTechFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeGetLinkActivity;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.OnCompleteListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class CommonDialog {
    private static CommonDialog mCommonDialog;
    public static boolean isAlertDialogShowing = false;
    ReviewManager reviewManager;
    ReviewInfo reviewInfo = null;
    public static String itemListFromWhere = "";

    /**
     * The private constructor for the EnquiryDialog Singleton class
     */
    private CommonDialog() {
    }

    public static CommonDialog getInstance() {
        //instantiate a new EnquiryDialog if we didn't instantiate one yet
        if (mCommonDialog == null) {
            mCommonDialog = new CommonDialog();
        }
        return mCommonDialog;
    }

    public void showImageInFullScreenMode(Activity activity, String imageURL, String imageName) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_show_image_view);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        ImageView imageView = dialog.findViewById(R.id.image_view_full_screen);
        TextView textViewImageName = dialog.findViewById(R.id.textViewImageName);
        ImageView imageViewDownload = dialog.findViewById(R.id.imageView_download);

        textViewImageName.setText(imageName);

//        .placeholder(R.drawable.ic_image_view)
        Picasso.get().load(imageURL)
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.ic_image_view);
                    }
                });

        imageView.setOnTouchListener(new ImageMatrixTouchHandler(activity));

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
            }
        });

        imageViewDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadFile.downloadFile(imageURL, activity);
            }
        });

        isAlertDialogShowing = true;
        dialog.show();
    }

    public void getLeadFormLink(Activity activity) {
        try {
            Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.diaolog_lead_form_link);
            dialog.getWindow().setGravity(Gravity.BOTTOM);

            Button btn_getEnquiryFormLink = dialog.findViewById(R.id.btn_getEnquiryFormLink);
            //Button btngeLeadFormQR = dialog.findViewById(R.id.btngeLeadFormQR);
            Button button_cancel = dialog.findViewById(R.id.button_cancel);
            TextView haddenq = dialog.findViewById(R.id.textView_how_to_get_link);


            haddenq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent send = new Intent(activity.getApplicationContext(), YouTubeGetLinkActivity.class);
                    activity.startActivity(send);
                }
            });

            btn_getEnquiryFormLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String getUserID = String.valueOf(SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                        if (getUserID != null) {
                            String[] iVal = getUserID.split("");
                            String encryptedVal = enryptUserID(iVal);
                            String strgetFormLink = URLConstant.Base.LEADFORM_LINK + encryptedVal;
                            // String strgetFormLink = URLConstant.Base.GET_LINK;
                            if (!strgetFormLink.isEmpty()) {
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "FieldWeb : Lead Form");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, strgetFormLink);
                                activity.startActivity(Intent.createChooser(sharingIntent, "Share"));
                            }
                        }
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            });

            /*btngeLeadFormQR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String getUserID = String.valueOf(SharedPrefManager.getInstance(getApplicationContext()).getUserId());
                        if (getUserID != null) {
                            String[] iVal = getUserID.split("");
                            String encryptedVal = enryptUserID(iVal);
                            String strgetFormLink = "https://quickchart.io/qr?text=" + URLConstant.Base.LEADFORM_LINK + encryptedVal + "=&centerImageUrl=https://play-lh.googleusercontent.com/y7A-hlJPYg9k_b1eSAkZlsIyxKIwjIkuDXK5k4CKblLhzTGPSr42algKOaNECMOdJ84&size=500";

                            if (!strgetFormLink.isEmpty()) {
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "FieldWeb : Lead Form");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, strgetFormLink);
                                DownloadFile.downloadFile(strgetFormLink, activity);
                                //activity.startActivity(Intent.createChooser(sharingIntent, "Share"));
                            }
                        }
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            });*/


            button_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void addOptionsDialog(Activity activity) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_options_new);

        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(activity).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(activity).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);

        LinearLayout linearAddEnq = dialog.findViewById(R.id.linear_add_enq);
        LinearLayout linearAddTask = dialog.findViewById(R.id.linear_add_task);
        LinearLayout linearAddTech = dialog.findViewById(R.id.linear_add_tech);
        LinearLayout linearAddItem = dialog.findViewById(R.id.linear_add_item);
        LinearLayout linearAssignItem = dialog.findViewById(R.id.linear_assign_item);
        LinearLayout linearAddDeductBal = dialog.findViewById(R.id.linear_add_deduct_bal);
        LinearLayout linearAddAMC = dialog.findViewById(R.id.linear_add_amc);
        LinearLayout lineargetlink = dialog.findViewById(R.id.linear_get_link);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        linearAddEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                ((HomeActivityNew) activity).addEnquiry();
            }
        });

        linearAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                itemListFromWhere = "cDialog";
                ((HomeActivityNew) activity).getItemList();
                /*((HomeActivityNew) activity).addTask();*/
            }
        });

        linearAddTech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                ((HomeActivityNew) activity).loadFragment(new AddBulkTechFragment(), R.id.navigation_home);
            }
        });

        linearAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                ((HomeActivityNew) activity).addItem();
            }
        });

        linearAssignItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                itemListFromWhere = "dialog";
                ((HomeActivityNew) activity).getItemList();
                /*((HomeActivityNew) activity).assignItem();*/
            }
        });

        linearAddDeductBal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                ((HomeActivityNew) activity).addDeductBalance();
            }
        });

        linearAddAMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AMCDialog amcDialog = AMCDialog.getInstance();
//                amcDialog.addEditAMC(activity, this, mServiceOccurrenceTypeList, mReminderModeList, -1, -1, -1);
                FirebaseAnalytics.getInstance(activity).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                dialog.dismiss();
                ((HomeActivityNew) activity).serviceOccurrenceTask();
                //((HomeActivityNew) activity).addAMCDialog(((HomeActivityNew) activity).mServiceOccurrenceTypeList, ((HomeActivityNew) activity).mReminderModeList);

            }
        });

        dialog.show();
        if (HomeActivityNew.OwnerAppTourFlag.equalsIgnoreCase("SKIPABLE")) {
            if (!SharedPrefManager.getInstance(getApplicationContext()).isAppTourCompleted()) {
                skippableAppTour(dialog, activity);
            }
        } else if (HomeActivityNew.onDemandAppTourFlag) {
            nonSkippableAppTour(dialog, activity);
        }


        // }
    }

    private void skippableAppTour(Dialog dialog, Activity activity) {
        try {
            TapTarget target1 = TapTarget.forView(dialog.findViewById(R.id.linear_add_tech),
                    activity.getString(R.string.title_add_tech),
                    activity.getString(R.string.title_add_tech))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.black)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.transperant_black)
                    .dimColor(R.color.black).targetRadius(60);

            showFor(dialog, target1, new TapTargetView.Listener(), activity);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void nonSkippableAppTour(Dialog dialog, Activity activity) {
        try {
            TapTarget target1 = TapTarget.forView(dialog.findViewById(R.id.linear_add_tech),
                    activity.getString(R.string.title_add_tech),
                    activity.getString(R.string.title_add_tech))
                    .drawShadow(true)
                    .cancelable(false)
                    .targetCircleColor(R.color.black)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.transperant_black)
                    .dimColor(R.color.black).targetRadius(60);
            showFor(dialog, target1, new TapTargetView.Listener(), activity);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    public static TapTargetView showFor(Dialog dialog, TapTarget target, TapTargetView.Listener listener, Activity mActivity) {
        if (dialog == null) throw new IllegalArgumentException("Dialog is null");
        final Context context = dialog.getContext();
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.type = WindowManager.LayoutParams.TYPE_APPLICATION;
        params.format = PixelFormat.RGBA_8888;
        params.flags = 0;
        params.gravity = Gravity.START | Gravity.TOP;
        params.x = 0;
        params.y = 0;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;

        final TapTargetView tapTargetView = new TapTargetView(context, windowManager, null, target, listener);
        windowManager.addView(tapTargetView, params);

        tapTargetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapTargetView.dismiss(true);
                if (HomeActivityNew.OwnerAppTourFlag.equalsIgnoreCase("SKIPABLE")) {
                    SharedPrefManager.getInstance(getApplicationContext()).setAppTourCompleted(true);
                    ((HomeActivityNew) mActivity).AppGuideForOwner_Skippable();
                } else if (HomeActivityNew.onDemandAppTourFlag) {
                    ((HomeActivityNew) mActivity).AppGuideForOwner_NonSkippable();
                }

                dialog.dismiss();
            }
        });

        return tapTargetView;
    }

    public void rateUsDialog(Activity activity) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_rate_us);

        ImageView imageViewClose = dialog.findViewById(R.id.imageView_close);
        ImageView imageViewThumbsUp = dialog.findViewById(R.id.imageView_thumbs_up);
        ImageView imageViewThumbsDown = dialog.findViewById(R.id.imageView_thumbs_down);
        TextView textViewNotReally = dialog.findViewById(R.id.textView_not_really);
        TextView textViewLoveIt = dialog.findViewById(R.id.textView_love_it);
        LinearLayout linearLoveIt = dialog.findViewById(R.id.linear_love_it);
        LinearLayout linearNotReally = dialog.findViewById(R.id.linear_not_really);

        getReviewInfo(activity);

        linearLoveIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLoveIt.setBackground(activity.getResources().getDrawable(R.drawable.button_bg));
                imageViewThumbsUp.setColorFilter(activity.getResources().getColor(R.color.White));
                textViewLoveIt.setTextColor(activity.getResources().getColor(R.color.White));
                startReviewFlow(activity);
//                rateMe(activity);
            }
        });

        linearNotReally.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearNotReally.setBackground(activity.getResources().getDrawable(R.drawable.button_bg));
                imageViewThumbsDown.setColorFilter(activity.getResources().getColor(R.color.White));
                textViewNotReally.setTextColor(activity.getResources().getColor(R.color.White));
                dialog.dismiss();
            }
        });

        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void rateMe(Activity activity) {
        try {
            FWLogger.logInfo("TAG", "PackageName : " + activity.getPackageName());
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + activity.getPackageName())));
        } catch (android.content.ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        }
    }

    private void getReviewInfo(Activity activity) {
        reviewManager = ReviewManagerFactory.create(activity);
        Task<ReviewInfo> manager = reviewManager.requestReviewFlow();
        manager.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reviewInfo = task.getResult();
            } else {
                Toast.makeText(activity, "In App ReviewFlow failed to start", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void startReviewFlow(Activity activity) {
        if (reviewInfo != null) {
            Task<Void> flow = reviewManager.launchReviewFlow(activity, reviewInfo);
            flow.addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(Task<Void> task) {
                    Toast.makeText(activity, "In App Rating completed", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(activity, "In App Rating failed", Toast.LENGTH_LONG).show();
        }
    }

    public String Encrypt(String UseridInput) {
        String EncString = "";
        switch (UseridInput) {
            case "0":
                EncString = "AZ=";
                break;


            case "1":
                EncString = "BY/";
                break;


            case "2":
                EncString = "CX=";
                break;


            case "3":
                EncString = "DW/";
                break;


            case "4":
                EncString = "EV=";
                break;


            case "5":
                EncString = "FU/";
                break;


            case "6":
                EncString = "G-=";
                break;


            case "7":
                EncString = "/=/";
                break;


            case "8":
                EncString = "=/=";
                break;


            case "9":
                EncString = "A=/";
                break;
        }
        return EncString;
    }

    private String enryptUserID(String[] userID) {
        String encrypted = "";
        /*String sourceStr = userID;
        try {
            encrypted = AESUtils.encrypt(sourceStr);
            Log.d("TEST", "encrypted:" + encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
        String EncUID = "";
        for (int i = 0; i < userID.length; i++) {
            EncUID += Encrypt(userID[i]);
        }
        return EncUID;
    }


}
