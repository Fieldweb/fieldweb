package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.FieldWeb.Admin.CRMTaskAmcTabHostFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ExpenseDetailsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ExpenseTechnicianListFragment;
import com.corefield.fieldweb.FieldWeb.Admin.PassbookExpenditureTabHost;
import com.corefield.fieldweb.FieldWeb.Admin.TechnicianListFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddDecBalActivity;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class AddBalanceDialog {
    private static AddBalanceDialog mExpenditureDialog;
    public static ImageView mImageViewExpense;
    public static int mUserId = 0, mRemainingBalance = 0;
    public static String mBalType = "ADD";
    private static String addDeduct_Description = "";

    /**
     * The private constructor for the AdminDialog Singleton class
     */
    private AddBalanceDialog() {
    }

    public static AddBalanceDialog getInstance() {
        //instantiate a new ExpenditureDialog if we didn't instantiate one yet
        if (mExpenditureDialog == null) {
            mExpenditureDialog = new AddBalanceDialog();
        }
        return mExpenditureDialog;
    }

    public static void addExpenseDialog(Context context, Fragment fragment) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_balance);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonAddExpense = dialog.findViewById(R.id.button_add);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        mImageViewExpense = dialog.findViewById(R.id.imageview_expense);
        EditText editTextExpenseName = dialog.findViewById(R.id.editText_expense_name);
        EditText editTextExpenseAmount = dialog.findViewById(R.id.editText_expense_amount);

        mImageViewExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExpenseDetailsFragment) fragment).pickImage(ExpenseDetailsFragment.mExpenseImage, 1, null);
            }
        });

        buttonAddExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editTextExpenseName)) {
                    editTextExpenseName.setError(context.getString(R.string.please_enter_expense_details));
                } else if (isEmpty(editTextExpenseAmount)) {
                    editTextExpenseAmount.setError(context.getString(R.string.please_enter_expense_amout));
                } else if ((Integer.valueOf(editTextExpenseAmount.getText().toString()) == 0)) {
                    editTextExpenseAmount.setError(context.getString(R.string.please_enter_valid_amount));
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(context).getUserId());
                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(context).getUserGroup());
                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                    bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
                    bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, Integer.valueOf(editTextExpenseAmount.getText().toString())); //To equalize event column
                    FirebaseAnalytics.getInstance(context).logEvent(FirebaseGoogleAnalytics.Event.EXPENSE, bundle);
                    ((ExpenseDetailsFragment) fragment).addExpense(editTextExpenseName.getText().toString(), Integer.valueOf(editTextExpenseAmount.getText().toString()));
                    dialog.dismiss();
                    ExpenseDetailsFragment.mExpenseEncodedBaseString = null;
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);

            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpenseDetailsFragment.mExpenseEncodedBaseString = null;
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void addDeductBalance(Context context, String techName, int techId, int remainingBalance /*List<ExpenseTechList.ResultData> mExpenseTechList*/) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_balance);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        ArrayList<String> techNameList = new ArrayList<>();

        TextView textViewCurrentBal = dialog.findViewById(R.id.textView_current_amount);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        Button buttonAddDeductBal = dialog.findViewById(R.id.button_add_deduct);
        EditText editTextAmount = dialog.findViewById(R.id.edittext_amount);
        EditText edittext_description = dialog.findViewById(R.id.edittext_description);
        Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        TextView hAddDeductBalVideo = dialog.findViewById(R.id.textView_how_to_add_dec_bal);

       /* for (ExpenseTechList.ResultData resultData : mExpenseTechList) {
            techNameList.add(resultData.getName());
        }*/

        hAddDeductBalVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(context.getApplicationContext(), YouTubeAddDecBalActivity.class);
                context.startActivity(send);
            }
        });

        /*techNameList.add(context.getString(R.string.select_technicians));*/
        try {
            techNameList.add(techName);
            mUserId = techId;
            textViewCurrentBal.setText("" + remainingBalance);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context.getApplicationContext(), android.R.layout.simple_spinner_item, techNameList);
            technicians.setAdapter(arrayAdapter);
        } catch (Exception ex) {
            ex.getMessage();
        }

       /* ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, techNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };
        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());*/

        //FWLogger.logInfo("TAG", "COUNT : " + techNameList.size() + "  mExpenseTechList : " + mExpenseTechList.size());
       /* new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                technicians.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        FWLogger.logInfo("TAG", "onItemSelected called : " + position);
                        if (!arrayAdapterTech.getItem(position).equalsIgnoreCase(context.getString(R.string.select_technicians))) {
                            ExpenseTechList.ResultData resultData = mExpenseTechList.get(position);
                            mRemainingBalance = resultData.getRemainingBalance();
                            textViewCurrentBal.setText("" + mRemainingBalance);
                            mUserId = resultData.getUserId();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });*/

        buttonAddDeductBal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (technicians.getSelectedItem() == context.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(context.getString(R.string.please_select_option));
                } else if (isEmpty(editTextAmount)) {
                    editTextAmount.setError(context.getString(R.string.please_enter_amount));
                } else if ((Integer.valueOf(editTextAmount.getText().toString()) == 0)) {
                    editTextAmount.setError(context.getString(R.string.please_enter_valid_amount));
                } else {
                    mBalType = "ADD";
                    FWLogger.logInfo("TAG", "mBalType : " + mBalType);

                    try {
                        if (edittext_description.getText().toString().isEmpty()) {
                            addDeduct_Description = "";
                        } else {
                            addDeduct_Description = edittext_description.getText().toString();
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    //NOTE: Log GA event
                    Bundle bundle = new Bundle();
                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(context).getUserId());
                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(context).getUserGroup());
                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                    if (mBalType.equals("ADD")) {
                        bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CLICKED);
                        bundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, Integer.valueOf(editTextAmount.getText().toString())); //To equalize event column
                        FirebaseAnalytics.getInstance(context).logEvent(FirebaseGoogleAnalytics.Event.ADD_BAL, bundle);
                        ((HomeActivityNew) context).addCredit(Integer.valueOf(editTextAmount.getText().toString()), mUserId, addDeduct_Description);
                        dialog.dismiss();
                        ((HomeActivityNew) context).loadFragment(new PassbookExpenditureTabHost(), R.id.navigation_home);
                    } else {
                        if (mRemainingBalance <= 0) {
                            Toast.makeText(context, "Current amount is 0, balance cannot be deducted.", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();
                        }
                    }
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
