package com.corefield.fieldweb.FieldWeb.Passbook;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.YearlyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.YearlyPassbookAsyncTask;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;

/**
 * Fragment Controller for Yearly Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show passbook of Yearly bar graph
 */
public class YearlyPassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = YearlyPassbookFragment.class.getSimpleName();
    public List<YearlyPassbook.GetYearlyData> mYearlyPassbook = null;
    private View mRootView;
    private BarChart mBarChart;
    private YearlyPassbookAsyncTask mYearlyPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.passbook_graph_fragment, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {
        mBarChart = mRootView.findViewById(R.id.barchart);
        mBarChart.setDescription("");
        mBarChart.setMaxVisibleValueCount(50);
        mBarChart.setPinchZoom(false);
        mBarChart.animateX(2000);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisLeft().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawGridLines(false);
        mYearlyPassbookAsyncTask = new YearlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mYearlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
    }


    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(YearlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            YearlyPassbook yearlyPassbook = gson.fromJson(urlConnectionResponse.resultData, YearlyPassbook.class);
            mYearlyPassbook = yearlyPassbook.getResultData().getGetYearlyData();
            FWLogger.logInfo(TAG, "Check = " + yearlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            yearlyPassbook.getResultData().getTotalEarned();
            yearlyPassbook.getResultData().getTotalCredit();
            yearlyPassbook.getResultData().getTotalExpenses();
            yearlyPassbook.getResultData().getTotalDeduction();
            yearlyPassbook.getResultData().getTotalOpening();
            /////////////////////////////////////////BAR CHART ////////////////////////////////////
            ArrayList<BarEntry> estimated = new ArrayList<>();
            ArrayList<BarEntry> earned = new ArrayList<>();
            ArrayList<BarEntry> expense = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < mYearlyPassbook.size(); i++) {
                estimated.add(new BarEntry(mYearlyPassbook.get(i).getEstimate(), i));
                earned.add(new BarEntry(mYearlyPassbook.get(i).getEarned(), i));
                expense.add(new BarEntry(mYearlyPassbook.get(i).getExpenses(), i));
                labels.add(String.valueOf(mYearlyPassbook.get(i).getYear()));
            }

            XAxis x = mBarChart.getXAxis();
            x.setSpaceBetweenLabels(0);
            x.setXOffset(.5f);
            x.setYOffset(.5f);

            BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
            expenseBar.setColors(new int[]{R.color.orange}, getContext());

            BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
            estimatedBar.setColors(new int[]{R.color.green}, getContext());

            BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
            earnedBar.setColors(new int[]{R.color.blue}, getContext());

            ArrayList<BarDataSet> dataSets = new ArrayList<>();
            dataSets.add(expenseBar);
            dataSets.add(estimatedBar);
            dataSets.add(earnedBar);

            BarData data = new BarData(labels, dataSets);
            mBarChart.setData(data);
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mYearlyPassbookAsyncTask != null) {
            mYearlyPassbookAsyncTask.cancel(true);
        }
        super.onDestroyView();
    }
}



