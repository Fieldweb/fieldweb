package com.corefield.fieldweb.FieldWeb.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.corefield.fieldweb.DTO.TravelPathHistory.TravelledPathHistory;

import org.joda.time.DateTime;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    public Context context;
    private static final int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "FW_DB";
    //
    public static String TABLE_TRAVELLED_PATH_DATA = "TtravelledData";

    String CREATE_TRAVEL_PATH_DATA = "CREATE TABLE IF NOT EXISTS \"TtravelledData\" (\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"Id\" TEXT, \"Latitude\" TEXT, \"Longitude\" TEXT, \"LocAddress\" TEXT, \"TaskId\" TEXT, \"UserID\" TEXT, \"CreatedBy\" TEXT, \"CreatedDate\" TEXT, \"UpdatedBy\" TEXT, \"UpdatedDate\" TEXT)";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TRAVEL_PATH_DATA);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void copyDataBase(Context context) throws IOException {
        String inputFileName;
        FileInputStream myInput = null;
        OutputStream myOutput = null;
        byte[] buffer;
        try {
            // Open your local db as the input stream
            inputFileName = context.getDatabasePath(DATABASE_NAME.toString()).getAbsolutePath();
            myInput = new FileInputStream(inputFileName);

            // Path to the just created empty db
            String outFileName;
            outFileName = "sdcard/" + DATABASE_NAME;

            // Open the empty db as the output stream
            myOutput = new FileOutputStream(outFileName);

            // Dump all the data to file line by line
            buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
        } catch (Exception e) {
            e.toString();
        } finally {
            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
            buffer = null;
        }
    }

    public long insertTravelledPathData(TravelledPathHistory.ResultData obj) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Id", obj.getId());
        values.put("Latitude", obj.getLatitude());
        values.put("Longitude", obj.getLongitude());
        values.put("LocAddress", obj.getLocAddress());
        values.put("TaskId", obj.getTaskId());
        values.put("UserID", obj.getUserID());
        values.put("CreatedBy", obj.getCreatedBy());
        values.put("CreatedDate", obj.getCreatedDate());
        values.put("UpdatedBy", obj.getUpdatedBy());
        values.put("UpdatedDate", obj.getUpdatedDate());

        long id = db.insert(TABLE_TRAVELLED_PATH_DATA, null, values);
        db.close();
        return id;
    }

    public ArrayList<TravelledPathHistory.ResultData> getTravelledPathData() {
        ArrayList<TravelledPathHistory.ResultData> list = new ArrayList<TravelledPathHistory.ResultData>();
        try {
            SQLiteDatabase db = getWritableDatabase();
            String query = "";
            query = "select * from " + TABLE_TRAVELLED_PATH_DATA;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        TravelledPathHistory.ResultData obj = new TravelledPathHistory.ResultData();

                        obj.setId(cursor.getInt(cursor.getColumnIndex("Id")));
                        obj.setLatitude(cursor.getString(cursor.getColumnIndex("Latitude")));
                        obj.setLongitude(cursor.getString(cursor.getColumnIndex("Longitude")));
                        obj.setLocAddress(cursor.getString(cursor.getColumnIndex("LocAddress")));
                        obj.setTaskId(cursor.getInt(cursor.getColumnIndex("TaskId")));
                        obj.setUserID(cursor.getInt(cursor.getColumnIndex("UserID")));
                        obj.setCreatedBy(cursor.getInt(cursor.getColumnIndex("CreatedBy")));
                        obj.setCreatedDate(cursor.getString(cursor.getColumnIndex("CreatedDate")));
                        obj.setUpdatedBy(cursor.getInt(cursor.getColumnIndex("UpdatedBy")));
                        obj.setUpdatedDate(cursor.getString(cursor.getColumnIndex("UpdatedDate")));
                        list.add(obj);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void deleteTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from " + tableName);
        db.close();
    }


}
