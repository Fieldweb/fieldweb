package com.corefield.fieldweb.FieldWeb.Task;

import static android.content.Context.ALARM_SERVICE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.ItemListAdapterNew;
import com.corefield.fieldweb.Adapter.MultipleItemListAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Expenditure.AddExpense;
import com.corefield.fieldweb.DTO.Task.AddPhotoBeforeTask;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.TeleCMI.Connect;
import com.corefield.fieldweb.DTO.TeleCMI.Pcmo;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddPhotoBeforeTaskAsyncTask;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.LocationService.GoogleLocAlarmReceiver;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Services.AlarmReceiver;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.RoundedTransformation;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Countdown timer for Technician
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show Countdown timer for  Technician while staring the task
 */
@SuppressLint("ValidFragment")
public class CountdownTechFragmentNew extends Fragment implements View.OnClickListener, Chronometer.OnChronometerTickListener, BaseActivity.RequestPermissionsUpdateListener, OnTaskCompleteListener {
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    private View mRootView;
    private String mTaskStatus = "", mTaskType = "";
    private int mTaskState = 0;
    private int mTaskStatusId = 0;
    private Chronometer mChronometer;
    private Button mButtonStartTask, mButtonOnHold;
    private TextView mTextViewTaskName, mTextViewTaskDesc, mTextViewTaskType, mTextViewItem, mTextViewClickHere, mTextViewTakePic, mTextViewNote;
    private ImageView mImageViewBeforeTaskPhoto, mImageViewBeforeTaskPhoto2, mImageViewBeforeTaskPhoto3, mImageViewEditTaskPhoto, playAudio, imageViewCall;
    private TasksList.ResultData mResultData;
    private long mStartDate = 0, mEndDate = 0, mElapsedTime = 0;

    private String mWhichImage = "";
    protected String mCustomerContNo = ""; // Changes done by Manav
    private Uri mImageUri;
    private int mImageNo;
    private String mCapturedImagePath = "";
    public static String mBeforeTaskImage = "BeforeTask", mBeforeTaskImage2 = "BeforeTask2", mBeforeTaskImage3 = "BeforeTask3", mEncodedBaseString = null;
    private String mEncodedBaseString4 = null, mEncodedBaseString5 = null;
    public static String mEncodedBaseString1 = null, mEncodedBaseString2 = null, mEncodedBaseString3 = null;
    private CountdownTechFragmentNew.ImageUpdateListener mImageUpdateListener;
    private LinearLayout mLinearBeforeTask, mLinearClickHere;
    private AddPhotoBeforeTaskAsyncTask mAddPhotoBeforeTask;
    private String mPhotoName = "", mNote = "";
    private String mImageBeforeTaskURL = "", mImageBeforeTaskURL2 = "", mImageBeforeTaskURL3 = "";
    private TasksList.PreDeviceInfoDto preDeviceInfoDto;
    private LinearLayout mLinearNote;
    private String mAudioFilePath = "";
    Context mContext;
    RecyclerView mrecyclerViewMultiItemlist;
    LinearLayoutManager mLayoutManager;
    MultipleItemListAdapter multipleItemListAdapter;

    public CountdownTechFragmentNew() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.countdown_tech_fragment_new, container, false);

        Bundle getBundle = this.getArguments();
        if (getBundle != null)
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

        ((HomeActivityNew) getActivity()).hideHeader();

        mContext = getActivity();
        inIt();
        return mRootView;
    }

    private void inIt() {
        mTextViewTaskName = mRootView.findViewById(R.id.textView_task_name);
        mTextViewTaskType = mRootView.findViewById(R.id.textview_task_type);
        mTextViewTaskDesc = mRootView.findViewById(R.id.textView_description);
        mTextViewItem = mRootView.findViewById(R.id.textView_item);
        mTextViewClickHere = mRootView.findViewById(R.id.textView_click_here);
        mTextViewTakePic = mRootView.findViewById(R.id.textView_take_pic);
        mTextViewNote = mRootView.findViewById(R.id.textView_note);
        mButtonStartTask = mRootView.findViewById(R.id.button_start_task);
        mButtonOnHold = mRootView.findViewById(R.id.button_start_onhold);
        mChronometer = mRootView.findViewById(R.id.chronometer);
        mImageViewBeforeTaskPhoto = mRootView.findViewById(R.id.imageView_before_task_image);
        mImageViewBeforeTaskPhoto2 = mRootView.findViewById(R.id.imageView_before_task_image2);
        mImageViewBeforeTaskPhoto3 = mRootView.findViewById(R.id.imageView_before_task_image3);
        mImageViewEditTaskPhoto = mRootView.findViewById(R.id.imageView_edit);
        mLinearBeforeTask = mRootView.findViewById(R.id.linear_before_task_photo);
        mLinearClickHere = mRootView.findViewById(R.id.linear_click_here);
        mLinearNote = mRootView.findViewById(R.id.linear_note);
        mTaskState = mResultData.getTaskState();
        mStartDate = mResultData.getStartDate();
        mEndDate = mResultData.getEndDate();
        playAudio = (ImageView) mRootView.findViewById(R.id.playAudio);
        mrecyclerViewMultiItemlist = (RecyclerView) mRootView.findViewById(R.id.recyclerView_MultiItemlist);

        try {
            if (SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("true") || SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                imageViewCall = mRootView.findViewById(R.id.imageView_call);
                imageViewCall.setOnClickListener(this);
                imageViewCall.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        //TODO update TASKSTATE = 0 then Start The Task
        //TODO update TASKSTATE = 1 then Continue To Task (started but not ended)
        if (mTaskState == TaskState.STARTED_NOT_ENDED) {
            mButtonStartTask.setText(R.string.continue_to_task);
            mImageViewEditTaskPhoto.setVisibility(View.GONE);
            mLinearClickHere.setVisibility(View.GONE);
//            Toast.makeText(getActivity(), "You can't edit field photo", Toast.LENGTH_SHORT).show();
        } else {
            mButtonStartTask.setText(getResources().getString(R.string.start_the_task));
//            capturePhotoBeforeTask();
        }

        mTextViewTaskName.setText(mResultData.getName());
        mTextViewTaskType.setText(mResultData.getTaskType());
        if (mResultData.getTaskType() != null && !mResultData.getTaskType().isEmpty()) {
            switch (mResultData.getTaskType()) {
                case "Urgent":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_urgent));
                    break;
                case "Today":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.orange));
                    break;
                case "Schedule":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_schedule));
                    break;
                default:
                    break;
            }
        }

        // ADD RECYCLERVIEW FOR MULTIITEM
        try {
            List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
            multipleItemAssignedArrayList = mResultData.getMultipleItemAssigned();
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                mrecyclerViewMultiItemlist.setLayoutManager(mLayoutManager);
                multipleItemListAdapter = new MultipleItemListAdapter(getActivity(), multipleItemAssignedArrayList);
                mrecyclerViewMultiItemlist.setAdapter(multipleItemListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        if ((mResultData.getItemName() == null || mResultData.getItemName().isEmpty() || mResultData.getItemName().equalsIgnoreCase("")))
            mTextViewItem.setText(getResources().getString(R.string.na));
        else mTextViewItem.setText(mResultData.getItemName());

        if ((mResultData.getDescription() == null || mResultData.getDescription().isEmpty() || mResultData.getDescription().equalsIgnoreCase("")))
            mTextViewTaskDesc.setText(getResources().getString(R.string.na));
        else mTextViewTaskDesc.setText(mResultData.getDescription());

        // Changes Done by Manav
        if (mResultData.getContactNo() != null && !mResultData.getContactNo().isEmpty())
            mCustomerContNo = mResultData.getContactNo();
        else mCustomerContNo = getString(R.string.na);
        // End of changes done by Manav

        if (mResultData.getPreDeviceInfoDto() != null) {
            if (mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath().isEmpty()) {
                mLinearBeforeTask.setVisibility(View.VISIBLE);
                mLinearClickHere.setVisibility(View.GONE);
                mTextViewTakePic.setVisibility(View.GONE);
                mImageBeforeTaskURL = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath();
                setPhotoBeforeTask(mImageBeforeTaskURL);
            }
            if (mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath1() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath1().isEmpty()) {
                mLinearBeforeTask.setVisibility(View.VISIBLE);
                mLinearClickHere.setVisibility(View.GONE);
                mTextViewTakePic.setVisibility(View.GONE);
                mImageBeforeTaskURL2 = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath1();
                setPhotoBeforeTask2(mImageBeforeTaskURL2);
            }
            if (mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath2() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath2().isEmpty()) {
                mLinearBeforeTask.setVisibility(View.VISIBLE);
                mLinearClickHere.setVisibility(View.GONE);
                mTextViewTakePic.setVisibility(View.GONE);
                mImageBeforeTaskURL3 = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath2();
                setPhotoBeforeTask3(mImageBeforeTaskURL3);
            }


            if (mResultData.getPreDeviceInfoDto().getDeviceInfoImageName() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoImageName().isEmpty())
                mPhotoName = mResultData.getPreDeviceInfoDto().getDeviceInfoImageName();

            if (mResultData.getPreDeviceInfoDto().getDeviceInfoNotes() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoNotes().isEmpty()) {
                mNote = mResultData.getPreDeviceInfoDto().getDeviceInfoNotes();
                mLinearNote.setVisibility(View.VISIBLE);
                mTextViewNote.setText(mResultData.getPreDeviceInfoDto().getDeviceInfoNotes());
            } else {
                mLinearNote.setVisibility(View.GONE);
            }
        } else if (mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.start_the_task)) && mTaskState == TaskState.NOT_STARTED) {
            //capturePhotoBeforeTask();
        }
        /*int distanceInKm = 0;

        if(mResultData.getTechLatitude() == null && mResultData.getTechLongitude() == null) {
            if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                double latitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLatitude();
                double longitude = SharedPrefManager.getInstance(getActivity()).getUserLocation().getLongitude();
                distanceInKm = MapUtils.calculateDistanceInKilometer(latitude, longitude,
                        Double.parseDouble(mResultData.getLatitude()), Double.parseDouble(mResultData.getLongitude()));
            }
        } else {
            distanceInKm = MapUtils.calculateDistanceInKilometer(Double.parseDouble(mResultData.getTechLatitude()), Double.parseDouble(mResultData.getTechLongitude()),
                    Double.parseDouble(mResultData.getLatitude()), Double.parseDouble(mResultData.getLongitude()));
        }
        FWLogger.logInfo(TAG, "Distance In KM : " + distanceInKm);*/

        mTaskStatus = mResultData.getTaskStatus();
        mTaskStatusId = mResultData.getTaskStatusId();
        mTaskState = mResultData.getTaskState();
        mAudioFilePath = mResultData.getAudioFilePath();

        mButtonStartTask.setOnClickListener(this);
        mButtonOnHold.setOnClickListener(this);
        mTextViewClickHere.setOnClickListener(this);
        mImageViewBeforeTaskPhoto.setOnClickListener(this);
        mImageViewBeforeTaskPhoto2.setOnClickListener(this);
        mImageViewBeforeTaskPhoto3.setOnClickListener(this);

        playAudio.setOnClickListener(this);

        //Timer
        initializeTimer();
    }

    private void initializeTimer() {
        mChronometer.setOnChronometerTickListener(this);
    }

    public void startTimer() {
        //Timer
        //mChronometer.setBase(SystemClock.elapsedRealtime()- (25*60*60000 +12*60000+45*1000+ 10)); This is how chronometer cal the initial time and set it to base time
        mChronometer.setBase(SystemClock.elapsedRealtime() - mElapsedTime);
        mChronometer.start();
    }

    protected void stopTimer() {
        //Timer
        //mElapsedTime = SystemClock.elapsedRealtime() - mChronometer.getBase();//New time to send to API or to store into shared pref
        //FWLogger.logInfo(logging,"Elapse time = "+mElapsedTime);
        mChronometer.stop();
    }

    @Override
    public void onChronometerTick(Chronometer chronometer) {

    }

    public void capturePhotoBeforeTask() {
        mEncodedBaseString = null;
        checkCameraPermission();
        TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
        taskDialogNew.captureBeforeTaskPhotoDialog(getActivity(), mResultData, this);
    }

    public void updatestatusfromTaskDialog() {
        ((HomeActivityNew) getActivity()).mTaskListData = mResultData;
        //TODO : Update API with TASKSTATE 1 Task started but not ended then open TechTaskTrackingFragment
        ((HomeActivityNew) getActivity()).updateTaskStatus(TaskStatus.ON_GOING, TaskState.STARTED_NOT_ENDED, "");
    }

    public void addPhotoBeforeTask(String note) {
        checkCameraPermission();
        if (note != null) mNote = note;
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        mPhotoName = timeStamp + "_" + mWhichImage + ".jpg";
        FWLogger.logInfo(TAG, "Photo Name : " + mPhotoName + "    note : " + note);

        /*mAddPhotoBeforeTask = new AddPhotoBeforeTaskAsyncTask(getActivity(), BaseAsyncTask.Priority.LOW, this);
        mAddPhotoBeforeTask.execute(mPhotoName, note, mResultData.getUserId(), mEncodedBaseString, mResultData.getId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                Call<AddPhotoBeforeTask> call = RetrofitClient.getInstance(getContext()).getMyApi().addPhotoBeforeTask(mPhotoName, note, mResultData.getUserId(), mEncodedBaseString, mEncodedBaseString4, mEncodedBaseString5, mResultData.getId(), "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<AddPhotoBeforeTask>() {
                    @Override
                    public void onResponse(Call<AddPhotoBeforeTask> call, Response<AddPhotoBeforeTask> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                AddPhotoBeforeTask addPhotoBeforeTask = response.body();
                                if (addPhotoBeforeTask != null) {
                                    if (addPhotoBeforeTask.getCode().equalsIgnoreCase("200") && (addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_saved_successfully, getActivity())) || addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_updated_successfully, getActivity())))) {
                                        Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                                        FWLogger.logInfo(TAG, addPhotoBeforeTask.getMessage());

                                        mLinearBeforeTask.setVisibility(View.VISIBLE);
                                        mLinearClickHere.setVisibility(View.GONE);
                                        mTextViewTakePic.setVisibility(View.GONE);
                                        if (mNote != null && !mNote.isEmpty()) {
                                            mLinearNote.setVisibility(View.VISIBLE);
                                            mTextViewNote.setText(mNote);
                                        } else {
                                            mLinearNote.setVisibility(View.GONE);
                                        }

                                        if (addPhotoBeforeTask.getResultData() != null) {
                                            mImageBeforeTaskURL = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath();
                                            setPhotoBeforeTask(mImageBeforeTaskURL);
                                            mImageBeforeTaskURL2 = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath1();
                                            setPhotoBeforeTask2(mImageBeforeTaskURL2);
                                            mImageBeforeTaskURL3 = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath2();
                                            setPhotoBeforeTask3(mImageBeforeTaskURL3);
                                        }

                                    } else {
                                        mLinearBeforeTask.setVisibility(View.GONE);
                                        mLinearClickHere.setVisibility(View.VISIBLE);
                                        mTextViewTakePic.setVisibility(View.VISIBLE);
                                        Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddPhotoBeforeTask> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in AddPreDeviceInfoDetails API:");
                    }
                });
            } else {
                CommonFunction.hideProgressDialog(getActivity());
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(getActivity());
            FWLogger.logInfo(TAG, "Exception in AddPreDeviceInfoDetails API:");
            ex.getMessage();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_click_here:
                capturePhotoBeforeTask();
                break;

            case R.id.imageView_before_task_image:
                if (mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.end_task)) || mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.continue_to_task))) {
                    mImageViewEditTaskPhoto.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "You can not edit field photo", Toast.LENGTH_SHORT).show();
                } else {
                    mImageViewEditTaskPhoto.setVisibility(View.VISIBLE);
                    capturePhotoBeforeTask();
                }
                break;

            case R.id.imageView_before_task_image2:
                if (mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.end_task)) || mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.continue_to_task))) {
                    mImageViewEditTaskPhoto.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "You can not edit field photo", Toast.LENGTH_SHORT).show();
                } else {
                    mImageViewEditTaskPhoto.setVisibility(View.VISIBLE);
                    capturePhotoBeforeTask();
                }
                break;
            case R.id.imageView_before_task_image3:
                if (mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.end_task)) || mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.continue_to_task))) {
                    mImageViewEditTaskPhoto.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "You can not edit field photo", Toast.LENGTH_SHORT).show();
                } else {
                    mImageViewEditTaskPhoto.setVisibility(View.VISIBLE);
                    capturePhotoBeforeTask();
                }
                break;

            case R.id.button_start_task:
                if (mButtonStartTask.getText().toString().equalsIgnoreCase(getResources().getString(R.string.end_task))) {
                    stopTimer();
                    setBeforeTaskData();

                    ((HomeActivityNew) getActivity()).mTaskListData = mResultData;
                    ((HomeActivityNew) getActivity()).updateTaskStatus(TaskStatus.ON_GOING, TaskState.ENDED_NO_PAYMENT, "");
                } else {
                    if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING && mTaskState == TaskState.STARTED_NOT_ENDED) {
                        //Already running task so startTime(elapsedTime) with elapsedTime time (currentDateTime - startDateTime)
                        mElapsedTime = System.currentTimeMillis() - mStartDate;
                        startTimer();
                        setBeforeTaskData();
                        mButtonStartTask.setText(getResources().getString(R.string.end_task));
                    } else if (mTaskStatus.equalsIgnoreCase(Constant.TaskCategories.Ongoing.name()) && mTaskStatusId == TaskStatus.ON_GOING && mTaskState == TaskState.NOT_STARTED) {
                        //API call to update task status (Start the task)
                        //startTime(0) with 0 time
                        mElapsedTime = 0;
                        /*((HomeActivityNew) getActivity()).mTaskListData = mResultData;
                        //TODO : Update API with TASKSTATE 1 Task started but not ended then open TechTaskTrackingFragment
                        ((HomeActivityNew) getActivity()).updateTaskStatus(TaskStatus.ON_GOING, TaskState.STARTED_NOT_ENDED, "");
                        setBeforeTaskData();
                        startTimer();*/
                        setBeforeTaskData();
                        TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                        taskDialogNew.captureBeforeTaskPhotoDialog(getActivity(), mResultData, this);
                        //startTimer(); // To Change
                        mButtonStartTask.setText(getResources().getString(R.string.end_task));
                    }
                    mLinearClickHere.setVisibility(View.GONE);
                    mImageViewEditTaskPhoto.setVisibility(View.GONE);
                }
                break;

            case R.id.button_start_onhold:

                TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                taskDialogNew.onHoldDialog(getActivity(), this, mResultData);

                break;

            case R.id.playAudio:
                FWLogger.logInfo(TAG, "Audio File Path : " + mAudioFilePath);
                if (mAudioFilePath != null && !mAudioFilePath.isEmpty()) {
                    AudioPermission();
                    playRecordedFile(mAudioFilePath);
                } else {
                    Toast.makeText(getActivity(), "File not found.", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.imageView_call:
                if (mCustomerContNo != null) {
                    try {
                        if (SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("true")) {
                            if (mCustomerContNo != null && !mCustomerContNo.isEmpty() && !mCustomerContNo.equalsIgnoreCase(getString(R.string.na)))
                                TelCMI(getActivity(), mCustomerContNo);
                            else {
                                Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }

                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;
            // End of Changes done by Manav

            default:
                break;
        }
    }

    public void setBeforeTaskData() {
//        if (mResultData.getPreDeviceInfoDto() == null) {
        preDeviceInfoDto = new TasksList.PreDeviceInfoDto();
//        }
        FWLogger.logInfo(TAG, "mImageBeforeTaskURL : " + mImageBeforeTaskURL);

        if (mImageBeforeTaskURL != null && !mImageBeforeTaskURL.isEmpty())
            preDeviceInfoDto.setDeviceInfoImagePath(mImageBeforeTaskURL);
        if (mImageBeforeTaskURL2 != null && !mImageBeforeTaskURL2.isEmpty())
            preDeviceInfoDto.setDeviceInfoImagePath1(mImageBeforeTaskURL2);

        if (mImageBeforeTaskURL3 != null && !mImageBeforeTaskURL3.isEmpty())
            preDeviceInfoDto.setDeviceInfoImagePath2(mImageBeforeTaskURL3);
        if (mNote != null && !mNote.isEmpty()) preDeviceInfoDto.setDeviceInfoNotes(mNote);
        if (mPhotoName != null && !mPhotoName.isEmpty())
            preDeviceInfoDto.setDeviceInfoImageName(mPhotoName);
        preDeviceInfoDto.setTaskId(mResultData.getId());
        preDeviceInfoDto.setCreatedBy(SharedPrefManager.getInstance(getContext()).getUserId());

        mResultData.setPreDeviceInfoDto(preDeviceInfoDto);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(AddPhotoBeforeTask.class.getSimpleName())) {
            Gson gson = new Gson();
            AddPhotoBeforeTask addPhotoBeforeTask = gson.fromJson(urlConnectionResponse.resultData, AddPhotoBeforeTask.class);
            if (addPhotoBeforeTask != null) {
                if (addPhotoBeforeTask.getCode().equalsIgnoreCase("200") && (addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_saved_successfully, getActivity())) || addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_updated_successfully, getActivity())))) {
                    Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, addPhotoBeforeTask.getMessage());

                    mLinearBeforeTask.setVisibility(View.VISIBLE);
                    mLinearClickHere.setVisibility(View.GONE);
                    mTextViewTakePic.setVisibility(View.GONE);
                    if (mNote != null && !mNote.isEmpty()) {
                        mLinearNote.setVisibility(View.VISIBLE);
                        mTextViewNote.setText(mNote);
                    } else {
                        mLinearNote.setVisibility(View.GONE);
                    }

                    if (addPhotoBeforeTask.getResultData() != null) {
                        mImageBeforeTaskURL = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath();
                        setPhotoBeforeTask(mImageBeforeTaskURL);

                        mImageBeforeTaskURL2 = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath1();
                        setPhotoBeforeTask2(mImageBeforeTaskURL2);
                        mImageBeforeTaskURL3 = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath2();
                        setPhotoBeforeTask3(mImageBeforeTaskURL3);
                    }

                } else {
                    mLinearBeforeTask.setVisibility(View.GONE);
                    mLinearClickHere.setVisibility(View.VISIBLE);
                    mTextViewTakePic.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void setPhotoBeforeTask(String imageBeforeTaskURL) {
        Picasso.get().load(imageBeforeTaskURL).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit()
//                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewBeforeTaskPhoto, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mImageViewBeforeTaskPhoto.setImageResource(R.drawable.field_location_upload_photo);
                    }
                });
    }

    private void setPhotoBeforeTask2(String imageBeforeTaskURL2) {
        Picasso.get().load(imageBeforeTaskURL2).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit()
//                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewBeforeTaskPhoto2, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        mImageViewBeforeTaskPhoto2.setImageResource(R.drawable.field_location_upload_photo);
                    }
                });
    }

    private void setPhotoBeforeTask3(String imageBeforeTaskURL3) {
        Picasso.get().load(imageBeforeTaskURL3).placeholder(R.drawable.field_location_upload_photo).error(R.drawable.field_location_upload_photo).transform(new RoundedTransformation(20, 4)).fit()
//                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewBeforeTaskPhoto3, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        mImageViewBeforeTaskPhoto.setImageResource(R.drawable.field_location_upload_photo);
                    }
                });
    }


    public void pickImage(String whichImage, int imageNo, CountdownTechFragmentNew.ImageUpdateListener imageUpdateListener) {
        checkCameraPermission();
        mWhichImage = whichImage;
        mImageNo = imageNo;
        mImageUpdateListener = imageUpdateListener;
        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        builder.setTitle(getResources().getString(R.string.select_option));
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(getActivity().getApplicationContext())) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.camera_not_supported), Toast.LENGTH_LONG).show();
                    }
                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(CountdownTechFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {

                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(CountdownTechFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            //photo = new File(Environment.getExternalStorageDirectory(), getFileName() + ".jpg");
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            photo = File.createTempFile(getFileName(), ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCapturedImagePath = photo.getAbsolutePath();
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mImageUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private String getFileName() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        String imageFileName = timeStamp + "_" + mWhichImage + "_" + mImageNo + "_";
        return imageFileName;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            FWLogger.logInfo(TAG, "Camera");
            try {
                getActivity().getContentResolver().notifyChange(mImageUri, null);
                Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), mImageUri, mCapturedImagePath);
                //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                String filePath = CameraUtils.getFilePathFromURI(mImageUri, getContext());
                //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage)) {
                    setBeforeTaskPhoto(bitmap);
                } else if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage2)) {
                    setBeforeTaskPhoto2(bitmap);
                } else if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage3)) {
                    setBeforeTaskPhoto3(bitmap);
                } else if (mWhichImage.equalsIgnoreCase("fromOnHold_1")) {
                    setonHoldTaskPhoto_1(bitmap);
                } else if (mWhichImage.equalsIgnoreCase("fromOnHold_2")) {
                    setonHoldTaskPhoto_2(bitmap);
                } else if (mWhichImage.equalsIgnoreCase("fromOnHold_3")) {
                    setonHoldTaskPhoto_3(bitmap);
                } else {
                    mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                }
            } catch (Exception e) {
                Toast.makeText(getActivity(), getResources().getString(R.string.failed_to_load), Toast.LENGTH_SHORT).show();
                FWLogger.logInfo("Camera", e.toString());
            }
        } else if (requestCode == BaseActivity.PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                FWLogger.logInfo(TAG, "Gallery");
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), selectedImage, null);
                    //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                    String filePath = CameraUtils.getFilePathFromURI(selectedImage, getContext());
                    FWLogger.logInfo("filePath : ", filePath);
                    //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                    bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                    if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage)) {
                        setBeforeTaskPhoto(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage2)) {
                        setBeforeTaskPhoto2(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase(mBeforeTaskImage3)) {
                        setBeforeTaskPhoto3(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase("fromOnHold_1")) {
                        setonHoldTaskPhoto_1(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase("fromOnHold_2")) {
                        setonHoldTaskPhoto_2(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase("fromOnHold_3")) {
                        setonHoldTaskPhoto_3(bitmap);
                    } else {
                        mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.sorry_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setBeforeTaskPhoto(Bitmap bitmap) {
        try {
            TaskDialogNew.mImageViewBeforeTask.setBackground(null);
            mEncodedBaseString = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "BeforeTask_Base64 : " + mEncodedBaseString);
            TaskDialogNew.mImageViewBeforeTask.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void setBeforeTaskPhoto2(Bitmap bitmap) {
        try {
            TaskDialogNew.mImageViewBeforeTask2.setBackground(null);
            mEncodedBaseString4 = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "BeforeTask_Base64 : " + mEncodedBaseString4);
            TaskDialogNew.mImageViewBeforeTask2.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void setBeforeTaskPhoto3(Bitmap bitmap) {
        try {
            TaskDialogNew.mImageViewBeforeTask2.setBackground(null);
            mEncodedBaseString5 = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "BeforeTask_Base64 : " + mEncodedBaseString5);
            TaskDialogNew.mImageViewBeforeTask3.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    public void setonHoldTaskPhoto_1(Bitmap bitmap) {
        try {
            TaskDialogNew.photo1.setBackground(null);
            mEncodedBaseString1 = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "OnHoldTask_Base64 : " + mEncodedBaseString1);
            TaskDialogNew.photo1.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void setonHoldTaskPhoto_2(Bitmap bitmap) {
        try {
            TaskDialogNew.photo2.setBackground(null);
            mEncodedBaseString2 = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "OnHoldTask_Base64 : " + mEncodedBaseString2);
            TaskDialogNew.photo2.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void setonHoldTaskPhoto_3(Bitmap bitmap) {
        try {
            TaskDialogNew.photo3.setBackground(null);
            mEncodedBaseString3 = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "OnHoldTask_Base64 : " + mEncodedBaseString3);
            TaskDialogNew.photo3.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                captureImage();
            } else if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
            }
        }
    }

    public interface ImageUpdateListener {
        void onImageCaptured(Bitmap bitmap, String whichImage, int imageNo);
    }

    private void playRecordedFile(String mAudioFilePath) {
        try {
            //if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
            if (mAudioFilePath != null && !mAudioFilePath.isEmpty() && mAudioFilePath.contains(".wav")) {
                Uri a = Uri.parse(mAudioFilePath);
                Intent viewMediaIntent = new Intent();
                viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
                viewMediaIntent.setDataAndType(a, "audio/*");
//                    viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                viewMediaIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(viewMediaIntent);
            } else {
                Toast.makeText(getActivity(), "Audio File not found!!", Toast.LENGTH_SHORT).show();
            }
           /* } else {
                ((HomeActivityNew) getActivity()).requestToEnableAllPermission(CountdownTechFragmentNew.this, BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
            }*/

            /*if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                File file = new File(mAudioFilePath);
                if (file.exists()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Uri contentUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider",
                                file);
                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setDataAndType(contentUri, "audio/*");
                        getActivity().startActivity(intent);
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file), "audio");
                        getActivity().startActivity(intent);
                    }
                } else {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                    builder.setTitle("");
                    builder.setMessage("File does not exist");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.create().show();
                }
            } else {
                ((HomeActivityNew) getActivity()).requestToEnableAllPermission(((HomeActivityNew) getActivity()), BaseActivity.PERMISSION_REQUEST_REC_AUDIO);
            }*/

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public boolean AudioPermission() {
        boolean audioFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkMicroPhone = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
                if (checkMicroPhone != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    audioFlag = true;
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return audioFlag;
    }


    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }

    private void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918035234087"));
            teleCMICall.setTo(Long.valueOf("91" + techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("918035234087"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf("91" + custContactnum));
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

/*
    private void TelCMI(Activity activity, String custContactnum) {
        try {
            String techMobile = SharedPrefManager.getInstance(activity).getMOBILE_OR_EMAIL();
            List<TeleCMICall> teleCMICallArrayList = new ArrayList<>();
            ArrayList<Pcmo> pcmoArrayList = new ArrayList<>();
            ArrayList<Connect> connectArrayList = new ArrayList<>();

            TeleCMICall teleCMICall = new TeleCMICall();
            teleCMICall.setAppid(2222293);
            teleCMICall.setSecret("b136e71c-fc27-4d89-ae35-122399eef784");
            teleCMICall.setFrom(Long.valueOf("918037222880"));
            teleCMICall.setTo(Long.valueOf(techMobile));// TECH NUMBER
            //
            Pcmo pcmo1 = new Pcmo();
            pcmo1.setAction("record");
            //
            Pcmo pcmo = new Pcmo();
            pcmo.setAction("bridge");
            pcmo.setDuration(100);
            pcmo.setTimeout(20);
            pcmo.setFrom(Long.valueOf("8037222880"));
            pcmo.setLoop(2);
            teleCMICall.setPcmo(pcmoArrayList);
            //
            Connect connect = new Connect();
            connect.setType("pstn");
            connect.setNumber(Long.valueOf(custContactnum));//8691901008
            connectArrayList.add(connect);

            pcmo.setConnect(connectArrayList);
            pcmoArrayList.add(pcmo1);
            pcmoArrayList.add(pcmo);
            //
            teleCMICallArrayList.add(teleCMICall);

            ((HomeActivityNew) activity).postData(teleCMICallArrayList);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
*/


    @Override
    public void onDetach() {
        super.onDetach();
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        ((HomeActivityNew) getActivity()).showHeader();
    }
}
