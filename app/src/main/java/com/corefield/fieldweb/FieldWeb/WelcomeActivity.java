package com.corefield.fieldweb.FieldWeb;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Notification.RegisterDeviceId;
import com.corefield.fieldweb.DTO.RegisterHere.RegisterOwner;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Response;


public class WelcomeActivity extends BaseActivity implements View.OnClickListener, OnTaskCompleteListener {

    private final String TAG = WelcomeActivity.class.getSimpleName();
    private ImageView backButton;
    private EditText edtFirstName, edtLastName, edtComanyName, edt_NoofFieldWorker;
    private Button button_proceed;
    private int mTouchlessSignupId, mCountryDetailsId, totalnoOfWorkers;
    private String mEmailOrContactNo = "", mFromWhere = "", mDisplayName = "";
    //MultiSlider multiSlider;
    //Slider discreteSlider;
    private SeekBar seekBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);
        inIt();
    }


    private void inIt() {

        backButton = (ImageView) findViewById(R.id.backButton);
        edtFirstName = (EditText) findViewById(R.id.edtFirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtComanyName = (EditText) findViewById(R.id.edtComanyName);
        edt_NoofFieldWorker = (EditText) findViewById(R.id.edt_NoofFieldWorker);
        button_proceed = (Button) findViewById(R.id.button_proceed);
        // multiSlider = (MultiSlider) findViewById(R.id.range_slider);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        //discreteSlider = (Slider) findViewById(R.id.discreteSlider);

        backButton.setOnClickListener(this);
        button_proceed.setOnClickListener(this);

        try {
            Intent iVal = getIntent();
            mEmailOrContactNo = iVal.getStringExtra("EmailOrContactNo");
            mTouchlessSignupId = iVal.getIntExtra("TouchlessSignupId", 0);
            mCountryDetailsId = iVal.getIntExtra("CountryDetailsId", 0);
            // SET OTP VERIFIED FLAG
            SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(false);

            try {
                if (mDisplayName != null || mFromWhere != null) {
                    mFromWhere = iVal.getStringExtra("Google");
                    mDisplayName = iVal.getStringExtra("DisplayName");
                    String[] ival = mDisplayName.split(" ");
                    edtFirstName.setText(ival[0]);
                    edtLastName.setText(ival[1]);
                }
            } catch (Exception e) {
                e.getMessage();
            }
           /* if (mFromWhere != null) {
                mFromWhere = iVal.getStringExtra("FromWhere");
            }*/
        } catch (Exception ex) {
            ex.getMessage();
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                edt_NoofFieldWorker.setText(String.valueOf(seekBar.getProgress()));
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                edt_NoofFieldWorker.setText(String.valueOf(seekBar.getProgress()));
            }
        });


        edt_NoofFieldWorker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String value = s.toString();

                    int progress = Integer.parseInt(value);
                    if (progress >= 0 && progress <= 100) {
                        seekBar.setProgress(progress);
                        edt_NoofFieldWorker.setText(progress);
                    } else {
                        edt_NoofFieldWorker.setText("");
                    }
                } catch (
                        Exception e) {
                    e.getMessage();
                }
            }
        });



       /* implementation 'io.apptik.widget:multislider-holo:1.3'
        multiSlider.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider,
                                       MultiSlider.Thumb thumb,
                                       int thumbIndex,
                                       int value) {
                if (thumbIndex == 0) {
                    edt_NoofFieldWorker.setText(String.valueOf(value));
                } else {
                    edt_NoofFieldWorker.setText(String.valueOf(value));
                }
            }
        });*/

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.backButton:
                navigateToTouchLessSignupActivity();
                break;

            case R.id.button_proceed:
                registerTouchLessOwner();
                break;

            default:
                break;
        }
    }

    /*  private void navigateToHomeActivity() {
          Intent intent = new Intent(WelcomeActivity.this, HomeActivityNew.class);
          startActivity(intent);
          finish();
      }*/
    private void navigateToWelcomeSliderActivity() {
        Intent intent = new Intent(WelcomeActivity.this, WelcomeSliderActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTouchLessSignupActivity() {
        Intent intent = new Intent(WelcomeActivity.this, TouchLessSignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
       /* mRegisterDeviceAsyncTask = new RegisterDeviceAsyncTask(getApplicationContext(), BaseAsyncTask.Priority.LOW, this);
        mRegisterDeviceAsyncTask.execute(token);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(WelcomeActivity.this)) {
                String ToInstanceDeviceId = token;
                String UserDeviceId = "Android Device";
                int UserId = SharedPrefManager.getInstance(WelcomeActivity.this).getUserId();
                int CreatedBy = SharedPrefManager.getInstance(WelcomeActivity.this).getUserId();
                Call<RegisterDeviceId> call = RetrofitClient.getInstance(WelcomeActivity.this).getMyApi().
                        doSendRegistrationToServer(ToInstanceDeviceId, UserDeviceId, UserId, CreatedBy);
                call.enqueue(new retrofit2.Callback<RegisterDeviceId>() {
                    @Override
                    public void onResponse(Call<RegisterDeviceId> call, Response<RegisterDeviceId> response) {
                        try {
                            if (response.code() == 200) {
                                RegisterDeviceId registerDeviceId = response.body();
                                FWLogger.logInfo(TAG, "Device Registration");
                                FWLogger.logInfo(TAG, "Device Registration");
                                if (registerDeviceId != null) {
                                    if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                                        FWLogger.logInfo(TAG, "Device Registration Completed.");
                                    } else {
                                        FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                                    }
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterDeviceId> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in RegisterDevice API:");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(WelcomeActivity.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in RegisterDevice API:");
            ex.getMessage();
        }

    }

    private boolean isValidate() {

        if (edtFirstName.getText().toString().isEmpty()) {
            edtFirstName.setError(getString(R.string.please_enter_first_name));
            return false;
        }
        if (edtLastName.getText().toString().isEmpty()) {
            edtLastName.setError(getString(R.string.please_enter_last_name));
            return false;
        }
        if (edtComanyName.getText().toString().isEmpty()) {
            edtComanyName.setError(getString(R.string.company_name));
            return false;
        }
        if (edt_NoofFieldWorker.getText().toString().isEmpty()) {
            edt_NoofFieldWorker.setError(getString(R.string.please_enter_no_of_worker));
            return false;
        }
        if (isNumberGreater(edt_NoofFieldWorker)) {
            edt_NoofFieldWorker.setError(getString(R.string.no_of_worker_should_not_greater));
            return false;
        }
        return true;
    }

    private boolean isNumberGreater(EditText edtNoOfWorker) {
        try {
            //Pattern pattern = Pattern.compile("^(0?[1-9]|[1-9][0-9])$"); 0-99
            Pattern pattern = Pattern.compile("^[1-9][0-9]?$|^100$"); // 0-100
            /* Pattern pattern = Pattern.compile("^[1-9]?$|^100$");*/ // 1-100
            Matcher matcher = pattern.matcher(edtNoOfWorker.getText().toString());
            if (!edtNoOfWorker.getText().toString().isEmpty()) {
                if (matcher.matches()) {
                    return false;
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }

        return true;
    }


    private void registerTouchLessOwner() {
        try {
            if (Connectivity.isNetworkAvailableRetro(WelcomeActivity.this)) {
                if (isValidate()) {
                    String mAndroidID = CommonFunction.getUniqueDeviceID(WelcomeActivity.this);
                    RegisterOwner.ResultData register = new RegisterOwner.ResultData();
                    RegisterOwner.LgModel lgModel = new RegisterOwner.LgModel();
                    register.setTouchlessSignupId(mTouchlessSignupId);
                    register.setEmailOrContactNo(mEmailOrContactNo);
                    register.setOTP(0);
                    register.setCountryDetailsId(mCountryDetailsId);
                    register.setFirstName(edtFirstName.getText().toString());
                    register.setLastName(edtLastName.getText().toString());
                    register.setCreatedBy(0);
                    register.setCreatedDate("");
                    register.setUpdatedBy(0);
                    register.setUpdatedDate("");
                    register.setCompanyName(edtComanyName.getText().toString());
                    register.setNoOfUsers(Integer.parseInt(edt_NoofFieldWorker.getText().toString()));
                    lgModel.setAndroidID(mAndroidID);
                    register.setLgModel(lgModel);

                  /*  mTouchRegisterOwnerAsyncTask = new TouchLessRegisterOwnerAsyncTask(WelcomeActivity.this, BaseAsyncTask.Priority.LOW, this);
                    mTouchRegisterOwnerAsyncTask.execute(register);*/
                    try {
                        Call<RegisterOwner> call = RetrofitClient.getInstance(WelcomeActivity.this).getMyApi().registerOwner(register);
                        call.enqueue(new retrofit2.Callback<RegisterOwner>() {
                            @Override
                            public void onResponse(Call<RegisterOwner> call, Response<RegisterOwner> response) {
                                try {
                                    if (response.code() == 200) {
                                        RegisterOwner registerOwner = response.body();
                                        FWLogger.logInfo(TAG, "REGISTER TOUCHLESS OWNER : FIRST_NAME/LAST_NAME/NO_OF_USERS");
                                        if (registerOwner != null) {
                                            if (registerOwner.getCode().equalsIgnoreCase("200") &&
                                                    registerOwner.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.owner_created_successfully, WelcomeActivity.this))) {
                                                SharedPrefManager.getInstance(getApplicationContext()).ownerSignUp(registerOwner, CommonFunction.getUniqueDeviceID(WelcomeActivity.this));
                                                // SET OTP VERIFIED FLAG
                                                SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(true);
                                                //NOTE: Log GA event
                                                Bundle bundle = new Bundle();
                                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, registerOwner.getResultData().getLoginOutPut().getUserID());
                                                //bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, edtFirstName.getText().toString());
                                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, registerOwner.getResultData().getLoginOutPut().getUserGroupName());
                                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                                FirebaseAnalytics.getInstance(WelcomeActivity.this).logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                                                //FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                                                if (SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID() != null) {
                                                    sendRegistrationToServer(SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID());
                                                }
                                                navigateToWelcomeSliderActivity();
                                            }
                                        }


                                    }
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                            }

                            @Override
                            public void onFailure(Call<RegisterOwner> call, Throwable throwable) {
                                FWLogger.logInfo(TAG, "Exception in RegisterOwner API:");
                            }
                        });

                    } catch (Exception ex) {
                        FWLogger.logInfo(TAG, "Exception in RegisterOwner API:");
                        ex.getMessage();
                    }
                }
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(WelcomeActivity.this);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        try {
            if (classType.equalsIgnoreCase(RegisterOwner.class.getSimpleName())) {
                FWLogger.logInfo(TAG, "REGISTER TOUCHLESS OWNER : FIRST_NAME/LAST_NAME/NO_OF_USERS");
                Gson gson = new Gson();
                RegisterOwner tRegister = gson.fromJson(urlConnectionResponse.resultData, RegisterOwner.class);
                if (tRegister != null) {
                    if (tRegister.getCode().equalsIgnoreCase("200") &&
                            tRegister.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.owner_created_successfully, WelcomeActivity.this))) {
                        SharedPrefManager.getInstance(getApplicationContext()).ownerSignUp(tRegister, CommonFunction.getUniqueDeviceID(WelcomeActivity.this));
                        // SET OTP VERIFIED FLAG
                        SharedPrefManager.getInstance(getApplicationContext()).setOTPVerified(true);
                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, tRegister.getResultData().getLoginOutPut().getUserID());
                        //bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, edtFirstName.getText().toString());
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, tRegister.getResultData().getLoginOutPut().getUserGroupName());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                        //FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                        if (SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID() != null) {
                            sendRegistrationToServer(SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID());
                        }
                        navigateToWelcomeSliderActivity();
                        //navigateToHomeActivity();
                        //navigateToTouchLessLoginActivity();

                    }
                }
            } else if (classType.equalsIgnoreCase(RegisterDeviceId.class.getSimpleName())) {
                Gson gson = new Gson();
                RegisterDeviceId registerDeviceId = gson.fromJson(urlConnectionResponse.resultData, RegisterDeviceId.class);
                FWLogger.logInfo(TAG, "Device Registration");
                if (registerDeviceId != null) {
                    if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                        FWLogger.logInfo(TAG, "Device Registration Completed.");
                    } else {
                        FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            navigateToTouchLessSignupActivity();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
