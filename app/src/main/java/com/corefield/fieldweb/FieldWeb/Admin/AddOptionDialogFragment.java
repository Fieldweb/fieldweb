package com.corefield.fieldweb.FieldWeb.Admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * //
 * Created by CFS on 5/7/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Admin
 * Version : 1.3.1
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This fragment is used to submit the technicians in bulk
 * //
 **/
public class AddOptionDialogFragment extends Fragment {

    //Variables
    private String TAG = AddOptionDialogFragment.class.getSimpleName();

    //View's
    private View mRootView;
    Bundle bundle;
    ScrollView mScrollView;

    LinearLayout linearAddEnq, linearAddTask, linearAddTech,
            linearAddItem, linearAssignItem, linearAddDeductBal, linearAddAMC, linearAddCustomer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.dialog_add_options_new, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {

        linearAddEnq = mRootView.findViewById(R.id.linear_add_enq);
        linearAddTask = mRootView.findViewById(R.id.linear_add_task);
        linearAddTech = mRootView.findViewById(R.id.linear_add_tech);
        linearAddItem = mRootView.findViewById(R.id.linear_add_item);
        linearAssignItem = mRootView.findViewById(R.id.linear_assign_item);
        linearAddDeductBal = mRootView.findViewById(R.id.linear_add_deduct_bal);
        linearAddAMC = mRootView.findViewById(R.id.linear_add_amc);
        linearAddCustomer = mRootView.findViewById(R.id.linear_add_customer);
        mScrollView = mRootView.findViewById(R.id.scrollView);


        try {
            //NOTE: Log GA event
            bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getActivity()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getActivity()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        } catch (Exception ex) {
            ex.getMessage();
        }

        linearAddEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).addEnquiry();
                getActivity().onBackPressed();
            }
        });

        linearAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).addTask();
                getActivity().onBackPressed();
            }
        });

        linearAddTech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).loadFragment(new AddBulkTechFragment(), R.id.navigation_home);
                getActivity().onBackPressed();
            }
        });

        linearAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).addItem();
                getActivity().onBackPressed();
            }
        });

        linearAssignItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).assignItem();
                getActivity().onBackPressed();
            }
        });

        linearAddDeductBal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).addDeductBalance();
                getActivity().onBackPressed();
            }
        });

        linearAddAMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AMCDialog amcDialog = AMCDialog.getInstance();
//                amcDialog.addEditAMC(activity, this, mServiceOccurrenceTypeList, mReminderModeList, -1, -1, -1);
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                ((HomeActivityNew) getActivity()).addAMCDialog(((HomeActivityNew) getActivity()).mServiceOccurrenceTypeList, ((HomeActivityNew) getActivity()).mReminderModeList);
                getActivity().onBackPressed();

            }
        });

        linearAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.TOOLBAR_MENU_CLICK, bundle);
                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.addCustomer(getActivity());
                getActivity().onBackPressed();
            }
        });

        AddOptionAppTour();
    }

    private void AddOptionAppTour() {
        try {
            TapTarget target1 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_enq),
                    getString(R.string.title_add_enquiry),
                    getString(R.string.title_add_enquiry))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target2 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_task),
                    getString(R.string.add_task),
                    getString(R.string.add_task))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target3 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_tech),
                    getString(R.string.title_add_tech),
                    getString(R.string.title_add_tech))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target4 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_item),
                    getString(R.string.add_item),
                    getString(R.string.add_item))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target5 = TapTarget.forView(mRootView.findViewById(R.id.linear_assign_item),
                    getString(R.string.assign_item),
                    getString(R.string.assign_item))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target6 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_deduct_bal),
                    getString(R.string.add_deduct_bal),
                    getString(R.string.add_deduct_bal))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            TapTarget target7 = TapTarget.forView(mRootView.findViewById(R.id.linear_add_amc),
                    getString(R.string.add_amc),
                    getString(R.string.add_amc))
                    .drawShadow(true)
                    .cancelable(true)
                    .targetCircleColor(R.color.colorAccent)
                    .transparentTarget(true)
                    .outerCircleColor(R.color.calendar_text_default)
                    .dimColor(R.color.colorPrimaryDark).targetRadius(60);

            mScrollView.post(new Runnable() {
                public void run() {
                    mScrollView.fullScroll(mScrollView.FOCUS_DOWN);
                }
            });

            TapTargetSequence sequence = new TapTargetSequence(getActivity());
            sequence.targets(target1, target2, target3, target4, target5, target6, target7);


            sequence.listener(new TapTargetSequence.Listener() {
                @Override
                public void onSequenceFinish() {
                    //mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                    //Toast.makeText(HomeActivityNew.this, "App Tour Completed Successfully.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                }

                @Override
                public void onSequenceCanceled(TapTarget lastTarget) {
                    // mCalculatorSetting.getEditor().putBoolean(GRAPH_STATED, true).apply();
                }
            });
            sequence.start();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
