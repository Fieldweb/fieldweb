package com.corefield.fieldweb.FieldWeb.Accounts;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListViewAdapter;
import com.corefield.fieldweb.Adapter.NotesViewAdapter;
import com.corefield.fieldweb.Adapter.QuotationItemDetailsListAdapter;
import com.corefield.fieldweb.Adapter.QuotServiceDetailsListAdapter;
import com.corefield.fieldweb.DTO.Account.DeleteQuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.DownloadQuotationPdfDTO;
import com.corefield.fieldweb.DTO.Account.UpdateQuotationStatusDTO;
import com.corefield.fieldweb.DTO.Account.InvoicePaymentStatusDTO;
import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.SaveInvoicePaymentAmountDetailsDTO;
import com.corefield.fieldweb.DTO.Account.TaxDetails;
import com.corefield.fieldweb.DTO.Account.UpdateQuotationStatusDTO;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Task.DownloadReport;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.LeadManagement.LeadManagementFragment;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;

import com.corefield.fieldweb.Util.SharedPrefManager;

import com.google.android.gms.maps.model.Marker;


import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks Details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show completed task details
 */
@SuppressLint("ValidFragment")
public class QuotationDetailsFragment extends Fragment implements View.OnClickListener {

    public static String TAG = QuotationDetailsFragment.class.getSimpleName();
    private View mRootView;
    private int quoteID;
    private TasksList.ResultData mResultData;
    private TextView ViewSLA, mTextViewQuotStatus, mTextCreateDate, mTextValidityDate, mTextQuotTaskId, mTextViewEndDateTime, mTextQuoteId, mTerms, mTextViewItemName, mTextViewItemQuantity, mTextViewCustomerName, mTextViewCustomerNo, mTextViewLandmark, mTextViewAddress, mTextViewTaskName, mTextViewWorkMode, mTextViewSignedBy, mTextViewRating, mTextViewSignedMobileNo, mTextViewTaskID, mtxtQuoTotalAmt, mtxtQuoGrandTotalAmt, mtxtQuoDicount, mtxtQuoTax, mExtraName, mExtraPrice;
    private ImageView mImageVieweditquote, mImageViewupdatestatus, mImageViewdelete, mImageViewaddtask, mImageViewDownload;
    private Marker mMarkerDestination;
    private double mDestLat = 0, mDestLong = 0;
    private RecyclerView multipleRecycleQuoItem, multipleServiceDetailsRecycl;
    private NotesViewAdapter mNotesViewAdapter;
    private ProgressDialog mProgressDialog;
    private String mUrl;
    private DeviceListViewAdapter mDeviceListViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManagerServ;
    private List<TaskClosure.TechnicalNotedto> mTechnicalNoteDTOs = new ArrayList<>();
    private List<TaskClosure.DeviceInfoList> mDeviceInfoLists = new ArrayList<>();
    private ImageView mImageViewSignature;
    private RatingBar mRatingBar;
    Fragment fragment;
    Context mContext;
    private LinearLayout mLinearNotesList, mLinearDeviceList;
    private LinearLayout mTaskClosureWrapper, mLinearLayoutRatingWrapper;
    private static final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET};
    private static final String[] PERMISSIONS_TIRAMISU = {Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_AUDIO};
    QuotationItemDetailsListAdapter quoItemDetailsListAdapter;
    QuotServiceDetailsListAdapter quotationServiceDetailsListAdapter;
    public List<QuotationDetailsDTO.ResultData> mQuoteList = null;
    //
    public List<CustomerList.ResultData> mCustomerList;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    private List<TaxDetails.ResultData> mTaxDetailsArry;
    int StatusId;

    @SuppressLint("ValidFragment")
    public QuotationDetailsFragment() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.quotation_details_fragment, container, false);

        inIt();

        //setData();
        //manageBackPress();
        checkStoragePermission();
        return mRootView;
    }

    private void inIt() {

        mTextViewQuotStatus = mRootView.findViewById(R.id.txtQuotStatus);
        mTextCreateDate = mRootView.findViewById(R.id.quoCreateDate);
        mTextValidityDate = mRootView.findViewById(R.id.txQuotValidyDate);
        mTextQuotTaskId = mRootView.findViewById(R.id.txtQuotTaskID);
        mTextViewEndDateTime = mRootView.findViewById(R.id.task_details_task_end_date_time);
        mTextQuoteId = mRootView.findViewById(R.id.txtQuoteId);
        mTextViewItemName = mRootView.findViewById(R.id.task_details_item_name);
        mTextViewItemQuantity = mRootView.findViewById(R.id.task_details_item_quantity);
        mTextViewCustomerName = mRootView.findViewById(R.id.task_details_customer_name);
        mTextViewCustomerNo = mRootView.findViewById(R.id.task_details_customer_number);
        mTextViewLandmark = mRootView.findViewById(R.id.txtQuoLandmark);
        mTextViewAddress = mRootView.findViewById(R.id.task_details_address);
        mTextViewTaskName = mRootView.findViewById(R.id.task_details_task_name);

        mImageViewupdatestatus = mRootView.findViewById(R.id.imageView_updatestatus);
        mImageViewdelete = mRootView.findViewById(R.id.imageView_delete);
        mImageViewaddtask = mRootView.findViewById(R.id.imageView_addtask);
        mImageViewDownload = mRootView.findViewById(R.id.imageView_download);

        mTaskClosureWrapper = mRootView.findViewById(R.id.linear_layout_task_closure_wrapper);
        mLinearLayoutRatingWrapper = mRootView.findViewById(R.id.linear_layout_rating_wrapper);
        mTextViewWorkMode = mRootView.findViewById(R.id.textView_work_mode);
        mImageViewSignature = mRootView.findViewById(R.id.customer_signature);
        mRatingBar = mRootView.findViewById(R.id.ratingbar_task_closure_view);
        mTextViewRating = mRootView.findViewById(R.id.textView_rating);
        //
        mtxtQuoTotalAmt = mRootView.findViewById(R.id.txtQuoTotalAmt);
        mtxtQuoGrandTotalAmt = mRootView.findViewById(R.id.txtQuoGrandTotalAmt);
        mtxtQuoDicount = mRootView.findViewById(R.id.txtQuoDicount);
        mtxtQuoTax = mRootView.findViewById(R.id.txtQuoTax);

        mTerms = mRootView.findViewById(R.id.txtterms);

        multipleRecycleQuoItem = mRootView.findViewById(R.id.multipleRecycleQuoItem);
        multipleServiceDetailsRecycl = mRootView.findViewById(R.id.multipleServiceDetails);

        mTextViewSignedBy = mRootView.findViewById(R.id.textView_signed_by);
        mTextViewSignedMobileNo = mRootView.findViewById(R.id.textview_signed_mobile_no);
        mTextViewTaskID = mRootView.findViewById(R.id.task_id);
        //
        mImageVieweditquote = mRootView.findViewById(R.id.imageView_edit);
        ViewSLA = mRootView.findViewById(R.id.view_SLA);

        mExtraName = mRootView.findViewById(R.id.txtExtraName);
        mExtraPrice = mRootView.findViewById(R.id.txtExtraAmt);

        mImageViewupdatestatus.setOnClickListener(this);
        mImageViewdelete.setOnClickListener(this);
        mImageViewaddtask.setOnClickListener(this);
        mImageViewDownload.setOnClickListener(this);
        //mImageVieweditquote.setOnClickListener(this);
        ViewSLA.setOnClickListener(this);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                quoteID = getBundle.getInt("quoteID");
                getQuoteDetails(quoteID);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void getQuoteDetails(int quoteId) {
        try {
            if (quoteId != 0) {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    CommonFunction.showProgressDialog(getActivity());
                    int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    Call<QuotationDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getQuotationDetailsById(quoteId, userID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<QuotationDetailsDTO>() {
                        @Override
                        public void onResponse(Call<QuotationDetailsDTO> call, Response<QuotationDetailsDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    CommonFunction.hideProgressDialog(getActivity());
                                    mQuoteList = new ArrayList<>();
                                    QuotationDetailsDTO quotationDetailsDTO = response.body();
                                    mQuoteList = Collections.singletonList(quotationDetailsDTO.getResultData());
                                    if (mQuoteList.size() > 0) {
                                        setData(mQuoteList);
                                        getTaxDetails();
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<QuotationDetailsDTO> call, Throwable throwable) {
                            CommonFunction.hideProgressDialog(getActivity());
                            FWLogger.logInfo(TAG, "Exception in getquotationsbyid API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in getquotationsbyid API:");
            ex.getMessage();
        }

    }

    private void setData(List<QuotationDetailsDTO.ResultData> mQuoteList) {
        try {
            mTextViewTaskName.setText(mQuoteList.get(0).getQuoteName());
            String strCreatedDate = mQuoteList.get(0).getCreatedDate().split("T")[0];
            String strCreatedTime = mQuoteList.get(0).getCreatedDate().split("T")[1];

            String strValidityDate = mQuoteList.get(0).getValidityDate().split("T")[0];
            String strValidityTime = mQuoteList.get(0).getValidityDate().split("T")[1];

            String formattedCreatedDate = DateUtils.convertDateFormat(strCreatedDate, "yyyy-MM-dd", "dd-MM-yyyy");
            String formattedValidityDate = DateUtils.convertDateFormat(strValidityDate, "yyyy-MM-dd", "dd-MM-yyyy");

            String formattedCrTime = DateUtils.timeFormatConversion(strCreatedTime, "HH:mm:ss", "hh:mm aa");
            String formattedValTime = DateUtils.timeFormatConversion(strValidityTime, "HH:mm:ss", "hh:mm aa");

            if (formattedCreatedDate != null && formattedCrTime != null) {
                mTextCreateDate.setText(formattedCreatedDate + "  " + formattedCrTime);
            } else {
                mTextCreateDate.setText("-NA-");
            }
            if (formattedValidityDate != null && formattedValTime != null) {
                mTextValidityDate.setText(formattedValidityDate + "  " + formattedValTime);
            } else {
                mTextValidityDate.setText("-NA-");
            }
            mTextQuoteId.setText("QUO" + mQuoteList.get(0).getId());
            mTextViewCustomerName.setText(mQuoteList.get(0).getCustomer().getCustomerName());
            mTextViewCustomerNo.setText(mQuoteList.get(0).getCustomer().getMobileNumber());
            mTextViewAddress.setText(mQuoteList.get(0).getCustomer().getLocationList().getAddress());
            mTextViewLandmark.setText(mQuoteList.get(0).getCustomer().getLocationList().getDescription());

            if (mQuoteList.get(0).getQuoteTaxList().size() > 0) {
                mtxtQuoDicount.setText("₹" + mQuoteList.get(0).getQuoteTaxList().get(0).getDiscountAmounnt() + "(" + mQuoteList.get(0).getQuoteTaxList().get(0).getDiscount() + "%" + ")");

                mtxtQuoTax.setText("₹" + mQuoteList.get(0).getQuoteTaxList().get(0).getTaxAmount() + "(" + mQuoteList.get(0).getQuoteTaxList().get(0).getTax() + "%" + ")");
            }

            mtxtQuoTotalAmt.setText("₹" + mQuoteList.get(0).getTotalAmount());
            mtxtQuoGrandTotalAmt.setText("₹" + mQuoteList.get(0).getGrandTotalAmount());

            mExtraName.setText(mQuoteList.get(0).getExtraItem());
            mExtraPrice.setText("₹" + String.valueOf(mQuoteList.get(0).getExtraAmount()));
            mTerms.setText(mQuoteList.get(0).getTermCondition());

            switch (mQuoteList.get(0).getStatus().getStatusName()) {

                case "Assigned":
                    mTextViewQuotStatus.setTextColor(getActivity().getResources().getColor(R.color.task_ongoing_dark));
                    mTextViewQuotStatus.setText(mQuoteList.get(0).getStatus().getStatusName());
                    mTextViewQuotStatus.setText(mQuoteList.get(0).getStatus().getStatusName());
                    mImageViewupdatestatus.setVisibility(View.INVISIBLE);
                    mImageViewdelete.setVisibility(View.INVISIBLE);
                    mImageViewaddtask.setVisibility(View.INVISIBLE);
                    mImageVieweditquote.setVisibility(View.INVISIBLE);
                    break;

                case "Not Assigned":
                    mTextViewQuotStatus.setTextColor(getActivity().getResources().getColor(R.color.completed));
                    mTextViewQuotStatus.setText(mQuoteList.get(0).getStatus().getStatusName());
                    break;

                case "Lost":
                    mTextViewQuotStatus.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    mTextViewQuotStatus.setText(mQuoteList.get(0).getStatus().getStatusName());
                    break;

                case "Task Completed":
                    mTextViewQuotStatus.setTextColor(getActivity().getResources().getColor(R.color.green));
                    mTextViewQuotStatus.setText(mQuoteList.get(0).getStatus().getStatusName());
                    mImageViewupdatestatus.setVisibility(View.INVISIBLE);
                    mImageViewdelete.setVisibility(View.INVISIBLE);
                    mImageViewaddtask.setVisibility(View.INVISIBLE);
                    mImageVieweditquote.setVisibility(View.INVISIBLE);
                    break;

                default:
                    break;
            }

            // FOR MULTIPLE QUOTE ITEM DETAILS
            try {
                List<QuotationDetailsDTO.QuoteItemList> multipleItemAssignedArrayList = new ArrayList<QuotationDetailsDTO.QuoteItemList>();
                multipleItemAssignedArrayList = mQuoteList.get(0).getQuoteItemList();
                if (multipleItemAssignedArrayList.size() > 0) {
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    multipleRecycleQuoItem.setLayoutManager(mLayoutManager);
                    quoItemDetailsListAdapter = new QuotationItemDetailsListAdapter(getActivity(), multipleItemAssignedArrayList);
                    multipleRecycleQuoItem.setAdapter(quoItemDetailsListAdapter);
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR MULTIPLE SERVICE DETAILS
        try {
            List<QuotationDetailsDTO.QuoteServiceList> multiplServiceArrayList = new ArrayList<QuotationDetailsDTO.QuoteServiceList>();
            multiplServiceArrayList = mQuoteList.get(0).getQuoteServiceList();
            if (multiplServiceArrayList.size() > 0) {
                mLayoutManagerServ = new LinearLayoutManager(getActivity());
                multipleServiceDetailsRecycl.setLayoutManager(mLayoutManagerServ);
                quotationServiceDetailsListAdapter = new QuotServiceDetailsListAdapter(getActivity(), multiplServiceArrayList);
                multipleServiceDetailsRecycl.setAdapter(quotationServiceDetailsListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageView_edit:
               /* try {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddUpdateServiceDialog addUpdateServiceDialog = AddUpdateServiceDialog.getInstance();
                    addUpdateServiceDialog.editQuote(getContext(), activity, ((HomeActivityNew) getActivity()).mCustomerList, ((HomeActivityNew) getActivity()).mServiceTypeResultData, mItemsNameList, mTaxDetailsArry, mQuoteList);
                } catch (Exception e) {
                    e.getMessage();
                }*/

                break;

            case R.id.imageView_delete:
                deleteQuotationDetailsDialog(getActivity(), quoteID);
                break;

            case R.id.imageView_updatestatus:
                UpdateQuotationStatus(getContext(), getActivity());
                break;

            case R.id.imageView_addtask:
                TaskDialogNew taskDialog = TaskDialogNew.getInstance();
                taskDialog.addTaskFromQuoteDialog(getActivity(), ((HomeActivityNew) getActivity()).mItemsNameList, ((HomeActivityNew) getActivity()).mUsersNameList, false, null, -1, 0, false, null, ((HomeActivityNew) getActivity()).mCustomerList, mQuoteList, quoteID);
                break;

            case R.id.imageView_download:
                getDownloadQuotePDF(quoteID);
                break;

         /*   case R.id.view_SLA:
                viewSLAFullScreenMode(getActivity());
                break;*/

            default:
                break;
        }
    }

/*
    public void viewSLAFullScreenMode(Activity activity) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_show_web_view);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        WebView webView = dialog.findViewById(R.id.web_view_full_screen);

        webView.setWebViewClient(new MyBrowser());

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        mUrl = "https://fieldweb.co.in/terms-conditions";
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(mUrl);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }
*/

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error:" + description, Toast.LENGTH_SHORT).show();

        }
    }


    public void UpdateQuotationStatus(Context context, Activity mActivity) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_quote_status);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        Spinner quoteSpinStatus = dialog.findViewById(R.id.quoteSpinStatus);
        TextView quoteNotes = dialog.findViewById(R.id.quoteNotes);
        ImageView mcloseLeadstatus = dialog.findViewById(R.id.closeLeadstatus);
        Button mbtnUpdateLeadstatus = dialog.findViewById(R.id.btnUpdateLeadstatus);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        // SET SPINNER DATA
        String[] quoteValues = {"Select Status", "Lost"};
        ArrayAdapter<String> quoteType = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, quoteValues);
        quoteSpinStatus.setAdapter(quoteType);

        mcloseLeadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnUpdateLeadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Id = quoteID;
                int UserId = SharedPrefManager.getInstance(context).getUserId();
                String Notes = quoteNotes.getText().toString();

                if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Select Status")) {
                    Toast.makeText(getContext(), "Select Status!", Toast.LENGTH_SHORT).show();
                } else if (quoteSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Lost")) {
                    StatusId = 4;
                    dialog.dismiss();
                    UpdateQuotationStatuss(Id, UserId, StatusId, Notes);
                    AccountsTabHost myFragment = new AccountsTabHost();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                } else {
                    dialog.dismiss();
                    UpdateQuotationStatuss(Id, UserId, StatusId, Notes);
                    AccountsTabHost myFragment = new AccountsTabHost();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                }
            }
        });
    }


    private void deleteQuotationDetailsDialog(Activity activity, int quoteID) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_quotedetails_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button deleteQuote, buttonCancel;

        deleteQuote = dialog.findViewById(R.id.deleteQuote);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        deleteQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeleteQuotationDetails(quoteID);
                dialog.dismiss();
                //
                AccountsTabHost myFragment = new AccountsTabHost();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void getDeleteQuotationDetails(int quoteId) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<DeleteQuotationDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getDeleteQuotationDetails(quoteId, userID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<DeleteQuotationDetailsDTO>() {
                    @Override
                    public void onResponse(Call<DeleteQuotationDetailsDTO> call, Response<DeleteQuotationDetailsDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                if (response.code() == 200) {
                                    DeleteQuotationDetailsDTO deleteQuotationDetailsDTO = response.body();
                                    if (deleteQuotationDetailsDTO != null) {
                                        if (deleteQuotationDetailsDTO.getCode().equalsIgnoreCase("200")) {
                                            Toast.makeText(getContext(), deleteQuotationDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getContext(), deleteQuotationDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                        setRefresh();
                                    } else {
                                        Toast.makeText(getContext(), R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteQuotationDetailsDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteQuote API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteQuote API:");
            ex.getMessage();
        }
    }

    private void getTaxDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<TaxDetails> call = RetrofitClient.getInstance(getActivity()).getMyApi().getTaxDetails();
                call.enqueue(new retrofit2.Callback<TaxDetails>() {
                    @Override
                    public void onResponse(Call<TaxDetails> call, Response<TaxDetails> response) {
                        try {
                            if (response.code() == 200) {
                                TaxDetails taxDetails = response.body();
                                FWLogger.logInfo(TAG, "TaxDetails Successful");
                                if (taxDetails != null) {
                                    if (taxDetails.getCode().equalsIgnoreCase("200") && taxDetails.getMessage().equalsIgnoreCase("successfully.")) {
                                        mTaxDetailsArry = new ArrayList<>();
                                        mTaxDetailsArry = taxDetails.getResultData();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TaxDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in gettaxlist API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetCountryList API:");
            ex.getMessage();
        }
    }

    public void UpdateQuotationStatuss(int QuoteId, int UserID, int StatusId, String note) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<UpdateQuotationStatusDTO> call = RetrofitClient.getInstance(getActivity()).getMyApi().updateQuotationStatus(QuoteId, UserID, StatusId, note, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateQuotationStatusDTO>() {
                    @Override
                    public void onResponse(Call<UpdateQuotationStatusDTO> call, Response<UpdateQuotationStatusDTO> response) {
                        try {
                            if (response.code() == 200) {
                                UpdateQuotationStatusDTO UpdateQuotationStatusDTO = response.body();
                                //NOTE: Log GA event
                                Toast.makeText(getActivity(), UpdateQuotationStatusDTO.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateQuotationStatusDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getContext());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
            ex.getMessage();
        }
    }

    private void getDownloadQuotePDF(int quoteID) {
        if (quoteID != 0) {
            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    int userId = SharedPrefManager.getInstance(getContext()).getUserId();
                    Call<DownloadQuotationPdfDTO> call = RetrofitClient.getInstance(getContext()).getMyApi().getQuotationPdf(quoteID, userId, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<DownloadQuotationPdfDTO>() {
                        @Override
                        public void onResponse(Call<DownloadQuotationPdfDTO> call, Response<DownloadQuotationPdfDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    DownloadQuotationPdfDTO downloadQuotationPdfDTO = response.body();
                                    if (downloadQuotationPdfDTO != null) {
                                        FWLogger.logInfo(TAG, "Code : " + downloadQuotationPdfDTO.getCode());
                                        FWLogger.logInfo(TAG, "Message : " + downloadQuotationPdfDTO.getMessage());
                                        if (downloadQuotationPdfDTO.getCode().equalsIgnoreCase("200")) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                                if (!hasPermissions(getContext(), PERMISSIONS_TIRAMISU)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(downloadQuotationPdfDTO.getResultData().getQuotationPdfPath(), getActivity());
                                                }
                                            } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                                                if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(downloadQuotationPdfDTO.getResultData().getQuotationPdfPath(), getActivity());
                                                }
                                            }
                                           /* if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                t.show();
                                            } else {
                                                DownloadFile.downloadFile(downloadQuotationPdfDTO.getMessage(), getActivity());
                                            }*/
                                        } else {
                                            Toast.makeText(getContext(), downloadQuotationPdfDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<DownloadQuotationPdfDTO> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                ex.getMessage();
            }
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setRefresh() {
        clearBackStackEntries();
        AccountsTabHost myFragment = new AccountsTabHost();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, myFragment);
        transaction.commit();
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private boolean checkStoragePermission() {
        boolean storageFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkWriteStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                final int checkReadStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if (checkWriteStorage != PackageManager.PERMISSION_GRANTED && checkReadStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    storageFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return storageFlag;
    }


    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearBackStackEntries();
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    AccountsTabHost myFragment = new AccountsTabHost();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}