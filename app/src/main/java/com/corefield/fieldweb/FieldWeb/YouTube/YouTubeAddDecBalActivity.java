package com.corefield.fieldweb.FieldWeb.YouTube;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.corefield.fieldweb.R;

public class YouTubeAddDecBalActivity extends Activity {
    private WebView webView;
    private static final String VIDEO_ID = "80rYRL_syGk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_youtube_new);

        webView = findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Load the YouTube video URL with autoplay
        String videoUrl = "https://www.youtube.com/embed/" + VIDEO_ID + "?rel=0&autoplay=1&controls=1&showinfo=0";
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl(videoUrl);
    }
}