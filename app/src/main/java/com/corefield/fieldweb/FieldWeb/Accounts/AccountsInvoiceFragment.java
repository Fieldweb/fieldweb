package com.corefield.fieldweb.FieldWeb.Accounts;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.InvoiceListAdapter;
import com.corefield.fieldweb.Adapter.QuotationListAdapter;
import com.corefield.fieldweb.Adapter.ServiceTypeListAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Account.InvoiceListDTO;
import com.corefield.fieldweb.DTO.Account.QuotationListDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Dialogs.AddUpdateServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerItemInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show item inventory where Owner can add item and Assign Item
 */
public class AccountsInvoiceFragment extends Fragment implements View.OnClickListener, RecyclerTouchListener {
    protected static String TAG = AccountsInvoiceFragment.class.getSimpleName();
    public List<InvoiceListDTO.ResultData> mInvoiceList = null;
    public List<UsersList.ResultData> mUsersLists = null;
    public ArrayList<String> mItemsNameList;
    public ArrayList<String> mUsersNameList;
    private View mRootView;
    ImageView noResultImg;
    private String mSearchParam = "";
    private boolean mSearchFilter = false;
    private List<InvoiceListDTO.ResultData> getmInvoiceList;

    private RecyclerView mRecyclerViewInvoiceList;
    private InvoiceListAdapter minvoiceListAdapter;
    //private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager;
    private boolean mAllItemApiCall = false, mItemListApiCall = false, mUserListApiCall = false;
    private Button mButtonInventory, mButtonIssuedItems;
    private String mSelection = "";
    private SearchView mSearchView;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;
    //
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START;
    private boolean isLoadFirstTime = false;
    Spinner invoiceSpinStatus;
    private int StatusId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.accounts_invoice_fragment, container, false);
        mButtonInventory = mRootView.findViewById(R.id.button_inventory);
        mButtonIssuedItems = mRootView.findViewById(R.id.button_issued_item);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        noResultImg = mRootView.findViewById(R.id.noResultImg);
        invoiceSpinStatus = mRootView.findViewById(R.id.spin_invoice_type);
        recyclerTouchListener = this;
        fragment = this;

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        mRecyclerViewInvoiceList = mRootView.findViewById(R.id.recycler_invoice_list);

        FGALoadEvent();
        mSelection = "INV";
        isLoadFirstTime = true;
        //getInvoiceList(mSearchParam, mCurrentPage, StatusId);
        setSearchFilter();


        // BY M
        AutoCompleteTextView search_text = (AutoCompleteTextView) mSearchView.findViewById(mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        search_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.list_title));
        search_text.setGravity(Gravity.CENTER_VERTICAL);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(10);
        search_text.setFilters(filterArray);
        mSearchView.setGravity(Gravity.CENTER_VERTICAL);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
            }
        });
        //

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewInvoiceList.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        //
        mRecyclerViewInvoiceList.setLayoutManager(mLayoutManager);
        mRecyclerViewInvoiceList.setItemAnimator(new DefaultItemAnimator());
        minvoiceListAdapter = new InvoiceListAdapter(getActivity(), mInvoiceList, recyclerTouchListener);
        mRecyclerViewInvoiceList.setAdapter(minvoiceListAdapter);
        minvoiceListAdapter.notifyDataSetChanged();
        minvoiceListAdapter.setClickListener(recyclerTouchListener);

        // SET SPINNER DATA
        String[] quoteValues = {"Select Status", "Un-Paid", "Partial-Paid", "Paid"};
        ArrayAdapter<String> quoteType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, quoteValues);
        invoiceSpinStatus.setAdapter(quoteType);

        invoiceSpinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (invoiceSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Select Status")) {
                    if (mInvoiceList != null) clearTaskList();
                    StatusId = 0;
                    mSearchView.setQuery("", false);
                    getInvoiceList(mSearchParam, mCurrentPage, StatusId);
                } else if (invoiceSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Un-Paid")) {
                    if (mInvoiceList != null) clearTaskList();
                    StatusId = 6;
                    mSearchView.setQuery("", false);
                    getInvoiceList(mSearchParam, mCurrentPage, StatusId);
                } else if (invoiceSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Partial-Paid")) {
                    if (mInvoiceList != null) clearTaskList();
                    StatusId = 7;
                    mSearchView.setQuery("", false);
                    getInvoiceList(mSearchParam, mCurrentPage, StatusId);
                } else if (invoiceSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Paid")) {
                    if (mInvoiceList != null) clearTaskList();
                    StatusId = 8;
                    mSearchView.setQuery("", false);
                    getInvoiceList(mSearchParam, mCurrentPage, StatusId);
                } else if (invoiceSpinStatus.getSelectedItem().toString().equalsIgnoreCase("Lost")) {
                    if (mInvoiceList != null) clearTaskList();
                    StatusId = 4;
                    mSearchView.setQuery("", false);
                    getInvoiceList(mSearchParam, mCurrentPage, StatusId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return mRootView;
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        getInvoiceList(mSearchParam, mCurrentPage, StatusId);
    }

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }


    public void getInvoiceList(String mSearchParam, int mCurrentPage, int StatusId) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<InvoiceListDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getInvoiceList(userID, 50, mCurrentPage, mSearchParam, StatusId, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<InvoiceListDTO>() {
                    @Override
                    public void onResponse(Call<InvoiceListDTO> call, Response<InvoiceListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                FWLogger.logInfo(TAG, "Get Invoice list");
                                mInvoiceList = new ArrayList<>();
                                InvoiceListDTO invoiceListDTO = response.body();
                                mInvoiceList = invoiceListDTO.getResultData();
                                if (mSearchFilter && mInvoiceList.size() == 0) {
                                    mRecyclerViewInvoiceList.setVisibility(View.GONE);
                                    noResultImg.setVisibility(View.VISIBLE);
                                    noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                } else {
                                    setResults(invoiceListDTO);
                                }
                                mSearchFilter = false;
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<InvoiceListDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetInvoiceStatusListByModuleId API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetInvoiceStatusListByModuleId API:");
            ex.getMessage();
        }
    }

    private void setResults(InvoiceListDTO invoiceListDTO) {
        getmInvoiceList = new ArrayList<>();
        getmInvoiceList = invoiceListDTO.getResultData();
        if (isLoadFirstTime) {
            if (getmInvoiceList == null || getmInvoiceList.isEmpty() || getmInvoiceList.size() <= 0) {
                mRecyclerViewInvoiceList.setVisibility(View.GONE);
                noResultImg.setVisibility(View.VISIBLE);
                noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                isLoading = false;
                isLastPage = true;
            } else {
                mRecyclerViewInvoiceList.setVisibility(View.VISIBLE);
                minvoiceListAdapter.addAll(getmInvoiceList);
                minvoiceListAdapter.notifyDataSetChanged();
                minvoiceListAdapter.addLoadingFooter();
            }
        } else {
            if (getmInvoiceList == null || getmInvoiceList.isEmpty() || getmInvoiceList.size() <= 0) {
                minvoiceListAdapter.removeLoadingFooter();
                isLoading = false;
                isLastPage = true;
            } else {
                minvoiceListAdapter.removeLoadingFooter();
                isLoading = false;
                minvoiceListAdapter.addAll(getmInvoiceList);
                minvoiceListAdapter.notifyDataSetChanged();
                minvoiceListAdapter.addLoadingFooter();
            }
        }
    }

    public void clearTaskList() {
        int size = mInvoiceList.size();
        mInvoiceList.clear();
        minvoiceListAdapter.notifyItemRangeRemoved(0, size);

        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        mSearchParam = "";

        minvoiceListAdapter = new InvoiceListAdapter(getActivity(), mInvoiceList, recyclerTouchListener);
        mRecyclerViewInvoiceList.setAdapter(minvoiceListAdapter);
        minvoiceListAdapter.notifyDataSetChanged();
        minvoiceListAdapter.setClickListener(recyclerTouchListener);
    }


    @Override
    public void onClick(View v) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        switch (v.getId()) {
            case R.id.button_inventory:
                break;

            case R.id.button_issued_item:
                break;

            default:
                break;
        }
    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ITEM_INVENTORY, bundle);
    }

    private void refresh() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        AccountsInvoiceFragment myFragment = new AccountsInvoiceFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }


    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;

            case R.id.image_item_update:

                break;

            case R.id.image_issue_item_update:
                break;

            default:
                break;
        }
    }

    private void setSearchFilter() {
        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if (minvoiceListAdapter != null) clearTaskList();
                mSearchParam = query;
                mSearchFilter = true;
                mockingNetworkDelay(query, 0, 0);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when query submitted
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    private void mockingNetworkDelay(String searchParam, int typeId, int statusId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getInvoiceList(mSearchParam, mCurrentPage, StatusId);
            }
        }, 1000);
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }

}
