package com.corefield.fieldweb.FieldWeb.Task;

/**
 * interface for Tab Change Listener
 *
 * @author CoreField
 * @version 1.1
 * @implNote This interface is used to change the tab of task tracking
 */
public interface TabChangeListener {

    void onTabChangeListener(int tabIndex);
}
