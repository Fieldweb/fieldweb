package com.corefield.fieldweb.FieldWeb.Passbook;


import static com.facebook.FacebookSdk.getApplicationContext;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.TodayPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.TodayPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Today Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show passbook of Today bar graph
 */
public class TodayPassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = TodayPassbookFragment.class.getSimpleName();
    private View mRootView;
    private BarChart mBarChart;
    private TodayPassbookAsyncTask mTodayPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.passbook_graph_fragment, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {
        mBarChart = mRootView.findViewById(R.id.barchart);
        mBarChart.setDescription("");
        mBarChart.setMaxVisibleValueCount(50);
        mBarChart.setPinchZoom(false);
        mBarChart.animateX(2000);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisLeft().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawGridLines(false);
       /* mTodayPassbookAsyncTask = new TodayPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mTodayPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        getTodayPassbookDetails();
    }

    private void getTodayPassbookDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<TodayPassbook> call = RetrofitClient.getInstance(getContext()).getMyApi().getTodayPassbookDetails(userID);
                call.enqueue(new retrofit2.Callback<TodayPassbook>() {
                    @Override
                    public void onResponse(Call<TodayPassbook> call, Response<TodayPassbook> response) {
                        try {
                            if (response.code() == 200) {
                                TodayPassbook todayPassbook = response.body();
                                // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
                                FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
                                ///////////////////////////////////////////////////////////////////////////////////////
                                todayPassbook.getResultData().getEarningAmount();
                                todayPassbook.getResultData().getCredit();
                                todayPassbook.getResultData().getExpenses();
                                todayPassbook.getResultData().getReturn();
                                todayPassbook.getResultData().getBalance();

                                /////////////////////////////////////////BAR CHART ////////////////////////////////////
                                ArrayList<BarEntry> estimated = new ArrayList<>();
                                estimated.add(new BarEntry(todayPassbook.getResultData().getEstimatedAmount(), 0));

                                ArrayList<BarEntry> earned = new ArrayList<>();
                                earned.add(new BarEntry(todayPassbook.getResultData().getEarningAmount(), 0));

                                ArrayList<BarEntry> expense = new ArrayList<>();
                                expense.add(new BarEntry(todayPassbook.getResultData().getExpenses(), 0));

                                BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
                                expenseBar.setColors(new int[]{R.color.orange}, getContext());

                                BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
                                estimatedBar.setColors(new int[]{R.color.green}, getContext());

                                BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
                                earnedBar.setColors(new int[]{R.color.blue}, getContext());

                                XAxis x = mBarChart.getXAxis();
                                x.setSpaceBetweenLabels(0);
                                x.setXOffset(.5f);
                                x.setYOffset(.5f);

                                ArrayList<String> labels = new ArrayList<String>();
                                labels.add(getString(R.string.today));

                                ArrayList<BarDataSet> dataSets = new ArrayList<>();
                                dataSets.add(expenseBar);
                                dataSets.add(estimatedBar);
                                dataSets.add(earnedBar);
                                BarData data = new BarData(labels, dataSets);
                                mBarChart.setData(data);

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayPassbook> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "GetTodaysPassbookByUserId API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in GetTodaysPassbookByUserId API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTodaysPassbookByUserId API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(TodayPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            TodayPassbook todayPassbook = gson.fromJson(urlConnectionResponse.resultData, TodayPassbook.class);
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            todayPassbook.getResultData().getEarningAmount();
            todayPassbook.getResultData().getCredit();
            todayPassbook.getResultData().getExpenses();
            todayPassbook.getResultData().getReturn();
            todayPassbook.getResultData().getBalance();

            /////////////////////////////////////////BAR CHART ////////////////////////////////////
            ArrayList<BarEntry> estimated = new ArrayList<>();
            estimated.add(new BarEntry(todayPassbook.getResultData().getEstimatedAmount(), 0));

            ArrayList<BarEntry> earned = new ArrayList<>();
            earned.add(new BarEntry(todayPassbook.getResultData().getEarningAmount(), 0));

            ArrayList<BarEntry> expense = new ArrayList<>();
            expense.add(new BarEntry(todayPassbook.getResultData().getExpenses(), 0));

            BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
            expenseBar.setColors(new int[]{R.color.orange}, getContext());

            BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
            estimatedBar.setColors(new int[]{R.color.green}, getContext());

            BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
            earnedBar.setColors(new int[]{R.color.blue}, getContext());

            XAxis x = mBarChart.getXAxis();
            x.setSpaceBetweenLabels(0);
            x.setXOffset(.5f);
            x.setYOffset(.5f);

            ArrayList<String> labels = new ArrayList<String>();
            labels.add(getString(R.string.today));

            ArrayList<BarDataSet> dataSets = new ArrayList<>();
            dataSets.add(expenseBar);
            dataSets.add(estimatedBar);
            dataSets.add(earnedBar);
            BarData data = new BarData(labels, dataSets);
            mBarChart.setData(data);
            ////////////////////////////////////////////////////////////////////////////////////////
        }

    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mTodayPassbookAsyncTask != null) {
            mTodayPassbookAsyncTask.cancel(true);
        }
        super.onDestroyView();

    }
}
