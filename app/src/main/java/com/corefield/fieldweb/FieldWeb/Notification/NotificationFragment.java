package com.corefield.fieldweb.FieldWeb.Notification;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.NotificationListAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Notification.Notification;
import com.corefield.fieldweb.DTO.Notification.UpdateNotificationIsRead;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.AMC.AMCDetailsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateNotificationIsReadAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Passbook.HomePassbookFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.OwnerTaskTrackingFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskDetailsFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TechPaymentReceivedFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.NotificationUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * //
 * Created by CFS on 2/17/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Notification
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class NotificationFragment extends Fragment implements OnTaskCompleteListener, RecyclerTouchListener, NewNotificationListener {
    private static String TAG = NotificationFragment.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewNotification;
    private List<Notification.ResultData> mResultData = null;
    private NotificationListAdapter mNotificationListAdapter;
    private String mNavigateFor = "";
    private LinearLayoutManager mLayoutManager;
    public NewNotificationListener mNewNotificationListener;
    private boolean isSyncInProgress = false;
    private UpdateNotificationIsReadAsyncTask mUpdateNotificationIsReadAsyncTask;
    private String mNotificationType = "";
    private int mAMCId, mAMCServiceDetailsId;
    private String mAMCTypeName = "";

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "onResume");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_notification, container, false);
        mNewNotificationListener = this;

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        inIt();
        syncNotification();
        return mRootView;
    }

    private void inIt() {
        mRecyclerViewNotification = mRootView.findViewById(R.id.recycler_view_notification_list);
    }

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.image_view_remove_noti:
                //close
                break;
            case R.id.textView_reassign:
                //Navigate to assign or reassign task based click event
                if (((TextView) view).getText().toString().equalsIgnoreCase(getContext().getString(R.string.task_assign))) {
                    if (mResultData.get(position).isRegistered())
                        assignTask(mResultData.get(position).getUserId());
                    else
                        Toast.makeText(getActivity(), getString(R.string.can_not_assign_task), Toast.LENGTH_SHORT).show();
                } else if (((TextView) view).getText().toString().equalsIgnoreCase(getContext().getString(R.string.re_assign))) {
                    mNavigateFor = getContext().getString(R.string.re_assign);
                    getTaskDetail(mResultData.get(position).getTaskId());
                }
                break;
            case R.id.textView_view_details:
                mNotificationType = mResultData.get(position).getNotificationType();
                if (mResultData.get(position).getAMCServiceDetailDtoObj() != null) {
                    mAMCId = mResultData.get(position).getAMCServiceDetailDtoObj().getAMCsId();
                    mAMCServiceDetailsId = mResultData.get(position).getAMCServiceDetailDtoObj().getAMCServiceDetailsId();
                    mAMCTypeName = mResultData.get(position).getAMCServiceDetailDtoObj().getAMCTypeName();
                }
                if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_AMC)) {
                    UpdateNotificationIsRead updateNotificationIsRead = new UpdateNotificationIsRead();
                    updateNotificationIsRead.setRead(true);
                    updateNotificationIsRead.setUserId(SharedPrefManager.getInstance(getContext()).getUserId());
                    updateNotificationIsRead.setOwnerId(SharedPrefManager.getInstance(getContext()).getUserId());
                    updateNotificationIsRead.setNotificationType(mNotificationType);
                    updateNotificationIsRead.setAMCServiceDetailsId(mResultData.get(position).getAMCServiceDetailDtoObj().getAMCServiceDetailsId());
                    syncIsRead(updateNotificationIsRead);
                    navigateToAMCDetailsFragment(mAMCId, mAMCServiceDetailsId, mAMCTypeName);
                } else {
                    //Navigate to task detail
                    mNavigateFor = getContext().getString(R.string.detail);
                    getTaskDetail(mResultData.get(position).getTaskId());
                }
                break;
            case R.id.linear_layout_noti_view:
                //NotificationType.TYPE_USER_INFO Navigate to Profile Fragment
                mNotificationType = mResultData.get(position).getNotificationType();
                if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_USER_INFO)) {
                    if (mResultData.get(position).isRegistered()) {
                        UpdateNotificationIsRead updateNotificationIsRead = new UpdateNotificationIsRead();
                        updateNotificationIsRead.setRead(true);
                        updateNotificationIsRead.setUserId(mResultData.get(position).getUserId());
                        updateNotificationIsRead.setOwnerId(mResultData.get(position).getOwnerId());
                        updateNotificationIsRead.setNotificationType(mNotificationType);
                        syncIsRead(updateNotificationIsRead);
//                        navigateToProfileFragment(mResultData.get(position).getUserId());
                        //navigateToProfileFragment(mResultData.get(position)); // 08-03-2022 / COMMENTED BY MANISH : ITS GOINGON
                        // PROOFILE SCREEN WHICH IS UNWANTED
                    } else
                        Toast.makeText(getActivity(), getString(R.string.can_not_assign_task), Toast.LENGTH_LONG).show();
                } else {
                    mNavigateFor = getContext().getString(R.string.task);
                    getTaskDetail(mResultData.get(position).getTaskId());
                }
                break;
            default:
                break;
        }
    }

    private void reAssignTask(TasksList.ResultData resultData) {
        ((HomeActivityNew) getActivity()).addTaskWithReassignDefaults(resultData);
    }

    private void assignTask(int techId) {
        int i = 0;
        for (i = 0; i < ((HomeActivityNew) getActivity()).mUsersLists.size(); i++) {
            if (((HomeActivityNew) getActivity()).mUsersLists.get(i).getId() == techId) {
                techId = ((HomeActivityNew) getActivity()).mUsersLists.get(i).getId();
                break;
            }
        }
        FWLogger.logInfo(TAG, "techId = " + techId + " pos = " + i);
        ((HomeActivityNew) getActivity()).addTask(techId, i);
    }

    private void getTaskDetail(int taskId) {
        if (taskId != 0) {
            if (getContext() == null) FWLogger.logInfo(TAG, "getContext is null");
           /* mGetTaskDetailFromTaskId = new GetTaskDetailFromTaskId(getContext(), BaseAsyncTask.Priority.LOW, this);
            mGetTaskDetailFromTaskId.execute(taskId, SharedPrefManager.getInstance(getContext()).getUserId());*/

            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                    Call<TasksList> call = RetrofitClient.getInstance(getContext()).getMyApi().getTaskDetail(userID, taskId,
                            "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<TasksList>() {
                        @Override
                        public void onResponse(Call<TasksList> call, Response<TasksList> response) {
                            try {
                                if (response.code() == 200) {
                                    TasksList tasksList = response.body();
                                    if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                                        if (tasksList != null) {
                                            TasksList.ResultData task = tasksList.getResultData().get(0);
                                            FWLogger.logInfo(TAG, "success");
                                            UpdateNotificationIsRead updateNotificationIsRead = setUpdateNotificationIsReadData(task);
                                            syncIsRead(updateNotificationIsRead);
                                            if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.re_assign))) {
                                                if (task != null)
                                                    reAssignTask(task);
                                            } else if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.task))) {
                                                //First check user is tech or owner

                                                //Owner Condition :
                                                //if owner then navigate to respective screen as per the task status (Completed,Rejected,Ongoing/Inactive)
                                                //1. Completed : Navigate to TaskDetailsFragment without FromRejectedList (Without Reassign functionality)
                                                //2. Rejected : Navigate to TaskDetailsFragment with FromRejectedList (With Reassign functionality)
                                                //3. Ongoing/Inactive : Navigate to OwnerTaskTrackingFragment
                                                //4.NotificationType.TYPE_EARNINGS navigate to Passbook Fragment

                                                //Tech Condition :
                                                //if tech then navigate to respective screen as per the task status (Completed,Ongoing/Inactive)
                                                //1. Completed : Navigate to TaskDetailsFragment without FromRejectedList (Without Reassign functionality)
                                                //2. Rejected : No Navigation
                                                //3. Inactive : Navigate to TechTaskTrackingFragment for StartTaskTechFragment
                                                //4. Ongoing with TaskState.ENDED_NO_PAYMENT and RATE : Navigate to TechPaymentReceivedFragment
                                                //5.NotificationType.TYPE_EARNINGS navigate to Passbook Fragment

                                                int taskStateId = task.getTaskState();
                                                String taskMode = task.getPaymentMode();
                                                int taskStatusId = task.getTaskStatusId();
                                                if (SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                                                    if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_TASK)) {
                                                        if (taskStatusId == TaskStatus.COMPLETED) {
                                                            navigateToTaskDetails(task, true, true);
                                                        } else if (taskStatusId == TaskStatus.REJECTED) {
                                                            navigateToTaskDetails(task, true, false);
                                                        } else if (taskStatusId == TaskStatus.ON_GOING) {
                                                            navigateToOwnerTaskTrackingFragment(task);
                                                        }
                                                    } else if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_EARNINGS)) {
                                                        navigateToHomePassbookFragment();
                                                    }
                                                } else if (SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                                                    if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_TASK)) {
                                                        if (taskStatusId == TaskStatus.COMPLETED) {
                                                            //navigateToTaskDetails(task, false);
                                                        } else if (taskStatusId == TaskStatus.IN_ACTIVE) {
//                                        navigateToTechTaskTrackingFragment(task, 0);
                                                            TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                                            taskDialogNew.acceptTaskDialog(getActivity(), task, TaskStatus.IN_ACTIVE);
                                                        } else if (taskStatusId == TaskStatus.ON_GOING) {
                                                            if (taskMode != null && taskStateId == TaskState.ENDED_NO_PAYMENT && taskMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING)
                                                                    && task.getPaymentModeId() == Constant.PaymentMode.RATE_ID && task.isTaskClosureStatus() == true) {
                                                                navigateToTechPaymentReceivedFragment(task);
                                                            } else {
                                                                navigateToTaskClosure(task);
                                                            }
                                                        }
                                                    } else if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_EARNINGS)) {
                                                        navigateToHomePassbookFragment();
                                                    }
                                                }

                                            } else if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.detail))) {
                                                if (task != null) {
                                                    if (task.getTaskStatusId() == TaskStatus.COMPLETED)
                                                        navigateToTaskDetails(task, true, true);
                                                    else navigateToTaskDetails(task, true, false);
                                                }
                                            }
                                        }
                                    } else {
                                        Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<TasksList> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in TaskListByTaskId? API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in TaskListByTaskId? API:");
                ex.getMessage();
            }
        }
    }

    private void syncNotification() {
        if (!isSyncInProgress) {
           /* mSyncNotificationAsyncTask = new SyncNotificationAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
            mSyncNotificationAsyncTask.execute();*/
            isSyncInProgress = true;
            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    Call<Notification> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getsyncNotification(userID);
                    call.enqueue(new retrofit2.Callback<Notification>() {
                        @Override
                        public void onResponse(Call<Notification> call, Response<Notification> response) {
                            try {
                                if (response.code() == 200) {
                                    Notification notification = response.body();
                                    if (notification.getCode().equalsIgnoreCase("200") && notification.getMessage().equalsIgnoreCase("Success")) {
                                        FWLogger.logInfo(TAG, "200");
                                        if (notification.getResultData() != null) {
                                            if (mResultData != null && mResultData.size() > 0) {
                                                FWLogger.logInfo(TAG, "add to existing old result data size= " + mResultData.size());
                                                FWLogger.logInfo(TAG, "add to existing new result data size= " + notification.getResultData().size());
                                                //Add to existing list on receive from sync
                                                if (mNotificationListAdapter != null)
                                                    mNotificationListAdapter.updateItems(notification.getResultData());
                                                clearBadge();
                                            } else {
                                                FWLogger.logInfo(TAG, "add to new list");
                                                setNotificationAdapterData(notification);
                                                clearBadge();
                                            }
                                        }
                                    } else {
                                        Toast.makeText(getContext(), notification.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                    isSyncInProgress = false;
                                }

                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<Notification> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in AllUsersList API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in AllUsersList API:");
                ex.getMessage();
            }
        }
    }

    private void syncIsRead(UpdateNotificationIsRead updateNotificationIsRead) {
        if (getContext() == null) FWLogger.logInfo(TAG, "getContext is null");
        mUpdateNotificationIsReadAsyncTask = new UpdateNotificationIsReadAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUpdateNotificationIsReadAsyncTask.execute(updateNotificationIsRead);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
/*            if (classType.equalsIgnoreCase(Notification.class.getSimpleName())) {
                Gson gson = new Gson();
                Notification notification = gson.fromJson(urlConnectionResponse.resultData, Notification.class);
                if (notification.getCode().equalsIgnoreCase("200") && notification.getMessage().equalsIgnoreCase("Success")) {
                    FWLogger.logInfo(TAG, "200");
                    if (notification.getResultData() != null) {
                        if (mResultData != null && mResultData.size() > 0) {
                            FWLogger.logInfo(TAG, "add to existing old result data size= " + mResultData.size());
                            FWLogger.logInfo(TAG, "add to existing new result data size= " + notification.getResultData().size());
                            //Add to existing list on receive from sync
                            if (mNotificationListAdapter != null)
                                mNotificationListAdapter.updateItems(notification.getResultData());
                            clearBadge();
                        } else {
                            FWLogger.logInfo(TAG, "add to new list");
                            setNotificationAdapterData(gson, urlConnectionResponse);
                            clearBadge();
                        }
                    }
                } else {
                    Toast.makeText(getContext(), notification.getMessage(), Toast.LENGTH_LONG).show();
                }
                isSyncInProgress = false;
            }*/
            if (classType.equalsIgnoreCase(TasksList.class.getSimpleName())) {
                Gson gson = new Gson();
                TasksList tasksList = gson.fromJson(urlConnectionResponse.resultData, TasksList.class);
                if (tasksList.getCode().equalsIgnoreCase("200") && tasksList.getMessage().equalsIgnoreCase("success")) {
                    if (tasksList != null) {
                        TasksList.ResultData task = tasksList.getResultData().get(0);
                        FWLogger.logInfo(TAG, "success");
                        UpdateNotificationIsRead updateNotificationIsRead = setUpdateNotificationIsReadData(task);
                        syncIsRead(updateNotificationIsRead);
                        if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.re_assign))) {
                            if (task != null)
                                reAssignTask(task);
                        } else if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.task))) {
                            //First check user is tech or owner

                            //Owner Condition :
                            //if owner then navigate to respective screen as per the task status (Completed,Rejected,Ongoing/Inactive)
                            //1. Completed : Navigate to TaskDetailsFragment without FromRejectedList (Without Reassign functionality)
                            //2. Rejected : Navigate to TaskDetailsFragment with FromRejectedList (With Reassign functionality)
                            //3. Ongoing/Inactive : Navigate to OwnerTaskTrackingFragment
                            //4.NotificationType.TYPE_EARNINGS navigate to Passbook Fragment

                            //Tech Condition :
                            //if tech then navigate to respective screen as per the task status (Completed,Ongoing/Inactive)
                            //1. Completed : Navigate to TaskDetailsFragment without FromRejectedList (Without Reassign functionality)
                            //2. Rejected : No Navigation
                            //3. Inactive : Navigate to TechTaskTrackingFragment for StartTaskTechFragment
                            //4. Ongoing with TaskState.ENDED_NO_PAYMENT and RATE : Navigate to TechPaymentReceivedFragment
                            //5.NotificationType.TYPE_EARNINGS navigate to Passbook Fragment

                            int taskStateId = task.getTaskState();
                            String taskMode = task.getPaymentMode();
                            int taskStatusId = task.getTaskStatusId();
                            if (SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                                if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_TASK)) {
                                    if (taskStatusId == TaskStatus.COMPLETED) {
                                        navigateToTaskDetails(task, true, true);
                                    } else if (taskStatusId == TaskStatus.REJECTED) {
                                        navigateToTaskDetails(task, true, false);
                                    } else if (taskStatusId == TaskStatus.ON_GOING) {
                                        navigateToOwnerTaskTrackingFragment(task);
                                    }
                                } else if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_EARNINGS)) {
                                    navigateToHomePassbookFragment();
                                }
                            } else if (SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                                if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_TASK)) {
                                    if (taskStatusId == TaskStatus.COMPLETED) {
                                        //navigateToTaskDetails(task, false);
                                    } else if (taskStatusId == TaskStatus.IN_ACTIVE) {
//                                        navigateToTechTaskTrackingFragment(task, 0);
                                        TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
                                        taskDialogNew.acceptTaskDialog(getActivity(), task, TaskStatus.IN_ACTIVE);
                                    } else if (taskStatusId == TaskStatus.ON_GOING) {
                                        if (taskMode != null && taskStateId == TaskState.ENDED_NO_PAYMENT && taskMode.equalsIgnoreCase(Constant.PaymentMode.RATE_STRING)
                                                && task.getPaymentModeId() == Constant.PaymentMode.RATE_ID && task.isTaskClosureStatus() == true) {
                                            navigateToTechPaymentReceivedFragment(task);
                                        } else {
                                            navigateToTaskClosure(task);
                                        }
                                    }
                                } else if (mNotificationType.equalsIgnoreCase(Constant.NotificationType.TYPE_EARNINGS)) {
                                    navigateToHomePassbookFragment();
                                }
                            }

                        } else if (mNavigateFor.equalsIgnoreCase(getContext().getString(R.string.detail))) {
                            if (task != null) {
                                if (task.getTaskStatusId() == TaskStatus.COMPLETED)
                                    navigateToTaskDetails(task, true, true);
                                else navigateToTaskDetails(task, true, false);
                            }
                        }
                    }
                } else {
                    Toast.makeText(getContext(), tasksList.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else if (classType.equalsIgnoreCase(UpdateNotificationIsRead.class.getSimpleName())) {
                Gson gson = new Gson();
                UpdateNotificationIsRead updateNotificationIsRead = gson.fromJson(urlConnectionResponse.resultData, UpdateNotificationIsRead.class);
                if (updateNotificationIsRead.getCode().equalsIgnoreCase("200") && updateNotificationIsRead.getMessage().equalsIgnoreCase("IsRead added successfully.")) {
                    // Update clicked notification row UI
                    FWLogger.logInfo(TAG, "Notification is read true");
                    syncNotification();
                }
            }
        }
    }

/*
    private void setNotificationAdapterData(Gson gson, URLConnectionResponse urlConnectionResponse) {
        //Add to result data at first time
        mResultData = new ArrayList<>();
        mResultData = gson.fromJson(urlConnectionResponse.resultData, Notification.class).getResultData();
        mNotificationListAdapter = new NotificationListAdapter(getContext(), 0, mResultData, this);
        mNotificationListAdapter.setClickListener(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewNotification.setLayoutManager(mLayoutManager);
        mRecyclerViewNotification.setAdapter(mNotificationListAdapter);
        FWLogger.logInfo(TAG, "mResultData");
    }
*/

    private void setNotificationAdapterData(Notification notification) {
        //Add to result data at first time
        mResultData = new ArrayList<>();
        mResultData = notification.getResultData();
        // mResultData = gson.fromJson(urlConnectionResponse.resultData, Notification.class).getResultData();
        mNotificationListAdapter = new NotificationListAdapter(getContext(), 0, mResultData, this);
        mNotificationListAdapter.setClickListener(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewNotification.setLayoutManager(mLayoutManager);
        mRecyclerViewNotification.setAdapter(mNotificationListAdapter);
        FWLogger.logInfo(TAG, "mResultData");
    }

    private void clearBadge() {
        Constant.UNREAD_COUNT = 0;
        Intent unreadCountIntent = new Intent(Constant.FCM.UNREAD_FOREGROUND);
        unreadCountIntent.putExtra(Constant.FCM.CLEAR_BADGE, Constant.FCM.CLEAR_BADGE);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(unreadCountIntent);
        NotificationUtils.clearNotifications(getContext());
    }

    private Bundle getBundle(TasksList.ResultData data) {
        Bundle bundle = new Bundle();
        if (data != null) {
            //for tech and owner
            bundle.putString("Task Status", data.getTaskStatus());
            bundle.putString("Task Name", data.getName());
            bundle.putString("Task Assignee", data.getAssignedTo());
            bundle.putInt("Task Amount", data.getWagesPerHours());
            bundle.putString("Image", "a");
            bundle.putString("FieldLat", data.getLatitude());
            bundle.putString("FieldLong", data.getLongitude());
            bundle.putString("TaskDescription", data.getDescription());
            bundle.putString("AddressDesc", data.getLocationDesc());
            bundle.putString("ShortAddress", data.getLocationName());
            bundle.putString("ContactNo", data.getContactNo());
            bundle.putString("CustomerName", data.getCustomerName());
            bundle.putInt("TaskId", data.getId());
            bundle.putString("Task Type", data.getTaskType());
            bundle.putInt("TaskTypeId", data.getTaskTypeId());
            bundle.putInt("TaskStatusId", data.getTaskStatusId());
            bundle.putLong("StartDate", data.getStartDate());
            bundle.putLong("EndDate", data.getEndDate());
            bundle.putInt("TaskState", data.getTaskState());
            bundle.putString("FullAddress", data.getFullAddress());
            bundle.putString("PaymentMode", data.getPaymentMode());
            bundle.putInt("PaymentModeId", data.getPaymentModeId());
            bundle.putString("TaskDate", data.getTaskDate());
            //owner specific
            bundle.putString("Photo", data.getPhoto());
            bundle.putString("TechLat", data.getTechLatitude());
            bundle.putString("TechLong", data.getTechLongitude());
            bundle.putInt("UserId", data.getUserId());
        }
        return bundle;
    }

    @NotNull
    private UpdateNotificationIsRead setUpdateNotificationIsReadData(TasksList.ResultData task) {
        UpdateNotificationIsRead updateNotificationIsRead = new UpdateNotificationIsRead();
        updateNotificationIsRead.setTaskId(task.getId());
        updateNotificationIsRead.setRead(true);
        updateNotificationIsRead.setUserId(SharedPrefManager.getInstance(getContext()).getUserId());
        if (SharedPrefManager.getInstance(getContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            updateNotificationIsRead.setOwnerId(task.getOwnerId());
        }
        updateNotificationIsRead.setNotificationType(mNotificationType);
        updateNotificationIsRead.setTaskStatus(task.getTaskStatusId());
        updateNotificationIsRead.setTaskType(task.getTaskTypeId());
        if (task.getPaymentNotReceived() != null) {
            updateNotificationIsRead.setPaymentNotReceived(task.getPaymentNotReceived());
        }
        updateNotificationIsRead.setTaskState(task.getTaskState());
        updateNotificationIsRead.setPaymentMode(task.getPaymentModeId());
        updateNotificationIsRead.setTaskClosureStatus(task.isTaskClosureStatus());
        return updateNotificationIsRead;
    }

    private void navigateToOwnerTaskTrackingFragment(TasksList.ResultData data) {
        OwnerTaskTrackingFragmentNew ownerTaskTrackingFragment = new OwnerTaskTrackingFragmentNew();
        getBundle(data).putBoolean("navigateToTaskTab", false);
        ownerTaskTrackingFragment.setArguments(getBundle(data));
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, ownerTaskTrackingFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /*private void navigateToTechTaskTrackingFragment(TasksList.ResultData data, int pageIndex) {
        TechTaskTrackingFragment techTaskTrackingFragment = new TechTaskTrackingFragment();
//        MainTaskFragmentNew techTaskTrackingFragment = new MainTaskFragmentNew();
        getBundle(data).putBoolean("navigateToTaskTab", false);
        getBundle(data).putInt("pageIndex", pageIndex);
        techTaskTrackingFragment.setArguments(getBundle(data));
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, techTaskTrackingFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }*/

    private void navigateToTaskClosure(TasksList.ResultData data) {
        Bundle bundle = new Bundle();
        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
        bundle.putSerializable("taskResult", data);
        taskClosureFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, taskClosureFragment);
        transaction.commit();
    }

    private void navigateToTechPaymentReceivedFragment(TasksList.ResultData data) {
        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
        /*getBundle(data).putBoolean("navigateToTaskTab", false);
        techPaymentReceivedFragment.setArguments(getBundle(data));*/

        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", data);
        bundle.putBoolean("navigateToTaskTab", false);
        techPaymentReceivedFragment.setArguments(bundle);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, techPaymentReceivedFragment);
        /*transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);*/
        transaction.commit();
    }

    private void navigateToTaskDetails(TasksList.ResultData resultData, boolean allowToReassign, boolean allowToDownloadReport) {
        FWLogger.logInfo(TAG, "navigateToTaskDetails");
        TaskDetailsFragmentNew taskDetailsFragment = new TaskDetailsFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putBoolean("downloadReport", allowToDownloadReport);
        bundle.putSerializable("taskResult", resultData);
        bundle.putBoolean("allowToReassign", allowToReassign);
        taskDetailsFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, taskDetailsFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void navigateToAMCDetailsFragment(int AMCsId, int AMCServiceDetailsId, String AMCTypeName) {
        FragmentManager fragmentManager = getFragmentManager();
        AMCDetailsFragment amcDetailsFragment = new AMCDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("AMCsId", AMCsId);
        args.putInt("AMCServiceDetailsId", AMCServiceDetailsId);
        args.putString("AMCTypeName", AMCTypeName);
        amcDetailsFragment.setArguments(args);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.home_fragment_container, amcDetailsFragment, "AMC Details");
        fragmentTransaction.isAddToBackStackAllowed();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void navigateToHomePassbookFragment() {
        /*HomePassbookFragment homePassbookFragment = new HomePassbookFragment();*/
        HomePassbookFragmentNew homePassbookFragment = new HomePassbookFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putInt("Id", SharedPrefManager.getInstance(getContext()).getUserId());
        homePassbookFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_container, homePassbookFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void navigateToProfileFragment(Notification.ResultData mUserDetails) {
        ProfileFragmentNew technicianDetails = new ProfileFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("profileData", mUserDetails);
        bundle.putString("screen", "techList");
        technicianDetails.setArguments(bundle);
        /*Bundle bundle = new Bundle();
        bundle.putInt("Id", id);
        technicianDetails.setArguments(bundle);*/
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        HomeActivityNew.isOwner = false;
        transaction.add(R.id.home_fragment_container, technicianDetails);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onNewNotification() {
        FWLogger.logInfo(TAG, "onNewNotification");
        syncNotification();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Constant.UNREAD_COUNT = 0;
        Intent unreadCountIntent = new Intent(Constant.FCM.UNREAD_FOREGROUND);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(unreadCountIntent);
        NotificationUtils.clearNotifications(getContext());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUpdateNotificationIsReadAsyncTask != null) {
            mUpdateNotificationIsReadAsyncTask.cancel(true);
        }
        if (mRecyclerViewNotification != null) {
            mRecyclerViewNotification = null;
        }
        if (mNotificationListAdapter != null) {
            mNotificationListAdapter = null;
        }
    }
}
