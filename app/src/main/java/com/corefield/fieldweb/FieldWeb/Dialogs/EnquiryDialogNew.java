package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.corefield.fieldweb.DTO.CRMTask.AddCustDTO;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Enquiry.AddEnquiry;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.DTO.Enquiry.ServiceType;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.FieldWeb.Admin.CRMFragment;
import com.corefield.fieldweb.FieldWeb.Admin.CustomerListFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddEnquiryActivity;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class EnquiryDialogNew {
    String mTempLocName = "";
    double mTempLat = 0, mTempLong = 0;
    Place mTempPlace = null;
    int serviceId = 0;
    public final String TAG = EnquiryDialogNew.class.getSimpleName();

    private static EnquiryDialogNew mEnquiryDialog;
    private String mName, mNumber;
    EditText mEditTextCustName, mEditTextCustNumber;
    EditText mtxtCustName, mtxtCustNumber, mtxtCustNumberSec;

    /**
     * The private constructor for the EnquiryDialog Singleton class
     */
    private EnquiryDialogNew() {
    }

    public static EnquiryDialogNew getInstance() {
        //instantiate a new EnquiryDialog if we didn't instantiate one yet
        if (mEnquiryDialog == null) {
            mEnquiryDialog = new EnquiryDialogNew();
        }
        return mEnquiryDialog;
    }

    //    public void addEnquiryDialog(Activity activity, ArrayList<String> listOfServiceType, ArrayList<String> listOfReferenceType) {
    public void addEnquiryDialog(Activity activity, List<EnquiryServiceTypeDTO.ResultData> listOfServiceType/*List<ServiceType.ResultData> listOfServiceType*//*, ArrayList<String> listOfReferenceType*/) {

        ///////////////////////////////////////Behaviour of Dialog/////////////////////////////////////////////////////////
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_enquiry_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        ArrayList<String> mServiceTypeList = new ArrayList<>();
        EnquiryServiceTypeDTO.ResultData data;

        //Button
        Button buttonSaveEnq = dialog.findViewById(R.id.button_add);
        Button buttonCancelEnq = dialog.findViewById(R.id.button_cancel);
        //Spinner
        //Spinner serviceTypeForEnquiry = dialog.findViewById(R.id.enquiry_spin_services);
        SearchableSpinner serviceTypeForEnquiry = (SearchableSpinner) dialog.findViewById(R.id.enquiry_spin_services);

        //Edit Text
        EditText editTextDatePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_date);
        EditText editTextTimePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_time);
        EditText editTextCustAddress = dialog.findViewById(R.id.enquiry_edittext_customer_address);
        EditText editTextCustLandmark = dialog.findViewById(R.id.enquiry_edittext_customer_landmark);
        EditText editTextCustName = dialog.findViewById(R.id.enquiry_edittext_customer_name);
        mEditTextCustName = editTextCustName;
        EditText editTextCustNumber = dialog.findViewById(R.id.enquiry_edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextTechProblem = dialog.findViewById(R.id.enquiry_edittext_tech_problem);
        EditText editTextSpecialInstructions = dialog.findViewById(R.id.enquiry_edittext_special_instructions);
        ImageView closeAddEnquiry = dialog.findViewById(R.id.closeAddEnquiry);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        TextView hAddEnqVideo = dialog.findViewById(R.id.textView_how_to_add_enquiry);

        //Set service type spinner
//        listOfServiceType.add(activity.getString(R.string.select_service_type));
        /*ServiceType.ResultData data = new ServiceType.ResultData();*/
        data = new EnquiryServiceTypeDTO.ResultData();
        data.setId(0);
        data.setServiceName(activity.getString(R.string.select_service_type));
        listOfServiceType.add(data);

        for (EnquiryServiceTypeDTO.ResultData serviceResultList : listOfServiceType) {
            mServiceTypeList.add(serviceResultList.getServiceName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mServiceTypeList);
        mServiceTypeList.clear();
        mServiceTypeList.addAll(set);

        closeAddEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        hAddEnqVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddEnquiryActivity.class);
                activity.startActivity(send);
            }
        });
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentEnquiry();
            }
        });

        ArrayAdapter<String> arrayAdapterServiceType = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mServiceTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterServiceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTypeForEnquiry.setAdapter(arrayAdapterServiceType);
        serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount());
        /* serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount() - 1);*/


        serviceTypeForEnquiry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfServiceType.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfServiceType.get(i).getServiceName())) {
                        serviceId = listOfServiceType.get(i).getId();
                    }
                }
                //serviceId = listOfServiceType.get(position).getId();
                FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //--------------To set default Urgent date time--------------
        currentDatePicker(editTextDatePicker);
        //String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        editTextTimePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        editTextDatePicker.setEnabled(true);
        //----------------------------------------------------------

        /////////////////////////////////////////Save enquiry form/////////////////////////////////////////////////////////
        buttonSaveEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editTextCustName)) {
                    editTextCustName.setError(activity.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(editTextCustName) && (Character.isWhitespace(editTextCustName.getText().toString().charAt(0)) || editTextCustName.getText().toString().trim().isEmpty())) {
                    editTextCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1 &&
                        isValidPhone(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } /*else if (taskTypeForEnquiry.getSelectedItem() == activity.getResources().getString(R.string.select_task_type)) {
                    ((TextView) taskTypeForEnquiry.getSelectedView()).setError(activity.getResources().getString(R.string.please_select_option));
                }*/ else if (isEmpty(editTextDatePicker)) {
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextTimePicker)) {
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_time));
                } else if (isEmpty(editTextCustAddress)) {
                    editTextCustAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextCustLandmark)) {
                    editTextCustLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextCustLandmark) && (Character.isWhitespace(editTextCustLandmark.getText().toString().charAt(0)) || editTextCustLandmark.getText().toString().trim().isEmpty())) {
                    editTextCustLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (serviceTypeForEnquiry.getSelectedItem() == activity.getString(R.string.select_service_type)) {
                    ((TextView) serviceTypeForEnquiry.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (!isEmpty(editTextTechProblem) && (Character.isWhitespace(editTextTechProblem.getText().toString().charAt(0)) || editTextTechProblem.getText().toString().trim().isEmpty())) {
                    editTextTechProblem.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(editTextSpecialInstructions) && (Character.isWhitespace(editTextSpecialInstructions.getText().toString().charAt(0)) || editTextSpecialInstructions.getText().toString().trim().isEmpty())) {
                    editTextSpecialInstructions.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    //Create Enquiry Object
                    AddEnquiry.ResultData addEnquiry = new AddEnquiry.ResultData();
                    addEnquiry.setCustomerName(editTextCustName.getText().toString());
                    addEnquiry.setMobileNumber(editTextCustNumber.getText().toString());
                    addEnquiry.setPreferableDate(editTextDatePicker.getText().toString());
                    addEnquiry.setPreferableTime(editTextTimePicker.getText().toString());
                    addEnquiry.setAddress(editTextCustAddress.getText().toString());
                    addEnquiry.setLocDescription(editTextCustLandmark.getText().toString());
                    addEnquiry.setLatitude(String.valueOf(mTempLat));
                    addEnquiry.setLongitude(String.valueOf(mTempLong));
                    addEnquiry.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));
                    addEnquiry.setLocName(mTempLocName);
//                    addEnquiry.setServicesId(serviceTypeForEnquiry.getSelectedItemPosition() + 1);//TODO Need to verify this through add enquiry API field when enquiry added
                    FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
                    addEnquiry.setServicesId(serviceId);//TODO Need to verify this through add enquiry API field when enquiry added
                    addEnquiry.setTechnicalProblem(editTextTechProblem.getText().toString());
                    addEnquiry.setTechnicalNote(editTextSpecialInstructions.getText().toString());
                    addEnquiry.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setOwnerId(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setInquaryState(1);
                    addEnquiry.setIsActive(true);

                    //Call to API
                    ((HomeActivityNew) activity).saveAddEnquiry(addEnquiry);

                    //Remove hint (place holder object)from the list
                    removeTypes(activity, arrayAdapterServiceType, dialog);
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        /////////////////////////////////////////Cancel enquiry form/////////////////////////////////////////////////////////
        buttonCancelEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeTypes(activity, arrayAdapterServiceType, dialog);
            }
        });

        /////////////////////////////////////////Date picker on enquiry form/////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        /////////////////////////////////////////Time picker on enquiry form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        /////////////////////////////////////////Address on enquiry form/////////////////////////////////////////////////////////
        editTextCustAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextCustAddress.setError(null);
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        /////////////////////////////////////////Update enquiry form by select place listener /////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    editTextCustAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }


    public void addEnquiryDialogForService(Activity activity, int serviceId, String serviceName) {

        ///////////////////////////////////////Behaviour of Dialog/////////////////////////////////////////////////////////
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_enquiry_for_services);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        ArrayList<String> mServiceTypeList = new ArrayList<>();
        EnquiryServiceTypeDTO.ResultData data;

        //Button
        Button buttonSaveEnq = dialog.findViewById(R.id.button_add);
        Button buttonCancelEnq = dialog.findViewById(R.id.button_cancel);
        //Spinner
        EditText serviceTypeForEnquiry = dialog.findViewById(R.id.enquiry_spin_services);

        //Edit Text
        EditText editTextDatePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_date);
        EditText editTextTimePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_time);
        EditText editTextCustAddress = dialog.findViewById(R.id.enquiry_edittext_customer_address);
        EditText editTextCustLandmark = dialog.findViewById(R.id.enquiry_edittext_customer_landmark);
        EditText editTextCustName = dialog.findViewById(R.id.enquiry_edittext_customer_name);
        mEditTextCustName = editTextCustName;
        EditText editTextCustNumber = dialog.findViewById(R.id.enquiry_edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextTechProblem = dialog.findViewById(R.id.enquiry_edittext_tech_problem);
        EditText editTextSpecialInstructions = dialog.findViewById(R.id.enquiry_edittext_special_instructions);
        ImageView closeAddEnquiry = dialog.findViewById(R.id.closeAddEnquiry);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        TextView hAddEnqVideo = dialog.findViewById(R.id.textView_how_to_add_enquiry);

        //Set service type spinner
//        listOfServiceType.add(activity.getString(R.string.select_service_type));
        /*ServiceType.ResultData data = new ServiceType.ResultData();*/
       /* data = new EnquiryServiceTypeDTO.ResultData();
        data.setId(0);
        data.setServiceName(activity.getString(R.string.select_service_type));
        listOfServiceType.add(data);

        for (EnquiryServiceTypeDTO.ResultData serviceResultList : listOfServiceType) {
            mServiceTypeList.add(serviceResultList.getServiceName());
        }*/

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
       /* Set<String> set = new HashSet<>(mServiceTypeList);
        mServiceTypeList.clear();
        mServiceTypeList.addAll(set);*/

        try {
            if (serviceId != 0 && !serviceName.isEmpty()) {
                serviceTypeForEnquiry.setText(serviceName);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        closeAddEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        hAddEnqVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddEnquiryActivity.class);
                activity.startActivity(send);
            }
        });

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentEnquiryForService();
            }
        });

       /* ArrayAdapter<String> arrayAdapterServiceType = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mServiceTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterServiceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTypeForEnquiry.setAdapter(arrayAdapterServiceType);
        *//*serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount());*//*
        serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount() - 1);*/


      /*  serviceTypeForEnquiry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfServiceType.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfServiceType.get(i).getServiceName())) {
                        serviceId = listOfServiceType.get(i).getId();
                    }
                }
                //serviceId = listOfServiceType.get(position).getId();
                FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        //--------------To set default Urgent date time--------------
        currentDatePicker(editTextDatePicker);
        //String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        editTextTimePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        editTextDatePicker.setEnabled(true);
        //----------------------------------------------------------

        /////////////////////////////////////////Save enquiry form/////////////////////////////////////////////////////////
        buttonSaveEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editTextCustName)) {
                    editTextCustName.setError(activity.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(editTextCustName) && (Character.isWhitespace(editTextCustName.getText().toString().charAt(0)) || editTextCustName.getText().toString().trim().isEmpty())) {
                    editTextCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1 &&
                        isValidPhone(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } /*else if (taskTypeForEnquiry.getSelectedItem() == activity.getResources().getString(R.string.select_task_type)) {
                    ((TextView) taskTypeForEnquiry.getSelectedView()).setError(activity.getResources().getString(R.string.please_select_option));
                }*/ else if (isEmpty(editTextDatePicker)) {
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextTimePicker)) {
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_time));
                } else if (isEmpty(editTextCustAddress)) {
                    editTextCustAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextCustLandmark)) {
                    editTextCustLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextCustLandmark) && (Character.isWhitespace(editTextCustLandmark.getText().toString().charAt(0)) || editTextCustLandmark.getText().toString().trim().isEmpty())) {
                    editTextCustLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } /*else if (serviceTypeForEnquiry.getSelectedItem() == activity.getString(R.string.select_service_type)) {
                    ((TextView) serviceTypeForEnquiry.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                }*/ else if (!isEmpty(editTextTechProblem) && (Character.isWhitespace(editTextTechProblem.getText().toString().charAt(0)) || editTextTechProblem.getText().toString().trim().isEmpty())) {
                    editTextTechProblem.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(editTextSpecialInstructions) && (Character.isWhitespace(editTextSpecialInstructions.getText().toString().charAt(0)) || editTextSpecialInstructions.getText().toString().trim().isEmpty())) {
                    editTextSpecialInstructions.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    //Create Enquiry Object
                    AddEnquiry.ResultData addEnquiry = new AddEnquiry.ResultData();
                    addEnquiry.setCustomerName(editTextCustName.getText().toString());
                    addEnquiry.setMobileNumber(editTextCustNumber.getText().toString());
                    addEnquiry.setPreferableDate(editTextDatePicker.getText().toString());
                    addEnquiry.setPreferableTime(editTextTimePicker.getText().toString());
                    addEnquiry.setAddress(editTextCustAddress.getText().toString());
                    addEnquiry.setLocDescription(editTextCustLandmark.getText().toString());
                    addEnquiry.setLatitude(String.valueOf(mTempLat));
                    addEnquiry.setLongitude(String.valueOf(mTempLong));
                    addEnquiry.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));
                    addEnquiry.setLocName(mTempLocName);
//                    addEnquiry.setServicesId(serviceTypeForEnquiry.getSelectedItemPosition() + 1);//TODO Need to verify this through add enquiry API field when enquiry added
                    FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
                    addEnquiry.setServicesId(serviceId);//TODO Need to verify this through add enquiry API field when enquiry added
                    addEnquiry.setTechnicalProblem(editTextTechProblem.getText().toString());
                    addEnquiry.setTechnicalNote(editTextSpecialInstructions.getText().toString());
                    addEnquiry.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setOwnerId(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setInquaryState(1);
                    addEnquiry.setIsActive(true);

                    //Call to API
                    ((HomeActivityNew) activity).saveAddEnquiry(addEnquiry);

                    dialog.dismiss();

                    //Remove hint (place holder object)from the list
                    //removeTypes(activity, arrayAdapterServiceType, dialog);
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        /////////////////////////////////////////Cancel enquiry form/////////////////////////////////////////////////////////
        buttonCancelEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // removeTypes(activity, arrayAdapterServiceType, dialog);
                dialog.dismiss();
            }
        });

        /////////////////////////////////////////Date picker on enquiry form/////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        /////////////////////////////////////////Time picker on enquiry form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        /////////////////////////////////////////Address on enquiry form/////////////////////////////////////////////////////////
        editTextCustAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextCustAddress.setError(null);
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        /////////////////////////////////////////Update enquiry form by select place listener /////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    editTextCustAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }


    public static void currentDatePicker(EditText editText) {
        Date currentTime = Calendar.getInstance().getTime();
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText.setText(sdf.format(currentTime.getTime()));
    }

    private void removeTypes(Activity activity, ArrayAdapter<String> arrayAdapterServiceType, FWDialog dialog) {
        arrayAdapterServiceType.remove(activity.getString(R.string.select_service_type));
        dialog.dismiss();
    }

    public void enquiryDetailsDialog(Activity activity, EnquiryList.ResultData mResultData) {

        ///////////////////////////////////////Behaviour of Dialog/////////////////////////////////////////////////////////
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_enquiry_details);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView textViewEnqDate, textViewEnqDetailsNo, textViewEnqCustName, textViewEnqCustContact,
                textViewEnqAddress, textViewEnqTaskType, textViewEnqTechProblem, textViewEnqTechNote,
                textViewEnqReference, textViewEnqServiceName;
        Button buttonCreateTask, buttonCancel;
        ImageView imageViewCall, imageViewMessage;

        textViewEnqAddress = dialog.findViewById(R.id.enquiry_details_address);
        textViewEnqCustContact = dialog.findViewById(R.id.enquiry_details_cust_contact);
        textViewEnqCustName = dialog.findViewById(R.id.enquiry_details_cust_name);
        textViewEnqDate = dialog.findViewById(R.id.enquiry_details_date);
        textViewEnqDetailsNo = dialog.findViewById(R.id.enquiry_details_number);
        textViewEnqReference = dialog.findViewById(R.id.enquiry_details_reference);
        textViewEnqTechNote = dialog.findViewById(R.id.enquiry_details_tech_note);
        textViewEnqTaskType = dialog.findViewById(R.id.enquiry_details_task_type);
        textViewEnqTechProblem = dialog.findViewById(R.id.enquiry_details_tech_problem);
        textViewEnqServiceName = dialog.findViewById(R.id.enquiry_details_service_name);
        imageViewCall = dialog.findViewById(R.id.imageView_call);
        imageViewMessage = dialog.findViewById(R.id.imageView_message);

        buttonCreateTask = dialog.findViewById(R.id.enquiry_details_create_task);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        if (mResultData.getAddress() != null)
            textViewEnqAddress.setText(mResultData.getAddress());
        if (mResultData.getMobileNumber() != null)
            textViewEnqCustContact.setText(mResultData.getMobileNumber());

        if (mResultData.getCustomerName() == null || mResultData.getCustomerName().isEmpty() || mResultData.getCustomerName().equalsIgnoreCase(""))
            textViewEnqCustName.setText(activity.getResources().getString(R.string.na));
        else textViewEnqCustName.setText(mResultData.getCustomerName());

        if (mResultData.getPrefDate() != null)
            textViewEnqDate.setText(DateUtils.convertDateFormat(mResultData.getPrefDate(), "yyyy-MM-dd", "dd-MM-yyyy"));
        if (mResultData.getEnquiryId() != 0)
            textViewEnqDetailsNo.setText("#" + mResultData.getEnquiryNo());
        if (mResultData.getReferenceName() != null)
            textViewEnqReference.setText(mResultData.getReferenceName());

        if (mResultData.getTechnicalNote() == null || mResultData.getTechnicalNote().isEmpty() || mResultData.getTechnicalNote().equalsIgnoreCase(""))
            textViewEnqTechNote.setText(activity.getResources().getString(R.string.na));
        else textViewEnqTechNote.setText(mResultData.getTechnicalNote());

        if (mResultData.getTaskTypeName() != null)
            textViewEnqTaskType.setText(mResultData.getTaskTypeName());

        if (mResultData.getTechnicalProblem() == null || mResultData.getTechnicalProblem().isEmpty() || mResultData.getTechnicalProblem().equalsIgnoreCase(""))
            textViewEnqTechProblem.setText(activity.getResources().getString(R.string.na));
        else textViewEnqTechProblem.setText(mResultData.getTechnicalProblem());

        if (mResultData.getServiceName() != null)
            textViewEnqServiceName.setText(mResultData.getServiceName());

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResultData.getMobileNumber() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mResultData.getMobileNumber()));
                    activity.startActivity(intent);
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
            }
        });

        imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResultData.getMobileNumber() != null) {
                    try {
                        String number = mResultData.getMobileNumber();  // The number on which you want to send SMS
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(activity, R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonCreateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity != null) {
                    dialog.dismiss();
                    ((HomeActivityNew) activity).addTaskWithEnquiryDefaults(mResultData);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void updateEnquiryDialog(Activity activity, List<EnquiryServiceTypeDTO.ResultData> listOfServiceType, EnquiryList.ResultData resultData, Fragment fragment) {
        ///////////////////////////////////////Behaviour of Dialog/////////////////////////////////////////////////////////
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_enquiry_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        ArrayList<String> mServiceTypeList = new ArrayList<>();

        //Button
        Button buttonSaveEnq = dialog.findViewById(R.id.button_add);
        Button buttonCancelEnq = dialog.findViewById(R.id.button_cancel);
        //Spinner
        SearchableSpinner serviceTypeForEnquiry = (SearchableSpinner) dialog.findViewById(R.id.enquiry_spin_services);

        //Edit Text
        EditText editTextDatePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_date);
        EditText editTextTimePicker = dialog.findViewById(R.id.enquiry_edittext_preferable_time);
        EditText editTextCustAddress = dialog.findViewById(R.id.enquiry_edittext_customer_address);
        EditText editTextCustLandmark = dialog.findViewById(R.id.enquiry_edittext_customer_landmark);
       /* EditText editTextCustName = dialog.findViewById(R.id.enquiry_edittext_customer_name);
        EditText editTextCustNumber = dialog.findViewById(R.id.enquiry_edittext_customer_number);*/
        EditText editTextCustName = dialog.findViewById(R.id.enquiry_edittext_customer_name);
        mEditTextCustName = editTextCustName;
        EditText editTextCustNumber = dialog.findViewById(R.id.enquiry_edittext_customer_number);
        mEditTextCustNumber = editTextCustNumber;
        EditText editTextTechProblem = dialog.findViewById(R.id.enquiry_edittext_tech_problem);
        EditText editTextSpecialInstructions = dialog.findViewById(R.id.enquiry_edittext_special_instructions);

        ImageView closeAddEnquiry = dialog.findViewById(R.id.closeAddEnquiry);
        buttonSaveEnq.setText(activity.getResources().getString(R.string.update));
        //
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);
        //Set service type spinner
//        listOfServiceType.add(activity.getString(R.string.select_service_type));
        EnquiryServiceTypeDTO.ResultData data = new EnquiryServiceTypeDTO.ResultData();
        data.setId(0);
        data.setServiceName(activity.getString(R.string.select_service_type));
        listOfServiceType.add(data);

        for (EnquiryServiceTypeDTO.ResultData serviceResultList : listOfServiceType) {
            mServiceTypeList.add(serviceResultList.getServiceName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mServiceTypeList);
        mServiceTypeList.clear();
        mServiceTypeList.addAll(set);

        ArrayAdapter<String> arrayAdapterServiceType = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, mServiceTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterServiceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTypeForEnquiry.setAdapter(arrayAdapterServiceType);
        serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount());

        serviceTypeForEnquiry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*serviceId = listOfServiceType.get(position).getId();*/
                for (int i = 0; i < listOfServiceType.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfServiceType.get(i).getServiceName())) {
                        serviceId = listOfServiceType.get(i).getId();
                    }
                }
                FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        closeAddEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeTypes(activity, arrayAdapterServiceType, dialog);
            }
        });

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentEnquiry();
            }
        });

        //--------------To set default Urgent date time--------------
        currentDatePicker(editTextDatePicker);
        //String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        editTextTimePicker.setEnabled(true);
        //editTextTimePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        //editTextDatePicker.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
        editTextDatePicker.setEnabled(true);
        //----------------------------------------------------------

        /*if (resultData.getTaskTypeId() != 0) {
            if (resultData.getTaskTypeId() == 1)
                segmentedGroupTaskType.check(R.id.radiobuttonUrgent);
            else if (resultData.getTaskTypeId() == 2) {
                segmentedGroupTaskType.check(R.id.radiobuttonToday);
//                textInputTimePicker.setEnabled(true);
            } else {
                segmentedGroupTaskType.check(R.id.radiobuttonOther);
//                textInputDatePicker.setEnabled(true);
//                textInputTimePicker.setEnabled(true);
            }
        }*/

        if (resultData.getCustomerName() != null)
            editTextCustName.setText(resultData.getCustomerName());
        if (resultData.getMobileNumber() != null)
            editTextCustNumber.setText(resultData.getMobileNumber());
        if (resultData.getAddress() != null)
            editTextCustAddress.setText(resultData.getAddress());
        if (resultData.getLocDescription() != null)
            editTextCustLandmark.setText(resultData.getLocDescription());
        if (resultData.getTechnicalProblem() != null)
            editTextTechProblem.setText(resultData.getTechnicalProblem());
        if (resultData.getTechnicalNote() != null)
            editTextSpecialInstructions.setText(resultData.getTechnicalNote());
        if (resultData.getServicesId() != 0) {
            String assignedService = null;
            for (int i = 0; i < listOfServiceType.size(); i++) {
                if (resultData.getServicesId() == listOfServiceType.get(i).getId()) {
                    assignedService = listOfServiceType.get(i).getServiceName();
                    serviceId = listOfServiceType.get(i).getId();
                }
            }
            FWLogger.logInfo(TAG, "Service Name : " + assignedService);
            if (assignedService != null) {
                serviceTypeForEnquiry.setSelection(((ArrayAdapter<String>) serviceTypeForEnquiry.getAdapter()).getPosition(assignedService));
            }

        }

        /////////////////////////////////////////Save enquiry form/////////////////////////////////////////////////////////
        buttonSaveEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editTextCustName)) {
                    editTextCustName.setError(activity.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(editTextCustName) && (Character.isWhitespace(editTextCustName.getText().toString().charAt(0)) || editTextCustName.getText().toString().trim().isEmpty())) {
                    editTextCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1 &&
                        isValidPhone(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(editTextCustNumber)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(editTextCustNumber.getText().toString()) == 0)) {
                    editTextCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } /*else if (taskTypeForEnquiry.getSelectedItem() == activity.getResources().getString(R.string.select_task_type)) {
                    ((TextView) taskTypeForEnquiry.getSelectedView()).setError(activity.getResources().getString(R.string.please_select_option));
                }*/ else if (isEmpty(editTextDatePicker)) {
                    editTextDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextTimePicker)) {
                    editTextTimePicker.setError(activity.getString(R.string.please_enter_time));
                } else if (isEmpty(editTextCustAddress)) {
                    editTextCustAddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(editTextCustLandmark)) {
                    editTextCustLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(editTextCustLandmark) && (Character.isWhitespace(editTextCustLandmark.getText().toString().charAt(0)) || editTextCustLandmark.getText().toString().trim().isEmpty())) {
                    editTextCustLandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (serviceTypeForEnquiry.getSelectedItem() == activity.getString(R.string.select_service_type)) {
                    ((TextView) serviceTypeForEnquiry.getSelectedView()).setError(activity.getString(R.string.please_select_option));
                } else if (!isEmpty(editTextTechProblem) && (Character.isWhitespace(editTextTechProblem.getText().toString().charAt(0)) || editTextTechProblem.getText().toString().trim().isEmpty())) {
                    editTextTechProblem.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(editTextSpecialInstructions) && (Character.isWhitespace(editTextSpecialInstructions.getText().toString().charAt(0)) || editTextSpecialInstructions.getText().toString().trim().isEmpty())) {
                    editTextSpecialInstructions.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    //Create Enquiry Object
                    AddEnquiry.ResultData addEnquiry = new AddEnquiry.ResultData();
                    addEnquiry.setEnquiryId(resultData.getEnquiryId());
                    addEnquiry.setCustomerName(editTextCustName.getText().toString());
                    addEnquiry.setMobileNumber(editTextCustNumber.getText().toString());
                    addEnquiry.setPreferableDate(editTextDatePicker.getText().toString());
                    addEnquiry.setPreferableTime(editTextTimePicker.getText().toString());
                    addEnquiry.setAddress(editTextCustAddress.getText().toString());
                    addEnquiry.setLocDescription(editTextCustLandmark.getText().toString());
                    if (mTempLat != 0 && mTempLong != 0) {
                        addEnquiry.setLatitude(String.valueOf(mTempLat));
                        addEnquiry.setLongitude(String.valueOf(mTempLong));
                    } else {
                        addEnquiry.setLatitude(resultData.getLatitude());
                        addEnquiry.setLongitude(resultData.getLongitude());
                    }
                    addEnquiry.setPinCode(MapUtils.getPostalCodeFromLatLong(activity, mTempLat, mTempLong));
                    addEnquiry.setLocName(mTempLocName);
//                    addEnquiry.setServicesId(serviceTypeForEnquiry.getSelectedItemPosition() + 1);//TODO Need to verify this through add enquiry API field when enquiry added
                    addEnquiry.setServicesId(serviceId);//TODO Need to verify this through add enquiry API field when enquiry added
                    addEnquiry.setTechnicalProblem(editTextTechProblem.getText().toString());
                    addEnquiry.setTechnicalNote(editTextSpecialInstructions.getText().toString());
                    addEnquiry.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setOwnerId(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setUserId(SharedPrefManager.getInstance(activity).getUserId());
                    addEnquiry.setInquaryState(1);
                    addEnquiry.setIsActive(true);

                    //Call to API
                    /*((CRMFragment) fragment).saveUpdateEnquiry(addEnquiry);*/
                    CRMFragment crmFragment = new CRMFragment();
                    crmFragment.saveUpdateEnquiry(addEnquiry, activity);

                    //Remove hint (place holder object)from the list
                    removeTypes(activity, arrayAdapterServiceType, dialog);
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        /////////////////////////////////////////Cancel enquiry form/////////////////////////////////////////////////////////
        buttonCancelEnq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeTypes(activity, arrayAdapterServiceType, dialog);
            }
        });

        /////////////////////////////////////////Date picker on enquiry form/////////////////////////////////////////////////////////
        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextDatePicker);
            }
        });

        /////////////////////////////////////////Time picker on enquiry form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });

        /////////////////////////////////////////Address on enquiry form/////////////////////////////////////////////////////////
        editTextCustAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextCustAddress.setError(null);
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));
                // Set the fields to specify which types of place data to return.
                // Specify the fields to return.
                /*List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.PLUS_CODE);*/
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(activity);
                activity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        /////////////////////////////////////////Update enquiry form by select place listener /////////////////////////////////////////////////////////
        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    editTextCustAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });
        dialog.show();
    }

    public void getContactData(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        mEditTextCustName.setText(mName);
        mEditTextCustNumber.setText(mNumber);
    }

    public void getContactDataForAddUpdateCustPri(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        mtxtCustName.setText(mName);
        mtxtCustNumber.setText(mNumber);
    }

    public void getContactDataForAddUpdateCustSec(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        //mtxtCustName.setText(mName);
        mtxtCustNumberSec.setText(mNumber);
    }

    public void addCustomer(Activity activity) {

        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        //Button
        Button btnAddCust = dialog.findViewById(R.id.btnAddCust);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        //Edit Text
        EditText txtCustName = dialog.findViewById(R.id.txtCustName);
        EditText txtPrimaryAddress = dialog.findViewById(R.id.txtPrimaryAddress);
        EditText txtLandmark = dialog.findViewById(R.id.txtLandmark);
        EditText txtCustNumber = dialog.findViewById(R.id.txtCustNumber);
        EditText txtPrimEmailid = dialog.findViewById(R.id.txtPrimEmailid);

        EditText txtNotes = dialog.findViewById(R.id.txtNotes);
        EditText txtSecondaryAddress = dialog.findViewById(R.id.txtSecondaryAddress);
        EditText txtSecdCustNumber = dialog.findViewById(R.id.txtSecdCustNumber);
        EditText txtSecdLandmark = dialog.findViewById(R.id.txtSecdLandmark);
        EditText txtSecdEmailid = dialog.findViewById(R.id.txtSecdEmailid);
        EditText txtSecdNotes = dialog.findViewById(R.id.txtSecdNotes);

        ImageView closeAddCust = dialog.findViewById(R.id.closeAddCust);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);
        ImageView phonebuttonSec = dialog.findViewById(R.id.img_phonebookSec);

        mtxtCustName = txtCustName;
        mtxtCustNumber = txtCustNumber;
        mtxtCustNumberSec = txtSecdCustNumber;

        TextView hAddEnqVideo = dialog.findViewById(R.id.textView_how_to_add_enquiry);

        closeAddCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

       /* hAddEnqVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddEnquiryActivity.class);
                activity.startActivity(send);
            }
        });*/
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentAddUpdateCustPri();
            }
        });
        phonebuttonSec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentAddUpdateCustSec();
            }
        });

        btnAddCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(txtCustName)) {
                    txtCustName.setError(activity.getResources().getString(R.string.please_enter_cust_name));
                } else if (isEmpty(txtPrimaryAddress)) {
                    txtPrimaryAddress.setError(activity.getResources().getString(R.string.please_enter_address));
                } else if (!isEmpty(txtCustName) && (Character.isWhitespace(txtCustName.getText().toString().charAt(0)) || txtCustName.getText().toString().trim().isEmpty())) {
                    txtCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(txtCustNumber)) {
                    txtCustNumber.setError(activity.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1 &&
                        isValidPhone(txtCustNumber)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(txtCustNumber)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(txtCustNumber.getText().toString()) == 0)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (!txtPrimEmailid.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(txtPrimEmailid.getText().toString()).matches() == false) {
                    txtPrimEmailid.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else if (!txtSecdEmailid.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(txtSecdEmailid.getText().toString()).matches() == false) {
                    txtSecdEmailid.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else if (isEmpty(txtLandmark)) {
                    txtLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else {
                    //Create Enquiry Object
                    AddCustDTO.ResultData addCust = new AddCustDTO.ResultData();

                    addCust.setCustomerName(txtCustName.getText().toString());
                    addCust.setAddress(txtPrimaryAddress.getText().toString());
                    addCust.setDescription(txtLandmark.getText().toString()); //set Landmark
                    addCust.setMobileNumber(txtCustNumber.getText().toString());
                    addCust.setEmailId(txtPrimEmailid.getText().toString());
                    addCust.setInstructions(txtNotes.getText().toString());
                    //
                    addCust.setSecondaryAddress(txtSecondaryAddress.getText().toString());
                    addCust.setSecondaryDescription(txtSecdLandmark.getText().toString());
                    addCust.setSecondaryMobileNumber(txtSecdCustNumber.getText().toString());
                    addCust.setSecondaryEmailId(txtSecdEmailid.getText().toString());
                    addCust.setSecondaryInstructions(txtSecdNotes.getText().toString());
                    //
                    addCust.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    //Call to API
                    CustomerListFragment customerListFragment = new CustomerListFragment();
                    customerListFragment.addCustomerCRM(addCust, activity);
                    dialog.dismiss();

                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }

    public void updateCustomer(Activity activity, CustomerList.ResultData resultData) {

        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button btnUpdateCust = dialog.findViewById(R.id.btnAddCust);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        EditText txtCustName = dialog.findViewById(R.id.txtCustName);
        EditText txtPrimaryAddress = dialog.findViewById(R.id.txtPrimaryAddress);
        EditText txtLandmark = dialog.findViewById(R.id.txtLandmark);
        EditText txtCustNumber = dialog.findViewById(R.id.txtCustNumber);
        EditText txtPrimEmailid = dialog.findViewById(R.id.txtPrimEmailid);
        // mEditTextCustName = editTextCustName;
        EditText txtNotes = dialog.findViewById(R.id.txtNotes);
        // mEditTextCustNumber = editTextCustNumber;
        EditText txtSecondaryAddress = dialog.findViewById(R.id.txtSecondaryAddress);
        EditText txtSecdCustNumber = dialog.findViewById(R.id.txtSecdCustNumber);
        EditText txtSecdLandmark = dialog.findViewById(R.id.txtSecdLandmark);
        EditText txtSecdEmailid = dialog.findViewById(R.id.txtSecdEmailid);
        EditText txtSecdNotes = dialog.findViewById(R.id.txtSecdNotes);

        ImageView closeAddCust = dialog.findViewById(R.id.closeAddCust);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);
        ImageView phonebuttonSec = dialog.findViewById(R.id.img_phonebookSec);

        TextView hAddEnqVideo = dialog.findViewById(R.id.textView_how_to_add_enquiry);
        TextView txtLabel = dialog.findViewById(R.id.txtLabel);
        txtLabel.setText("Edit Customer");
        btnUpdateCust.setText("Update");
        //
        mtxtCustName = txtCustName;
        mtxtCustNumber = txtCustNumber;
        mtxtCustNumberSec = txtSecdCustNumber;

        closeAddCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

       /* hAddEnqVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(activity.getApplicationContext(), YouTubeAddEnquiryActivity.class);
                activity.startActivity(send);
            }
        });*/
        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentAddUpdateCustPri();
            }
        });

        phonebuttonSec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) activity).getContactDetailsIntentAddUpdateCustSec();
            }
        });

        // PRIMARY DETAILS
        if (resultData.getCustomerName() != null) {
            txtCustName.setText(resultData.getCustomerName());
        }
        if (resultData.getAddress() != null) {
            txtPrimaryAddress.setText(resultData.getAddress());
        }
        if (resultData.getDescription() != null) {
            txtLandmark.setText(resultData.getDescription());
        }
        if (resultData.getMobileNumber() != null) {
            txtCustNumber.setText(resultData.getMobileNumber());
        }
        if (resultData.getEmailId() != null) {
            txtPrimEmailid.setText(resultData.getEmailId());
        }
        if (resultData.getInstructions() != null) {
            txtNotes.setText(resultData.getInstructions());
        }
        // SECONDARY DETAILS
        if (resultData.getSecondaryAddress() != null) {
            txtPrimaryAddress.setText(resultData.getSecondaryAddress());
        }
        if (resultData.getSecondaryDescription() != null) {
            txtLandmark.setText(resultData.getSecondaryDescription());
        }
        if (resultData.getSecondaryMobileNumber() != null) {
            txtCustNumber.setText(resultData.getSecondaryMobileNumber());
        }
        if (resultData.getSecondaryEmailId() != null) {
            txtPrimEmailid.setText(resultData.getSecondaryEmailId());
        }
        if (resultData.getSecondaryInstructions() != null) {
            txtNotes.setText(resultData.getSecondaryInstructions());
        }

        btnUpdateCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(txtCustName)) {
                    txtCustName.setError(activity.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(txtCustName) && (Character.isWhitespace(txtCustName.getText().toString().charAt(0)) || txtCustName.getText().toString().trim().isEmpty())) {
                    txtCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(txtCustNumber)) {
                    txtCustNumber.setError(activity.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1 &&
                        isValidPhone(txtCustNumber)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(activity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(txtCustNumber)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(txtCustNumber.getText().toString()) == 0)) {
                    txtCustNumber.setError(activity.getString(R.string.please_enter_valid_number));
                } else if (!txtPrimEmailid.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(txtPrimEmailid.getText().toString()).matches() == false) {
                    txtPrimEmailid.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else if (!txtSecdEmailid.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(txtSecdEmailid.getText().toString()).matches() == false) {
                    txtSecdEmailid.setError(activity.getString(R.string.please_enter_valid_email_id));
                } else if (isEmpty(txtLandmark)) {
                    txtLandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else {
                    //Create Enquiry Object
                    AddCustDTO.ResultData updateCust = new AddCustDTO.ResultData();

                    updateCust.setCustomerName(txtCustName.getText().toString());
                    updateCust.setAddress(txtPrimaryAddress.getText().toString());
                    updateCust.setDescription(txtLandmark.getText().toString()); //set Landmark
                    updateCust.setMobileNumber(txtCustNumber.getText().toString());
                    updateCust.setEmailId(txtPrimEmailid.getText().toString());
                    updateCust.setInstructions(txtNotes.getText().toString());
                    //
                    updateCust.setSecondaryAddress(txtSecondaryAddress.getText().toString());
                    updateCust.setSecondaryDescription(txtSecdLandmark.getText().toString());
                    updateCust.setSecondaryMobileNumber(txtSecdCustNumber.getText().toString());
                    updateCust.setSecondaryEmailId(txtSecdEmailid.getText().toString());
                    updateCust.setSecondaryInstructions(txtSecdNotes.getText().toString());
                    //
                    updateCust.setCreatedBy(SharedPrefManager.getInstance(activity).getUserId());
                    //Call to API
                    CustomerListFragment customerListFragment = new CustomerListFragment();
                    customerListFragment.UpdateCustomerCRM(updateCust, activity);
                    dialog.dismiss();

                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //removeTypes(activity, arrayAdapterServiceType, dialog);
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

        dialog.show();
    }


}