package com.corefield.fieldweb.FieldWeb.Passbook;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.DTO.Passbook.DailyPassbook;
import com.corefield.fieldweb.DTO.Passbook.MonthlyPassbook;
import com.corefield.fieldweb.DTO.Passbook.TodayPassbook;
import com.corefield.fieldweb.DTO.Passbook.WeeklyPassbook;
import com.corefield.fieldweb.DTO.Passbook.YearlyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.DailyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.MonthlyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.TodayPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.WeeklyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.YearlyPassbookAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Home Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class contain all the view pager adapter for daily/monthly/yearly/today/weekly passbook fragment class
 */
public class HomePassbookFragmentNew extends Fragment implements OnTaskCompleteListener/*, DatePickerDialog.OnDateSetListener*/ {

    private static String TAG = HomePassbookFragment.class.getSimpleName();
    public List<MonthlyPassbook.MonthlyALlDataList> mMonthlyPassbook = null;
    public List<YearlyPassbook.GetYearlyData> mYearlyPassbook = null;
    public List<WeeklyPassbook.WeeklyData> mWeeklyPassbook = null;
    private View mRootView;
    TextView creditgiven, expenses, received, remainingAmt;
    TextView txtEstimatedEarning, txtMonthLblEarning, txtEarnings;
    private TextView txtToday, txtMonthly, txtYearly;
    private List<DailyPassbook.GetCurrentWeekData> mDailyPassbook = null;
    private TodayPassbookAsyncTask mTodayPassbookAsyncTask;
    private DailyPassbookAsyncTask mDailyPassbookAsyncTask;
    private WeeklyPassbookAsyncTask mWeeklyPassbookAsyncTask;
    private MonthlyPassbookAsyncTask mMonthlyPassbookAsyncTask;
    private YearlyPassbookAsyncTask mYearlyPassbookAsyncTask;
    private int mYear;
    private int mMonth;
    private int mDay;
    private Calendar mCal;
    private String currentMonthName = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*mRootView = inflater.inflate(R.layout.home_passbook_fragment, container, false);*/
        mRootView = inflater.inflate(R.layout.home_passbook_fragment_new, container, false);
        inIt();
        FGALoadEvent();
        return mRootView;
    }

    private void inIt() {
        /*mViewPagerPassbook = mRootView.findViewById(R.id.passbook_viewpager);
        mEarned = mRootView.findViewById(R.id.textview_passbook_earned);
        mCredit = mRootView.findViewById(R.id.textview_passbook_credit);
        mExpense = mRootView.findViewById(R.id.textview_passbook_expense);
        mReturn = mRootView.findViewById(R.id.textview_passbook_return);
        mTitle = mRootView.findViewById(R.id.textview_passbook_title);
        mTitle.setText(getResources().getString(R.string.today_bracket));*/
        txtEstimatedEarning = mRootView.findViewById(R.id.txtEstimatedEarning);
        txtToday = mRootView.findViewById(R.id.txtToday);
        txtMonthly = mRootView.findViewById(R.id.txtMonthly);
        txtYearly = mRootView.findViewById(R.id.txtYearly);
        txtMonthLblEarning = mRootView.findViewById(R.id.txtMonthLblEarning);
        txtEarnings = mRootView.findViewById(R.id.txtEarnings);

        creditgiven = mRootView.findViewById(R.id.creditgiven);
        expenses = mRootView.findViewById(R.id.expenses);
        received = mRootView.findViewById(R.id.received);
        remainingAmt = mRootView.findViewById(R.id.remainingAmt);

        callTodayAPI();

        //apiCall(0); // First Default Api Call for Today passbook
        /*setupViewPagerPassbook(mViewPagerPassbook);
        mTabLayoutPassbook = mRootView.findViewById(R.id.passbook_tab);
        mTabLayoutPassbook.setupWithViewPager(mViewPagerPassbook);
        mViewPagerPassbook.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    apiCall(position);
                    mTitle.setText(getResources().getString(R.string.today_bracket));
                } else if (position == 1) {
                    apiCall(position);
                    mTitle.setText(R.string.current_week_bracket);
                } else if (position == 2) {
                    apiCall(position);
                    mTitle.setText(R.string.current_month_btacket);
                } else if (position == 3) {
                    apiCall(position);
                    mTitle.setText(R.string.current_year_bracket);
                } else if (position == 4) {
                    apiCall(position);
                    mTitle.setText(R.string.years_bracket);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });*/
       /* mCal = Calendar.getInstance();
        mYear = mCal.get(Calendar.YEAR);
        mMonth = mCal.get(Calendar.MONTH);
        mDay = mCal.get(Calendar.DAY_OF_MONTH);*/

        txtToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* txtToday.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtMonthly.setBackgroundResource(0);
                txtYearly.setBackgroundResource(0);
                apiCall(0);*/
                txtToday.setBackgroundResource(R.drawable.bg_curve_passbook_selected);
                txtToday.setTextColor(Color.WHITE);
                txtMonthly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtMonthly.setTextColor(Color.BLACK);
                txtYearly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtYearly.setTextColor(Color.BLACK);
                apiCall(0);
            }
        });

       /* DatePickerDialog monthYearPickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, ((HomeActivityNew) getActivity()).currentYear, ((HomeActivityNew) getActivity()).currentMonth, ((HomeActivityNew) getActivity()).currentDay) {
            //DatePickerDialog dpd = new DatePickerDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT,this,year, month, day){
            // DatePickerDialog dpd = new DatePickerDialog(getActivity(), AlertDialog.THEME_TRADITIONAL,this,year, month, day){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int day = getContext().getResources().getIdentifier("android:id/day", null, null);
                if (day != 0) {
                    View dayPicker = findViewById(day);
                    if (dayPicker != null) {
                        //Set Day view visibility Off/Gone
                        dayPicker.setVisibility(View.GONE);
                    }
                }
            }
        };
        mCal.add(Calendar.DATE, 0);
        monthYearPickerDialog.getDatePicker().setMaxDate(mCal.getTimeInMillis());
        mCal.add(Calendar.MONTH, -2);
        monthYearPickerDialog.getDatePicker().setMinDate(mCal.getTimeInMillis());*/

        txtMonthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  txtMonthly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtToday.setBackgroundResource(0);
                txtYearly.setBackgroundResource(0);
                //monthYearPickerDialog.show();
                btnMonthYear();*/
                txtMonthly.setBackgroundResource(R.drawable.bg_curve_passbook_selected);
                txtMonthly.setTextColor(Color.WHITE);
                txtToday.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtToday.setTextColor(Color.BLACK);
                txtYearly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtYearly.setTextColor(Color.BLACK);
                //monthYearPickerDialog.show();
                btnMonthYear();
            }
        });

        txtYearly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* txtYearly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtToday.setBackgroundResource(0);
                txtMonthly.setBackgroundResource(0);
                //monthYearPickerDialog.show();
                btnYear();*/
                txtYearly.setBackgroundResource(R.drawable.bg_curve_passbook_selected);
                txtYearly.setTextColor(Color.WHITE);
                txtToday.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtToday.setTextColor(Color.BLACK);
                txtMonthly.setBackgroundResource(R.drawable.bg_curve_border_selected);
                txtMonthly.setTextColor(Color.BLACK);
                //monthYearPickerDialog.show();
                btnYear();
            }
        });

        // Yearly
        /* DatePickerDialog yearPickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, ((HomeActivityNew) getActivity()).currentYear, 0, 0*//*, ((HomeActivityNew) getActivity()).currentMonth, ((HomeActivityNew) getActivity()).currentDay*//*) {
            //DatePickerDialog dpd = new DatePickerDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT,this,year, month, day){
            // DatePickerDialog dpd = new DatePickerDialog(getActivity(), AlertDialog.THEME_TRADITIONAL,this,year, month, day){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int day = getContext().getResources().getIdentifier("android:id/day", null, null);
                int month = getContext().getResources().getIdentifier("android:id/Month", null, null);
                if (day != 0) {
                    View dayPicker = findViewById(day);
                    View monthPicker = findViewById(month);
                    if (dayPicker != null && monthPicker != null) {
                        //Set Day view visibility Off/Gone
                        dayPicker.setVisibility(View.GONE);
                        monthPicker.setVisibility(View.GONE);
                    }
                }
            }
        };
        mCal.add(Calendar.YEAR, 0);
        yearPickerDialog.getDatePicker().setMaxDate(mCal.getTimeInMillis());
        mCal.add(Calendar.YEAR, -2);
        yearPickerDialog.getDatePicker().setMinDate(mCal.getTimeInMillis());*/
    }

    private void callTodayAPI() {
        try {
            txtToday.setBackgroundResource(R.drawable.bg_curve_passbook_selected);
            txtToday.setTextColor(Color.WHITE);
            txtMonthly.setBackgroundResource(R.drawable.bg_curve_border_selected);
            txtMonthly.setTextColor(Color.BLACK);
            txtYearly.setBackgroundResource(R.drawable.bg_curve_border_selected);
            txtYearly.setTextColor(Color.BLACK);
            apiCall(0);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.PASSBOOK, bundle);
    }

    public void btnMonthYear() {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(),
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) { // on date set
                        //etMonthYear.setText((selectedMonth + 1) + "/" + selectedYear);
                        currentMonthName = DateUtils.getMonthName(selectedMonth);
                        mMonth = selectedMonth;
                        mYear = selectedYear;
                        //txtMonthLblEarning.setText(currentMonthName + " Earnings");
                        apiCall(3);
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setActivatedMonth(today.get(Calendar.MONTH))
                .setMinYear(today.get(Calendar.YEAR) - 1)
                .setActivatedYear(today.get(Calendar.YEAR))
                .setMaxYear(today.get(Calendar.YEAR))
                /* .setMinMonth(today.get(Calendar.MONTH))
                 .setMaxMonth(today.get(Calendar.MONTH))*/
                .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                .setTitle("Select month year")
                .build().show();
    }

    public void btnYear() {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(),
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) { // on date set
                        //etYear.setText(String.valueOf(selectedYear));
                        mYear = selectedYear;
                        apiCall(4);
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setActivatedMonth(Calendar.JULY)
                .setMinYear(today.get(Calendar.YEAR) - 2)
                .setActivatedYear(today.get(Calendar.YEAR))
                .setMaxYear(today.get(Calendar.YEAR))
                .setMinMonth(Calendar.FEBRUARY)
                .setTitle("Select year")
                .showYearOnly()
                .build().show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void apiCall(int position) {
        if (position == 0) {
            /*mTodayPassbookAsyncTask = new TodayPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mTodayPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
            getTodayPassbookDetails();
        } else if (position == 1) {
            mDailyPassbookAsyncTask = new DailyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mDailyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 2) {
            mWeeklyPassbookAsyncTask = new WeeklyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mWeeklyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
        } else if (position == 3) {
            /*mMonthlyPassbookAsyncTask = new MonthlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mMonthlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), mMonth + 1, mYear);*/
            getMonthlyPassbookDetails();
        } else if (position == 4) {
           /* mYearlyPassbookAsyncTask = new YearlyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
            mYearlyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId(), mYear);*/
            getYearlyPassbookDetails();
        }
    }

    private void setupViewPagerPassbook(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new TodayPassbookFragment(), getResources().getString(R.string.today));
        adapter.addFragment(new DailyPassbookFragment(), getString(R.string.daily));
        adapter.addFragment(new WeeklyPassbookFragment(), getString(R.string.weekly));
        adapter.addFragment(new MonthlyPassbookFragment(), getString(R.string.monthly));
        adapter.addFragment(new YearlyPassbookFragment(), getString(R.string.yearly));
        viewPager.setAdapter(adapter);
    }

    private void getTodayPassbookDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<TodayPassbook> call = RetrofitClient.getInstance(getContext()).getMyApi().getTodayPassbookDetails(userID);
                call.enqueue(new retrofit2.Callback<TodayPassbook>() {
                    @Override
                    public void onResponse(Call<TodayPassbook> call, Response<TodayPassbook> response) {
                        try {
                            if (response.code() == 200) {
                                TodayPassbook todayPassbook = response.body();
                                FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
                                setClearText();
                                ///////////////////////////////////////////////////////////////////////////////////////
                                txtEstimatedEarning.setText(String.valueOf(todayPassbook.getResultData().getEarningAmount()));
                                creditgiven.setText("₹ " + String.valueOf(todayPassbook.getResultData().getCredit()));
                                expenses.setText("₹ " + String.valueOf(todayPassbook.getResultData().getExpenses()));
                                received.setText("₹ " + String.valueOf(todayPassbook.getResultData().getReturn()));
                                remainingAmt.setText("₹ " + String.valueOf(todayPassbook.getResultData().getBalance()));
                                txtMonthLblEarning.setText("Today's Earnings");
                                txtEarnings.setText("₹ " + String.valueOf(todayPassbook.getResultData().getEarningAmount()));

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TodayPassbook> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetTodaysPassbookByUserId API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetTodaysPassbookByUserId API:");
            ex.getMessage();
        }
    }


    private void getMonthlyPassbookDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getActivity()).getUserId();
                Call<MonthlyPassbook> call = RetrofitClient.getInstance(getActivity()).getMyApi().getMonthlyPassbookDetails(userID, mMonth + 1, mYear);
                call.enqueue(new retrofit2.Callback<MonthlyPassbook>() {
                    @Override
                    public void onResponse(Call<MonthlyPassbook> call, Response<MonthlyPassbook> response) {
                        try {
                            if (response.code() == 200) {
                                MonthlyPassbook monthlyPassbook = response.body();

                                mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
                                // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
                                FWLogger.logInfo(TAG, "Check = " + monthlyPassbook.getMessage());
                                setClearText();
                                for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                                    if (mMonthlyPassbook.get(i).getMonth().contains(String.valueOf(currentMonthName))
                                            && mMonthlyPassbook.get(i).getYear().equalsIgnoreCase(String.valueOf(mYear))) {
                                        txtEstimatedEarning.setText(String.valueOf(mMonthlyPassbook.get(i).getEstimated()));
                                        creditgiven.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalCredit()));
                                        expenses.setText("₹ " + String.valueOf(mMonthlyPassbook.get(i).getExpenses()));
                                        received.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalDeduction()));
                                        txtEarnings.setText("₹ " + String.valueOf(mMonthlyPassbook.get(i).getEarning()));
                                        txtMonthLblEarning.setText(currentMonthName + " " + mYear + "'s" + " Earnings");
                                        remainingAmt.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalOpening()));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<MonthlyPassbook> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetMonthlyPassbookV2 API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetMonthlyPassbookV2 API:");
            ex.getMessage();
        }
    }

    private void getYearlyPassbookDetails() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getActivity()).getUserId();
                Call<YearlyPassbook> call = RetrofitClient.getInstance(getActivity()).getMyApi().getYearlyPassbookDetails(userID, mYear);
                call.enqueue(new retrofit2.Callback<YearlyPassbook>() {
                    @Override
                    public void onResponse(Call<YearlyPassbook> call, Response<YearlyPassbook> response) {
                        try {
                            if (response.code() == 200) {
                                YearlyPassbook yearlyPassbook = response.body();
                                mYearlyPassbook = yearlyPassbook.getResultData().getGetYearlyData();
                                FWLogger.logInfo(TAG, "Check = " + yearlyPassbook.getMessage());
                                setClearText();
                                txtEstimatedEarning.setText(String.valueOf(yearlyPassbook.getResultData().getTotalEstimated()));
                                creditgiven.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalCredit()));
                                expenses.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalExpenses()));
                                received.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalDeduction()));
                                remainingAmt.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalOpening()));
                                txtMonthLblEarning.setText(mYear + " " + "'s" + " Earnings");
                                txtEarnings.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalEarned()));
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<YearlyPassbook> call, Throwable throwable) {
                        //Toast.makeText(getApplicationContext(), "GetYearlyPassbookV2 API : An error has occured", Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, "Exception in GetYearlyPassbookV2 API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetYearlyPassbookV2 API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {

        if (classType.equalsIgnoreCase(TodayPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            TodayPassbook todayPassbook = gson.fromJson(urlConnectionResponse.resultData, TodayPassbook.class);
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
            setClearText();
            ///////////////////////////////////////////////////////////////////////////////////////
            txtEstimatedEarning.setText(String.valueOf(todayPassbook.getResultData().getEarningAmount()));
            creditgiven.setText("₹ " + String.valueOf(todayPassbook.getResultData().getCredit()));
            expenses.setText("₹ " + String.valueOf(todayPassbook.getResultData().getExpenses()));
            received.setText("₹ " + String.valueOf(todayPassbook.getResultData().getReturn()));
            remainingAmt.setText("₹ " + String.valueOf(todayPassbook.getResultData().getBalance()));
            txtMonthLblEarning.setText("Today's Earnings");
            txtEarnings.setText("₹ " + String.valueOf(todayPassbook.getResultData().getEarningAmount()));
        } else if (classType.equalsIgnoreCase(MonthlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            MonthlyPassbook monthlyPassbook = gson.fromJson(urlConnectionResponse.resultData, MonthlyPassbook.class);
            mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + monthlyPassbook.getMessage());
            setClearText();
            ///////////////////////////////////////////////////////////////////////////////////////
           /* txtEstimatedEarning.setText(String.valueOf(monthlyPassbook.getResultData().getTotalEarned()));
            creditgiven.setText(String.valueOf(monthlyPassbook.getResultData().getTotalCredit()));
            expenses.setText(String.valueOf(monthlyPassbook.getResultData().getTotalExpenses()));
            received.setText(String.valueOf(monthlyPassbook.getResultData().getTotalDeduction()));
            txtMonthLblEarning.setText(currentMonthName + " " + "Earnings");*/
            for (int i = 0; i < mMonthlyPassbook.size(); i++) {
                if (mMonthlyPassbook.get(i).getMonth().contains(String.valueOf(currentMonthName))
                        && mMonthlyPassbook.get(i).getYear().equalsIgnoreCase(String.valueOf(mYear))) {
                    txtEstimatedEarning.setText(String.valueOf(mMonthlyPassbook.get(i).getEstimated()));
                    creditgiven.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalCredit()));
                    expenses.setText("₹ " + String.valueOf(mMonthlyPassbook.get(i).getExpenses()));
                    received.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalDeduction()));
                    txtEarnings.setText("₹ " + String.valueOf(mMonthlyPassbook.get(i).getEarning()));
                    txtMonthLblEarning.setText(currentMonthName + " " + mYear + "'s" + " Earnings");
                    remainingAmt.setText("₹ " + String.valueOf(monthlyPassbook.getResultData().getTotalOpening()));
                }
            }

        } else if (classType.equalsIgnoreCase(YearlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            YearlyPassbook yearlyPassbook = gson.fromJson(urlConnectionResponse.resultData, YearlyPassbook.class);
            mYearlyPassbook = yearlyPassbook.getResultData().getGetYearlyData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + yearlyPassbook.getMessage());
            setClearText();
            ///////////////////////////////////////////////////////////////////////////////////////
            txtEstimatedEarning.setText(String.valueOf(yearlyPassbook.getResultData().getTotalEstimated()));
            creditgiven.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalCredit()));
            expenses.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalExpenses()));
            received.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalDeduction()));
            remainingAmt.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalOpening()));
            txtMonthLblEarning.setText(mYear + " " + "'s" + " Earnings");
            txtEarnings.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalEarned()));
            /*for (int i = 0; i < mYearlyPassbook.size(); i++) {
                if (mYearlyPassbook.get(i).getYear() == mYear) {
                    txtEstimatedEarning.setText(String.valueOf(mYearlyPassbook.get(i).getEarned()));
                    creditgiven.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalCredit()));
                    expenses.setText("₹ " + mYearlyPassbook.get(i).getExpenses());
                    received.setText("₹ " + String.valueOf(yearlyPassbook.getResultData().getTotalDeduction()));
                    txtMonthLblEarning.setText(mYear + " " + "Earnings");
                }
            }*/

        }
        /*if (classType.equalsIgnoreCase(TodayPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            TodayPassbook todayPassbook = gson.fromJson(urlConnectionResponse.resultData, TodayPassbook.class);
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + todayPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(todayPassbook.getResultData().getEarningAmount()));
            mCredit.setText(String.valueOf(todayPassbook.getResultData().getCredit()));
            mExpense.setText(String.valueOf(todayPassbook.getResultData().getExpenses()));
            mReturn.setText(String.valueOf(todayPassbook.getResultData().getReturn()));
        } else if (classType.equalsIgnoreCase(DailyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            DailyPassbook dailyPassbook = gson.fromJson(urlConnectionResponse.resultData, DailyPassbook.class);
            mDailyPassbook = dailyPassbook.getResultData().getGetCurrentWeekData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + dailyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(dailyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(dailyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(dailyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(dailyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(WeeklyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            WeeklyPassbook weeklyPassbook = gson.fromJson(urlConnectionResponse.resultData, WeeklyPassbook.class);
            mWeeklyPassbook = weeklyPassbook.getResultData().getWeeklyData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + weeklyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(weeklyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(weeklyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(weeklyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(weeklyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(MonthlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            MonthlyPassbook monthlyPassbook = gson.fromJson(urlConnectionResponse.resultData, MonthlyPassbook.class);
            mMonthlyPassbook = monthlyPassbook.getResultData().getMonthlyALlDataList();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + monthlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(monthlyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(monthlyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(monthlyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(monthlyPassbook.getResultData().getTotalDeduction()));
        } else if (classType.equalsIgnoreCase(YearlyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            YearlyPassbook yearlyPassbook = gson.fromJson(urlConnectionResponse.resultData, YearlyPassbook.class);
            mYearlyPassbook = yearlyPassbook.getResultData().getGetYearlyData();
            // Toast.makeText(getContext(), todayPassbook.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Check = " + yearlyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            mEarned.setText(String.valueOf(yearlyPassbook.getResultData().getTotalEarned()));
            mCredit.setText(String.valueOf(yearlyPassbook.getResultData().getTotalCredit()));
            mExpense.setText(String.valueOf(yearlyPassbook.getResultData().getTotalExpenses()));
            mReturn.setText(String.valueOf(yearlyPassbook.getResultData().getTotalDeduction()));
        }*/
    }

    private void setClearText() {
        try {
            txtEstimatedEarning.setText("");
            creditgiven.setText("");
            expenses.setText("");
            received.setText("");
            remainingAmt.setText("");
            txtEarnings.setText("");
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mTodayPassbookAsyncTask != null) {
            mTodayPassbookAsyncTask.cancel(true);
        }
        if (mDailyPassbookAsyncTask != null) {
            mDailyPassbookAsyncTask.cancel(true);
        }
        if (mWeeklyPassbookAsyncTask != null) {
            mWeeklyPassbookAsyncTask.cancel(true);
        }
        if (mMonthlyPassbookAsyncTask != null) {
            mMonthlyPassbookAsyncTask.cancel(true);
        }
        if (mYearlyPassbookAsyncTask != null) {
            mYearlyPassbookAsyncTask.cancel(true);
        }
        super.onDestroyView();

    }

   /* @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mDay = dayOfMonth;
        mMonth = month;
        mYear = year;
        currentMonthName = DateUtils.getMonthName(mMonth);
        FWLogger.logInfo(TAG, "mMonth : " + mMonth + " " + mYear);
        if (fromMonth.equalsIgnoreCase("Month")) {
            apiCall(3);
        }


    }*/
}

/**
 * ViewPagerAdapter for Home Passbook fragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This ViewPagerAdapter class contain all the fragment class of daily/monthly/yearly/today/weekly passbook
 */
/*
class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}*/
