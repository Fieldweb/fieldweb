package com.corefield.fieldweb.FieldWeb.TechRouteTaskMap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListViewAdapter;
import com.corefield.fieldweb.Adapter.NotesViewAdapter;
import com.corefield.fieldweb.Adapter.TaskCompletedMultitemListAdapter;
import com.corefield.fieldweb.Adapter.TaskOngoingMultitemListAdapter;
import com.corefield.fieldweb.Adapter.TasksListAdapter;
import com.corefield.fieldweb.Adapter.TechTaskRouteMapAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Task.DownloadReport;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Unused.SphericalUtil;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.RoundedTransformation;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.BuildConfig;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks Details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show completed task details
 */
@SuppressLint("ValidFragment")
public class TechnTaskRouteMapFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback, BaseActivity.RequestPermissionsUpdateListener {

    public static String TAG = TechnTaskRouteMapFragment.class.getSimpleName();
    private String mFieldImageURL = "", mFieldImageURL1 = "", mFieldImageURL2 = "", mImageBeforeTaskURL = "", mImageBeforeTaskURL1 = "", mImageBeforeTaskURL2 = "";
    private View mRootView;
    private TasksList mResultData;
    private SupportMapFragment mMapFragment;
    private RecyclerTouchListener mRecyclerTouchListener;

    private List<TasksList.ResultData> resultDataList;
    private GoogleMap mGoogleMap;
    private Marker mMarkerDestination;
    private double mDestLat = 0, mDestLong = 0;
    private RecyclerView mtaskTechMapRecycler;
    private TechTaskRouteMapAdapter mTechTaskRouteMapAdapter;
    private LinearLayoutManager mLayoutManager;

    private ArrayList<LatLng> locationArrayList;
    private ArrayList<Double> distanceArrayList;

    List<LatLng> path = new ArrayList();
    private static final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET};

    ArrayList<TasksList.ResultData> mTasksLists;

    int value;
    double distance, distanceinkm;

    private final int FINE_PERMISSION_CODE = 1;
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;

    @SuppressLint("ValidFragment")
    public TechnTaskRouteMapFragment() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tech_task_route_map, container, false);
        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);
            ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);

            inIt();
            getTaskList();
            manageBackPress();
        }
        return mRootView;
    }

    private void inIt() {

        mtaskTechMapRecycler = mRootView.findViewById(R.id.taskTechMapRecycler);

        //mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.task_details_map);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        getLastLocation();

        mRecyclerTouchListener = new RecyclerTouchListener() {
            @Override
            public void onClick(View view, int position) {
                if (view.getId() == R.id.imageView_call) {
                   /* if (mResultData.getResultData().get(0).getContactNo() != null && !mResultData.getResultData().get(0).getContactNo().isEmpty()) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + mResultData.getResultData().get(0).getContactNo()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                    }*/
                    try {
                        if (SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("true") || SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                            CommonFunction.TelCMIForGeneral(getActivity(), mResultData.getResultData().get(0).getContactNo());
                        } else {
                            if (mResultData.getResultData().get(0).getContactNo() != null && !mResultData.getResultData().get(0).getContactNo().isEmpty()) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + mResultData.getResultData().get(0).getContactNo()));
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
            }
        };
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_PERMISSION_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.task_details_map);
                }
            }
        });
    }


    protected void getTaskList() {
        if (getContext() != null) {
            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    Call<TasksList> call = RetrofitClient.getInstance(getContext()).getMyApi().getTodayTaskList(SharedPrefManager.getInstance(getContext()).getUserId(), 1, false);
                    call.enqueue(new retrofit2.Callback<TasksList>() {
                        @Override
                        public void onResponse(Call<TasksList> call, Response<TasksList> response) {
                            try {
                                if (response.code() == 200) {
                                    mResultData = response.body();
                                    if (mResultData.getCode().equalsIgnoreCase("200") && mResultData.getMessage().equalsIgnoreCase("success")) {
                                        if (mResultData != null) {
                                            FWLogger.logInfo(TAG, "success");
                                            setData(mResultData);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), mResultData.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<TasksList> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in TodayTaskListNew? API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in TodayTaskListNew? API:");
                ex.getMessage();
            }

        }
    }

    private void setData(TasksList mResultData) {
        mMapFragment.getMapAsync(this);
        List<TasksList.ResultData> mTempTasklist = new ArrayList<>();
        mTasksLists = new ArrayList<TasksList.ResultData>();
        mTempTasklist = mResultData.getResultData();
        // SHOW ONLY TODAYS TASK
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String compDate = formatter.format(date) + "T00:00:00";
            for (int i = 0; i < mTempTasklist.size(); i++) {
                if (compDate.equalsIgnoreCase(mTempTasklist.get(i).getTaskDate())) {
                    mTasksLists.addAll(Collections.singleton(mTempTasklist.get(i)));
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        if (mTasksLists.size() > 0) {
            mTechTaskRouteMapAdapter = new TechTaskRouteMapAdapter(getContext(), mTasksLists);
            mTechTaskRouteMapAdapter.setClickListener(mRecyclerTouchListener);
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            mtaskTechMapRecycler.setLayoutManager(mLayoutManager);
            mtaskTechMapRecycler.setItemAnimator(new DefaultItemAnimator());
            mtaskTechMapRecycler.setAdapter(mTechTaskRouteMapAdapter);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.textView_call:
            case R.id.imageView_call:

                break;

            default:
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        FWLogger.logInfo(TAG, "updateGoogleMapUI called");
        if (mGoogleMap != null) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = mGoogleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.style_json));

                if (!success) {
                    FWLogger.logInfo(TAG, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(TAG, "Can't find style. Error: ", e);
            }

            MapsInitializer.initialize(getContext());
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

            //Check permission
            if (((BaseActivity) getActivity()).checkAllPermissionEnabled()) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                if (((BaseActivity) getActivity()).requestToEnableAllPermission(this, BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }

            mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

            value = mTasksLists.size();

            //Toast.makeText(getContext(), String.valueOf(currentLocation.getLatitude()), Toast.LENGTH_SHORT).show();
            // Toast.makeText(getContext(), String.valueOf(currentLocation.getLongitude()), Toast.LENGTH_SHORT).show();
            path.add(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));

            // UPDATE MRKERS ON MAP ::
            try {
                locationArrayList = new ArrayList<>();
                if (mTasksLists.size() > 0) {
                    for (int i = 0; i < mTasksLists.size(); i++) {

                        value--;
                        if (mTasksLists.get(i).getLatitude() != null)
                            mDestLat = Double.parseDouble(mTasksLists.get(i).getLatitude());

                        if (mTasksLists.get(i).getLongitude() != null)
                            mDestLong = Double.parseDouble(mTasksLists.get(i).getLongitude());

                        LatLng taskLatlong = new LatLng(Double.parseDouble(mTasksLists.get(i).getLatitude()), Double.parseDouble(mTasksLists.get(i).getLongitude()));
                        locationArrayList.add(taskLatlong);
                        path.add(new LatLng(locationArrayList.get(i).latitude, locationArrayList.get(i).longitude));

                        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
                        Bitmap bmp = Bitmap.createBitmap(200, 400, conf);
                        Canvas canvas = new Canvas(bmp);

                        Paint paint = new Paint();
                        paint.setColor(getResources().getColor(R.color.colorPrimaryDark));
                        paint.setTextSize(60);

                        canvas.drawText(String.valueOf(value + 1), 30, 300, paint); // paint defines the text color, stroke width, size
                        mGoogleMap.addMarker(new MarkerOptions().anchor(0.0f, 1.0f).position(new LatLng(mDestLat, mDestLong)).icon(BitmapDescriptorFactory.fromBitmap(bmp)));

                        // below line is use to add marker to each location of our array list.
                        mGoogleMap.addMarker(new MarkerOptions().anchor(0.0f, 1.0f).position(new LatLng(mDestLat, mDestLong)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_fieldweb_location)).title(mResultData.getResultData().get(i).getName()).snippet(mResultData.getResultData().get(i).getCustomerName()));
                        // below line is use to zoom our camera on map.
                        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(18.0f));

                        // DRAW POLY LINE
                        // First, create a PolylineOptions outside the loop to configure the common properties.
                        PolylineOptions polylineOptions = new PolylineOptions().color(Color.RED) // Set the color of the polyline.
                                .width(5); // Set the width of the polyline.

                        for (LatLng destination : locationArrayList) {
                            // Add a polyline from your current location to the destination.
                            PolylineOptions individualPolyline = new PolylineOptions().addAll(Arrays.asList(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destination)).color(polylineOptions.getColor()).width(polylineOptions.getWidth());

                            mGoogleMap.addPolyline(individualPolyline);
                        }

                        LatLng sydney = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        LatLng Brisbane = new LatLng(locationArrayList.get(i).latitude, locationArrayList.get(i).longitude);
                        distance = SphericalUtil.computeDistanceBetween(sydney, Brisbane);
                        distanceinkm = distance / 1000;

                        distanceArrayList = new ArrayList<>();
                        distanceArrayList.add(distanceinkm);

                        // Move the camera to the last location in your list.
                        if (!locationArrayList.isEmpty()) {
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList.get(locationArrayList.size() - 1)));
                        }
                    }
                }
            } catch (Exception ex) {
                ex.getMessage();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == FINE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLastLocation();
        } else {
            Toast.makeText(getContext(), "Location Permission is denied", Toast.LENGTH_SHORT).show();
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_LOCATION)) {
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}