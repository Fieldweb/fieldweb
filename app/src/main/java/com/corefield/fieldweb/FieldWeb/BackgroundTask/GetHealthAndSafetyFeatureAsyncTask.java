package com.corefield.fieldweb.FieldWeb.BackgroundTask;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.HealthAndSaftey.HealthAndSafety;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

/**
 * //
 * Created by CFS on 3/9/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.BackgroundTask
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : Get status of health and safety feature
 * //
 **/
public class GetHealthAndSafetyFeatureAsyncTask extends BaseAsyncTask {
    protected static String TAG = GetHealthAndSafetyFeatureAsyncTask.class.getSimpleName();
    public OnTaskCompleteListener onTaskCompleteListener;

    public GetHealthAndSafetyFeatureAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                int UserId = SharedPrefManager.getInstance(mContext).getUserId();
                if (UserId != 0) {
                    String query = "TechUserId=" + UserId;
                    FWLogger.logInfo(TAG, "query = " + query);
                    URLConnectionRequest urlConnectionRequest = new URLConnectionRequest();
                    if (urlConnectionRequest != null && query != null) {
                        Request request = RequestBuilder.buildRequest(mContext, URLConstant.HealthAndSafteyDetails.GET_HEALTH_SAFETY_FEATURE_DETAILS, null);
                        mUrlConnectionResponse = urlConnectionRequest.sendGetRequest(request.getmURL(), request.getmHeader(), query, request.getToken());

                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null && mUrlConnectionResponse != null) {
            if (mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (status) {
                    if (onTaskCompleteListener != null)
                        onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, HealthAndSafety.class.getSimpleName());
                } else {
                    //Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
