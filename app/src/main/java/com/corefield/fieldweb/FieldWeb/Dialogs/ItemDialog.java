package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.DTO.Item.AddItem;
import com.corefield.fieldweb.DTO.Item.AddDeductUsedItem;
import com.corefield.fieldweb.DTO.Item.EditItem;
import com.corefield.fieldweb.DTO.Item.GetUsedItemList;
import com.corefield.fieldweb.DTO.Item.IssueItem;
import com.corefield.fieldweb.DTO.Item.ItemUnitType;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryFragment;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerItemInventoryFragmentNew;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddPhotoBeforeTaskAsyncTask;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAddItemActivity;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeAssignItemActivity;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class ItemDialog {
    private static ItemDialog mItemDialog;
    public ImageView mImageview, imageview_expense_hint;
    TextView captureimg;
    int mCount = 0;
    private String mWhichImage = "", mEncodedBaseString = "", imageFileName = "";
    public String mBaseStringFromService = "";
    private int strSelectedunitTypeID;
    private String mPhotoName = "", mNote = "";
    private String mCapturedImagePath = "";
    public static final int PICK_IMAGE_GALLERY = 2;
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    private AddPhotoBeforeTaskAsyncTask mAddPhotoBeforeTask;
    private TasksList.ResultData mResultData;
    private Uri mImageUri;
    private SearchableSpinner unitSpinner;
    Context mContext;
    CountdownTechFragmentNew mCountdownTechFrag;
    private String mUsedItemType;


    /**
     * The private constructor for the ItemDialog Singleton class
     */
    private ItemDialog() {
    }

    public static ItemDialog getInstance() {
        //instantiate a new ExpenditureDialog if we didn't instantiate one yet
        if (mItemDialog == null) {
            mItemDialog = new ItemDialog();
        }
        return mItemDialog;
    }

    public void addItemDialog(Context context, List<ItemUnitType.ResultData> mitemUnitTypeList) { //, Fragment fragment
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_item);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextInputEditText textInputItemName = dialog.findViewById(R.id.edittext_item_name);
        EditText editTextItemQuantity = dialog.findViewById(R.id.edittext_item_quantity);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView addItemVideo = dialog.findViewById(R.id.textView_how_to_add_item);

        EditText mSalesPrice = dialog.findViewById(R.id.edittext_sales_price);
        EditText mPurchasePrice = dialog.findViewById(R.id.edittext_purchase_pricee);


        mImageview = dialog.findViewById(R.id.imageView_before_task);
        imageview_expense_hint = dialog.findViewById(R.id.imageview_expense_hint);
        captureimg = dialog.findViewById(R.id.captureimg);
        unitSpinner = (SearchableSpinner) dialog.findViewById(R.id.spinner_unit);

        Button buttonAddItem = dialog.findViewById(R.id.button_add);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        ImageView closeAddItem = dialog.findViewById(R.id.closeAddItem);

        mContext = context;


        if (Integer.parseInt(editTextItemQuantity.getText().toString()) == 0) {
            textViewMinus.setVisibility(View.INVISIBLE);
        } else {
            textViewMinus.setVisibility(View.VISIBLE);
        }

        addItemVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(mContext.getApplicationContext(), YouTubeAddItemActivity.class);
                mContext.startActivity(send);
            }
        });

        // SET UNIT TYPE DATA
        setUnitTypeData(mitemUnitTypeList);

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    strSelectedunitTypeID = mitemUnitTypeList.get(position - 1).getItemUnitTypeId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivityNew) mContext).pickImage("ItemDialog");
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQuantity.getText() != null && !editTextItemQuantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQuantity.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        textViewMinus.setVisibility(View.INVISIBLE);
                        Toast.makeText(context, "Quantity should not less than 1", Toast.LENGTH_SHORT).show();
                    } else {
                        textViewMinus.setVisibility(View.VISIBLE);
                        editTextItemQuantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQuantity.getText() != null && !editTextItemQuantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQuantity.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        textViewMinus.setVisibility(View.INVISIBLE);
                        Toast.makeText(context, "Quantity should not less than 1", Toast.LENGTH_SHORT).show();
                    } else {
                        textViewMinus.setVisibility(View.VISIBLE);
                        editTextItemQuantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(textInputItemName)) {
                    textInputItemName.setError(context.getString(R.string.please_enter_name));
                } else if (isEmpty(editTextItemQuantity)) {
                    editTextItemQuantity.setError(context.getString(R.string.please_enter_quantity));
                } else if ((Integer.valueOf(editTextItemQuantity.getText().toString()) == 0)) {
                    editTextItemQuantity.setError(context.getString(R.string.please_enter_valid_quantity));
                } else if (isEmpty(textInputItemNote)) {
                    textInputItemNote.setError(context.getString(R.string.please_enter_notes));
                } else if (unitSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Unit")) {
                    Toast.makeText(mContext, "Please Select Unit", Toast.LENGTH_SHORT).show();
                } else {
                    AddItem.Resultdata addItem = new AddItem.Resultdata();
                    addItem.setCreatedby(SharedPrefManager.getInstance(context).getUserId());
                    addItem.setDescription(textInputItemNote.getText().toString());
                    addItem.setId(0);
                    addItem.setIsmodelerror(true);
                    addItem.setIssuccessful(true);
                    addItem.setItemtype(1);
                    try {
                        String originalString = textInputItemName.getText().toString();
                        String newString = originalString.replaceAll("[^a-zA-Z0-9\\s]", "");
                        addItem.setName(newString);
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    //addItem.setName(textInputItemName.getText().toString());
                    addItem.setQuantity(Integer.parseInt(editTextItemQuantity.getText().toString()));
                    addItem.setUpdatedby(SharedPrefManager.getInstance(context).getUserId());
                    //START NEW INPUT
                    addItem.setItemUnitTypeId(strSelectedunitTypeID);
                    addItem.setSalesPrice(mSalesPrice.getText().toString());
                    addItem.setPurchasePrice(mPurchasePrice.getText().toString());
                    if (!imageFileName.isEmpty() && !mEncodedBaseString.isEmpty()) {
                        addItem.setImageFileName(imageFileName);
                        addItem.setImageFileBase64Str(mEncodedBaseString);
                    } else {
                        addItem.setImageFileName("");
                        addItem.setImageFileBase64Str("");
                    }
                    // END NEW INPUT

                    ((HomeActivityNew) context).saveAddItem(addItem);
                    mCount = 0;
                    imageFileName = "";
                    mEncodedBaseString = "";
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(TextInputEditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCount = 0;
                dialog.dismiss();
            }
        });

        closeAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCount = 0;
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setUnitTypeData(List<ItemUnitType.ResultData> mitemUnitTypeList) {
        try {
            if (mitemUnitTypeList.size() > 0) {

                ArrayList<String> tempArray = new ArrayList<String>();
                tempArray.add("Select Unit");
                for (int i = 0; i < mitemUnitTypeList.size(); i++) {
                    tempArray.add(mitemUnitTypeList.get(i).getUnitTypeName());
                }

                ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_spinner_item, tempArray) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);
                        ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.BLACK);
                        if (position == getCount()) {
                            ((TextView) v.findViewById(android.R.id.text1)).setText("");
                            ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                            ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.BLACK);
                        }
                        return v;
                    }

                    @Override
                    public int getCount() {
                        return super.getCount();
                    }
                };
                typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                unitSpinner.setAdapter(typeAdapter);
                unitSpinner.setSelection(0);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void issueItemTechDialog(Context context, Fragment fragment, int userId, ArrayList<String> itemsNameList, String techName) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_issue_item_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Spinner items = dialog.findViewById(R.id.spin_items);
        Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        TextInputEditText textInputQuantity = dialog.findViewById(R.id.item_issue_quantity);
        Button buttonAssign = dialog.findViewById(R.id.button_assign);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        ///////////////////////////////////////////////////////////////////////////////////////////////
        itemsNameList.add(context.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());
        ///////////////////////////////////////////////////////////////////////////////////////////////
        buttonAssign.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (items.getSelectedItem() == context.getString(R.string.select_item)) {
                    ((TextView) items.getSelectedView()).setError(context.getString(R.string.please_select_option));
                } else if (isEmpty(textInputQuantity)) {
                    textInputQuantity.setError(context.getString(R.string.please_enter_quantity));
                } else if ((Integer.valueOf(textInputQuantity.getText().toString()) == 0)) {
                    textInputQuantity.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }

            private boolean isEmpty(TextInputEditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(context.getString(R.string.select_item));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void updateItemDialog(Context context, Fragment fragment, String itemName, String getTotalQuantity, int mItemID) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_item_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        EditText editTextItemQty = dialog.findViewById(R.id.edittext_item_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView textViewItem = dialog.findViewById(R.id.textView_item);

        mImageview = dialog.findViewById(R.id.imageView_update_task);
        imageview_expense_hint = dialog.findViewById(R.id.imageview_expense_hint);
        captureimg = dialog.findViewById(R.id.captureimg);
        unitSpinner = dialog.findViewById(R.id.spinner_unit);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);

        EditText mSalesPrice = dialog.findViewById(R.id.edittext_sales_pricee);
        EditText mPurchasePrice = dialog.findViewById(R.id.edittext_purchase_pricee);


        Button buttonUpdateItem = dialog.findViewById(R.id.button_add);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        textViewItem.setText(itemName);


        // SET UNIT TYPE DATA
        try {
            if (((HomeActivityNew) context).mitemUnitTypeList.size() > 0) {

                ArrayList<String> tempArray = new ArrayList<String>();
                tempArray.add("Select Unit");
                for (int i = 0; i < ((HomeActivityNew) context).mitemUnitTypeList.size(); i++) {
                    tempArray.add(((HomeActivityNew) context).mitemUnitTypeList.get(i).getUnitTypeName());
                }

                ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, tempArray) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);
                        ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.BLACK);
                        if (position == getCount()) {
                            ((TextView) v.findViewById(android.R.id.text1)).setText("");
                            ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                            ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.BLACK);
                        }
                        return v;
                    }

                    @Override
                    public int getCount() {
                        return super.getCount();
                    }
                };
                typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                unitSpinner.setAdapter(typeAdapter);
                unitSpinner.setSelection(0);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    strSelectedunitTypeID = ((HomeActivityNew) context).mitemUnitTypeList.get(position - 1).getItemUnitTypeId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivityNew) context).pickImage("ItemDialogUpdate");
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText().toString() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(editTextItemQty.getText().toString()) == 0)) {
                        editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else {
                       /* AddItem.Resultdata addItem = new AddItem.Resultdata();
                        addItem.setId(mItemID);
                        addItem.setUpdatedby(SharedPrefManager.getInstance(context).getUserId());
                        addItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        if (!textInputItemNote.getText().toString().isEmpty()) {
                            addItem.setDescription(textInputItemNote.getText().toString());
                        } else {
                            addItem.setDescription("");
                        }
                        //START NEW INPUT
                        addItem.setItemUnitTypeId(strSelectedunitTypeID);
                        addItem.setSalesPrice(mSalesPrice.getText().toString());
                        addItem.setPurchasePrice(mPurchasePrice.getText().toString());
                        if (!imageFileName.isEmpty() && !mEncodedBaseString.isEmpty()) {
                            addItem.setImageFileName(imageFileName);
                            addItem.setImageFileBase64Str(mEncodedBaseString);
                        } else {
                            addItem.setImageFileName("");
                            addItem.setImageFileBase64Str("");
                        }
                        // END NEW INPUT
                        *//*((OwnerItemInventoryFragmentNew) fragment).updateItem(Integer.valueOf(editTextItemQty.getText().toString()));*//*
                        ((OwnerItemInventoryFragmentNew) fragment).updateItem(addItem);
                        dialog.dismiss();*/
                        EditItem.ResultData addItem = new EditItem.ResultData();
                        addItem.setId(mItemID);
                        addItem.setUpdatedBy(SharedPrefManager.getInstance(context).getUserId());
                        addItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        if (!textInputItemNote.getText().toString().isEmpty()) {
                            addItem.setDescription(textInputItemNote.getText().toString());
                        } else {
                            addItem.setDescription("");
                        }
                        //START NEW INPUT
                        addItem.setItemUnitTypeId(strSelectedunitTypeID);
                       /* addItem.setSalesPrice(Integer.parseInt(mSalesPrice.getText().toString()));
                        addItem.setPurchasePrice(Integer.parseInt(mPurchasePrice.getText().toString()));*/
                        try {
                            int salesPrice = Integer.parseInt(mSalesPrice.getText().toString());
                            int purPrice = Integer.parseInt(mPurchasePrice.getText().toString());
                            addItem.setSalesPrice(salesPrice);
                            addItem.setPurchasePrice(purPrice);
                        } catch (Exception ex) {
                            ex.getMessage();
                            addItem.setSalesPrice(0);
                            addItem.setPurchasePrice(0);
                        }
                        if (!imageFileName.isEmpty() && !mEncodedBaseString.isEmpty()) {
                            addItem.setImageFileName(imageFileName);
                            addItem.setImageFileBase64Str(mEncodedBaseString);
                        } else {
                            addItem.setImageFileName("");
                            addItem.setImageFileBase64Str("");
                        }
                        // END NEW INPUT
                        /*((OwnerItemInventoryFragmentNew) fragment).updateItem(Integer.valueOf(editTextItemQty.getText().toString()));*/
                        ((OwnerItemInventoryFragmentNew) fragment).updateItem(addItem);
                        dialog.dismiss();
                    }
                } else {
                    editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void updateItemEditDialog(Context context, Fragment fragment, String itemName,
                                     String getTotalQuantity, int mItemID,
                                     String UnitTypeName, double salesPrice,
                                     double purchasePrice, String imgPath) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_edit_item_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        EditText editTextItemQty = dialog.findViewById(R.id.edittext_item_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView textViewItem = dialog.findViewById(R.id.textView_item);

        mImageview = dialog.findViewById(R.id.imageView_update_task);
        imageview_expense_hint = dialog.findViewById(R.id.imageview_expense_hint);
        captureimg = dialog.findViewById(R.id.captureimg);
        unitSpinner = dialog.findViewById(R.id.spinner_unit);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);

        EditText mSalesPrice = dialog.findViewById(R.id.edittext_sales_pricee);
        EditText mPurchasePrice = dialog.findViewById(R.id.edittext_purchase_pricee);


        Button buttonUpdateItem = dialog.findViewById(R.id.button_add);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);
        textViewItem.setText(itemName);

        try {
            editTextItemQty.setText(getTotalQuantity);
            int salesPriceInt = (int) salesPrice;
            int purchasePriceInt = (int) purchasePrice;
            mSalesPrice.setText(String.valueOf(salesPriceInt));
            mPurchasePrice.setText(String.valueOf(purchasePriceInt));

            //editTextItemQty.setText(getTotalQuantity);
            //mSalesPrice.setText(String.valueOf(salesPrice));
            //mPurchasePrice.setText(String.valueOf(purchasePrice));
            Picasso.get().load(imgPath).placeholder(R.drawable.ic_no_image).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageview_expense_hint, new Callback() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onError(Exception e) {
                    imageview_expense_hint.setImageResource(R.drawable.ic_no_image);
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }

        mImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivityNew) context).pickImage("ItemDialogUpdate");
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        // SET UNIT TYPE DATA
        try {
            if (((HomeActivityNew) context).mitemUnitTypeList.size() > 0) {

                ArrayList<String> tempArray = new ArrayList<String>();
                tempArray.add("Select Unit");
                for (int i = 0; i < ((HomeActivityNew) context).mitemUnitTypeList.size(); i++) {
                    tempArray.add(((HomeActivityNew) context).mitemUnitTypeList.get(i).getUnitTypeName());
                }

                ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, tempArray) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);
                        ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.BLACK);
                        if (position == getCount()) {
                            ((TextView) v.findViewById(android.R.id.text1)).setText("");
                            ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                            ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.BLACK);
                        }
                        return v;
                    }

                    @Override
                    public int getCount() {
                        return super.getCount();
                    }
                };
                typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                unitSpinner.setAdapter(typeAdapter);

                for (int i = 0; i < tempArray.size(); i++) {
                    if (tempArray.get(i).equalsIgnoreCase(UnitTypeName)) {
                        unitSpinner.setSelection(i);
                    }
                }
                //unitSpinner.setSelection(0);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    strSelectedunitTypeID = ((HomeActivityNew) context).mitemUnitTypeList.get(position - 1).getItemUnitTypeId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText().toString() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(editTextItemQty.getText().toString()) == 0)) {
                        editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else {
                       /* AddItem.Resultdata addItem = new AddItem.Resultdata();
                        addItem.setId(mItemID);
                        addItem.setUpdatedby(SharedPrefManager.getInstance(context).getUserId());
                        addItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        if (!textInputItemNote.getText().toString().isEmpty()) {
                            addItem.setDescription(textInputItemNote.getText().toString());
                        } else {
                            addItem.setDescription("");
                        }
                        //START NEW INPUT
                        addItem.setItemUnitTypeId(strSelectedunitTypeID);
                        addItem.setSalesPrice(mSalesPrice.getText().toString());
                        addItem.setPurchasePrice(mPurchasePrice.getText().toString());
                        if (!imageFileName.isEmpty() && !mEncodedBaseString.isEmpty()) {
                            addItem.setImageFileName(imageFileName);
                            addItem.setImageFileBase64Str(mEncodedBaseString);
                        } else {
                            addItem.setImageFileName("");
                            addItem.setImageFileBase64Str("");
                        }
                        // END NEW INPUT
                        *//*((OwnerItemInventoryFragmentNew) fragment).updateItem(Integer.valueOf(editTextItemQty.getText().toString()));*//*
                        ((OwnerItemInventoryFragmentNew) fragment).updateItem(addItem);
                        dialog.dismiss();*/
                        EditItem.ResultData addItem = new EditItem.ResultData();
                        addItem.setId(mItemID);
                        addItem.setUpdatedBy(SharedPrefManager.getInstance(context).getUserId());
                        addItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                        if (!textInputItemNote.getText().toString().isEmpty()) {
                            addItem.setDescription(textInputItemNote.getText().toString());
                        } else {
                            addItem.setDescription("");
                        }
                        //START NEW INPUT
                        addItem.setItemUnitTypeId(strSelectedunitTypeID);
                        try {
                            int salesPrice = Integer.parseInt(mSalesPrice.getText().toString());
                            int purPrice = Integer.parseInt(mPurchasePrice.getText().toString());
                            addItem.setSalesPrice(salesPrice);
                            addItem.setPurchasePrice(purPrice);
                        } catch (Exception ex) {
                            ex.getMessage();
                            addItem.setSalesPrice(0);
                            addItem.setPurchasePrice(0);
                        }

                        if (!imageFileName.isEmpty() && !mEncodedBaseString.isEmpty()) {
                            addItem.setImageFileName(imageFileName);
                            addItem.setImageFileBase64Str(mEncodedBaseString);
                        } else {
                            addItem.setImageFileName("");
                            addItem.setImageFileBase64Str("");
                        }
                        // END NEW INPUT
                        /*((OwnerItemInventoryFragmentNew) fragment).updateItem(Integer.valueOf(editTextItemQty.getText().toString()));*/
                        ((OwnerItemInventoryFragmentNew) fragment).updateItem(addItem);
                        dialog.dismiss();
                    }
                } else {
                    editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void unAssignItemDialog(Context context, Fragment fragment, String itemName, String techName, String getTotalQuantity) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_unassign_item_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextInputLayout textInputTechName = dialog.findViewById(R.id.textInput_tech_name);
        TextInputLayout textInputUsedQty = dialog.findViewById(R.id.textInput_used_qty);
        EditText editTextTechName = dialog.findViewById(R.id.edittext_tech_name);
        EditText editTextUsedQty = dialog.findViewById(R.id.edittext_used_qty);
        EditText editTextItemQty = dialog.findViewById(R.id.edittext_return_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView textViewItem = dialog.findViewById(R.id.textView_item);

        Button buttonUpdateItem = dialog.findViewById(R.id.button_add);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel);

        editTextTechName.setText(techName);
        textInputTechName.setEnabled(false);

        editTextUsedQty.setText(getTotalQuantity);
        textInputUsedQty.setEnabled(false);

        textViewItem.setText(itemName);
        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText().toString() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(editTextItemQty.getText().toString()) == 0)) {
                        editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else {
                        ((OwnerItemInventoryFragmentNew) fragment).unAssign(Integer.valueOf(editTextItemQty.getText().toString()));
                        dialog.dismiss();
                    }
                } else {
                    editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void assignItemDialog(Context context, ArrayList<String> itemsNameList, ArrayList<String> userNameList) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_issue_item_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Spinner items = dialog.findViewById(R.id.spin_items);
        Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        EditText editTextItemQty = dialog.findViewById(R.id.item_issue_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView hassignItemVideo = dialog.findViewById(R.id.textView_how_to_assign_item);

        Button buttonAssign = dialog.findViewById(R.id.button_assign);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);

        hassignItemVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(context.getApplicationContext(), YouTubeAssignItemActivity.class);
                context.startActivity(send);
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

///////////////////////////////////////////////////////////////////////////////////////////////
        itemsNameList.add(context.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());
///////////////////////////////////////////////////////////////////////////////////////////////
        userNameList.add(context.getString(R.string.select_technicians));
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, userNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };

        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());
///////////////////////////////////////////////////////////////////////////////////////////////
        buttonAssign.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (items.getSelectedItem() == context.getString(R.string.select_item)) {
                    ((TextView) items.getSelectedView()).setError(context.getString(R.string.please_select_option));
                } else if (isEmpty(editTextItemQty)) {
                    editTextItemQty.setError(context.getString(R.string.please_enter_quantity));
                } else if ((Integer.valueOf(editTextItemQty.getText().toString()) == 0)) {
                    editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                } else if (technicians.getSelectedItem() == context.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(context.getString(R.string.please_select_option));
                } else {
                    IssueItem.ResultData issueItem = new IssueItem.ResultData();
                    ItemsList.ResultData itemsList = ((HomeActivityNew) context).mItemsLists.get(items.getSelectedItemPosition());
                    UsersList.ResultData usersLists = ((HomeActivityNew) context).mUsersLists.get(technicians.getSelectedItemPosition());
                    issueItem.setIsModelError(true);
                    issueItem.setIsSuccessful(true);
                    issueItem.setUpdatedBy(usersLists.getId());
                    issueItem.setCreatedBy(usersLists.getId());
                    issueItem.setIsActive(true);
                    issueItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                    issueItem.setTransactionType(2);
                    issueItem.setItemId(itemsList.getId());
                    issueItem.setId(0);
                    issueItem.setUserId(usersLists.getId());
                    ((HomeActivityNew) context).saveIssuedItem(issueItem);
                    arrayAdapterItem.remove(context.getString(R.string.select_item));
                    arrayAdapterTech.remove(context.getString(R.string.select_technicians));
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapterItem.remove(context.getString(R.string.select_item));
                arrayAdapterTech.remove(context.getString(R.string.select_technicians));
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void assignItemDialogFromAdapter(Context context, String itemsNameList,
                                            String userNameList, int userID, int itemID) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_issue_item_new_adapter);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView items = dialog.findViewById(R.id.spin_items);
        Spinner technicians = dialog.findViewById(R.id.spin_technicians);
        EditText editTextItemQty = dialog.findViewById(R.id.item_issue_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextView hassignItemVideo = dialog.findViewById(R.id.textView_how_to_assign_item);

        Button buttonAssign = dialog.findViewById(R.id.button_assign);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);

        try {
            items.setText(itemsNameList);
            //technicians.setText(userNameList);
        } catch (Exception ex) {
            ex.getMessage();
        }

        hassignItemVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent send = new Intent(context.getApplicationContext(), YouTubeAssignItemActivity.class);
                context.startActivity(send);
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextItemQty.getText() != null && !editTextItemQty.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(editTextItemQty.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        editTextItemQty.setText(String.valueOf(mCount));
                    }
                }
            }
        });


///////////////////////////////////////////////////////////////////////////////////////////////
       /* itemsNameList.add(context.getString(R.string.select_item));
        ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, itemsNameList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };
        arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        items.setAdapter(arrayAdapterItem);
        items.setSelection(arrayAdapterItem.getCount());*/
///////////////////////////////////////////////////////////////////////////////////////////////
        ((HomeActivityNew) context).mUsersNameList.add(context.getString(R.string.select_technicians));
        //userNameList.add(context.getString(R.string.select_technicians));
        ArrayAdapter<String> arrayAdapterTech = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, ((HomeActivityNew) context).mUsersNameList/*userNameList*/) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }
        };

        arrayAdapterTech.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        technicians.setAdapter(arrayAdapterTech);
        technicians.setSelection(arrayAdapterTech.getCount());
///////////////////////////////////////////////////////////////////////////////////////////////
        buttonAssign.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                /*if (items.getSelectedItem() == context.getString(R.string.select_item)) {
                    ((TextView) items.getSelectedView()).setError(context.getString(R.string.please_select_option));
                }*/
                if (isEmpty(editTextItemQty)) {
                    editTextItemQty.setError(context.getString(R.string.please_enter_quantity));
                } else if ((Integer.valueOf(editTextItemQty.getText().toString()) == 0)) {
                    editTextItemQty.setError(context.getString(R.string.please_enter_valid_quantity));
                } else if (technicians.getSelectedItem() == context.getString(R.string.select_technicians)) {
                    ((TextView) technicians.getSelectedView()).setError(context.getString(R.string.please_select_option));
                } else {
                    IssueItem.ResultData issueItem = new IssueItem.ResultData();
                    //ItemsList.ResultData itemsList = ((HomeActivityNew) context).mItemsLists.get(items.getSelectedItemPosition());
                    UsersList.ResultData usersLists = ((HomeActivityNew) context).mUsersLists.get(technicians.getSelectedItemPosition());
                    issueItem.setIsModelError(true);
                    issueItem.setIsSuccessful(true);
                    issueItem.setUpdatedBy(usersLists.getId());
                    issueItem.setCreatedBy(usersLists.getId());
                    issueItem.setIsActive(true);
                    issueItem.setQuantity(Integer.valueOf(editTextItemQty.getText().toString()));
                    issueItem.setTransactionType(2);
                    issueItem.setItemId(itemID);
                    issueItem.setId(0);
                    issueItem.setUserId(usersLists.getId());
                    ((HomeActivityNew) context).saveIssuedItem(issueItem);
                    //arrayAdapterItem.remove(context.getString(R.string.select_item));
                    arrayAdapterTech.remove(context.getString(R.string.select_technicians));
                    dialog.dismiss();

                    AppCompatActivity activity = (AppCompatActivity) context;
                    OwnerItemInventoryFragmentNew myFragment = new OwnerItemInventoryFragmentNew();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // arrayAdapterItem.remove(context.getString(R.string.select_item));
                arrayAdapterTech.remove(context.getString(R.string.select_technicians));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void deleteItemDialog(Context context, int itemId) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_delete_item);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonDelete = dialog.findViewById(R.id.button_delete_item);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel_delete_item);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) context;
                ItemInventoryFragment myFragment = new ItemInventoryFragment();
                Bundle args = new Bundle();
                args.putInt("itemId", itemId);
                myFragment.setArguments(args);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void SureTodeleteItemDialog(Context context, int itemId, Fragment fragment) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.sureto_dialog_delete_item);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonDelete = dialog.findViewById(R.id.button_delete_item);
        Button buttonCancel = dialog.findViewById(R.id.button_cancel_delete_item);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((OwnerItemInventoryFragmentNew) fragment).deleteItem(itemId);
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    /*public void capturePhotoBeforeTask() {
        mEncodedBaseString = null;
        checkCameraPermission();
        TaskDialogNew taskDialogNew = TaskDialogNew.getInstance();
        taskDialogNew.captureBeforeTaskPhotoDialog(mActivity, mResultData, mCountdownTechFrag);
    }*/

    public void addPhotoBeforeTask(String note) {
        //checkCameraPermission();
        if (note != null)
            mNote = note;
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        mPhotoName = timeStamp + "_" + mWhichImage + ".jpg";
        FWLogger.logInfo(TAG, "Photo Name : " + mPhotoName + "    note : " + note);
        mAddPhotoBeforeTask = new AddPhotoBeforeTaskAsyncTask(mContext, BaseAsyncTask.Priority.LOW, mCountdownTechFrag);
        mAddPhotoBeforeTask.execute(mPhotoName, note, mEncodedBaseString);
    }

    /*private void setBeforeTaskData() {
//        if (mResultData.getPreDeviceInfoDto() == null) {
        preDeviceInfoDto = new TasksList.ResultData.PreDeviceInfoDto();
//        }
        FWLogger.logInfo(TAG, "mImageBeforeTaskURL : " + mImageBeforeTaskURL);
        if (mImageBeforeTaskURL != null && !mImageBeforeTaskURL.isEmpty())
            preDeviceInfoDto.setDeviceInfoImagePath(mImageBeforeTaskURL);
        if (mNote != null && !mNote.isEmpty())
            preDeviceInfoDto.setDeviceInfoNotes(mNote);
        if (mPhotoName != null && !mPhotoName.isEmpty())
            preDeviceInfoDto.setDeviceInfoImageName(mPhotoName);
        preDeviceInfoDto.setTaskId(mResultData.getId());
        preDeviceInfoDto.setCreatedBy(SharedPrefManager.getInstance(getContext()).getUserId());

        mResultData.setPreDeviceInfoDto(preDeviceInfoDto);
    }*/

    /*public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(AddPhotoBeforeTask.class.getSimpleName())) {
            Gson gson = new Gson();
            AddPhotoBeforeTask addPhotoBeforeTask = gson.fromJson(urlConnectionResponse.resultData, AddPhotoBeforeTask.class);
            if (addPhotoBeforeTask != null) {
                if (addPhotoBeforeTask.getCode().equalsIgnoreCase("200")
                        && (addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_saved_successfully, getActivity()))
                        || addPhotoBeforeTask.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.pre_task_closure_updated_successfully, getActivity())))) {
                    Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                    FWLogger.logInfo(TAG, addPhotoBeforeTask.getMessage());

                    mLinearBeforeTask.setVisibility(View.VISIBLE);
                    mLinearClickHere.setVisibility(View.GONE);
                    mTextViewTakePic.setVisibility(View.GONE);
                    if (mNote != null && !mNote.isEmpty()) {
                        mLinearNote.setVisibility(View.VISIBLE);
                        mTextViewNote.setText(mNote);
                    } else {
                        mLinearNote.setVisibility(View.GONE);
                    }

                    if (addPhotoBeforeTask.getResultData() != null) {
                        mImageBeforeTaskURL = addPhotoBeforeTask.getResultData().getDeviceInfoImagePath();
                        setPhotoBeforeTask(mImageBeforeTaskURL);
                    }

                } else {
                    mLinearBeforeTask.setVisibility(View.GONE);
                    mLinearClickHere.setVisibility(View.VISIBLE);
                    mTextViewTakePic.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), addPhotoBeforeTask.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }*/

    /*private void setPhotoBeforeTask(String imageBeforeTaskURL) {
        Picasso.get()
                .load(imageBeforeTaskURL)
                .placeholder(R.drawable.field_location_upload_photo)
                .error(R.drawable.field_location_upload_photo)
                .transform(new RoundedTransformation(20, 4))
                .fit()
//                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(mImageViewBeforeTaskPhoto, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mImageViewBeforeTaskPhoto.setImageResource(R.drawable.field_location_upload_photo);
                    }
                });
    }*/

/*
    public void pickImage(String whichImage, int imageNo, CountdownTechFragmentNew.ImageUpdateListener imageUpdateListener) {
        checkCameraPermission();
        mWhichImage = whichImage;
        mImageNo = imageNo;
        mImageUpdateListener = imageUpdateListener;
        final CharSequence[] options = {mContext.getResources().getString(R.string.take_photo), mContext.getResources().getString(R.string.choose_from_gallery), mContext.getResources().getString(R.string.cancel)};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.select_option));
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(mContext.getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(mContext.getApplicationContext())) {
                        Toast.makeText(mContext.getApplicationContext(),
                                mContext.getResources().getString(R.string.camera_not_supported),
                                Toast.LENGTH_LONG).show();
                    }
                    if (((HomeActivityNew) mContext).checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (((HomeActivityNew) mContext).requestToEnableAllPermission(mCountdownTechFrag, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    dialog.dismiss();
                } else if (options[item].equals(mContext.getResources().getString(R.string.choose_from_gallery))) {

                    if (((HomeActivityNew) mContext).checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickPhoto.putExtra("PICK_IMAGE_GALLERY", 2);
                        mContext.startActivity(pickPhoto);
                    } else if (((HomeActivityNew) mContext).requestToEnableAllPermission(mCountdownTechFrag, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickPhoto.putExtra("PICK_IMAGE_GALLERY", 2);
                        mContext.startActivity(pickPhoto);
                    }
                    dialog.dismiss();
                } else if (options[item].equals(mContext.getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
*/

    /*private void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            //photo = new File(Environment.getExternalStorageDirectory(), getFileName() + ".jpg");
            File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            photo = File.createTempFile(getFileName(), ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCapturedImagePath = photo.getAbsolutePath();
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mImageUri = FileProvider.getUriForFile(mContext,
                BuildConfig.APPLICATION_ID + ".provider",
                photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        intent.putExtra("CAMERA_CAPTURE_IMAGE_REQUEST_CODE", 100);
        mContext.startActivity(intent);
    }*/

   /* private String getFileName() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        String imageFileName = timeStamp + "_" + mWhichImage + "_" + mImageNo + "_";
        return imageFileName;
    }*/

/*
    public void updateAssignedItem(Context context, String techName, int quantity, String itemName,
                                   int itemID, int techID) {

        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_assigned_item_update);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView txtTechName = dialog.findViewById(R.id.txtTechName);
        EditText item_issue_quantity = dialog.findViewById(R.id.item_issue_quantity);
        ImageView textViewMinus = dialog.findViewById(R.id.textView_minus);
        ImageView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
        TextView txtAssignedItemName = dialog.findViewById(R.id.txtAssignedItemName);

        Button buttonUpdateItem = dialog.findViewById(R.id.buttonUpdateItem);

        try {
            txtTechName.setText(techName);
            //item_issue_quantity.setText(String.valueOf(quantity));
            txtAssignedItemName.setText("Assigned Item : " + " " + itemName);
        } catch (Exception ex) {
            ex.getMessage();
        }


        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText().toString() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(item_issue_quantity.getText().toString()) == 0)) {
                        item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else {
                        IssueItem.ResultData issueItem = new IssueItem.ResultData();
                        issueItem.setIsModelError(true);
                        issueItem.setIsSuccessful(true);
                        issueItem.setUpdatedBy(techID);
                        issueItem.setCreatedBy(techID);
                        issueItem.setIsActive(true);
                        issueItem.setQuantity(Integer.valueOf(item_issue_quantity.getText().toString()));
                        issueItem.setTransactionType(2);
                        issueItem.setItemId(itemID);
                        issueItem.setId(0);
                        issueItem.setUserId(techID);
                        ((HomeActivityNew) context).saveIssuedItem(issueItem);
                        dialog.dismiss();

                        AppCompatActivity activity = (AppCompatActivity) context;
                        OwnerItemInventoryFragmentNew myFragment = new OwnerItemInventoryFragmentNew();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    }
                } else {
                    item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });
        dialog.show();
    }
*/

    public void updateReturnItem(Context context, String techName, int quantity, String itemName,
                                 int itemID, int userID, int taskID, int issuedId) {

        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_assigned_item_update);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView txtTechName = dialog.findViewById(R.id.txtTechName);
        EditText editTextTechName = dialog.findViewById(R.id.edittext_tech_name);
        EditText item_issue_quantity = dialog.findViewById(R.id.item_issue_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
        TextView txtAssignedItemName = dialog.findViewById(R.id.txtAssignedItemName);

        Button buttonUpdateItem = dialog.findViewById(R.id.buttonUpdateItem);

        try {
            editTextTechName.setText(techName);
            editTextTechName.setKeyListener(null);
            //item_issue_quantity.setText(String.valueOf(quantity));
            txtAssignedItemName.setText(itemName);
        } catch (Exception ex) {
            ex.getMessage();
        }


        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText().toString() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(item_issue_quantity.getText().toString()) == 0)) {
                        item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else if (textInputItemNote.getText().toString().isEmpty()) {
                        textInputItemNote.setError(context.getString(R.string.please_enter_notes));
                    } else {
                        int qty = Integer.valueOf(item_issue_quantity.getText().toString());
                        String strNotes = textInputItemNote.getText().toString();

                        ((HomeActivityNew) context).saveReturnItem(userID, itemID, qty, issuedId, taskID, userID, strNotes);

                        dialog.dismiss();

                        AppCompatActivity activity = (AppCompatActivity) context;
                        OwnerItemInventoryFragmentNew myFragment = new OwnerItemInventoryFragmentNew();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    }
                } else {
                    item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });
        dialog.show();
    }

    public void updateUsedItem(Context context, GetUsedItemList.ResultData resultData) {

        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_used_item_update);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView txtAssignedQty = dialog.findViewById(R.id.txtAssignedQty);
        TextView txtIssuedQty = dialog.findViewById(R.id.txtIssuedQty);
        TextView txtUseddQty = dialog.findViewById(R.id.txtUseddQty);

        EditText item_issue_quantity = dialog.findViewById(R.id.item_issue_quantity);
        TextView textViewMinus = dialog.findViewById(R.id.textView_minus);
        TextView textViewPlus = dialog.findViewById(R.id.textView_plus);
        TextInputEditText textInputItemNote = dialog.findViewById(R.id.edittext_item_note);
        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
        TextView txtAssignedItemName = dialog.findViewById(R.id.txtAssignedItemName);
        TextView txtItemid = dialog.findViewById(R.id.txtItemid);

        Button buttonUpdateItem = dialog.findViewById(R.id.buttonUpdateItem);
        mUsedItemType = "AddUsedItem";

        try {
            txtAssignedItemName.setText(resultData.getItemName());
            txtItemid.setText("[" + (String.valueOf(resultData.getItemId()) + "]"));
            //
           /* txtIssuedQty.setText(String.valueOf(resultData.getAssignedQty()));
            txtUseddQty.setText(String.valueOf(resultData.getUsedQty()));*/
            txtAssignedQty.setText(String.valueOf(resultData.getAssignedQty()));
            txtIssuedQty.setText(String.valueOf(resultData.getCurrentAssignedQty()));
            txtUseddQty.setText(String.valueOf(resultData.getUsedQty()));

        } catch (Exception ex) {
            ex.getMessage();
        }


        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        textViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount--;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        textViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    mCount = Integer.parseInt(item_issue_quantity.getText().toString());
                    mCount++;
                    if (mCount <= 0) {
                        Toast.makeText(context, "Quantity should not less than 0", Toast.LENGTH_SHORT).show();
                    } else {
                        item_issue_quantity.setText(String.valueOf(mCount));
                    }
                }
            }
        });

        SegmentedGroup segmentedGroupBalance = dialog.findViewById(R.id.segmentedGroupBalance);
        segmentedGroupBalance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobuttonAddBalance:
                        // buttonAddDeductBal.setText("ADD");
                        mUsedItemType = "AddUsedItem";

                        break;
                    case R.id.radiobuttonDeductBalance:
                        //buttonAddDeductBal.setText("DEDUCT");
                        mUsedItemType = "DeductUsedItem";

                        break;
                    default:
                        break;
                }
            }
        });


        buttonUpdateItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item_issue_quantity.getText().toString() != null && !item_issue_quantity.getText().toString().isEmpty()) {
                    if ((Integer.valueOf(item_issue_quantity.getText().toString()) == 0)) {
                        item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                    } else if (textInputItemNote.getText().toString().isEmpty()) {
                        textInputItemNote.setError(context.getString(R.string.please_enter_notes));
                    } else if (Integer.parseInt(item_issue_quantity.getText().toString()) > Integer.parseInt(txtAssignedQty.getText().toString())) {
                        Toast.makeText(context, "Used quantity is greater than Updated assign quantity!", Toast.LENGTH_LONG).show();
                    } else {
                        if (mUsedItemType.equalsIgnoreCase("AddUsedItem")) {
                            AddDeductUsedItem addUsedItem = new AddDeductUsedItem();
                            //addUsedItem.setId(resultData.getId());
                            addUsedItem.setItemId(resultData.getItemId());
                            addUsedItem.setItemIssuedId(resultData.getItemIssuedId());
                            addUsedItem.setTaskId(resultData.getTaskId());
                            // addUsedItem.setAvlQty(resultData.getAvlQty());
                            //addUsedItem.setAssignedQty(resultData.getAssignedQty());
                            addUsedItem.setUsedQty(Integer.parseInt(item_issue_quantity.getText().toString()));
                            addUsedItem.setUserId(resultData.getUserId()); // TECH ID
                            addUsedItem.setCreatedBy(resultData.getCreatedBy()); // OWNER ID
                            //addUsedItem.setCreatedDate();
                            addUsedItem.setUpdateBy(resultData.getUpdateBy()); // OWNER ID
                            //addUsedItem.setUpdatedDate();
                            addUsedItem.setNotes(textInputItemNote.getText().toString());

                            ((HomeActivityNew) context).addUsedItems(addUsedItem);

                        } else if (mUsedItemType.equalsIgnoreCase("DeductUsedItem")) {
                            AddDeductUsedItem addUsedItem = new AddDeductUsedItem();
                            //addUsedItem.setId(resultData.getId());
                            addUsedItem.setItemId(resultData.getItemId());
                            addUsedItem.setItemIssuedId(resultData.getItemIssuedId());
                            addUsedItem.setTaskId(resultData.getTaskId());
                            // addUsedItem.setAvlQty(resultData.getAvlQty());
                            // addUsedItem.setAssignedQty(resultData.getAssignedQty());
                            addUsedItem.setUsedQty(Integer.parseInt(item_issue_quantity.getText().toString()));
                            addUsedItem.setUserId(resultData.getUserId());
                            addUsedItem.setCreatedBy(resultData.getCreatedBy());
                            //addUsedItem.setCreatedDate();
                            addUsedItem.setUpdateBy(resultData.getUpdateBy());
                            //addUsedItem.setUpdatedDate();
                            addUsedItem.setNotes(textInputItemNote.getText().toString());

                            ((HomeActivityNew) context).deductUsedItems(addUsedItem);

                        }
                        dialog.dismiss();
                        AppCompatActivity activity = (AppCompatActivity) context;
                        OwnerItemInventoryFragmentNew myFragment = new OwnerItemInventoryFragmentNew();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    }
                } else {
                    item_issue_quantity.setError(context.getString(R.string.please_enter_valid_quantity));
                }
            }
        });
        dialog.show();
    }


    public void setPhoto(Bitmap bitmap) {
        mImageview.setBackground(null);
        imageview_expense_hint.setVisibility(View.GONE);
        captureimg.setVisibility(View.GONE);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName = timeStamp + "_" + "ItemImg" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mEncodedBaseString = Base64.encodeToString(b, Base64.DEFAULT);
        FWLogger.logInfo(TAG, "FROM ADD ITEM DIALOG : " + mEncodedBaseString);
        mImageview.setImageBitmap(bitmap);
    }

    public void setPhotoUpdate(Bitmap bitmap) {
        mImageview.setBackground(null);
        imageview_expense_hint.setVisibility(View.GONE);
        captureimg.setVisibility(View.GONE);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName = timeStamp + "_" + "ItemImgUpdate" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mEncodedBaseString = Base64.encodeToString(b, Base64.DEFAULT);
        FWLogger.logInfo(TAG, "FROM ADD ITEM DIALOG : " + mEncodedBaseString);
        mImageview.setImageBitmap(bitmap);
    }


    public interface ImageUpdateListener {
        void onImageCaptured(Bitmap bitmap, String whichImage, int imageNo);
    }


    /*private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    mActivity.requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            },
                            ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }*/
}
