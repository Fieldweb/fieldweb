package com.corefield.fieldweb.FieldWeb.OpenAI;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.OpenAIResponseAdapter;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.ApiClientForOpenAI;
import com.corefield.fieldweb.Util.FWLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OpenAIFragment extends Fragment {
    protected static String TAG = OpenAIFragment.class.getSimpleName();

    private View mRootView;
    String requiredstring, srt;
    private Call<ResponseBody> call;

    private String AI_chatgpt_key_1, AI_chatgpt_key_2, AI_chatgpt_key_3, AI_chatgpt_key_4, AI_chatgpt_key_5, Intercom_key, Intercom_key_id;
    RecyclerView recyclerView;
    LinearLayout layoutaichathint;
    TextView hint1, hint2, hint3, hint4;
    ProgressBar aiprogressbar;
    OpenAIResponseAdapter adapter;
    List<String> userResponseList = new ArrayList<>();
    List<String> aiResponseList = new ArrayList<>();
    //populate the lists with data
    TextView aidown;
    ImageView imgaidown;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.open_ai_fragment, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.AI));

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        recyclerView = mRootView.findViewById(R.id.recycler_view);
        adapter = new OpenAIResponseAdapter(userResponseList, aiResponseList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        layoutaichathint = mRootView.findViewById(R.id.aihint);
        aiprogressbar = mRootView.findViewById(R.id.ai_progressbar);
        hint1 = mRootView.findViewById(R.id.hint1);
        hint2 = mRootView.findViewById(R.id.hint2);
        hint3 = mRootView.findViewById(R.id.hint3);
        hint4 = mRootView.findViewById(R.id.hint4);

        aidown = mRootView.findViewById(R.id.aidown);
        imgaidown = mRootView.findViewById(R.id.img_aidown);

        ApiClientForOpenAI.ApiInterface apiService = ApiClientForOpenAI.getClient().create(ApiClientForOpenAI.ApiInterface.class);
        EditText inputText = mRootView.findViewById(R.id.etMessage);
        //TextView textView = findViewById(R.id.textView);
        ImageButton sendButton = mRootView.findViewById(R.id.btnSend);
        RelativeLayout aiView = mRootView.findViewById(R.id.aiView);

        new Fetch().execute(new String[0]);

        hint1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText.setText(hint1.getText().toString());
            }
        });
        hint2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText.setText(hint2.getText().toString());
            }
        });
        hint3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText.setText(hint3.getText().toString());
            }
        });
        hint4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText.setText(hint4.getText().toString());
            }
        });
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aiprogressbar.setVisibility(View.VISIBLE);
                String userInput = inputText.getText().toString();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("prompt", userInput);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("model", "gpt-3.5-turbo-instruct");
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("temperature", 0.9);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("max_tokens", 4090);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                /*try {
                    jsonObject.put("top_p", 1);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("frequency_penalty", 0.0);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("presence_penalty", 0.6);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                try {
                    jsonObject.put("stop", Arrays.asList(" Human:", " AI:"));
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }*/
                RequestBody requestBody = RequestBody.create(jsonObject.toString(), MediaType.parse("application/json"));
                Calendar c = Calendar.getInstance();
                int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

                if (timeOfDay >= 0 && timeOfDay < 10) {
                    call = apiService.getAnswer("Bearer " + AI_chatgpt_key_3, requestBody);
                } else if (timeOfDay >= 10 && timeOfDay < 13) {
                    call = apiService.getAnswer("Bearer " + AI_chatgpt_key_1, requestBody);
                } else if (timeOfDay >= 13 && timeOfDay < 16) {
                    call = apiService.getAnswer("Bearer " + AI_chatgpt_key_4, requestBody);
                } else if (timeOfDay >= 16 && timeOfDay < 19) {
                    call = apiService.getAnswer("Bearer " + AI_chatgpt_key_5, requestBody);
                } else if (timeOfDay >= 19 && timeOfDay < 24) {
                    call = apiService.getAnswer("Bearer " + AI_chatgpt_key_3, requestBody);
                }
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                String json = response.body().string();
                                JSONObject jsonResponseObject = new JSONObject(json);
                                JSONArray jsonResponseArray = jsonResponseObject.getJSONArray("choices");

                                for (int n = 0; n < jsonResponseArray.length(); n++) {
                                    JSONObject object = jsonResponseArray.getJSONObject(n);
                                    requiredstring = object.getString("text").replaceAll("\n\n", "").trim();
                                }

                                aiView.setVisibility(View.GONE);
                                layoutaichathint.setVisibility(View.GONE);
                                userResponseList.add(inputText.getText().toString());
                                aiResponseList.add(requiredstring);
                                aiprogressbar.setVisibility(View.GONE);

                                // Clear the input EditText
                                inputText.setText("");

                                adapter.notifyDataSetChanged();

                                View view = getView().getRootView();
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //aiView.setVisibility(View.GONE);
                            //layoutaichathint.setVisibility(View.GONE);
                            imgaidown.setVisibility(View.VISIBLE);
                            aidown.setVisibility(View.VISIBLE);
                            aiprogressbar.setVisibility(View.GONE);

                            // Clear the input EditText
                            inputText.setText("");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("Error", t.getMessage());
                    }
                });
            }
        });
    }

    private class Fetch extends AsyncTask<String, String, String> {
        private Fetch() {
        }

        /* access modifiers changed from: protected */


        /* access modifiers changed from: protected */
        public String doInBackground(String... strArr) {
            HttpHandler httpHandler = new HttpHandler();
            String url = "https://fieldweb.co.in/api/ai.json";
            String jsonStr = httpHandler.makeServiceCall(url);
            Log.d(TAG, "jsonparser: " + jsonStr);
            jsonparser(jsonStr);
            return null;
        }
    }

    public void jsonparser(String str) {
        if (str != null) {
            try {
                Log.d(TAG, "jsonparser: " + "Json");
                //  JSONObject data=new JSONObject(str).getJSONObject("name");
                //  JSONArray children=data.getJSONArray("children");
                JSONArray jSONArray = new JSONObject(str).getJSONArray("key");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    AI_chatgpt_key_1 = jSONObject.getString("AI_chatgpt_key_1");
                    AI_chatgpt_key_2 = jSONObject.getString("AI_chatgpt_key_2");
                    AI_chatgpt_key_3 = jSONObject.getString("AI_chatgpt_key_3");
                    AI_chatgpt_key_4 = jSONObject.getString("AI_chatgpt_key_4");
                    AI_chatgpt_key_5 = jSONObject.getString("AI_chatgpt_key_5");
                    /*Intercom_key = jSONObject.getString("Intercom_key");
                    Intercom_key_id = jSONObject.getString("Intercom_key_id");*/
                    Log.d(TAG, "jsonparser: " + AI_chatgpt_key_1 + AI_chatgpt_key_1 + AI_chatgpt_key_1 + AI_chatgpt_key_1);

                }
            } catch (JSONException unused) {
            }
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        transaction.replace(R.id.home_fragment_container, fragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }
}