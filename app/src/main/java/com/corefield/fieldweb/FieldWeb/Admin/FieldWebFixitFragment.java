package com.corefield.fieldweb.FieldWeb.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Fragment Controller for FieldFinz
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show FieldFinz in WebView
 */
public class FieldWebFixitFragment extends Fragment {
    protected static String TAG = PrivacyPolicyFragment.class.getSimpleName();
    private View mRootView;
    private ProgressDialog mProgressDialog;
    private String mType;
    private String mUrl;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_fieldwebfixit, container, false);

        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            mType = getBundle.getString("TYPE");
        }
        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.fixit));
        //((HomeActivityNew) getActivity()).mToolbar.setTitle("FieldFinz");

        try {
            // FIREBASE TRIGGER EVENT
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(getActivity()).logEvent(FirebaseGoogleAnalytics.Event.FIELDWEBFIXIT_CLICK, bundle);
        } catch (Exception ex) {
            ex.getMessage();
        }

        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        WebView privacyPolicyWebView = (WebView) mRootView.findViewById(R.id.webView_privacy_policy);
        WebSettings webSettings = privacyPolicyWebView.getSettings();
        privacyPolicyWebView.setWebViewClient(new MyBrowser());

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

       /* mUrl = "https://www.product.corefield.co.in/fixit-listing/";*/
        mUrl = "https://fieldwebfixit.com/";

        privacyPolicyWebView.getSettings().setLoadsImagesAutomatically(true);
        privacyPolicyWebView.getSettings().setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setSupportZoom(true);
        webSettings.setDefaultTextEncodingName("utf-8");
        privacyPolicyWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        privacyPolicyWebView.loadUrl(mUrl);
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.fieldweb));
        super.onDestroyView();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error:" + description, Toast.LENGTH_SHORT).show();

        }
    }
}
