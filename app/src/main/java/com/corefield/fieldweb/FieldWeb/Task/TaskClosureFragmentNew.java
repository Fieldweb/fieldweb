package com.corefield.fieldweb.FieldWeb.Task;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListAdapterNew;
import com.corefield.fieldweb.Adapter.NotesAdapterNew;
import com.corefield.fieldweb.Adapter.TaskClosureMultitemListAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskClosureDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.Signature;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.corefield.fieldweb.Util.ToggleRadioButton;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * //
 * Created by CFS on 5/7/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Task
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This fragment is used to submit the task closure form
 * //
 **/
public class TaskClosureFragmentNew extends Fragment implements RecyclerTouchListener, BaseActivity.RequestPermissionsUpdateListener, View.OnClickListener, Signature.OnSignatureSave, OnSaveRating, TaskClosureMultitemListAdapter.MultipleUsdItemPostListener {
    //Variables
    private String TAG = TaskClosureFragmentNew.class.getSimpleName();
    private float mRating = 0;
    private int mTaskId = 0;
    private int mOwnerId = 0;
    private int mWorkModeId = 0;
    private String mWorkMode = "";
    private String mCustomerEmail = "";
    private int mImageNo;
    private String mWhichImage = "";
    private Uri mImageUri;
    private String mFieldImage = "Field", mPersonName = "", mSignedMobileNo = "", mFieldImage2 = "Field2", mFieldImage3 = "Field3";
    private String mCapturedImagePath = "";
    private String mSignatureEncodedBaseString = null, mFieldEncodedBaseString = null, mFieldEncodedBaseString2 = null, mFieldEncodedBaseString3 = null;
    //View's
    private View mRootView;
    private RecyclerView mRecyclerViewNote, mRecyclerViewDeviceList, recyclerViewMultiItem_list;
    private ImageView mImageViewFieldPhoto, mImageViewFieldPhoto2, mImageViewFieldPhoto3;
    private Button mButtonEndTask;
    private ToggleRadioButton mRadioButtonInstallation, mRadioButtonRepair, mRadioButtonService;
    private RelativeLayout mRelativeLayoutSignatureWrapper;
    private ImageView mImageViewSignature;
    private TextView mTextViewSignatureHint;
    private LinearLayoutManager mLayoutManager;
    private LinearLayout linear_layout_multi_item_list;
    ArrayList<TaskClosure.UsedItemDetailsDto> finalUsedItemPostArray = new ArrayList<TaskClosure.UsedItemDetailsDto>();

    //Task Closure DTO
    private TaskClosure.ResultData mTaskClosureResultData = new TaskClosure.ResultData();

    //Notes DTO
    private TaskClosure.TechnicalNotedto mTechnicalNoteDTOFirst = new TaskClosure.TechnicalNotedto();
    private List<TaskClosure.TechnicalNotedto> mTechnicalNoteDTOs = new ArrayList<>();
    private List<TaskClosure.TechnicalNotedto> mTempNoteList;

    //Device List DTO
    private TaskClosure.DeviceInfoList mDeviceInfoListFirst = new TaskClosure.DeviceInfoList();
    private List<TaskClosure.DeviceInfoList> mDeviceInfoLists = new ArrayList<>();
    private List<TaskClosure.DeviceInfoList> mTempDeviceList;

    //Other, Custom
    private NotesAdapterNew mTaskClosureNoteAdapter;
    private TaskClosureMultitemListAdapter taskClosureMultitemListAdapter;
    private DeviceListAdapterNew mDeviceListAdapter;
    private DeviceListAdapterNew.ImageUpdateListener mImageUpdateListener;

    private TasksList.ResultData mResultData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.task_closure_fragment_new, container, false);

        Bundle getBundle = this.getArguments();
        if (getBundle != null)
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.task_details_title);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIt();
        manageBackPress();
        return mRootView;
    }

    private void inIt() {
        //getArguments
        mTaskId = mResultData.getId();
        mOwnerId = mResultData.getOwnerId();
        mCustomerEmail = mResultData.getCustomerEmailId();
        FWLogger.logInfo(TAG, "Task Id : " + mTaskId + "  OwnerId : " + mOwnerId);


        try {
            List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList = new ArrayList<TasksList.MultipleItemAssigned>();
            multipleItemAssignedArrayList = mResultData.getMultipleItemAssigned();

            if (multipleItemAssignedArrayList.size() > 0) {
                linear_layout_multi_item_list = mRootView.findViewById(R.id.linear_layout_multi_item_list);
                recyclerViewMultiItem_list = mRootView.findViewById(R.id.recyclerView_multi_item_list);
                taskClosureMultitemListAdapter = new TaskClosureMultitemListAdapter(this, getContext(), multipleItemAssignedArrayList);
                mLayoutManager = new LinearLayoutManager(getContext());
                recyclerViewMultiItem_list.setLayoutManager(mLayoutManager);
                recyclerViewMultiItem_list.setAdapter(taskClosureMultitemListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        //Notes
        mTechnicalNoteDTOFirst.setId(0);
        mTechnicalNoteDTOs.add(mTechnicalNoteDTOFirst);
        mTaskClosureNoteAdapter = new NotesAdapterNew(mTechnicalNoteDTOs, getContext(), this);
        mRecyclerViewNote = mRootView.findViewById(R.id.recyclerView_note);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewNote.setLayoutManager(mLayoutManager);
        mRecyclerViewNote.setAdapter(mTaskClosureNoteAdapter);

        //DeviceList
        mDeviceInfoListFirst.setId(0);
        mDeviceInfoLists.add(mDeviceInfoListFirst);
        mDeviceListAdapter = new DeviceListAdapterNew(mDeviceInfoLists, getContext(), this);
        mRecyclerViewDeviceList = mRootView.findViewById(R.id.recyclerView_device_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewDeviceList.setLayoutManager(mLayoutManager);
        mRecyclerViewDeviceList.setAdapter(mDeviceListAdapter);

        //Field Photo
        mImageViewFieldPhoto = mRootView.findViewById(R.id.imageview_field_photo);
        mImageViewFieldPhoto.setOnClickListener(this);
        mImageViewFieldPhoto2 = mRootView.findViewById(R.id.imageview_photo2);
        mImageViewFieldPhoto2.setOnClickListener(this);
        mImageViewFieldPhoto3 = mRootView.findViewById(R.id.imageview_photo3);
        mImageViewFieldPhoto3.setOnClickListener(this);
        //Work Mode
        mRadioButtonInstallation = mRootView.findViewById(R.id.radiobutton_installation);
        mRadioButtonRepair = mRootView.findViewById(R.id.radiobutton_repair);
        mRadioButtonService = mRootView.findViewById(R.id.radiobutton_service);

        mRadioButtonInstallation.setOnClickListener(this);
        mRadioButtonRepair.setOnClickListener(this);
        mRadioButtonService.setOnClickListener(this);

        //Signature
        mRelativeLayoutSignatureWrapper = mRootView.findViewById(R.id.relative_layout_signature_wrapper);
        mRelativeLayoutSignatureWrapper.setOnClickListener(this);
        mImageViewSignature = mRootView.findViewById(R.id.imageView_customer_signature);
        mTextViewSignatureHint = mRootView.findViewById(R.id.imageView_add_signature_hint);

        //EndTask
        mButtonEndTask = mRootView.findViewById(R.id.button_end_task_task_closure);
        mButtonEndTask.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageview_field_photo:
                pickImage(mFieldImage, 1, null);
                break;

            case R.id.imageview_photo2:
                pickImage(mFieldImage2, 2, null);
                break;
            case R.id.imageview_photo3:
                pickImage(mFieldImage3, 3, null);
                break;

            case R.id.radiobutton_installation:
                if (mWorkModeId == Constant.WorkMode.INSTALLATION_ID && mWorkMode.equalsIgnoreCase(Constant.WorkMode.INSTALLATION_STRING)) {
                    mWorkModeId = 0;
                    mWorkMode = "";
                    mRadioButtonRepair.setError(null);
                    mRadioButtonInstallation.setError(null);
                    mRadioButtonService.setError(null);
                    mRadioButtonRepair.setChecked(false);
                    mRadioButtonInstallation.setChecked(false);
                    mRadioButtonService.setChecked(false);
                } else {
                    mRadioButtonRepair.setError(null);
                    mRadioButtonRepair.setChecked(false);
                    mRadioButtonService.setChecked(false);
                    mRadioButtonService.setError(null);
                    mWorkModeId = Constant.WorkMode.INSTALLATION_ID;
                    mWorkMode = Constant.WorkMode.INSTALLATION_STRING;
                }
                break;
            case R.id.radiobutton_repair:
                if (mWorkModeId == Constant.WorkMode.REPAIR_ID && mWorkMode.equalsIgnoreCase(Constant.WorkMode.REPAIR_STRING)) {
                    mWorkModeId = 0;
                    mWorkMode = "";
                    mRadioButtonRepair.setError(null);
                    mRadioButtonInstallation.setError(null);
                    mRadioButtonService.setError(null);
                    mRadioButtonRepair.setChecked(false);
                    mRadioButtonInstallation.setChecked(false);
                    mRadioButtonService.setChecked(false);
                } else {
                    mRadioButtonInstallation.setChecked(false);
                    mRadioButtonInstallation.setError(null);
                    mRadioButtonService.setChecked(false);
                    mRadioButtonService.setError(null);
                    mWorkModeId = Constant.WorkMode.REPAIR_ID;
                    mWorkMode = Constant.WorkMode.REPAIR_STRING;
                }
                break;
            case R.id.radiobutton_service:
                if (mWorkModeId == Constant.WorkMode.SERVICE_ID && mWorkMode.equalsIgnoreCase(Constant.WorkMode.SERVICE_STRING)) {
                    mWorkModeId = 0;
                    mWorkMode = "";
                    mRadioButtonRepair.setError(null);
                    mRadioButtonInstallation.setError(null);
                    mRadioButtonService.setError(null);
                    mRadioButtonRepair.setChecked(false);
                    mRadioButtonInstallation.setChecked(false);
                    mRadioButtonService.setChecked(false);
                } else {
                    mRadioButtonInstallation.setChecked(false);
                    mRadioButtonInstallation.setError(null);
                    mRadioButtonRepair.setError(null);
                    mRadioButtonRepair.setChecked(false);
                    mWorkModeId = Constant.WorkMode.SERVICE_ID;
                    mWorkMode = Constant.WorkMode.SERVICE_STRING;
                }
                break;
            case R.id.relative_layout_signature_wrapper:
                TaskClosureDialogNew taskClosureDialog = TaskClosureDialogNew.getInstance();
                taskClosureDialog.signatureAndRatingDialog(getContext(), "Sign", this, this);
                break;
            case R.id.button_end_task_task_closure:
                if (validate()) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("taskClosureResult", mTaskClosureResultData);
                    bundle.putSerializable("taskResult", mResultData);
                    SummaryDetailsFragment summaryDetailsFragment = new SummaryDetailsFragment();
                    summaryDetailsFragment.setArguments(bundle);

                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_fragment_container, summaryDetailsFragment);
                    transaction.isAddToBackStackAllowed();
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                break;
        }
    }

    @Override
    public void onSignature(Bitmap bitmap) {
        FWLogger.logInfo(TAG, "onSignature ");
        mTextViewSignatureHint.setError(null);

        if (bitmap != null) {
            mSignatureEncodedBaseString = ImageUtils.ConvertBitmapToString(bitmap);
            FWLogger.logInfo(TAG, "Base64 : " + mSignatureEncodedBaseString);
            mImageViewSignature.setBackground(null);
            mImageViewSignature.setImageBitmap(bitmap);
            mImageViewSignature.setScaleType(ImageView.ScaleType.FIT_XY);
            mTextViewSignatureHint.setText("");
        } else {
            FWLogger.logInfo(TAG, "Null Bitmap");
            mImageViewSignature.setBackground(null);
            mImageViewSignature.setImageBitmap(null);
            mTextViewSignatureHint.setText(R.string.click_below_customer_signature);
        }
    }

    @Override
    public void onRating(float rating, String personName, String signedMobileNo) {
        if (rating != 0) {
            mRating = rating;
            mTextViewSignatureHint.setError(null);
            mPersonName = personName;
            mSignedMobileNo = null;
            mSignedMobileNo = signedMobileNo;
            FWLogger.logInfo(TAG, "No = " + signedMobileNo);
        }
    }

    @Override
    public void onClick(View view, int position) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        HomeActivityNew.isOwner = true;// TODO check
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            FWLogger.logInfo(TAG, "Camera");
            try {
                getActivity().getContentResolver().notifyChange(mImageUri, null);
                Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), mImageUri, mCapturedImagePath);
                //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                String filePath = CameraUtils.getFilePathFromURI(mImageUri, getContext());
                //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                if (mWhichImage.equalsIgnoreCase(mFieldImage)) {
                    setFieldPhoto(bitmap);
                } else if (mWhichImage.equalsIgnoreCase(mFieldImage2)) {
                    setFieldPhoto2(bitmap);
                } else if (mWhichImage.equalsIgnoreCase(mFieldImage3)) {
                    setFieldPhoto3(bitmap);
                } else {
                    mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                }
            } catch (Exception e) {
                Toast.makeText(getActivity(), getResources().getString(R.string.failed_to_load), Toast.LENGTH_SHORT).show();
                FWLogger.logInfo("Camera", e.toString());
            }

        } else if (requestCode == BaseActivity.PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                FWLogger.logInfo(TAG, "Gallery");
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = ImageUtils.handleSamplingAndRotationBitmap(getContext(), selectedImage, null);
                    //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                    String filePath = CameraUtils.getFilePathFromURI(selectedImage, getContext());
                    //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                    bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                    if (mWhichImage.equalsIgnoreCase(mFieldImage)) {
                        setFieldPhoto(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase(mFieldImage2)) {
                        setFieldPhoto2(bitmap);
                    } else if (mWhichImage.equalsIgnoreCase(mFieldImage3)) {
                        setFieldPhoto3(bitmap);
                    } else {
                        mImageUpdateListener.onImageCaptured(bitmap, mWhichImage, mImageNo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.sorry_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {

            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                captureImage();
            } else if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
            }
        }
    }

    private boolean validate() {
        if (!addDeviceListToTaskClosure()) {
            return false;// add error message
        } else if (!addNotesToTaskClosure()) {
            Toast.makeText(getContext(), getResources().getString(R.string.note_error), Toast.LENGTH_SHORT).show();
            return false; //add error message
        } else if (mSignatureEncodedBaseString == null) {
            Toast.makeText(getContext(), getResources().getString(R.string.signature_error), Toast.LENGTH_SHORT).show();
            mTextViewSignatureHint.setError(getResources().getString(R.string.signature_error));
            return false;
        } /*else if (mFieldEncodedBaseString == null) {
            Toast.makeText(getContext(), getResources().getString(R.string.field_photo_error), Toast.LENGTH_SHORT).show();
            return false;
        }*/ else if (mRating == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.rate_us), Toast.LENGTH_SHORT).show();
            mTextViewSignatureHint.setError(getResources().getString(R.string.rate_us));
            return false;
        } else if (mWorkModeId == 0 && mWorkMode.equalsIgnoreCase("")) {
            Toast.makeText(getContext(), getResources().getString(R.string.work_mode_error), Toast.LENGTH_SHORT).show();
            mRadioButtonRepair.setError(getResources().getString(R.string.work_mode_error));
            return false;
        } else {
            mTaskClosureResultData.setFieldPhoto(mFieldEncodedBaseString);// Image 1
            mTaskClosureResultData.setFieldPhoto1(mFieldEncodedBaseString2); // Image 2
            mTaskClosureResultData.setFieldPhoto2(mFieldEncodedBaseString3); // Image 3

            mTaskClosureResultData.setUserId(SharedPrefManager.getInstance(getContext()).getUserId());
            mTaskClosureResultData.setTaskId(mTaskId);
            mTaskClosureResultData.setCustomerSignatureImage(mSignatureEncodedBaseString);
            mTaskClosureResultData.setWorkModeId(mWorkModeId);
            mTaskClosureResultData.setWorkModeType(mWorkMode);
            mTaskClosureResultData.setRatingBar(mRating);

            if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                mTaskClosureResultData.setTaskState(TaskState.ENDED_NO_PAYMENT);//TODO need to create this
                mTaskClosureResultData.setTaskStatus(TaskStatus.ON_GOING);
            } else {
                mTaskClosureResultData.setTaskState(TaskState.PAYMENT_RECEIVED);//TODO need to create this
                mTaskClosureResultData.setTaskStatus(TaskStatus.COMPLETED);
            }
            mTaskClosureResultData.setSignedBy(mPersonName);
            if (mSignedMobileNo != null && !mSignedMobileNo.isEmpty())
                mTaskClosureResultData.setMobileNo(Long.parseLong(mSignedMobileNo));

            // POST USED ITEM DETAILS DTO DATA
            try {
                List<TaskClosure.UsedItemDetailsDto> usedItemDetailsDtoList = new ArrayList<TaskClosure.UsedItemDetailsDto>();
                //usedItemDetailsDtoList = mTaskClosureResultData.getUsedItemDetailsDto();

                if (finalUsedItemPostArray.size() > 0) {

                    for (int i = 0; i < finalUsedItemPostArray.size(); i++) {
                        TaskClosure.UsedItemDetailsDto usedItemDetailsDto = new TaskClosure.UsedItemDetailsDto();
                        usedItemDetailsDto.setId(mTaskClosureResultData.getId());
                        usedItemDetailsDto.setItemId(finalUsedItemPostArray.get(i).getItemId());
                        usedItemDetailsDto.setItemIssuedId(finalUsedItemPostArray.get(i).getItemIssuedId());
                        usedItemDetailsDto.setTaskId(mTaskClosureResultData.getTaskId());
                        usedItemDetailsDto.setAvlQty(0);
                        usedItemDetailsDto.setAssignedQty(finalUsedItemPostArray.get(i).getAssignedQty());
                        usedItemDetailsDto.setUsedQty(finalUsedItemPostArray.get(i).getUsedQty());
                        usedItemDetailsDto.setUserId(SharedPrefManager.getInstance(getContext()).getUserId());
                        usedItemDetailsDto.setCreatedBy(SharedPrefManager.getInstance(getContext()).getUserId());
                        // usedItemDetailsDto.setCreatedDate("");
                        usedItemDetailsDto.setUpdateBy(SharedPrefManager.getInstance(getContext()).getUserId());
                        // usedItemDetailsDto.setUpdatedDate("");
                        //  usedItemDetailsDto.setNotes("");

                        usedItemDetailsDtoList.add(usedItemDetailsDto);
                        mTaskClosureResultData.setUsedItemDetailsDto(usedItemDetailsDtoList);
                    }
                }

            } catch (Exception ex) {
                ex.getMessage();
            }

            return true;
        }
    }

    public boolean addNotesToTaskClosure() {
        //verify list size and if the size is grater than 1 (i.e 2 or more ) then remove first item as it was added for hint and copy remaining into temp to send to API
        boolean ret = true;
        if (mTechnicalNoteDTOs.size() > 1) {
            mTempNoteList = new ArrayList<>();
            mTempNoteList.clear();
            for (int i = 0; i < mTechnicalNoteDTOs.size(); i++) {
                if (mTechnicalNoteDTOs.get(i).getId() != 0) {
                    if (mTechnicalNoteDTOs.get(i).getTechnicalNote1() == null || mTechnicalNoteDTOs.get(i).getTechnicalNote1().equalsIgnoreCase("")) {
                        TextView textView = mRecyclerViewNote.getLayoutManager().findViewByPosition(i).findViewById(R.id.textView_note_summary_text);
                        if (textView != null)
                            textView.setError(getResources().getString(R.string.note_add_something));
                        ret = false;
                        break;
                    } else {
                        mTempNoteList.add(mTechnicalNoteDTOs.get(i));
                    }
                }
            }
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putInt(FirebaseGoogleAnalytics.Param.NO_OF_NOTES, mTempNoteList.size());
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ADD_NOTES, bundle);

            mTaskClosureResultData.setTechnicalNotedto(mTempNoteList);
        }
        return ret;
    }

    private boolean addDeviceListToTaskClosure() {
        //verify list size and if the size is grater than 1 (i.e 2 or more ) then remove first item as it was added for hint and copy remaining into temp to send to API
        boolean ret = true;
        if (mDeviceInfoLists.size() > 1) {
            mTempDeviceList = new ArrayList<>();
            mTempDeviceList.clear();
            for (int i = 0; i < mDeviceInfoLists.size(); i++) {
                EditText deviceName = null;
                EditText modelNo = null;
                try {
                    FWLogger.logInfo(TAG, "CHILD_COUNT : " + mRecyclerViewDeviceList.getLayoutManager().getChildCount());
                    deviceName = mRecyclerViewDeviceList.getLayoutManager().findViewByPosition(i).findViewById(R.id.edittext_device_name);
                    modelNo = mRecyclerViewDeviceList.getLayoutManager().findViewByPosition(i).findViewById(R.id.edittext_serial_no);
                } catch (Exception e) {
                    FWLogger.logInfo(TAG, "LOG : " + e.getMessage());
                }
                if (mDeviceInfoLists.get(i).getId() != 0) {
                    if (mDeviceInfoLists.get(i).getDeviceName() == null || mDeviceInfoLists.get(i).getDeviceName().equalsIgnoreCase("")) {
                        if (deviceName != null) {
                            deviceName.setError(getResources().getString(R.string.please_add_device_name));
                            Toast.makeText(getContext(), getResources().getString(R.string.please_add_device_name), Toast.LENGTH_SHORT).show();
                            ret = false;
                        }
                        break;
                    } else if (!mDeviceInfoLists.get(i).getDeviceName().isEmpty() && (Character.isWhitespace(mDeviceInfoLists.get(i).getDeviceName().charAt(0)) || mDeviceInfoLists.get(i).getDeviceName().trim().isEmpty())) {
                        deviceName.setError(getContext().getResources().getString(R.string.space_is_not_allowed));
                        ret = false;
                        break;
                    } else if (mDeviceInfoLists.get(i).getModelNumber() == null || mDeviceInfoLists.get(i).getModelNumber().equalsIgnoreCase("")) {
                        if (modelNo != null)
                            modelNo.setError(getResources().getString(R.string.please_add_serial_no));
                        Toast.makeText(getContext(), getResources().getString(R.string.please_add_serial_no), Toast.LENGTH_SHORT).show();
                        ret = false;
                        break;
                    } else if (!mDeviceInfoLists.get(i).getModelNumber().isEmpty() && (Character.isWhitespace(mDeviceInfoLists.get(i).getModelNumber().charAt(0)) || mDeviceInfoLists.get(i).getModelNumber().trim().isEmpty())) {
                        modelNo.setError(getContext().getResources().getString(R.string.space_is_not_allowed));
                        ret = false;
                        break;
                    } else if ((mDeviceInfoLists.get(i).getDevicePhoto1() == null || mDeviceInfoLists.get(i).getDevicePhoto1().equalsIgnoreCase("")) && (mDeviceInfoLists.get(i).getDevicePhoto2() == null || mDeviceInfoLists.get(i).getDevicePhoto2().equalsIgnoreCase("")) && (mDeviceInfoLists.get(i).getDevicePhoto3() == null || mDeviceInfoLists.get(i).getDevicePhoto3().equalsIgnoreCase(""))) {
                        TextView textView = mRecyclerViewDeviceList.getLayoutManager().findViewByPosition(i).findViewById(R.id.textView_remove_device_image_one);
                        if (textView != null)
                            textView.setError(getResources().getString(R.string.please_add_photo));
                        Toast.makeText(getContext(), getResources().getString(R.string.please_add_photo), Toast.LENGTH_SHORT).show();
                        ret = false;
                        break;
                    } else {
                        mTempDeviceList.add(mDeviceInfoLists.get(i));
                    }
                }
            }
            mTaskClosureResultData.setDeviceInfoList(mTempDeviceList);
        }
        return ret;
    }

    public void pickImage(String whichImage, int imageNo, DeviceListAdapterNew.ImageUpdateListener imageUpdateListener) {
        checkCameraPermission();
        mWhichImage = whichImage;
        mImageNo = imageNo;
        mImageUpdateListener = imageUpdateListener;
        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        builder.setTitle(getResources().getString(R.string.select_option));
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(getActivity().getApplicationContext())) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.camera_not_supported), Toast.LENGTH_LONG).show();
                    }
                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(TaskClosureFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {

                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(TaskClosureFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, BaseActivity.PICK_IMAGE_GALLERY);
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            //photo = new File(Environment.getExternalStorageDirectory(), getFileName() + ".jpg");
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            photo = File.createTempFile(getFileName(), ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCapturedImagePath = photo.getAbsolutePath();
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mImageUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private String getFileName() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        String imageFileName = timeStamp + "_" + mWhichImage + "_" + mImageNo + "_";
        return imageFileName;
    }

    private void setFieldPhoto(Bitmap bitmap) {
        mImageViewFieldPhoto.setBackground(null);
//        mButtonUploadFieldPhoto.setError(null);
        mFieldEncodedBaseString = ImageUtils.ConvertBitmapToString(bitmap);
        FWLogger.logInfo(TAG, "Base64 : " + mFieldEncodedBaseString);
//        System.out.println("\n base : " + mFieldEncodedBaseString);
        mImageViewFieldPhoto.setImageBitmap(bitmap);
        mImageViewFieldPhoto.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private void setFieldPhoto2(Bitmap bitmap) {
        mImageViewFieldPhoto2.setBackground(null);
//        mButtonUploadFieldPhoto.setError(null);
        mFieldEncodedBaseString2 = ImageUtils.ConvertBitmapToString(bitmap);
        FWLogger.logInfo(TAG, "Base64 : " + mFieldEncodedBaseString2);
//        System.out.println("\n base : " + mFieldEncodedBaseString);
        mImageViewFieldPhoto2.setImageBitmap(bitmap);
        mImageViewFieldPhoto2.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private void setFieldPhoto3(Bitmap bitmap) {
        mImageViewFieldPhoto3.setBackground(null);
//        mButtonUploadFieldPhoto.setError(null);
        mFieldEncodedBaseString3 = ImageUtils.ConvertBitmapToString(bitmap);
        FWLogger.logInfo(TAG, "Base64 : " + mFieldEncodedBaseString3);
//        System.out.println("\n base : " + mFieldEncodedBaseString);
        mImageViewFieldPhoto3.setImageBitmap(bitmap);
        mImageViewFieldPhoto3.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivityNew) getActivity()).showHeader();
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    private boolean checkCamerPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                    }
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onMultipleUsdItemPostListener(ArrayList<TaskClosure.UsedItemDetailsDto> usedItemArry) {
        finalUsedItemPostArray = usedItemArry;
    }
}

