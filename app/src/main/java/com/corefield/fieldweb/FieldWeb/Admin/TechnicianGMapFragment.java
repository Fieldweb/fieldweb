package com.corefield.fieldweb.FieldWeb.Admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Fragment Controller for TechnicianListFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used show  technician list (owner login)
 */
public class TechnicianGMapFragment extends DialogFragment implements ValueEventListener, OnMapReadyCallback {
    protected static String TAG = TechnicianGMapFragment.class.getSimpleName();
    private View mRootView;
    private DatabaseReference mDatabaseReference;
    private double mTechLat = 0, mTechLong = 0;
    private LatLng mCoordinateDestination = null;
    private LatLng mCoordinateOrigin = null;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mMapFragment;
    private int mTechuserID;
    ImageView closeMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.technician_google_map, container, false);

        //NOTE: Log GA event
       /* Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_LIST, bundle);*/

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        //mMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        closeMap = (ImageView) mRootView.findViewById(R.id.closeMap);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

       /* try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                mTechuserID = getBundle.getInt("TUserID");
                mDatabaseReference = FirebaseDatabase.getInstance().getReference("" + mTechuserID);
                mDatabaseReference.addValueEventListener(this);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }*/

        /*closeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HomeActivityNew.class);
                startActivity(intent);
                getActivity().finish();
            }
        });*/
        return mRootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.Theme_Design_BottomSheetDialog);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        closeMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    public TechnicianGMapFragment(int techUserID) {
        try {
            mTechuserID = techUserID;
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


  /*  @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
        Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }*/

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        try {
            FWLogger.logInfo(TAG, "onDataChange From Technician List");
            String databaseLatitudeString = snapshot.child("lat").getValue().toString();
            String databaseLongitudeString = snapshot.child("lng").getValue().toString();

            mTechLat = Double.parseDouble(databaseLatitudeString);
            mTechLong = Double.parseDouble(databaseLongitudeString);
            mCoordinateOrigin = new LatLng(mTechLat, mTechLong);
            mCoordinateDestination = new LatLng(mTechLat, mTechLong);
            MapUtils.updateMarkerOnMap(true, getContext(), mGoogleMap, mCoordinateOrigin, mCoordinateDestination);
            FWLogger.logInfo(TAG, "onDataChange From Tech REAL TIME :" + databaseLatitudeString + "/" + databaseLongitudeString);
        } catch (Exception e) {
            e.getMessage();
            FWLogger.logInfo(TAG, "onDataChange From Tech REAL TIME Exception:" + e.getMessage());

        }
    }


    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        try {
            try {
                mDatabaseReference = FirebaseDatabase.getInstance().getReference("" + mTechuserID);
                mDatabaseReference.addValueEventListener(this);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
                mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
                mGoogleMap.getMinZoomLevel();
            } catch (Exception ex) {
                ex.getMessage();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
}