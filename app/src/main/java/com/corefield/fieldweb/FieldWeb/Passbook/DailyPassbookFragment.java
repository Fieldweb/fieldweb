package com.corefield.fieldweb.FieldWeb.Passbook;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.DailyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.DailyPassbookAsyncTask;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;

/**
 * Fragment Controller for DailyPassbookFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show passbook of daily bar graph
 */
public class DailyPassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = DailyPassbookFragment.class.getSimpleName();
    public List<DailyPassbook.GetCurrentWeekData> mDailyPassbook = null;
    private View mRootView;
    private BarChart mBarChart;
    private DailyPassbookAsyncTask mDailyPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.passbook_graph_fragment, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {
        mBarChart = mRootView.findViewById(R.id.barchart);
        mBarChart.setDescription("");
        mBarChart.setMaxVisibleValueCount(50);
        mBarChart.setPinchZoom(false);
        mBarChart.animateX(2000);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisLeft().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawGridLines(false);
        mDailyPassbookAsyncTask = new DailyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mDailyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(DailyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            DailyPassbook dailyPassbook = gson.fromJson(urlConnectionResponse.resultData, DailyPassbook.class);
            mDailyPassbook = dailyPassbook.getResultData().getGetCurrentWeekData();
            FWLogger.logInfo(TAG, "Check = " + dailyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            dailyPassbook.getResultData().getTotalEarned();
            dailyPassbook.getResultData().getTotalCredit();
            dailyPassbook.getResultData().getTotalExpenses();
            dailyPassbook.getResultData().getTotalDeduction();
            dailyPassbook.getResultData().getTotalOpening();
            /////////////////////////////////////////BAR CHART ////////////////////////////////////
            ArrayList<BarEntry> estimated = new ArrayList<>();

            for (int i = 0; i < mDailyPassbook.size(); i++) {
                estimated.add(new BarEntry(mDailyPassbook.get(i).getEstimated(), i));
            }

            ArrayList<BarEntry> earned = new ArrayList<>();
            for (int i = 0; i < mDailyPassbook.size(); i++) {
                earned.add(new BarEntry(mDailyPassbook.get(i).getEarned(), i));
            }

            ArrayList<BarEntry> expense = new ArrayList<>();
            for (int i = 0; i < mDailyPassbook.size(); i++) {
                expense.add(new BarEntry(mDailyPassbook.get(i).getExpenses(), i));
            }
            XAxis x = mBarChart.getXAxis();
            x.setSpaceBetweenLabels(0);
            x.setXOffset(.5f);
            x.setYOffset(.5f);

            BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
            expenseBar.setColors(new int[]{R.color.orange}, getContext());

            BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
            estimatedBar.setColors(new int[]{R.color.green}, getContext());

            BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
            earnedBar.setColors(new int[]{R.color.blue}, getContext());

            ArrayList<String> labels = new ArrayList<String>();
            labels.add("Mon");
            labels.add("Tue");
            labels.add("Wed");
            labels.add("Thu");
            labels.add("Fri");
            labels.add("Sat");
            labels.add("Sun");

            ArrayList<BarDataSet> dataSets = new ArrayList<>();
            dataSets.add(expenseBar);
            dataSets.add(estimatedBar);
            dataSets.add(earnedBar);
            BarData data = new BarData(labels, dataSets);
            mBarChart.setData(data);
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mDailyPassbookAsyncTask != null) {
            mDailyPassbookAsyncTask.cancel(true);
        }
        super.onDestroyView();
    }
}

