package com.corefield.fieldweb.FieldWeb.Passbook;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.WeeklyPassbook;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.WeeklyPassbookAsyncTask;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;

/**
 * Fragment Controller for Weekly Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for to show passbook of Weekly bar graph
 */
public class WeeklyPassbookFragment extends Fragment implements OnTaskCompleteListener {

    private static String TAG = WeeklyPassbookFragment.class.getSimpleName();
    public List<WeeklyPassbook.WeeklyData> mWeeklyPassbook = null;
    private View mRootView;
    private BarChart mBarChart;
    private WeeklyPassbookAsyncTask mWeeklyPassbookAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.passbook_graph_fragment, container, false);
        inIt();
        return mRootView;
    }

    private void inIt() {
        mBarChart = mRootView.findViewById(R.id.barchart);
        mBarChart.setDescription("");
        mBarChart.setMaxVisibleValueCount(50);
        mBarChart.setPinchZoom(false);
        mBarChart.animateX(2000);
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisLeft().setDrawGridLines(false);
        mBarChart.getXAxis().setDrawGridLines(false);

        mWeeklyPassbookAsyncTask = new WeeklyPassbookAsyncTask((getContext()), BaseAsyncTask.Priority.LOW, this);
        mWeeklyPassbookAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(WeeklyPassbook.class.getSimpleName())) {
            Gson gson = new Gson();
            WeeklyPassbook weeklyPassbook = gson.fromJson(urlConnectionResponse.resultData, WeeklyPassbook.class);
            mWeeklyPassbook = weeklyPassbook.getResultData().getWeeklyData();
            FWLogger.logInfo(TAG, "Check = " + weeklyPassbook.getMessage());
            ///////////////////////////////////////////////////////////////////////////////////////
            weeklyPassbook.getResultData().getTotalEarned();
            weeklyPassbook.getResultData().getTotalCredit();
            weeklyPassbook.getResultData().getTotalExpenses();
            weeklyPassbook.getResultData().getTotalDeduction();
            weeklyPassbook.getResultData().getTotalOpening();
            /////////////////////////////////////////BAR CHART ////////////////////////////////////
            ArrayList<BarEntry> estimated = new ArrayList<>();

            for (int i = 0; i < mWeeklyPassbook.size(); i++) {
                estimated.add(new BarEntry(mWeeklyPassbook.get(i).getEstimated(), i));
            }

            ArrayList<BarEntry> earned = new ArrayList<>();
            for (int i = 0; i < mWeeklyPassbook.size(); i++) {
                earned.add(new BarEntry(mWeeklyPassbook.get(i).getEarning(), i));
            }
            ArrayList<BarEntry> expense = new ArrayList<>();
            for (int i = 0; i < mWeeklyPassbook.size(); i++) {
                expense.add(new BarEntry(mWeeklyPassbook.get(i).getExpenses(), i));
            }
            XAxis x = mBarChart.getXAxis();
            x.setSpaceBetweenLabels(0);
            x.setXOffset(.5f);
            x.setYOffset(.5f);

            BarDataSet expenseBar = new BarDataSet(expense, "Total Expense");
            expenseBar.setColors(new int[]{R.color.orange}, getContext());

            BarDataSet estimatedBar = new BarDataSet(estimated, "Estimated Earning");
            estimatedBar.setColors(new int[]{R.color.green}, getContext());

            BarDataSet earnedBar = new BarDataSet(earned, "Total Earning");
            earnedBar.setColors(new int[]{R.color.blue}, getContext());

            ArrayList<String> labels = new ArrayList<String>();
            labels.add("1 week");
            labels.add("2 week");
            labels.add("3 week");
            labels.add("4 week");
            labels.add("5 week");
            labels.add("6 week");

            ArrayList<BarDataSet> dataSets = new ArrayList<>();
            dataSets.add(expenseBar);
            dataSets.add(estimatedBar);
            dataSets.add(earnedBar);
            BarData data = new BarData(labels, dataSets);
            mBarChart.setData(data);
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        if (mWeeklyPassbookAsyncTask != null) {
            mWeeklyPassbookAsyncTask.cancel(true);
        }

        super.onDestroyView();
    }
}
