package com.corefield.fieldweb.FieldWeb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.ChangePassword;
import com.corefield.fieldweb.DTO.FWUser;
import com.corefield.fieldweb.DTO.ForgotPassword;
import com.corefield.fieldweb.DTO.HealthAndSaftey.HealthAndSafety;
import com.corefield.fieldweb.DTO.Notification.RegisterDeviceId;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog;
import com.corefield.fieldweb.FieldWeb.HealthAndSafety.HealthAndSafetyActivity;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeSplashWhatsappActivity;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.LocaleUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Activity for Login Activity
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Activity  class is used for login function
 */
public class LoginTouchlessActivity extends BaseActivity implements OnTaskCompleteListener, BaseActivity.RequestPermissionsUpdateListener {

    protected static String TAG = LoginTouchlessActivity.class.getSimpleName();
    private Button button_Getotp;
    private EditText /*mEditTextUser, mEditTextPass,*/ editText_emailorMobile;
    private TextInputLayout mTextInputPassword;
    private ImageView mFWLogo;
    private TextView mTextViewResetPassword;
    //private GetHealthAndSafetyFeatureAsyncTask mGetHealthAndSafetyFeatureAsyncTask;
    private TextView mTextViewChooseLanguage, mTextViewSignup;
    private String mUserName = "";
    ImageView video, whatsapp;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_touchless);
       /* if (!checkAllPermissionEnabled()) {
            requestToEnableAllPermission(LoginTouchlessActivity.this, PERMISSION_REQUEST_ACCESS_ALL);
        }*/
        button_Getotp = findViewById(R.id.button_Getotp);
        editText_emailorMobile = findViewById(R.id.editText_emailorMobile);
        //
        whatsapp = (ImageView) findViewById(R.id.whatsapp);
        video = (ImageView) findViewById(R.id.video);

        // mEditTextUser = findViewById(R.id.editText_username);
        // mEditTextPass = findViewById(R.id.editText_password);
        //mTextInputPassword = findViewById(R.id.textInput_password);

        //mTextViewResetPassword = findViewById(R.id.reset_password);
        mTextViewSignup = findViewById(R.id.textView_Signup);
        mTextViewChooseLanguage = (TextView) findViewById(R.id.textView_choose_language);
        // check and set default locale
        if (SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage() != null) {  //&& !SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage().isEmpty()
            LocaleUtils.setLocale(LoginTouchlessActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
        } else {
            SharedPrefManager.getInstance(getApplicationContext()).setPreferredLanguage(Constant.PreferredLanguage.LOCALE_ENGLISH, 0);
            LocaleUtils.setLocale(LoginTouchlessActivity.this, SharedPrefManager.getInstance(getApplicationContext()).getPreferredLanguage());
        }

        mTextViewChooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDialog userDialog = UserDialog.getInstance();
                userDialog.changeLanguageDialog(LoginTouchlessActivity.this);
            }
        });

        mFWLogo = findViewById(R.id.textView_title);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
        mFWLogo.startAnimation(anim);

       /* if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            checkUserTypeForHealthAndSafetyFeature();
            return;
        }*/
       /* if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            checkUserTypeForHealthAndSafetyFeature();
            return;
        }*/
        button_Getotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLoginAttempt();
            }
        });
        mTextViewSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginTouchlessActivity.this, TouchLessSignupActivity.class);
                startActivity(intent);
                finish();
            }
        });
     /*   mTextViewResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordDialog passwordDialog = PasswordDialog.getInstance();
                passwordDialog.resetPasswordDialog(LoginTouchlessActivity.this);
            }
        });*/

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "+919354795965"; // replace with the phone number you want to message
                String message = "Hello, I am having trouble logging in to FieldWeb. Please help"; // replace with your custom message
                try {
                    // Encode the message to be URL-safe
                    String encodedMessage = URLEncoder.encode(message, "UTF-8");
                    // Create the WhatsApp intent with the phone number and message
                    Uri uri = Uri.parse("https://wa.me/" + phoneNumber + "/?text=" + encodedMessage);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send = new Intent(getApplicationContext(), YouTubeSplashWhatsappActivity.class);
                startActivity(send);
            }
        });

    }

    private void userLoginAttempt() {
        if (validateMobileNumorEmailId()) {
           /* mLoginAsyncTask = new TouchLessLoginAsyncTask(LoginTouchlessActivity.this, BaseAsyncTask.Priority.LOW, LoginTouchlessActivity.this);
            mLoginAsyncTask.execute(editText_emailorMobile.getText().toString(), "", CommonFunction.getUniqueDeviceID(LoginTouchlessActivity.this));*/

            try {
                if (Connectivity.isNetworkAvailableRetro(LoginTouchlessActivity.this)) {
                    Call<FWUser> call = RetrofitClient.getInstance(LoginTouchlessActivity.this).getMyApi().doTouchlessLogin(editText_emailorMobile.getText().toString(), "", CommonFunction.getUniqueDeviceID(LoginTouchlessActivity.this), "en");
                    call.enqueue(new retrofit2.Callback<FWUser>() {
                        @Override
                        public void onResponse(Call<FWUser> call, Response<FWUser> response) {
                            try {
                                if (response.code() == 200) {
                                    FWUser fwUser = response.body();

                                    FWLogger.logInfo(TAG, "Login Successful");

                                    if (fwUser != null) {
                                        if (fwUser.getCode().equalsIgnoreCase("200") && fwUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.login_successfully, LoginTouchlessActivity.this))) {
                                            //Toast.makeText(LoginTouchlessActivity.this, "OTP IS::" + fwUser.getResultdata().getOTP(), Toast.LENGTH_SHORT).show();
                                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser, CommonFunction.getUniqueDeviceID(LoginTouchlessActivity.this));
                                            int uniqueId = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                                            if (uniqueId != 0) {
                                                FirebaseCrashlytics.getInstance().setUserId("" + uniqueId);
                                            }
                                            //NOTE: Log GA event
                                            Bundle bundle = new Bundle();
                                            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, fwUser.getResultdata().getUserid());
                                            // bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, mEditTextUser.getText().toString());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, fwUser.getResultdata().getUsergroupname());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                            FirebaseAnalytics.getInstance(LoginTouchlessActivity.this).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                                            if (SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID() != null) {
                                                sendRegistrationToServer(SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID());
                                            } else {
                                                navigateToVerifyOTP();
                                            }
                                        } else {
                                            try {
                                                Toast.makeText(LoginTouchlessActivity.this, R.string.pls_sign_up, Toast.LENGTH_LONG).show();
                                                Intent intent = new Intent(LoginTouchlessActivity.this, TouchLessSignupActivity.class);
                                                intent.putExtra("LoginMobNumber", editText_emailorMobile.getText().toString().trim());
                                                startActivity(intent);
                                                finish();
                                            } catch (Exception ex) {
                                                ex.getMessage();
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<FWUser> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in UserLoginMobile API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(LoginTouchlessActivity.this);
                }

            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in UserLoginMobile API:");
                ex.getMessage();
            }
        }
    }

   /* public void forgotPassword(String userName) {
        String androidId = getUniqueDeviceID();
        SharedPrefManager.getInstance(getApplicationContext()).putAndroidID(androidId);

        mUserName = userName;
        mForgotPasswordAsyncTask = new ForgotPasswordAsyncTask(LoginTouchlessActivity.this, BaseAsyncTask.Priority.LOW, this);
        mForgotPasswordAsyncTask.execute(userName, getUniqueDeviceID());
    }*/

    /*private void checkHealthAndSafetyFeatureEnabled() {
        mGetHealthAndSafetyFeatureAsyncTask = new GetHealthAndSafetyFeatureAsyncTask(LoginTouchlessActivity.this, BaseAsyncTask.Priority.LOW, this);
        mGetHealthAndSafetyFeatureAsyncTask.execute();
    }*/

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
      /*  mRegisterDeviceAsyncTask = new RegisterDeviceAsyncTask(getApplicationContext(), BaseAsyncTask.Priority.LOW, this);
        mRegisterDeviceAsyncTask.execute(token);*/

        try {
            if (Connectivity.isNetworkAvailableRetro(LoginTouchlessActivity.this)) {
                String ToInstanceDeviceId = token;
                String UserDeviceId = "Android Device";
                int UserId = SharedPrefManager.getInstance(LoginTouchlessActivity.this).getUserId();
                int CreatedBy = SharedPrefManager.getInstance(LoginTouchlessActivity.this).getUserId();
                Call<RegisterDeviceId> call = RetrofitClient.getInstance(LoginTouchlessActivity.this).getMyApi().doSendRegistrationToServer(ToInstanceDeviceId, UserDeviceId, UserId, CreatedBy);
                call.enqueue(new retrofit2.Callback<RegisterDeviceId>() {
                    @Override
                    public void onResponse(Call<RegisterDeviceId> call, Response<RegisterDeviceId> response) {
                        try {
                            if (response.code() == 200) {
                                RegisterDeviceId registerDeviceId = response.body();
                                FWLogger.logInfo(TAG, "Device Registration");
                                if (registerDeviceId != null) {
                                    if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                                        FWLogger.logInfo(TAG, "Device Registration Completed.");
                                    } else {
                                        FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                                    }
                                }
                                //checkUserTypeForHealthAndSafetyFeature();
                                // SEND FOR OTP VERIFICATION
                                navigateToVerifyOTP();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterDeviceId> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AllUsersList API:");
                    }

                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(LoginTouchlessActivity.this);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AllUsersList API:");
            ex.getMessage();
        }
    }


/*
    private boolean validateUserCredential() {
        //validating inputs
        if (TextUtils.isEmpty(mEditTextUser.getText().toString())) {
            mEditTextUser.setError(getString(R.string.please_enter_username));
            mEditTextUser.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(mEditTextPass.getText().toString())) {
            mTextInputPassword.setError(getString(R.string.please_enter_password));
            mEditTextPass.requestFocus();
            return false;
        } else return true;
    }
*/

    public void checkUserTypeForHealthAndSafetyFeature() {
        //If user type is technician then verify Health And Safety Feature is enabled or not (API call)
        if (SharedPrefManager.getInstance(getApplicationContext()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyEnabled()) {
                int comp = DateUtils.compareTo(SharedPrefManager.getInstance(getApplicationContext()).getHealthSafetyDate());
                if (SharedPrefManager.getInstance(getApplicationContext()).isHealthSafetyLogged() && comp == 0) {
                    //navigateToHomeActivity();// comp == 0 Today's date = logged date
                } else {
                    //checkHealthAndSafetyFeatureEnabled();
                }
            } else {
                //checkHealthAndSafetyFeatureEnabled();
            }
        } else {
            //If user type is owner then Send to Home Activity
            //navigateToHomeActivity();
            // SEND FOR OTP VERIFICATION
            // navigateToVerifyOTP();
        }
    }

    private void navigateToHomeActivity() {
        Intent intent = new Intent(LoginTouchlessActivity.this, HomeActivityNew.class);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("NewNotification") != null) {
                intent.putExtra("NewNotification", "NewNotification");
            }
        }
        startActivity(intent);
        finish();
        return;
    }

    private void navigateToHealthAndSafetyActivity() {
        Intent intent = new Intent(LoginTouchlessActivity.this, HealthAndSafetyActivity.class);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("NewNotification") != null) {
                intent.putExtra("NewNotification", "NewNotification");
            }
        }
        intent.putExtra("isFromOptionMenu", false);
        startActivity(intent);
        finish();
        return;
    }


    @SuppressLint("HardwareIds")
    private String getUniqueDeviceID() {
        return Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        FWLogger.logInfo(TAG, "Login onTaskComplete");
        if (classType.equalsIgnoreCase(FWUser.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Login Successful");
            Gson gson = new Gson();
            FWUser fwUser = gson.fromJson(urlConnectionResponse.resultData, FWUser.class);

            if (fwUser != null) {
                if (fwUser.getCode().equalsIgnoreCase("200") && fwUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.login_successfully, LoginTouchlessActivity.this))) {
                   /* if (fwUser.getResultdata().isForgotPassword()) {
                        PasswordDialog passwordDialog = PasswordDialog.getInstance();
                        passwordDialog.changePasswordDialog(LoginTouchlessActivity.this, fwUser.getResultdata().isForgotPassword(), fwUser.getResultdata().getUserid());
                    } else {*/
                    //Toast.makeText(LoginTouchlessActivity.this, "OTP IS::" + fwUser.getResultdata().getOTP(), Toast.LENGTH_SHORT).show();
                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser, CommonFunction.getUniqueDeviceID(LoginTouchlessActivity.this));
                    int uniqueId = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    if (uniqueId != 0) {
                        FirebaseCrashlytics.getInstance().setUserId("" + uniqueId);
                    }

                    //NOTE: Log GA event
                    Bundle bundle = new Bundle();
                    bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, fwUser.getResultdata().getUserid());
                    // bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, mEditTextUser.getText().toString());
                    bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, fwUser.getResultdata().getUsergroupname());
                    bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                    bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                    bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                    FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                    if (SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID() != null) {
                        sendRegistrationToServer(SharedPrefManager.getInstance(getApplicationContext()).getRegFCMID());
                    } else {
                        // IF EVERYTHING IS DONE : LOGIN/DEVICE REG/FIREBASE/
                        // But USER PRESSED BACK Buton just redirect them on to OTP
                        // SEND FOR OTP VERIFICATION
                        navigateToVerifyOTP();
                    }
                    // }
                } else {
                    /* Toast.makeText(this, fwUser.getMessage(), Toast.LENGTH_LONG).show();*/
                    try {
                        Toast.makeText(this, R.string.pls_sign_up, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LoginTouchlessActivity.this, TouchLessSignupActivity.class);
                        intent.putExtra("LoginMobNumber", editText_emailorMobile.getText().toString().trim());
                        startActivity(intent);
                        finish();
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                }
            }
        } else if (classType.equalsIgnoreCase(ForgotPassword.class.getSimpleName())) {
            Gson gson = new Gson();
            ForgotPassword forgotPassword = gson.fromJson(urlConnectionResponse.resultData, ForgotPassword.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.USERNAME, mUserName);
            FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.FORGOT_PASSWORD, bundle);

            if (forgotPassword != null) {
                if (forgotPassword.getCode().equalsIgnoreCase("200") && forgotPassword.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.password_send_successfully, LoginTouchlessActivity.this))) {
                    Toast.makeText(this, getString(R.string.password_send_successfully), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, LoginTouchlessActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, forgotPassword.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (classType.equalsIgnoreCase(RegisterDeviceId.class.getSimpleName())) {
            Gson gson = new Gson();
            RegisterDeviceId registerDeviceId = gson.fromJson(urlConnectionResponse.resultData, RegisterDeviceId.class);
            FWLogger.logInfo(TAG, "Device Registration");
            if (registerDeviceId != null) {
                if (registerDeviceId.getCode().equalsIgnoreCase("200") && registerDeviceId.getMessage().equalsIgnoreCase("success")) {
                    FWLogger.logInfo(TAG, "Device Registration Completed.");
                } else {
                    FWLogger.logInfo(TAG, registerDeviceId.getMessage());
                }
            }
            //checkUserTypeForHealthAndSafetyFeature();
            // SEND FOR OTP VERIFICATION
            navigateToVerifyOTP();
        } else if (classType.equalsIgnoreCase(HealthAndSafety.class.getSimpleName())) {
            Gson gson = new Gson();
            HealthAndSafety healthAndSafety = gson.fromJson(urlConnectionResponse.resultData, HealthAndSafety.class);
            if (healthAndSafety != null) {
                if (healthAndSafety.getResultData().getIsHealthFeaturesEnabled()) {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(true);
                    if (healthAndSafety.getResultData().getIsTodayHealthStatus().getIsHealthAndSafetyValuesAddedForToday()) {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(true);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd"));
                        //navigateToHomeActivity();
                        navigateToVerifyOTP();
                    } else {
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                        SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                        navigateToHealthAndSafetyActivity();
                    }
                } else {
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                    SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                    //navigateToHomeActivity();
                    navigateToVerifyOTP();
                }
            } else {
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyEnabled(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyLogged(false);
                SharedPrefManager.getInstance(getApplicationContext()).setHealthSafetyDate(null);
                Toast.makeText(this, getString(R.string.something_went_wrong) + "..!", Toast.LENGTH_LONG).show();
            }
        } else if (classType.equalsIgnoreCase(ChangePassword.class.getSimpleName())) {
            Gson gson = new Gson();
            ChangePassword changePassword = gson.fromJson(urlConnectionResponse.resultData, ChangePassword.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getApplicationContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getApplicationContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            FirebaseAnalytics.getInstance(getApplicationContext()).logEvent(FirebaseGoogleAnalytics.Event.CHANGE_PASSWORD, bundle);
            if (changePassword != null) {
                if (changePassword.getCode().equalsIgnoreCase("200") && changePassword.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.password_updated_successfully, LoginTouchlessActivity.this))) {
                    Toast.makeText(this, getString(R.string.password_updated_successfully), Toast.LENGTH_LONG).show();
                    SharedPrefManager.getInstance(LoginTouchlessActivity.this).logout(false);
//                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(fwUser,getUniqueDeviceID());
                    Intent intent = new Intent(LoginTouchlessActivity.this, LoginTouchlessActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, changePassword.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {

    }

    private boolean validateMobileNumorEmailId() {
        FWLogger.logInfo(TAG, "validateMobileNo OR Email ID");
        String input = editText_emailorMobile.getText().toString().trim();

        if (CommonFunction.isEmpty(editText_emailorMobile)) {
            editText_emailorMobile.setError(getString(R.string.enter_your_mobile_email));
            return false;
        }

        if (input.contains("@")) {
            if (!CommonFunction.isEmailValid(input)) {
                editText_emailorMobile.setError(getString(R.string.please_enter_valid_emailid));
                return false;
            }
        } else {
            //requestSmsPermission();
            if (CommonFunction.isValidPhoneNRI/*CommonFunction.isValidPhone*/(editText_emailorMobile)) {
                editText_emailorMobile.setError(getString(R.string.please_enter_valid_number));
                return false;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void navigateToVerifyOTP() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("dataMessage") != null) {
                Intent intent = new Intent(LoginTouchlessActivity.this, VerifyOTPActivity.class);
                intent.putExtra("NewNotification", "NewNotification");
                intent.putExtra("UserID", editText_emailorMobile.getText().toString().trim());
                intent.putExtra("UserOTP", SharedPrefManager.getInstance(getApplicationContext()).getUserOTP());
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(LoginTouchlessActivity.this, VerifyOTPActivity.class);
            intent.putExtra("UserID", editText_emailorMobile.getText().toString().trim());
            intent.putExtra("UserOTP", SharedPrefManager.getInstance(getApplicationContext()).getUserOTP());
            intent.putExtra("FromWhere", "FromLogin");
            startActivity(intent);
            finish();
        }
    }

  /*  private void requestSmsPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);
        }
    }*/

    @Override
    public void onDestroy() {
        FWLogger.logInfo(TAG, "onDestroy");

        /*if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
        }*/
        super.onDestroy();
    }
}


