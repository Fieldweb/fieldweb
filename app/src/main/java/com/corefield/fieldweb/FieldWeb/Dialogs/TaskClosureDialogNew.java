package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.corefield.fieldweb.Adapter.NotesAdapterNew;
import com.corefield.fieldweb.FieldWeb.Task.OnSaveRating;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.Signature;

public class TaskClosureDialogNew {
    public static boolean isAlertDialogShowing = false;
    public final String TAG = TaskClosureDialogNew.class.getSimpleName();

    private static TaskClosureDialogNew mTaskClosureDialog;

    /**
     * The private constructor for the TaskClosureDialog Singleton class
     */
    private TaskClosureDialogNew() {
    }

    public static TaskClosureDialogNew getInstance() {
        //instantiate a new TaskClosureDialog if we didn't instantiate one yet
        if (mTaskClosureDialog == null) {
            mTaskClosureDialog = new TaskClosureDialogNew();
        }
        return mTaskClosureDialog;
    }

    public void addNotesDialog(Context context, int noteNo, NotesAdapterNew.NotesUpdateListener listener, String existingNote) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        NotesAdapterNew.NotesUpdateListener mNotesUpdateListener = listener;
        Button buttonCloseNotes = dialog.findViewById(R.id.button_cancel);
        EditText editTextNotesDetails = dialog.findViewById(R.id.edit_text_note_detail);
        editTextNotesDetails.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        Button buttonSave = dialog.findViewById(R.id.button_save_note_detail);

        if (existingNote != null && !existingNote.equalsIgnoreCase(""))
            editTextNotesDetails.setText(existingNote);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((isEmpty(editTextNotesDetails))) {
                    Toast.makeText(context, context.getResources().getString(R.string.note_error), Toast.LENGTH_SHORT).show();
                    editTextNotesDetails.setError(context.getResources().getString(R.string.note_add_something));
                } else if (!isEmpty(editTextNotesDetails) && (Character.isWhitespace(editTextNotesDetails.getText().toString().charAt(0)) || editTextNotesDetails.getText().toString().trim().isEmpty())) {
                    editTextNotesDetails.setError(context.getResources().getString(R.string.space_is_not_allowed));
                } else {
                    editTextNotesDetails.setError(null);
                    mNotesUpdateListener.onNotesAdded(editTextNotesDetails.getText().toString(), noteNo);
                    isAlertDialogShowing = false;
                    dialog.dismiss();
                }
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });

        buttonCloseNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
            }
        });
        isAlertDialogShowing = true;
        dialog.show();
    }

    public void signatureAndRatingDialog(Context context, String uniqueId, Signature.OnSignatureSave saveCallback, OnSaveRating ratingCallback) {
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.signature_new);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button buttonSave, buttonClear, buttonCancel;
        EditText editTextName, editTextSignedMobileNo;
        RatingBar ratingBar;
        TextView textViewRating;
        LinearLayout linearLayout;
        Signature signature;
        View mView;

        buttonSave = dialog.findViewById(R.id.button_save_signature);
        buttonClear = dialog.findViewById(R.id.button_clear_signature);
        buttonCancel = dialog.findViewById(R.id.button_cancel_signature);
        editTextName = dialog.findViewById(R.id.edittext_your_name);
        linearLayout = dialog.findViewById(R.id.linearLayout_signature);
        ratingBar = dialog.findViewById(R.id.ratingbar_task_closure);
        textViewRating = dialog.findViewById(R.id.txt_rating);
        editTextSignedMobileNo = dialog.findViewById(R.id.edittext_signed_mobile_no);

        /*StringBuilder sb=new StringBuilder();
        editTextSignedMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                FWLogger.logInfo(TAG, "beforeText_Count : "+count);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FWLogger.logInfo(TAG, "onTextChanged_Count : "+count);
                sb.append(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                FWLogger.logInfo(TAG, "afterText : "+s.toString());
                if(sb.length()==10)
                {
                    editTextSignedMobileNo.clearFocus();
                    linearLayout.requestFocus();
                }
            }
        });*/

        signature = new Signature(context, null, linearLayout, uniqueId, saveCallback);
        signature.setBackgroundColor(Color.WHITE);
        linearLayout.addView(signature, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        mView = linearLayout;

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90); //<-- int width=400;
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.30);//<-- int height =300;
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(width, height));

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                textViewRating.setText("" + rating);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                FWLogger.logInfo(TAG, "Panel Saved");
                boolean error = captureSignature();
                boolean errorRating = captureRating();
                boolean errorMobileNo = isCheckMobileError();
                if (!error && !errorRating && !errorMobileNo) {
                    mView.setDrawingCacheEnabled(true);
                    signature.save(mView);
                    ratingCallback.onRating(ratingBar.getRating(), editTextName.getText().toString(), editTextSignedMobileNo.getText().toString());
                    dialog.dismiss();
                }
            }

            private boolean captureSignature() {
                boolean error = false;
                String errorMessage = "";
                if (editTextName.getText().toString().equalsIgnoreCase("")) {
                    errorMessage = errorMessage + context.getResources().getString(R.string.please_add_your_name);
                    error = true;
                } else if (!isEmpty(editTextName) && (Character.isWhitespace(editTextName.getText().toString().charAt(0)) || editTextName.getText().toString().trim().isEmpty())) {
                    errorMessage = errorMessage + context.getResources().getString(R.string.space_is_not_allowed);
                    error = true;
                }

                if (error) {
                    editTextName.setError(errorMessage);
                }
                return error;
            }

            private boolean isValidPhone(EditText mobileNo) {
                FWLogger.logInfo(TAG, "isValidPhone");
                CharSequence phone = mobileNo.getText().toString();
                return phone.length() != 10;
            }

            private boolean isEmpty(EditText mobileNo) {
                CharSequence str = mobileNo.getText().toString();
                return TextUtils.isEmpty(str);
            }

            private boolean isCheckMobileError() {
                boolean errorMobile;
                if (!isEmpty(editTextSignedMobileNo)) {
                    if (SharedPrefManager.getInstance(context).getCountryDetailsID() == 1) {
                        errorMobile = isValidPhone(editTextSignedMobileNo);
                        if (errorMobile) {
                            editTextSignedMobileNo.setError(context.getResources().getString(R.string.please_enter_valid_no));
                            editTextSignedMobileNo.requestFocus();
                            errorMobile = true;
                        } else {
                            errorMobile = false;
                        }
                    } else { //FOR NRI
                        errorMobile = CommonFunction.isValidPhoneNRI(editTextSignedMobileNo);
                        if (errorMobile) {
                            editTextSignedMobileNo.setError(context.getResources().getString(R.string.please_enter_valid_no));
                            editTextSignedMobileNo.requestFocus();
                            errorMobile = true;
                        } else {
                            errorMobile = false;
                        }
                    }
                   /* errorMobile = isValidPhone(editTextSignedMobileNo);
                    if (errorMobile) {
                        editTextSignedMobileNo.setError(context.getResources().getString(R.string.please_enter_valid_no));
                        editTextSignedMobileNo.requestFocus();
                        errorMobile = true;
                    } else {
                        errorMobile = false;
                    }*/
                } else {
                    errorMobile = false;
                }
                return errorMobile;
            }

            private boolean captureRating() {
                boolean error = false;
                String errorMessage = "";
                if (ratingBar.getRating() <= 0) {
                    errorMessage = errorMessage + context.getResources().getString(R.string.rate_us);
                    error = true;
                }
                if (error) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
                return error;
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signature.clear();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAlertDialogShowing = false;
                dialog.dismiss();
            }
        });

        isAlertDialogShowing = true;
        dialog.show();
    }
}
