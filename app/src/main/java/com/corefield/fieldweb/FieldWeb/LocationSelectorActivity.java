package com.corefield.fieldweb.FieldWeb;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.GoogleMapUtils.MapUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Currently not in use but can be used in future
 */
public class LocationSelectorActivity extends BaseActivity implements GoogleMap.OnMapClickListener {

    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_selectore_activity);

        // Getting a reference to the map
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.select_location_map);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                // Setting a click event handler for the map
                mGoogleMap.setOnMapClickListener(LocationSelectorActivity.this);
            }
        });
    }

    @Override
    public void onMapClick(LatLng latLng) {
        // Creating a marker
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting the position for the marker
        markerOptions.position(latLng);

        // Setting the title for the marker.
        // This will be displayed on taping the marker
        markerOptions.title("Address: ");
        markerOptions.snippet(MapUtils.getFullAddressFromLatLong(this, latLng.latitude, latLng.longitude));
        // Clears the previously touched position
        mGoogleMap.clear();

        // Animating to the touched position
        //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                latLng, 8);
        mGoogleMap.animateCamera(location);

        // Placing a marker on the touched position
        mGoogleMap.addMarker(markerOptions).showInfoWindow();
    }
}
