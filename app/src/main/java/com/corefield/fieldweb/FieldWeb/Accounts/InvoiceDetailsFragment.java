package com.corefield.fieldweb.FieldWeb.Accounts;

import static com.corefield.fieldweb.Util.CommonFunction.isEmpty;
import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListViewAdapter;
import com.corefield.fieldweb.Adapter.InvoiceItemDetailsListAdapter;
import com.corefield.fieldweb.Adapter.InvoicePaymentDetailsListAdapter;
import com.corefield.fieldweb.Adapter.InvoiceServiceDetailsListAdapter;
import com.corefield.fieldweb.Adapter.NotesViewAdapter;
import com.corefield.fieldweb.DTO.Account.DeleteInvoiceDetailsDTO;
import com.corefield.fieldweb.DTO.Account.DownloadInvoicePdfDTO;
import com.corefield.fieldweb.DTO.Account.InvoiceDetailsDTO;
import com.corefield.fieldweb.DTO.Account.InvoiceFollowUpNotesDTO;
import com.corefield.fieldweb.DTO.Account.InvoicePaymentStatusDTO;
import com.corefield.fieldweb.DTO.Account.SaveInvoicePaymentAmountDetailsDTO;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.DownloadFile;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Tasks Details
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show completed task details
 */
@SuppressLint("ValidFragment")
public class InvoiceDetailsFragment extends Fragment implements View.OnClickListener {

    public static String TAG = InvoiceDetailsFragment.class.getSimpleName();
    private View mRootView;
    private TasksList.ResultData mResultData;
    private int invoiceID;
    private String invoiceStatus, invName = "";
    Fragment fragment;
    private TextView mTextViewTaskName, mTextViewTaskStatus, mtxtInvCreateDate, mtxtInvValidityDate, mTextViewStartDateTime, mTextViewEndDateTime, mTextViewTechnicianName, mTextViewItemName, mTextViewItemQuantity, mTextViewCustomerName, mTextViewCustomerNo, mTextViewLandmark, mTextViewAddress, mTextViewWorkMode, mTextViewSignedBy, mTextViewRating, mTextViewSignedMobileNo, mTextViewTaskID, mTextViewInvoiceID, mTextViewTotalAmt, mTextViewDiscount, mTextViewTax, mTextViewGrandTotalAmt,
            mTextViewPendingAmt,mTextViewHeadingPendingAmt;
    private ImageView mImageViewCall, mImageViewDownload, mImageViewMessage;
    private Marker mMarkerDestination;
    private double mDestLat = 0, mDestLong = 0;
    private RecyclerView multiItemInvoiceDetails, multiServiceInvoiceDetails, multiPaymentInvoiceDetails;
    private NotesViewAdapter mNotesViewAdapter;
    private DeviceListViewAdapter mDeviceListViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<TaskClosure.TechnicalNotedto> mTechnicalNoteDTOs = new ArrayList<>();
    private List<TaskClosure.DeviceInfoList> mDeviceInfoLists = new ArrayList<>();
    private ImageView mImageViewSignature;
    private RatingBar mRatingBar;
    private LinearLayout mLinearNotesList, mLinearDeviceList;
    private LinearLayout mTaskClosureWrapper, mLinearLayoutRatingWrapper;
    public List<InvoiceDetailsDTO.ResultData> mInvoiceList = null;
    InvoiceItemDetailsListAdapter invoiceItemDetailsListAdapter;
    InvoiceServiceDetailsListAdapter invoiceServiceDetailsListAdapter;
    InvoicePaymentDetailsListAdapter invoicePaymentDetailsListAdapter;
    private static final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET};
    private static final String[] PERMISSIONS_TIRAMISU = {Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_AUDIO};
    private ImageView mImageViewDownloadInvoice, mImageViewEditStatus, mImageViewFollowup, mImageViewDelete;
    public List<InvoicePaymentStatusDTO.ResultData> mInvoiceFollowUpList = null;
    private TextView Ftotalamt, Freceivedamt, Fremainingamt, ViewSLA;
    private ProgressDialog mProgressDialog;
    private String mUrl;


    @SuppressLint("ValidFragment")
    public InvoiceDetailsFragment() {
        FWLogger.logInfo(TAG, "Constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.invoice_details_fragment, container, false);
        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

            inIt();
            //manageBackPress();
            checkStoragePermission();
        }
        return mRootView;
    }

    private void inIt() {

        mTextViewInvoiceID = mRootView.findViewById(R.id.invoice_id);
        mTextViewTaskStatus = mRootView.findViewById(R.id.task_details_task_status);
        mtxtInvCreateDate = mRootView.findViewById(R.id.txtInvCreateDate);
        mtxtInvValidityDate = mRootView.findViewById(R.id.txtInvValidityDate);
        mTextViewStartDateTime = mRootView.findViewById(R.id.task_details_task_start_date_time);
        mTextViewEndDateTime = mRootView.findViewById(R.id.task_details_task_end_date_time);
        mTextViewTechnicianName = mRootView.findViewById(R.id.task_details_tech_name);
        mTextViewItemName = mRootView.findViewById(R.id.task_details_item_name);
        mTextViewItemQuantity = mRootView.findViewById(R.id.task_details_item_quantity);
        mTextViewCustomerName = mRootView.findViewById(R.id.task_details_customer_name);
        mTextViewCustomerNo = mRootView.findViewById(R.id.task_details_customer_number);
        mTextViewLandmark = mRootView.findViewById(R.id.task_details_landmark);
        mTextViewAddress = mRootView.findViewById(R.id.task_details_address);
        mTextViewTaskName = mRootView.findViewById(R.id.task_details_task_name);
        mImageViewCall = mRootView.findViewById(R.id.imageView_call);
        mImageViewMessage = mRootView.findViewById(R.id.imageView_message);
        mTaskClosureWrapper = mRootView.findViewById(R.id.linear_layout_task_closure_wrapper);
        mLinearLayoutRatingWrapper = mRootView.findViewById(R.id.linear_layout_rating_wrapper);
        mTextViewWorkMode = mRootView.findViewById(R.id.textView_work_mode);
        mImageViewSignature = mRootView.findViewById(R.id.customer_signature);
        mRatingBar = mRootView.findViewById(R.id.ratingbar_task_closure_view);
        mTextViewRating = mRootView.findViewById(R.id.textView_rating);

        mTextViewSignedBy = mRootView.findViewById(R.id.textView_signed_by);
        mTextViewSignedMobileNo = mRootView.findViewById(R.id.textview_signed_mobile_no);
        mTextViewTaskID = mRootView.findViewById(R.id.task_id);
        //
        mImageViewEditStatus = mRootView.findViewById(R.id.imageView_updatestatus);
        mImageViewDelete = mRootView.findViewById(R.id.imageView_delete);
        mImageViewFollowup = mRootView.findViewById(R.id.imageView_followup);
        mImageViewDownloadInvoice = mRootView.findViewById(R.id.imageView_download);

        mTextViewTotalAmt = mRootView.findViewById(R.id.tamt);
        mTextViewDiscount = mRootView.findViewById(R.id.discount);
        mTextViewTax = mRootView.findViewById(R.id.tax);
        mTextViewGrandTotalAmt = mRootView.findViewById(R.id.gtamt);
        mTextViewPendingAmt = mRootView.findViewById(R.id.pending_amt);
        mTextViewHeadingPendingAmt = mRootView.findViewById(R.id.hpending_amt);

        multiItemInvoiceDetails = mRootView.findViewById(R.id.multiItemInvoiceDetails);
        multiServiceInvoiceDetails = mRootView.findViewById(R.id.multiServiceInvoiceDetails);
        multiPaymentInvoiceDetails = mRootView.findViewById(R.id.multiPaymentInvoiceDetails);

        /*ViewSLA = mRootView.findViewById(R.id.view_SLA);*/

        mImageViewEditStatus.setOnClickListener(this);
        mImageViewDelete.setOnClickListener(this);
        mImageViewFollowup.setOnClickListener(this);
        mImageViewDownloadInvoice.setOnClickListener(this);
        //ViewSLA.setOnClickListener(this);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                invoiceStatus = getBundle.getString("invoiceStatus");
                invoiceID = getBundle.getInt("invoiceID");
                invName = getBundle.getString("invoiceName");
                getInvoiceDetails(invoiceID);
                fragment = this;
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        mImageViewEditStatus.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(), "Update Payment Status", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        mImageViewDelete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(), "Delete Invoice", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        mImageViewFollowup.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(), "Invoice Follow up", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        mImageViewDownloadInvoice.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(), "Download Invoice", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void getInvoiceDetails(int invoiceId) {
        try {
            if (invoiceID != 0) {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    CommonFunction.showProgressDialog(getActivity());
                    int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    Call<InvoiceDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getInvoiceDetailsById(invoiceId, userID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<InvoiceDetailsDTO>() {
                        @Override
                        public void onResponse(Call<InvoiceDetailsDTO> call, Response<InvoiceDetailsDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    CommonFunction.hideProgressDialog(getActivity());
                                    FWLogger.logInfo(TAG, "Get INV Details by Id");
                                    mInvoiceList = new ArrayList<>();
                                    InvoiceDetailsDTO invoiceDetailsDTO = response.body();
                                    mInvoiceList = Collections.singletonList(invoiceDetailsDTO.getResultData());

                                    if (mInvoiceList.size() > 0) {
                                        setData(mInvoiceList);
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<InvoiceDetailsDTO> call, Throwable throwable) {
                            CommonFunction.hideProgressDialog(getActivity());
                            FWLogger.logInfo(TAG, "Exception in GetLeadListByUserIdandLeadId API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetLeadListByUserIdandLeadId API:");
            ex.getMessage();
        }

    }

    private void setData(List<InvoiceDetailsDTO.ResultData> mInvoiceList) {
        try {
            /*mTextViewTaskName.setText(mInvoiceList.get(0).getCompanyName());*/
            try {
                mTextViewTaskName.setText(invName);
            } catch (Exception ex) {
                ex.getMessage();
            }
            mTextViewInvoiceID.setText(mInvoiceList.get(0).getInvoiceCode());

            mTextViewCustomerName.setText(mInvoiceList.get(0).getCustomerName());
            mTextViewCustomerNo.setText(mInvoiceList.get(0).getMobileNumber());
            mTextViewAddress.setText(mInvoiceList.get(0).getCustAddress());
            mTextViewLandmark.setText(mInvoiceList.get(0).getLandmark());

            mTextViewTotalAmt.setText("₹" + String.valueOf(mInvoiceList.get(0).getInvoiceAmount()));
            mTextViewDiscount.setText("₹" + String.valueOf(mInvoiceList.get(0).getDiscountAmounnt()) + "(" + String.valueOf(mInvoiceList.get(0).getDiscount()) + "%)");
            mTextViewTax.setText("₹" + String.valueOf(mInvoiceList.get(0).getTaxAmount()) + "(" + String.valueOf(mInvoiceList.get(0).getTax()) + "%)");
            mTextViewGrandTotalAmt.setText("₹" + String.valueOf(mInvoiceList.get(0).getGrandTotalAmount()));
            mTextViewPendingAmt.setText("₹" + String.valueOf(mInvoiceList.get(0).getRemainingAmount()));


            //
            String strDate = mInvoiceList.get(0).getInvoiceDateTime().split("T")[0];

            String formattedCreatedDate = DateUtils.convertDateFormat(strDate, "yyyy-MM-dd", "dd-MM-yyyy");

            if (formattedCreatedDate != null) {
                mtxtInvCreateDate.setText(formattedCreatedDate);
            } else {
                mtxtInvCreateDate.setText("-NA-");
            }

        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR MULTIPLE INVOICE ITEM DETAILS
        try {
            List<InvoiceDetailsDTO.ItemList> multipleItemAssignedArrayList = new ArrayList<InvoiceDetailsDTO.ItemList>();
            multipleItemAssignedArrayList = mInvoiceList.get(0).getItemList();
            if (multipleItemAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiItemInvoiceDetails.setLayoutManager(mLayoutManager);
                invoiceItemDetailsListAdapter = new InvoiceItemDetailsListAdapter(getActivity(), multipleItemAssignedArrayList);
                multiItemInvoiceDetails.setAdapter(invoiceItemDetailsListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR MULTIPLE SERVICE ITEM DETAILS
        try {
            List<InvoiceDetailsDTO.ServiceList> multipleServiceAssignedArrayList = new ArrayList<InvoiceDetailsDTO.ServiceList>();
            multipleServiceAssignedArrayList = mInvoiceList.get(0).getServiceList();
            if (multipleServiceAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiServiceInvoiceDetails.setLayoutManager(mLayoutManager);
                invoiceServiceDetailsListAdapter = new InvoiceServiceDetailsListAdapter(getActivity(), multipleServiceAssignedArrayList);
                multiServiceInvoiceDetails.setAdapter(invoiceServiceDetailsListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR MULTIPLE INVOICE PAYMENT DETAILS
        try {
            List<InvoiceDetailsDTO.PaymentList> multiplePaymentAssignedArrayList = new ArrayList<InvoiceDetailsDTO.PaymentList>();
            multiplePaymentAssignedArrayList = mInvoiceList.get(0).getPaymentList();
            if (multiplePaymentAssignedArrayList.size() > 0) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                multiPaymentInvoiceDetails.setLayoutManager(mLayoutManager);
                invoicePaymentDetailsListAdapter = new InvoicePaymentDetailsListAdapter(getActivity(), multiplePaymentAssignedArrayList);
                multiPaymentInvoiceDetails.setAdapter(invoicePaymentDetailsListAdapter);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }



           /* String strCreatedDate = mInvoiceList.get(0).getCreatedDate().split("T")[0];
            String strCreatedTime = mInvoiceList.get(0).getCreatedDate().split("T")[1];*/

            /*String strValidityDate = mInvoiceList.get(0).getValidityDate().split("T")[0];
            String strValidityTime = mInvoiceList.get(0).getValidityDate().split("T")[1];

            String formattedCreatedDate = DateUtils.convertDateFormat(strCreatedDate, "yyyy-MM-dd", "dd-MM-yyyy");
            String formattedValidityDate = DateUtils.convertDateFormat(strValidityDate, "yyyy-MM-dd", "dd-MM-yyyy");

            String formattedCrTime = DateUtils.timeFormatConversion(strCreatedTime, "HH:mm:ss", "hh:mm aa");
            String formattedValTime = DateUtils.timeFormatConversion(strValidityTime, "HH:mm:ss", "hh:mm aa");

            if (formattedCreatedDate != null && formattedCrTime != null) {
                mTextCreateDate.setText(formattedCreatedDate + "  " + formattedCrTime);
            } else {
                mTextCreateDate.setText("-NA-");
            }
            if (formattedValidityDate != null && formattedValTime != null) {
                mTextValidityDate.setText(formattedValidityDate + "  " + formattedValTime);
            } else {
                mTextValidityDate.setText("-NA-");
            }*/

        //String formattedTime = DateUtils.timeFormatConversion(mInvoiceList.get(i).getQuoteTime(), "HH:mm:ss", "hh:mm aa");
                /*if (formattedCreatedDate != null && formattedValidityDate != null && formattedTime != null) {
                    mTextViewEstimatedAmount.setText(formattedCreatedDate + "  " + formattedTime);
                    mTextViewEarnedAmount.setText(formattedValidityDate + "  " +formattedTime);
                }*/
        //mTextViewCustomerName.setText(mInvoiceList.get(i).getCustomer().getCustomerName());
        //mTextViewCustomerNo.setText(mInvoiceList.get(i).getCustomer().getMobileNumber());
        //mTextViewAddress.setText(mInvoiceList.get(i).getCustomer().getLocationList().getAddress());
        //mTextViewLandmark.setText(mInvoiceList.get(i).getCustomer().getLocationList().getLandmark);
        //mTextView.setText(mInvoiceList.get(i).getQuoteServiceList());


        switch (invoiceStatus) {
            case "Paid":
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.green));
                mTextViewTaskStatus.setText(invoiceStatus);
                mTextViewHeadingPendingAmt.setVisibility(View.GONE);
                mTextViewPendingAmt.setVisibility(View.GONE);

                mImageViewEditStatus.setVisibility(View.INVISIBLE);
                mImageViewDelete.setVisibility(View.INVISIBLE);
                mImageViewFollowup.setVisibility(View.INVISIBLE);
                mImageViewDownloadInvoice.setVisibility(View.INVISIBLE);
                break;

            case "Partial Paid":
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.orange));
                mTextViewTaskStatus.setText(invoiceStatus);
                mImageViewDownloadInvoice.setVisibility(View.INVISIBLE);
                break;

            case "UnPaid":
                mTextViewTaskStatus.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                mTextViewTaskStatus.setText(invoiceStatus);
                mImageViewDownloadInvoice.setVisibility(View.INVISIBLE);
                break;

            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageView_updatestatus:
                UpdatePaymentStatus(getContext(), getActivity());
                break;

            case R.id.imageView_delete:
                deleteInvoiceDetailsDialog(getActivity(), invoiceID/*mInvoiceList.get(0).getId()*/);
                break;

            case R.id.imageView_followup:
                InvoiceFollowUp(getContext(), getActivity());
                break;

            case R.id.imageView_download:
                getDownloadInvoicePDF(invoiceID);
                break;

           /* case R.id.view_SLA:
                viewSLAFullScreenMode(getActivity());
                break;*/

            default:
                break;
        }
    }

    public void InvoiceFollowUp(Context context, Activity mActivity) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_invoice_follow_up);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        EditText editTextCreateDatePicker = dialog.findViewById(R.id.edittext_createdate_picker);
        EditText notes = dialog.findViewById(R.id.invoiceNotes);

        TextInputLayout textInputCreateDatePicker = dialog.findViewById(R.id.textInput_createdate_picker);
        textInputCreateDatePicker.setEnabled(true);

        ImageView mclose = dialog.findViewById(R.id.closeLeadstatus);
        Button mbtnAdd = dialog.findViewById(R.id.btnUpdateLeadstatus);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        editTextCreateDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(getActivity(), editTextCreateDatePicker);
            }
        });
        mclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int InvoiceId = invoiceID;
                int StatusId = 1; // need to pass final value once Details API is ready
                int UserId = SharedPrefManager.getInstance(context).getUserId();

                Date date = new Date();
                String currentDate = editTextCreateDatePicker.getText().toString();
                String finalD = currentDate + "T00:00:00";

                String note = notes.getText().toString();

                if (currentDate.isEmpty()) {
                    Toast.makeText(context, R.string.please_enter_date, Toast.LENGTH_SHORT).show();
                } else {
                    followUpNotes(InvoiceId, StatusId, UserId, finalD, note);
                    dialog.dismiss();
                }
            }
        });
    }

    public void followUpNotes(int InvoiceId, int StatusId, int UserID, String finalD, String note) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<InvoiceFollowUpNotesDTO> call = RetrofitClient.getInstance(getActivity()).getMyApi().followUpNotes(InvoiceId, StatusId, UserID, finalD, note, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<InvoiceFollowUpNotesDTO>() {
                    @Override
                    public void onResponse(Call<InvoiceFollowUpNotesDTO> call, Response<InvoiceFollowUpNotesDTO> response) {
                        try {
                            if (response.code() == 200) {
                                InvoiceFollowUpNotesDTO invoiceFollowUpNotesDTO = response.body();
                                //NOTE: Log GA event
                                Toast.makeText(getActivity(), invoiceFollowUpNotesDTO.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<InvoiceFollowUpNotesDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getContext());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
            ex.getMessage();
        }
    }

    public void UpdatePaymentStatus(Context context, Activity mActivity) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update_payment_status);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        TextView totalamt = dialog.findViewById(R.id.total_amt);
        Ftotalamt = totalamt;
        TextView receivedamt = dialog.findViewById(R.id.received_amt);
        Freceivedamt = receivedamt;
        TextView remainingamt = dialog.findViewById(R.id.remaining_amt);
        Fremainingamt = remainingamt;

        TextInputEditText amount = dialog.findViewById(R.id.edittext_amount);
        ImageView mclose = dialog.findViewById(R.id.closeLeadstatus);
        Spinner spinPaymntMode = dialog.findViewById(R.id.spinPaymntMode);
        Button mbtnAdd = dialog.findViewById(R.id.btnUpdateLeadstatus);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        // SET SPINNER DATA
        String[] paymentValues = {"Select Payment Type", "Cash", "UPI", "NEFT", "Cheque", "Other"};
        ArrayAdapter<String> paymentType = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, paymentValues);
        spinPaymntMode.setAdapter(paymentType);

        getInvoicePaymentStatus(invoiceID);

        mclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int RemAmount = 0;
                int Id = invoiceID;
                int InvoiceId = invoiceID;
                int CreatedBy = SharedPrefManager.getInstance(context).getUserId();
                int UserId = SharedPrefManager.getInstance(context).getUserId();
                boolean IsActive = true;
                try {
                    RemAmount = Integer.valueOf(amount.getText().toString());
                } catch (Exception ex) {
                    RemAmount = 0;
                    ex.getMessage();
                }

                if (isEmpty(amount)) {
                    amount.setError(context.getResources().getString(R.string.please_enter_amount));
                } else if (amount.getText().toString().equalsIgnoreCase("0")) {
                    amount.setError(context.getResources().getString(R.string.please_enter_amount));
                } else if (spinPaymntMode.getSelectedItem().toString().equalsIgnoreCase("Select Payment Type")) {
                    Toast.makeText(getContext(), "Please select a payment type!", Toast.LENGTH_SHORT).show();
                } else if (RemAmount > mInvoiceFollowUpList.get(0).getRemainingAmount()) {
                    Toast.makeText(getContext(), "Entered amount is greater than remaining amount", Toast.LENGTH_SHORT).show();
                } else {
                    //RemAmount = Integer.valueOf(amount.getText().toString());
                    String paymentTransactionType = spinPaymntMode.getSelectedItem().toString();
                    dialog.dismiss();
                    updatePayment(Id, InvoiceId, RemAmount, CreatedBy, UserId, paymentTransactionType, IsActive);
                    AccountsTabHost myFragment = new AccountsTabHost();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                }
            }
        });
    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    public void updatePayment(int id, int invoiceId, int remAmount, int createdBy, int userId, String paymentTransactionType, boolean isActiveFlag) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<SaveInvoicePaymentAmountDetailsDTO> call = RetrofitClient.getInstance(getActivity()).getMyApi().updatePayment(id, invoiceId, remAmount, createdBy, userId, paymentTransactionType, isActiveFlag, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<SaveInvoicePaymentAmountDetailsDTO>() {
                    @Override
                    public void onResponse(Call<SaveInvoicePaymentAmountDetailsDTO> call, Response<SaveInvoicePaymentAmountDetailsDTO> response) {
                        try {
                            if (response.code() == 200) {
                                SaveInvoicePaymentAmountDetailsDTO saveInvoicePaymentAmountDetailsDTO = response.body();
                                //NOTE: Log GA event
                                Toast.makeText(getActivity(), saveInvoicePaymentAmountDetailsDTO.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<SaveInvoicePaymentAmountDetailsDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getContext());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UnAssignItemPortal API:");
            ex.getMessage();
        }

    }

    private void getInvoicePaymentStatus(int invoiceId) {
        try {
            if (invoiceId != 0) {
                /*if (getContext() == null)*/
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    CommonFunction.showProgressDialog(getActivity());
                    int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                    Call<InvoicePaymentStatusDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getInvoicePaymentStatus(invoiceId, userID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<InvoicePaymentStatusDTO>() {
                        @Override
                        public void onResponse(Call<InvoicePaymentStatusDTO> call, Response<InvoicePaymentStatusDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    CommonFunction.hideProgressDialog(getActivity());
                                    mInvoiceFollowUpList = new ArrayList<>();
                                    InvoicePaymentStatusDTO invoicePaymentStatusDTO = response.body();
                                    mInvoiceFollowUpList = invoicePaymentStatusDTO.getResultData();
                                    if (mInvoiceFollowUpList.size() > 0) {
                                        setDataForInvoiceFollowUp(mInvoiceFollowUpList);
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<InvoicePaymentStatusDTO> call, Throwable throwable) {
                            CommonFunction.hideProgressDialog(getActivity());
                            FWLogger.logInfo(TAG, "Exception in getInvoicePaymentStatus API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in getInvoicePaymentStatus API:");
            ex.getMessage();
        }

    }

    private void setDataForInvoiceFollowUp(List<InvoicePaymentStatusDTO.ResultData> mInvoiceFollowUpList) {
        try {

            Ftotalamt.setText("₹ " + String.valueOf(mInvoiceFollowUpList.get(0).getTotalAmount()));
            Freceivedamt.setText("₹ " + String.valueOf(mInvoiceFollowUpList.get(0).getRecievedAmount()));
            Fremainingamt.setText("₹ " + String.valueOf(mInvoiceFollowUpList.get(0).getRemainingAmount()));
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void deleteInvoiceDetailsDialog(Activity activity, int quoteID) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.delete_invoicedetails_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        Button deleteQuote, buttonCancel;

        deleteQuote = dialog.findViewById(R.id.deleteQuote);
        buttonCancel = dialog.findViewById(R.id.button_cancel);

        deleteQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeleteInvoiceDetails(quoteID);
                dialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void getDeleteInvoiceDetails(int invoiceId) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<DeleteInvoiceDetailsDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getDeleteInvoiceDetails(invoiceId, userID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<DeleteInvoiceDetailsDTO>() {
                    @Override
                    public void onResponse(Call<DeleteInvoiceDetailsDTO> call, Response<DeleteInvoiceDetailsDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                if (response.code() == 200) {
                                    DeleteInvoiceDetailsDTO deleteInvoiceDetailsDTO = response.body();
                                    if (deleteInvoiceDetailsDTO != null) {
                                        if (deleteInvoiceDetailsDTO.getCode().equalsIgnoreCase("200")) {
                                            Toast.makeText(getContext(), deleteInvoiceDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getContext(), deleteInvoiceDetailsDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                        setRefresh();
                                    } else {
                                        Toast.makeText(getContext(), R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteInvoiceDetailsDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in DeleteInvoice API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in DeleteInvoice API:");
            ex.getMessage();
        }
    }

    private void getDownloadInvoicePDF(int invoiceID) {
        if (invoiceID != 0) {
            try {
                if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                    int userId = SharedPrefManager.getInstance(getContext()).getUserId();
                    Call<DownloadInvoicePdfDTO> call = RetrofitClient.getInstance(getContext()).getMyApi().getInvoicePdf(userId, invoiceID, "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                    call.enqueue(new retrofit2.Callback<DownloadInvoicePdfDTO>() {
                        @Override
                        public void onResponse(Call<DownloadInvoicePdfDTO> call, Response<DownloadInvoicePdfDTO> response) {
                            try {
                                if (response.code() == 200) {
                                    DownloadInvoicePdfDTO DownloadInvoicePdfDTO = response.body();
                                    if (DownloadInvoicePdfDTO != null) {
                                        FWLogger.logInfo(TAG, "Code : " + DownloadInvoicePdfDTO.getCode());
                                        FWLogger.logInfo(TAG, "Message : " + DownloadInvoicePdfDTO.getMessage());
                                        if (DownloadInvoicePdfDTO.getCode().equalsIgnoreCase("200")) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                                if (!hasPermissions(getContext(), PERMISSIONS_TIRAMISU)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(DownloadInvoicePdfDTO.getResultData().getInvoicePdfPath(), getActivity());
                                                }
                                            } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                                                if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                    Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                    Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                    t.show();
                                                } else {
                                                    DownloadFile.downloadFile(DownloadInvoicePdfDTO.getResultData().getInvoicePdfPath(), getActivity());
                                                }
                                            }
                                           /* if (!hasPermissions(getContext(), PERMISSIONS)) {
                                                Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
                                                Toast t = Toast.makeText(getContext(), R.string.you_dont_have_write_access, Toast.LENGTH_LONG);
                                                t.show();
                                            } else {
                                                DownloadFile.downloadFile(DownloadInvoicePdfDTO.getMessage(), getActivity());
                                            }*/
                                        } else {
                                            Toast.makeText(getContext(), DownloadInvoicePdfDTO.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.getMessage();
                            }
                        }

                        @Override
                        public void onFailure(Call<DownloadInvoicePdfDTO> call, Throwable throwable) {
                            FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                        }
                    });
                } else {
                    ServiceDialog serviceDialog = ServiceDialog.getInstance();
                    serviceDialog.noConnectionDialogRetro(getActivity());
                }
            } catch (Exception ex) {
                FWLogger.logInfo(TAG, "Exception in GenerateReportPDF API:");
                ex.getMessage();
            }
        }
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error:" + description, Toast.LENGTH_SHORT).show();

        }
    }

    private void setRefresh() {
        clearBackStackEntries();
        AccountsTabHost myFragment = new AccountsTabHost();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, myFragment);
        transaction.commit();
    }

    private void clearBackStackEntries() {
        FWLogger.logInfo(TAG, "inside clearBackStackEntries");
        int count = getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearBackStackEntries();
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    //AccountsTabHost myFragment = new AccountsTabHost();
                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    return true;
                }
                return false;
            }
        });
    }


    /*private void downloadFile(String path) {
        try {
            final ProgressDialog pd = new ProgressDialog(getContext());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(getString(R.string.please_wait));
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean downloadResponse = HttpDownloadUtility.downloadFile(path, storageDir.getAbsolutePath());
                        if (downloadResponse) {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.file_download_successfully, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            String filename = path.substring(path.lastIndexOf("/") + 1);
                            File file = new File(storageDir, filename);
                            Uri uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", file);
                            // Open file with user selected app
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(uri, "application/pdf");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.sownload_failed, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        pd.dismiss();
                    } catch (IOException ex) {
                        pd.dismiss();
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean checkStoragePermission() {
        boolean storageFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkWriteStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                final int checkReadStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
                if (checkWriteStorage != PackageManager.PERMISSION_GRANTED && checkReadStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    storageFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return storageFlag;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}