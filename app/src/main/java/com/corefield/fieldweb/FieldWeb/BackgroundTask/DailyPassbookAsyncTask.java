package com.corefield.fieldweb.FieldWeb.BackgroundTask;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Passbook.DailyPassbook;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;

/**
 * Async Task for Daily Passbook
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Async Task class is used to call - Daily Passbook API
 */
public class DailyPassbookAsyncTask extends BaseAsyncTask {

    protected static String TAG = DailyPassbookAsyncTask.class.getSimpleName();

    public DailyPassbookAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                int userId = 0;
                userId = (int) objects[0];

                if (userId != 0) {

                    URLConnectionRequest URLConnectionRequest = new URLConnectionRequest();
                    String query = "UserId=" + userId;
                    if (query != null && URLConnectionRequest != null) {
                        Request request = RequestBuilder.buildRequest(mContext, URLConstant.Passbook.GET_DAILY_PASSBOOK, null);
                        mUrlConnectionResponse = URLConnectionRequest.sendGetRequest(request.getmURL(), request.getmHeader(), query, request.getToken());
                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null) {
            if (status) {
                if (onTaskCompleteListener != null && mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE)
                    onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, DailyPassbook.class.getSimpleName());
            } else {
                //Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
            }
        }
    }
}
