package com.corefield.fieldweb.FieldWeb.Admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Attendance.TechMonthlyAttendance;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for TechAttendanceFragment
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show attendance of  technician for particular date (tech login)
 */
public class TechAttendanceFragment extends Fragment implements OnTaskCompleteListener {

    protected static String TAG = TechAttendanceFragment.class.getSimpleName();
    public List<TechMonthlyAttendance.ResultData> mTechAttend = null;
    private View mRootView;
    private CalendarView mCalendarView;
    private List<EventDay> mEvents;
    OnDayClickListener onDayClickListener;
    TextView txtDate, txtCheckIn, txtCheckOut, txtDayname;
    RelativeLayout curvLayoutAbsent, curvLayoutPresent, curvLayoutIdle;
    TextView txtStatusAbsent, txtStatusPresent, txtStatusIdle;
    TextView txtAbsent, txtPresent, txtIdle;
    String Date1;
    int presentCount = 0, absentCount = 0, idleCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // mRootView = inflater.inflate(R.layout.tech_attendance_details, container, false);
        mRootView = inflater.inflate(R.layout.tech_attendance_details_new, container, false);
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_ATTENDANCE, bundle);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        inIt();
        return mRootView;
    }

    private void inIt() {
        mCalendarView = mRootView.findViewById(R.id.calendar_attendance);
        txtDate = mRootView.findViewById(R.id.txtDate);
        txtCheckIn = mRootView.findViewById(R.id.txtCheckIn);
        txtCheckOut = mRootView.findViewById(R.id.txtCheckOut);

        txtDayname = mRootView.findViewById(R.id.txtDayname);

        curvLayoutAbsent = mRootView.findViewById(R.id.curvLayoutAbsent);
        curvLayoutPresent = mRootView.findViewById(R.id.curvLayoutPresent);
        curvLayoutIdle = mRootView.findViewById(R.id.curvLayoutIdle);
        //
        txtStatusAbsent = mRootView.findViewById(R.id.txtStatusAbsent);
        txtStatusPresent = mRootView.findViewById(R.id.txtStatusPresent);
        txtStatusIdle = mRootView.findViewById(R.id.txtStatusIdle);


       /* Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String currentDate = mYear + "-" + (mMonth + 1) + "-" + mDay;*/

        /*mTechAttendanceAsyncTask = new TechAttendanceAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mTechAttendanceAsyncTask.execute(currentDate, SharedPrefManager.getInstance(getContext()).getUserId());*/

        getTechAttendance();

        mCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                try {
                    java.util.Date date = new Date(eventDay.getCalendar().getTime().toString());
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String format = formatter.format(date);
                    System.out.println(format);
                    String finalD = format + "T00:00:00";
                    if (mTechAttend.size() > 0) {
                        for (int i = 0; i < mTechAttend.size(); i++) {
                            if (finalD.contains(mTechAttend.get(i).getDate())) {
                                try {
                                    if (mTechAttend.get(i).getAttendanceTypeId() == 2) { // PRESENT
                                        curvLayoutPresent.setVisibility(View.VISIBLE);
                                        curvLayoutAbsent.setVisibility(View.GONE);
                                        curvLayoutIdle.setVisibility(View.GONE);
                                        txtStatusPresent.setText(mTechAttend.get(i).getAttendance());
                                    } else if (mTechAttend.get(i).getAttendanceTypeId() == 3) { // ABSENT
                                        curvLayoutPresent.setVisibility(View.GONE);
                                        curvLayoutAbsent.setVisibility(View.VISIBLE);
                                        curvLayoutIdle.setVisibility(View.GONE);
                                        txtStatusAbsent.setText(mTechAttend.get(i).getAttendance());
                                    } else if (mTechAttend.get(i).getAttendanceTypeId() == 5) { // IDLE
                                        curvLayoutPresent.setVisibility(View.GONE);
                                        curvLayoutAbsent.setVisibility(View.GONE);
                                        curvLayoutIdle.setVisibility(View.VISIBLE);
                                        txtStatusIdle.setText(mTechAttend.get(i).getAttendance());
                                    }
                                    if (mTechAttend.get(i).getCheckIn() == null) {
                                        txtDate.setText(format);
                                        txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                                        txtCheckIn.setText(/*"CheckIn : " + " " + */"-NA-");
                                    } else {
                                        txtDate.setText(mTechAttend.get(i).getCheckIn().split("T")[0]);
                                        txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                                        txtCheckIn.setText(/*"CheckIn : " + " " + */mTechAttend.get(i).getCheckIn().split("T")[1].split("[.]", 0)[0]);
                                    }
                                    if (mTechAttend.get(i).getCheckOut() == null) {
                                        txtCheckOut.setText(/*"CheckOut : " + " " + */"-NA-");
                                    } else {
                                        txtDate.setText(mTechAttend.get(i).getCheckOut().split("T")[0]);
                                        txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                                        txtCheckOut.setText(/*"CheckOut : " + " " +*/mTechAttend.get(i).getCheckOut().split("T")[1].split("[.]", 0)[0]);
                                    }
                                } catch (Exception ex) {
                                    ex.getMessage();
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });
    }

    private void getTechAttendance() {

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String currentDate = mYear + "-" + (mMonth + 1) + "-" + mDay;

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getContext()).getUserId();
                Call<TechMonthlyAttendance> call = RetrofitClient.getInstance(getContext()).getMyApi().getTechAttendance(userID, currentDate);
                call.enqueue(new retrofit2.Callback<TechMonthlyAttendance>() {
                    @Override
                    public void onResponse(Call<TechMonthlyAttendance> call, Response<TechMonthlyAttendance> response) {
                        try {
                            if (response.code() == 200) {
                                TechMonthlyAttendance techMonthlyAttendance = response.body();
                                mTechAttend = techMonthlyAttendance.getResultData();
                                if (mTechAttend != null) {
                                    mEvents = new ArrayList<>();
                                    for (TechMonthlyAttendance.ResultData resultData : mTechAttend) {
                                        if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.present, getContext()))) {
                                            FWLogger.logInfo(TAG, "Present");
                                            presentCount++;
                                            Date1 = resultData.getDate();
                                            FWLogger.logInfo(TAG, Date1);
                                            Date present = null;
                                            try {
                                                present = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            FWLogger.logInfo(TAG, "calender");
                                            Calendar mPresentDate = Calendar.getInstance();
                                            mPresentDate.setTime(present);
                                            mEvents.add(new EventDay(mPresentDate, R.drawable.marker_calendar_present));
                                            FWLogger.logInfo(TAG, "calender finish");
                                        } else if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.absent, getContext()))) {
                                            FWLogger.logInfo(TAG, "Absent");
                                            absentCount++;
                                            String Date1 = resultData.getDate();
                                            Date absent = null;
                                            try {
                                                absent = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            Calendar mAbsentDate = Calendar.getInstance();
                                            mAbsentDate.setTime(absent);
                                            mEvents.add(new EventDay(mAbsentDate, R.drawable.marker_calendar_absent));
                                        } else if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.idle, getContext()))) {
                                            FWLogger.logInfo(TAG, "Idle");
                                            idleCount++;
                                            Date1 = resultData.getDate();
                                            Date idle = null;
                                            try {
                                                idle = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            Calendar mIdleDate = Calendar.getInstance();
                                            mIdleDate.setTime(idle);
                                            mEvents.add(new EventDay(mIdleDate, R.drawable.marker_calendar_idle));
                                        }
                                    }
                                    FWLogger.logInfo(TAG, techMonthlyAttendance.getMessage());
                                    Calendar min = Calendar.getInstance();
                                    min.set(Calendar.DATE, 0);
                                    Calendar max = Calendar.getInstance();
                                    max.getTimeInMillis();

                                    mCalendarView.setMinimumDate(min);
                                    mCalendarView.setMaximumDate(max);
                                    mCalendarView.setEvents(mEvents);

                                    setTodaysCheckInCheckOut();
                                    setAttedanceCount();
                                } else {
                                    Toast.makeText(getActivity(), "Error Occurred", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TechMonthlyAttendance> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetAttendanceMonthlyForTechnicians API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAttendanceMonthlyForTechnicians API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {

        if (classType.equalsIgnoreCase(TechMonthlyAttendance.class.getSimpleName())) {
            Gson gson = new Gson();
            TechMonthlyAttendance techMonthlyAttendance = gson.fromJson(urlConnectionResponse.resultData, TechMonthlyAttendance.class);

            mTechAttend = techMonthlyAttendance.getResultData();

            if (mTechAttend != null) {
                mEvents = new ArrayList<>();
                for (TechMonthlyAttendance.ResultData resultData : mTechAttend) {
                    if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.present, getContext()))) {
                        FWLogger.logInfo(TAG, "Present");
                        presentCount++;
                        Date1 = resultData.getDate();
                        FWLogger.logInfo(TAG, Date1);
                        Date present = null;
                        try {
                            present = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        FWLogger.logInfo(TAG, "calender");
                        Calendar mPresentDate = Calendar.getInstance();
                        mPresentDate.setTime(present);
                        mEvents.add(new EventDay(mPresentDate, R.drawable.marker_calendar_present));
                        FWLogger.logInfo(TAG, "calender finish");
                    } else if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.absent, getContext()))) {
                        FWLogger.logInfo(TAG, "Absent");
                        absentCount++;
                        String Date1 = resultData.getDate();
                        Date absent = null;
                        try {
                            absent = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar mAbsentDate = Calendar.getInstance();
                        mAbsentDate.setTime(absent);
                        mEvents.add(new EventDay(mAbsentDate, R.drawable.marker_calendar_absent));
                    } else if (resultData.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.idle, getContext()))) {
                        FWLogger.logInfo(TAG, "Idle");
                        idleCount++;
                        Date1 = resultData.getDate();
                        Date idle = null;
                        try {
                            idle = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(Date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar mIdleDate = Calendar.getInstance();
                        mIdleDate.setTime(idle);
                        mEvents.add(new EventDay(mIdleDate, R.drawable.marker_calendar_idle));
                    }
                }
                FWLogger.logInfo(TAG, techMonthlyAttendance.getMessage());
                Calendar min = Calendar.getInstance();
                min.set(Calendar.DATE, 0);
                Calendar max = Calendar.getInstance();
                max.getTimeInMillis();

                mCalendarView.setMinimumDate(min);
                mCalendarView.setMaximumDate(max);
                mCalendarView.setEvents(mEvents);

                setTodaysCheckInCheckOut();
                setAttedanceCount();
            } else {
                Toast.makeText(getActivity(), "Error Occurred", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setTodaysCheckInCheckOut() {
        try {
            Date date = new Date();
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String finalD = currentDate + "T00:00:00";
            if (mTechAttend.size() > 0) {
                for (int i = 0; i < mTechAttend.size(); i++) {
                    if (finalD.contains(mTechAttend.get(i).getDate())) {
                        try {
                            if (mTechAttend.get(i).getAttendanceTypeId() == 2) { // PRESENT
                                curvLayoutPresent.setVisibility(View.VISIBLE);
                                curvLayoutAbsent.setVisibility(View.GONE);
                                curvLayoutIdle.setVisibility(View.GONE);
                                txtStatusPresent.setText(mTechAttend.get(i).getAttendance());
                            } else if (mTechAttend.get(i).getAttendanceTypeId() == 3) { // ABSENT
                                curvLayoutPresent.setVisibility(View.GONE);
                                curvLayoutAbsent.setVisibility(View.VISIBLE);
                                curvLayoutIdle.setVisibility(View.GONE);
                                txtStatusAbsent.setText(mTechAttend.get(i).getAttendance());
                            } else if (mTechAttend.get(i).getAttendanceTypeId() == 5) { // IDLE
                                curvLayoutPresent.setVisibility(View.GONE);
                                curvLayoutAbsent.setVisibility(View.GONE);
                                curvLayoutIdle.setVisibility(View.VISIBLE);
                                txtStatusIdle.setText(mTechAttend.get(i).getAttendance());
                            }
                            if (mTechAttend.get(i).getCheckIn() == null) {
                                txtDate.setText(/*"Date: " + " " + */"-NA-");
                                txtCheckIn.setText(/*"CheckIn : " + " " + */"-NA-");
                                txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                            } else {
                                txtDate.setText(mTechAttend.get(i).getCheckIn().split("T")[0]);
                                txtCheckIn.setText(mTechAttend.get(i).getCheckIn().split("T")[1].split("[.]", 0)[0]);
                                txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                            }
                            if (mTechAttend.get(i).getCheckOut() == null) {
                                txtCheckOut.setText("-NA-");
                            } else {
                                txtDate.setText(mTechAttend.get(i).getCheckOut().split("T")[0]);
                                txtCheckOut.setText(mTechAttend.get(i).getCheckOut().split("T")[1].split("[.]", 0)[0]);
                                txtDayname.setText(String.valueOf(android.text.format.DateFormat.format("EEEE", date)));
                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void setAttedanceCount() {
        try {
            txtPresent = mRootView.findViewById(R.id.txtPresent);
            txtAbsent = mRootView.findViewById(R.id.txtAbsent);
            txtIdle = mRootView.findViewById(R.id.txtIdle);

            txtPresent.setText(String.valueOf(presentCount));
            txtAbsent.setText(String.valueOf(absentCount));
            txtIdle.setText(String.valueOf(idleCount));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
    }

}