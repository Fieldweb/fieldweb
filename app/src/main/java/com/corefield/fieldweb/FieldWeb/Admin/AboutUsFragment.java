package com.corefield.fieldweb.FieldWeb.Admin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;

/**
 * Fragment Controller for Privacy Policy
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show privacy policy
 */
public class AboutUsFragment extends Fragment {
    protected static String TAG = AboutUsFragment.class.getSimpleName();
    private View mRootView;
    private ProgressDialog mProgressDialog;
    private String mType;
    private String mUrl;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_privacy_policy, container, false);

        Bundle getBundle = this.getArguments();
        if (getBundle != null) {
            mType = getBundle.getString("TYPE");
        }


        ((HomeActivityNew) getActivity()).mToolbar.setTitle(getResources().getString(R.string.about_fieldweb));
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).hideHeader();

        inIT();
        return mRootView;
    }

    private void inIT() {
        WebView privacyPolicyWebView = (WebView) mRootView.findViewById(R.id.webView_privacy_policy);
        privacyPolicyWebView.setWebViewClient(new MyBrowser());

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        mUrl = "https://www.fieldweb.co.in/about-us";
        privacyPolicyWebView.getSettings().setLoadsImagesAutomatically(true);
        privacyPolicyWebView.getSettings().setJavaScriptEnabled(true);
        privacyPolicyWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        privacyPolicyWebView.loadUrl(mUrl);
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        ((HomeActivityNew) getActivity()).mBottomNavigationView.setVisibility(View.VISIBLE);
//        ((HomeActivityNew) getActivity()).mBottomConstraint.setVisibility(View.VISIBLE);
        ((HomeActivityNew) getActivity()).mBottomCoordinator.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(getActivity(), "Error:" + description, Toast.LENGTH_SHORT).show();

        }
    }
}
