package com.corefield.fieldweb.FieldWeb.Task;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.Task.UpdateTaskWithEarnedAmount;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateTaskStatusWithEarnedAmountAsyncTask;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for Technician Payment Received
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used for technician to enter amount received after completing task
 */
public class TechPaymentReceivedFragmentNew extends Fragment implements View.OnClickListener, OnTaskCompleteListener {
    protected static String TAG = TechPaymentReceivedFragmentNew.class.getSimpleName();
    private View mRootView;
    private TextView mTextViewAmount, mTextViewTaskName, mTextViewTaskType;

    private Spinner spinPaymntMode;
    Button mButtonCloseTask;
    private EditText mEarnedMoney;
    private String mTakName = "", mTaskShortAddress = "";
    private int mTaskId = 0, mTaskAmount = 0;
    private int mOwnerId = 0;
    private String mCustomerEmail = "", mTaskStatus = "", mTaskType = "", mPaymentType = "";
    private TasksList.ResultData mResultData;
    private UpdateTaskStatusWithEarnedAmountAsyncTask mUpdateTaskStatusWithEarnedAmount;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tech_payment_recevied_fragment_new, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.task);
//        ((HomeActivityNew) getActivity()).hideHeader();
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        Bundle getBundle = this.getArguments();
        if (getBundle != null)
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

        inIt();
        if (getArguments().getBoolean("navigateToTaskTab"))
            manageBackPress();//Navigation from Task Fragment
        return mRootView;
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    private void inIt() {
        //NOTE: To avoid null value exception argument should be matching from EndTaskTechFragment and TaskFragment
        if (mResultData.getName() != null) mTakName = mResultData.getName();
        if (mResultData.getLocationName() != null)
            mTaskShortAddress = mResultData.getLocationName();
        if (mResultData.getWagesPerHours() != 0) mTaskAmount = mResultData.getWagesPerHours();
        if (mResultData.getId() != 0) mTaskId = mResultData.getId();
        if (mResultData.getOwnerId() != 0) mOwnerId = mResultData.getOwnerId();
        if (mResultData.getCustomerEmailId() != null)
            mCustomerEmail = mResultData.getCustomerEmailId();
        if (mResultData.getTaskStatus() != null) mTaskStatus = mResultData.getTaskStatus();
        if (mResultData.getTaskType() != null) mTaskType = mResultData.getTaskType();

        mButtonCloseTask = mRootView.findViewById(R.id.button_close_task);
        mEarnedMoney = mRootView.findViewById(R.id.edittext_finish_task_amount);
        mTextViewAmount = mRootView.findViewById(R.id.textview_finish_amount);
        mTextViewTaskName = mRootView.findViewById(R.id.textview_finish_task_name);
        mTextViewTaskType = mRootView.findViewById(R.id.textview_task_type);

        spinPaymntMode = mRootView.findViewById(R.id.spinPaymntMode);

        mButtonCloseTask.setOnClickListener(this);
        mTextViewTaskName.setText(mTakName);
        mTextViewAmount.setText(String.valueOf(mTaskAmount));
        mTextViewTaskType.setText(mTaskType);
        if (mTaskType != null && !mTaskType.isEmpty()) {
            switch (mTaskType) {
                case "Urgent":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_urgent));
                    break;
                case "Today":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.orange));
                    break;
                case "Schedule":
                    mTextViewTaskType.setTextColor(getResources().getColor(R.color.task_schedule));
                    break;
                default:
                    break;
            }
        }

        // SET SPINNER DATA
        String[] paymentValues = {"Select Payment Type", "Cash", "UPI", "NEFT", "Cheque", "Other"};
        ArrayAdapter<String> paymentType = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, paymentValues);
        spinPaymntMode.setAdapter(paymentType);
    }

    @Override
    public void onClick(View v) {
        updateTask();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivityNew) getActivity()).showHeader();
    }

    private void updateTask() {
        int earnedAmount = 0;
        if (isEmpty(mEarnedMoney)) {
            mEarnedMoney.setError(getString(R.string.please_enter_amount));
        } else if (Integer.valueOf(mEarnedMoney.getText().toString()) == 0) {
            mPaymentType = "Payment Pending";
            UpdateTaskWithEarnedAmount(earnedAmount, mPaymentType);
            FWLogger.logInfo(TAG, "Updating Task ");
        } else {
            earnedAmount = Integer.valueOf(mEarnedMoney.getText().toString());
            try {
                if (spinPaymntMode.getSelectedItem().toString().equalsIgnoreCase("Select Payment Type")) {
                    Toast.makeText(getContext(), "Select Payment Type", Toast.LENGTH_SHORT).show();
                    mPaymentType = "";
                } else if (earnedAmount > mTaskAmount) {
                    Toast.makeText(getContext(), "Entered amount is greater than actual amount", Toast.LENGTH_SHORT).show();
                } else {
                    mPaymentType = spinPaymntMode.getSelectedItem().toString();
                    UpdateTaskWithEarnedAmount(earnedAmount, mPaymentType);
                    FWLogger.logInfo(TAG, "Updating Task ");
                }

            } catch (Exception ex) {
                ex.getMessage();
            }
            //Note Call to Update passbook : update passbook and task completion is performed in this api call
            //mUpdateTaskStatusWithEarnedAmount = new UpdateTaskStatusWithEarnedAmountAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
//            mUpdateTaskStatusWithEarnedAmount.execute(SharedPrefManager.getInstance(getContext()).getUserId(), mTaskId, earnedAmount, TaskState.PAYMENT_RECEIVED, TaskStatus.ON_GOING);
            // mUpdateTaskStatusWithEarnedAmount.execute(SharedPrefManager.getInstance(getContext()).getUserId(), mTaskId, earnedAmount, TaskState.PAYMENT_RECEIVED, TaskStatus.COMPLETED);

        }
    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private void UpdateTaskWithEarnedAmount(int earnedAmount, String mPaymentType) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                Call<UpdateTaskWithEarnedAmount> call = RetrofitClient.getInstance(getContext()).getMyApi().updateTaskWithEarnedAmt(earnedAmount, mTaskId, SharedPrefManager.getInstance(getContext()).getUserId(), TaskState.PAYMENT_RECEIVED, TaskStatus.COMPLETED, mPaymentType, "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateTaskWithEarnedAmount>() {
                    @Override
                    public void onResponse(Call<UpdateTaskWithEarnedAmount> call, Response<UpdateTaskWithEarnedAmount> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                UpdateTaskWithEarnedAmount updateTaskWithEarnedAmount = response.body();
                                if (updateTaskWithEarnedAmount != null) {
                                    if (updateTaskWithEarnedAmount.getCode().equalsIgnoreCase("200") && updateTaskWithEarnedAmount.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.passbook_updated_successfully, getContext()))) {
                                        Toast.makeText(getContext(), getString(R.string.passbook_updated_successfully), Toast.LENGTH_LONG).show();
                                        FWLogger.logInfo(TAG, getString(R.string.passbook_updated_successfully));
                                        //NOTE: Log GA event
                                        Bundle GABundle = new Bundle();
                                        GABundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                        GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                                        GABundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mTaskAmount);
//                        GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS,Constant.TaskCategories.Ongoing.name());
                                        GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Completed.name());
                                        GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.PAYMENT_RECEIVED);
                                        GABundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                        GABundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                        GABundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                        GABundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.PAYMENT_RECEIVED, GABundle);
//                        navigateToTaskClosure();
                                        ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                                    } else {
                                        Toast.makeText(getContext(), updateTaskWithEarnedAmount.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateTaskWithEarnedAmount> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateTaskStatusWithEarnedAmount API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateTaskStatusWithEarnedAmount API:");
            ex.getMessage();
        }

    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        FWLogger.logInfo(TAG, "onTaskComplete()");
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(UpdateTaskWithEarnedAmount.class.getSimpleName())) {
                Gson gson = new Gson();
                UpdateTaskWithEarnedAmount updateTaskWithEarnedAmount = gson.fromJson(urlConnectionResponse.resultData, UpdateTaskWithEarnedAmount.class);
                if (updateTaskWithEarnedAmount != null) {
                    if (updateTaskWithEarnedAmount.getCode().equalsIgnoreCase("200") && updateTaskWithEarnedAmount.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.passbook_updated_successfully, getContext()))) {
                        Toast.makeText(getContext(), getString(R.string.passbook_updated_successfully), Toast.LENGTH_LONG).show();
                        FWLogger.logInfo(TAG, getString(R.string.passbook_updated_successfully));
                        //NOTE: Log GA event
                        Bundle GABundle = new Bundle();
                        GABundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                        GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                        GABundle.putInt(FirebaseGoogleAnalytics.Param.AMOUNT, mTaskAmount);
//                        GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS,Constant.TaskCategories.Ongoing.name());
                        GABundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Completed.name());
                        GABundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.PAYMENT_RECEIVED);
                        GABundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                        GABundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        GABundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        GABundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.PAYMENT_RECEIVED, GABundle);
//                        navigateToTaskClosure();
                        ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    } else {
                        Toast.makeText(getContext(), updateTaskWithEarnedAmount.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }


    private void navigateToTaskClosure() {
        TaskClosureFragmentNew taskClosureFragment = new TaskClosureFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", mResultData);
        taskClosureFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (getArguments().getBoolean("navigateToTaskTab"))
            transaction.replace(R.id.home_fragment_container, taskClosureFragment);//Navigation from Task Fragment
        else
            transaction.replace(R.id.home_fragment_container, taskClosureFragment);// Navigation from notification
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        if (mUpdateTaskStatusWithEarnedAmount != null) {
            mUpdateTaskStatusWithEarnedAmount.cancel(true);
        }
        super.onDestroyView();
    }
}
