package com.corefield.fieldweb.FieldWeb.BackgroundTask;

import android.content.Context;
import android.widget.Toast;

import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.DTO.Task.SendReport;
import com.corefield.fieldweb.Network.Request;
import com.corefield.fieldweb.Network.RequestBuilder;
import com.corefield.fieldweb.Network.URLConnectionRequest;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;

/**
 * Async Task for Add Task
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Async Task class is used to call - Add Task API
 */
public class SendEmailAsyncTask extends BaseAsyncTask {
    protected static String TAG = SendEmailAsyncTask.class.getSimpleName();
    public OnTaskCompleteListener onTaskCompleteListener;

    public SendEmailAsyncTask(Context context, Priority priority, OnTaskCompleteListener onTaskCompleteListener) {
        super(context, priority);
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        Boolean isSuccessful = null;
        if (super.doInBackground(null)) {
            try {
                isSuccessful = false;
                int taskId = (int) objects[0];
                int ownerId = (int) objects[1];
                String emailId = (String) objects[2];
                if (taskId != 0) {
                    String query = "UserId=" + ownerId + "&TaskID=" + taskId + "&EmailId=" + emailId;
                    URLConnectionRequest urlConnectionRequest = new URLConnectionRequest();
                    if (urlConnectionRequest != null) {
                        Request request = RequestBuilder.buildRequest(mContext, URLConstant.Report.SEND_EMAIL_REPORT, null);
                        mUrlConnectionResponse = urlConnectionRequest.sendGetRequest(request.getmURL(), request.getmHeader(), query, request.getToken());
                        if (mUrlConnectionResponse != null) {
                            if (mUrlConnectionResponse.statusCode == 200) {
                                FWLogger.logInfo(TAG, "\n" + mUrlConnectionResponse.toString());
                                isSuccessful = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                FWLogger.logInfo(TAG, "Exception in background service");
                e.printStackTrace();
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status != null && mUrlConnectionResponse != null) {
            if (mUrlConnectionResponse.statusCode != Constant.HttpUrlConn.HTTP_UNDER_MAINTENANCE) {
                if (status) {
                    if (onTaskCompleteListener != null)
                        onTaskCompleteListener.onTaskComplete(mUrlConnectionResponse, SendReport.class.getSimpleName());
                } else {
                   // Toast.makeText(mContext, "Server Error", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
