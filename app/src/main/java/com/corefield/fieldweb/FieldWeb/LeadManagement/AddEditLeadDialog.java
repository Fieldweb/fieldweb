package com.corefield.fieldweb.FieldWeb.LeadManagement;

import static com.corefield.fieldweb.Util.CommonFunction.isEmpty;
import static com.corefield.fieldweb.Util.CommonFunction.isValidPhone;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.LeadManagement.AddCustomerLeadInternalDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadDetailsDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.toptoche.searchablespinnerlibrary.SearchableListDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

public class AddEditLeadDialog {
    private static AddEditLeadDialog mItemDialog;
    public ImageView mImageview1, mImageview2, mImageview3;
    private String mWhichImage = "", mBaseStringFromLead1 = "", mBaseStringFromLead2 = "", mBaseStringFromLead3 = "",
            imageFileName1 = "", imageFileName2 = "", imageFileName3 = "";
    private CountdownTechFragmentNew.ImageUpdateListener mImageUpdateListener;
    private String mCapturedImagePath = "";
    public static final int PICK_IMAGE_GALLERY = 2;
    public static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    private Uri mImageUri;
    Context mContext;
    String mTempLocName = "";
    double mTempLat = 0, mTempLong = 0;
    Place mTempPlace = null;
    private int leadStatusId, serviceId;
    private String mName, mNumber;
    EditText mEditTextCustName,mEditTextCustNumber;

    private AddEditLeadDialog() {
    }

    public static AddEditLeadDialog getInstance() {
        //instantiate a new ExpenditureDialog if we didn't instantiate one yet
        if (mItemDialog == null) {
            mItemDialog = new AddEditLeadDialog();
        }
        return mItemDialog;
    }

    public void addLead(Context context, Activity mActivity) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_lead_fragment);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        ImageView mcloseLeadForm = dialog.findViewById(R.id.closeLeadForm);
        ImageView phonebutton = dialog.findViewById(R.id.imageView_phonebook);

        Button mbtnAddLead = dialog.findViewById(R.id.btnAddLead);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        EditText leadCustName = dialog.findViewById(R.id.leadCustName);
        mEditTextCustName = leadCustName;

        EditText leadCustMobileno = dialog.findViewById(R.id.leadCustMobileno);
        mEditTextCustNumber = leadCustMobileno;

        EditText leadCustAddress = dialog.findViewById(R.id.leadCustAddress);
        EditText leadCustLandmark = dialog.findViewById(R.id.leadCustLandmark);
        /*Spinner leadSpinService = dialog.findViewById(R.id.leadSpinService);*/
        SearchableSpinner leadSpinService = (SearchableSpinner) dialog.findViewById(R.id.leadSpinService);
        EditText leadNotes = dialog.findViewById(R.id.leadNotes);

        mImageview1 = dialog.findViewById(R.id.imageview_photo1);
        mImageview2 = dialog.findViewById(R.id.imageview_photo2);
        mImageview3 = dialog.findViewById(R.id.imageview_photo3);


        mContext = context;
        FGALoadEvent(mContext);

        ArrayList<String> mServiceTypeList = new ArrayList<>();
        EnquiryServiceTypeDTO.ResultData data;
        data = new EnquiryServiceTypeDTO.ResultData();
        data.setId(0);
        data.setServiceName(mActivity.getString(R.string.select_service_type));
        List<EnquiryServiceTypeDTO.ResultData> listOfServiceType = ((HomeActivityNew) mActivity).mServiceTypeResultData;
        listOfServiceType.add(data);

        for (EnquiryServiceTypeDTO.ResultData serviceResultList : listOfServiceType) {
            mServiceTypeList.add(serviceResultList.getServiceName());
        }

        // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
        Set<String> set = new HashSet<>(mServiceTypeList);
        mServiceTypeList.clear();
        mServiceTypeList.addAll(set);

        ArrayAdapter<String> arrayAdapterServiceType = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mServiceTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you don't display last item. It is used as hint.
            }

        };
        arrayAdapterServiceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        leadSpinService.setAdapter(arrayAdapterServiceType);
        leadSpinService.setSelection(arrayAdapterServiceType.getCount());
        /* serviceTypeForEnquiry.setSelection(arrayAdapterServiceType.getCount() - 1);*/


        leadSpinService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < listOfServiceType.size(); i++) {
                    if (parent.getSelectedItem().equals(listOfServiceType.get(i).getServiceName())) {
                        serviceId = listOfServiceType.get(i).getId();
                    }
                }
                FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        phonebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mActivity).getContactDetailsIntentLead();
            }
        });

        mcloseLeadForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        mImageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddLead1";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddLead2";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddLead3";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        leadCustAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leadCustAddress.setError(null);
                // Initialize Places.
                Places.initialize(mActivity.getApplicationContext(), mActivity.getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE);
            }
        });
        mbtnAddLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(leadCustName)) {
                    leadCustName.setError(mContext.getResources().getString(R.string.please_enter_cust_name));
                } else if (!isEmpty(leadCustName) && (Character.isWhitespace(leadCustName.getText().toString().charAt(0)) || leadCustName.getText().toString().trim().isEmpty())) {
                    leadCustName.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(leadCustMobileno)) {
                    leadCustMobileno.setError(mContext.getResources().getString(R.string.please_enter_cust_number));
                } else if (SharedPrefManager.getInstance(mActivity).getCountryDetailsID() == 1 &&
                        isValidPhone(leadCustMobileno)) {
                    leadCustMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(mActivity).getCountryDetailsID() != 1 &&
                        CommonFunction.isValidPhoneNRI(leadCustMobileno)) {
                    leadCustMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if ((Long.valueOf(leadCustMobileno.getText().toString()) == 0)) {
                    leadCustMobileno.setError(mContext.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(leadCustAddress)) {
                    leadCustAddress.setError(mContext.getString(R.string.please_enter_address));
                } else if (isEmpty(leadCustLandmark)) {
                    leadCustLandmark.setError(mContext.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(leadCustLandmark) && (Character.isWhitespace(leadCustLandmark.getText().toString().charAt(0)) || leadCustLandmark.getText().toString().trim().isEmpty())) {
                    leadCustLandmark.setError(mContext.getResources().getString(R.string.space_is_not_allowed));
                } else if (leadSpinService.getSelectedItem() == mActivity.getString(R.string.select_service_type)) {
                    ((TextView) leadSpinService.getSelectedView()).setError(mActivity.getString(R.string.please_select_option));
                } /*else if (isEmpty(leadNotes)) {
                    leadNotes.setError(context.getString(R.string.please_enter_notes));
                }*/ else {
                    AddCustomerLeadInternalDTO.ResultData resultData = new AddCustomerLeadInternalDTO.ResultData();

                    resultData.setCustomerName(leadCustName.getText().toString());
                    resultData.setMobileNumber(leadCustMobileno.getText().toString());
                    resultData.setAddress(leadCustAddress.getText().toString());
                    resultData.setLocName(mTempLocName);
                    resultData.setLocDescription(leadCustLandmark.getText().toString());
                    resultData.setServiceName(leadSpinService.getSelectedItem().toString());
                    resultData.setServicesId(serviceId);
                    resultData.setDescription(leadNotes.getText().toString());
                    resultData.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    resultData.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    resultData.setLatitude(String.valueOf(mTempLat));
                    resultData.setLongitude(String.valueOf(mTempLong));

                   /* if (!imageFileName1.isEmpty() && mBaseStringFromLead1 != null) {
                        resultData.setImageFileName(imageFileName1);
                        resultData.setImageFileBase64Str(mBaseStringFromLead1);
                    }
                    if (!imageFileName2.isEmpty() && mBaseStringFromLead2 != null) {
                        resultData.setImageFileName1(imageFileName2);
                        resultData.setImageFileBase64Str1(mBaseStringFromLead2);
                    }
                    if (!imageFileName3.isEmpty() && mBaseStringFromLead3 != null) {
                        resultData.setImageFileName2(imageFileName3);
                        resultData.setImageFileBase64Str2(mBaseStringFromLead3);
                    }*/

                    resultData.setIsActive(true);
                    resultData.setLeadStatus(1);

                    addLeadForDetailsInternal(mActivity, resultData);
                    refresh(mContext);
                    dialog.dismiss();
                }
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    leadCustAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });
    }


    public void UpdateLeadStatus(Context context, Activity mActivity, ArrayList<String> leadStatusList, List<LeadDetailsDTO.ResultData> mLeadList) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(mActivity, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_lead_status);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();


        ImageView mcloseLeadstatus = dialog.findViewById(R.id.closeLeadstatus);
        Button mbtnUpdateLeadstatus = dialog.findViewById(R.id.btnUpdateLeadstatus);
        Button mButtoncancel = dialog.findViewById(R.id.button_cancel);

        EditText leadCustName = dialog.findViewById(R.id.leadCustName);
        EditText leadCustMobileno = dialog.findViewById(R.id.leadCustMobileno);
        EditText leadCustAddress = dialog.findViewById(R.id.leadCustAddress);
        EditText leadCustLandmark = dialog.findViewById(R.id.leadCustLandmark);
        Spinner leadSpinStatus = dialog.findViewById(R.id.leadSpinStatus);
        EditText leadNotes = dialog.findViewById(R.id.leadNotes);


        mContext = context;
        FGALoadEvent(mContext);

        leadStatusList.add(0, "Select Lead Status");
        Set<String> uniqueSet = new LinkedHashSet<>(leadStatusList);
        List<String> uniqueList = new ArrayList<>(uniqueSet);


        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_spinner_item, uniqueList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.BLACK);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.BLACK);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount();
            }
        };
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        leadSpinStatus.setAdapter(statusAdapter);


        leadSpinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < ((HomeActivityNew) mActivity).mLeadStatusList.size(); i++) {
                    if (parent.getSelectedItem().equals(((HomeActivityNew) mActivity).mLeadStatusList.get(i).getName())) {
                        leadStatusId = ((HomeActivityNew) mActivity).mLeadStatusList.get(i).getId();
                    }
                }
                FWLogger.logInfo(TAG, " LEAD STATUS ID : " + leadStatusId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mcloseLeadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusAdapter.remove("Select Lead Status");
                dialog.dismiss();
            }
        });

        mButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusAdapter.remove("Select Lead Status");
                dialog.dismiss();
            }
        });

        mbtnUpdateLeadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leadSpinStatus.getSelectedItem() == "Select Lead Status") {
                    ((TextView) leadSpinStatus.getSelectedView()).setError(mActivity.getString(R.string.please_select_option));
                } /*else if (isEmpty(leadNotes)) {
                    leadNotes.setError(context.getString(R.string.please_enter_notes));
                }*/ else {
                    AddCustomerLeadInternalDTO.ResultData updateResultData = new AddCustomerLeadInternalDTO.ResultData();

                    updateResultData.setCustomerName(mLeadList.get(0).getCustomerName());
                    updateResultData.setMobileNumber(mLeadList.get(0).getMobileNumber());
                    updateResultData.setAddress(mLeadList.get(0).getAddress());
                    updateResultData.setLocName(mLeadList.get(0).getLocName());
                    updateResultData.setServiceName(leadSpinStatus.getSelectedItem().toString());
                    updateResultData.setServicesId(mLeadList.get(0).getServicesId());
                    updateResultData.setLeadStatusId(leadStatusId);
                    updateResultData.setLeadId(mLeadList.get(0).getLeadId());
                    updateResultData.setDescription(mLeadList.get(0).getDescription());
                    updateResultData.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    updateResultData.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    updateResultData.setUserId(SharedPrefManager.getInstance(mContext).getUserId());
                    updateResultData.setOwnerId(SharedPrefManager.getInstance(mContext).getUserId());
                    updateResultData.setIsActive(true);
                    updateResultData.setLocationId(mLeadList.get(0).getLocationId());
                    updateResultData.setLeadStatus(leadStatusId);
                    updateResultData.setFollowUpNotes(leadNotes.getText().toString());

                    updateLeadStatus(mActivity, updateResultData);
                    refresh(mContext);
                    dialog.dismiss();
                }
            }
        });

        dialog.setOnUpdateDialogListener(new DialogUpdateListener() {
            @Override
            public void onDialogUpdate(Object... obj) {
                FWLogger.logInfo(TAG, "onDialogUpdate");
                Place place = null;
                if (obj != null) {
                    place = (Place) obj[0];
                }

                if (place != null) {
                    leadCustAddress.setText(place.getAddress());
                    mTempPlace = place;
                    mTempLat = mTempPlace.getLatLng().latitude;
                    mTempLong = mTempPlace.getLatLng().longitude;
                    mTempLocName = mTempPlace.getName();
                }

            }
        });

        /////////////////////////////////////////Remove callback from enquiry form/////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });
    }


    private void refresh(Context mContext) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        LeadManagementFragment myFragment = new LeadManagementFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }


    public void setPhotoUpdateForLead1(Bitmap bitmap) {
        mImageview1.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName1 = timeStamp + "_" + "AddUpdateLead1" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead1 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview1.setImageBitmap(bitmap);
    }

    //
    public void setPhotoUpdateForLead2(Bitmap bitmap) {
        mImageview2.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName2 = timeStamp + "_" + "AddUpdateLead2" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead2 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview2.setImageBitmap(bitmap);
    }

    public void setPhotoUpdateForLead3(Bitmap bitmap) {
        mImageview3.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName3 = timeStamp + "_" + "AddUpdateLead3" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromLead3 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview3.setImageBitmap(bitmap);
    }

    public void addLeadForDetailsInternal(Activity mActivity, AddCustomerLeadInternalDTO.ResultData resultData) {
        try {
            CommonFunction.showProgressDialog(mActivity);
            if (Connectivity.isNetworkAvailableRetro(mActivity)) {
                Call<AddCustomerLeadInternalDTO> call = RetrofitClient.getInstance(mContext).getMyApi().addExternalLeadForm(resultData,
                        "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<AddCustomerLeadInternalDTO>() {
                    @Override
                    public void onResponse(Call<AddCustomerLeadInternalDTO> call, Response<AddCustomerLeadInternalDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(mActivity);
                                AddCustomerLeadInternalDTO addCustomerLeadInternalDTO = response.body();
                                if (addCustomerLeadInternalDTO != null) {
                                    if (addCustomerLeadInternalDTO.getCode().equalsIgnoreCase("200")) {
                                        Toast.makeText(mContext, addCustomerLeadInternalDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(mContext, addCustomerLeadInternalDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    //FWLogger.logInfo(TAG, "Add External Lead Form = " + addCustomerLeadInternalDTO.getResultData());
                                    Toast.makeText(mContext, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCustomerLeadInternalDTO> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(mActivity);
                        FWLogger.logInfo(TAG, "Exception in AddCustomerLead API:");
                    }
                });
            } else {
                CommonFunction.hideProgressDialog(mActivity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(mActivity);
            FWLogger.logInfo(TAG, "Exception in AddCustomerLead API:");
            ex.getMessage();
        }

    }

    public void updateLeadStatus(Activity mActivity, AddCustomerLeadInternalDTO.ResultData resultData) {
        try {
            //CommonFunction.showProgressDialog(mActivity);
            if (Connectivity.isNetworkAvailableRetro(mActivity)) {
                Call<AddCustomerLeadInternalDTO> call = RetrofitClient.getInstance(mContext).getMyApi().updateLeadStatus(resultData,
                        "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<AddCustomerLeadInternalDTO>() {
                    @Override
                    public void onResponse(Call<AddCustomerLeadInternalDTO> call, Response<AddCustomerLeadInternalDTO> response) {
                        try {
                            if (response.code() == 200) {
                                //CommonFunction.hideProgressDialog(mActivity);
                                AddCustomerLeadInternalDTO addCustomerLeadInternalDTO = response.body();
                                if (addCustomerLeadInternalDTO != null) {
                                    if (addCustomerLeadInternalDTO.getCode().equalsIgnoreCase("200")) {
                                        Toast.makeText(mContext, addCustomerLeadInternalDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(mContext, addCustomerLeadInternalDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(mContext, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCustomerLeadInternalDTO> call, Throwable throwable) {
                        //CommonFunction.hideProgressDialog(mActivity);
                        FWLogger.logInfo(TAG, "Exception in Lead_UpdateLeadStatus API:");
                    }
                });
            } else {
                // CommonFunction.hideProgressDialog(mActivity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            // CommonFunction.hideProgressDialog(mActivity);
            FWLogger.logInfo(TAG, "Exception in Lead_UpdateLeadStatus API:");
            ex.getMessage();
        }

    }


    private void FGALoadEvent(Context mContext) {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(mContext).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(mContext).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(mContext).logEvent(FirebaseGoogleAnalytics.Event.ADD_LEAD, bundle);
    }
    public void getContactData(String strName, String strNumber) {
        mName = strName;
        mNumber = strNumber;
        //Toast.makeText(mContext, mName + "/" + mNumber, Toast.LENGTH_SHORT).show();
        //if(!mName.isEmpty() && !mNumber.isEmpty()){
        mEditTextCustName.setText(mName);
        mEditTextCustNumber.setText(mNumber);
        //}
    }
}
