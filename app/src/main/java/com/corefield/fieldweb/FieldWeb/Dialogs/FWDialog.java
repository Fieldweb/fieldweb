package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Dialog;
import android.content.Context;

import com.corefield.fieldweb.Listener.DialogUpdateListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FWDialog extends Dialog {

    public static DialogUpdateListener mDialogUpdateListener;

    public FWDialog(@NonNull Context context) {
        super(context);
    }

    public FWDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected FWDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static void callback(Object... obj) {
        if (mDialogUpdateListener != null) mDialogUpdateListener.onDialogUpdate(obj);
    }

    public static void removeCallBack() {
        if (mDialogUpdateListener != null) mDialogUpdateListener = null;
    }

    public void setOnUpdateDialogListener(DialogUpdateListener dialogUpdateListener) {
        FWDialog.mDialogUpdateListener = dialogUpdateListener;
    }

}