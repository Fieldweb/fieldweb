package com.corefield.fieldweb.FieldWeb.Admin;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.corefield.fieldweb.Adapter.CustomerListAdapterNew;
import com.corefield.fieldweb.Adapter.EnquiryListAdapterNew;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateEnquiryAsyncTask;
import com.corefield.fieldweb.FieldWeb.Dialogs.EnquiryDialogNew;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Listener.RecyclerTouchListenerUpdateItem;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment Controller for CRMFragment
 *
 * @author CoreField
 * @version 2.0
 * @implNote This Fragment class is used to show Enquiry and Customer list
 */
public class ItemInventoryTabHost extends Fragment implements RecyclerTouchListener, RecyclerTouchListenerUpdateItem {
    protected static String TAG = ItemInventoryTabHost.class.getSimpleName();
    private View mRootView;
    private RecyclerView mRecyclerViewList;
    private Button mButtonEnquiries, mButtonCustomers;
    private List<EnquiryList.ResultData> mEnquiryLists;
    private EnquiryListAdapterNew mEnquiryListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SearchView mSearchView;
    //
    private ViewPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private List<CustomerList.ResultData> mCustomerList;
    private CustomerListAdapterNew mCustomerListAdapter;
    private String mSelection = "";
    Bundle mBundle;
    EnquiryList.ResultData resultData = null;
    private UpdateEnquiryAsyncTask mUpdateEnquiryAsyncTask;
    Button plusenq;

    @Override
    public void onResume() {
        super.onResume();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_iteminventory_tabhost, container, false);
        //NOTE: Log GA event
        mBundle = new Bundle();
        mBundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        mBundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        mBundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        mBundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        mBundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_LIST, mBundle);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        inIT();
        return mRootView;
    }

    private void inIT() {
        mRecyclerViewList = mRootView.findViewById(R.id.recycler_list_view);
        mButtonEnquiries = mRootView.findViewById(R.id.button_enquiries);
        mButtonCustomers = mRootView.findViewById(R.id.button_customers);
        //mSearchView = mRootView.findViewById(R.id.edittext_search);
        plusenq = mRootView.findViewById(R.id.plus_enq);

        //
        viewPager = (ViewPager) mRootView.findViewById(R.id.viewpager_calculator);
        setupViewPager(viewPager);


        tabLayout = (TabLayout) mRootView.findViewById(R.id.tabs_calculator);
        tabLayout.setupWithViewPager(viewPager);

        mSelection = "ENQ";
        // mSearchView.setQueryHint("Search by Enq no., Customer name...");
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new OwnerItemInventoryFragmentNew(), getString(R.string.inventory));
        adapter.addFragment(new OwnerUsedItemInventoryFragmentNew(), getString(R.string.used_item));
        viewPager.setAdapter(adapter);
    }

    public void updateEnquiry(EnquiryList.ResultData resultData) {
        EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
        enquiryDialog.updateEnquiryDialog(getActivity(), ((HomeActivityNew) getActivity()).mServiceTypeResultData, resultData, this);
    }


    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.enquiry_card_view:
                //NOTE: Log GA event
                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.ENQUIRY_DETAILS, bundle);

                EnquiryDialogNew enquiryDialog = EnquiryDialogNew.getInstance();
                enquiryDialog.enquiryDetailsDialog(getActivity(), mEnquiryListAdapter.getItem(position));
                break;

            case R.id.imageView_call:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mCustomerListAdapter.getItem(position).getMobileNumber()));
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_message:
                if (mCustomerListAdapter.getItem(position).getMobileNumber() != null) {
                    try {
                        String number = mCustomerListAdapter.getItem(position).getMobileNumber();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    } catch (Exception e) {
                        Toast.makeText(getContext(), R.string.unable_to_open_message_app, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imageView_call_cust:
                if (mEnquiryLists != null && mEnquiryListAdapter != null) {
                    resultData = mEnquiryListAdapter.getItem(position);
                    if (resultData.getMobileNumber() != null && !resultData.getMobileNumber().isEmpty()) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + resultData.getMobileNumber()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.imageView_edit:
                if (mEnquiryLists != null && mEnquiryListAdapter != null) {
                    resultData = mEnquiryListAdapter.getItem(position);
//                ((HomeActivityNew) getActivity()).addEnquiry();
//                ((HomeActivityNew) getActivity()).updateEnquiry(resultData);
                    updateEnquiry(resultData);
                }
                break;

            default:
                break;
        }

    }

    @Override
    public void onUpdateClick(View view, int position) {

    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        super.onDestroyView();
    }
}
