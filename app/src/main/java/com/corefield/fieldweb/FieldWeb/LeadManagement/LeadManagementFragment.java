package com.corefield.fieldweb.FieldWeb.LeadManagement;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.CRMTasksListAdapter;
import com.corefield.fieldweb.Adapter.LeadListAdapter;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Enquiry.ServiceType;
import com.corefield.fieldweb.DTO.LeadManagement.LeadListDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadStatusListDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.TeleCMI.Connect;
import com.corefield.fieldweb.DTO.TeleCMI.Pcmo;
import com.corefield.fieldweb.DTO.TeleCMI.TeleCMICall;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.PaginationScrollListener;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for OwnerItemInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show item inventory where Owner can add item and Assign Item
 */
public class LeadManagementFragment extends Fragment implements View.OnClickListener, RecyclerTouchListener {
    protected static String TAG = LeadManagementFragment.class.getSimpleName();
    public List<LeadListDTO.ResultData> mLeadList = null;
    public List<UsersList.ResultData> mUsersLists = null;
    public List<LeadStatusListDTO.ResultData> mLeadStatusList = null;
    public ArrayList<String> mLeadNameList;
    public ArrayList<String> mUsersNameList;
    public ArrayList<String> mSpineerLeadStatusList;
    private View mRootView;
    ImageView noResultImg;
    private String mSearchParam = "";
    public final int REQUEST_CODE_PERMISSIONS = 0x1;
    ArrayList<String> tempLeadList;

    private RecyclerView mRecyclerViewLeadList;
    private LeadListAdapter mleadListAdapter;
    /*private RecyclerView.LayoutManager mLayoutManager;*/
    private LinearLayoutManager mLinearLayoutManager;
    private Button mButtonInventory, mButtonIssuedItems, mAddLead;
    private String mSelection = "";
    private SearchView mSearchView;
    private ImageView mgetlinkForm;
    RecyclerTouchListener recyclerTouchListener;
    Fragment fragment;
    Spinner spinLeadStatus;
    //
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int mCurrentPage = PAGE_START, mStatusCheck = 0, mLeadStatusSelectedId;
    private boolean isLoadFirstTime = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.lead_management_fragment, container, false);
        mButtonInventory = mRootView.findViewById(R.id.button_inventory);
        mButtonIssuedItems = mRootView.findViewById(R.id.button_issued_item);
        mAddLead = mRootView.findViewById(R.id.addLead);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        noResultImg = mRootView.findViewById(R.id.noResultImg);
        mgetlinkForm = mRootView.findViewById(R.id.getlinkForm);
        spinLeadStatus = mRootView.findViewById(R.id.spinLeadStatus);
        mRecyclerViewLeadList = mRootView.findViewById(R.id.recycler_lead_list);
        recyclerTouchListener = this;
        fragment = this;

        try {
            //if (((HomeActivityNew) getActivity()).mLeadStatusList.size() > 0) {
                //setSpinnerData();
                getLeadStatusList();
                // getAllLeadList(recyclerTouchListener, fragment, mSearchParam, mLeadStatusSelectedId, mCurrentPage);
           // }
        } catch (Exception ex) {
            ex.getMessage();
        }


        FGALoadEvent();
        //manageBackPress();
        mButtonInventory.setOnClickListener(this);
        mButtonIssuedItems.setOnClickListener(this);

        mAddLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    checkCameraPermission();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddEditLeadDialog addEditLeadDialog = AddEditLeadDialog.getInstance();
                    addEditLeadDialog.addLead(getContext(), activity);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        mgetlinkForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonDialog commonDialog = CommonDialog.getInstance();
                commonDialog.getLeadFormLink(getActivity());
            }
        });

       /* mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewLeadList.addOnScrollListener(new PaginationScrollListener(mLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage += 1;
                if (!isLastPage) {
                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            FWLogger.logInfo(TAG, "loadNextPage()");
                            isLoadFirstTime = false;
                            loadNextPage();
                        }
                    }, 1000);
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });*/


        return mRootView;
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + mCurrentPage);
        getAllLeadList(recyclerTouchListener, fragment, mSearchParam, mLeadStatusSelectedId, mCurrentPage);
    }

    @Override
    public void onResume() {
        super.onResume();
        isLoadFirstTime = true;
        mCurrentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        //
        //setSpinnerData();
        FWLogger.logInfo(TAG, "On Resume Called ");
    }


    public void getAllLeadList(RecyclerTouchListener recyclerTouchListener, Fragment fragment, String mSearchParam, int leadStatusId, int mCurrentPage) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                //CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<LeadListDTO> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getAllLeadList(userID, mCurrentPage, mSearchParam, leadStatusId,
                        "Bearer " + SharedPrefManager.getInstance(getActivity()).getUserToken());
                call.enqueue(new retrofit2.Callback<LeadListDTO>() {
                    @Override
                    public void onResponse(Call<LeadListDTO> call, Response<LeadListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                //CommonFunction.hideProgressDialog(getActivity());
                                FWLogger.logInfo(TAG, "Get Lead list");
                                mLeadList = new ArrayList<>();
                                LeadListDTO leadListDTO = response.body();
                                mLeadList = leadListDTO.getResultData();
                                if (mLeadList.size() > 0) {
                                    mRecyclerViewLeadList.setVisibility(View.VISIBLE);
                                    mLeadNameList = new ArrayList<>();
                                    for (LeadListDTO.ResultData resultData : mLeadList) {
                                        mLeadNameList.add(resultData.getServiceName());
                                        FWLogger.logInfo(TAG, resultData.getServiceName());
                                    }
                                    //mLayoutManager = new LinearLayoutManager(getActivity());
                                    mLinearLayoutManager = new LinearLayoutManager(getActivity());
                                    mRecyclerViewLeadList.setLayoutManager(mLinearLayoutManager);
                                    mRecyclerViewLeadList.setItemAnimator(new DefaultItemAnimator());
                                    mleadListAdapter = new LeadListAdapter(getActivity(), mLeadList, recyclerTouchListener, fragment);
                                    mRecyclerViewLeadList.setAdapter(mleadListAdapter);
                                    mleadListAdapter.setClickListener(recyclerTouchListener);
                                } else {
                                    mRecyclerViewLeadList.setVisibility(View.GONE);
                                    noResultImg.setVisibility(View.VISIBLE);
                                    noResultImg.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_noresultfound));
                                }
                                setSearchFilter();

                            }
                        } catch (
                                Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<LeadListDTO> call, Throwable throwable) {
                        //CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in GetAllLeadList API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (
                Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAllLeadList API:");
            ex.getMessage();
        }

    }

    public void getLeadStatusList() {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<LeadStatusListDTO> call = RetrofitClient.getInstance(getActivity()).getMyApi().getLeadStatusList();
                call.enqueue(new retrofit2.Callback<LeadStatusListDTO>() {
                    @Override
                    public void onResponse(Call<LeadStatusListDTO> call, Response<LeadStatusListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                LeadStatusListDTO leadStatusListDTO = response.body();
                                FWLogger.logInfo(TAG, "Received Lead Status Successfully");
                                mLeadStatusList = new ArrayList<>();
                                mLeadStatusList = leadStatusListDTO.getResultData();

                                mSpineerLeadStatusList = new ArrayList<>();

                                for (LeadStatusListDTO.ResultData resultData : mLeadStatusList) {
                                    mSpineerLeadStatusList.add(resultData.getName());
                                }

                                setSpinnerData();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<LeadStatusListDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetLeadStatusList Type API :");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetLeadStatusList API :");
            ex.getMessage();
        }
    }


    private void setSpinnerData() {
        try {
            tempLeadList = new ArrayList<>();
            /* tempLeadList = ((HomeActivityNew) getActivity()).mSpineerLeadStatusList;*/
            tempLeadList = mSpineerLeadStatusList;
            tempLeadList.add(0, "Select Lead Status");

            Set<String> uniqueSet = new LinkedHashSet<>(tempLeadList);
            List<String> uniqueList = new ArrayList<>(uniqueSet);


            ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_item, uniqueList/*((HomeActivityNew) getActivity()).mSpineerLeadStatusList*/) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextColor(Color.WHITE);
                    if (position == getCount()) {
                        ((TextView) v.findViewById(android.R.id.text1)).setText("");
                        ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                        ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.WHITE);
                    }
                    return v;
                }

                @Override
                public int getCount() {
                    return super.getCount();
                }
            };
            statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinLeadStatus.setAdapter(statusAdapter);

            spinLeadStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (++mStatusCheck > 1) {
                        //if (mLeadList != null) clearTaskList();
                        //mLeadStatusSelectedId = position;
                        mSearchView.setQuery("", false);
                        for (int i = 0; i < ((HomeActivityNew) getActivity()).mLeadStatusList.size(); i++) {
                            if (((HomeActivityNew) getActivity()).mLeadStatusList.get(i).getName().equalsIgnoreCase(spinLeadStatus.getSelectedItem().toString())) {
                                mLeadStatusSelectedId = ((HomeActivityNew) getActivity()).mLeadStatusList.get(i).getId();
                            }
                        }
                        if (spinLeadStatus.getSelectedItem().toString().equalsIgnoreCase("Select Lead Status")) {
                            mLeadStatusSelectedId = 0;
                            mCurrentPage = 1;
                            getAllLeadList(recyclerTouchListener, fragment, mSearchParam, mLeadStatusSelectedId, mCurrentPage);
                        } else {
                            getAllLeadList(recyclerTouchListener, fragment, mSearchParam, mLeadStatusSelectedId, mCurrentPage);
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
        getAllLeadList(recyclerTouchListener, fragment, mSearchParam, mLeadStatusSelectedId, mCurrentPage);

    }

    public void clearTaskList() {

        try {
            int size = mLeadList.size();
            mLeadList.clear();
            mleadListAdapter.notifyItemRangeRemoved(0, size);

            // For loading data after selecting task type from spinner
            isLoadFirstTime = true;
            mCurrentPage = PAGE_START;
            isLoading = false;
            isLastPage = false;
            mSearchParam = "";

            mRecyclerViewLeadList.setLayoutManager(mLinearLayoutManager);
            mRecyclerViewLeadList.setItemAnimator(new DefaultItemAnimator());
            mleadListAdapter = new LeadListAdapter(getActivity(), mLeadList, recyclerTouchListener, fragment);
            mRecyclerViewLeadList.setAdapter(mleadListAdapter);
            mleadListAdapter.setClickListener(recyclerTouchListener);
            mleadListAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.LEAD_MANGEMENT, bundle);
    }

    private void refresh() {
        AppCompatActivity activity = (AppCompatActivity) getContext();
        LeadManagementFragment myFragment = new LeadManagementFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }


   /* @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                //mSearchView.setFocusable(true);
                break;

            case R.id.leadlistCallbtn:
                try {
                    if (mleadListAdapter.getItem(position).getMobileNumber() != null) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + mleadListAdapter.getItem(position).getMobileNumber()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex) {
                    ex.getMessage();
                }

                break;


            default:
                break;
        }
    }*/

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);

        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if (mleadListAdapter != null)
                    //if (mleadListAdapter != null) clearTaskList();
                    mleadListAdapter.getFilter().filter(query);
                mSearchParam = query;
                mockingNetworkDelay(query, 0, 0);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when query submitted
                if (mleadListAdapter != null)
                    mleadListAdapter.getFilter().filter(query);
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    private void mockingNetworkDelay(String searchParam, int typeId, int statusId) {
        // mocking network delay for API call
        Log.d(TAG, "mockingNetworkDelay: " + mCurrentPage);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // load list first time
                isLoadFirstTime = true;
                getAllLeadList(recyclerTouchListener, fragment, searchParam, mLeadStatusSelectedId, mCurrentPage);
            }
        }, 1000);
    }


    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        //setSpinnerData();
        super.onDestroyView();
    }

    @Override
    public void onClick(View view, int position) {
        /*switch (view.getId()) {
            case R.id.leadlistCallbtn:
                *//*try {
                    if (mleadListAdapter.getItem(position).getMobileNumber() != null) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + mleadListAdapter.getItem(position).getMobileNumber()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), R.string.contact_no_is_not_available, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex) {
                    ex.getMessage();
                }
*//*
                try {
                    if (SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("true") ||
                            SharedPrefManager.getInstance(getActivity()).getTeleCMIModuleFlag().equalsIgnoreCase("True")) {
                        TelCMI(getActivity(), mleadListAdapter.getItem(position).getMobileNumber());
                    } else {
                        if (mleadListAdapter.getItem(position).getMobileNumber() != null && !mleadListAdapter.getItem(position).getMobileNumber().isEmpty()) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + mleadListAdapter.getItem(position).getMobileNumber()));
                            getActivity().startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.contact_no_is_not_available), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

                break;
        }*/
    }

    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            },
                            REQUEST_CODE_PERMISSIONS);
                } else {
                    if (cameraFlag == false) {
                        requestPermissions(new String[]{
                                        Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                },
                                REQUEST_CODE_PERMISSIONS);
                    } else {
                        cameraFlag = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
        return cameraFlag;
    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    //AppCompatActivity activity = (AppCompatActivity) getApplicationContext();
                    LeadManagementFragment myFragment = new LeadManagementFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
                    //((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View v) {

    }
}
