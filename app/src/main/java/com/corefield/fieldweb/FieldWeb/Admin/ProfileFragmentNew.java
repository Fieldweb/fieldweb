package com.corefield.fieldweb.FieldWeb.Admin;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ReplacementTransformationMethod;
import android.util.Base64;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.PlacesAutoCompleteAdapter;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Enquiry.ServiceType;
import com.corefield.fieldweb.DTO.Item.IssueItem;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.User.AuthMobileEmail;
import com.corefield.fieldweb.DTO.User.UpdateProfilePic;
import com.corefield.fieldweb.DTO.User.UpdateUser;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.FWDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Services.SmsBroadcastReceiver;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.ViewUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Response;

import static com.corefield.fieldweb.FieldWeb.BaseActivity.PICK_IMAGE_GALLERY;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Fragment Controller for ProfileFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used to show all the details of User
 */
public class ProfileFragmentNew extends Fragment implements OnTaskCompleteListener, View.OnClickListener, BaseActivity.RequestPermissionsUpdateListener, PlacesAutoCompleteAdapter.ClickListener, DatePickerDialog.OnDateSetListener, OnOtpCompletionListener {  //, PlaceSelectionListener
    protected static final String TAG = ProfileFragmentNew.class.getSimpleName();
    public List<ItemsList.ResultData> mItemsLists = null;
    TextView textView_validate, textView_submit;
    public ArrayList<String> mItemsNameList;
    public static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private View mRootView;
    private UserDetails.ResultData mUserDetails = new UserDetails.ResultData();
    private Uri mImageUri;
    private int mUserID = 0;
    private String mTechFullName = "";
    UserDetails.ResultData mResultData;
    UsersList.ResultData mListResultData;
    private boolean isProfileImage = true;
    private int serviceId = 0;
    private String strFrom = "";
    private int mOTP;
    private int mOTPEnteredByUser = 0;
    SmsBroadcastReceiver smsBroadcastReceiver;
    private List<ServiceType.ResultData> mServiceTypeResultData = null;
    private ArrayList<String> mServiceTypeList;

    /*AppCompatAutoCompleteTextView mCityAutoComplete;
    PlaceArrayAdapter mPlaceArrayAdapter;
    RectangularBounds CURRENT_LOCATION_BONDS;*/

    EditText mEditTextFirstName, mEditTextLastName, mEditTextContact, mEditTextAddress, mEditTextEmail, mEditTextCompanyAddress, mEditTextGSTPanNo, mEditTextCompanyContact, mEditTextCompanyName, mEditTextCompanyWebsite, mEditTextReferralCode, mEditTextDOB, mEditTextCity, medtAadharCard, meditText_NoofEmp;
    EditText editText_Ownerfirstname, editText_Ownerlastname;

    TextInputLayout mTextInputCompanyName, mtextInput_aadharCard, mtextInput_companyType, mtextInput_NoofEmp;
    TextView mTextViewLogoHint, mTextViewCompLogoClose;

    CircleImageView mImageView;
    Button mButtonSave;
    SearchableSpinner enquiry_spin_services;
    ImageView mImageViewCompLogo;//, mImageViewSearchCity;
    private double mTempLat = 0, mTempLong = 0;
    private Place mTempPlace = null;
    private String mTempLocName = "";
    private String mImageEncodeBaseStringProfile;
    private String mImageEncodeBaseStringCompany;
    private LinearLayout mLinearCompanyDetails;
    private boolean isAddress = false, isCompAddress = false;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 23488;
    private String mScreen;
    private static final int REQUEST_SELECT_PLACE = 1000;
    private boolean IsEmailIdUpdate = false, IsMobileNoUpdate = false;
    // Create a new Places client instance.
    PlacesClient placesClient;
    private StringBuilder mResult;
    TextInputLayout txtMobileNo_Layout, txtEmailId_Layout;
    TextInputLayout textInput_Ownerfirst_name, textInput_OwnerLast_name;
    ImageView imageView_editMobile, imageView_editEmail;
    List<ServiceType.ResultData> listOfServiceType;
    private Calendar mCal;
    private int mYear, mMonth, mDay;

    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    private static final int REQ_USER_CONSENT = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_edit_profile_new, container, false);

        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

        try {
            Bundle getBundle = this.getArguments();
            if (getBundle != null) {
                mScreen = getBundle.getString("screen");
                if (mScreen.equalsIgnoreCase("home")) {
                    mResultData = (UserDetails.ResultData) getBundle.getSerializable("profileData");
            /*else if(screen.equalsIgnoreCase("notiTechList"))
                mListResultData = (UsersList.ResultData) getBundle.getSerializable("profileData");*/
                } else {
                    mListResultData = (UsersList.ResultData) getBundle.getSerializable("profileData");
                }
            } else {
                mScreen = "ProfileEdit";
                getProfileDetails();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        if (!Places.isInitialized()) {
            Places.initialize(getActivity(), getResources().getString(R.string.place_api_key));
        }
        placesClient = Places.createClient(getActivity());

        /*CURRENT_LOCATION_BONDS = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363), //dummy lat/lng
                new LatLng(-33.858754, 151.229596));*/

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.profile);
        FGALoadEvent();
        inIt();
        return mRootView;
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {
                    recyclerView.setVisibility(View.VISIBLE);
                }
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    @Override
    public void click(Place place) {
        if (place.getAddress() != null)
            mEditTextCity.setText("" + place.getAddress().split(",")[0]);
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
        }
//        Toast.makeText(getActivity(), place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude, Toast.LENGTH_SHORT).show();
    }

    private void inIt() {
        mEditTextFirstName = mRootView.findViewById(R.id.editText_firstname);
        mEditTextLastName = mRootView.findViewById(R.id.editText_lastname);
        mEditTextContact = mRootView.findViewById(R.id.editText_contact);
        mEditTextAddress = mRootView.findViewById(R.id.editText_address);
        mEditTextEmail = mRootView.findViewById(R.id.editText_email);
        mEditTextCompanyName = mRootView.findViewById(R.id.editText_company_name);
        mEditTextCompanyAddress = mRootView.findViewById(R.id.editText_company_address);
        mEditTextGSTPanNo = mRootView.findViewById(R.id.editText_gst_or_pan_no);
        mEditTextCompanyContact = mRootView.findViewById(R.id.editText_company_cont_no);
        mEditTextCompanyWebsite = mRootView.findViewById(R.id.editText_company_website);
        mEditTextReferralCode = mRootView.findViewById(R.id.editText_referral_code);
        mEditTextDOB = mRootView.findViewById(R.id.editText_dob);
        mEditTextCity = mRootView.findViewById(R.id.editText_city);
        mImageView = mRootView.findViewById(R.id.imageView_user_profile);
        mImageViewCompLogo = mRootView.findViewById(R.id.signup_screen_company_image);
//        mImageViewSearchCity = mRootView.findViewById(R.id.imageView_search_city);
        mTextViewCompLogoClose = mRootView.findViewById(R.id.close_comp_image);
        mTextViewLogoHint = mRootView.findViewById(R.id.textview_add_comp_logo_hint);
        mLinearCompanyDetails = mRootView.findViewById(R.id.linear_company_details);
        mTextInputCompanyName = mRootView.findViewById(R.id.textInput_company_name);
        mtextInput_aadharCard = mRootView.findViewById(R.id.textInput_aadharCard);

        //mtextInput_companyType = mRootView.findViewById(R.id.textInput_companyType);
        mtextInput_NoofEmp = mRootView.findViewById(R.id.textInput_NoofEmp);

        medtAadharCard = mRootView.findViewById(R.id.edtAadharCard);
        /*enquiry_spin_services = mRootView.findViewById(R.id.enquiry_spin_services);*/
        enquiry_spin_services = (SearchableSpinner) mRootView.findViewById(R.id.enquiry_spin_services);
        meditText_NoofEmp = mRootView.findViewById(R.id.editText_NoofEmp);

        mEditTextGSTPanNo.setTransformationMethod(new UpperCaseTransform());

        imageView_editMobile = (ImageView) mRootView.findViewById(R.id.imageView_editMobile);
        imageView_editEmail = (ImageView) mRootView.findViewById(R.id.imageView_editEmail);

        txtMobileNo_Layout = mRootView.findViewById(R.id.txtMobileNo_Layout);
        txtEmailId_Layout = mRootView.findViewById(R.id.txtEmailId_Layout);

        editText_Ownerfirstname = mRootView.findViewById(R.id.editText_Ownerfirstname);
        editText_Ownerlastname = mRootView.findViewById(R.id.editText_Ownerlastname);
        textInput_Ownerfirst_name = mRootView.findViewById(R.id.textInput_Ownerfirst_name);
        textInput_OwnerLast_name = mRootView.findViewById(R.id.textInput_OwnerLast_name);

        recyclerView = (RecyclerView) mRootView.findViewById(R.id.places_recycler_view);
//        ((EditText) mRootView.findViewById(R.id.editText_city)).addTextChangedListener(filterTextWatcher);
        mEditTextCity.addTextChangedListener(filterTextWatcher);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();

        /*mCityAutoComplete = mRootView.findViewById(R.id.autoCompleteEditText);
        mCityAutoComplete.setThreshold(3);

        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), R.layout.language_list_item, CURRENT_LOCATION_BONDS);*/

        mButtonSave = mRootView.findViewById(R.id.button_save);

        // CHECK USER IS INDIAN OR NON-INDIAN : 1=INDIAN / 2-NRI
        try {
            if (SharedPrefManager.getInstance(getActivity()).getCountryDetailsID() == 1) {
                txtMobileNo_Layout.setVisibility(View.VISIBLE);
                imageView_editMobile.setVisibility(View.VISIBLE);
            } else {
                txtMobileNo_Layout.setVisibility(View.GONE);
                imageView_editMobile.setVisibility(View.GONE);

            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // FOR TECH : SHOW OWNER FIRST / LAST NAME
        try {
            if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
                textInput_Ownerfirst_name.setVisibility(View.VISIBLE);
                textInput_OwnerLast_name.setVisibility(View.VISIBLE);
                try {
                    editText_Ownerfirstname.setText(mResultData.getOwnerFirstName());
                    editText_Ownerlastname.setText(mResultData.getOwnerLastName());
                    editText_Ownerfirstname.setEnabled(false);
                    editText_Ownerlastname.setEnabled(false);
                } catch (Exception e) {
                    e.getMessage();
                }
            } else {
                textInput_Ownerfirst_name.setVisibility(View.GONE);
                textInput_OwnerLast_name.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.getMessage();
        }

        try {
            if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {

                listOfServiceType = ((HomeActivityNew) getActivity()).mServiceTypeResultDataProfile;


                if (mScreen.equalsIgnoreCase("techList")) {
                    mtextInput_aadharCard.setVisibility(View.VISIBLE);
                    mtextInput_aadharCard.setEnabled(false);
                    mtextInput_NoofEmp.setVisibility(View.GONE);
                    enquiry_spin_services.setVisibility(View.GONE);
                } else {
                    mtextInput_NoofEmp.setVisibility(View.VISIBLE);
                    enquiry_spin_services.setVisibility(View.VISIBLE);
                    ArrayList<String> mServiceTypeList = new ArrayList<>();
                    ServiceType.ResultData data = new ServiceType.ResultData();
                    data.setId(0);
                    data.setServiceName(getContext().getString(R.string.select_service_type));
                    listOfServiceType.add(data);
                    for (ServiceType.ResultData serviceResultList : listOfServiceType) {
                        mServiceTypeList.add(serviceResultList.getServiceName());
                    }
                    // FOR REMOVING DUPLICATE HINT LIKE Select Service Type
                    Set<String> set = new HashSet<>(mServiceTypeList);
                    mServiceTypeList.clear();
                    mServiceTypeList.addAll(set);

                    ArrayAdapter<String> arrayAdapterServiceType = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, mServiceTypeList) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            if (position == getCount()) {
                                ((TextView) v.findViewById(android.R.id.text1)).setText("");
                                ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                            }
                            return v;
                        }

                        @Override
                        public int getCount() {
                            return super.getCount() - 1; // you don't display last item. It is used as hint.
                        }

                    };
                    arrayAdapterServiceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    enquiry_spin_services.setAdapter(arrayAdapterServiceType);
                    /*enquiry_spin_services.setSelection(arrayAdapterServiceType.getCount());*/
                    enquiry_spin_services.setSelection(arrayAdapterServiceType.getCount() - 1);

                    enquiry_spin_services.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listOfServiceType.size(); i++) {
                                if (parent.getSelectedItem().equals(listOfServiceType.get(i).getServiceName())) {
                                    serviceId = listOfServiceType.get(i).getId();
                                }
                            }
                            //serviceId = listOfServiceType.get(position).getId();
                            FWLogger.logInfo(TAG, " ServiceId : " + serviceId);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

            } else {
                mtextInput_aadharCard.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        imageView_editMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    strFrom = "MOBILE";
                    VerifyOTPDialog(getActivity(), strFrom);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });

        imageView_editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    strFrom = "EMAIL";
                    VerifyOTPDialog(getActivity(), strFrom);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });

        /*mImageViewSearchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCity();
            }
        });*/

        if (mScreen.equalsIgnoreCase("home")) {
            try {
                Picasso.get().load(mResultData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mImageView.setImageResource(R.drawable.profile_icon);
                    }
                });
            } catch (Exception e) {
                e.getMessage();
            }

            try {
                Picasso.get().load(mResultData.getCompanyLogo()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
//                    .centerCrop()
//                    .fit()
//                    .resize(50, 50)
                        .into(mImageViewCompLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                mTextViewLogoHint.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
//                mImageViewCompLogo.setImageResource(R.drawable.profile_icon);
                                mTextViewLogoHint.setVisibility(View.VISIBLE);
                            }
                        });
            } catch (Exception e) {
                e.getMessage();
            }


            mEditTextFirstName.setText(mResultData.getFirstName());
            mEditTextLastName.setText(mResultData.getLastName());
            if (mResultData.getContactNo() != null) {
                mEditTextContact.setText(mResultData.getContactNo());
                //mEditTextContact.setTextColor(getResources().getColor(R.color.light_gray));
                txtMobileNo_Layout.setEnabled(false);
            } else {
                mEditTextContact.setText(mResultData.getContactNo());
                txtMobileNo_Layout.setEnabled(true);
            }

            mEditTextAddress.setText(mResultData.getAddress());
            if (mResultData.getEmail() != null) {
                mEditTextEmail.setText(mResultData.getEmail());
                //mEditTextEmail.setTextColor(getResources().getColor(R.color.light_gray));
                txtEmailId_Layout.setEnabled(false);
            } else {
                mEditTextEmail.setText(mResultData.getEmail());
                txtEmailId_Layout.setEnabled(false);
            }

            mEditTextCompanyName.setText(mResultData.getCompanyName());
            mEditTextCompanyAddress.setText(mResultData.getCompanyAddress());
            mEditTextGSTPanNo.setText(mResultData.getCompanyGSTorPanNo());
            if (mResultData.getDOB() != null && !mResultData.getDOB().isEmpty())
                mEditTextDOB.setText(DateUtils.convertDateFormat(mResultData.getDOB(), "yyyy-MM-dd'T'HH:mm:ss", "MM-dd-yyyy"));

            if (mResultData.getCompanyCity() != null && !mResultData.getCompanyCity().isEmpty())
                mEditTextCity.setText(mResultData.getCompanyCity());

            if (recyclerView.getVisibility() == View.VISIBLE) {
                recyclerView.setVisibility(View.GONE);
            }

            //
            if (mResultData.getAadharCardNo() != null) {
                medtAadharCard.setText((mResultData.getAadharCardNo()));
            }
            if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
                if (mResultData.getCompanyServiceTypeId() != 0) {
                    try {
                        String assignedService = null;
                        for (int i = 0; i < listOfServiceType.size(); i++) {
                            if (mResultData.getCompanyServiceTypeId() == listOfServiceType.get(i).getId()) {
                                assignedService = listOfServiceType.get(i).getServiceName();
                                serviceId = listOfServiceType.get(i).getId();

                            }
                        }
                        FWLogger.logInfo(TAG, "Service Name : " + assignedService);
                        if (assignedService != null) {
                            enquiry_spin_services.setSelection(((ArrayAdapter<String>) enquiry_spin_services.getAdapter()).getPosition(assignedService));
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }

                if (mResultData.getNoOfUsers() != null) {
                    meditText_NoofEmp.setText(mResultData.getNoOfUsers());
                }
            }


            if (mResultData.getCompanyContactNo() != 0)
                mEditTextCompanyContact.setText("" + mResultData.getCompanyContactNo());

            mEditTextCompanyWebsite.setText(mResultData.getCompanyWebsite());
            /*if (mResultData.getReferralCodeName() != null)
                mEditTextReferralCode.setText("" + mResultData.getReferralCodeName());*/

        } else if (mListResultData != null) {
            try {
                Picasso.get().load(mListResultData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mImageView.setImageResource(R.drawable.profile_icon);
                    }
                });
            } catch (Exception e) {
                e.getMessage();
            }

            mEditTextFirstName.setText(mListResultData.getFirstName());
            mEditTextLastName.setText(mListResultData.getLastName());
            if (mListResultData.getContactNo() != null) {
                mEditTextContact.setText(mListResultData.getContactNo());
                //mEditTextContact.setTextColor(getResources().getColor(R.color.light_gray));
                txtMobileNo_Layout.setEnabled(false);
            } else {
                mEditTextContact.setText(mListResultData.getContactNo());
                txtMobileNo_Layout.setEnabled(true);
            }

            mEditTextAddress.setText(mListResultData.getAddress());
            if (mListResultData.getEmail() != null) {
                mEditTextEmail.setText(mListResultData.getEmail());
                txtEmailId_Layout.setEnabled(false);
                //mEditTextEmail.setTextColor(getResources().getColor(R.color.light_gray));
            } else {
                mEditTextEmail.setText(mListResultData.getEmail());
                txtEmailId_Layout.setEnabled(false);
            }

            if (mListResultData.getDOB() != null && !mListResultData.getDOB().isEmpty())
                mEditTextDOB.setText(DateUtils.convertDateFormat(mListResultData.getDOB(), "yyyy-MM-dd'T'HH:mm:ss", "MM-dd-yyyy"));

            if (mListResultData.getAadharCardNo() != null) {
                medtAadharCard.setText((mListResultData.getAadharCardNo()));
            }
        }

        if (SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && mScreen.equalsIgnoreCase("home") || SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && mScreen.equalsIgnoreCase("ProfileEdit")) {
            mTextInputCompanyName.setVisibility(View.VISIBLE);
            mLinearCompanyDetails.setVisibility(View.VISIBLE);
        } else {
            mEditTextCompanyName.setVisibility(View.GONE);
            mLinearCompanyDetails.setVisibility(View.GONE);
        }

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission();
                if (SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && !mScreen.equalsIgnoreCase("home")) {
                    if (mScreen.equalsIgnoreCase("ProfileEdit")) {
                        isProfileImage = true;
                        PickImage();
                    } else {
                        Toast.makeText(getActivity(), R.string.owner_cant_change_tech_profile, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isProfileImage = true;
                    PickImage();
                }
            }
        });

        mImageViewCompLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCameraPermission();
                isProfileImage = false;
                PickImage();
            }
        });

        mEditTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAddress = true;
                Places.initialize(getContext(), getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(getActivity());
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        mEditTextCompanyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCompAddress = true;
                Places.initialize(getContext(), getResources().getString(R.string.place_api_key));
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(getActivity());
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        mTextViewCompLogoClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageEncodeBaseStringCompany = null;
                mTextViewCompLogoClose.setVisibility(View.INVISIBLE);
                mTextViewLogoHint.setVisibility(View.VISIBLE);
                mImageViewCompLogo.setImageBitmap(null);
            }
        });

        mCal = Calendar.getInstance();
        mYear = mCal.get(Calendar.YEAR);
        mMonth = mCal.get(Calendar.MONTH);
        mDay = mCal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, this, mYear, mMonth, mDay) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                /*int monthId = getContext().getResources().getIdentifier("month", "id", "android");
                int dayId = getContext().getResources().getIdentifier("day", "id", "android");
                int yearId = getContext().getResources().getIdentifier("year", "id", "android");

                NumberPicker monthPicker = (NumberPicker) findViewById(monthId);
                NumberPicker dayPicker = (NumberPicker) findViewById(dayId);
                NumberPicker yearPicker = (NumberPicker) findViewById(yearId);

                setDividerColor(monthPicker);
                setDividerColor(dayPicker);
                setDividerColor(yearPicker);*/

            }
        };
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        mEditTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DateUtils.birthDayDatePicker(getActivity(), mEditTextDOB);
               /* DialogFragment dFragment = new DatePickerFragment();
                dFragment.show(getFragmentManager(), "Date Picker");*/
                datePickerDialog.show();
            }
        });

        medtAadharCard.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Remove spacing char
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                // Insert char where needed.
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                        s.insert(s.length() - 1, String.valueOf(space));
                    }
                }
            }
        });

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(mEditTextFirstName)) {
                    mEditTextFirstName.setError(getResources().getString(R.string.please_enter_first_name));
                } else if (mEditTextFirstName.getText().toString().contains(" ") || mEditTextFirstName.getText().toString().trim().isEmpty()) {
                    mEditTextFirstName.setError(getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(mEditTextLastName)) {
                    mEditTextLastName.setError(getResources().getString(R.string.please_enter_last_name));
                } else if (mEditTextLastName.getText().toString().contains(" ") || mEditTextLastName.getText().toString().trim().isEmpty()) {
                    mEditTextLastName.setError(getResources().getString(R.string.space_is_not_allowed));
                } else if (SharedPrefManager.getInstance(getActivity()).getCountryDetailsID() == 1 && isEmpty(mEditTextContact)) {
                    mEditTextContact.setError(getString(R.string.please_enter_contact_number));
                } else if (SharedPrefManager.getInstance(getActivity()).getCountryDetailsID() == 1 && isValidPhone(mEditTextContact)) {
                    mEditTextContact.setError(getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(getActivity()).getCountryDetailsID() == 1 && (Long.valueOf(mEditTextContact.getText().toString()) == 0)) {
                    mEditTextContact.setError(getString(R.string.please_enter_valid_number));
                } else if (mEditTextEmail.getText().toString() != null && !mEditTextEmail.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(mEditTextEmail.getText().toString()).matches() == false) {
                    mEditTextEmail.setError(getString(R.string.please_enter_valid_email_id));
                } else if (isEmpty(mEditTextDOB)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    mEditTextDOB.setError(getString(R.string.please_enter_date));
                } else if (isEmpty(mEditTextCompanyName) && SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && mScreen.equalsIgnoreCase("home")) {
                    mEditTextCompanyName.setError(getString(R.string.company_name_is_required));
                } else if (!isEmpty(mEditTextGSTPanNo) && isValidGSTOrPAN(mEditTextGSTPanNo)) {
                    mEditTextGSTPanNo.setError(getString(R.string.please_enter_valid_gst_pan));
                } else if (SharedPrefManager.getInstance(getActivity()).getCountryDetailsID() == 1 && !isEmpty(mEditTextCompanyContact) && isValidPhone(mEditTextCompanyContact)) {
                    mEditTextCompanyContact.setError(getString(R.string.please_enter_valid_contact_number));
                } else if (!isEmpty(mEditTextCompanyContact) && CommonFunction.isValidPhoneNRI(mEditTextCompanyContact)) {
                    mEditTextCompanyContact.setError(getString(R.string.please_enter_valid_contact_number));
                }
                /*else if (!isEmpty(mEditTextCompanyContact) && isValidPhone(mEditTextCompanyContact)) {
                    mEditTextCompanyContact.setError(getString(R.string.please_enter_valid_contact_number));
                }*/
                else if (!isEmpty(mEditTextCompanyWebsite) && !isValidWebsite(mEditTextCompanyWebsite)) {
                    mEditTextCompanyWebsite.setError(getString(R.string.please_enter_valid_website));
                } else if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN) && (medtAadharCard.getText().toString().length() < 14)) {
                    medtAadharCard.setError(getString(R.string.enter_valid_aadhar_number));
                } else if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && enquiry_spin_services.getSelectedItem() == getActivity().getString(R.string.select_service_type)) {
                    ((TextView) enquiry_spin_services.getSelectedView()).setError(getContext().getString(R.string.please_select_option));
                } else if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && isNumberGreater(meditText_NoofEmp) && mtextInput_NoofEmp.getVisibility() == View.VISIBLE) {
                    meditText_NoofEmp.setError(getContext().getString(R.string.no_of_worker_should_not_greater));
                } else {
                    UpdateUser.ResultData updateUser = new UpdateUser.ResultData();
                    updateUser.setFirstName(mEditTextFirstName.getText().toString());
                    updateUser.setLastName(mEditTextLastName.getText().toString());
                    updateUser.setContactNo(mEditTextContact.getText().toString());
                    updateUser.setEmail(mEditTextEmail.getText().toString());
                    updateUser.setAddress(mEditTextAddress.getText().toString());
                    updateUser.setDOB(mEditTextDOB.getText().toString());
                    updateUser.setCompanyCity(mEditTextCity.getText().toString());

                    if (SharedPrefManager.getInstance(getActivity()).getUserGroup() != null && SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) && mScreen.equalsIgnoreCase("home")) {
                        updateUser.setCompanyId(mResultData.getCompanyId());
                        updateUser.setCompanyName(mEditTextCompanyName.getText().toString());
                        updateUser.setCompanyAddress(mEditTextCompanyAddress.getText().toString());
                        updateUser.setCompanyGSTorPanNo(mEditTextGSTPanNo.getText().toString());
//                            if(editTextReferralCode.getText()!= null) updateUser.setReferralCode(editTextReferralCode.getText().toString());
                        if (mEditTextCompanyWebsite.getText() != null)
                            updateUser.setCompanyWebsite(mEditTextCompanyWebsite.getText().toString());
                        if (mEditTextCompanyContact.getText() != null) {
                            try {
//                                    int phone = Integer.parseInt(editTextCompanyContact.getText().toString());
                                int phone = (int) Double.parseDouble(mEditTextCompanyContact.getText().toString());
                                updateUser.setCompanyContactNo(phone);
                            } catch (NumberFormatException nfe) {
                                System.out.println("Could not parse " + nfe);
                            }
                        }

                        updateUser.setCompanyWebsite(mEditTextCompanyWebsite.getText().toString());

                        if (!medtAadharCard.getText().toString().isEmpty()) {
                            updateUser.setAadharCardNo(medtAadharCard.getText().toString());
                        }
                        updateUser.setCompanyServiceTypeId(serviceId);

                        if (meditText_NoofEmp.getText() != null) {
                            updateUser.setNoOfUsers(meditText_NoofEmp.getText().toString());
                        }

                        if (mImageEncodeBaseStringCompany != null) {
                            updateUser.setCompanyLogo(mImageEncodeBaseStringCompany);
                            updateUser.setCompanyLogoName(mResultData.getUserName() + "Company.jpg");
                        }

                        updateUser.setFileName(mResultData.getUserName() + ".jpg");
                        updateUser.setFile(mImageEncodeBaseStringProfile);
                        updateUser.setUserId(mResultData.getId());

                        /*if (isEmpty(mEditTextCity)) {
                            mEditTextCity.setError(getString(R.string.please_enter_city));
                        }*/
                    } else if (mScreen.equalsIgnoreCase("ProfileEdit")) {
                        try {
                            updateUser.setCompanyId(mUserDetails.getCompanyId());
                            updateUser.setCompanyName(mEditTextCompanyName.getText().toString());
                            updateUser.setCompanyAddress(mEditTextCompanyAddress.getText().toString());
                            updateUser.setCompanyGSTorPanNo(mEditTextGSTPanNo.getText().toString());
                            if (mEditTextCompanyWebsite.getText() != null)
                                updateUser.setCompanyWebsite(mEditTextCompanyWebsite.getText().toString());
                            if (mEditTextCompanyContact.getText() != null) {
                                try {
                                    int phone = (int) Double.parseDouble(mEditTextCompanyContact.getText().toString());
                                    updateUser.setCompanyContactNo(phone);
                                } catch (NumberFormatException nfe) {
                                    System.out.println("Could not parse " + nfe);
                                }
                            }
                            if (mImageEncodeBaseStringCompany != null) {
                                updateUser.setCompanyLogo(mImageEncodeBaseStringCompany);
                                updateUser.setCompanyLogoName(mUserDetails.getUserName() + "Company.jpg");
                            }

                            if (medtAadharCard.getText() != null) {
                                updateUser.setAadharCardNo(medtAadharCard.getText().toString());
                            }
                            try {
                                updateUser.setCompanyServiceTypeId(serviceId);
                            } catch (Exception e) {
                                e.getMessage();
                            }

                            if (meditText_NoofEmp.getText() != null) {
                                updateUser.setNoOfUsers(meditText_NoofEmp.getText().toString());
                            }

                            updateUser.setFileName(mUserDetails.getUserName() + ".jpg");
                            updateUser.setFile(mImageEncodeBaseStringProfile);
                            updateUser.setUserId(mUserDetails.getId());
                            updateUser.setCompanyId(mUserDetails.getCompanyId());
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    /*if (mScreen.equalsIgnoreCase("home")) {
                        updateUser.setFileName(mResultData.getUserName() + ".jpg");
                        updateUser.setFile(mImageEncodeBaseStringProfile);
                        updateUser.setUserId(mResultData.getId());
                    }*/
                    if (mScreen.equalsIgnoreCase("home")) {
                        try {
                            if (!medtAadharCard.getText().toString().isEmpty()) {
                                updateUser.setAadharCardNo(medtAadharCard.getText().toString());
                            }
                            updateUser.setFileName(mResultData.getUserName() + ".jpg");
                            updateUser.setFile(mImageEncodeBaseStringProfile);
                            updateUser.setUserId(mResultData.getId());
                            updateUser.setCompanyId(mResultData.getCompanyId());
                        } catch (Exception e) {
                            e.getMessage();
                        }
                       /* updateUser.setFileName(mResultData.getUserName() + ".jpg");
                        updateUser.setFile(mImageEncodeBaseStringProfile);
                        updateUser.setUserId(mResultData.getId());*/
                    }
                    try {
                        if (mScreen.equalsIgnoreCase("techList")) {
                            updateUser.setUserId(mListResultData.getId());
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                    updateUser.setUpdatedBy(SharedPrefManager.getInstance(getActivity()).getUserId());
                    updateUser.setIsSuccessful(true);
                    updateUser.setIsModelError(true);
                    if (IsMobileNoUpdate) {
                        updateUser.setMobileNoUpdate(true);
                    } else {
                        updateUser.setMobileNoUpdate(false);
                    }
                    if (IsEmailIdUpdate) {
                        updateUser.setEmailIdUpdate(true);
                    } else {
                        updateUser.setEmailIdUpdate(false);
                    }
                    //API call
                    updateProfileUser(updateUser);
                }
            }

            public boolean isValidPhone(EditText text) {
                CharSequence phone = text.getText().toString();
                return phone.length() != 10;
            }

            private boolean isValidGSTOrPAN(EditText text) {
                CharSequence GstOrPan = text.getText().toString();
                return GstOrPan.length() > 15 || GstOrPan.length() < 6;
            }

           /* private boolean isValidaAadharNumber(String aadhaCardNumber) {
                try {
                    Pattern pattern = Pattern.compile("^[2-9]{1}[0-9]{3}\\\\s[0-9]{4}\\\\s[0-9]{4}$");
                    Matcher matcher = pattern.matcher(aadhaCardNumber);
                    if (matcher.matches()) {
                        return true;
                    } else {
                        //medtAadharCard.setError(R.string.note_error);
                        return false;
                    }

                } catch (Exception e) {
                    e.getMessage();
                }
                return true;
            }
*/

            private boolean isValidWebsite(EditText text) {
                String WebUrl = "^((ftp|http|https):\\/\\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\\.[a-zA-Z]+)+((\\/)[\\w#]+)*(\\/\\w+\\?[a-zA-Z0-9_]+=\\w+(&[a-zA-Z0-9_]+=\\w+)*)?$";
                String website = text.getText().toString().trim();
                if (website.trim().length() > 0) {
                    if (!website.matches(WebUrl)) {
                        //validation msg
                        return false;
                    }
                }
                return true;
            }

            private boolean isNumberGreater(EditText edtNoOfWorker) {
                try {
                    //Pattern pattern = Pattern.compile("^(0?[1-9]|[1-9][0-9])$"); 0-99
                    Pattern pattern = Pattern.compile("^[1-9][0-9]?$|^100$"); // 0-100
                    Matcher matcher = pattern.matcher(edtNoOfWorker.getText().toString());
                    if (!edtNoOfWorker.getText().toString().isEmpty()) {
                        if (matcher.matches()) {
                            return false;
                        }
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

                return true;
            }

            private boolean isEmpty(EditText text) {
                CharSequence str = text.getText().toString();
                return TextUtils.isEmpty(str);
            }
        });
    }

    private void SetUserDetailsFromEdit() {
        try {
            // FROM TECHNICIAN DASHBOARD
            try {
                Picasso.get().load(mUserDetails.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        mImageView.setImageResource(R.drawable.profile_icon);
                    }
                });
            } catch (Exception e) {
                e.getMessage();
            }

            try {
                Picasso.get().load(mUserDetails.getCompanyLogo()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(mImageViewCompLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                        mTextViewLogoHint.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
//                mImageViewCompLogo.setImageResource(R.drawable.profile_icon);
                        mTextViewLogoHint.setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception e) {
                e.getMessage();
            }

            try {
                if (SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
                    if (mScreen.equalsIgnoreCase("ProfileEdit")) {
                        try {
                            editText_Ownerfirstname.setText(mUserDetails.getOwnerFirstName());
                            editText_Ownerlastname.setText(mUserDetails.getOwnerLastName());
                            editText_Ownerfirstname.setEnabled(false);
                            editText_Ownerlastname.setEnabled(false);
                        } catch (Exception e) {
                            e.getMessage();
                        }

                    }
            } catch (Exception e) {
                e.getMessage();
            }

            try {
                mEditTextFirstName.setText(mUserDetails.getFirstName());
                mEditTextLastName.setText(mUserDetails.getLastName());
                if (mUserDetails.getContactNo() != null) {
                    txtMobileNo_Layout.setEnabled(false);
                    //mEditTextContact.setTextColor(getResources().getColor(R.color.light_gray));
                    mEditTextContact.setText(mUserDetails.getContactNo());
                } else {
                    mEditTextContact.setText(mUserDetails.getContactNo());
                    txtMobileNo_Layout.setEnabled(true);
                }
                mEditTextAddress.setText(mUserDetails.getAddress());
                if (mUserDetails.getContactNo() != null) {
                    mEditTextEmail.setText(mUserDetails.getEmail());
                    txtEmailId_Layout.setEnabled(false);
                    //mEditTextEmail.setTextColor(getResources().getColor(R.color.light_gray));
                } else {
                    mEditTextEmail.setText(mUserDetails.getEmail());
                    txtEmailId_Layout.setEnabled(false);
                }

                mEditTextCompanyName.setText(mUserDetails.getCompanyName());
                mEditTextCompanyAddress.setText(mUserDetails.getCompanyAddress());
                mEditTextGSTPanNo.setText(mUserDetails.getCompanyGSTorPanNo());
                if (mUserDetails.getDOB() != null && !mUserDetails.getDOB().isEmpty())
                    mEditTextDOB.setText(DateUtils.convertDateFormat(mUserDetails.getDOB(), "yyyy-MM-dd'T'HH:mm:ss", "MM-dd-yyyy"));

                if (mUserDetails.getCompanyCity() != null && !mUserDetails.getCompanyCity().isEmpty())
                    mEditTextCity.setText(mUserDetails.getCompanyCity());

                if (recyclerView.getVisibility() == View.VISIBLE) {
                    recyclerView.setVisibility(View.GONE);
                }

                if (mUserDetails.getCompanyContactNo() != 0)
                    mEditTextCompanyContact.setText("" + mUserDetails.getCompanyContactNo());

                mEditTextCompanyWebsite.setText(mUserDetails.getCompanyWebsite());

                if (mUserDetails.getAadharCardNo() != null) {
                    medtAadharCard.setText((mUserDetails.getAadharCardNo()));
                }
                if (mUserDetails.getCompanyServiceTypeId() != 0) {
                    try {
                        if (mUserDetails.getCompanyServiceTypeId() != 0) {
                            String assignedService = null;
                            for (int i = 0; i < listOfServiceType.size(); i++) {
                                if (mUserDetails.getCompanyServiceTypeId() == listOfServiceType.get(i).getId()) {
                                    assignedService = listOfServiceType.get(i).getServiceName();
                                    serviceId = listOfServiceType.get(i).getId();
                                }
                            }
                            FWLogger.logInfo(TAG, "Service Name : " + assignedService);
                            if (assignedService != null) {
                                enquiry_spin_services.setSelection(((ArrayAdapter<String>) enquiry_spin_services.getAdapter()).getPosition(assignedService));
                            }

                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
                if (mUserDetails.getNoOfUsers() != null) {
                    meditText_NoofEmp.setText(mUserDetails.getNoOfUsers());
                }
               /* if (mUserDetails.getReferralCodeName() != null)
                    mEditTextReferralCode.setText("" + mUserDetails.getReferralCodeName());*/
            } catch (Exception ex) {
                ex.getMessage();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void getCity() {
        try {
                    /*AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                            .build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder
                            (PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(getActivity());
                    startActivityForResult(intent, REQUEST_SELECT_PLACE);*/

            Toast.makeText(getActivity(), mEditTextCity.getText().toString(), Toast.LENGTH_SHORT).show();
            // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
            // and once again when the user makes a selection (for example when calling fetchPlace()).
            AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
            // Create a RectangularBounds object.
            RectangularBounds bounds = RectangularBounds.newInstance(new LatLng(-33.880490, 151.184363), //dummy lat/lng
                    new LatLng(-33.858754, 151.229596));
            // Use the builder to create a FindAutocompletePredictionsRequest.
            FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                    // Call either setLocationBias() OR setLocationRestriction().
                    .setLocationBias(bounds)
                    //.setLocationRestriction(bounds)
//                            .setCountry("in")//India
                    .setTypeFilter(TypeFilter.CITIES).setSessionToken(token).setQuery(mEditTextCity.getText().toString()).build();

            placesClient.findAutocompletePredictions(request).addOnSuccessListener(response -> {
                mResult = new StringBuilder();
                for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                    mResult.append(" ").append(prediction.getFullText(null) + "\n");
                    FWLogger.logInfo(TAG, prediction.getPlaceId());
                    FWLogger.logInfo(TAG, prediction.getPrimaryText(null).toString());
                    Toast.makeText(getActivity(), prediction.getPrimaryText(null) + "-" + prediction.getSecondaryText(null), Toast.LENGTH_SHORT).show();
                }
//                        mEditTextCity.setText(String.valueOf(mResult));
            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    FWLogger.logInfo(TAG, "Place not found: " + apiException.getStatusCode());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        try {
            mCal.set(Calendar.YEAR, year);
            mCal.set(Calendar.MONTH, month);
            mCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM-dd-yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            mEditTextDOB.setText(sdf.format(mCal.getTime()));
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onOtpCompleted(String otp) {
        FWLogger.logInfo(TAG, "onOtpCompleted");
        mOTPEnteredByUser = Integer.valueOf(otp);
        ViewUtils.hideKeyboard(getActivity());
    }

    /*@Override
    public void onPlaceSelected(@NonNull @NotNull Place place) {
        FWLogger.logInfo(TAG, "Place Selected: " + place.getAddress());
    }

    @Override
    public void onError(@NonNull @NotNull Status status) {
        FWLogger.logInfo(TAG, "Place status: " + status.getStatusMessage());
    }*/

    //First is the method of lowercase to uppercase
    public class UpperCaseTransform extends ReplacementTransformationMethod {
        @Override
        protected char[] getOriginal() {
            char[] aa = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
            return aa;
        }

        @Override
        protected char[] getReplacement() {
            char[] cc = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
            return cc;
        }

    }

    private void FGALoadEvent() {
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.PROFILE, bundle);
    }

    private void getItemList() {
       /* mItemsListAsyncTask = new ItemsListAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mItemsListAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<ItemsList> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getItemList(userID);
                call.enqueue(new retrofit2.Callback<ItemsList>() {
                    @Override
                    public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                        try {
                            if (response.code() == 200) {
                                FWLogger.logInfo(TAG, "Get Item list");
                                ItemsList itemsLists = response.body();
                                mItemsLists = new ArrayList<>();
                                mItemsLists = itemsLists.getResultData();
                                mItemsNameList = new ArrayList<>();

                                for (ItemsList.ResultData resultData : mItemsLists) {
                                    mItemsNameList.add(resultData.getName());
                                    FWLogger.logInfo(TAG, resultData.getName());
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ItemsList> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AllItemListAssignAndUnAssignV2 API:");
            ex.getMessage();
        }
    }

    public void updateProfileUser(UpdateUser.ResultData updateUser) {
        /*mUpdateUserAsyncTask = new UpdateUserAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUpdateUserAsyncTask.execute(updateUser);*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<UpdateUser> call = RetrofitClient.getInstance(getContext()).getMyApi().updateUser(updateUser, "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<UpdateUser>() {
                    @Override
                    public void onResponse(Call<UpdateUser> call, Response<UpdateUser> response) {
                        try {
                            if (response.code() == 200) {
                                UpdateUser updateUser = response.body();
                                //NOTE: Log GA event
                                Bundle bundle = new Bundle();
                                bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
                                bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.EDIT);
                                FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.CONFIRM_PROFILE, bundle);
                                FWLogger.logInfo(TAG, "Updated user Name " + updateUser.getResultData().getFirstName());
                                if (updateUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.profile_updated_successfully, getContext()))) {
                                    Toast.makeText(getContext(), getString(R.string.profile_updated_successfully), Toast.LENGTH_SHORT).show();
                                    ((HomeActivityNew) getActivity()).getUSerDetails();
                                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
                                } else {
                                    Toast.makeText(getContext(), updateUser.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateUser> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in UpdateUser API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in UpdateUser API:");
            ex.getMessage();
        }

    }

    /*public void issuedItem(IssueItem.ResultData issueItem) {
        mIssueItemAsyncTask = new IssueItemAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mIssueItemAsyncTask.execute(issueItem);
    }*/

    public void getProfileDetails() {
        /*mProfileDetailsAsyncTask = new GetProfileDetailsAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mProfileDetailsAsyncTask.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<UserDetails> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getProfileDetails(userID);
                call.enqueue(new retrofit2.Callback<UserDetails>() {
                    @Override
                    public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                        try {
                            if (response.code() == 200) {
                                UserDetails userDetails = response.body();
                                mUserDetails = userDetails.getResultData();
                                //Update UI
                                if (mScreen.equalsIgnoreCase("ProfileEdit")) {
                                    SetUserDetailsFromEdit();
                                } else {
                                    if (mUserDetails != null) {
                                        if (mUserDetails.getIsRegistered()) getItemList();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetails> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetProfileDetails? API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(UserDetails.class.getSimpleName())) {
            Gson gson = new Gson();
            UserDetails userDetails = gson.fromJson(urlConnectionResponse.resultData, UserDetails.class);
            mUserDetails = userDetails.getResultData();
            //Update UI
            if (mScreen.equalsIgnoreCase("ProfileEdit")) {
                SetUserDetailsFromEdit();
            } else {
                if (mUserDetails != null) {
                    if (mUserDetails.getIsRegistered()) getItemList();
                }
            }
        } else if (classType.equalsIgnoreCase(UpdateUser.class.getSimpleName())) {
            Gson gson = new Gson();
            UpdateUser updateUser = gson.fromJson(urlConnectionResponse.resultData, UpdateUser.class);
            //NOTE: Log GA event
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
            bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
            bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
            bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
            bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
            bundle.putString(FirebaseGoogleAnalytics.Param.OPERATION, FirebaseGoogleAnalytics.Defaults.CONFIRMATION);
            bundle.putString(FirebaseGoogleAnalytics.Param.ACTION, FirebaseGoogleAnalytics.Defaults.EDIT);
            FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.CONFIRM_PROFILE, bundle);
            FWLogger.logInfo(TAG, "Updated user Name " + updateUser.getResultData().getFirstName());
            if (updateUser.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.profile_updated_successfully, getContext()))) {
                Toast.makeText(getContext(), getString(R.string.profile_updated_successfully), Toast.LENGTH_SHORT).show();
                ((HomeActivityNew) getActivity()).getUSerDetails();
                ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_home);
            } else {
                Toast.makeText(getContext(), updateUser.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (classType.equalsIgnoreCase(UpdateProfilePic.class.getSimpleName())) {
            Gson gson = new Gson();
            UpdateProfilePic updateProfilePic = gson.fromJson(urlConnectionResponse.resultData, UpdateProfilePic.class);
            FWLogger.logInfo(TAG, "pic message " + updateProfilePic.getMessage());
            if (updateProfilePic.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.photo_uploaded_successfully, getContext()))) {
                Toast.makeText(getContext(), getString(R.string.photo_uploaded_successfully), Toast.LENGTH_SHORT).show();
                ((HomeActivityNew) getActivity()).getUSerDetails();
            } else {
                Toast.makeText(getContext(), updateProfilePic.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (classType.equalsIgnoreCase(ItemsList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Get Item list");
            Gson gson = new Gson();
            mItemsLists = new ArrayList<>();
            ItemsList itemsLists = gson.fromJson(urlConnectionResponse.resultData, ItemsList.class);
            mItemsLists = itemsLists.getResultData();
            mItemsNameList = new ArrayList<>();

            for (ItemsList.ResultData resultData : mItemsLists) {
                mItemsNameList.add(resultData.getName());
                FWLogger.logInfo(TAG, resultData.getName());
            }
        } else if (classType.equalsIgnoreCase(IssueItem.class.getSimpleName())) {
            Gson gson = new Gson();
            IssueItem issueItem = gson.fromJson(urlConnectionResponse.resultData, IssueItem.class);
            if (issueItem.getCode().equalsIgnoreCase("200") && issueItem.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.item_assigned_successfully, getContext())))
                Toast.makeText(getContext(), getString(R.string.item_assigned_successfully), Toast.LENGTH_SHORT).show();
            else Toast.makeText(getContext(), issueItem.getMessage(), Toast.LENGTH_SHORT).show();
            FWLogger.logInfo(TAG, "Issue Items to  = " + issueItem.getResultData().getUserId());
        } else if (classType.equalsIgnoreCase(AuthMobileEmail.class.getSimpleName())) {
            Gson gson = new Gson();
            AuthMobileEmail authMobileEmail = gson.fromJson(urlConnectionResponse.resultData, AuthMobileEmail.class);
            if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Email id already registered.")) {
                Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
            } else if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Mobile number already registered.")) {
                Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                // FOR AUTOFILL OTP
                if (strFrom.equalsIgnoreCase("MOBILE")) {
                    startSmartUserConsent();
                    mOTP = authMobileEmail.getResultData().getOTP();
                    //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                } else {
                    mOTP = authMobileEmail.getResultData().getOTP();
                    //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(getActivity(), "AUTH OTP : " + mOTP, Toast.LENGTH_SHORT).show();
                EnableDisableBtn();
                //Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // START : AUTO READ/SET OTP
    private void startSmartUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        client.startSmsUserConsent(null);
    }

    private void getOtpFromMessage(String message) {

        Pattern otpPattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = otpPattern.matcher(message);
        if (matcher.find()) {
            //mOtpView.setText(matcher.group(0));
            mOTP = Integer.parseInt(matcher.group(0));
            //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            registerBroadcastReceiver();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(smsBroadcastReceiver);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener = new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
            @Override
            public void onSuccess(Intent intent) {
                startActivityForResult(intent, REQ_USER_CONSENT);
            }

            @Override
            public void onFailure() {
            }
        };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        getActivity().registerReceiver(smsBroadcastReceiver, intentFilter);
    }
    // END

    private void EnableDisableBtn() {
        try {
            //if (mOTP != 0) {
            textView_submit.setEnabled(true);
            textView_submit.setBackgroundResource(R.drawable.rounded_button);
            textView_validate.setEnabled(false);
            textView_validate.setBackgroundResource(R.drawable.rounded_button_gray);
            //}
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (SharedPrefManager.getInstance(getActivity()).getUserGroup() != null)
            HomeActivityNew.isOwner = SharedPrefManager.getInstance(getActivity()).getUserGroup().equalsIgnoreCase("Owner");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_assign_item:
                if (mUserDetails.getIsRegistered()) {
                    if (mItemsNameList == null || mItemsNameList.size() == 0) {
                        Toast toast = Toast.makeText(getActivity(), R.string.please_add_item_first, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        ItemDialog itemDialog = ItemDialog.getInstance();
                        itemDialog.issueItemTechDialog(getActivity(), this, mUserID, mItemsNameList, mTechFullName);
                    }
                } else
                    Toast.makeText(getActivity(), getString(R.string.can_not_assign_item), Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FWLogger.logInfo(TAG, "requestCode : " + requestCode + "  resultCode : " + resultCode);
        if (requestCode == BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

            FWLogger.logInfo(TAG, "Camera");
            Uri selectedImage = mImageUri;

            getActivity().getContentResolver().notifyChange(selectedImage, null);
            ContentResolver cr = getActivity().getContentResolver();
            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(cr, selectedImage);
                //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                String filePath = CameraUtils.getFilePathFromURI(selectedImage, getContext());
                //bitmap = CameraUtils.getCorrectBitmap(bitmap, filePath);
                bitmap = CommonFunction.ImgCompressUpto200KB(filePath, bitmap);
                if (isProfileImage) {
                    mImageView.setImageBitmap(bitmap);
                } else {
                    mImageViewCompLogo.setImageBitmap(bitmap);
                    mTextViewLogoHint.setVisibility(View.GONE);
                    mTextViewCompLogoClose.setVisibility(View.VISIBLE);
                }
                ConvertBitmapToString(bitmap);

            } catch (Exception e) {
                if (isProfileImage) {
                    mImageView.setImageResource(R.drawable.profile_icon);
                } else {
                    mImageEncodeBaseStringProfile = null;
                    mTextViewCompLogoClose.setVisibility(View.INVISIBLE);
                    mTextViewLogoHint.setVisibility(View.VISIBLE);
                    mImageViewCompLogo.setImageBitmap(null);
                }
                Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT).show();
                FWLogger.logInfo("Camera", e.getMessage());
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            if (resultCode == RESULT_OK) {
                FWLogger.logInfo(TAG, "Gallery");
                Uri selectedImage = data.getData();
                try {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();

                    Bitmap bitmap = CameraUtils.optimizeBitmap(2, picturePath);
                    //bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
                    String chooseFilePath = CameraUtils.getFilePathFromURI(selectedImage, getContext());
                    // bitmap = CameraUtils.getCorrectBitmap(bitmap, chooseFilePath);
                    bitmap = CommonFunction.ImgCompressUpto200KB(chooseFilePath, bitmap);

                    if (isProfileImage) {
                        mImageView.setImageBitmap(bitmap);
                    } else {
                        mImageViewCompLogo.setImageBitmap(bitmap);
                        mTextViewLogoHint.setVisibility(View.INVISIBLE);
                        mTextViewCompLogoClose.setVisibility(View.VISIBLE);
                    }
                    ConvertBitmapToString(bitmap);

                } catch (Exception e) {
                    if (isProfileImage) mImageView.setImageResource(R.drawable.profile_icon);
                    e.printStackTrace();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                if (isProfileImage) mImageView.setImageResource(R.drawable.profile_icon);
                Toast.makeText(getActivity(), "Sorry! Failed", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                FWLogger.logInfo(TAG, "Place : " + place.getName() + ", ID : " + place.getId() + ",Address : " + place.getAddress() + ", longitude : " + place.getLatLng().longitude + ", latitude : " + place.getLatLng().latitude);
                if (place != null) setAddress(place);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                if (isAddress) {
                    mEditTextAddress.setError(status.getStatusMessage());
                    mEditTextAddress.setText("");
                } else {
                    mEditTextCompanyAddress.setError(status.getStatusMessage());
                    mEditTextCompanyAddress.setText("");
                }

                FWLogger.logInfo(TAG, status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
                if (isAddress) {
                    mEditTextAddress.setError(null);
                    mEditTextAddress.setText("");
                } else {
                    mEditTextCompanyAddress.setError(null);
                    mEditTextCompanyAddress.setText("");
                }
                FWLogger.logInfo(TAG, "Request has cancelled");
            }
            // FOR AUTO FILL OTP
            else if (requestCode == REQ_USER_CONSENT) {
                if ((resultCode == RESULT_OK) && (data != null)) {
                    String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                    getOtpFromMessage(message);
                }
            }
        }
    }

    public void ConvertBitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        if (isProfileImage)
            mImageEncodeBaseStringProfile = Base64.encodeToString(b, Base64.DEFAULT);
        else mImageEncodeBaseStringCompany = Base64.encodeToString(b, Base64.DEFAULT);
    }

    private void PickImage() {
        final CharSequence[] options = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_option);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_photo))) {

                    if (!CameraUtils.isDeviceSupportCamera(getActivity().getApplicationContext())) {
                        Toast.makeText(getActivity(), R.string.device_doesnt_supprt_camera, Toast.LENGTH_LONG).show();
                    }
                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        captureImage();
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(ProfileFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                        captureImage();
                    }
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {

                    if (((HomeActivityNew) getActivity()).checkAllPermissionEnabled()) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    } else if (((HomeActivityNew) getActivity()).requestToEnableAllPermission(ProfileFragmentNew.this, BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    }
                    dialog.dismiss();

                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void captureImage() {
        FWLogger.logInfo(TAG, "captureImage: ");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //File photo = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_profile.jpg");
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File photo = null;
        try {
            photo = File.createTempFile(System.currentTimeMillis() + "_profile", ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Use File Provide as Android N Uri.fromFile() is deprecated
        mImageUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, BaseActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void setAddress(Place place) {
        FWLogger.logInfo(TAG, "setAddress");
        if (place != null) {
            if (isAddress) {
                mEditTextAddress.setError(null);
                mEditTextAddress.setText(place.getAddress());
            } else {
                mEditTextCompanyAddress.setError(null);
                mEditTextCompanyAddress.setText(place.getAddress());
            }

            mTempLat = place.getLatLng().latitude;
            mTempLong = place.getLatLng().longitude;
            mTempLocName = place.getName();

            isAddress = false;
            isCompAddress = false;
        }
    }

    @Override
    public void onRequestPermissionsUpdate(boolean isPermissionGranted, String permissionRequestedFeature) {
        if (isPermissionGranted) {
            if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_CAMERA)) {
                captureImage();
            } else if (permissionRequestedFeature.equalsIgnoreCase(BaseActivity.PERMISSION_REQUEST_ACCESS_GALLERY)) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
            }
        }
    }

    private boolean checkCameraPermission() {
        boolean cameraFlag = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                final int checkAudioStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_AUDIO);
                final int checkImageStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_IMAGES);
                final int checkVideoStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_MEDIA_VIDEO);
                if (checkAudioStorage != PackageManager.PERMISSION_GRANTED && checkImageStorage != PackageManager.PERMISSION_GRANTED && checkVideoStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
            //
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int checkCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
                final int checkStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (checkCamera != PackageManager.PERMISSION_GRANTED && checkStorage != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,}, ProfileFragmentNew.REQUEST_CODE_PERMISSIONS);
                } else {
                    cameraFlag = true;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        return cameraFlag;
    }

    private static void setDividerColor(NumberPicker picker) {
        if (picker == null) return;

        final int count = picker.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    picker.setTextColor(picker.getSolidColor());
                } else {
                    Field dividerField = picker.getClass().getDeclaredField("mSelectionDivider");
                    dividerField.setAccessible(true);
                    ColorDrawable colorDrawable = new ColorDrawable(picker.getResources().getColor(R.color.colorPrimary));
                    dividerField.set(picker, colorDrawable);
                    picker.invalidate();
                }
            } catch (Exception e) {
                // Log.w("setDividerColor", e);
            }
        }
    }

    private void getEmailOTPVerified(String strEmail) {
       /* mVerifyEmailOTP_ForProfileAsyncTask = new VerifyEmailOTP_ForProfileAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mVerifyEmailOTP_ForProfileAsyncTask.execute(strEmail, SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<AuthMobileEmail> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getEmailOTPVerified(strEmail, SharedPrefManager.getInstance(getContext()).getUserId());
                call.enqueue(new retrofit2.Callback<AuthMobileEmail>() {
                    @Override
                    public void onResponse(Call<AuthMobileEmail> call, Response<AuthMobileEmail> response) {
                        try {
                            if (response.code() == 200) {
                                AuthMobileEmail authMobileEmail = response.body();
                                if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Email id already registered.")) {
                                    Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                } else if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Mobile number already registered.")) {
                                    Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    // FOR AUTOFILL OTP
                                    if (strFrom.equalsIgnoreCase("MOBILE")) {
                                        startSmartUserConsent();
                                        mOTP = authMobileEmail.getResultData().getOTP();
                                        //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                                    } else {
                                        mOTP = authMobileEmail.getResultData().getOTP();
                                        //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                                    }
                                    //Toast.makeText(getActivity(), "AUTH OTP : " + mOTP, Toast.LENGTH_SHORT).show();
                                    EnableDisableBtn();
                                    //Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthMobileEmail> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in IsTechEmailIdExistForUpdateProfile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in IsTechEmailIdExistForUpdateProfile API:");
            ex.getMessage();
        }
    }

    private void getMobileOTPVerified(String strMobile) {
        /*mVerifyMobileOTP_ForProfileAsyncTask = new VerifyMobileOTP_ForProfileAsyncTask(getContext(), BaseAsyncTask.Priority.LOW, this);
        mVerifyMobileOTP_ForProfileAsyncTask.execute(strMobile, SharedPrefManager.getInstance(getContext()).getUserId());*/

        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                Call<AuthMobileEmail> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getMobileOTPVerified(strMobile, SharedPrefManager.getInstance(getContext()).getUserId());
                call.enqueue(new retrofit2.Callback<AuthMobileEmail>() {
                    @Override
                    public void onResponse(Call<AuthMobileEmail> call, Response<AuthMobileEmail> response) {
                        try {
                            if (response.code() == 200) {
                                AuthMobileEmail authMobileEmail = response.body();
                                if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Email id already registered.")) {
                                    Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                } else if (authMobileEmail.getCode().equalsIgnoreCase("200") && authMobileEmail.getMessage().equalsIgnoreCase("Mobile number already registered.")) {
                                    Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    // FOR AUTOFILL OTP
                                    if (strFrom.equalsIgnoreCase("MOBILE")) {
                                        startSmartUserConsent();
                                        mOTP = authMobileEmail.getResultData().getOTP();
                                        //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                                    } else {
                                        mOTP = authMobileEmail.getResultData().getOTP();
                                        //Toast.makeText(getActivity(), "OTP IS::" + mOTP, Toast.LENGTH_SHORT).show();
                                    }
                                    //Toast.makeText(getActivity(), "AUTH OTP : " + mOTP, Toast.LENGTH_SHORT).show();
                                    EnableDisableBtn();
                                    //Toast.makeText(getActivity(), authMobileEmail.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthMobileEmail> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in IsMobileNoExistForUpdateProfile API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in IsMobileNoExistForUpdateProfile API:");
            ex.getMessage();
        }

    }

    public void VerifyOTPDialog(Activity activity, String strFrom) {
        FWDialog dialog = new FWDialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.otp_verified);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        TextInputLayout txtMobileNo_VerifyLayout, txtEmailId_VerifyLayout;
        EditText editText_Verifycontact, editText_Verifyemail;
        OtpView profileotp_view;
        ImageView closeAuthProfile;
        TextView txtContctMsg, txtEmailMsg;

        editText_Verifycontact = dialog.findViewById(R.id.editText_Verifycontact);
        editText_Verifyemail = dialog.findViewById(R.id.editText_Verifyemail);
        txtMobileNo_VerifyLayout = dialog.findViewById(R.id.txtMobileNo_VerifyLayout);
        txtEmailId_VerifyLayout = dialog.findViewById(R.id.txtEmailId_VerifyLayout);
        profileotp_view = dialog.findViewById(R.id.profileotp_view);
        textView_validate = dialog.findViewById(R.id.textView_validate);
        textView_submit = dialog.findViewById(R.id.textView_submit);
        closeAuthProfile = dialog.findViewById(R.id.closeAuthProfile);
        txtContctMsg = dialog.findViewById(R.id.txtContctMsg);
        txtEmailMsg = dialog.findViewById(R.id.txtEmailMsg);

        if (strFrom.equalsIgnoreCase("MOBILE")) {
            txtMobileNo_VerifyLayout.setVisibility(View.VISIBLE);
            txtContctMsg.setVisibility(View.VISIBLE);
            txtEmailMsg.setVisibility(View.GONE);

        } else if (strFrom.equalsIgnoreCase("EMAIL")) {
            txtEmailId_VerifyLayout.setVisibility(View.VISIBLE);
            txtEmailMsg.setVisibility(View.VISIBLE);
            txtContctMsg.setVisibility(View.GONE);
        }

        editText_Verifycontact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.length() == 10) {
                        profileotp_view.setVisibility(View.VISIBLE);
                    } else {
                        profileotp_view.setVisibility(View.GONE);
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_Verifyemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.toString().contains("@")) {
                        profileotp_view.setVisibility(View.VISIBLE);
                    } else {
                        profileotp_view.setVisibility(View.GONE);
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        closeAuthProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        textView_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(getActivity(), v);

                if (strFrom.equalsIgnoreCase("MOBILE")) {
                    if (editText_Verifycontact.getText().toString().isEmpty() || CommonFunction.isValidPhone(editText_Verifycontact)) {
                        editText_Verifycontact.setError(getString(R.string.please_enter_valid_number));
                    } else {
                        if (mEditTextContact.getText().toString().equalsIgnoreCase(editText_Verifycontact.getText().toString())) {
                            editText_Verifycontact.setError(getString(R.string.duplicate_mobile_no));
                        } else {
                            getMobileOTPVerified(editText_Verifycontact.getText().toString());
                        }
                    }
                } else if (strFrom.equalsIgnoreCase("EMAIL")) {
                    if (/*editText_Verifyemail.getText().toString() != null && */editText_Verifyemail.getText().toString().isEmpty() && Patterns.EMAIL_ADDRESS.matcher(mEditTextEmail.getText().toString()).matches() == false) {
                        editText_Verifyemail.setError(getString(R.string.please_enter_valid_email_id));
                    } else {
                        if (mEditTextEmail.getText().toString().equalsIgnoreCase(editText_Verifyemail.getText().toString())) {
                            editText_Verifyemail.setError((getString(R.string.duplicate_emailid)));
                        } else {
                            getEmailOTPVerified(editText_Verifyemail.getText().toString());
                        }
                    }
                }

            }
        });

        textView_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    hideKeyboard(getActivity(), v);
                    if (strFrom.equalsIgnoreCase("MOBILE")) {
                        if (mOTP == Integer.parseInt(profileotp_view.getText().toString())) {
                            Toast.makeText(getActivity(), R.string.otp_verified, Toast.LENGTH_SHORT).show();
                            mEditTextContact.setText(editText_Verifycontact.getText().toString());
                            IsMobileNoUpdate = true;
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), R.string.invalid_otp, Toast.LENGTH_SHORT).show();
                        }
                    } else if (strFrom.equalsIgnoreCase("EMAIL")) {
                        if (mOTP == Integer.parseInt(profileotp_view.getText().toString())) {
                            Toast.makeText(getActivity(), R.string.otp_verified, Toast.LENGTH_SHORT).show();
                            mEditTextEmail.setText(editText_Verifyemail.getText().toString());
                            IsEmailIdUpdate = true;
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), R.string.invalid_otp, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });
        dialog.show();
    }

    public void hideKeyboard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");

        HomeActivityNew.isOwner = true;
        super.onDestroyView();
    }
}
