package com.corefield.fieldweb.FieldWeb.Notification;

/**
 * //
 * Created by CFS on 7/29/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Notification
 * Version : 1.2.1
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public interface NewNotificationListener {
    void onNewNotification();
}
