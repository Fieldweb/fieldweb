package com.corefield.fieldweb.FieldWeb.Task;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.DeviceListViewAdapterNew;
import com.corefield.fieldweb.Adapter.NotesViewAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.UpdateTaskClosure;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CameraUtils;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.RoundedTransformation;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * //
 * Created by CFS on 10/5/2021.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.FieldWeb.Task
 * Version : 2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This fragment is used to show the task closure details
 * //
 **/
public class SummaryDetailsFragment extends Fragment implements View.OnClickListener, OnTaskCompleteListener {

    //Variables
    private String TAG = SummaryDetailsFragment.class.getSimpleName();

    //View's
    private View mRootView;
    private Button buttonSubmit;
    private ImageView mImageViewSignature, mImageViewFieldPhoto, mImageViewFieldPhoto2, mImageViewFieldPhoto3, mImageViewPhotoBeforeTask, mImageViewPhotoBeforeTask2, mImageViewPhotoBeforeTask3;
    private RecyclerView mRecyclerViewNote, mRecyclerViewDeviceList;
    private TextView mTextViewSignedBy, mTextViewSignedMobileNo, mTextViewRating, mTextViewWorkMode;
    private RatingBar mRatingBar;

    private NotesViewAdapter mNotesViewAdapter;
    private DeviceListViewAdapterNew mDeviceListViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<TaskClosure.TechnicalNotedto> mTechnicalNoteDTOs = new ArrayList<>();
    private List<TaskClosure.DeviceInfoList> mDeviceInfoLists = new ArrayList<>();
    private UpdateTaskClosure mUpdateTaskClosure;

    //Task Closure DTO
    private TaskClosure.ResultData mTaskClosureResultData = new TaskClosure.ResultData();
    private TasksList.ResultData mResultData;

    private String mFieldImageURL = "", mFieldImageURL2 = "", mFieldImageURL3 = "", mSignatureImage = "", mImageBeforeTaskURL = "", mImageBeforeTaskURL2 = "", mImageBeforeTaskURL3 = "";
    private LinearLayout mLinearNotesList, mLinearDeviceList;
    private int mTaskId;
    Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_summary_details, container, false);

        ((HomeActivityNew) getActivity()).mToolbar.setTitle(R.string.summary_details);
        ((HomeActivityNew) getActivity()).hideHeader();
        activity = ((HomeActivityNew) getActivity());


        Bundle getBundle = this.getArguments();
        if (getBundle != null)
            mTaskClosureResultData = (TaskClosure.ResultData) getBundle.getSerializable("taskClosureResult");

        if (getBundle != null)
            mResultData = (TasksList.ResultData) getBundle.getSerializable("taskResult");

        mTaskId = mTaskClosureResultData.getTaskId();
        inIt();
        manageBackPress();
        return mRootView;
    }

    private void inIt() {
        mImageViewFieldPhoto = mRootView.findViewById(R.id.imageView_field_photo);
        mImageViewFieldPhoto2 = mRootView.findViewById(R.id.imageView_field_photo2);
        mImageViewFieldPhoto3 = mRootView.findViewById(R.id.imageView_field_photo3);
        mImageViewPhotoBeforeTask = mRootView.findViewById(R.id.imageView_before_task);
        mImageViewPhotoBeforeTask2 = mRootView.findViewById(R.id.imageview_before_task2);
        mImageViewPhotoBeforeTask3 = mRootView.findViewById(R.id.imageview_before_task3);
        mTextViewWorkMode = mRootView.findViewById(R.id.textView_work_mode);
        mRecyclerViewNote = mRootView.findViewById(R.id.recyclerView_note_view);
        mRecyclerViewDeviceList = mRootView.findViewById(R.id.recyclerView_device_view);
        mImageViewSignature = mRootView.findViewById(R.id.customer_signature);
        mTextViewSignedBy = mRootView.findViewById(R.id.textView_signed_by);
        mTextViewSignedMobileNo = mRootView.findViewById(R.id.textview_signed_mobile_no);
        mRatingBar = mRootView.findViewById(R.id.ratingbar_task_closure_view);
        mTextViewRating = mRootView.findViewById(R.id.textView_rating);
        buttonSubmit = mRootView.findViewById(R.id.button_submit_closure);

        mLinearNotesList = mRootView.findViewById(R.id.linear_notes_list);
        mLinearDeviceList = mRootView.findViewById(R.id.linear_device_list);

        mLinearNotesList.setVisibility(View.GONE);
        mLinearDeviceList.setVisibility(View.GONE);

        buttonSubmit.setOnClickListener(this);

        setTaskClosureToView();
    }

    private void setTaskClosureToView() {
        // After Get Response
        mTextViewWorkMode.setText(mTaskClosureResultData.getWorkModeType());
        mRatingBar.setRating((float) mTaskClosureResultData.getRatingBar());
        mTextViewRating.setText("" + (float) mRatingBar.getRating());
        mRatingBar.setFocusable(false);
        mRatingBar.setIsIndicator(true);
        mTextViewSignedBy.setText(mTaskClosureResultData.getSignedBy());
        mFieldImageURL = mTaskClosureResultData.getFieldPhoto();
        mFieldImageURL2 = mTaskClosureResultData.getFieldPhoto1();
        mFieldImageURL3 = mTaskClosureResultData.getFieldPhoto2();

        if (mResultData.getPreDeviceInfoDto() != null) {
            if (mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath() != null && !mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath().isEmpty()) {
                mImageBeforeTaskURL = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath();
                mImageBeforeTaskURL2 = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath1();
                mImageBeforeTaskURL3 = mResultData.getPreDeviceInfoDto().getDeviceInfoImagePath2();
            }
        }
        if (mTaskClosureResultData.getMobileNo() == 0)
            mTextViewSignedMobileNo.setText(getResources().getString(R.string.na));
        else
            mTextViewSignedMobileNo.setText("" + mTaskClosureResultData.getMobileNo());

        //if (mImageBeforeTaskURL != null && !mImageBeforeTaskURL.isEmpty() && mImageBeforeTaskURL2 != null && !mImageBeforeTaskURL2.isEmpty() && mImageBeforeTaskURL3 != null && !mImageBeforeTaskURL3.isEmpty()) {
        setPhotoBeforeTask();
        setPhotoBeforeTask2();
        setPhotoBeforeTask3();
      /*  } else {
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
            mImageViewPhotoBeforeTask.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_no_image));
            mImageViewPhotoBeforeTask2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_no_image));
            mImageViewPhotoBeforeTask3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_no_image));
//            mImageViewPhotoBeforeTask.setScaleType(ImageView.ScaleType.FIT_XY);
//            mImageViewPhotoBeforeTask.setLayoutParams(layoutParams);
        }*/

        // SET AFTER FIELD PHOTO
        setFieldPhoto();
        setFieldPhoto2();
        setFieldPhoto3();

        setSignatureImage();

        //Notes View
        if (mTaskClosureResultData.getTechnicalNotedto() != null) {
            mTechnicalNoteDTOs = mTaskClosureResultData.getTechnicalNotedto();
            if (mTechnicalNoteDTOs.size() > 0) {
                mLinearNotesList.setVisibility(View.VISIBLE);
                mNotesViewAdapter = new NotesViewAdapter(mTechnicalNoteDTOs, getContext());
                mLayoutManager = new LinearLayoutManager(getContext());
                mRecyclerViewNote.setLayoutManager(mLayoutManager);
                mRecyclerViewNote.setAdapter(mNotesViewAdapter);
            }
        }

        // Device List View
        if (mTaskClosureResultData.getDeviceInfoList() != null) {
            mDeviceInfoLists = mTaskClosureResultData.getDeviceInfoList();
            if (mDeviceInfoLists.size() > 0) {
                mLinearDeviceList.setVisibility(View.VISIBLE);
                mDeviceListViewAdapter = new DeviceListViewAdapterNew(mDeviceInfoLists, getActivity());
                mLayoutManager = new LinearLayoutManager(getContext());
                mRecyclerViewDeviceList.setLayoutManager(mLayoutManager);
                mRecyclerViewDeviceList.setAdapter(mDeviceListViewAdapter);
            }
        }
    }

    /*@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        if(mTaskClosureResultData != null)
            savedInstanceState.putSerializable("taskResult", mTaskClosureResultData);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        if(savedInstanceState != null)
            savedInstanceState.getSerializable("taskResult");
    }*/

    private void setSignatureImage() {
        try {
            mSignatureImage = mTaskClosureResultData.getCustomerSignatureImage();
            Bitmap bitmap = ImageUtils.ConvertStringToBitmap(mSignatureImage);
            bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 300/*500*/);
            Bitmap roundedBitmap = ImageUtils.getRoundedCornerBitmap(bitmap, 30);
            mImageViewSignature.setImageBitmap(roundedBitmap);
            mImageViewSignature.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void setFieldPhoto() {
        try {
            Bitmap bitmap = ImageUtils.ConvertStringToBitmap(mFieldImageURL);
            bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 300/*500*/);
            Bitmap roundedBitmap = ImageUtils.getRoundedCornerBitmap(bitmap, 50);
            mImageViewFieldPhoto.setImageBitmap(roundedBitmap);
            mImageViewFieldPhoto.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void setFieldPhoto2() {
        try {
            Bitmap bitmap = ImageUtils.ConvertStringToBitmap(mFieldImageURL2);
            bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 300/*500*/);
            Bitmap roundedBitmap = ImageUtils.getRoundedCornerBitmap(bitmap, 50);
            mImageViewFieldPhoto2.setImageBitmap(roundedBitmap);
            mImageViewFieldPhoto2.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void setFieldPhoto3() {
        try {
            Bitmap bitmap = ImageUtils.ConvertStringToBitmap(mFieldImageURL3);
            bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 300/*500*/);
            Bitmap roundedBitmap = ImageUtils.getRoundedCornerBitmap(bitmap, 50);
            mImageViewFieldPhoto3.setImageBitmap(roundedBitmap);
            mImageViewFieldPhoto3.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    /*private void setPhotoBeforeTask() {
        Bitmap bitmap = ImageUtils.ConvertStringToBitmap(mImageBeforeTaskURL);
        bitmap = CameraUtils.getResizedBitmapLessThan500KB(bitmap, 500);
        Bitmap roundedBitmap = ImageUtils.getRoundedCornerBitmap(bitmap, 30);
        mImageViewPhotoBeforeTask.setImageBitmap(roundedBitmap);
        mImageViewPhotoBeforeTask.setScaleType(ImageView.ScaleType.FIT_XY);
    }*/

    private void setPhotoBeforeTask() {
        try {
            Picasso.get()
                    .load(mImageBeforeTaskURL)
                    .placeholder(R.drawable.field_location_upload_photo)
                    .error(R.drawable.field_location_upload_photo)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(mImageViewPhotoBeforeTask, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewPhotoBeforeTask.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void setPhotoBeforeTask2() {
        try {
            Picasso.get()
                    .load(mImageBeforeTaskURL2)
                    .placeholder(R.drawable.field_location_upload_photo)
                    .error(R.drawable.field_location_upload_photo)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(mImageViewPhotoBeforeTask2, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewPhotoBeforeTask2.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void setPhotoBeforeTask3() {
        try {
            Picasso.get()
                    .load(mImageBeforeTaskURL3)
                    .placeholder(R.drawable.field_location_upload_photo)
                    .error(R.drawable.field_location_upload_photo)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(mImageViewPhotoBeforeTask3, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            mImageViewPhotoBeforeTask3.setImageResource(R.drawable.field_location_upload_photo);
                        }
                    });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_submit_closure:
                /*if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID)
                    navigateToPaymentScreen();
                else*/
                updateTask();
                break;
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (urlConnectionResponse != null) {
            if (classType.equalsIgnoreCase(TaskClosure.class.getSimpleName())) {
                Gson gson = new Gson();
                TaskClosure taskClosure = gson.fromJson(urlConnectionResponse.resultData, TaskClosure.class);
                mTaskClosureResultData = taskClosure.getResultData();
                if (taskClosure != null) {
                    if (taskClosure.getCode().equalsIgnoreCase("200") && taskClosure.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, getContext()))) {
                        //NOTE: Log GA event
                        Bundle bundle = new Bundle();
                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.TASK_CLOSURE);
                        bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Completed.name());
                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TASK_CLOSURE, bundle);
                        Toast.makeText(getContext(), getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                        if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID)
                            navigateToPaymentScreen();
                        else
                            ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    } else {
                        Toast.makeText(getContext(), taskClosure.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    private void navigateToPaymentScreen() {
        TechPaymentReceivedFragmentNew techPaymentReceivedFragment = new TechPaymentReceivedFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskResult", mResultData);
        techPaymentReceivedFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, techPaymentReceivedFragment);
        transaction.isAddToBackStackAllowed();
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void updateTask() {
        mUpdateTaskClosure = new UpdateTaskClosure(getContext(), BaseAsyncTask.Priority.LOW, this);
        mUpdateTaskClosure.execute(mTaskClosureResultData);

/*
        try {
            if (Connectivity.isNetworkAvailableRetro(activity)) {
                Call<TaskClosure> call = RetrofitClient.getInstance(getContext()).getMyApi().updateTaskClosure(mTaskClosureResultData,
                        "Bearer " + SharedPrefManager.getInstance(getContext()).getUserToken());
                call.enqueue(new retrofit2.Callback<TaskClosure>() {
                    @Override
                    public void onResponse(Call<TaskClosure> call, Response<TaskClosure> response) {
                        try {
                            if (response.code() == 200) {
                                TaskClosure taskClosure = response.body();
                                mTaskClosureResultData = taskClosure.getResultData();
                                if (taskClosure != null) {
                                    if (taskClosure.getCode().equalsIgnoreCase("200") && taskClosure.getMessage().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.task_updated_successfully, getContext()))) {
                                        //NOTE: Log GA event
                                        Bundle bundle = new Bundle();
                                        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
                                        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_ID, mTaskId);
                                        bundle.putInt(FirebaseGoogleAnalytics.Param.TASK_STATE, TaskState.TASK_CLOSURE);
                                        bundle.putString(FirebaseGoogleAnalytics.Param.TASK_STATUS, Constant.TaskCategories.Completed.name());
                                        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
                                        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
                                        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
                                        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
                                        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TASK_CLOSURE, bundle);
                                        Toast.makeText(getContext(), getString(R.string.task_updated_successfully), Toast.LENGTH_LONG).show();
                                        if (mResultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && mResultData.getPaymentMode().equalsIgnoreCase(Constant.PaymentMode.RATE_STRING) && mResultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID)
                                            navigateToPaymentScreen();
                                        else
                                            ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                                    } else {
                                        Toast.makeText(getContext(), taskClosure.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<TaskClosure> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in AddTaskClosureDetails API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(activity);
            }

        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in AddTaskClosureDetails API:");
            ex.getMessage();
        }
*/

    }

    private void manageBackPress() {
        mRootView.setFocusableInTouchMode(true);
        mRootView.requestFocus();
        mRootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FWLogger.logInfo(TAG, "setOnKeyListener()");
                    ((HomeActivityNew) getActivity()).navigateToTab(R.id.navigation_task);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivityNew) getActivity()).showHeader();
    }

    @Override
    public void onDestroyView() {
        if (mUpdateTaskClosure != null) {
            mUpdateTaskClosure.cancel(true);
            mUpdateTaskClosure = null;
        }
        super.onDestroyView();
    }
}

