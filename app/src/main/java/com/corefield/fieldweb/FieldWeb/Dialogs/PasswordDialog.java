package com.corefield.fieldweb.FieldWeb.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordDialog {
    public static String mErrorMessage = "";
    public static Pattern mPattern;
    public static Matcher mMatcher;
    public static boolean mResult = false;
    public static String mCheckCharPattern = "";

    private static PasswordDialog mPasswordDialog;

    /**
     * The private constructor for the PasswordDialog Singleton class
     */
    private PasswordDialog() {
    }

    public static PasswordDialog getInstance() {
        //instantiate a new PasswordDialog if we didn't instantiate one yet
        if (mPasswordDialog == null) {
            mPasswordDialog = new PasswordDialog();
        }
        return mPasswordDialog;
    }


    public boolean isValidatePassword(final String password, Activity activity) {
        if (TextUtils.isEmpty(password)) {
            mErrorMessage = activity.getResources().getString(R.string.please_enter_your_password);
        } else {
            mCheckCharPattern = "(?=.*[a-z]).*";
            mPattern = Pattern.compile(mCheckCharPattern);
            mMatcher = mPattern.matcher(password);
            mResult = mMatcher.matches();
            if (!mResult) {
                mErrorMessage = activity.getString(R.string.pass_should_contain_one_lowercase_letter);
            } else {
                mCheckCharPattern = "(?=.*[A-Z]).*";
                mPattern = Pattern.compile(mCheckCharPattern);
                mMatcher = mPattern.matcher(password);
                mResult = mMatcher.matches();
                if (!mResult) {
                    mErrorMessage = activity.getString(R.string.pass_should_contain_one_uppercase_letter);
                } else {
                    mCheckCharPattern = "(?=.*[0-9]).*";
                    mPattern = Pattern.compile(mCheckCharPattern);
                    mMatcher = mPattern.matcher(password);
                    mResult = mMatcher.matches();
                    if (!mResult) {
                        mErrorMessage = activity.getString(R.string.pass_should_contain_one_numeral_value);
                    } else {
                        mCheckCharPattern = "^[a-zA-Z0-9&@#]*[&@#]+[a-zA-Z0-9&@#]*$";//"(?=.*[&@#_]).*";
                        mPattern = Pattern.compile(mCheckCharPattern);
                        mMatcher = mPattern.matcher(password);
                        mResult = mMatcher.matches();
                        if (!mResult) {
                            mErrorMessage = activity.getString(R.string.pass_should_contain_one_special_char);
                        } else {
                            mCheckCharPattern = "(?=.*[a-z0-9])(?=.*[A-Z0-9])(?=.*[&@#_])(?=\\S+$).{6,10}$";
                            mPattern = Pattern.compile(mCheckCharPattern);
                            mMatcher = mPattern.matcher(password);
                            mResult = mMatcher.matches();
                            if (!mResult) {
                                mErrorMessage = activity.getString(R.string.password_more_than_6_less_than_10_chars);
                            }
                        }
                    }
                }
            }
        }
        return mResult;
    }
}
