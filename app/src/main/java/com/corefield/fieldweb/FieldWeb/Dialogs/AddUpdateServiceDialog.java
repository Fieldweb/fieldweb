package com.corefield.fieldweb.FieldWeb.Dialogs;

import static com.corefield.fieldweb.FieldWeb.Dialogs.UserDialog.mTempPlace;
import static com.corefield.fieldweb.FieldWeb.HomeActivityNew.AUTOCOMPLETE_REQUEST_CODE;
import static com.corefield.fieldweb.Util.CommonFunction.isEmpty;
import static com.corefield.fieldweb.Util.CommonFunction.isValidPhone;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.AddQuoteItemsAdapter;
import com.corefield.fieldweb.Adapter.AddQuoteServiceAdapter;
import com.corefield.fieldweb.Adapter.CustomerAdapter;
import com.corefield.fieldweb.Adapter.QuotSubExtraItemAdapter;
import com.corefield.fieldweb.Adapter.QuotSubItemAdapter;
import com.corefield.fieldweb.Adapter.QuotSubServiceAdapter;
import com.corefield.fieldweb.Adapter.QuotationItemDetailsListAdapter;
import com.corefield.fieldweb.AsyncManager.BaseAsyncTask;
import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.SaveQuotationDTO;
import com.corefield.fieldweb.DTO.Account.TaxDetails;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.FieldWeb.Accounts.AccountsTabHost;
import com.corefield.fieldweb.FieldWeb.Accounts.QuotationDetailsFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ProfileFragmentNew;
import com.corefield.fieldweb.FieldWeb.BackgroundTask.AddHealthStatusAsyncTask;
import com.corefield.fieldweb.FieldWeb.HealthAndSafety.HealthAndSafetyActivity;
import com.corefield.fieldweb.FieldWeb.ServiceManagement.ServiceManagementFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CountdownTechFragmentNew;
import com.corefield.fieldweb.Listener.DialogUpdateListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

public class AddUpdateServiceDialog implements AddQuoteItemsAdapter.MultipleMaterialPostListener, AddQuoteServiceAdapter.MultipleServicePostListener {
    private static AddUpdateServiceDialog mItemDialog;

    private Date date1, date2;
    public ImageView mImageview1, mImageview2, mImageview3;
    int mCount = 0;
    private String mTempLocName = "";
    private String mWhichImage = "", mBaseStringFromService1 = "", mBaseStringFromService2 = "", mBaseStringFromService3 = "", imageFileName1 = "", imageFileName2 = "", imageFileName3 = "";
    private int customerDetailsId = 0;
    private String mCapturedImagePath = "";
    public static final int PICK_IMAGE_GALLERY = 2;
    public static final int REQUEST_CODE_PERMISSIONS = 0x1;
    private static String TAG = CountdownTechFragmentNew.class.getSimpleName();
    private Uri mImageUri;
    Context mContext;
    EditText editTextChooseFile;
    private String inputStreamPDF;
    public static final int DEFAULT_BUFFER_SIZE = 8192;
    //
    CustomerList.ResultData customerObj = null;
    double mTempLat = 0, mTempLong = 0;
    private LinearLayoutManager mLayoutManager;
    AddQuoteItemsAdapter addItemsAdapter;
    AddQuoteServiceAdapter addServiceAdapter;
    ArrayList<AddTask.MultipleItemAssigned> finalPostArray = new ArrayList<AddTask.MultipleItemAssigned>();
    ArrayList<EnquiryServiceTypeDTO.ResultData> finalServiceArry = new ArrayList<EnquiryServiceTypeDTO.ResultData>();
    QuotSubServiceAdapter quotSubServiceAdapter;
    QuotSubItemAdapter quotSubItemAdapter;
    QuotSubExtraItemAdapter quotSubExtraItemAdapter;
    List<SaveQuotationDTO.QuoteServiceList> multipleServiceArray = null;
    List<SaveQuotationDTO.QuoteItemList> multipleItemArray = null;


    int taxId;

    /**
     * The private constructor for the ItemDialog Singleton class
     */
    private AddUpdateServiceDialog() {
    }

    public static AddUpdateServiceDialog getInstance() {
        //instantiate a new ExpenditureDialog if we didn't instantiate one yet
        if (mItemDialog == null) {
            mItemDialog = new AddUpdateServiceDialog();
        }
        return mItemDialog;
    }

    public void addService(Context context, Activity mActivity) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_service);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        mImageview1 = dialog.findViewById(R.id.imageview_photo1);
        mImageview2 = dialog.findViewById(R.id.imageview_photo2);
        mImageview3 = dialog.findViewById(R.id.imageview_photo3);

        Button buttonAddService = dialog.findViewById(R.id.button_addservice);
        Button buttonCloseService = dialog.findViewById(R.id.button_cancel);
        ImageView closeButton = dialog.findViewById(R.id.closeDialog);
        EditText medittextServiceName = dialog.findViewById(R.id.edittext_service_name);
        //medittextServiceName.setFilters(new InputFilter[]{new CustomEditText.EmojiExcludeFilter()});
        EditText medittextServicePrice = dialog.findViewById(R.id.edittext_service_price);
        EditText medittextPhone = dialog.findViewById(R.id.edittext_phone);
        EditText medittextServiceDescription = dialog.findViewById(R.id.edittext_service_description);
        //medittextServiceDescription.setFilters(new InputFilter[]{new CustomEditText.EmojiExcludeFilter()});
        TextView txtLabel = dialog.findViewById(R.id.txtLabel);
        txtLabel.setText("Add Service");

        mContext = context;


        buttonCloseService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mImageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService1";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService2";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService3";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        buttonAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(medittextServiceName)) {
                    medittextServiceName.setError(context.getString(R.string.please_enter_name));
                } else if (isEmpty(medittextServicePrice)) {
                    medittextServicePrice.setError(context.getString(R.string.please_enter_rate));
                } else if (isEmpty(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(mContext).getCountryDetailsID() == 1 && isValidPhone(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(mContext).getCountryDetailsID() != 1 && CommonFunction.isValidPhoneNRI(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(medittextServiceDescription)) {
                    medittextServiceDescription.setError(context.getString(R.string.please_enter_notes));
                } else {
                    ServiceTypeListDTO.ResultData resultData = new ServiceTypeListDTO.ResultData();
                    resultData.setServiceName(medittextServiceName.getText().toString());
                    resultData.setPrice(Integer.parseInt(medittextServicePrice.getText().toString()));
                    resultData.setContactNo(medittextPhone.getText().toString());
                    resultData.setDescription(medittextServiceDescription.getText().toString());
                    resultData.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    resultData.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    addServiceTypeDetails(mActivity, resultData);
                    refresh(mContext);
                    dialog.dismiss();
                }
            }
        });
    }

    public void EditService(Context context, Activity mActivity, int serviceID, String serviceName, int price, String contactNo, String description) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_service);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        mImageview1 = dialog.findViewById(R.id.imageview_photo1);
        mImageview2 = dialog.findViewById(R.id.imageview_photo2);
        mImageview3 = dialog.findViewById(R.id.imageview_photo3);

        Button buttonAddService = dialog.findViewById(R.id.button_addservice);
        Button buttonCloseService = dialog.findViewById(R.id.button_cancel);
        ImageView closeButton = dialog.findViewById(R.id.closeDialog);
        EditText medittextServiceName = dialog.findViewById(R.id.edittext_service_name);
        EditText medittextServicePrice = dialog.findViewById(R.id.edittext_service_price);
        EditText medittextPhone = dialog.findViewById(R.id.edittext_phone);
        EditText medittextServiceDescription = dialog.findViewById(R.id.edittext_service_description);
        TextView txtLabel = dialog.findViewById(R.id.txtLabel);

        mContext = context;

        try {
            medittextServiceName.setText(serviceName);
            medittextServicePrice.setText(String.valueOf(price));
            medittextPhone.setText(contactNo);
            medittextServiceDescription.setText(description);
            txtLabel.setText("Edit Service");
            buttonAddService.setText("Update");
        } catch (Exception ex) {
            ex.getMessage();
        }

        buttonCloseService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mImageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService1";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService2";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });

        mImageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromWhere = "fromAddUpdateService3";
                ((HomeActivityNew) mContext).pickImage(fromWhere);

            }
        });
        buttonAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEmpty(medittextServiceName)) {
                    medittextServiceName.setError(context.getString(R.string.please_enter_name));
                } else if (isEmpty(medittextServicePrice)) {
                    medittextServicePrice.setError(context.getString(R.string.please_enter_rate));
                } else if (isEmpty(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(mContext).getCountryDetailsID() == 1 && isValidPhone(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (SharedPrefManager.getInstance(mContext).getCountryDetailsID() != 1 && CommonFunction.isValidPhoneNRI(medittextPhone)) {
                    medittextPhone.setError(context.getString(R.string.please_enter_valid_number));
                } else if (isEmpty(medittextServiceDescription)) {
                    medittextServiceDescription.setError(context.getString(R.string.please_enter_notes));
                } else {
                    ServiceTypeListDTO.ResultData resultData = new ServiceTypeListDTO.ResultData();
                    resultData.setId(serviceID);
                    resultData.setServiceName(medittextServiceName.getText().toString());
                    resultData.setIsActive(true);
                    resultData.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    resultData.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    resultData.setDescription(medittextServiceDescription.getText().toString());
                    resultData.setPrice(Integer.parseInt(medittextServicePrice.getText().toString()));
                    resultData.setContactNo(medittextPhone.getText().toString());

                    UpdateServiceTypeDetails(mActivity, resultData);
                    refresh(mContext);
                    dialog.dismiss();
                }
            }
        });
    }


    private void refresh(Context mContext) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        ServiceManagementFragment myFragment = new ServiceManagementFragment();
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
    }

    private void callAccountTabHost(Context mContext) {
        AppCompatActivity activity = (AppCompatActivity) mContext;
        /*int count = activity.getFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            FWLogger.logInfo(TAG, "inside for = " + i);
            activity.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }*/
        //
        AccountsTabHost myFragment = new AccountsTabHost();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment_container, myFragment);
        transaction.commit();
    }


   /* public void addQuote(Context context, Activity mActivity) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_quote);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        mContext = context;

        Button buttonCloseService = dialog.findViewById(R.id.button_cancel);
        ImageView closeButton = dialog.findViewById(R.id.closeAddQuote);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }*/

    public void addQuote(Context context, Activity activity, List<CustomerList.ResultData> customerList, List<EnquiryServiceTypeDTO.ResultData> serviceNameList, ArrayList<String> itemsNameList, List<TaxDetails.ResultData> mTaxDetailsArry) {
        //Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        FWDialog dialog = new FWDialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_quote);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        mContext = context;

        Button buttonCloseService = dialog.findViewById(R.id.button_cancel);
        Button buttonAddQuote = dialog.findViewById(R.id.buttonAddQuote);
        ImageView closeButton = dialog.findViewById(R.id.closeAddQuote);

        EditText editTextCreateDatePicker = dialog.findViewById(R.id.edittext_createdate_picker);
        EditText edittextQuoteName = dialog.findViewById(R.id.edittext_quote_name);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        EditText editTextValidityDate = dialog.findViewById(R.id.edittext_validitydate_picker);
        EditText edittextquoteaddress = dialog.findViewById(R.id.edittext_quote_address);
        EditText edittextquotelandmark = dialog.findViewById(R.id.edittext_quote_landmark);
        EditText edittextquotenumber = dialog.findViewById(R.id.edittext_quote_number);
        EditText edittext_tandc = dialog.findViewById(R.id.edittext_tandc);
        EditText edittext_discount_percentage = dialog.findViewById(R.id.edittext_discount_percentage);

        EditText edittext_extra_name = dialog.findViewById(R.id.edittext_extra_name);
        EditText edittext_extra_price = dialog.findViewById(R.id.edittext_extra_price);

        //editTextChooseFile = dialog.findViewById(R.id.edittext_choosefile);

        TextInputLayout textInputCreateDatePicker = dialog.findViewById(R.id.textInput_createdate_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputValidityDatePicker = dialog.findViewById(R.id.textInput_createdate_picker);

        RadioButton radiobuttonService = dialog.findViewById(R.id.radiobuttonService);
        RadioButton radiobuttonItems = dialog.findViewById(R.id.radiobuttonItems);
        RadioButton radiobuttonDiscount = dialog.findViewById(R.id.radiobuttonDiscount);

        RecyclerView servicelist = dialog.findViewById(R.id.recyclerView_service_list);
        RecyclerView itemlist = dialog.findViewById(R.id.recyclerView_item_list);

        TextInputLayout spindiscount = dialog.findViewById(R.id.textInput_discount_percentage);
        Spinner spintax = dialog.findViewById(R.id.spin_tax);
        Spinner spinpercent = dialog.findViewById(R.id.spin_percent);

        LinearLayout ldiscount = dialog.findViewById(R.id.layout_discount);
        LinearLayout items = dialog.findViewById(R.id.items);


        LinearLayoutManager mLayoutManager;

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputCreateDatePicker.setEnabled(true);
        textInputValidityDatePicker.setEnabled(true);
        DateUtils.currentDatePicker(editTextCreateDatePicker);

        ArrayList<EnquiryServiceTypeDTO.ResultData> multipleServiceArrayList = new ArrayList<EnquiryServiceTypeDTO.ResultData>();
        EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
        resultData.setId(0);
        resultData.setServiceName("");
        multipleServiceArrayList.add(resultData);

        addServiceAdapter = new AddQuoteServiceAdapter(this, multipleServiceArrayList, mContext, serviceNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        servicelist.setLayoutManager(mLayoutManager);
        servicelist.setAdapter(addServiceAdapter);

        ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList1 = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned1 = new AddTask.MultipleItemAssigned();
        multipleItemAssigned1.setItemId(0);
        multipleItemAssigned1.setItemQuantity(0);
        multipleItemAssigned1.setItemName("");
        multipleItemAssignedArrayList1.add(multipleItemAssigned1);

        addItemsAdapter = new AddQuoteItemsAdapter(this, multipleItemAssignedArrayList1/*addItemList*/, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        itemlist.setLayoutManager(mLayoutManager);
        itemlist.setAdapter(addItemsAdapter);

        /*----------------------------------------------------------------------------------------------------
                                            Auto Fetch Customer Name
        ------------------------------------------------------------------------------------------------------*/

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerDetailsId = customerList.get(position).getCustomerDetailsid();
                customerObj = customerList.get(position);
                edittextquotenumber.setText(customerObj.getMobileNumber());
                edittextquoteaddress.setText(customerObj.getAddress());
                edittextquotelandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        /*----------------------------------------------------------------------------------------------------
                                            Fetch Address via Google
        ------------------------------------------------------------------------------------------------------*/
        edittextquoteaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));

                // Set the fields to specify which types of place data to return.
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        /////////////////////////////////////////Date & Time picker on Add Quote form/////////////////////////////////////////////////////////
        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });
        editTextCreateDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextCreateDatePicker);
            }
        });
        editTextValidityDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextValidityDate);
            }
        });

        /*----------------------------------------------------------------------------------------------------
                                            Radio Button OnClick
        ------------------------------------------------------------------------------------------------------*/
        radiobuttonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.VISIBLE);
                itemlist.setVisibility(View.GONE);
                spindiscount.setVisibility(View.GONE);
                ldiscount.setVisibility(View.GONE);
            }
        });
        radiobuttonItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.GONE);
                items.setVisibility(View.VISIBLE);
                itemlist.setVisibility(View.VISIBLE);
                spindiscount.setVisibility(View.GONE);
                ldiscount.setVisibility(View.GONE);
            }
        });
        radiobuttonDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.GONE);
                itemlist.setVisibility(View.GONE);
                spindiscount.setVisibility(View.VISIBLE);
                ldiscount.setVisibility(View.VISIBLE);
            }
        });
       /* editTextChooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // FileUtils.openPdfFile(mContext);
                try {
                    ((HomeActivityNew) mContext).openPdfFile();
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });
*/
        buttonAddQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String crDate = editTextCreateDatePicker.getText().toString();
                    String valDate = editTextValidityDate.getText().toString();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    date1 = format.parse(crDate);
                    date2 = format.parse(valDate);
                } catch (Exception e) {
                    e.getMessage();
                }

                if (isEmpty(edittextQuoteName)) {
                    edittextQuoteName.setError(activity.getString(R.string.please_enter_task_name));
                } else if (!isEmpty(edittextQuoteName) && Character.isWhitespace(edittextQuoteName.getText().toString().charAt(0)) || edittextQuoteName.getText().toString().trim().isEmpty()) {
                    edittextQuoteName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (!isEmpty(autoCompleteCustName) && (Character.isWhitespace(autoCompleteCustName.getText().toString().charAt(0)) || autoCompleteCustName.getText().toString().trim().isEmpty())) {
                    autoCompleteCustName.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(edittextquoteaddress)) {
                    edittextquoteaddress.setError(activity.getString(R.string.please_enter_address));
                } else if (isEmpty(edittextquotelandmark)) {
                    edittextquotelandmark.setError(activity.getString(R.string.please_enter_landmark));
                } else if (!isEmpty(edittextquotelandmark) && (Character.isWhitespace(edittextquotelandmark.getText().toString().charAt(0)) || edittextquotelandmark.getText().toString().trim().isEmpty())) {
                    edittextquotelandmark.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (isEmpty(editTextCreateDatePicker)) {//This will validate only when user select Task Type as Others as Urgent/Today task type have default value
                    editTextCreateDatePicker.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(editTextValidityDate)) {
                    editTextValidityDate.setError(activity.getString(R.string.please_enter_date));
                } else if (isEmpty(edittext_tandc)) {
                    edittext_tandc.setError(activity.getString(R.string.please_enter_details));
                } else if (!isEmpty(edittext_tandc) && (Character.isWhitespace(edittext_tandc.getText().toString().charAt(0)) || edittext_tandc.getText().toString().trim().isEmpty())) {
                    edittext_tandc.setError(activity.getResources().getString(R.string.space_is_not_allowed));
                } else if (mTempLat == 0 || mTempLong == 0) {
                    edittextquoteaddress.setError(activity.getString(R.string.please_enter_address));
                    Toast.makeText(activity, activity.getResources().getString(R.string.please_enter_address_correctly), Toast.LENGTH_SHORT).show();
                } else if (!edittext_extra_name.getText().toString().isEmpty() && edittext_extra_price.getText().toString().isEmpty()) {
                    edittext_extra_price.setError("Please Enter Price");
                } else if (date1.after(date2)) {
                    Toast.makeText(mContext, "Validity Date should not be less than Quote Date!", Toast.LENGTH_SHORT).show();
                } else if (finalServiceArry.size() <= 0) {
                    Toast.makeText(mContext, "Please add a Service!", Toast.LENGTH_LONG).show();

                } else {
                    if (!isEmpty(edittextQuoteName)) { //This will validate only if user enter the customer no
                        if ((Long.valueOf(edittextquotenumber.getText().toString()) == 0)) {
                            edittextquotenumber.setError(activity.getString(R.string.please_enter_valid_number));
                            return;
                        }
                        if (SharedPrefManager.getInstance(activity).getCountryDetailsID() == 1) {
                            if (isValidPhone(edittextquotenumber)) {
                                edittextquotenumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        } else {
                            if (CommonFunction.isValidPhoneNRI(edittextquotenumber)) { // NRI
                                edittextquotenumber.setError(activity.getString(R.string.please_enter_valid_number));
                                return;
                            }
                        }
                    }

                    try {
                        SaveQuotationDTO saveQuotationDTO = new SaveQuotationDTO();

                        SaveQuotationDTO.Customer saveCustomer = new SaveQuotationDTO.Customer();
                        SaveQuotationDTO.LocationList locationList = new SaveQuotationDTO.LocationList();
                        SaveQuotationDTO.QuoteServiceList quoteServiceList = new SaveQuotationDTO.QuoteServiceList();
                        SaveQuotationDTO.QuoteTaxList quoteTaxList = new SaveQuotationDTO.QuoteTaxList();
                        SaveQuotationDTO.QuoteItemList quoteItemList = new SaveQuotationDTO.QuoteItemList();
                        //SaveQuotationDTO.SLAAttachmentFile slaAttachmentFile = new SaveQuotationDTO.SLAAttachmentFile();


                        //saveQuotationDTO.setId();
                        saveQuotationDTO.setQuoteName(edittextQuoteName.getText().toString());
                        saveQuotationDTO.setCreatedDate(editTextCreateDatePicker.getText().toString() + "T00:00:00");
                        //saveQuotationDTO.setQuoteTime(editTextTimePicker.getText().toString());
                        saveQuotationDTO.setValidityDate(editTextValidityDate.getText().toString() + "T00:00:00");
                        saveQuotationDTO.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                        saveQuotationDTO.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                        saveQuotationDTO.setUserId(SharedPrefManager.getInstance(mContext).getUserId());
                        saveQuotationDTO.setTermCondition(edittext_tandc.getText().toString());

                        try {
                            saveQuotationDTO.setExtraItem(edittext_extra_name.getText().toString());
                            saveQuotationDTO.setExtraAmount(Integer.parseInt(edittext_extra_price.getText().toString()));
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                        saveQuotationDTO.setStatusId(2);


                        //
                        try {
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Calendar cal = Calendar.getInstance();
                            String dt = dateFormat.format(cal.getTime());
                            saveQuotationDTO.setQuoteTime(dt);
                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        saveCustomer.setCustomerName(autoCompleteCustName.getText().toString());
                        saveCustomer.setCustomerDetailsid(customerDetailsId);
                        //saveCustomer.setLocationId();
                        saveCustomer.setMobileNumber(edittextquotenumber.getText().toString());

                        saveQuotationDTO.setCustomer(saveCustomer);


                        //locationList.setName();
                        locationList.setLatitude(String.valueOf(mTempLat));
                        locationList.setLongitude(String.valueOf(mTempLong));
                        locationList.setDescription(edittextquotelandmark.getText().toString());
                        locationList.setAddress(edittextquoteaddress.getText().toString());
                        //locationList.setPinCode();
                        locationList.setIsActive(true);
                        saveCustomer.setLocationList(locationList);
                        saveCustomer.setIsActive(true);

                        //FOR MULTIPLE SERVICE NAME
                        try {
                            if (finalServiceArry.size() > 0) {
                                ArrayList<SaveQuotationDTO.QuoteServiceList> tempServiceArry = new ArrayList<>();
                                for (int i = 0; i < finalServiceArry.size(); i++) {
                                    quoteServiceList = new SaveQuotationDTO.QuoteServiceList();
                                    quoteServiceList.setServiceId(finalServiceArry.get(i).getId());
                                    quoteServiceList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                    quoteServiceList.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                    quoteServiceList.setServiceName(finalServiceArry.get(i).getServiceName());
                                    quoteServiceList.setPrice(Integer.parseInt(String.valueOf(finalServiceArry.get(i).getPrice())));
                                    quoteServiceList.setIsActive(true);
                                    tempServiceArry.add(quoteServiceList);
                                    /*saveQuotationDTO.setQuoteServiceList(Collections.singletonList(quoteServiceList));*/
                                    saveQuotationDTO.setQuoteServiceList(tempServiceArry);
                                }
                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        try {
                            String taxValue = spintax.getSelectedItem().toString();
                            if (taxValue.equalsIgnoreCase("Select Tax")) {
                                taxId = 0;
                                quoteTaxList.setTaxAmount(0);
                                quoteTaxList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                quoteTaxList.setWithoutTax(taxId);
                            }
                            if (taxValue.equalsIgnoreCase("With Tax")) {
                                taxId = 1;
                                quoteTaxList.setTaxAmount(Integer.parseInt(spinpercent.getSelectedItem().toString().split("%")[0]));
                                quoteTaxList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                quoteTaxList.setWithoutTax(taxId);
                            }
                            if (taxValue.equalsIgnoreCase("Without Tax")) {
                                taxId = 2;
                                quoteTaxList.setTaxAmount(0);
                                quoteTaxList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                quoteTaxList.setWithoutTax(taxId);
                            }

                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                        if (!edittext_discount_percentage.getText().toString().isEmpty()) {
                            quoteTaxList.setDiscount(Integer.parseInt(edittext_discount_percentage.getText().toString()));
                        }
                        //quoteTaxList.setDiscountAmounnt(Integer.parseInt(edittext_discount_percentage.getText().toString()));

                        saveQuotationDTO.setQuoteTaxList(Collections.singletonList(quoteTaxList));

                        // FOR MULTIPLE ITEMS :
                        try {
                            if (finalPostArray.size() > 0) {
                                ArrayList<SaveQuotationDTO.QuoteItemList> tempItemArry = new ArrayList<>();
                                for (int i = 0; i < finalPostArray.size(); i++) {
                                    quoteItemList = new SaveQuotationDTO.QuoteItemList();
                                    quoteItemList.setItemId(finalPostArray.get(i).getItemId());
                                    quoteItemList.setQuantity(finalPostArray.get(i).getUsedItemQty());
                                    quoteItemList.setItemName(finalPostArray.get(i).getItemName());
                                    quoteItemList.setUnitPrice(finalPostArray.get(i).getItemQuantity());
                                    quoteItemList.setTotalPrice(finalPostArray.get(i).getTempTechSrNo());// USER INPUT
                                    quoteItemList.setIsActive(true);
                                    tempItemArry.add(quoteItemList);
                                    saveQuotationDTO.setQuoteItemList(tempItemArry);
                                    /*saveQuotationDTO.setQuoteItemList(Collections.singletonList(quoteItemList));*/
                                }
                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                        viewAndSaveQuote(activity, context, saveQuotationDTO);
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).mServiceTypeList.remove(0);
                ((HomeActivityNew) mContext).mItemsLists.remove(0);
                dialog.dismiss();
            }
        });
        buttonCloseService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).mServiceTypeList.remove(0);
                ((HomeActivityNew) mContext).mItemsLists.remove(0);
                dialog.dismiss();
            }
        });
    /*---------------------------------------------------------------------------------------------------------
                                    Discount & Tax Spinner Static Data Set
    ---------------------------------------------------------------------------------------------------------*/
        if (mTaxDetailsArry.size() > 0) {
            ArrayList<Integer> discountArry = new ArrayList<>();
            ArrayList<String> taxNameArry = new ArrayList<>();

            // discountArry.add(0, "Discount %");
            //taxNameArry.add(0, "Tax %");

            for (TaxDetails.ResultData dicoutResuldata : mTaxDetailsArry) {
                /*discountArry.add(String.valueOf(dicoutResuldata.getTaxPercentage()));*/
                discountArry.add(dicoutResuldata.getTaxPercentage());// 9823054520
                taxNameArry.add(dicoutResuldata.getTaxName());
            }

            String[] values1 = {"Select Tax", "With Tax", "Without Tax"};
            ArrayAdapter<String> tax = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, values1);
            spintax.setAdapter(tax);

            //String[] values2 = {"Tax %", "5%", "10%", "18%"};
            ArrayAdapter<String> percent = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, taxNameArry);
            spinpercent.setAdapter(percent);

        }


        spintax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedOption = parent.getItemAtPosition(position).toString();
                if (selectedOption.equals("Without Tax")) {
                    spinpercent.setVisibility(View.INVISIBLE);
                } else if (selectedOption.equals("With Tax")) {
                    spinpercent.setVisibility(View.VISIBLE);
                } else if (selectedOption.equals("Select Tax")) {
                    spinpercent.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case when nothing is selected
            }
        });

        dialog.setOnUpdateDialogListener(new

                                                 DialogUpdateListener() {
                                                     @Override
                                                     public void onDialogUpdate(Object... obj) {
                                                         FWLogger.logInfo(TAG, "onDialogUpdate");
                                                         Place place = null;
                                                         if (obj != null) {
                                                             place = (Place) obj[0];
                                                         }
                                                         if (place != null) {
                                                             edittextquoteaddress.setError(null);
                                                             edittextquotelandmark.setError(null);
                                                             edittextquoteaddress.setText(place.getAddress());
                                                             mTempPlace = place;
                                                             mTempLat = mTempPlace.getLatLng().latitude;
                                                             mTempLong = mTempPlace.getLatLng().longitude;
                                                             mTempLocName = mTempPlace.getName();
                                                         }
                                                     }
                                                 });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mTempPlace = null;
                FWDialog.removeCallBack();
            }
        });

    }

    public void viewAndSaveQuote(Activity activity, Context mContext, SaveQuotationDTO saveQuotationDTO) {
        Dialog dialog = new Dialog(activity, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_view_saveadd_quote);


        TextView txtCustomerName = dialog.findViewById(R.id.txtCustomerName);
        TextView txtCustMobile = dialog.findViewById(R.id.txtCustMobile);
        TextView txtCustAddress = dialog.findViewById(R.id.txtCustAddress);
        TextView txtLandmark = dialog.findViewById(R.id.txtLandmark);
        TextView txtQuotDate = dialog.findViewById(R.id.txtQuotDate);
        TextView txtValidtill = dialog.findViewById(R.id.txtValidtill);

        TextView txtSubtotal = dialog.findViewById(R.id.txtSubtotal);
        TextView txtQuoDiscount = dialog.findViewById(R.id.txtQuoDiscount);
        TextView txtAmtAftrDisc = dialog.findViewById(R.id.txtAmtAftrDisc);
        TextView txtgstIgst = dialog.findViewById(R.id.txtgstIgst);
        TextView txtGrandTotal = dialog.findViewById(R.id.txtGrandTotal);
        TextView txtTermsConditn = dialog.findViewById(R.id.txtTermsConditn);
        //TextView discountTxt = dialog.findViewById(R.id.discountTxt);

        RecyclerView serviceRecyclerview = dialog.findViewById(R.id.serviceRecyclerview);
        RecyclerView itemRecyclerview = dialog.findViewById(R.id.itemRecyclerview);
        RecyclerView extraItmRecyclerview = dialog.findViewById(R.id.extraItmRecyclerview);
        TableRow extraItemTablRow = dialog.findViewById(R.id.extraItemTablRow);

        Button btnClose = dialog.findViewById(R.id.btnClose);
        //Button btnEdit = dialog.findViewById(R.id.btnEdit);
        Button btnSave = dialog.findViewById(R.id.btnSave);

        try {
            txtCustomerName.setText(saveQuotationDTO.getCustomer().getCustomerName());
            txtCustMobile.setText(saveQuotationDTO.getCustomer().getMobileNumber());
            txtCustAddress.setText(saveQuotationDTO.getCustomer().getLocationList().getAddress());
            txtLandmark.setText(saveQuotationDTO.getCustomer().getLocationList().getDescription());
            try {
                txtQuotDate.setText(saveQuotationDTO.getCreatedDate().toString().split("T")[0]);
            } catch (Exception ex) {
                ex.getMessage();
            }
            //
            try {
                txtValidtill.setText(saveQuotationDTO.getValidityDate().toString().split("T")[0]);
            } catch (Exception ex) {
                ex.getMessage();
            }

            txtTermsConditn.setText(saveQuotationDTO.getTermCondition());
            //txtSubtotal.setText(saveQuotationDTO.getQuoteTaxList().get(0).get);

            // FOR MULTIPLE SERVICE NAME AND PRICE
            if (saveQuotationDTO.getQuoteServiceList() != null) {
                try {
                    multipleServiceArray = new ArrayList<SaveQuotationDTO.QuoteServiceList>();
                    multipleServiceArray = saveQuotationDTO.getQuoteServiceList();

                    if (multipleServiceArray.size() > 0) {
                        mLayoutManager = new LinearLayoutManager(activity);
                        serviceRecyclerview.setLayoutManager(mLayoutManager);
                        quotSubServiceAdapter = new QuotSubServiceAdapter(activity, multipleServiceArray);
                        serviceRecyclerview.setAdapter(quotSubServiceAdapter);
                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
            // FOR MULTIPLE ITEM DETAILS
            if (saveQuotationDTO.getQuoteItemList() != null) {
                multipleItemArray = new ArrayList<SaveQuotationDTO.QuoteItemList>();
                multipleItemArray = saveQuotationDTO.getQuoteItemList();

                if (multipleItemArray.size() > 0) {
                    mLayoutManager = new LinearLayoutManager(activity);
                    itemRecyclerview.setLayoutManager(mLayoutManager);
                    quotSubItemAdapter = new QuotSubItemAdapter(activity, multipleItemArray);
                    itemRecyclerview.setAdapter(quotSubItemAdapter);
                }
            }

            // FOR EXTRA ITEM
            try {
                if (!saveQuotationDTO.getExtraItem().isEmpty() && saveQuotationDTO.getExtraAmount() != 0) {
                    extraItemTablRow.setVisibility(View.VISIBLE);
                    mLayoutManager = new LinearLayoutManager(activity);
                    extraItmRecyclerview.setLayoutManager(mLayoutManager);
                    quotSubExtraItemAdapter = new QuotSubExtraItemAdapter(activity, saveQuotationDTO.getExtraItem(), saveQuotationDTO.getExtraAmount());
                    extraItmRecyclerview.setAdapter(quotSubExtraItemAdapter);
                }

            } catch (Exception ex) {
                ex.getMessage();
            }

        } catch (Exception ex) {
            ex.getMessage();
        }

        try {
            double discDivide = 0.0;
            double amtAfterDisc = 0.0;
            double finalSubTotal = 0.0, serviceSum = 0.0, itemSum = 0.0, extraItemSum = 0.0;
            double gstIGST = 0.0, taxAmount = 0.0;
            double grandTotal = 0.0;
            //1 : Service Sum
            try {
                if (multipleServiceArray.size() > 0) {
                    for (int i = 0; i < multipleServiceArray.size(); i++) {
                        serviceSum += multipleServiceArray.get(i).getPrice();
                    }
                } else {
                    serviceSum = 0;
                }
            } catch (Exception ex) {
                serviceSum = 0;
                ex.getMessage();
            }
            //2 Item Sum
            try {
                if (multipleItemArray.size() > 0) {
                    for (int i = 0; i < multipleItemArray.size(); i++) {
                        itemSum += multipleItemArray.get(i).getQuantity();
                    }
                } else {
                    itemSum = 0;
                }
            } catch (Exception ex) {
                itemSum = 0;
                ex.getMessage();
            }
            //3 Extra Item Sum
            try {
                if (saveQuotationDTO.getExtraAmount() != 0) {
                    extraItemSum += saveQuotationDTO.getExtraAmount();
                } else {
                    extraItemSum = 0;
                }
            } catch (Exception ex) {
                extraItemSum = 0;
                ex.getMessage();
            }

            finalSubTotal = serviceSum + itemSum + extraItemSum;

            saveQuotationDTO.setTotalAmount((int) finalSubTotal);
            txtSubtotal.setText("₹" + String.valueOf(finalSubTotal));
            try {
                float disc = saveQuotationDTO.getQuoteTaxList().get(0).getDiscount();
                if (disc != 0) {
                    float discper = (disc / 100);
                    discDivide = finalSubTotal * (discper);
                    txtQuoDiscount.setText("₹" + String.valueOf(discDivide) + " " + "(" + disc + "%" + ")");
                    // SET AMOUNT AFTER DISC
                    try {
                        amtAfterDisc = finalSubTotal - discDivide;
                        txtAmtAftrDisc.setText("₹" + String.valueOf(amtAfterDisc));
                        saveQuotationDTO.getQuoteTaxList().get(0).setDiscount(disc);
                        saveQuotationDTO.getQuoteTaxList().get(0).setDiscountAmounnt((float) amtAfterDisc);
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                } else {
                    txtQuoDiscount.setText("₹" + "0.00");
                    txtAmtAftrDisc.setText("₹" + String.valueOf(finalSubTotal));
                    saveQuotationDTO.getQuoteTaxList().get(0).setDiscount(0);
                    saveQuotationDTO.getQuoteTaxList().get(0).setDiscountAmounnt((float) finalSubTotal);

                }

            } catch (Exception ex) {
                ex.getMessage();
            }

            try {
                // SELECT TAX
                if (saveQuotationDTO.getQuoteTaxList().get(0).getWithoutTax() == 0) {
                    txtgstIgst.setText("₹" + "0.00");
                    amtAfterDisc = finalSubTotal - discDivide;
                }
                // WITH TAX (18%,10%)
                if (saveQuotationDTO.getQuoteTaxList().get(0).getWithoutTax() == 1) {
                    taxAmount = saveQuotationDTO.getQuoteTaxList().get(0).getTaxAmount();
                    if (amtAfterDisc != 0) {
                        gstIGST = amtAfterDisc * (taxAmount / 100);
                        txtgstIgst.setText("₹" + String.format("%.2f", gstIGST) + " " + "(" + taxAmount + "%" + ")");
                        saveQuotationDTO.getQuoteTaxList().get(0).setTax((float) taxAmount);
                        saveQuotationDTO.getQuoteTaxList().get(0).setTaxAmount((float) gstIGST);
                    } else {
                        amtAfterDisc = finalSubTotal;
                        gstIGST = amtAfterDisc * (taxAmount / 100);
                        txtgstIgst.setText("₹" + String.format("%.2f", gstIGST) + " " + "(" + taxAmount + "%" + ")");
                        saveQuotationDTO.getQuoteTaxList().get(0).setTax((float) taxAmount);
                        saveQuotationDTO.getQuoteTaxList().get(0).setTaxAmount((float) gstIGST);
                    }

                }
                // WITHOUT TAX
                if (saveQuotationDTO.getQuoteTaxList().get(0).getWithoutTax() == 2) {
                    txtgstIgst.setText("₹" + "0.00");
                    amtAfterDisc = finalSubTotal - discDivide;
                    saveQuotationDTO.getQuoteTaxList().get(0).setTax(0);
                    saveQuotationDTO.getQuoteTaxList().get(0).setTaxAmount(0);
                }

                // GRAND TOTAL
                try {
                    if (gstIGST != 0) {
                        grandTotal = amtAfterDisc + gstIGST;
                        txtGrandTotal.setText("₹" + String.valueOf(String.format("%.2f", grandTotal)));
                        saveQuotationDTO.setGrandTotalAmount((float) grandTotal);
                    } else {
                        grandTotal = amtAfterDisc + 0.0;
                        txtGrandTotal.setText("₹" + String.valueOf(String.format("%.2f", grandTotal)));
                        saveQuotationDTO.setGrandTotalAmount((float) grandTotal);
                    }

                } catch (Exception ex) {
                    ex.getMessage();
                }


            } catch (Exception ex) {
                ex.getMessage();
            }
        } catch (Exception e) {
            e.getMessage();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // POST ADD QUOTE DATA
                try {
                    //saveQuotationDTO.setId();
                    saveQuotationDTO.setIsActive(true);
                    saveQuotationDTO.setQuoteName(saveQuotationDTO.getQuoteName());
                    saveQuotationDTO.setCreatedDate(saveQuotationDTO.getQuoteTime() + ".701Z");
                    //saveQuotationDTO.setQuoteTime(editTextTimePicker.getText().toString());
                    saveQuotationDTO.setValidityDate(saveQuotationDTO.getValidityDate() + ".701Z");
                    saveQuotationDTO.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    saveQuotationDTO.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    saveQuotationDTO.setUserId(SharedPrefManager.getInstance(mContext).getUserId());
                    saveQuotationDTO.setTermCondition(saveQuotationDTO.getTermCondition());
                    // EXTRA ITEM DETAILS
                    try {
                        saveQuotationDTO.setExtraItem(saveQuotationDTO.getExtraItem());
                        saveQuotationDTO.setExtraAmount(saveQuotationDTO.getExtraAmount());
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                    saveQuotationDTO.setStatusId(2);

                    saveQuotationDTO.setQuoteTime(saveQuotationDTO.getQuoteTime() + ".701Z");
                    saveQuotationDTO.setCustomerId(saveQuotationDTO.getCustomer().getCustomerDetailsid());


                    saveQuotationDTO.getCustomer().setCustomerName(saveQuotationDTO.getCustomer().getCustomerName());
                    saveQuotationDTO.getCustomer().setCustomerDetailsid(saveQuotationDTO.getCustomer().getCustomerDetailsid());
                    //saveCustomer.setLocationId();
                    saveQuotationDTO.getCustomer().setMobileNumber(saveQuotationDTO.getCustomer().getMobileNumber());
                    saveQuotationDTO.setCustomer(saveQuotationDTO.getCustomer());

                    //locationList.setName();

                    saveQuotationDTO.getCustomer().getLocationList().setName("");
                    saveQuotationDTO.getCustomer().getLocationList().setLatitude(saveQuotationDTO.getCustomer().getLocationList().getLatitude());
                    saveQuotationDTO.getCustomer().getLocationList().setLongitude(saveQuotationDTO.getCustomer().getLocationList().getLongitude());
                    saveQuotationDTO.getCustomer().getLocationList().setDescription(saveQuotationDTO.getCustomer().getLocationList().getDescription());
                    saveQuotationDTO.getCustomer().getLocationList().setAddress(saveQuotationDTO.getCustomer().getLocationList().getAddress());
                    saveQuotationDTO.getCustomer().getLocationList().setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    saveQuotationDTO.getCustomer().getLocationList().setCreatedDate(saveQuotationDTO.getCreatedDate());
                    //locationList.setPinCode();
                    saveQuotationDTO.getCustomer().getLocationList().setIsActive(true);
                    saveQuotationDTO.getCustomer().setLocationList(saveQuotationDTO.getCustomer().getLocationList());
                    saveQuotationDTO.getCustomer().setIsActive(true);

                    // SERVICE DETAILS
                    if (saveQuotationDTO.getQuoteServiceList() != null) {
                        for (int i = 0; i < saveQuotationDTO.getQuoteServiceList().size(); i++) {
                            saveQuotationDTO.getQuoteServiceList().get(i).setServiceId(saveQuotationDTO.getQuoteServiceList().get(i).getServiceId());
                            saveQuotationDTO.getQuoteServiceList().get(i).setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                            saveQuotationDTO.getQuoteServiceList().get(i).setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                            saveQuotationDTO.getQuoteServiceList().get(i).setServiceName(saveQuotationDTO.getQuoteServiceList().get(i).getServiceName());
                            saveQuotationDTO.getQuoteServiceList().get(i).setPrice(Integer.parseInt(String.valueOf(saveQuotationDTO.getQuoteServiceList().get(i).getPrice())));
                            saveQuotationDTO.getQuoteServiceList().get(i).setCreatedDate(saveQuotationDTO.getQuoteTime());
                            saveQuotationDTO.getQuoteServiceList().get(i).setIsActive(true);
                            saveQuotationDTO.setQuoteServiceList(saveQuotationDTO.getQuoteServiceList());
                        }
                    } else {
                        saveQuotationDTO.setQuoteServiceList(null);
                    }


                    // ITEM DETAILS
                    if (saveQuotationDTO.getQuoteItemList() != null) {
                        for (int i = 0; i < saveQuotationDTO.getQuoteItemList().size(); i++) {
                            saveQuotationDTO.getQuoteItemList().get(i).setItemId(saveQuotationDTO.getQuoteItemList().get(i).getItemId());
                            saveQuotationDTO.getQuoteItemList().get(i).setQuantity(finalPostArray.get(i).getItemQuantity());
                            saveQuotationDTO.getQuoteItemList().get(i).setItemName(saveQuotationDTO.getQuoteItemList().get(i).getItemName());
                            saveQuotationDTO.getQuoteItemList().get(i).setUnitPrice(finalPostArray.get(i).getTempTechSrNo()); // UNIT PRICE
                            saveQuotationDTO.getQuoteItemList().get(i).setTotalPrice(finalPostArray.get(i).getUsedItemQty());// Total PRICE
                            saveQuotationDTO.getQuoteItemList().get(i).setCreatedDate(saveQuotationDTO.getQuoteTime());
                            saveQuotationDTO.getQuoteItemList().get(i).setIsActive(true);
                            saveQuotationDTO.setQuoteItemList(saveQuotationDTO.getQuoteItemList());
                        }
                    } else {
                        saveQuotationDTO.setQuoteItemList(null);
                    }


                    // TAX LIST
                    if (saveQuotationDTO.getQuoteTaxList() != null) {
                        for (int i = 0; i < saveQuotationDTO.getQuoteTaxList().size(); i++) {
                            saveQuotationDTO.getQuoteTaxList().get(i).setDiscount(saveQuotationDTO.getQuoteTaxList().get(i).getDiscount());
                            saveQuotationDTO.getQuoteTaxList().get(i).setDiscountAmounnt(saveQuotationDTO.getQuoteTaxList().get(i).getDiscountAmounnt());
                            saveQuotationDTO.getQuoteTaxList().get(i).setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                            saveQuotationDTO.getQuoteTaxList().get(i).setTax(saveQuotationDTO.getQuoteTaxList().get(i).getTax());
                            saveQuotationDTO.getQuoteTaxList().get(i).setTaxAmount(saveQuotationDTO.getQuoteTaxList().get(i).getTaxAmount());
                            saveQuotationDTO.getQuoteTaxList().get(i).setWithoutTax(saveQuotationDTO.getQuoteTaxList().get(i).getWithoutTax());
                            saveQuotationDTO.getQuoteTaxList().get(i).setCreatedDate(saveQuotationDTO.getQuoteTime());
                            saveQuotationDTO.getQuoteTaxList().get(i).setUserId(saveQuotationDTO.getUserId());
                            saveQuotationDTO.getQuoteTaxList().get(i).setIsActive(true);
                            saveQuotationDTO.setQuoteTaxList(saveQuotationDTO.getQuoteTaxList());
                        }
                    } else {
                        saveQuotationDTO.setQuoteTaxList(null);
                    }

                    saveQuoteDetails(activity, mContext, saveQuotationDTO);
                    dialog.dismiss();

                } catch (Exception ex) {
                    ex.getMessage();
                }

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multipleServiceArray.size() > 0) {
                    multipleServiceArray.clear();
                    finalServiceArry.clear();
                }
                try {
                    if (multipleItemArray.size() > 0) {
                        multipleItemArray.clear();
                        finalPostArray.clear();

                    }
                } catch (Exception ex) {
                    ex.getMessage();
                }
                ((HomeActivityNew) mContext).mServiceTypeList.remove(0);
                ((HomeActivityNew) mContext).mItemsLists.remove(0);
                dialog.dismiss();
            }
        });

        /*btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        dialog.show();
    }


    public void editQuote(Context context, Activity activity, List<CustomerList.ResultData> customerList, List<EnquiryServiceTypeDTO.ResultData> serviceNameList, ArrayList<String> itemsNameList, List<TaxDetails.ResultData> mTaxDetailsArry, List<QuotationDetailsDTO.ResultData> mQuoteList) {
        Dialog dialog = new Dialog(context, R.style.DialogSlideAnim);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_edit_quote);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

        mContext = context;

        Button buttonCloseEditQuot = dialog.findViewById(R.id.button_cancel);
        Button buttonEditQuote = dialog.findViewById(R.id.buttonAddQuote);
        ImageView closeButton = dialog.findViewById(R.id.closeAddQuote);

        TextView heading = dialog.findViewById(R.id.textView_dialog_title);
        heading.setText("Edit Quote");
        buttonEditQuote.setText("UPDATE");

        EditText editTextCreateDatePicker = dialog.findViewById(R.id.edittext_createdate_picker);
        EditText edittextQuoteName = dialog.findViewById(R.id.edittext_quote_name);
        EditText editTextTimePicker = dialog.findViewById(R.id.edittext_time);
        EditText editTextValidityDate = dialog.findViewById(R.id.edittext_validitydate_picker);
        EditText edittextquoteaddress = dialog.findViewById(R.id.edittext_quote_address);
        EditText edittextquotelandmark = dialog.findViewById(R.id.edittext_quote_landmark);
        EditText edittextquotenumber = dialog.findViewById(R.id.edittext_quote_number);
        EditText edittext_tandc = dialog.findViewById(R.id.edittext_tandc);
        //
        EditText edittext_extra_name = dialog.findViewById(R.id.edittext_extra_name);
        EditText edittext_extra_price = dialog.findViewById(R.id.edittext_extra_price);

        editTextChooseFile = dialog.findViewById(R.id.edittext_choosefile);

        TextInputLayout textInputCreateDatePicker = dialog.findViewById(R.id.textInput_createdate_picker);
        TextInputLayout textInputTimePicker = dialog.findViewById(R.id.textInput_time);
        TextInputLayout textInputValidityDatePicker = dialog.findViewById(R.id.textInput_createdate_picker);

        RadioButton radiobuttonService = dialog.findViewById(R.id.radiobuttonService);
        RadioButton radiobuttonItems = dialog.findViewById(R.id.radiobuttonItems);
        RadioButton radiobuttonDiscount = dialog.findViewById(R.id.radiobuttonDiscount);

        RecyclerView servicelist = dialog.findViewById(R.id.recyclerView_service_list);
        RecyclerView itemlist = dialog.findViewById(R.id.recyclerView_item_list);

        TextInputLayout spindiscount = dialog.findViewById(R.id.textInput_discount_percentage);
        Spinner spintax = dialog.findViewById(R.id.spin_tax);
        Spinner spinpercent = dialog.findViewById(R.id.spin_percent);

        LinearLayout ldiscount = dialog.findViewById(R.id.layout_discount);
        LinearLayout items = dialog.findViewById(R.id.items);

        LinearLayoutManager mLayoutManager;

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(DateUtils.getPlusFifteenMins());
        editTextTimePicker.setError(null);
        editTextTimePicker.setText(currentTime);
        textInputTimePicker.setEnabled(true);
        textInputCreateDatePicker.setEnabled(true);
        textInputValidityDatePicker.setEnabled(true);

        ArrayList<EnquiryServiceTypeDTO.ResultData> multipleServiceArrayList = new ArrayList<EnquiryServiceTypeDTO.ResultData>();
        EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
        resultData.setId(0);
        resultData.setServiceName("");
        multipleServiceArrayList.add(resultData);

        addServiceAdapter = new AddQuoteServiceAdapter(this, multipleServiceArrayList, mContext, serviceNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        servicelist.setLayoutManager(mLayoutManager);
        servicelist.setAdapter(addServiceAdapter);


        ArrayList<AddTask.MultipleItemAssigned> multipleItemAssignedArrayList1 = new ArrayList<AddTask.MultipleItemAssigned>();
        AddTask.MultipleItemAssigned multipleItemAssigned1 = new AddTask.MultipleItemAssigned();
        multipleItemAssigned1.setItemId(0);
        multipleItemAssigned1.setItemQuantity(0);
        multipleItemAssigned1.setItemName("");
        multipleItemAssignedArrayList1.add(multipleItemAssigned1);

        addItemsAdapter = new AddQuoteItemsAdapter(this, multipleItemAssignedArrayList1/*addItemList*/, mContext, itemsNameList);
        mLayoutManager = new LinearLayoutManager(mContext);
        itemlist.setLayoutManager(mLayoutManager);
        itemlist.setAdapter(addItemsAdapter);

        AutoCompleteTextView autoCompleteCustName = (AutoCompleteTextView) dialog.findViewById(R.id.autoComplete_cust_name);
        CustomerAdapter adapter = new CustomerAdapter(activity, R.layout.customer_name_item, customerList);
        autoCompleteCustName.setAdapter(adapter);

        // SET DATA
        try { //mQuoteList
            if (mQuoteList.size() > 0) {
                edittextQuoteName.setText(mQuoteList.get(0).getQuoteName());
                try {
                    editTextCreateDatePicker.setText(DateUtils.convertDateFormat(mQuoteList.get(0).getCreatedDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy"));
                    editTextValidityDate.setText(DateUtils.convertDateFormat(mQuoteList.get(0).getValidityDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy"));
                } catch (Exception e) {
                    e.getMessage();
                }

                autoCompleteCustName.setText(mQuoteList.get(0).getCustomer().getCustomerName());
                edittextquoteaddress.setText(mQuoteList.get(0).getCustomer().getLocationList().getAddress());
                edittextquotenumber.setText(mQuoteList.get(0).getCustomer().getMobileNumber());
                // SERVICE DETAILS
                edittext_tandc.setText(mQuoteList.get(0).getTermCondition());

            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        autoCompleteCustName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FWLogger.logInfo(TAG, "Name : " + customerList.get(position).getCustomerName() + " Number : " + customerList.get(position).getMobileNumber() + " ID : " + customerList.get(position).getCustomerDetailsid() + " Description : " + customerList.get(position).getDescription());
                customerDetailsId = customerList.get(position).getCustomerDetailsid();
                customerObj = customerList.get(position);
                edittextquotenumber.setText(customerObj.getMobileNumber());
                edittextquoteaddress.setText(customerObj.getAddress());
                edittextquotelandmark.setText(customerObj.getDescription());
                // Get Latlong from Address API : GetAllCustomerListForMobile
                try {
                    mTempLat = Double.parseDouble(customerObj.getLatitude());
                    mTempLong = Double.parseDouble(customerObj.getLongitude());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });

        edittextquoteaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialize Places.
                Places.initialize(activity.getApplicationContext(), activity.getResources().getString(R.string.place_api_key));

                // Set the fields to specify which types of place data to return.
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields).build(activity);
                activity.startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        editTextTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.timePicker(activity, editTextTimePicker);
            }
        });
        editTextCreateDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextCreateDatePicker);
            }
        });
        editTextValidityDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateUtils.datePicker(activity, editTextValidityDate);
            }
        });

        radiobuttonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.VISIBLE);
                itemlist.setVisibility(View.GONE);
                spindiscount.setVisibility(View.GONE);
                ldiscount.setVisibility(View.GONE);
            }
        });
        radiobuttonItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.GONE);
                items.setVisibility(View.VISIBLE);
                itemlist.setVisibility(View.VISIBLE);
                spindiscount.setVisibility(View.GONE);
                ldiscount.setVisibility(View.GONE);
            }
        });
        radiobuttonDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicelist.setVisibility(View.GONE);
                itemlist.setVisibility(View.GONE);
                spindiscount.setVisibility(View.VISIBLE);
                ldiscount.setVisibility(View.VISIBLE);
            }
        });
        editTextChooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // FileUtils.openPdfFile(mContext);
                try {
                    ((HomeActivityNew) mContext).openPdfFile();
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });

        buttonEditQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SaveQuotationDTO saveQuotationDTO = new SaveQuotationDTO();

                    SaveQuotationDTO.Customer saveCustomer = new SaveQuotationDTO.Customer();
                    SaveQuotationDTO.LocationList locationList = new SaveQuotationDTO.LocationList();
                    SaveQuotationDTO.QuoteServiceList quoteServiceList = new SaveQuotationDTO.QuoteServiceList();
                    SaveQuotationDTO.QuoteTaxList quoteTaxList = new SaveQuotationDTO.QuoteTaxList();
                    SaveQuotationDTO.QuoteItemList quoteItemList = new SaveQuotationDTO.QuoteItemList();
                    SaveQuotationDTO.SLAAttachmentFile slaAttachmentFile = new SaveQuotationDTO.SLAAttachmentFile();

                    //saveQuotationDTO.setId();
                    saveQuotationDTO.setQuoteName(edittextQuoteName.getText().toString());
                    saveQuotationDTO.setCreatedDate(editTextCreateDatePicker.getText().toString() + "T00:00:00");
                    saveQuotationDTO.setQuoteTime(editTextTimePicker.getText().toString());
                    saveQuotationDTO.setValidityDate(editTextValidityDate.getText().toString() + "T00:00:00");
                    saveQuotationDTO.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    saveQuotationDTO.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    //
                    saveQuotationDTO.setExtraItem(edittext_extra_name.getText().toString());
                    saveQuotationDTO.setExtraAmount(Integer.parseInt(edittext_extra_price.getText().toString()));


                    saveCustomer.setCustomerName(autoCompleteCustName.getText().toString());
                    saveCustomer.setCustomerDetailsid(customerDetailsId);
                    //saveCustomer.setLocationId();
                    saveCustomer.setMobileNumber(edittextquotenumber.getText().toString());

                    saveQuotationDTO.setCustomer(saveCustomer);

                    // PDF FILE ATTACHMENT DETAILS
                    //slaAttachmentFile.setInputStream(inputStreamPDF);


                    //locationList.setName();
                    locationList.setLatitude(String.valueOf(mTempLat));
                    locationList.setLongitude(String.valueOf(mTempLong));
                    locationList.setDescription(edittextquotelandmark.getText().toString());
                    locationList.setAddress(edittextquoteaddress.getText().toString());
                    //locationList.setPinCode();
                    locationList.setIsActive(true);
                    saveCustomer.setLocationList(locationList);

                    //FOR MULTIPLE SERVICE NAME
                    try {
                        if (finalServiceArry.size() > 0) {
                            for (int i = 0; i < finalServiceArry.size(); i++) {
                                quoteServiceList.setServiceId(finalServiceArry.get(i).getId());
                                quoteServiceList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                quoteServiceList.setUpdatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                                saveQuotationDTO.setQuoteServiceList(Collections.singletonList(quoteServiceList));
                            }
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    try {
                        String taxValue = spintax.getSelectedItem().toString();
                        if (taxValue.equalsIgnoreCase("With Tax")) {
                            taxId = 1;
                        }
                        if (taxValue.equalsIgnoreCase("Without Tax")) {
                            taxId = 2;
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    //quoteTaxList.setDiscount(5/*Integer.parseInt(spindiscount.getSelectedItem().toString())*/);
                    //quoteTaxList.setDiscountAmounnt();
                    quoteTaxList.setCreatedBy(SharedPrefManager.getInstance(mContext).getUserId());
                    quoteTaxList.setTax(taxId);
                    quoteTaxList.setTaxAmount(20/*Integer.parseInt(spinpercent.getSelectedItem().toString())*/);
                    saveQuotationDTO.setQuoteTaxList(Collections.singletonList(quoteTaxList));

                    // FOR MULTIPLE ITEMS
                    try {
                        if (finalPostArray.size() > 0) {
                            for (int i = 0; i < finalPostArray.size(); i++) {
                                quoteItemList.setItemId(finalPostArray.get(i).getItemId());
                                quoteItemList.setQuantity(finalPostArray.get(i).getItemQuantity());
                                saveQuotationDTO.setQuoteItemList(Collections.singletonList(quoteItemList));
                            }
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }

                    updateQuoteDetails(activity, saveQuotationDTO);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).mServiceTypeList.remove(0);
                ((HomeActivityNew) mContext).mItemsLists.remove(0);
                dialog.dismiss();
            }
        });
        buttonCloseEditQuot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivityNew) mContext).mServiceTypeList.remove(0);
                ((HomeActivityNew) mContext).mItemsLists.remove(0);
                dialog.dismiss();
            }
        });
    /*---------------------------------------------------------------------------------------------------------
                                    Discount & Tax Spinner Static Data Set
    ---------------------------------------------------------------------------------------------------------*/
        if (mTaxDetailsArry.size() > 0) {
            ArrayList<Integer> discountArry = new ArrayList<>();
            ArrayList<String> taxNameArry = new ArrayList<>();

            // discountArry.add(0, "Discount %");
            //taxNameArry.add(0, "Tax %");

            for (TaxDetails.ResultData dicoutResuldata : mTaxDetailsArry) {
                /*discountArry.add(String.valueOf(dicoutResuldata.getTaxPercentage()));*/
                discountArry.add(dicoutResuldata.getTaxPercentage());
                taxNameArry.add(dicoutResuldata.getTaxName());
            }

            String[] values1 = {"With Tax", "Without Tax"};
            ArrayAdapter<String> tax = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, values1);
            spintax.setAdapter(tax);

            //String[] values2 = {"Tax %", "5%", "10%", "18%"};
            ArrayAdapter<String> percent = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, taxNameArry);
            spinpercent.setAdapter(percent);

        }


        spintax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedOption = parent.getItemAtPosition(position).toString();
                if (selectedOption.equals("Without Tax")) {
                    spinpercent.setVisibility(View.INVISIBLE);
                } else if (selectedOption.equals("With Tax")) {
                    spinpercent.setVisibility(View.VISIBLE);
                } else if (selectedOption.equals("Select Tax")) {
                    spinpercent.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case when nothing is selected
            }
        });
    }


    public void setPhotoUpdateForService1(Bitmap bitmap) {
        mImageview1.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName1 = timeStamp + "_" + "AddUpdateService1" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromService1 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview1.setImageBitmap(bitmap);
    }

    //
    public void setPhotoUpdateForService2(Bitmap bitmap) {
        mImageview2.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName2 = timeStamp + "_" + "AddUpdateService2" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromService2 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview2.setImageBitmap(bitmap);
    }

    public void setPhotoUpdateForService3(Bitmap bitmap) {
        mImageview3.setBackground(null);
        //
        String timeStamp = DateUtils.getDate(System.currentTimeMillis(), "yyyyMMdd_HHmmss");
        imageFileName3 = timeStamp + "_" + "AddUpdateService3" + "_" + ".jpg";
        //
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        mBaseStringFromService3 = Base64.encodeToString(b, Base64.DEFAULT);
        mImageview3.setImageBitmap(bitmap);
    }

    public void setSelectedPDFName(String selectedPDFName, InputStream inputStream) {
        try {
            // Set PDF File Name WHICH IS BELOW OR = 6MB
            editTextChooseFile.setText(selectedPDFName);
            String result = convertInputStreamToString(inputStream);
            inputStreamPDF = result;
            //inputStreamPDF = String.valueOf(inputStream);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private static String convertInputStreamToString(InputStream is) throws IOException {

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int length;
        while ((length = is.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        return result.toString("UTF-8");

    }

    public void addServiceTypeDetails(Activity mActivity, ServiceTypeListDTO.ResultData resultData) {
        try {
            CommonFunction.showProgressDialog(mActivity);
            if (Connectivity.isNetworkAvailableRetro(mActivity)) {
                Call<ServiceTypeListDTO> call = RetrofitClient.getInstance(mContext).getMyApi().addServiceType(resultData, "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<ServiceTypeListDTO>() {
                    @Override
                    public void onResponse(Call<ServiceTypeListDTO> call, Response<ServiceTypeListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(mActivity);
                                ServiceTypeListDTO serviceTypeListDTO = response.body();
                                if (serviceTypeListDTO != null) {
                                    if (serviceTypeListDTO.getCode().equalsIgnoreCase("200")) {
                                        Toast.makeText(mContext, serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(mContext, serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "Add Service Type = " + serviceTypeListDTO.getResultData());
                                    Toast.makeText(mContext, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceTypeListDTO> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(mActivity);
                        FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
                    }
                });
            } else {
                CommonFunction.hideProgressDialog(mActivity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(mActivity);
            FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
            ex.getMessage();
        }

    }


    public void UpdateServiceTypeDetails(Activity mActivity, ServiceTypeListDTO.ResultData resultData) {
        try {
            if (Connectivity.isNetworkAvailableRetro(mActivity)) {
                //CommonFunction.showProgressDialog(mActivity);
                Call<ServiceTypeListDTO> call = RetrofitClient.getInstance(mContext).getMyApi().updateServiceType(resultData, "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<ServiceTypeListDTO>() {
                    @Override
                    public void onResponse(Call<ServiceTypeListDTO> call, Response<ServiceTypeListDTO> response) {
                        try {
                            if (response.code() == 200) {
                                //CommonFunction.hideProgressDialog(mActivity);
                                ServiceTypeListDTO serviceTypeListDTO = response.body();
                                if (serviceTypeListDTO != null) {
                                    if (serviceTypeListDTO.getCode().equalsIgnoreCase("200")) {
                                        Toast.makeText(mContext, serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(mContext, serviceTypeListDTO.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    FWLogger.logInfo(TAG, "Edit Service Type = " + serviceTypeListDTO.getResultData());
                                    Toast.makeText(mContext, R.string.failed_to_update_try_again, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceTypeListDTO> call, Throwable throwable) {
                        //CommonFunction.hideProgressDialog(mActivity);
                        FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
                    }
                });
            } else {
                //CommonFunction.hideProgressDialog(mActivity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            //CommonFunction.hideProgressDialog(mActivity);
            FWLogger.logInfo(TAG, "Exception in UpdateTaskByTaskID API:");
            ex.getMessage();
        }
    }

    public void saveQuoteDetails(Activity activity, Context mContext, SaveQuotationDTO saveQuotationDTO) {
        try {
            if (Connectivity.isNetworkAvailableRetro(activity)) {
                //
                //CommonFunction.showProgressDialog(activity);
                Call<SaveQuotationDTO> call = RetrofitClient.getInstance(mContext).getMyApi().saveQuotationDetails(saveQuotationDTO, "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<SaveQuotationDTO>() {
                    @Override
                    public void onResponse(Call<SaveQuotationDTO> call, Response<SaveQuotationDTO> response) {
                        try {
                            if (response.code() == 200) {
                                // CommonFunction.hideProgressDialog(activity);
                                SaveQuotationDTO saveQuotationDTO = response.body();
                                if (saveQuotationDTO != null) {
                                    Toast.makeText(activity, "Quotation Created Successfully.", Toast.LENGTH_LONG).show();
                                   /* Intent intent = new Intent(activity, HomeActivityNew.class);
                                    activity.startActivity(intent);*/
                                    callAccountTabHost(mContext);

                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<SaveQuotationDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in savequotations API:");
                    }
                });
            } else {
                //CommonFunction.hideProgressDialog(activity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            //CommonFunction.hideProgressDialog(activity);
            FWLogger.logInfo(TAG, "Exception in savequotations API:");
            ex.getMessage();
        }
    }

    public void updateQuoteDetails(Activity activity, SaveQuotationDTO saveQuotationDTO) {
        try {
            if (Connectivity.isNetworkAvailableRetro(activity)) {
                //
                CommonFunction.showProgressDialog(activity);
                Call<SaveQuotationDTO> call = RetrofitClient.getInstance(mContext).getMyApi().updateQuotationDetails(saveQuotationDTO, "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken());
                call.enqueue(new retrofit2.Callback<SaveQuotationDTO>() {
                    @Override
                    public void onResponse(Call<SaveQuotationDTO> call, Response<SaveQuotationDTO> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(activity);
                                SaveQuotationDTO saveQuotationDTO = response.body();
                                if (saveQuotationDTO != null) {
                                    //Toast.makeText(activity, "Status Updated Successfully.", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(activity, HomeActivityNew.class);
                                    activity.startActivity(intent);
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<SaveQuotationDTO> call, Throwable throwable) {
                        FWLogger.logInfo(TAG, "Exception in Updatequotations API:");
                    }
                });
            } else {
                CommonFunction.hideProgressDialog(activity);
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(mContext);
            }

        } catch (Exception ex) {
            CommonFunction.hideProgressDialog(activity);
            FWLogger.logInfo(TAG, "Exception in savequotations API:");
            ex.getMessage();
        }
    }


    @Override
    public void onMultipleMaterialPost(ArrayList<AddTask.MultipleItemAssigned> SODArray) {
        finalPostArray = SODArray;
    }

    @Override
    public void onMultipleServicePost(ArrayList<EnquiryServiceTypeDTO.ResultData> SODArray) {
        finalServiceArry = SODArray;
    }
}
