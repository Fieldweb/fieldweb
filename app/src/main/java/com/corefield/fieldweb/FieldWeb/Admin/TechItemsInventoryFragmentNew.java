package com.corefield.fieldweb.FieldWeb.Admin;


import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.Adapter.TechItemIssueListAdapterNew;
import com.corefield.fieldweb.AsyncManager.OnTaskCompleteListener;
import com.corefield.fieldweb.BuildConfig;
import com.corefield.fieldweb.DTO.Item.ItemIssueList;
import com.corefield.fieldweb.FieldWeb.Dialogs.ServiceDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.Network.URLConnectionResponse;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Retrofit.RetrofitClient;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.Connectivity;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.FirebaseGoogleAnalytics;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Fragment Controller for TechItemsInventoryFragmentNew
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Fragment class is used show item that the particular technician have
 */
public class TechItemsInventoryFragmentNew extends Fragment implements OnTaskCompleteListener, RecyclerTouchListener, View.OnClickListener {
    protected static String TAG = TechItemsInventoryFragmentNew.class.getSimpleName();
    public List<ItemIssueList.ResultData> mItemsLists = null;
    private View mRootView;
    private RecyclerView mRecyclerViewItemsList;
    private TechItemIssueListAdapterNew mTechItemIssueListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int mPosition;
    private SearchView mSearchView;
    RecyclerTouchListener recyclerTouchListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tech_item_inventory_fragment_new, container, false);
        //NOTE: Log GA event
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseGoogleAnalytics.Param.USER_ID, SharedPrefManager.getInstance(getContext()).getUserId());
        bundle.putString(FirebaseGoogleAnalytics.Param.VERSION_NAME, BuildConfig.VERSION_NAME);
        bundle.putString(FirebaseGoogleAnalytics.Param.USER_TYPE, SharedPrefManager.getInstance(getContext()).getUserGroup());
        bundle.putString(FirebaseGoogleAnalytics.Param.TIME_STAMP, DateUtils.getDateForWithGAFormat());
        bundle.putString(FirebaseGoogleAnalytics.Param.PLATFORM, FirebaseGoogleAnalytics.Defaults.PLATFORM_ANDROID);
        FirebaseAnalytics.getInstance(getContext()).logEvent(FirebaseGoogleAnalytics.Event.TECH_ITEM_INVENTORY, bundle);

        recyclerTouchListener = this;
        ((HomeActivityNew) getActivity()).mRelativeHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mLinearHeaderLayout.setVisibility(View.GONE);
        ((HomeActivityNew) getActivity()).mDatesRecyclerView.setVisibility(View.GONE);

      /*  mItemIssueListAsyncTasks = new ItemIssueListAsyncTasks(getContext(), BaseAsyncTask.Priority.LOW, this);
        mItemIssueListAsyncTasks.execute(SharedPrefManager.getInstance(getContext()).getUserId());*/

        getItemsIssueList(recyclerTouchListener);

        mRecyclerViewItemsList = mRootView.findViewById(R.id.recycler_item_list);
        return mRootView;
    }

    public void getItemsIssueList(RecyclerTouchListener recyclerTouchListener) {
        try {
            if (Connectivity.isNetworkAvailableRetro(getActivity())) {
                CommonFunction.showProgressDialog(getActivity());
                int userID = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
                Call<ItemIssueList> call = RetrofitClient.getInstance(getApplicationContext()).getMyApi().getItemIssueList(userID);
                call.enqueue(new retrofit2.Callback<ItemIssueList>() {
                    @Override
                    public void onResponse(Call<ItemIssueList> call, Response<ItemIssueList> response) {
                        try {
                            if (response.code() == 200) {
                                CommonFunction.hideProgressDialog(getActivity());
                                FWLogger.logInfo(TAG, "Get Item list");
                                ItemIssueList itemIssueList = response.body();
                                mItemsLists = new ArrayList<>();
                                mItemsLists = itemIssueList.getResultData();
                                mLayoutManager = new LinearLayoutManager(getActivity());
                                mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
                                mTechItemIssueListAdapter = new TechItemIssueListAdapterNew(getActivity(), mPosition, mItemsLists);
                                mTechItemIssueListAdapter.setClickListener(recyclerTouchListener);
                                mRecyclerViewItemsList.setAdapter(mTechItemIssueListAdapter);
                                setSearchFilter();
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    @Override
                    public void onFailure(Call<ItemIssueList> call, Throwable throwable) {
                        CommonFunction.hideProgressDialog(getActivity());
                        FWLogger.logInfo(TAG, "Exception in GetAssignedItemsListByUserId? API:");
                    }
                });
            } else {
                ServiceDialog serviceDialog = ServiceDialog.getInstance();
                serviceDialog.noConnectionDialogRetro(getActivity());
            }
        } catch (Exception ex) {
            FWLogger.logInfo(TAG, "Exception in GetAssignedItemsListByUserId? API:");
            ex.getMessage();
        }
    }

    @Override
    public void onTaskComplete(URLConnectionResponse urlConnectionResponse, String classType) {
        if (classType.equalsIgnoreCase(ItemIssueList.class.getSimpleName())) {
            FWLogger.logInfo(TAG, "Get Item list");
            Gson gson = new Gson();
            mItemsLists = new ArrayList<>();
            ItemIssueList itemIssueList = gson.fromJson(urlConnectionResponse.resultData, ItemIssueList.class);
            mItemsLists = itemIssueList.getResultData();
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewItemsList.setLayoutManager(mLayoutManager);
            mTechItemIssueListAdapter = new TechItemIssueListAdapterNew(getActivity(), mPosition, mItemsLists);
            mTechItemIssueListAdapter.setClickListener(this);
            mRecyclerViewItemsList.setAdapter(mTechItemIssueListAdapter);

            setSearchFilter();
        }
    }

    private void setSearchFilter() {
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = mRootView.findViewById(R.id.edittext_search);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnClickListener(this);
        // listening to search query text change
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mTechItemIssueListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mTechItemIssueListAdapter.getFilter().filter(query);
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchView.clearFocus();
                return false;
            }
        });
    }

    @Override
    public void onClick(View view, int position) {
//        ItemIssueList.ResultData issueDetails = mItemsLists.get(position);

        switch (view.getId()) {
            case R.id.edittext_search:
                mSearchView.setIconified(false);
                mSearchView.setFocusable(true);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        FWLogger.logInfo(TAG, "onDestroyView");
        CommonFunction.hideProgressDialog(getActivity());
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }
}
