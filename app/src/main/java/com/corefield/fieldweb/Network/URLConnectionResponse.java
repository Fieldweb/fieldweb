package com.corefield.fieldweb.Network;

/**
 * This class is mapped to get Common header of all API
 */
public class URLConnectionResponse {


    public int statusCode;
    public String resultData;
    public String message;
    //public Map<String, List<String>> headers;

    @Override
    public String toString() {
        return "UrlRequestResponse{" +
                "statusCode=" + statusCode +
                ", message=" + message +
                ", resultData='" + resultData + '\'' +
                '}';

//return resultData;
    }

}
