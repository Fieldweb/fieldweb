package com.corefield.fieldweb.Network;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.List;

/**
 * Network Controller for API Request
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Network class is used for API Request like DELETE,POST,ET
 */
public class Request {

    private String mURL = "";
    private JSONObject mData;
    private JSONArray mArrayData;
    private List<AbstractMap.SimpleEntry<String, String>> mHeader;
    private String mToken = "";

    public JSONArray getmArrayData() {
        return mArrayData;
    }

    public void setmArrayData(JSONArray mArrayData) {
        this.mArrayData = mArrayData;
    }

    public String getmURL() {
        return mURL;
    }

    public void setmURL(String mURL) {
        this.mURL = mURL;
    }

    public JSONObject getmData() {
        return mData;
    }

    public void setmData(JSONObject mData) {
        this.mData = mData;
    }

    public List<AbstractMap.SimpleEntry<String, String>> getmHeader() {
        return mHeader;
    }

    public void setmHeader(List<AbstractMap.SimpleEntry<String, String>> mHeader) {
        this.mHeader = mHeader;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }
}
