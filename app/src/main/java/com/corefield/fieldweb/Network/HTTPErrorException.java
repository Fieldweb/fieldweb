package com.corefield.fieldweb.Network;

/**
 * Network Controller  for HTTP Error Exception
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Network  class is used to check HTTP Error
 */
public class HTTPErrorException extends RuntimeException {

    private final int statusCode;

    public HTTPErrorException() {
        statusCode = -1;
    }

    public HTTPErrorException(int statusCode) {
        this.statusCode = statusCode;
    }

    public HTTPErrorException(int statusCode, String detailMessage) {
        super(detailMessage);
        this.statusCode = statusCode;
    }

    public HTTPErrorException(int statusCode, String message, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;

    }

    public HTTPErrorException(int statusCode, Throwable cause) {
        super(cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
