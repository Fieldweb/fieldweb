package com.corefield.fieldweb.Network;


import android.os.Build;

import com.corefield.fieldweb.Util.FWLogger;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import androidx.annotation.StringDef;

/**
 * URLConnectionRequest : Common class to request connection
 * Method : GET, DELETE, PUT,POST
 * OUTPUT : API Response
 * Exception : ALL
 */
public class URLConnectionRequest {


    protected static String TAG = URLConnectionRequest.class.getSimpleName();
    protected static int timeout = 300000;

    protected static boolean canceled;

    public static final String METHOD_GET = "GET";
    public static final String METHOD_DELETE = "DELETE";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";

    @StringDef({METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_DELETE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RequestMethod {
    }

    private List<HttpURLConnection> activeConnections = new ArrayList<HttpURLConnection>();

    public URLConnectionRequest() {
    }

    public void cancel() {
        canceled = true;

        for (HttpURLConnection connection : activeConnections) {
            try {
                connection.getInputStream().close();
                connection.disconnect();
            } catch (Exception ex) {
                //no op, we're forcefully closing connections
            }
        }
    }

    public void reset() {
        canceled = false;
    }

    public URLConnectionResponse sendGetRequest(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String query, String token) throws IOException {
        return sendRequest(METHOD_GET, url + query, headers, null, token);
    }

    public URLConnectionResponse sendGetRequestForCountry(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String token) throws IOException {
        return sendRequest(METHOD_GET, url, headers, null, token);
    }

    /*public URLConnectionResponse sendDeleteRequest(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String token) throws IOException {
        return sendRequest(METHOD_DELETE, url, headers, null, token);
    }*/

    public URLConnectionResponse sendDeleteRequest(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String query, String token) throws IOException {
        return sendRequest(METHOD_DELETE, url + query, headers, null, token);
    }

    public URLConnectionResponse sendPostRequest(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String data, String token) throws IOException {
        return sendRequest(METHOD_POST, url, headers, data, token);
    }

    public URLConnectionResponse sendPutRequest(String url, List<AbstractMap.SimpleEntry<String, String>> headers, String data, String token) throws IOException {
        return sendRequest(METHOD_PUT, url, headers, data, token);
    }

    private URLConnectionResponse sendRequest(@RequestMethod String requestMethod, String urlString, List<AbstractMap.SimpleEntry<String, String>> headers, String data, String token) throws IOException {
        long start = System.currentTimeMillis();

        FWLogger.logInfo(TAG, (String.format("Sending %s request: %s", requestMethod, urlString)));

        URLConnectionResponse response = new URLConnectionResponse();
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader reader = null;
        OutputStream outputStream = null;
        OutputStreamWriter outputStreamWriter = null;

        try {
            URL url = new URL(urlString);

            if (urlString.startsWith("https")) {
                //prod, stag and qa are all https
                connection = (HttpsURLConnection) url.openConnection();

            } else {
                //dev is not
                connection = (HttpURLConnection) url.openConnection();
            }

            activeConnections.add(connection);

            if (headers != null) {
                for (AbstractMap.SimpleEntry<String, String> header : headers) {
                    FWLogger.logInfo(TAG, "Header name  = " + header.getKey() + " Header value =  " + header.getValue() + "\n");
                    connection.addRequestProperty(header.getKey(), header.getValue());
                }
            }
            if (token != null && !token.equalsIgnoreCase("")) {
                connection.addRequestProperty("Authorization", token);
            }

            connection.setRequestMethod(requestMethod);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            if (data != null) {
                connection.setDoOutput(true);
                outputStream = connection.getOutputStream();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8.name());
                } else {
                    outputStreamWriter = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
                }
                outputStreamWriter.write(data);
                outputStream.flush();
                outputStreamWriter.close();
            }

            //response.headers = connection.getHeaderFields();

            //response.statusCode = 401;
            //response.message = "UnAuthorised";
            response.statusCode = connection.getResponseCode();//200
            response.message = connection.getResponseMessage();//ok

            StringBuilder responseBuilder = new StringBuilder();

            //status codes over 300 are "errors"
            inputStream = response.statusCode >= 300 ? connection.getErrorStream() : connection.getInputStream();

            if (inputStream != null) {

                //CharEncoding.UTF_8 is deprecated now (we can use StandardCharsets.UTF_8.name() but needs to increase min API level to 19)
                //Note : Charset.forName("UTF-8").name() is equals to StandardCharsets.UTF_8.name() and returns string
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8.name()));
                } else {
                    reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                }
                String line;
                while ((line = reader.readLine()) != null) {
                    if (canceled) break;
                    responseBuilder.append(line);
                }
            }

            if (responseBuilder.length() > 0) {//response.response = responseBuilder.toString();
                try {
                    JSONObject jsonObj = new JSONObject(responseBuilder.toString());
                    if (jsonObj != null) {
                        if (jsonObj.getString("Code").equalsIgnoreCase("999")) {
                            response.statusCode = 999;
                            response.message = jsonObj.getString("Message");
                            response.resultData = null;
                        } else if (jsonObj.getString("Code").equalsIgnoreCase("401")) {
                            response.statusCode = 401;
                            response.message = jsonObj.getString("Message");
                            response.resultData = null;
                        } else if (jsonObj.getString("Code").equalsIgnoreCase("888")) {
                            response.statusCode = 888;
                            response.message = jsonObj.getString("Message");
                            response.resultData = null;
                        } else {
                            response.resultData = responseBuilder.toString();
                        }
                    }
                } catch (JSONException jse) {
                    FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s", requestMethod, jse.getMessage()));
                }
            }

        } catch (MalformedURLException mue) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s", requestMethod, mue.getMessage()));
            throw mue;

        } catch (UnsupportedEncodingException uee) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s", requestMethod, uee.getMessage()));
            throw uee;

        } catch (ProtocolException pe) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s", requestMethod, pe.getMessage()));
            throw pe;

        } catch (IOException ioe) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s", requestMethod, ioe.getMessage()));
            throw ioe;

        } catch (HTTPErrorException ioe) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s Status Code : " + ioe.getStatusCode(), requestMethod, ioe.getMessage()));
            throw ioe;

        } catch (Exception ioe) {
            FWLogger.logInfo(TAG, String.format("Could not send %s request -- %s Status Code : " + ioe.getCause(), requestMethod, ioe.getMessage()));
            throw ioe;

        } finally {
            //No replacement for closeQuietly(InputStream) as of commons-io:commons-io:2.6 hence to close explicitly and use try-with-resources statement
            try {
                //noinspection deprecation
                IOUtils.closeQuietly(inputStream);
                //noinspection deprecation
                IOUtils.closeQuietly(reader);
                //noinspection deprecation
                IOUtils.closeQuietly(outputStream);
                //noinspection deprecation
                IOUtils.closeQuietly(outputStreamWriter);
            } catch (Exception e) {

            }

            if (connection != null) {
                connection.disconnect();
                activeConnections.remove(connection);
            }

            long end = System.currentTimeMillis();

            FWLogger.logInfo(TAG, String.format("Request completed in: ", requestMethod));
        }

        return response;
    }
}
