package com.corefield.fieldweb.Network;

import android.content.Context;

import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Network Controller for Request Building
 *
 * @author CoreField
 * @version 1.1
 * @implNote This Network class is used for building request - "Content-type", "application/json"
 */
public class RequestBuilder {

    /**
     * Use to build the request(with JSONObject)
     *
     * @param context
     * @param method method type GET/POST/PUT/DELETE
     * @param data JSONObject
     * @return Request string
     */
    private static final String TAG = RequestBuilder.class.getSimpleName();

    public static Request buildRequest(Context context, String method, JSONObject data) {

        Request request = new Request();

        if (!method.equalsIgnoreCase("")) {
            request.setmURL(URLConstant.Base.BASEURL + method);
            if (data != null) request.setmData(data);
            List<AbstractMap.SimpleEntry<String, String>> headers = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
            headers.add(new AbstractMap.SimpleEntry<String, String>("Content-type", "application/json"));
            if (method.equalsIgnoreCase(URLConstant.SignUp.GET_OTP) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_OWNER) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_TECH) || method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN)) {   //|| method.equalsIgnoreCase(URLConstant.Login.FORGOT_PASSWORD)
                //Do nothing with headers
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = NA" + "User ID = NA");
            } else if (method.equalsIgnoreCase(URLConstant.Login.FORGOT_PASSWORD)) {
                final String androidId = SharedPrefManager.getInstance(context).getAndroidID();
                FWLogger.logInfo(TAG, "FORGOT_PASSWORD_Request = " + request.getmURL() + "\nAndroid ID = " + androidId);
                if (androidId != null)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("AndroidID", androidId));
            } else {
                final String androidId = SharedPrefManager.getInstance(context).getAndroidID();
                final int userId = SharedPrefManager.getInstance(context).getUserId();
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = " + androidId + "User ID = " + userId);
                if (androidId != null)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("AndroidID", androidId));
                if (userId != 0)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("UserID", "" + userId));
            }
            request.setmHeader(headers);
            if (!method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN))
                request.setToken("Bearer " + SharedPrefManager.getInstance(context).getUserToken());
            //request.setToken("Bearer umfUC-MgQTCUt1V1HH1Mw5lN7OI77F70iV7LppK1wZzFDxTBLd8lzbgNe7yDuSbsl2P1hMGmSisiXqrDCnrgT5KKp6n5CLvNMhaLRSaXuyGs1wmNmMcnIKFYAL0TiaFYKrSxnk08kltxAvNphlhryIBsiV80WxzEnIb2PvneBrnvPzn7E2FxIGZggiC7tfPjGFu0i-cun75RpBXe7COX4F1ahiyN4LYyDHaJu9b8zao");
        }
        return request;
    }

    /**
     * Use to build the request(with JSONArray)
     *
     * @param context
     * @param method  method type GET/POST/PUT/DELETE
     * @param data    JSONArray
     * @return Request string
     */
    public static Request buildArrayRequest(Context context, String method, JSONArray data) {

        Request request = new Request();

        if (!method.equalsIgnoreCase("")) {
            request.setmURL(URLConstant.Base.BASEURL + method);
            if (data != null) request.setmArrayData(data);
            List<AbstractMap.SimpleEntry<String, String>> headers = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
            headers.add(new AbstractMap.SimpleEntry<String, String>("Content-type", "application/json"));
            if (method.equalsIgnoreCase(URLConstant.SignUp.GET_OTP) || method.equalsIgnoreCase(URLConstant.Login.FORGOT_PASSWORD) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_OWNER) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_TECH) || method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN)) {
                //Do nothing with headers
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = NA" + "User ID = NA");
            } else {
                final String androidId = SharedPrefManager.getInstance(context).getAndroidID();
                final int userId = SharedPrefManager.getInstance(context).getUserId();
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = " + androidId + "User ID = " + userId);
                if (androidId != null)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("AndroidID", androidId));
                if (userId != 0)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("UserID", "" + userId));
            }
            request.setmHeader(headers);
            if (!method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN))
                request.setToken("Bearer " + SharedPrefManager.getInstance(context).getUserToken());
            //request.setToken("Bearer umfUC-MgQTCUt1V1HH1Mw5lN7OI77F70iV7LppK1wZzFDxTBLd8lzbgNe7yDuSbsl2P1hMGmSisiXqrDCnrgT5KKp6n5CLvNMhaLRSaXuyGs1wmNmMcnIKFYAL0TiaFYKrSxnk08kltxAvNphlhryIBsiV80WxzEnIb2PvneBrnvPzn7E2FxIGZggiC7tfPjGFu0i-cun75RpBXe7COX4F1ahiyN4LYyDHaJu9b8zao");
        }
        return request;
    }

    public static Request TeleCMIbuildArrayRequest(Context context, String method, JSONArray data) {

        Request request = new Request();

        if (!method.equalsIgnoreCase("")) {
            /*request.setmURL("https://piopiy.telecmi.com/v1" + method);*/
            request.setmURL("https://rest.telecmi.com/v2" + method);
            if (data != null) request.setmArrayData(data);
            List<AbstractMap.SimpleEntry<String, String>> headers = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
            headers.add(new AbstractMap.SimpleEntry<String, String>("Content-type", "application/json"));
            if (method.equalsIgnoreCase(URLConstant.SignUp.GET_OTP) || method.equalsIgnoreCase(URLConstant.Login.FORGOT_PASSWORD) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_OWNER) || method.equalsIgnoreCase(URLConstant.Login.URL_REGISTER_TECH) || method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN)) {
                //Do nothing with headers
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = NA" + "User ID = NA");
            } /*else {
                final String androidId = SharedPrefManager.getInstance(context).getAndroidID();
                final int userId = SharedPrefManager.getInstance(context).getUserId();
                FWLogger.logInfo(TAG, "Request = " + request.getmURL() + "\nAndroid ID = " + androidId + "User ID = " + userId);
                if (androidId != null)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("AndroidID", androidId));
                if (userId != 0)
                    headers.add(new AbstractMap.SimpleEntry<String, String>("UserID", "" + userId));
            }*/
            request.setmHeader(headers);
            if (!method.equalsIgnoreCase(URLConstant.Login.URL_LOGIN))
                request.setToken("Bearer " + SharedPrefManager.getInstance(context).getUserToken());
            //request.setToken("Bearer umfUC-MgQTCUt1V1HH1Mw5lN7OI77F70iV7LppK1wZzFDxTBLd8lzbgNe7yDuSbsl2P1hMGmSisiXqrDCnrgT5KKp6n5CLvNMhaLRSaXuyGs1wmNmMcnIKFYAL0TiaFYKrSxnk08kltxAvNphlhryIBsiV80WxzEnIb2PvneBrnvPzn7E2FxIGZggiC7tfPjGFu0i-cun75RpBXe7COX4F1ahiyN4LYyDHaJu9b8zao");
        }
        return request;
    }


}
