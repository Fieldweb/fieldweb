package com.corefield.fieldweb.Retrofit;

import android.content.Context;

import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance = null;
    private Api myApi;
    static Context mContext;
    //private String authToken = "Bearer" + " " + "gwpQ93d3nlHoOWrGPTzLyQD7Ea05T4I2ibZWCpoftadt_UKxeQTED6vXPP5LYT29zKVFxhXjjhMzPEfVSEWL-kmUyaeYQAqOxDWw6DMpHe-d422VBsAWkCYRO256xdS5BCc6RUaQIw9Y6KS--mLxJgRW7-nqreCY7Ga77GNg1taoJ2O41rxAbcaOjQmKGSyEf6Oyau4EZj_DyhR3Q07BejhQJNc3STgNQj540hv7ULQ";

    private RetrofitClient() {

        /*OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();*/
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().protocols(Util.immutableListOf(Protocol.HTTP_1_1))
                .build().newBuilder();
        Interceptor headerAuthorizationInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (request.method().equalsIgnoreCase("POST")) {
                    request = chain.request().newBuilder().addHeader("Content-Type", "application/json").build();
                } else {
                    request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + SharedPrefManager.getInstance(mContext).getUserToken())
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json").build();
                }
                return chain.proceed(request);

            }
        };
        clientBuilder.addInterceptor(headerAuthorizationInterceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLConstant.Base.BASEURL)
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build();

        myApi = retrofit.create(Api.class);
    }

    public static synchronized RetrofitClient getInstance(Context mContextt) {
        if (instance == null) {
            instance = new RetrofitClient();
            mContext = mContextt;
        }
        return instance;
    }

    public Api getMyApi() {
        return myApi;
    }
}
