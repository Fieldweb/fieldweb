package com.corefield.fieldweb.Retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ApiClientForOpenAI {
    private static final String BASE_URL = "https://api.openai.com/v1/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json")
        @POST("completions")
        Call<ResponseBody> getAnswer(@Header("Authorization") String apiKey, @Body RequestBody request);
    }

}
