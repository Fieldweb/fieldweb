package com.corefield.fieldweb.Retrofit;


import com.corefield.fieldweb.DTO.AMC.AMCDashboardCount;
import com.corefield.fieldweb.DTO.AMC.AMCDetails;
import com.corefield.fieldweb.DTO.AMC.AMCList;
import com.corefield.fieldweb.DTO.AMC.AMCTypeList;
import com.corefield.fieldweb.DTO.AMC.AddAMC;
import com.corefield.fieldweb.DTO.AMC.ReminderModeList;
import com.corefield.fieldweb.DTO.AMC.ServiceOccurrenceList;
import com.corefield.fieldweb.DTO.Account.DeleteInvoiceDetailsDTO;
import com.corefield.fieldweb.DTO.Account.DeleteQuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.DownloadInvoicePdfDTO;
import com.corefield.fieldweb.DTO.Account.DownloadQuotationPdfDTO;
import com.corefield.fieldweb.DTO.Account.InvoiceDetailsDTO;
import com.corefield.fieldweb.DTO.Account.InvoiceFollowUpNotesDTO;
import com.corefield.fieldweb.DTO.Account.InvoiceListDTO;
import com.corefield.fieldweb.DTO.Account.InvoicePaymentStatusDTO;
import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Account.QuotationListDTO;
import com.corefield.fieldweb.DTO.Account.QuoteBindListDTO;
import com.corefield.fieldweb.DTO.Account.SaveInvoicePaymentAmountDetailsDTO;
import com.corefield.fieldweb.DTO.Account.SaveQuotationDTO;
import com.corefield.fieldweb.DTO.Account.TaxDetails;
import com.corefield.fieldweb.DTO.Account.UpdateQuotationStatusDTO;
import com.corefield.fieldweb.DTO.Attendance.CheckOut;
import com.corefield.fieldweb.DTO.Attendance.OwnDateAttendance;
import com.corefield.fieldweb.DTO.Attendance.TechMonthlyAttendance;
import com.corefield.fieldweb.DTO.AttendanceCheck;
import com.corefield.fieldweb.DTO.CRMTask.AddCustDTO;
import com.corefield.fieldweb.DTO.CRMTask.CRMAMCkList;
import com.corefield.fieldweb.DTO.CRMTask.CRMTaskList;
import com.corefield.fieldweb.DTO.CountryList.GetCountryList;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Delete.DeleteOwnerTechnician;
import com.corefield.fieldweb.DTO.Enquiry.AddEnquiry;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.DTO.Enquiry.ReferenceType;
import com.corefield.fieldweb.DTO.Enquiry.ServiceType;
import com.corefield.fieldweb.DTO.Expenditure.AddCredit;
import com.corefield.fieldweb.DTO.Expenditure.AddExpense;
import com.corefield.fieldweb.DTO.Expenditure.DeductBalance;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseDetails;
import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.DTO.FWUser;
import com.corefield.fieldweb.DTO.Item.AddItem;
import com.corefield.fieldweb.DTO.Item.AddDeductUsedItem;
import com.corefield.fieldweb.DTO.Item.AllItemIssueList;
import com.corefield.fieldweb.DTO.Item.DeleteItemPortal;
import com.corefield.fieldweb.DTO.Item.EditItem;
import com.corefield.fieldweb.DTO.Item.GetUsedItemList;
import com.corefield.fieldweb.DTO.Item.IssueItem;
import com.corefield.fieldweb.DTO.Item.ItemIssueList;
import com.corefield.fieldweb.DTO.Item.ItemUnitType;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Item.TechWiseItemList;
import com.corefield.fieldweb.DTO.Item.UnAssignItem;
import com.corefield.fieldweb.DTO.LeadManagement.AddCustomerLeadInternalDTO;
import com.corefield.fieldweb.DTO.LeadManagement.DeleteLeadDetailsDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadDetailsDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadListDTO;
import com.corefield.fieldweb.DTO.LeadManagement.LeadStatusListDTO;
import com.corefield.fieldweb.DTO.Notification.Notification;
import com.corefield.fieldweb.DTO.Notification.RegisterDeviceId;
import com.corefield.fieldweb.DTO.Passbook.EarningDashboard;
import com.corefield.fieldweb.DTO.Passbook.MonthlyPassbook;
import com.corefield.fieldweb.DTO.Passbook.TodayPassbook;
import com.corefield.fieldweb.DTO.Passbook.YearlyPassbook;
import com.corefield.fieldweb.DTO.RegisterHere.RegisterOwner;
import com.corefield.fieldweb.DTO.RegisterHere.TouchlessTempRegistration;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.Task.AddPhotoBeforeTask;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.DownloadReport;
import com.corefield.fieldweb.DTO.Task.OnHoldTaskDetails;
import com.corefield.fieldweb.DTO.Task.ReassignTask;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TaskStatusCount;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.DTO.Task.UpdateTask;
import com.corefield.fieldweb.DTO.Task.UpdateTaskStatus;
import com.corefield.fieldweb.DTO.Task.UpdateTaskWithEarnedAmount;
import com.corefield.fieldweb.DTO.User.AddAttendance;
import com.corefield.fieldweb.DTO.User.AddBulkTech;
import com.corefield.fieldweb.DTO.User.AuthMobileEmail;
import com.corefield.fieldweb.DTO.User.LanguageList;
import com.corefield.fieldweb.DTO.User.TechnicianList;
import com.corefield.fieldweb.DTO.User.UpdateUser;
import com.corefield.fieldweb.DTO.User.UserDetails;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.DTO.UserDisclaimer.AcceptDisclaimer;
import com.corefield.fieldweb.DTO.UserDisclaimer.GetUserDisclaimer;
import com.corefield.fieldweb.Network.URLConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api {

    @GET(URLConstant.Users.GET_ALL_USER_LIST)
    Call<UsersList> getAllUsersList(@Query("OwnerId") int OwnerId);

    @GET(URLConstant.Users.GET_USER_DETAILS)
    Call<UserDetails> getUserDetails(@Query("UserId") int UserId);

    @GET(URLConstant.CustomerEnquiry.GET_REFERENCE_TYPE)
    Call<ReferenceType> getReferenceType();

    @GET(URLConstant.CustomerEnquiry.GET_SERVICE_TYPE)
    Call<ServiceType> getServiceType();

    @GET(URLConstant.Item.GET_ITEM_UNIT_TYPE)
    Call<ItemUnitType> getItemUnitType();

    @GET(URLConstant.Expenditure.GET_EXPENSE_USER_LIST)
    Call<ExpenseTechList> getTechnicianList(@Query("OwnerId") int OwnerId);

    @GET(URLConstant.Passbook.GET_TODAY_PASSBOOK)
    Call<TodayPassbook> getTodayPassbookDetails(@Query("UserId") int userId);

    @GET(URLConstant.Passbook.GET_YEARLY_PASSBOOK)
    Call<YearlyPassbook> getYearlyPassbookDetails(@Query("UserId") int userId, @Query("PassbookYear") int passbookYear);

    @GET(URLConstant.Passbook.GET_MONTHLY_PASSBOOK)
    Call<MonthlyPassbook> getMonthlyPassbookDetails(@Query("UserId") int userId, @Query("PassbookMonth") int passbookMonth, @Query("PassbookYear") int passbookYear);

    @GET(URLConstant.Customer.GET_CUSTOMER_LIST)
    Call<CustomerList> getCustomerList(@Query("UserId") int UserId);

    @GET(URLConstant.Disclaimer.GET_USER_DISCLAIMER)
    Call<GetUserDisclaimer> getUserDisclaimer(@Query("UserId") int UserId);

    @GET(URLConstant.Attendance.ATTENDANCE_CHECK)
    Call<AttendanceCheck> getAttendanceCheck(@Query("UserId") int UserId);

    @GET(URLConstant.AMC.GET_SERVICE_OCCURRENCE_LIST)
    Call<ServiceOccurrenceList> getServiceOccurrenceTask();

    @GET(URLConstant.CountryList.GET_COUNTRY_LIST)
    Call<GetCountryList> getCountryList();

    @GET(URLConstant.AMC.GET_REMINDER_MODE_LIST)
    Call<ReminderModeList> getReminderModeTask();

    @GET(URLConstant.Item.GET_ALL_ITEM_ASSIGNED_UNASSIGNED)
    Call<ItemsList> getItemList(@Query("OwnerId") int OwnerId);

    @GET(URLConstant.AMC.GET_CRM_AMCLIST)
    Call<CRMAMCkList> getCRMAMCList(@Query("UserId") int OwnerId, @Query("CustomerDetailsId") int CustomerDetailsId, @Query("pageIndex") int pageIndex);

    @GET(URLConstant.AMC.GET_AMC_REPORT_DETAILS)
    Call<AMCDetails> getAMCDetails(@Query("OwnerId") int OwnerId, @Query("AMCsId") int CustomerDetailsId, @Query("AMCServiceDetailsId") int amcServiceDetailsId);

    @GET(URLConstant.Report.DOWNLOAD_AMC_REPORT)
    Call<DownloadReport> getDownloadAMCReport(@Query("AmcsId") int amcsId, @Query("UserId") int userId);

    @GET(URLConstant.Report.DOWNLOAD_REPORT)
    Call<DownloadReport> getDownloadTaskReport(@Query("UserId") int userId, @Query("TaskID") int tasId);

    @GET(URLConstant.Login.USER_PREFER_LANGUAGE)
    Call<LanguageList> getPreferredLanguage(@Query("userId") int userId, @Query("PreferredLanguage") String preferredLanguage);

    @GET(URLConstant.Users.AUTHENTICATE_MOBILE)
    Call<AuthMobileEmail> getMobileOTPVerified(@Query("MobileNo") String mobileNo, @Query("UserId") int userId);

    @GET(URLConstant.Users.AUTHENTICATE_EMAIL)
    Call<AuthMobileEmail> getEmailOTPVerified(@Query("EmailId") String EmailId, @Query("UserId") int UserId);

    @GET(URLConstant.Task.GET_TODAY_TASK_LIST)
    Call<TasksList> getTodayTaskList(@Query("UserId") int userId, @Query("pageIndex") int pageIndex, @Query("AllData") boolean isAllData);

    @GET(URLConstant.Dashboard.GET_TASK_STATUS_COUNT_OWN_NEW)
    Call<TaskStatusCount> getTaskStatusCount(@Query("OwnerId") int userId, @Query("Flag") String flag);

    @GET(URLConstant.Task.GET_CRM_TASKLIST)
    Call<CRMTaskList> getCRMTaskList(@Query("UserId") int userId, @Query("searchparam") String searchparam, @Query("TaskStatusID") int TaskStatusID, @Query("TaskTypeID") int TaskTypeID, @Query("pageIndex") int pageIndex, @Query("TaskMonth") int TaskMonth, @Query("TaskYear") int TaskYear, @Query("CustomerDetailsId") int CustomerDetailsId);

    @GET(URLConstant.CustomerEnquiry.GET_ENQUIRY_LIST)
    Call<EnquiryList> getEnquiryList(@Query("UserId") int UserId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Notification.GET_NOTIFICATION_LIST)
    Call<Notification> getsyncNotification(@Query("UserId") int UserId);

    @GET(URLConstant.Users.GET_PROFILE_DETAILS)
    Call<UserDetails> getProfileDetails(@Query("UserId") int userId);

    @GET(URLConstant.Users.DOWNLOAD_TECH_LIST)
    Call<TechnicianList> getDownloadTechList(@Query("OwnerId") int userId);

    @GET(URLConstant.Item.GET_ALL_ITEM_ISSUE_LIST)
    Call<AllItemIssueList> getAllItemIssueList(@Query("OwnerId") int userId);

    @GET(URLConstant.Item.GET_ITEM_ISSUE_LIST_BY_USERID)
    Call<ItemIssueList> getItemIssueList(@Query("UserId") int userId);

    @GET(URLConstant.Item.GET_ALL_ASSIGN_ITEMLIST_TECHWISE)
    Call<TechWiseItemList> getAllAssignedItemsList(@Query("itemId") int itemId, @Query("OwnerId") int OwnerId);

    @GET(URLConstant.Item.GET_DELETE_ITEM_PORTAL)
    Call<DeleteItemPortal> getDeleteItemPortal(@Query("Id") int itemId);

    @GET(URLConstant.Task.GET_TASK_BY_ID)
    Call<TasksList> getTaskDetail(@Query("UserId") int userId, @Query("TaskID") int TaskId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Item.GET_USED_ITEMLIST)
    Call<GetUsedItemList> getUsedItemList(@Query("itemId") int itemId, @Query("OwnerId") int OwnerId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Task.GET_TASK_CLOSURE_DETAILS)
    Call<TaskClosure> getTaskClosureDetail(@Query("UserId") int userId, @Query("TaskID") int TaskId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Attendance.GET_ATTENDANCE_TECH_MONTHLY)
    Call<TechMonthlyAttendance> getTechAttendance(@Query("UserId") int userId, @Query("Dates") String date);

    @GET(URLConstant.Attendance.GET_ATTENDANCE_OWNER_MONTHLY)
    Call<OwnDateAttendance> getAttendanceMonthlyForAdmin(@Query("OwnerId") int userId, @Query("Sdate") String date);

    @GET(URLConstant.Passbook.GET_PASSBOOK_FOR_DASHBOARD)
    Call<EarningDashboard> getPassbookDetailsForDashboard(@Query("UserId") int userId, @Query("InputFilter") String inputFilter);

    @GET(URLConstant.AMC.GET_AMC_DASHBOARD_COUNT_DETAILS)
    Call<AMCDashboardCount> getAMCDashboardCountDetails(@Query("OwnerId") int userId, @Query("InputFilter") String inputFilter);

    @GET(URLConstant.Expenditure.GET_EXPENDITURE_DETAILS)
    Call<ExpenseDetails> getExpenditureDetails(@Query("UserId") int userId, @Query("ExpsDate") String date);

    @GET(URLConstant.AMC.GET_AMC_TYPE_LIST)
    Call<AMCTypeList> getAMCTypes();

    @GET(URLConstant.AMC.GET_AMC_SERVICE_MONTH_LIST)
    Call<AMCList> getAMCList(@Query("OwnerId") int ownerId, @Query("Date") String selectedDate, @Query("AMCTypeId") int selectedType);

    @GET(URLConstant.Task.GET_TASK_LIST_SEARCH_NEW)
    Call<TasksList> getTaskListSearch(@Query("UserId") int userId, @Query("searchparam") String searchparam, @Query("TaskStatusID") int statusId, @Query("TaskTypeID") int taskTypeId, @Query("pageIndex") int pageIndex, @Query("TaskMonth") int month, @Query("TaskYear") int TaskYear, @Query("AllData") boolean isAllData);

    @GET(URLConstant.Users.DELETE_ACCOUNT)
    Call<DeleteOwnerTechnician> getDeleteOwnAccount(@Query("UserId") int userId);

    /*SERVICE MANAGEMENT*/
    @GET(URLConstant.Services.GET_ServiceTypeList)
    Call<ServiceTypeListDTO> getServiceTypeList(@Query("OwnerId") int OwnerId, @Query("SearchParam") String searchparam, @Query("pageIndex") int pageIndex, @Header("Authorization") String authHeader);

    @GET(URLConstant.Services.GET_DELETE_SERVICE)
    Call<ServiceTypeListDTO> getDeleteService(@Query("OwnerId") int OwnerId, @Query("SId") int serviceID, @Header("Authorization") String authHeader);

    @GET(URLConstant.Services.GET_EnquiryServiceTypeList)
    Call<EnquiryServiceTypeDTO> getEnquiryServiceList(@Query("OwnerId") int OwnerId, @Header("Authorization") String authHeader);


    /*LEAD MANAGEMENT*/
    @GET(URLConstant.Lead.GET_ALL_LEADList)
    Call<LeadListDTO> getAllLeadList(@Query("UserId") int UserId, @Query("pageIndex") int pageIndex, @Query("SearchParam") String SearchParam, @Query("LeadStatusId") int LeadStatusId, @Header("Authorization") String authHeader);

    /*LEAD MANAGEMENT DETAILS*/
    @GET(URLConstant.Lead.GET_LEADETAILS_BY_LEAD_ID)
    Call<LeadDetailsDTO> getLeadDetailsById(@Query("UserId") int UserId, @Query("LeadId") int leadId, @Header("Authorization") String authHeader);


    /*ACCOUNT MANAGEMENT : QUOTATION LIST*/
    @DELETE(URLConstant.Accounts.DELETE_QUOTATION_DETAILS)
    Call<DeleteQuotationDetailsDTO> getDeleteQuotationDetails(@Query("Id") int quoteId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_TAX_LIST)
    Call<TaxDetails> getTaxDetails();

    @GET(URLConstant.Accounts.GET_QuotationList)
    Call<QuotationListDTO> getQuotationList(@Query("UserId") int UserId, @Query("PageSize") int PageSize, @Query("Pagenum") int Pagenum, @Query("StatusId") int StatusId, @Query("SearchParam") String SearchParam, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_QUOTATIONDETAILS_BY_QUOTE_ID)
    Call<QuotationDetailsDTO> getQuotationDetailsById(@Query("Id") int quoteId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);

    /*INVOICE LIST*/
    @GET(URLConstant.Accounts.GET_INVOICE_LIST)
    Call<InvoiceListDTO> getInvoiceList(@Query("UserId") int UserId, @Query("PageSize") int PageSize, @Query("Pagenum") int Pagenum, @Query("SearchParam") String SearchParam, @Query("StatusId") int StatusId, @Header("Authorization") String authHeader);

    @DELETE(URLConstant.Accounts.DELETE_INVOICE_DETAILS)
    Call<DeleteInvoiceDetailsDTO> getDeleteInvoiceDetails(@Query("Id") int invoiceId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_INVOICE_PAYMENT_STATUS)
    Call<InvoicePaymentStatusDTO> getInvoicePaymentStatus(@Query("Id") int invoiceId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Accounts.SAVE_INVOICE_PAYMMENT_DETAILS)
    Call<SaveInvoicePaymentAmountDetailsDTO> updatePayment(@Field("Id") int Id, @Field("InvoiceId") int InvoiceId, @Field("Amount") int amount, @Field("CreatedBy") int CreatedBy, @Field("UserId") int UserId, @Field("PaymentTransactionType") String paymentTransactionType, @Field("IsActive") boolean isActiveFlag, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Accounts.INVOICE_FOLLOW_UP_NOTES)
    Call<InvoiceFollowUpNotesDTO> followUpNotes(@Field("InvoiceId") int InvoiceId, @Field("StatuiId") int StatusId, @Field("UserId") int UserId, @Field("FolowUpDate") String FollowUpDate, @Field("Notes") String Notes, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_INVOICEDETAILS_BY_INVOICE_ID)
    Call<InvoiceDetailsDTO> getInvoiceDetailsById(@Query("InvoiceId") int invoiceId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);


    @GET(URLConstant.Lead.GET_LEADSTATUS_List)
    Call<LeadStatusListDTO> getLeadStatusList();

    @GET(URLConstant.Lead.DELETE_LEAD_DETAILS)
    Call<DeleteLeadDetailsDTO> getDeleteLead(@Query("UserId") int UserId, @Query("Id") int Id, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_QUOTATION_PDF)
    Call<DownloadQuotationPdfDTO> getQuotationPdf(@Query("Id") int quoteId, @Query("UserId") int UserId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_INVOICE_PDF)
    Call<DownloadInvoicePdfDTO> getInvoicePdf(@Query("UserId") int UserId, @Query("invoiceId") int invoiceId, @Header("Authorization") String authHeader);

    @GET(URLConstant.Accounts.GET_QUOTE_BIND_LIST)
    Call<QuoteBindListDTO> getQuoteBindList(@Query("UserId") int UserId, @Header("Authorization") String authHeader);

    /*============================ POST API ==================*/
    @FormUrlEncoded
    @POST(URLConstant.Login.URL_LOGIN)
    Call<FWUser> doTouchlessLogin(@Field("UserName") String userName, @Field("Password") String password, @Field("AndroidID") String androidId, @Field("UserPreferredLanguage") String preferredLang);

    @FormUrlEncoded
    @POST(URLConstant.SignUp.GET_OTP_REGISTER)
    Call<TouchlessTempRegistration> doTouchlessSignUp(@Field("EmailOrContactNo") String EmailOrContactNo, @Field("CountryDetailsId") int CountryDetailsId);

    @FormUrlEncoded
    @POST(URLConstant.Notification.REGISTER_DEVICE_ID)
    Call<RegisterDeviceId> doSendRegistrationToServer(@Field("ToInstanceDeviceId") String toInstanceDeviceId, @Field("UserDeviceId") String userDeviceId, @Field("UserId") int userId, @Field("CreatedBy") int createdBy);

    @POST(URLConstant.Attendance.ADD_ATTENDANCE)
    Call<AddAttendance> postAttendance(@Body AddAttendance.ResultData addAttendance, @Header("Authorization") String authHeader);

    @POST(URLConstant.CustomerEnquiry.ADD_ENQUIRY)
    Call<AddEnquiry> saveAddEnquiry(@Body AddEnquiry.ResultData addEnquiry, @Header("Authorization") String authHeader);

    @POST(URLConstant.Customer.ADD_CUSTOMER)
    Call<AddCustDTO> addCustomerDetails(@Body AddCustDTO.ResultData addCustomer, @Header("Authorization") String authHeader);

    @POST(URLConstant.Customer.UPDATE_CUSTOMER_DETAILS)
    Call<AddCustDTO> UpdateCustomerDetails(@Body AddCustDTO.ResultData addCustomer, @Header("Authorization") String authHeader);

    @POST(URLConstant.Item.ADD_USED_ITEM)
    Call<AddDeductUsedItem> addUsedItem(@Body AddDeductUsedItem addUsedItem, @Header("Authorization") String authHeader);

    @POST(URLConstant.Item.DEDUCT_USED_ITEM)
    Call<AddDeductUsedItem> deductUsedItem(@Body AddDeductUsedItem addUsedItem, @Header("Authorization") String authHeader);


    @POST(URLConstant.Item.ISSUE_ITEM)
    Call<IssueItem> saveIssuedItem(@Body IssueItem.ResultData issuItem, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Item.RETURN_ITEM)
    Call<UnAssignItem> saveReturnItem(@Field("UserId") int userId, @Field("ItemId") int itemId, @Field("Quantity") int quantity, @Field("ItemIssuedId") int itemIssuedId, @Field("TaskId") int taskId, @Field("CreatedBy") int createdBy, @Field("Notes") String notes, @Header("Authorization") String authHeader);

    @POST(URLConstant.Item.ADD_ITEM)
    Call<AddItem> saveAddItem(@Body AddItem.Resultdata addItem, @Header("Authorization") String authHeader);

    @POST(URLConstant.Item.EDIT_ITEM)
    Call<EditItem> updateItem(@Body EditItem.ResultData editItem, @Header("Authorization") String authHeader);

    @POST(URLConstant.Task.ADD_TASK)
    Call<AddTask> saveAddTask(@Body AddTask.ResultData addTask, @Header("Authorization") String authHeader);

    @POST(URLConstant.Disclaimer.ACCEPT_DISCLAIMER)
    Call<AcceptDisclaimer> acceptDisclaimer(@Body AcceptDisclaimer.ResultData acceptDisc, @Header("Authorization") String authHeader);

    @POST(URLConstant.AMC.ADD_AMC)
    Call<AddAMC> addAMCDetails(@Body AddAMC addAMC, @Header("Authorization") String authHeader);

    @POST(URLConstant.SignUp.GET_REGISTER)
    Call<RegisterOwner> registerOwner(@Body RegisterOwner.ResultData registerOwner);

    @POST(URLConstant.Task.ADD_TASK_CLOSURE)
    Call<TaskClosure> updateTaskClosure(@Body TaskClosure.ResultData taskClosure, @Header("Authorization") String authHeader);

    @POST(URLConstant.Task.REASSIGN_TASK)
    Call<ReassignTask> reAssignTask(@Body ReassignTask.ResultData reassignTask, @Header("Authorization") String authHeader);

    @POST(URLConstant.Users.ADD_BULK_USERS)
    Call<AddBulkTech> addBulkTech(@Body List<AddBulkTech.ResultData> addbulTech, @Header("Authorization") String authHeader);

    @POST(URLConstant.Users.ADD_BULK_USERS_VALIDATION)
    Call<AddBulkTech> addBulkTechValidation(@Body List<AddBulkTech.ResultData> addbulTechvalidation, @Header("Authorization") String authHeader);

    @POST(URLConstant.Task.UPDATE_TASK)
    Call<UpdateTask> updateTask(@Body UpdateTask.ResultData updateTask, @Header("Authorization") String authHeader);

    @POST(URLConstant.Users.UPDATE_USER)
    Call<UpdateUser> updateUser(@Body UpdateUser.ResultData updateUser, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Attendance.ATTENDANCE_CHECK_OUT)
    Call<CheckOut> checkOut(@Field("UserId") int userId, @Field("Latitude") double latitude, @Field("Longitude") double longitude, @Field("CheckOutPlace") String checkoutPlace, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Passbook.UPDATE_TASK_WITH_EARNED_AMOUNT)
    Call<UpdateTaskWithEarnedAmount> updateTaskWithEarnedAmt(@Field("EarningAmount") int earnedAmount, @Field("TaskId") int taskId, @Field("UserId") int userId, @Field("TaskState") int taskstate, @Field("TaskStatus") int statusId, @Field("PaymentTransactionType") String PaymentTransactionType, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Expenditure.UPDATE_ADD_CREDIT)
    Call<AddCredit> addCredit(@Field("Amount") int amount, @Field("ReceivedBy") int receivedBy, @Field("GivenBy") int givenBy, @Field("CredidDescription") String creditDesc, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Expenditure.UPDATE_DEDUCT_BALANCE)
    Call<DeductBalance> deductBalance(@Field("Amount") int amount, @Field("UserId") int userId, @Field("DeductBy") int deductBy, @Field("Description") String description, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Expenditure.UPDATE_ADD_EXPENSE)
    Call<AddExpense> addExpense(@Field("Amount") int amount, @Field("ExpenseName") String expenseName, @Field("UserId") int userId, @Field("ExpensePhoto") String expensePhoto, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Task.ADD_PHOTO_BEFORE_TASK)
    Call<AddPhotoBeforeTask> addPhotoBeforeTask(@Field("DeviceInfoImageName") String deviceInfoImageName, @Field("DeviceInfoNotes") String deviceInfoNotes, @Field("CreatedBy") int createdBy, @Field("DeviceInfoImagePath") String deviceInfoImagePath, @Field("DeviceInfoImagePath1") String deviceInfoImagePath1, @Field("DeviceInfoImagePath2") String deviceInfoImagePath2, @Field("TaskId") int taskId, @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST(URLConstant.Task.UPDATE_TASK_STATUS)
    Call<UpdateTaskStatus> updateTaskStatus(@Field("UserId") int userId, @Field("TaskId") int taskId, @Field("TaskStatus") int taskStatus, @Field("Time") long time, @Field("TaskState") int taskState, @Field("TotalDistance") String totalDistance, @Field("RejectedTaskNotes") String rejectedTaskNotes, @Header("Authorization") String authHeader);


    @POST(URLConstant.Task.POST_OnHoldTASKDetails)
    Call<OnHoldTaskDetails> postOnHoldTaskStatus(@Body OnHoldTaskDetails updateUser, @Header("Authorization") String authHeader);

    // POST SERVICE TYPE DATA
    @POST(URLConstant.Services.POST_ServiceTypeList)
    Call<ServiceTypeListDTO> addServiceType(@Body ServiceTypeListDTO.ResultData serResultData, @Header("Authorization") String authHeader);

    @POST(URLConstant.Services.UPDATE_ServiceTypeList)
    Call<ServiceTypeListDTO> updateServiceType(@Body ServiceTypeListDTO.ResultData serResultData, @Header("Authorization") String authHeader);


    // POST EXTERNAL LEAD FORM DATA
    @POST(URLConstant.Lead.POST_EXTERNAL_LEAD_FORM)
    Call<AddCustomerLeadInternalDTO> addExternalLeadForm(@Body AddCustomerLeadInternalDTO.ResultData resultData, @Header("Authorization") String authHeader);

    // UPDATE EXTERNAL LEAD FORM DATA
    @POST(URLConstant.Lead.UPDATE_LEAD_STATUS)
    Call<AddCustomerLeadInternalDTO> updateLeadStatus(@Body AddCustomerLeadInternalDTO.ResultData resultData, @Header("Authorization") String authHeader);


    // SAVE QUOTATION DETAILS
    @POST(URLConstant.Accounts.POST_QuotationDetails)
    Call<SaveQuotationDTO> saveQuotationDetails(@Body SaveQuotationDTO saveQuotation,
                                                @Header("Authorization") String authHeader);

    @POST(URLConstant.Accounts.UPDATE_QuotationDetails)
    Call<SaveQuotationDTO> updateQuotationDetails(@Body SaveQuotationDTO saveQuotation, @Header("Authorization") String authHeader);


    // UPDATE QUOTATION STATUS
    @FormUrlEncoded
    @POST(URLConstant.Accounts.UPDATE_QUOTATION_STATUS)
    Call<UpdateQuotationStatusDTO> updateQuotationStatus(@Field("QuotationId") int quoteId, @Field("UserId") int UserId, @Field("StatusId") int StatusId, @Field("Notes") String Notes, @Field("Authorization") String authHeader);




   /* @FormUrlEncoded
    @POST(URLConstant.Services.UPDATE_ServiceTypeList)
    Call<ServiceTypeListDTO> updateServiceType(@Field("ServiceName") String serviceName,
                                               @Field("Price") int price,
                                               @Field("ContactNo") String contactNo,
                                               @Field("Description") String description,
                                               @Field("UpdatedBy") int updatedBy,
                                               @Field("CreatedBy") int createdBy,
                                               @Field("Id") int id,
                                               @Header("Authorization") String authHeader);*/


    /*@POST(URLConstant.Login.URL_LOGIN)
    Call<FWUser> doTouchlessLogin(@Body FWUser user);*/

}