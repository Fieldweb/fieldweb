package com.corefield.fieldweb.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.corefield.fieldweb.Util.Constant;

public class AlarmReceiver extends BroadcastReceiver {
    int batteryStatus = 0;
    boolean isGPSStatus = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Context mContext = context.getApplicationContext();

        if (mContext == null) {
            mContext = context;
        }

        try {
            batteryStatus = intent.getIntExtra("BatteryStatus", 0);
            isGPSStatus = intent.getBooleanExtra("GPSStatus", false);
        } catch (Exception ex) {
            ex.getMessage();
        }

        try {
            Intent serviceIntent = new Intent(mContext, GPSBatteryStatus.class);
            serviceIntent.putExtra("BatteryStatus", batteryStatus);
            serviceIntent.putExtra("GPSStatus", isGPSStatus);
            mContext.startService(serviceIntent);
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
}
