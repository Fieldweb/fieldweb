package com.corefield.fieldweb.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.corefield.fieldweb.AsyncManager.CommonAsyncManager.DataAsyncTask;
import com.corefield.fieldweb.AsyncManager.CommonAsyncManager.DataCallBack;
import com.corefield.fieldweb.AsyncManager.CommonAsyncManager.DataOperation;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Network.URLConstant;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class GPSBatteryStatus extends Service {
    private String TAG = GPSBatteryStatus.class.getSimpleName();
    int batteryStatus = 0;
    boolean isGPSStatus = false;
    private Timer timer;
    private TimerTask timerTask;
    public int counter = 0;
    Context mContext;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            mContext = getApplicationContext();
            batteryStatus = intent.getIntExtra("BatteryStatus", 0);
            isGPSStatus = intent.getBooleanExtra("GPSStatus", false);
            FWLogger.logInfo(TAG, "Battery_Status : " + batteryStatus + " GPS_Status : " + isGPSStatus);

            //API call on 15 min interval
            updateGPSStatus(batteryStatus, isGPSStatus, String.valueOf(SharedPrefManager.getInstance(mContext).getUserId()));
        } catch (Exception ex) {
            ex.getMessage();
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void updateGPSStatus(int batteryStatus, boolean isGPSStatus, String UserId) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Id", 0);
            jsonObject.put("GPSOnOrOff", isGPSStatus);
            jsonObject.put("BatteryPercentage", batteryStatus);
            jsonObject.put("UserId", UserId);
            jsonObject.put("CreatedBy", 0);
            jsonObject.put("CreatedDate", "");
            jsonObject.put("UpdatedBy", 0);
            jsonObject.put("UpdatedDate", "");

            new DataAsyncTask(mContext, DataOperation.WEBAPI_POST, jsonObject.toString(), URLConstant.DeviceStatus.ADD_UPDATE_DEVICE_STATUS, new DataCallBack() {
                @Override
                public void onSuccess(Object result) {
                    FWLogger.logInfo(TAG, "GPS_Test_onSuccess: " + result.toString());

                }

                @Override
                public void onFailed(Object result) {
                    FWLogger.logInfo(TAG, "GPS_Test_onFailed: " + result.toString());
                }
            }).execute();

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("RestartService");
        broadcastIntent.setClass(this, HomeActivityNew.class);
        this.sendBroadcast(broadcastIntent);
    }
}

