package com.corefield.fieldweb.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.corefield.fieldweb.Util.GoogleMapUtils.LocationUpdatesService;

public class RestarterLocation extends BroadcastReceiver {
    Intent serviceIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        Context mContext = context.getApplicationContext();

        if (mContext == null) {
            mContext = context;
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                serviceIntent = new Intent(mContext, LocationUpdatesService.class);
                mContext.startForegroundService(serviceIntent);
            } else {
                mContext.startService(serviceIntent);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
}
