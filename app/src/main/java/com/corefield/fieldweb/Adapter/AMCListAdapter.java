package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.AMC.AMCList;
import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of AMC to Owner
 * @see
 */

public class AMCListAdapter extends RecyclerView.Adapter<AMCListAdapter.ViewHolder> implements Filterable {

    private List<AMCList.ResultData> mAMCList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    //private List<AMCList.ResultData> mAMCList;
    private List<AMCList.ResultData> mResultDataList;

    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param amcList List of amc's
     */
    public AMCListAdapter(Context context, List<AMCList.ResultData> amcList, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mAMCList = amcList;
        this.mContext = context;
        this.mClickListener = clickListener;
        this.mResultDataList = amcList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.amc_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AMCList.ResultData amcItem = mAMCList.get(position);

        String formattedTime = DateUtils.timeFormatConversion(amcItem.getActivationTime(), "HH:MM:SS", "hh:mm aa");
        if (formattedTime != null)
            viewHolder.serviceTime.setText(formattedTime);

        String convertedDate = DateUtils.convertDateFormat(amcItem.getAMCServiceDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
        FWLogger.logInfo("AMC", " " + convertedDate);
        if (convertedDate != null)
            viewHolder.serviceDate.setText(convertedDate);

        viewHolder.AMCName.setText(amcItem.getAMCName());
        viewHolder.customerName.setText(amcItem.getCustomerName());
        viewHolder.noOfService.setText("" + amcItem.getServiceNo());
        viewHolder.serviceType.setText(amcItem.getServiceOccuranceType());
        viewHolder.totalServices.setText("" + amcItem.getTotalServices());

        if (amcItem.getTaskDetails() != null) {
            viewHolder.linearTaskDetails.setVisibility(View.VISIBLE);
            if (amcItem.getTaskDetails().getTechnicianName() != null)
                viewHolder.technicianName.setText(amcItem.getTaskDetails().getTechnicianName());
            if (amcItem.getTaskDetails().getTaskId() != 0)
                viewHolder.taskId.setText("" + amcItem.getTaskDetails().getTaskId());
        } else {
            viewHolder.linearTaskDetails.setVisibility(View.GONE);
        }

        if (amcItem.getAMCTypeName() != null) {
            switch (amcItem.getAMCTypeName()) {
                case "Renewal":
                    viewHolder.amcType.setBackgroundResource(R.drawable.amc_status_renewal);
                    break;

                case "Expired":
                    viewHolder.amcType.setBackgroundResource(R.drawable.amc_status_expired);
                    break;

                case "Upcomming":
                    viewHolder.amcType.setBackgroundResource(R.drawable.amc_status_upcoming);
                    break;

                case "Completed":
                    viewHolder.amcType.setBackgroundResource(R.drawable.amc_status_completed);
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAMCList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder// implements View.OnClickListener
    {
        private TextView serviceTime, AMCName, customerName, noOfService, amcType, serviceDate, serviceType, totalServices, technicianName, taskId;
        private LinearLayout linearTaskDetails;

        ViewHolder(@NonNull View view) {
            super(view);
            serviceTime = view.findViewById(R.id.textView_service_time);
            AMCName = view.findViewById(R.id.textView_amc_name);
            customerName = view.findViewById(R.id.textView_customer_name);
            noOfService = view.findViewById(R.id.textView_no_of_service);
            amcType = view.findViewById(R.id.textView_amc_type);
            serviceDate = view.findViewById(R.id.textView_service_date);
            serviceType = view.findViewById(R.id.textView_service_type);
            totalServices = view.findViewById(R.id.textView_total_services);
            technicianName = view.findViewById(R.id.textView_tech_name);
            taskId = view.findViewById(R.id.textView_task_id);
            linearTaskDetails = view.findViewById(R.id.linear_task_details);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });
        }
    }

    public AMCList.ResultData getItem(int id) {
        return mAMCList.get(id);
    }

    public List<AMCList.ResultData> getData() {
        return mAMCList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mAMCList = mResultDataList;
                } else {
                    List<AMCList.ResultData> filteredList = new ArrayList<>();
                    for (AMCList.ResultData row : mResultDataList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getAMCName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCustomerName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mAMCList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mAMCList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mAMCList = (ArrayList<AMCList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mAMCList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(AMCList.ResultData amcList, int position) {
        mAMCList.add(position, amcList);
        notifyItemInserted(position);
    }
}


