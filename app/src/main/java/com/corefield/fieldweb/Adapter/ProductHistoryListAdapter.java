package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.LeadManagement.LeadDetailsDTO;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class ProductHistoryListAdapter extends RecyclerView.Adapter<ProductHistoryListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    List<LeadDetailsDTO.LeadStatusLogObj> leadStatusLogObjs;
    private Context mContext;

    /**
     * Constructor
     *
     * @param context
     */
    public ProductHistoryListAdapter(Context context, List<LeadDetailsDTO.LeadStatusLogObj> leadStatusLogObjs) {
        this.mInflater = LayoutInflater.from(context);
        this.leadStatusLogObjs = leadStatusLogObjs;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.product_list_history_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        LeadDetailsDTO.LeadStatusLogObj leadStatusLogObj = leadStatusLogObjs.get(position);
        try {
            if (leadStatusLogObj.getLeadNotes() != null) {
                viewHolder.mtxtFollowUpNotes.setText(String.valueOf(leadStatusLogObj.getLeadNotes()));
            } else {
                viewHolder.mtxtFollowUpNotes.setText("-NA-");
            }

            String formattedDate = DateUtils.convertDateFormat(leadStatusLogObj.getCreatedDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy hh:mm aa");
            if (formattedDate != null)
                viewHolder.mtxtLeadDateTime.setText(formattedDate);


            switch (leadStatusLogObj.getLeadStatusName()) {
                case "InActive":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.inactive));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                    break;

                case "Assigned":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.task_ongoing_dark));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.assigned));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.task_ongoing_dark));
                    break;

                case "In discussion":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.quantum_lightblue));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.indiscussion));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.quantum_lightblue));
                    break;

                case "Called":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.blue));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.called));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.blue));
                    break;

                case "Dormant":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.dot_dark_screen1));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.dormant));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.dot_dark_screen1));
                    break;

                case "Quote Sent":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.dot_light_screen1));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.quotesent));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.dot_light_screen1));
                    break;

                case "Converted":
                    viewHolder.mtxtLeadStatus.setTextColor(mContext.getResources().getColor(R.color.green));
                    viewHolder.mtxtLeadStatus.setText(mContext.getResources().getString(R.string.converted));
                    viewHolder.mtxtLeadDateTime.setTextColor(mContext.getResources().getColor(R.color.green));
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return leadStatusLogObjs.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mtxtLeadStatus, mtxtFollowUpNotes, mtxtLeadDateTime;

        ViewHolder(View view) {
            super(view);
            mtxtLeadStatus = view.findViewById(R.id.txtLeadStatus);
            mtxtFollowUpNotes = view.findViewById(R.id.txtFollowUpNotes);
            mtxtLeadDateTime = view.findViewById(R.id.txtLeadDateTime);

        }

    }

    public LeadDetailsDTO.LeadStatusLogObj getItem(int id) {
        return leadStatusLogObjs.get(id);
    }

    public List<LeadDetailsDTO.LeadStatusLogObj> getData() {
        return leadStatusLogObjs;
    }

    /*public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }*/
}


