package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemIssueList;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of item issued to technician (Tech login)
 * @see
 */
public class TechItemIssueListAdapterNew extends RecyclerView.Adapter<TechItemIssueListAdapterNew.ViewHolder> implements Filterable {

    private List<ItemIssueList.ResultData> mItemIssueList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int position;
    private List<ItemIssueList.ResultData> mResultDataListFilter;

    /**
     * Constructor
     *
     * @param context
     * @param position       position of ItemIssueList
     * @param mItemIssueList list of issue items
     */
    public TechItemIssueListAdapterNew(Context context, int position, List<ItemIssueList.ResultData> mItemIssueList) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemIssueList = mItemIssueList;
        this.mResultDataListFilter = mItemIssueList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.itemslist_new_tech, parent, false);
        FWLogger.logInfo("onCreateViewHolder = ", "" + position);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ItemIssueList.ResultData itemIssueList = mResultDataListFilter.get(position);
        viewHolder.mItemName.setText(itemIssueList.getName());
        viewHolder.mItemQuantity.setText(String.valueOf(itemIssueList.getQuantity()));
        viewHolder.textViewNote.setText(itemIssueList.getDescription());

        if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
            viewHolder.mImageViewAddItem.setVisibility(View.GONE);
        FWLogger.logInfo("onBindViewHolder = ", "" + position);

        try {
            if (itemIssueList.getItemImagePath() != null || !itemIssueList.getItemImagePath().isEmpty()) {
                Picasso.get().load(itemIssueList.getItemImagePath()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.itemImg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        viewHolder.itemImg.setImageResource(R.drawable.ic_no_image);
                    }
                });
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    @Override
    public int getItemCount() {
        FWLogger.logInfo("getItemCount = ", "" + mResultDataListFilter.size());
        return mResultDataListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mItemName, mItemQuantity, mItemDescription, textViewReturnTitle, textViewUsedTitle,
                textViewIssuedQty, textViewTotalQty, textViewNote;
        private ImageView mImageViewAddItem, itemImg;//, mImageViewItem;
        private LinearLayout linearLayoutNote;

        ViewHolder(View view) {
            super(view);
            mItemName = view.findViewById(R.id.item_name);
            mItemQuantity = view.findViewById(R.id.item_quantity);
            mItemDescription = view.findViewById(R.id.item_description);
            mImageViewAddItem = view.findViewById(R.id.image_item_update);
            textViewReturnTitle = view.findViewById(R.id.textView_return_title);
            textViewUsedTitle = view.findViewById(R.id.textView_used_title);
            textViewIssuedQty = view.findViewById(R.id.textView_issued_qty);
            textViewTotalQty = view.findViewById(R.id.textView_total_qty);
            textViewNote = view.findViewById(R.id.textView_notes);
            linearLayoutNote = view.findViewById(R.id.linear_note);
            itemImg = view.findViewById(R.id.itemImg);

            textViewReturnTitle.setText("Returned:");
            textViewUsedTitle.setText("Used:");
            textViewReturnTitle.setVisibility(View.GONE);
            textViewUsedTitle.setVisibility(View.GONE);
            textViewIssuedQty.setVisibility(View.GONE);
            textViewTotalQty.setVisibility(View.GONE);

            linearLayoutNote.setVisibility(View.VISIBLE);

            mItemName.setTypeface(mItemName.getTypeface(), Typeface.BOLD);

            mItemDescription.setVisibility(View.GONE);
            mImageViewAddItem.setVisibility(View.GONE);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClick(view, getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition());
        }
    }

    public ItemIssueList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<ItemIssueList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mItemIssueList;
                } else {
                    List<ItemIssueList.ResultData> filteredList = new ArrayList<>();
                    for (ItemIssueList.ResultData row : mItemIssueList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString)) {  // || String.valueOf(row.getFirstName()).contains(charSequence) ||  row.getLastName().toLowerCase().contains(charString.toLowerCase())
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<ItemIssueList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemIssueList.ResultData itemIssueList, int position) {
        mResultDataListFilter.add(position, itemIssueList);
        notifyItemInserted(position);
    }
}


