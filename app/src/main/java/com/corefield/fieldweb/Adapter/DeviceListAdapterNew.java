package com.corefield.fieldweb.Adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.ImageUtils;
import com.corefield.fieldweb.Util.ViewUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the Device list in Task Closure form
 * This adapter can add or remove item smoothly.
 * //
 **/
public class DeviceListAdapterNew extends RecyclerView.Adapter<DeviceListAdapterNew.ViewHolder> {

    protected final String TAG = DeviceListAdapterNew.class.getSimpleName();
    private List<TaskClosure.DeviceInfoList> mDeviceLists;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;
    private TaskClosureFragmentNew mTaskClosureFragment;
    private String mDeviceImage = "Device";

    /**
     * Constructor
     *
     * @param resultDataList      result data
     * @param context             context
     * @param taskClosureFragment calling fragment
     */
    public DeviceListAdapterNew(List<TaskClosure.DeviceInfoList> resultDataList, Context context, TaskClosureFragmentNew taskClosureFragment) {
        this.mInflater = LayoutInflater.from(context);
        this.mDeviceLists = resultDataList;
        this.mContext = context;
        this.mTaskClosureFragment = taskClosureFragment;
        this.mClickListener = taskClosureFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.task_closure_device_item_new, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        TaskClosure.DeviceInfoList deviceList = mDeviceLists.get(i);
        if (i == 0) {
            viewHolder.mTextViewDeviceCellAddOrClose.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_plus));
            viewHolder.mTextViewDeviceNo.setText("");
            viewHolder.textViewDeviceName.setText(R.string.add_new_device);
        } else {
            //Add/remove/align views
            if (viewHolder.mLinearLayoutExpandCollapse.getVisibility() == View.VISIBLE) {
                viewHolder.mTextViewDeviceNo.setBackground(null);
                viewHolder.textViewDeviceName.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_white_100));
                viewHolder.textViewDeviceName.setPadding(10, 0, 0, 0);
                viewHolder.textViewDeviceName.setHint(R.string.click_here_close_device_details);
            } else if (viewHolder.mLinearLayoutExpandCollapse.getVisibility() == View.GONE) {
                viewHolder.textViewDeviceName.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_grey600));
                viewHolder.textViewDeviceName.setPadding(10, 0, 0, 0);
                viewHolder.textViewDeviceName.setHint(R.string.click_here_device_details);
            }
            viewHolder.mTextViewDeviceCellAddOrClose.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_cross));
            viewHolder.textViewDeviceName.setGravity(Gravity.CENTER_VERTICAL);

            //set device details
            setDeviceDetails(viewHolder, deviceList);

            //set device photos
            setDevicePhotos(viewHolder, deviceList);
        }
    }

    private void setDeviceDetails(@NonNull ViewHolder viewHolder, TaskClosure.DeviceInfoList deviceList) {
        //Set values
        viewHolder.mTextViewDeviceNo.setText(Integer.toString(deviceList.getId()));
        if (deviceList.getDeviceName() != null && !deviceList.getDeviceName().equalsIgnoreCase("")) {
            viewHolder.textViewDeviceName.setText(deviceList.getDeviceName());
            viewHolder.mEditTextDeviceName.setText(deviceList.getDeviceName());
        } else {
            viewHolder.textViewDeviceName.setText("");
            viewHolder.mEditTextDeviceName.setText("");
        }
        if (deviceList.getModelNumber() != null && !deviceList.getModelNumber().equalsIgnoreCase("")) {
            viewHolder.mEditTextSerialNo.setText(deviceList.getModelNumber());
        } else {
            viewHolder.mEditTextSerialNo.setText("");
        }

        if (deviceList.getDeviceReading() != 0) {
            viewHolder.mEditTextDeviceReading.setText("" + deviceList.getDeviceReading());
        } else {
            viewHolder.mEditTextDeviceReading.setText("");
        }
    }

    private void setDevicePhotos(@NonNull ViewHolder viewHolder, TaskClosure.DeviceInfoList deviceList) {
        //set images
        if (deviceList.getDevicePhoto1() != null)
            viewHolder.mImageViewDeviceOne.setImageBitmap(ImageUtils.getRoundedCornerBitmap(ImageUtils.ConvertStringToBitmap(deviceList.getDevicePhoto1()), ImageUtils.convertDipToPixels(30)));
        else viewHolder.mImageViewDeviceOne.setImageBitmap(null);
        if (deviceList.getDevicePhoto2() != null)
            viewHolder.mImageViewDeviceTwo.setImageBitmap(ImageUtils.getRoundedCornerBitmap(ImageUtils.ConvertStringToBitmap(deviceList.getDevicePhoto2()), ImageUtils.convertDipToPixels(30)));
        else viewHolder.mImageViewDeviceTwo.setImageBitmap(null);
        if (deviceList.getDevicePhoto3() != null)
            viewHolder.mImageViewDeviceThree.setImageBitmap(ImageUtils.getRoundedCornerBitmap(ImageUtils.ConvertStringToBitmap(deviceList.getDevicePhoto3()), ImageUtils.convertDipToPixels(30)));
        else viewHolder.mImageViewDeviceThree.setImageBitmap(null);
    }

    @Override
    public int getItemCount() {
        return mDeviceLists.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ImageUpdateListener {
        TextView textViewDeviceName, mTextViewDeviceNo, mTextViewDeviceCellAddOrClose, mTextViewRemoveDeviceOne, mTextViewRemoveDeviceTwo, mTextViewRemoveDeviceThree;
        EditText mEditTextDeviceName, mEditTextSerialNo, mEditTextDeviceReading;
        TableRow mTableRowDevicePictureWrapper;
        LinearLayout mLinearLayoutExpandCollapse;
        RelativeLayout mRelativeLayoutDeviceListHeaderOne, mRelativeLayoutDeviceOne, mRelativeLayoutDeviceTwo, mRelativeLayoutDeviceThree, mRelativeLayoutDeviceListHeaderTwo;
        ImageView mImageViewHintDeviceOne, mImageViewDeviceOne, mImageViewHintDeviceTwo, mImageViewDeviceTwo, mImageViewHintDeviceThree, mImageViewDeviceThree;
        ImageUpdateListener mImageUpdateListener;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setIsRecyclable(false);
            mImageUpdateListener = this;

            init(itemView);

            mEditTextDeviceName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    mDeviceLists.get(getAdapterPosition()).setDeviceName(s.toString());
                    textViewDeviceName.setText(s.toString());

                }
            });

            mEditTextSerialNo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    mDeviceLists.get(getAdapterPosition()).setModelNumber(s.toString());
                }
            });

            mEditTextDeviceReading.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s != null && !s.toString().isEmpty()) {
                        try {
                            mDeviceLists.get(getAdapterPosition()).setDeviceReading(Double.parseDouble(s.toString()));
                        } catch (NumberFormatException e) {
                            mEditTextDeviceReading.setText("");
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.please_enter_valid_no), Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
        }

        private void init(@NonNull View itemView) {
            mRelativeLayoutDeviceListHeaderOne = itemView.findViewById(R.id.relative_layout_device_list_header_one);
            mRelativeLayoutDeviceListHeaderTwo = itemView.findViewById(R.id.relative_layout_device_list_header_two);
            mTableRowDevicePictureWrapper = itemView.findViewById(R.id.tablerow_device_picture_wrapper);
            mLinearLayoutExpandCollapse = itemView.findViewById(R.id.linearlayout_expand_collapse_wrapper);

            //Device One
            mImageViewHintDeviceOne = itemView.findViewById(R.id.imageview_device_hint_one);
            mImageViewDeviceOne = itemView.findViewById(R.id.imageview_device_one);
            mRelativeLayoutDeviceOne = itemView.findViewById(R.id.relative_layout_device_image_one);
            mTextViewRemoveDeviceOne = itemView.findViewById(R.id.textView_remove_device_image_one);
            mImageViewHintDeviceOne.setOnClickListener(this);
            mTextViewRemoveDeviceOne.setOnClickListener(this);

            //Two
            mImageViewHintDeviceTwo = itemView.findViewById(R.id.imageview_device_hint_two);
            mImageViewDeviceTwo = itemView.findViewById(R.id.imageview_device_two);
            mRelativeLayoutDeviceTwo = itemView.findViewById(R.id.relative_layout_device_image_two);
            mTextViewRemoveDeviceTwo = itemView.findViewById(R.id.textView_remove_device_image_two);
            mImageViewHintDeviceTwo.setOnClickListener(this);
            mTextViewRemoveDeviceTwo.setOnClickListener(this);

            //Three
            mImageViewHintDeviceThree = itemView.findViewById(R.id.imageview_device_hint_three);
            mImageViewDeviceThree = itemView.findViewById(R.id.imageview_device_three);
            mRelativeLayoutDeviceThree = itemView.findViewById(R.id.relative_layout_device_image_three);
            mTextViewRemoveDeviceThree = itemView.findViewById(R.id.textView_remove_device_image_three);
            mImageViewHintDeviceThree.setOnClickListener(this);
            mTextViewRemoveDeviceThree.setOnClickListener(this);

            //Header
            textViewDeviceName = itemView.findViewById(R.id.textView_device_name_header);
            textViewDeviceName.setOnClickListener(this);
            mTextViewDeviceNo = itemView.findViewById(R.id.textView_device_no);
            mTextViewDeviceNo.setOnClickListener(this);
            mTextViewDeviceCellAddOrClose = itemView.findViewById(R.id.textView_add_or_remove_device);
            mTextViewDeviceCellAddOrClose.setOnClickListener(this);

            //Edit Text
            mEditTextDeviceName = itemView.findViewById(R.id.edittext_device_name);
            mEditTextSerialNo = itemView.findViewById(R.id.edittext_serial_no);
            mEditTextDeviceReading = itemView.findViewById(R.id.edittext_device_reading);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
            switch (v.getId()) {
                case R.id.textView_device_name_header:
                    if (getAdapterPosition() == 0) {
                        if (mDeviceLists.size() < 10) {
                            ViewUtils.hideKeyboard((HomeActivityNew) mContext);
                            TaskClosure.DeviceInfoList device = new TaskClosure.DeviceInfoList();
                            device.setId(mDeviceLists.size());
                            device.setDeviceName("");
                            device.setModelNumber("");
                            device.setDevicePhoto1(null);
                            device.setDevicePhoto2(null);
                            device.setDevicePhoto3(null);
                            device.setDeviceReading(0);
                            restoreItem(device, 1);
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        expandAndShrink();
                    }
                    break;
                case R.id.textView_device_no:
                    if (getAdapterPosition() != 0) expandAndShrink();
                    break;
                case R.id.textView_add_or_remove_device:
                    if (getAdapterPosition() == 0) {
                        if (mDeviceLists.size() < 10) {
                            ViewUtils.hideKeyboard((HomeActivityNew) mContext);
                            TaskClosure.DeviceInfoList device = new TaskClosure.DeviceInfoList();
                            device.setId(mDeviceLists.size());
                            device.setDeviceName("");
                            device.setModelNumber("");
                            device.setDevicePhoto1(null);
                            device.setDevicePhoto2(null);
                            device.setDevicePhoto3(null);
                            device.setDeviceReading(0);
                            restoreItem(device, 1);
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //remove
                        removeItem(getAdapterPosition());
                        //also remove from array list of data
                    }
                    break;
                case R.id.imageview_device_hint_one:
                    mTaskClosureFragment.pickImage(mDeviceImage, 1, mImageUpdateListener);
                    break;
                case R.id.imageview_device_hint_two:
                    mTaskClosureFragment.pickImage(mDeviceImage, 2, mImageUpdateListener);
                    break;
                case R.id.imageview_device_hint_three:
                    mTaskClosureFragment.pickImage(mDeviceImage, 3, mImageUpdateListener);
                    break;
                case R.id.textView_remove_device_image_one:
                    removeDeviceImage(1);
                    break;
                case R.id.textView_remove_device_image_two:
                    removeDeviceImage(2);
                    break;
                case R.id.textView_remove_device_image_three:
                    removeDeviceImage(3);
                    break;
            }
        }

        /**
         * Can expand or collapse list item
         */
        private void expandAndShrink() {
            if (mLinearLayoutExpandCollapse.getVisibility() == View.VISIBLE) {
                mLinearLayoutExpandCollapse.setVisibility(View.GONE);
                mRelativeLayoutDeviceListHeaderTwo.setBackground(null);
                textViewDeviceName.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_grey600));
                textViewDeviceName.setPadding(10, 0, 0, 0);
                textViewDeviceName.setHint(R.string.click_here_device_details);

            } else if (mLinearLayoutExpandCollapse.getVisibility() == View.GONE) {
                mLinearLayoutExpandCollapse.setVisibility(View.VISIBLE);
                mTextViewDeviceNo.setBackground(null);
                textViewDeviceName.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_white_100));
                textViewDeviceName.setPadding(10, 0, 0, 0);
                textViewDeviceName.setHint(R.string.click_here_close_device_details);
            }
        }

        @Override
        public void onImageCaptured(Bitmap bitmap, String whichImage, int imageNo) {
            if (whichImage.equalsIgnoreCase(mDeviceImage)) {
                if (imageNo == 1) {
                    mTextViewRemoveDeviceOne.setError(null);
                    mImageViewDeviceOne.setImageBitmap(bitmap);
                    mDeviceLists.get(getAdapterPosition()).setDevicePhoto1(ImageUtils.ConvertBitmapToString(bitmap));
                } else if (imageNo == 2) {
                    mImageViewDeviceTwo.setImageBitmap(bitmap);
                    mDeviceLists.get(getAdapterPosition()).setDevicePhoto2(ImageUtils.ConvertBitmapToString(bitmap));
                } else if (imageNo == 3) {
                    mImageViewDeviceThree.setImageBitmap(bitmap);
                    mDeviceLists.get(getAdapterPosition()).setDevicePhoto3(ImageUtils.ConvertBitmapToString(bitmap));
                }
            }
        }

        private void removeDeviceImage(int imageNo) {
            if (imageNo == 1) {
                mDeviceLists.get(getAdapterPosition()).setDevicePhoto1(null);
                mImageViewDeviceOne.setImageBitmap(null);
            } else if (imageNo == 2) {
                mDeviceLists.get(getAdapterPosition()).setDevicePhoto2(null);
                mImageViewDeviceTwo.setImageBitmap(null);
            } else if (imageNo == 3) {
                mDeviceLists.get(getAdapterPosition()).setDevicePhoto3(null);
                mImageViewDeviceThree.setImageBitmap(null);
            }
        }
    }

    public TaskClosure.DeviceInfoList getItem(int id) {
        return mDeviceLists.get(id);
    }

    public List<TaskClosure.DeviceInfoList> getData() {
        return mDeviceLists;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }

    public void removeItem(int position) {
        mDeviceLists.get(position).setDevicePhoto1(null);
        mDeviceLists.get(position).setDevicePhoto2(null);
        mDeviceLists.get(position).setDevicePhoto3(null);
        mDeviceLists.get(position).setModelNumber(null);
        mDeviceLists.get(position).setDeviceName(null);
        mDeviceLists.get(position).setDeviceReading(0);
        mDeviceLists.remove(position);
        for (int i = position - 1; i > 0; i--) {
            mDeviceLists.get(i).setId(mDeviceLists.get(i).getId() - 1);
        }
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void restoreItem(TaskClosure.DeviceInfoList deviceList, int position) {
        mDeviceLists.add(position, deviceList);
        notifyItemInserted(position);
        notifyDataSetChanged();
    }

    public interface ImageUpdateListener {
        void onImageCaptured(Bitmap bitmap, String whichImage, int imageNo);
    }
}


