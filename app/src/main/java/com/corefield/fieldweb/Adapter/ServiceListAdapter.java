package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.AMC.AMCDetails;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.TaskStatus;

import java.util.List;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ViewHolder> { //implements Filterable

    protected final String TAG = ServiceListAdapter.class.getSimpleName();
    private List<AMCDetails.TaskDetails> mTaskList;
    private LayoutInflater mInflater;
    private Context mContext;

    public ServiceListAdapter(List<AMCDetails.TaskDetails> resultDataList, Context context) { //, RecyclerTouchListener clickListener
        this.mInflater = LayoutInflater.from(context);
        this.mTaskList = resultDataList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.service_details_custom, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceListAdapter.ViewHolder viewHolder, int i) {
        AMCDetails.TaskDetails resultData = mTaskList.get(i);
        int no = i + 1;
        viewHolder.textViewServiceNo.setText(mContext.getResources().getString(R.string.service) + " " + no);  //+ " :"
        viewHolder.textViewTechName.setText(resultData.getTechnicianName());
        viewHolder.textViewDate.setText(resultData.getTaskDate().split("T")[0]);
        viewHolder.textViewTime.setText(resultData.getTaskTime());
        String status = "";

        switch (resultData.getTaskStatus()) {
            case TaskStatus.COMPLETED:
                status = Constant.TaskCategories.Completed.name();
                break;
            case TaskStatus.IN_ACTIVE:
                status = Constant.TaskCategories.InActive.name();
                break;
            case TaskStatus.ON_GOING:
                status = Constant.TaskCategories.Ongoing.name();
                break;
            case TaskStatus.REJECTED:
                status = Constant.TaskCategories.Rejected.name();
                break;
            default:
                break;
        }

        viewHolder.textViewStatus.setText(status);
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewServiceNo, textViewTechName, textViewDate, textViewTime, textViewStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewServiceNo = itemView.findViewById(R.id.textView_service_no);
            textViewTechName = itemView.findViewById(R.id.textView_tech_name);
            textViewDate = itemView.findViewById(R.id.textView_date);
            textViewTime = itemView.findViewById(R.id.textView_time);
            textViewStatus = itemView.findViewById(R.id.textView_status);
        }
    }

    public AMCDetails.TaskDetails getItem(int id) {
        return mTaskList.get(id);
    }

    public List<AMCDetails.TaskDetails> getData() {
        return mTaskList;
    }

    public void removeItem(int position) {
        mTaskList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(AMCDetails.TaskDetails usersList, int position) {
        mTaskList.add(position, usersList);
        notifyItemInserted(position);
    }

}


