package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.InvoiceDetailsDTO;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class InvoiceServiceDetailsListAdapter extends RecyclerView.Adapter<InvoiceServiceDetailsListAdapter.ViewHolder> {

    private List<InvoiceDetailsDTO.ServiceList> mItemsList;
    private LayoutInflater mInflater;
    List<InvoiceDetailsDTO.ServiceList> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public InvoiceServiceDetailsListAdapter(Context context, List<InvoiceDetailsDTO.ServiceList> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.invoice_servicedetails_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        InvoiceDetailsDTO.ServiceList itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtServiceName.setText(String.valueOf(itemList.getServiceName()));
            viewHolder.txtServicePrice.setText("₹"+String.valueOf(itemList.getTotalPrice()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtServiceName, txtServicePrice;

        ViewHolder(View view) {
            super(view);
            txtServiceName = view.findViewById(R.id.txtServiceName);
            txtServicePrice = view.findViewById(R.id.txtServicePrice);
        }

    }

    public InvoiceDetailsDTO.ServiceList getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<InvoiceDetailsDTO.ServiceList> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(InvoiceDetailsDTO.ServiceList serviceList, int position) {
        mItemsList.add(position, serviceList);
        notifyItemInserted(position);
    }
}


