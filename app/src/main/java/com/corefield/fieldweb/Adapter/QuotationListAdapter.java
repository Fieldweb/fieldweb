package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.QuotationListDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Accounts.QuotationDetailsFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of task added by the Owner
 * @see
 */
public class QuotationListAdapter extends RecyclerView.Adapter<QuotationListAdapter.ViewHolder> /*implements Filterable*/ {

    private List<QuotationListDTO.ResultData> mQuotationList;
    private List<QuotationListDTO.ResultData> mResultDataList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private static String TAG = QuotationListAdapter.class.getSimpleName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;


    public QuotationListAdapter(Context context, List<QuotationListDTO.ResultData> mQuotationList, RecyclerTouchListener mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mQuotationList = new ArrayList<>();
        //this.mQuotationList = mQuotationList;
        //this.mResultDataList = mQuotationList;
        this.mClickListener = mClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (position) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.task_list_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.quotation_list, parent, false);
        viewHolder = new ViewHolder(v1);
        return viewHolder;
    }

    protected class LoadingVH extends ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        QuotationListDTO.ResultData quoteList = mQuotationList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder viewFHolder = (ViewHolder) viewHolder;
                if (quoteList != null) setTaskRowDetails(viewFHolder, quoteList);
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }

    private void setTaskRowDetails(ViewHolder viewHolder, QuotationListDTO.ResultData resultData) {

        try {
            viewHolder.mtxtCustName.setText(resultData.getCustomer().getCustomerName());
        } catch (Exception ex) {
            ex.getMessage();
        }
        viewHolder.mtxtQuotAmount.setText("₹ " + String.valueOf(resultData.getGrandTotalAmount()));

        try {
            String formattedDate = DateUtils.convertDateFormat(resultData.getCreatedDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
            viewHolder.mtxtQuoteDate.setText(formattedDate);
        } catch (Exception ex) {

        }
        viewHolder.mtxtQuotName.setText(resultData.getQuoteName());
        viewHolder.mtxtQuotID.setText("[" + "QUO" + resultData.getId() + "]");

        try {
            if (resultData.getStatus().getStatusName() != null || !resultData.getStatus().getStatusName().isEmpty()) {
                switch (resultData.getStatus().getStatusName()) {
                    case "Assigned":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.assigned_background));
                        viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.assigned));
                        //viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                        break;
                    case "Not Assigned":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.unassigned_background));
                        viewHolder.mTextViewType.setText("Not Assigned");
                        //viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.White));
                        break;
                    case "Lost":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.rejected_background));
                        viewHolder.mTextViewType.setText("Lost");
                        //viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.White));
                        break;
                    case "Task Completed":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.completed_background));
                        viewHolder.mTextViewType.setText("Task Completed");
                        //viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.White));
                        break;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return mQuotationList == null ? 0 : mQuotationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mtxtCustName, mtxtQuotAmount, mtxtQuoteDate, mtxtQuotName, mTextViewTaskStatus, mTextViewType, mtxtQuotID, mTextViewReassign;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mtxtCustName = view.findViewById(R.id.txtCustName);
            mtxtQuotAmount = view.findViewById(R.id.txtQuotAmount);
            mtxtQuoteDate = view.findViewById(R.id.txtQuoteDate);
            mtxtQuotName = view.findViewById(R.id.txtQuotName);
            mTextViewTaskStatus = view.findViewById(R.id.task_status_text);
            mTextViewType = view.findViewById(R.id.textView_type);
            mtxtQuotID = view.findViewById(R.id.txtQuotID);

        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition());
            ((HomeActivityNew) mContext).mToolbar.setTitle(R.string.quotation_details);
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            QuotationDetailsFragment myFragment = new QuotationDetailsFragment();
            Bundle args = new Bundle();
            args.putInt("quoteID", mQuotationList.get(getAdapterPosition()).getId());
            myFragment.setArguments(args);
            /*Bundle args = new Bundle();
            args.putInt("serviceID", mItemsListFilter.get(getAdapterPosition()).getId());
            args.putString("serviceName", mItemsListFilter.get(getAdapterPosition()).getServiceName());
            args.putInt("servicePrice", mItemsListFilter.get(getAdapterPosition()).getPrice());
            args.putString("contactNo", mItemsListFilter.get(getAdapterPosition()).getContactNo());
            args.putString("description", mItemsListFilter.get(getAdapterPosition()).getDescription());
            myFragment.setArguments(args);*/
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mQuotationList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public QuotationListDTO.ResultData getItem(int id) {
        return mQuotationList.get(id);
    }

    public List<QuotationListDTO.ResultData> getData() {
        return mQuotationList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void add(QuotationListDTO.ResultData resultData) {
        mQuotationList.add(resultData);
        notifyItemInserted(mQuotationList.size() - 1);
    }

    public void addAll(List<QuotationListDTO.ResultData> tasksList) {
        for (QuotationListDTO.ResultData resultData : tasksList) {
            add(resultData);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new QuotationListDTO.ResultData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mQuotationList.size() - 1;
        if (position >= 0) {
            QuotationListDTO.ResultData item = getItem(position);

            if (item != null) {
                mQuotationList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

   /* @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mQuotationList = mResultDataList;
                } else {
                    List<QuotationListDTO.ResultData> filteredList = new ArrayList<>();
                    for (QuotationListDTO.ResultData row : mResultDataList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCustomer().getCustomerName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getQuoteName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mQuotationList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mQuotationList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mQuotationList = (ArrayList<QuotationListDTO.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }*/

    public void removeItem(int position) {
        mQuotationList.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(TasksList.ResultData resultData) {
        int position = mQuotationList.indexOf(resultData);
        if (position > -1) {
            mQuotationList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void restoreItem(QuotationListDTO.ResultData quotList, int position) {
        mQuotationList.add(position, quotList);
        notifyItemInserted(position);
    }
}


