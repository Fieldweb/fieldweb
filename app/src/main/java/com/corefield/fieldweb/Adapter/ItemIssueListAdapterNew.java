package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.AllItemIssueList;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of item issued to technician (Owner login)
 * @see
 */
public class ItemIssueListAdapterNew extends RecyclerView.Adapter<ItemIssueListAdapterNew.ViewHolder> implements Filterable {

    private List<AllItemIssueList.ResultData> mItemIssueList;
    private List<AllItemIssueList.ResultData> mItemIssueListFilter;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;

    /**
     * Constructor
     *
     * @param context
     * @param position       position of item in AllItemIssueList
     * @param mItemIssueList List of issued item to technician by Owner
     */
    public ItemIssueListAdapterNew(Context context, int position, List<AllItemIssueList.ResultData> mItemIssueList, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemIssueList = mItemIssueList;
        this.mItemIssueListFilter = mItemIssueList;
        this.mContext = context;
        this.mClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.itemissuelist_new, parent, false);
        FWLogger.logInfo(" onCreateViewHolder = ", "" + position);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AllItemIssueList.ResultData itemIssueList = mItemIssueListFilter.get(position);
        viewHolder.mItemName.setText(itemIssueList.getName());
        viewHolder.mItemIssueTech.setText(itemIssueList.getAssignedTo());
        viewHolder.mItemQuantity.setText(String.valueOf(itemIssueList.getQuantity()));

        Picasso.get().load(itemIssueList.getTechPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                viewHolder.mProfile.setImageResource(R.drawable.profile_icon);
            }
        });

        FWLogger.logInfo("onBindViewHolder = ", "" + position);
    }

    @Override
    public int getItemCount() {
        FWLogger.logInfo("Issue_getItemCount = ", "" + mItemIssueListFilter.size());
        return mItemIssueListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mItemName, mItemIssueTech, mItemQuantity;
        private ImageView mProfile, mItemIssueUpdate;

        ViewHolder(View view) {
            super(view);
            mItemName = view.findViewById(R.id.item_name);
            mItemIssueTech = view.findViewById(R.id.item_issue_tech);
            mItemQuantity = view.findViewById(R.id.item_issue_list_quantity);
            mProfile = view.findViewById(R.id.imageView_technician);
            mItemIssueUpdate = view.findViewById(R.id.image_issue_item_update);
            mItemIssueUpdate.setOnClickListener(this);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClick(view, getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition());
        }
    }

    public AllItemIssueList.ResultData getItem(int id) {
        return mItemIssueListFilter.get(id);
    }

    public List<AllItemIssueList.ResultData> getData() {
        return mItemIssueListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mItemIssueListFilter = mItemIssueList;
                } else {
                    List<AllItemIssueList.ResultData> filteredList = new ArrayList<>();
                    for (AllItemIssueList.ResultData row : mItemIssueList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getAssignedTo().toLowerCase().contains(charString) || row.getName().toLowerCase().contains(charString)
                                || String.valueOf(row.getQuantity()).toLowerCase().contains(charString)) {
                            filteredList.add(row);
                        }
                    }

                    mItemIssueListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mItemIssueListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItemIssueListFilter = (ArrayList<AllItemIssueList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mItemIssueListFilter.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(AllItemIssueList.ResultData itemIssueList, int position) {
        mItemIssueListFilter.add(position, itemIssueList);
        notifyItemInserted(position);
    }
}


