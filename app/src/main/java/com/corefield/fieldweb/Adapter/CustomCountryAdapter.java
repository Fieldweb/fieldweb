package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.CountryList.GetCountryList;
import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.FieldWeb.TouchLessSignupActivity;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomCountryAdapter extends RecyclerView.Adapter<CustomCountryAdapter.HistoryViewHolder> implements Filterable {

    Context context;
    TouchLessSignupActivity activity;
    List<GetCountryList.ResultData> mCountryList;
    ArrayList<Bitmap> mbitmap_array;
    List<GetCountryList.ResultData> productListFiltered = new ArrayList<>();
    List<GetCountryList.ResultData> prods = new ArrayList<>();
    private RecyclerTouchListener mClickListener;
    CommonAdapterPositionListener mpositionListener;

    public CustomCountryAdapter(TouchLessSignupActivity activity, Context context, List<GetCountryList.ResultData> countryList,
                                ArrayList<Bitmap> bitmap_array, CommonAdapterPositionListener positionListener) {
        this.context = context;
        this.mCountryList = countryList;
        this.activity = activity;
        this.prods = countryList;
        this.mbitmap_array = bitmap_array;
        this.mpositionListener = positionListener;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_list_item, parent, false);

        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryViewHolder holder, final int position) {

        final GetCountryList.ResultData rowItem = mCountryList.get(position);
        try {
            mpositionListener.onItemSelected(position);
            holder.txtTitle.setText(rowItem.getCountryName());
            if (mbitmap_array.get(position) != null) {
                // holder.imageView.setImageBitmap(mbitmap_array.get(position));
                Picasso.get().load(rowItem.getCountryFlag()).into(holder.imageView);
            } else {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(
                        R.drawable.fw_logo));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /* public void setClickListener(RecyclerTouchListener itemClickListener) {
         this.mClickListener = itemClickListener;
     }
 */
    public GetCountryList.ResultData getItem(int id) {
        return productListFiltered.get(id);
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        ImageView imageView;
        TextView txtTitle;

        public HistoryViewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.spinnerTextView);
            imageView = (ImageView) itemView.findViewById(R.id.spinnerImages);

          /*  txtTitle.setOnClickListener(this);
            imageView.setOnClickListener(this);*/
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // mClickListener.onClick(v, getAdapterPosition());
                    mpositionListener.onItemSelected(getAdapterPosition());

                }
            });*/
        }


       /* @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
            //mpositionListener.onItemSelected(getAdapterPosition());
        }*/
    }


    @Override
    public int getItemCount() {
        return mCountryList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productListFiltered.clear();
                    productListFiltered = prods;

                } else {
                    ArrayList<GetCountryList.ResultData> filteredList = new ArrayList<>();
                    for (GetCountryList.ResultData row : mCountryList) {
                        if (row.getCountryCode().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCountryName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    productListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = productListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                mCountryList = (ArrayList<GetCountryList.ResultData>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }

    /*public void restoreItem(GetCountryList.ResultData cList, int position) {
        mCountryList.add(position, cList);
        notifyItemInserted(position);
    }*/

    public interface CommonAdapterPositionListener {

        void onItemSelected(int position);
    }
}
