package com.corefield.fieldweb.Adapter;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.FieldWeb.Dialogs.TaskClosureDialogNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskClosureFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the notes list in Task Closure form
 * This adapter can add or remove item smoothly.
 * //
 **/
public class NotesAdapterNew extends RecyclerView.Adapter<NotesAdapterNew.ViewHolder> {

    protected final String TAG = NotesAdapterNew.class.getSimpleName();
    private List<TaskClosure.TechnicalNotedto> mNotesList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;

    public NotesAdapterNew(List<TaskClosure.TechnicalNotedto> resultDataList, Context context, TaskClosureFragmentNew taskClosureFragment) {
        this.mInflater = LayoutInflater.from(context);
        this.mNotesList = resultDataList;
        this.mContext = context;
        this.mClickListener = taskClosureFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.task_closure_note_item_new, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotesAdapterNew.ViewHolder viewHolder, int i) {
        TaskClosure.TechnicalNotedto notes = mNotesList.get(i);

        if (i == 0) {
            //Add/remove/align views
            viewHolder.textViewAddOrRemove.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_plus));
            viewHolder.textViewNoteSummary.setText(R.string.add_new_notes);
            viewHolder.textViewNoteNo.setText("");
        } else {
            //Add/remove/align views
//            viewHolder.mRelativeLayoutNotesWrapper.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_whitesolid_curveborder));
//            viewHolder.textViewNoteNo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_primarysolid_leftupperlower_curve));
            viewHolder.textViewAddOrRemove.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_cross));
            viewHolder.textViewNoteSummary.setGravity(Gravity.CENTER_VERTICAL);
            viewHolder.textViewNoteSummary.setPadding(10, 0, 0, 0);

            //Assign value
            viewHolder.textViewNoteNo.setText(Integer.toString(notes.getId()));
            viewHolder.textViewNoteSummary.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_grey600));
            if (notes.getTechnicalNote1() != null && !notes.getTechnicalNote1().equalsIgnoreCase("")) {
                viewHolder.textViewNoteSummary.setText(notes.getTechnicalNote1());
            } else {
                viewHolder.textViewNoteSummary.setText("");
                viewHolder.textViewNoteSummary.setHint(R.string.click_here_note_details);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mNotesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, NotesUpdateListener {
        TextView textViewNoteNo, textViewNoteSummary, textViewAddOrRemove;
        NotesUpdateListener mNotesUpdateListener;
        RelativeLayout mRelativeLayoutNotesWrapper;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setIsRecyclable(false);
            mRelativeLayoutNotesWrapper = itemView.findViewById(R.id.relative_layout_notes_header_wrapper);
            textViewNoteNo = itemView.findViewById(R.id.textView_note_no);
            textViewNoteSummary = itemView.findViewById(R.id.textView_note_summary_text);
            textViewAddOrRemove = itemView.findViewById(R.id.textView_add_or_remove_note);
            textViewNoteSummary.setOnClickListener(this);
            textViewAddOrRemove.setOnClickListener(this);
            mNotesUpdateListener = this;
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
            switch (v.getId()) {
                case R.id.textView_note_summary_text:
                    if (getAdapterPosition() == 0) {
                        if (mNotesList.size() < 10) {
                            TaskClosure.TechnicalNotedto newNotes = new TaskClosure.TechnicalNotedto();
                            newNotes.setId(mNotesList.size());
                            restoreItem(newNotes, 1);
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //Navigate to notes dialog
                        TaskClosureDialogNew taskClosureDialog = TaskClosureDialogNew.getInstance();
                        taskClosureDialog.addNotesDialog(mContext, getAdapterPosition(), mNotesUpdateListener, mNotesList.get(getAdapterPosition()).getTechnicalNote1());
                    }
                    break;
                case R.id.textView_add_or_remove_note:
                    if (getAdapterPosition() == 0) {
                        if (mNotesList.size() < 10) {
                            TaskClosure.TechnicalNotedto newNotes = new TaskClosure.TechnicalNotedto();
                            newNotes.setId(mNotesList.size());
                            restoreItem(newNotes, 1);
                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //remove
                        removeItem(getAdapterPosition());
                        //also remove from array list of data
                    }
                    break;
            }
        }

        @Override
        public void onNotesAdded(String note, int pos) {
            if (note != null || !note.equalsIgnoreCase("")) {
                textViewNoteSummary.setError(null);
                textViewNoteSummary.setText(note);
            }
            if (pos != 0) mNotesList.get(pos).setTechnicalNote1(note);
        }
    }


    public TaskClosure.TechnicalNotedto getItem(int id) {
        return mNotesList.get(id);
    }

    public List<TaskClosure.TechnicalNotedto> getData() {
        return mNotesList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mNotesList.remove(position);
        for (int i = position - 1; i > 0; i--) {
            mNotesList.get(i).setId(mNotesList.get(i).getId() - 1);
        }
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void restoreItem(TaskClosure.TechnicalNotedto taskClosureNote, int position) {
        mNotesList.add(position, taskClosureNote);
        notifyItemInserted(position);
    }

    public interface NotesUpdateListener {
        void onNotesAdded(String note, int pos);
    }

}


