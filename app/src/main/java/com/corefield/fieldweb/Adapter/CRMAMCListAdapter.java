package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.CRMTask.CRMAMCkList;
import com.corefield.fieldweb.DTO.CRMTask.CRMTaskList;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.AMC.AMCDetailsFragment;
import com.corefield.fieldweb.FieldWeb.AMC.CRM_AMCDetailsFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CRMTaskDetailsFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of task added by the Owner
 * @see
 */
public class CRMAMCListAdapter extends RecyclerView.Adapter<CRMAMCListAdapter.ViewHolder> {

    private List<CRMAMCkList.ResultData> mAmcList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private static String TAG = CRMAMCListAdapter.class.getSimpleName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    CRMAMCkList.ResultData tasksList;


    public CRMAMCListAdapter(Context context, List<CRMAMCkList.ResultData> mAmcList, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mAmcList = new ArrayList<>();
        this.mAmcList = mAmcList;
        this.mClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (position) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.task_list_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.crm_amc_list_new, parent, false);
        viewHolder = new ViewHolder(v1);
        return viewHolder;
    }

    protected class LoadingVH extends ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        tasksList = mAmcList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder viewFHolder = (ViewHolder) viewHolder;
                if (tasksList != null)
                    setTaskRowDetails(viewFHolder, tasksList);
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }

    private void setTaskRowDetails(ViewHolder viewHolder, CRMAMCkList.ResultData tasksList) {
        viewHolder.txtAmcName.setText(tasksList.getAMCName());
        viewHolder.txtServiceLeft.setText(String.valueOf(tasksList.getAMCServicesLeftCount()));
        try {
            if (tasksList.getActivationDate() != null) {
                String[] activationDate = tasksList.getActivationDate().split("T");
                String date = DateUtils.convertDateFormat(activationDate[0], "yyyy-MM-dd", "dd-MM-yyyy");
                viewHolder.txtActivationDate.setText(date);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        viewHolder.txtNoofServices.setText(String.valueOf(tasksList.getTotalServices()));
        viewHolder.txtserviceOccurance.setText(String.valueOf(tasksList.getServiceOccuranceType()));

        try {
            if (tasksList.getIsActive() == true) {
                viewHolder.txtAmcStatus.setText("Active");
                viewHolder.txtAmcStatus.setTextColor(mContext.getResources().getColor(R.color.green));
            } else {
                viewHolder.txtAmcStatus.setText("DeActive");
                viewHolder.txtAmcStatus.setTextColor(mContext.getResources().getColor(R.color.dark_gray));
            }
        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public int getItemCount() {
        return mAmcList == null ? 0 : mAmcList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtAmcName, txtAmcStatus, txtActivationDate,
                txtNoofServices, txtserviceOccurance, txtServiceLeft;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            txtAmcName = view.findViewById(R.id.txtAmcName);
            txtAmcStatus = view.findViewById(R.id.txtAmcStatus);
            txtActivationDate = view.findViewById(R.id.txtActivationDate);
            txtNoofServices = view.findViewById(R.id.txtNoofServices);
            txtserviceOccurance = view.findViewById(R.id.txtserviceOccurance);
            txtServiceLeft = view.findViewById(R.id.txtServiceLeft);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
            try {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                CRM_AMCDetailsFragment amcDetailsFragment = new CRM_AMCDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("AMCsId", mAmcList.get(getAdapterPosition()).getAMCsId());
                // bundle.putInt("AMCServiceDetailsId", mAmcList.get(getAdapterPosition()).getAMCServiceDetailsId);
                //bundle.putString("AMCTypeName", mAmcList.get(getAdapterPosition()).getam);
                amcDetailsFragment.setArguments(bundle);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, amcDetailsFragment).addToBackStack(null).commit();
            } catch (
                    Exception ex) {
                ex.getMessage();
            }
        }

    }

    @Override
    public int getItemViewType(int position) {
        return (position == mAmcList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public CRMAMCkList.ResultData getItem(int id) {
        return mAmcList.get(id);
    }

    public List<CRMAMCkList.ResultData> getData() {
        return mAmcList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

}


