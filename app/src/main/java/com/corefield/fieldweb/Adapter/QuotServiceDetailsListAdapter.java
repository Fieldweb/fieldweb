package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class QuotServiceDetailsListAdapter extends RecyclerView.Adapter<QuotServiceDetailsListAdapter.ViewHolder> {

    private List<QuotationDetailsDTO.QuoteItemList> mItemsList;
    private LayoutInflater mInflater;
    List<QuotationDetailsDTO.QuoteServiceList> multipleServiceArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public QuotServiceDetailsListAdapter(Context context, List<QuotationDetailsDTO.QuoteServiceList> multipleServiceArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleServiceArrayList = multipleServiceArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.quotation_servicedetails_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        QuotationDetailsDTO.QuoteServiceList itemList = multipleServiceArrayList.get(position);
        try {
            viewHolder.txtServiceName.setText(itemList.getServiceName());//String.valueOf(itemList.getServiceId())
            viewHolder.txtServicePrice.setText("₹"+String.valueOf(itemList.getPrice()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleServiceArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtServiceName, txtServicePrice;

        ViewHolder(View view) {
            super(view);
            txtServiceName = view.findViewById(R.id.txtServiceName);
            txtServicePrice = view.findViewById(R.id.txtServicePrice);

        }

    }

    public QuotationDetailsDTO.QuoteServiceList getItem(int id) {
        return multipleServiceArrayList.get(id);
    }

    public List<QuotationDetailsDTO.QuoteServiceList> getData() {
        return multipleServiceArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(QuotationDetailsDTO.QuoteItemList itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


