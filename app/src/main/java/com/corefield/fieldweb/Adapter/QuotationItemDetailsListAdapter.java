package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class QuotationItemDetailsListAdapter extends RecyclerView.Adapter<QuotationItemDetailsListAdapter.ViewHolder> {

    private List<QuotationDetailsDTO.QuoteItemList> mItemsList;
    private LayoutInflater mInflater;
    List<QuotationDetailsDTO.QuoteItemList> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public QuotationItemDetailsListAdapter(Context context, List<QuotationDetailsDTO.QuoteItemList> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.quotation_itemdetails_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        QuotationDetailsDTO.QuoteItemList itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtItemDetails.setText(String.valueOf(itemList.getItemName()));
            viewHolder.txtQty.setText(String.valueOf(itemList.getQuantity()));
            viewHolder.txtUnitPrice.setText("₹" + String.valueOf(itemList.getUnitPrice()));
            viewHolder.txtTotalPrice.setText("₹" + String.valueOf(itemList.getTotalPrice()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtItemDetails, txtQty, txtUnitPrice, txtTotalPrice;

        ViewHolder(View view) {
            super(view);
            txtItemDetails = view.findViewById(R.id.txtItemDetails);
            txtQty = view.findViewById(R.id.txtQty);
            txtUnitPrice = view.findViewById(R.id.txtUnitPrice);
            txtTotalPrice = view.findViewById(R.id.txtTotalPrice);

        }

    }

    public QuotationDetailsDTO.QuoteItemList getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<QuotationDetailsDTO.QuoteItemList> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(QuotationDetailsDTO.QuoteItemList itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


