package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.InvoiceDetailsDTO;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class InvoicePaymentDetailsListAdapter extends RecyclerView.Adapter<InvoicePaymentDetailsListAdapter.ViewHolder> {

    private List<InvoiceDetailsDTO.PaymentList> mItemsList;
    private LayoutInflater mInflater;
    List<InvoiceDetailsDTO.PaymentList> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public InvoicePaymentDetailsListAdapter(Context context, List<InvoiceDetailsDTO.PaymentList> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.invoice_paymentdetails_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        InvoiceDetailsDTO.PaymentList paymentList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtDate.setText(String.valueOf(paymentList.getPaymentDate()));
            viewHolder.txtPaymentType.setText(paymentList.getPaymentTransactionType());
            viewHolder.txtReceivedAmt.setText("₹" + String.valueOf(paymentList.getAmount()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtDate, txtReceivedAmt, txtPaymentType;

        ViewHolder(View view) {
            super(view);
            txtDate = view.findViewById(R.id.txtDate);
            txtPaymentType = view.findViewById(R.id.txtPaymentType);
            txtReceivedAmt = view.findViewById(R.id.txtReceivedAmt);
        }

    }

    public InvoiceDetailsDTO.PaymentList getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<InvoiceDetailsDTO.PaymentList> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(InvoiceDetailsDTO.PaymentList paymentList, int position) {
        mItemsList.add(position, paymentList);
        notifyItemInserted(position);
    }
}


