package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.InvoiceDetailsDTO;
import com.corefield.fieldweb.DTO.Account.QuotationDetailsDTO;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class InvoiceItemDetailsListAdapter extends RecyclerView.Adapter<InvoiceItemDetailsListAdapter.ViewHolder> {

    private List<InvoiceDetailsDTO.ItemList> mItemsList;
    private LayoutInflater mInflater;
    List<InvoiceDetailsDTO.ItemList> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public InvoiceItemDetailsListAdapter(Context context, List<InvoiceDetailsDTO.ItemList> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.quotation_itemdetails_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        InvoiceDetailsDTO.ItemList itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtItemDetails.setText(String.valueOf(itemList.getItemName()));
            viewHolder.txtQty.setText(String.valueOf(itemList.getQuantity()));
            viewHolder.txtUnitPrice.setText("₹" + String.valueOf(itemList.getUnitPrice()));
            viewHolder.txtTotalPrice.setText("₹" + String.valueOf(itemList.getTotalPrice()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtItemDetails, txtQty, txtUnitPrice, txtTotalPrice;

        ViewHolder(View view) {
            super(view);
            txtItemDetails = view.findViewById(R.id.txtItemDetails);
            txtQty = view.findViewById(R.id.txtQty);
            txtUnitPrice = view.findViewById(R.id.txtUnitPrice);
            txtTotalPrice = view.findViewById(R.id.txtTotalPrice);

        }

    }

    public InvoiceDetailsDTO.ItemList getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<InvoiceDetailsDTO.ItemList> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(InvoiceDetailsDTO.ItemList itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


