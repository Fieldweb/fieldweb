package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.InvoiceListDTO;
import com.corefield.fieldweb.DTO.Account.QuotationListDTO;
import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Accounts.InvoiceDetailsFragment;
import com.corefield.fieldweb.FieldWeb.Accounts.QuotationDetailsFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of task added by the Owner
 * @see
 */
public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> /*implements Filterable*/ {

    //private List<InvoiceListDTO.ResultData> mInvoiceList;
    private List<InvoiceListDTO.ResultData> mResultDataList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private static String TAG = InvoiceListAdapter.class.getSimpleName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private List<InvoiceListDTO.ResultData> mInvoiceList;


    public InvoiceListAdapter(Context context, List<InvoiceListDTO.ResultData> mmInvoiceList, RecyclerTouchListener mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        //this.mInvoiceList = mInvoiceList;
        //this.mResultDataList = mInvoiceList;
        mInvoiceList = new ArrayList<>();
        this.mClickListener = mClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (position) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.task_list_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.invoice_list, parent, false);
        viewHolder = new ViewHolder(v1);
        return viewHolder;
    }

    protected class LoadingVH extends ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        InvoiceListDTO.ResultData tasksList = mInvoiceList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder viewFHolder = (ViewHolder) viewHolder;
                if (tasksList != null) setTaskRowDetails(viewFHolder, tasksList);
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }

    private void setTaskRowDetails(ViewHolder viewHolder, InvoiceListDTO.ResultData invoiceListData) {

        viewHolder.mTaskName.setText(invoiceListData.getQuoteTaskName());
        viewHolder.mTextViewID.setText("[" + "INV" + invoiceListData.getId() + "]");
        viewHolder.minvoiceCustName.setText(invoiceListData.getCustomerName());
        viewHolder.mtxtTotalAmt.setText("₹ " + String.valueOf(invoiceListData.getInvoiceAmount()));
        viewHolder.mTextViewPendingAmt.setText("₹ " + String.valueOf(invoiceListData.getRemainingAmount()));
        try {
            String formattedDate = DateUtils.convertDateFormat(invoiceListData.getCreatedDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
            viewHolder.mtxtInvoiceDate.setText(formattedDate);
        } catch (Exception ex) {
            ex.getMessage();
        }

        try {
            if (invoiceListData.getStatusName() != null || !invoiceListData.getStatusName().isEmpty()) {
                switch (invoiceListData.getStatusName()) {
                    case "Paid":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.completed_background));
                        viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.paid));
                        viewHolder.mtxtAmt.setText("Paid Amount :");
                        viewHolder.mTextViewTaskStatus.setVisibility(View.GONE);
                        viewHolder.mTextViewPendingAmt.setVisibility(View.GONE);
                        break;
                    case "UnPaid":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.rejected_background));
                        viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.unpaid));
                        viewHolder.mtxtAmt.setText("Pending Amount :");
                        viewHolder.mtxtAmt.setTextColor(mContext.getResources().getColor(R.color.red));
                        viewHolder.mtxtTotalAmt.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                        viewHolder.mTextViewTaskStatus.setVisibility(View.GONE);
                        viewHolder.mTextViewPendingAmt.setVisibility(View.GONE);
                        break;
                    case "Partial Paid":
                        viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.ongoing_background));
                        viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.partialpaid));
                        viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.orange));
                        break;
                }
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return mInvoiceList == null ? 0 : mInvoiceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTaskName, minvoiceCustName, mTechName, mtxtAmt, mtxtTotalAmt, mtxtInvoiceDate, mTextViewTaskDate, mTextViewTaskStatus, mTextViewPendingAmt, mTextViewID, mTextViewType, mTextViewReassign;
        private ImageView mProfile, mPNRImageView, mImageViewPaymentMode;
        private CardView mCardViewTask;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mTaskName = view.findViewById(R.id.task_name);
            mTextViewID = view.findViewById(R.id.task_id);
            minvoiceCustName = view.findViewById(R.id.invoiceCustName);
            mtxtAmt = view.findViewById(R.id.txtamt);
            mtxtTotalAmt = view.findViewById(R.id.txtTotalAmt);
            mTextViewPendingAmt = view.findViewById(R.id.pendingamt);
            mtxtInvoiceDate = view.findViewById(R.id.txtInvoiceDate);

            mPNRImageView = view.findViewById(R.id.imageView_pnr);
            mImageViewPaymentMode = view.findViewById(R.id.imageView_payment_mode);
            mTechName = view.findViewById(R.id.task_tech_name);
            mTextViewReassign = view.findViewById(R.id.textView_reassign_from_list);
            mTextViewTaskStatus = view.findViewById(R.id.task_status_text);
            mTextViewTaskDate = view.findViewById(R.id.task_date);
            mTextViewType = view.findViewById(R.id.textView_type);
            mCardViewTask = view.findViewById(R.id.task_card_view);

            if (mTextViewReassign != null) {
                mTextViewReassign.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mClickListener.onClick(v, getAdapterPosition());
                    }
                });
            }
            mProfile = view.findViewById(R.id.task_profile_pic);
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition());
            ((HomeActivityNew) mContext).mToolbar.setTitle(R.string.invoice_details);
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            InvoiceDetailsFragment myFragment = new InvoiceDetailsFragment();
            Bundle args = new Bundle();
            args.putString("invoiceStatus", mInvoiceList.get(getAdapterPosition()).getStatusName());
            args.putInt("invoiceID", mInvoiceList.get(getAdapterPosition()).getId());
            args.putString("invoiceName", mInvoiceList.get(getAdapterPosition()).getQuoteTaskName());
            myFragment.setArguments(args);
            /*Bundle args = new Bundle();
            args.putInt("serviceID", mItemsListFilter.get(getAdapterPosition()).getId());
            args.putString("serviceName", mItemsListFilter.get(getAdapterPosition()).getServiceName());
            args.putInt("servicePrice", mItemsListFilter.get(getAdapterPosition()).getPrice());
            args.putString("contactNo", mItemsListFilter.get(getAdapterPosition()).getContactNo());
            args.putString("description", mItemsListFilter.get(getAdapterPosition()).getDescription());
            myFragment.setArguments(args);*/
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
        }

    }

    @Override
    public int getItemViewType(int position) {
        return (position == mInvoiceList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public InvoiceListDTO.ResultData getItem(int id) {
        return mInvoiceList.get(id);
    }

    public List<InvoiceListDTO.ResultData> getData() {
        return mInvoiceList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void add(InvoiceListDTO.ResultData resultData) {
        mInvoiceList.add(resultData);
        notifyItemInserted(mInvoiceList.size() - 1);
    }

    public void addAll(List<InvoiceListDTO.ResultData> mInvoiceList) {
        for (InvoiceListDTO.ResultData resultData : mInvoiceList) {
            add(resultData);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new InvoiceListDTO.ResultData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mInvoiceList.size() - 1;
        if (position >= 0) {
            InvoiceListDTO.ResultData item = getItem(position);

            if (item != null) {
                mInvoiceList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

   /* @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mInvoiceList = mResultDataList;
                } else {
                    List<InvoiceListDTO.ResultData> filteredList = new ArrayList<>();
                    for (InvoiceListDTO.ResultData row : mResultDataList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getInvoiceCode().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCustomerName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mInvoiceList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mInvoiceList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mInvoiceList = (ArrayList<InvoiceListDTO.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }*/


    public void removeItem(int position) {
        mInvoiceList.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(TasksList.ResultData resultData) {
        int position = mInvoiceList.indexOf(resultData);
        if (position > -1) {
            mInvoiceList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void restoreItem(InvoiceListDTO.ResultData invoiceList, int position) {
        mInvoiceList.add(position, invoiceList);
        notifyItemInserted(position);
    }
}


