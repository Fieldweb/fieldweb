package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.LeadManagement.LeadDetailsDTO;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class ProductFeatureListAdapter extends RecyclerView.Adapter<ProductFeatureListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    List<LeadDetailsDTO.LeadStatusLogObj> leadStatusLogObjs;
    private Context mContext;

    /**
     * Constructor
     *
     * @param context
     */
    public ProductFeatureListAdapter(Context context, List<LeadDetailsDTO.LeadStatusLogObj> leadStatusLogObjs) {
        this.mInflater = LayoutInflater.from(context);
        this.leadStatusLogObjs = leadStatusLogObjs;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.product_list_features_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        LeadDetailsDTO.LeadStatusLogObj leadStatusLogObj = leadStatusLogObjs.get(position);
        try {
            if (leadStatusLogObj.getLeadNotes() != null) {
                viewHolder.mtxtLabel.setText("Feature " + position + ":");
                viewHolder.mtxtFeatureName.setText(leadStatusLogObj.getLeadNotes());

            } else {
                viewHolder.mtxtLabel.setText("Feature " + position + ":");
                viewHolder.mtxtFeatureName.setText("-NA-");
            }

           /* String formattedDate = DateUtils.convertDateFormat(leadStatusLogObj.getCreatedDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy hh:mm aa");
            if (formattedDate != null)
                viewHolder.mtxtLeadDateTime.setText(formattedDate);*/

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return leadStatusLogObjs.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mtxtLabel, mtxtFeatureName;

        ViewHolder(View view) {
            super(view);
            mtxtLabel = view.findViewById(R.id.txtLabel);
            mtxtFeatureName = view.findViewById(R.id.txtFeatureName);

        }

    }

    public LeadDetailsDTO.LeadStatusLogObj getItem(int id) {
        return leadStatusLogObjs.get(id);
    }

    public List<LeadDetailsDTO.LeadStatusLogObj> getData() {
        return leadStatusLogObjs;
    }

    /*public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }*/
}


