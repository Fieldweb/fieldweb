package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Enquiry.EnquiryList;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;

import java.util.ArrayList;
import java.util.List;

public class EnquiryListAdapterNew extends RecyclerView.Adapter<EnquiryListAdapterNew.ViewHolder> implements Filterable {

    protected final String TAG = EnquiryListAdapterNew.class.getSimpleName();
    private List<EnquiryList.ResultData> mResultDataListFilter;
    private List<EnquiryList.ResultData> mResultDataList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;

    public EnquiryListAdapterNew(List<EnquiryList.ResultData> resultDataList, Context context, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResultDataList = resultDataList;
        this.mResultDataListFilter = resultDataList;
        this.mContext = context;
        this.mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.enquirylists_custom_new, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        EnquiryList.ResultData resultData = mResultDataListFilter.get(i);
        viewHolder.textViewName.setText(resultData.getServiceName());
        viewHolder.textViewEnquiryNo.setText("#" + resultData.getEnquiryNo());

        /*if (resultData.getMobileNumber() != null && !resultData.getMobileNumber().isEmpty()) {
            viewHolder.linearMobile.setVisibility(View.VISIBLE);
            SpannableString spanString = new SpannableString(resultData.getMobileNumber());
            spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
            viewHolder.textViewCustNumber.setText(spanString);
        } else viewHolder.linearMobile.setVisibility(View.GONE);*/

        if (resultData.getCustomerName() != null && !resultData.getCustomerName().isEmpty()) {
            viewHolder.linearMobile.setVisibility(View.VISIBLE);
            String name = resultData.getCustomerName();
            viewHolder.textCustName.setText(name);
        } else viewHolder.linearMobile.setVisibility(View.GONE);

        if (resultData.getPreferableDate() != null && !resultData.getPreferableDate().isEmpty()) {
            String prefDate = resultData.getPreferableDate().split("T")[0];
            viewHolder.textViewEnquiryDate.setText(DateUtils.convertDateFormat(prefDate, "yyyy-MM-dd", "dd-MM-yyyy"));
        }
        viewHolder.textViewAddress.setText(resultData.getAddress());
    }

    @Override
    public int getItemCount() {
        FWLogger.logInfo("getItemCount = ", "" + mResultDataListFilter.size());
        return mResultDataListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewEnquiryNo, textViewEnquiryDate, textViewName, textCustName, textViewAddress, textViewCustNumber;//, textViewService, textViewContact, textViewTaskType, textViewCreateTask;
        ImageView imageViewEdit, imageViewCall;
        LinearLayout linearMobile;
        View layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = (View) itemView.findViewById(R.id.enquiry_card_view);
            textViewEnquiryNo = itemView.findViewById(R.id.textView_enq_id);
            textViewAddress = itemView.findViewById(R.id.textView_address);
            textViewName = itemView.findViewById(R.id.textView_service_name);
            textViewEnquiryDate = itemView.findViewById(R.id.textView_date);
            textCustName = itemView.findViewById(R.id.textView_customer_name);
            textViewCustNumber = itemView.findViewById(R.id.textView_customer_number);
            imageViewEdit = itemView.findViewById(R.id.imageView_edit);
            imageViewCall = itemView.findViewById(R.id.imageView_call_cust);

            linearMobile = itemView.findViewById(R.id.linear_mobile);

            textViewCustNumber.setOnClickListener(this);
            imageViewEdit.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);

            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }
    }

    public EnquiryList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<EnquiryList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mResultDataList;
                } else {
                    List<EnquiryList.ResultData> filteredList = new ArrayList<>();
                    for (EnquiryList.ResultData row : mResultDataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getServiceName() != null && row.getCustomerName() != null && row.getTaskTypeName() != null
                                && String.valueOf(row.getEnquiryNo()) != null && row.getPrefDate() != null) {
                            /*if (row.getServiceName().toLowerCase().contains(charString) || String.valueOf(row.getEnquiryNo()).contains(charSequence)
                                    || row.getPrefDate().toLowerCase().contains(charString) || row.getCustomerName().toLowerCase().contains(charString)
                                    || row.getTaskTypeName().toLowerCase().contains(charString) || row.getMobileNumber().contains(charString)) {*/
                            if (row.getServiceName().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getCustomerName().toLowerCase().contains(charString.toLowerCase())
                                    || String.valueOf(row.getEnquiryNo()).contains(charSequence) ||
                                    row.getPrefDate().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getCustomerName().toLowerCase().contains(charString.toLowerCase()) ||
                                    row.getTaskTypeName().toLowerCase().contains(charString.toLowerCase()) ||
                                    (row.getMobileNumber() != null && row.getMobileNumber().contains(charString))) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<EnquiryList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(EnquiryList.ResultData usersList, int position) {
        mResultDataListFilter.add(position, usersList);
        notifyItemInserted(position);
    }

}


