package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.ServiceManagement.EnquiryServiceTypeDTO;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.ViewUtils;
import com.google.android.material.textfield.TextInputLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the Technician list in Add Bulk Technician
 * This adapter can add or remove item smoothly.
 * //
 **/
public class AddQuoteServiceAdapter extends RecyclerView.Adapter<AddQuoteServiceAdapter.ViewHolder> {

    protected final String TAG = AddQuoteServiceAdapter.class.getSimpleName();
    //public List<AddItem.Resultdata> mTechLists;
    /*public AddItem.Resultdata mAddItem;*/
    public AddTask.MultipleItemAssigned mAddItem;
    private LayoutInflater mInflater;
    private Context mContext;
    //private RecyclerTouchListener mClickListener;
    private int mPosition;
    private String mMessage = "";
    List<EnquiryServiceTypeDTO.ResultData> serviceNameList;
    boolean isSameNameNotExists = false;
    //
    ArrayList<AddTask> selectedItemlArray = new ArrayList<AddTask>();
    public List<EnquiryServiceTypeDTO.ResultData> mTechLists;
    MultipleServicePostListener listener;
    ArrayList<EnquiryServiceTypeDTO.ResultData> selectedMaterialArray = new ArrayList<EnquiryServiceTypeDTO.ResultData>();

    private Pattern mPattern;
    private Matcher mMatcher;
    private boolean mResult = false;
    private String mCheckCharPattern = "", selectedItem = "";
    int selectedIndex;
    Map<Integer, Integer> mSpinnerSelectedItem = new HashMap<Integer, Integer>();
    ArrayAdapter<String> adapter;

    public AddQuoteServiceAdapter(MultipleServicePostListener listener, List<EnquiryServiceTypeDTO.ResultData> resultDataList, Context context, List<EnquiryServiceTypeDTO.ResultData> serviceNameList) {
        this.mInflater = LayoutInflater.from(context);
        /*this.mTechLists = resultDataList;*/
        this.mTechLists = resultDataList;
        this.mContext = context;
        this.serviceNameList = serviceNameList;
        this.listener = listener;
        //this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.add_bulk_service_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        /*AddItem.Resultdata techResult = mTechLists.get(position);*/
        EnquiryServiceTypeDTO.ResultData techResult = mTechLists.get(position);

        if (position == mTechLists.size() - 1) {
            viewHolder.mTextViewAddMore.setVisibility(View.VISIBLE);
        } else {
            if (viewHolder.mTextViewAddMore.getVisibility() == View.VISIBLE) {
                viewHolder.mTextViewAddMore.setVisibility(View.GONE);
            }
        }
        viewHolder.mImageButtonCancelAddTech.setVisibility(View.VISIBLE);
        viewHolder.mTextViewTechnicianNo.setText(mContext.getString(R.string.service) + " #" + (position + 1));
    }

    @Override
    public int getItemCount() {
        return mTechLists.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        TextView mTextViewTechnicianNo, mTextViewAddMore, txtUnassignQty;
        ImageButton mImageButtonCancelAddTech;
        TextInputLayout txtMobileLayout, txtEmailLayout;
        SearchableSpinner items;
        EditText edittextPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //setIsRecyclable(false);
            mTextViewAddMore = itemView.findViewById(R.id.textView_add_more);
            mTextViewTechnicianNo = itemView.findViewById(R.id.textView_technician_no);
            mImageButtonCancelAddTech = itemView.findViewById(R.id.imageButton_cancel_add_tech);
            //
            txtMobileLayout = itemView.findViewById(R.id.txtMobileLayout);
            txtEmailLayout = itemView.findViewById(R.id.txtEmailLayout);

            items = (SearchableSpinner) itemView.findViewById(R.id.spin_items);
            txtUnassignQty = itemView.findViewById(R.id.txtUnassignQty);
            edittextPrice = itemView.findViewById(R.id.edittextPrice);

            ((HomeActivityNew) mContext).mServiceTypeList.add(0, "Select Service Type");

            adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, ((HomeActivityNew) mContext).mServiceTypeList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            items.setAdapter(adapter);

            items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        //editTextItemQty.setText("");
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                    } else {

                        try {
                            for (int i = 0; i < serviceNameList.size(); i++) {
                                if (items.getSelectedItem().toString().equalsIgnoreCase(serviceNameList.get(i).getServiceName())) {
                                    edittextPrice.setText(String.valueOf(serviceNameList.get(i).getPrice()));
                                    EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
                                    resultData.setId(serviceNameList.get(i).getId());
                                    resultData.setServiceName(serviceNameList.get(i).getServiceName());
                                    resultData.setPrice(serviceNameList.get(i).getPrice());
                                    selectedMaterialArray.add(resultData);
                                    //selectedMaterialArray.get(getAdapterPosition()).setServiceName((serviceNameList.get(getAdapterPosition()).getServiceName()));
                                    listener.onMultipleServicePost(selectedMaterialArray);
                                }

                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }

                       /* if (position > 0) {
                            edittextPrice.setText(String.valueOf(serviceNameList.get(getAdapterPosition()).getPrice()));
                        }

                        EnquiryServiceTypeDTO.ResultData resultData = new EnquiryServiceTypeDTO.ResultData();
                        resultData.setId(serviceNameList.get(position).getId());
                        resultData.setServiceName(serviceNameList.get(position).getServiceName());
                        resultData.setPrice(serviceNameList.get(position).getPrice());
                        selectedMaterialArray.add(resultData);
                        //selectedMaterialArray.get(getAdapterPosition()).setServiceName((serviceNameList.get(getAdapterPosition()).getServiceName()));
                        listener.onMultipleServicePost(selectedMaterialArray);*/

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            mTextViewAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTechLists.size() < 10) {
                        levelOneValidation();
                    } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mImageButtonCancelAddTech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = getAdapterPosition();
                    if (adapterPosition > 0 && adapterPosition < mTechLists.size()) {
                        removeItem(adapterPosition);
                    } else {
                        ((HomeActivityNew) mContext).mToolbar.setTitle(R.string.fieldweb);
                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        OwnerDashboardFragment ownerDashboardFragment = new OwnerDashboardFragment();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, ownerDashboardFragment).addToBackStack(null).commit();
                    }
                }
            });
        }

        public void levelOneValidation() {
            try {
                if (items.getSelectedItem().equals("Select Service Type")) {
                    Toast.makeText(mContext.getApplicationContext(), "Please Select a Service!!", Toast.LENGTH_LONG).show();
                } else {
                    addNewTech();
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    private void addNewTech() {
        ViewUtils.hideKeyboard((HomeActivityNew) mContext);
        ((HomeActivityNew) mContext).mServiceTypeList.remove(0);

        EnquiryServiceTypeDTO.ResultData multipleItemAssigned = new EnquiryServiceTypeDTO.ResultData();
       /* multipleItemAssigned.setTempTechSrNo(mTechLists.size());
        multipleItemAssigned.setItemId(0);
        multipleItemAssigned.setItemName("");
        multipleItemAssigned.setItemQuantity(0);*/
        multipleItemAssigned.setId(mTechLists.size());
        multipleItemAssigned.setServiceName("");
        multipleItemAssigned.setPrice(0);
        //To handle the set error in onBindViewHolder
        if (mMessage != null && mMessage.equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, mContext))) {
            mMessage = "";
        }
        restoreItem(multipleItemAssigned, mTechLists.size());
    }

    public EnquiryServiceTypeDTO.ResultData getItem(int id) {
        return mTechLists.get(id);
    }

    public List<EnquiryServiceTypeDTO.ResultData> getData() {
        return mTechLists;
    }

    /*public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }*/

    private boolean isValidPhone(EditText text) {
        CharSequence phone = text.getText().toString();
        return phone.length() != 10;
    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private boolean isContainsSpace(EditText text) {
        return text.getText().toString().contains(" ");
    }

    public void removeItem(int position) {
        try {
            mTechLists.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();

            // Update visibility of "Add More" button for the new last item in the list
            if (mTechLists.size() > 0) {
                notifyItemChanged(mTechLists.size() - 1);
            }
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public void restoreItem(EnquiryServiceTypeDTO.ResultData techList, int position) {
        mTechLists.add(position, techList);
        notifyItemInserted(position);
        notifyDataSetChanged();
    }

    public interface MultipleServicePostListener {
        void onMultipleServicePost(ArrayList<EnquiryServiceTypeDTO.ResultData> SODArray);
    }


}
