package com.corefield.fieldweb.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

import com.corefield.fieldweb.FieldWeb.Admin.CustomerAppFragment;
import com.corefield.fieldweb.FieldWeb.Admin.FieldFinzFragment;
import com.corefield.fieldweb.FieldWeb.Admin.FieldStaffingFragment;
import com.corefield.fieldweb.FieldWeb.Admin.FieldTradesFragment;
import com.corefield.fieldweb.FieldWeb.Admin.FieldWebFixitFragment;
import com.corefield.fieldweb.FieldWeb.Task.CRMTaskDetailsFragmentNew;
import com.corefield.fieldweb.FieldWeb.YouTube.YouTubeCustAppActivity;
import com.corefield.fieldweb.R;

import java.util.ArrayList;

public class ServiceSliderAdapter extends PagerAdapter {
    ArrayList<Integer> images;
    LayoutInflater inflater;
    Context mContext;

    public ServiceSliderAdapter(Context context, ArrayList<Integer> images) {
        this.images = images;
        inflater = LayoutInflater.from(context);
        this.mContext = context;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.slide, container, false);
        ImageView myImage = (ImageView) v.findViewById(R.id.image);
        //change here
        myImage.setImageResource(images.get(position));

        container.addView(v, 0);
        return v;
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
