package com.corefield.fieldweb.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.FieldWeb.Dialogs.CommonDialog;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to view the Device list in Task Details form
 * <p>
 * //
 **/
public class DeviceListViewAdapter extends RecyclerView.Adapter<DeviceListViewAdapter.ViewHolder> {

    protected final String TAG = DeviceListViewAdapter.class.getSimpleName();
    private List<TaskClosure.DeviceInfoList> mDeviceLists;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private BigDecimal num;

    public DeviceListViewAdapter(List<TaskClosure.DeviceInfoList> resultDataList, Activity activity) {
        this.mInflater = LayoutInflater.from(activity);
        this.mDeviceLists = resultDataList;
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.task_closure_device_view_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        TaskClosure.DeviceInfoList deviceList = mDeviceLists.get(i);

        viewHolder.mDeviceImageFirst.setVisibility(View.GONE);
        viewHolder.mDeviceImageSecond.setVisibility(View.GONE);
        viewHolder.mDeviceImageThird.setVisibility(View.GONE);

        //set device details
        setDeviceDetails(viewHolder, deviceList);

        //set Device photos
        setDevicePhotos(viewHolder, deviceList);
    }

    private void setDevicePhotos(@NonNull ViewHolder viewHolder, TaskClosure.DeviceInfoList deviceList) {
        if (deviceList.getDevicePhoto1() != null && !deviceList.getDevicePhoto1().equalsIgnoreCase("")) {
            viewHolder.mDeviceImageFirst.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(deviceList.getDevicePhoto1())
                    .placeholder(R.drawable.ic_camera)
                    .error(R.drawable.ic_camera)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mDeviceImageFirst, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            viewHolder.mDeviceImageFirst.setImageResource(R.drawable.ic_camera);
                        }
                    });
        } else {
            viewHolder.mDeviceImageFirst.setVisibility(View.GONE);
        }

        if (deviceList.getDevicePhoto2() != null && !deviceList.getDevicePhoto2().equalsIgnoreCase("")) {
            viewHolder.mDeviceImageSecond.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(deviceList.getDevicePhoto2())
                    .placeholder(R.drawable.ic_camera)
                    .error(R.drawable.ic_camera)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mDeviceImageSecond, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            viewHolder.mDeviceImageSecond.setImageResource(R.drawable.ic_camera);
                        }
                    });
        } else {
            viewHolder.mDeviceImageSecond.setVisibility(View.GONE);
        }

        if (deviceList.getDevicePhoto3() != null && !deviceList.getDevicePhoto3().equalsIgnoreCase("")) {
            viewHolder.mDeviceImageThird.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(deviceList.getDevicePhoto3())
                    .placeholder(R.drawable.ic_camera)
                    .error(R.drawable.ic_camera)
                    .transform(new RoundedTransformation(20, 4))
                    .fit()
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(viewHolder.mDeviceImageThird, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            viewHolder.mDeviceImageThird.setImageResource(R.drawable.ic_camera);
                        }
                    });
        } else {
            viewHolder.mDeviceImageThird.setVisibility(View.GONE);
        }
    }

    private void setDeviceDetails(@NonNull ViewHolder viewHolder, TaskClosure.DeviceInfoList deviceList) {
        //Set values
        if (deviceList.getDeviceName() != null && !deviceList.getDeviceName().equalsIgnoreCase(""))
            viewHolder.textViewDeviceName.setText(deviceList.getDeviceName());
        else
            viewHolder.textViewDeviceName.setText("");

        if (deviceList.getModelNumber() != null && !deviceList.getModelNumber().equalsIgnoreCase(""))
            viewHolder.mTextViewDeviceModelNo.setText(deviceList.getModelNumber());
        else
            viewHolder.mTextViewDeviceModelNo.setText("");

       /* if (deviceList.getDeviceReading() != 0)
            viewHolder.mTextViewDeviceReading.setText("" + deviceList.getDeviceReading());
        else
            viewHolder.mTextViewDeviceReading.setText("");*/

        if (deviceList.getDeviceReading() != 0) {
            num = BigDecimal.valueOf(deviceList.getDeviceReading());
            viewHolder.mTextViewDeviceReading.setText("" + num.toString());
        }
        else
            viewHolder.mTextViewDeviceReading.setText("");
    }

    @Override
    public int getItemCount() {
        return mDeviceLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewDeviceName, mTextViewDeviceModelNo, mTextViewDeviceReading;
        ImageView mDeviceImageFirst, mDeviceImageSecond, mDeviceImageThird;
        LinearLayout mDeviceImages;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //Header
            mDeviceImages = itemView.findViewById(R.id.device_images);
            textViewDeviceName = itemView.findViewById(R.id.textView_device_name_view);
            mTextViewDeviceModelNo = itemView.findViewById(R.id.textView_model_number_view);
            mTextViewDeviceReading = itemView.findViewById(R.id.textView_device_reading_view);
            mDeviceImageFirst = itemView.findViewById(R.id.device_image_first);
            mDeviceImageSecond = itemView.findViewById(R.id.device_image_second);
            mDeviceImageThird = itemView.findViewById(R.id.device_image_third);
            mDeviceImageFirst.setOnClickListener(this);
            mDeviceImageSecond.setOnClickListener(this);
            mDeviceImageThird.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            CommonDialog commonDialog = CommonDialog.getInstance();
            switch (v.getId()) {
                case R.id.device_image_first:
                    commonDialog.showImageInFullScreenMode(mActivity, mDeviceLists.get(getAdapterPosition()).getDevicePhoto1(), mActivity.getResources().getString(R.string.device_photo));
                    break;
                case R.id.device_image_second:
                    commonDialog.showImageInFullScreenMode(mActivity, mDeviceLists.get(getAdapterPosition()).getDevicePhoto2(), mActivity.getResources().getString(R.string.device_photo));
                    break;
                case R.id.device_image_third:
                    commonDialog.showImageInFullScreenMode(mActivity, mDeviceLists.get(getAdapterPosition()).getDevicePhoto3(), mActivity.getResources().getString(R.string.device_photo));
                    break;
            }
        }
    }

    public TaskClosure.DeviceInfoList getItem(int id) {
        return mDeviceLists.get(id);
    }

    public List<TaskClosure.DeviceInfoList> getData() {
        return mDeviceLists;
    }

}


