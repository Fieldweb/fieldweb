package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.FieldWeb.Home.OwnerDashboardFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.ViewUtils;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the Technician list in Add Bulk Technician
 * This adapter can add or remove item smoothly.
 * //
 **/
public class ReassignAddItemsAdapter extends RecyclerView.Adapter<ReassignAddItemsAdapter.ViewHolder> {

    protected final String TAG = ReassignAddItemsAdapter.class.getSimpleName();
    //public List<AddItem.Resultdata> mTechLists;
    /*public AddItem.Resultdata mAddItem;*/
    public TasksList.MultipleItemAssigned mAddItem;
    private LayoutInflater mInflater;
    private Context mContext;
    //private RecyclerTouchListener mClickListener;
    private int mPosition;
    private String mMessage = "";
    ArrayList<String> itemNameList;
    //
    ArrayList<AddTask> selectedItemlArray = new ArrayList<AddTask>();
    public List<TasksList.MultipleItemAssigned> mTechLists;
    ReassignItemPostListener listener;
    ArrayList<TasksList.MultipleItemAssigned> selectedMaterialArray = new ArrayList<TasksList.MultipleItemAssigned>();

    private boolean mResult = false;
    private String mCheckCharPattern = "";
    int itemPos;
    int selectedIndex;
    Map<Integer, Integer> mSpinnerSelectedItem = new HashMap<Integer, Integer>();
    ArrayAdapter<ItemsList.ResultData> adapter;

    public ReassignAddItemsAdapter(ReassignItemPostListener listener, List<TasksList.MultipleItemAssigned> resultDataList, Context context, ArrayList<String> itemNameList) {
        this.mInflater = LayoutInflater.from(context);
        this.mTechLists = resultDataList;
        this.mContext = context;
        this.itemNameList = itemNameList;
        this.listener = listener;
        //this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.add_bulk_item_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        TasksList.MultipleItemAssigned techResult = mTechLists.get(position);
        if (techResult != null) {

            //viewHolder.editTextItemQty.setText(String.valueOf(techResult.getUsedItemQty()));

            /*for (int i = 0; i < ((HomeActivityNew) mContext).mItemsLists.size(); i++) {

                if (((HomeActivityNew) mContext).mItemsLists.equals(techResult.getItemId())) {

                    itemPos = ((HomeActivityNew) mContext).mItemsLists.indexOf(i);
                }
            }
            viewHolder.items.setSelection(itemPos);*/

        }

        if (position == mTechLists.size() - 1) {
            viewHolder.mTextViewAddMore.setVisibility(View.VISIBLE);
        } else {
            if (viewHolder.mTextViewAddMore.getVisibility() == View.VISIBLE) {
                viewHolder.mTextViewAddMore.setVisibility(View.GONE);
            }
        }
        viewHolder.mImageButtonCancelAddTech.setVisibility(View.VISIBLE);

        if (position == 0) {
            // viewHolder.mImageButtonCancelAddTech.setVisibility(View.GONE);
        }
        viewHolder.mTextViewTechnicianNo.setText(mContext.getString(R.string.item) + " #" + (position + 1));
    }

    @Override
    public int getItemCount() {
        return mTechLists.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        EditText editTextItemQty, mEditTextLastName, mEditTextPhoneNumber, meditTextEmailid/*, mEditTextUserName*/;
        TextView mTextViewTechnicianNo, mTextViewAddMore;
        ImageButton mImageButtonCancelAddTech;
        TextInputLayout txtMobileLayout, txtEmailLayout;
        Spinner items;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //setIsRecyclable(false);
            mTextViewAddMore = itemView.findViewById(R.id.textView_add_more);
            mTextViewTechnicianNo = itemView.findViewById(R.id.textView_technician_no);
            mImageButtonCancelAddTech = itemView.findViewById(R.id.imageButton_cancel_add_tech);
            //
            txtMobileLayout = itemView.findViewById(R.id.txtMobileLayout);
            txtEmailLayout = itemView.findViewById(R.id.txtEmailLayout);

            items = itemView.findViewById(R.id.spin_items);
            editTextItemQty = itemView.findViewById(R.id.edittext_task_item_quantity);
            TextInputLayout textInputQuantity = itemView.findViewById(R.id.textInput_quantity);

            /*mTextViewAddMore.setOnClickListener(this);
            mImageButtonCancelAddTech.setOnClickListener(this);*/

           /* if (itemNameList.size() == 0) {
//            spinnerItem.setVisibility(View.GONE);
                items.setVisibility(View.GONE);
                editTextItemQty.setVisibility(View.GONE);
            }

            itemNameList.add(0, mContext.getString(R.string.none));//Add this to select none
            itemNameList.add(mContext.getResources().getString(R.string.select_item));*/

          /*  ArrayAdapter<String> arrayAdapterItem = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, itemNameList) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    if (position == getCount()) {
                        ((TextView) v.findViewById(android.R.id.text1)).setText("");
                        ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                    }
                    return v;
                }

                @Override
                public int getCount() {
                    return super.getCount() - 1; // you don't display last item. It is used as hint.
                }

            };
            arrayAdapterItem.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            items.setAdapter(arrayAdapterItem);
            items.setSelection(arrayAdapterItem.getCount());*/

            ItemsList.ResultData selectDTO = new ItemsList.ResultData();
            selectDTO.setName("Select");
            ((HomeActivityNew) mContext).mItemsLists.add(0, selectDTO);

            adapter = new ArrayAdapter<ItemsList.ResultData>(mContext, android.R.layout.simple_spinner_item, ((HomeActivityNew) mContext).mItemsLists);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            items.setAdapter(adapter);

            items.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        //editTextItemQty.setText("");
                        textInputQuantity.setEnabled(false);
                        editTextItemQty.setEnabled(false);
                        //editTextItemQty.setBackground(ContextCompat.getDrawable(activity, R.drawable.edittext_border_disable));
                    } else {
                        // if (!itemNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(mContext.getString(R.string.select_item)) && !itemNameList.get(items.getSelectedItemPosition()).equalsIgnoreCase(mContext.getString(R.string.none))) {
                        textInputQuantity.setEnabled(true);
                        editTextItemQty.setEnabled(true);
                        selectedIndex = items.getSelectedItemPosition();
                        mSpinnerSelectedItem.put(position, selectedIndex);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            mTextViewAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTechLists.size() < 10) {
                        levelOneValidation();
                    } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.not_allowed), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mImageButtonCancelAddTech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() >= 1) {
                        removeItem(getAdapterPosition());
                    } else {
                        ((HomeActivityNew) mContext).mToolbar.setTitle(R.string.fieldweb);
                        AppCompatActivity activity = (AppCompatActivity) v.getContext();
                        OwnerDashboardFragment ownerDashboardFragment = new OwnerDashboardFragment();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, ownerDashboardFragment).addToBackStack(null).commit();

                    }
                }
            });

            editTextItemQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // do nothing
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                }

                @Override
                public void afterTextChanged(Editable s) {
                    String qty = editTextItemQty.getText().toString();
                    if (Integer.parseInt(qty) != 0 || !qty.isEmpty() || Integer.parseInt(qty) > 0) {
                        try {
                            for (int i = 0; i < ((HomeActivityNew) mContext).mItemsLists.size(); i++) {

                                if (items.getSelectedItem().toString().equalsIgnoreCase((((HomeActivityNew) mContext).mItemsLists.get(i).getName()))) {
                                    TasksList.MultipleItemAssigned multipleItemAssigned = new TasksList.MultipleItemAssigned();
                                    multipleItemAssigned.setItemName(((HomeActivityNew) mContext).mItemsLists.get(i).getName());
                                    multipleItemAssigned.setItemId(((HomeActivityNew) mContext).mItemsLists.get(i).getId());
                                    multipleItemAssigned.setItemQuantity(Integer.parseInt(qty));
                                    selectedMaterialArray.add(multipleItemAssigned);
                                    listener.onReassignItemPostListener(selectedMaterialArray);
                                }
                            }

                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                    } else {
                        Toast.makeText(mContext, "Used Qty should not be zero or empty!!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        public void levelOneValidation() {
            if (items.getSelectedItem().equals(mContext.getString(R.string.select_item)) ||
                    items.getSelectedItem().equals(mContext.getString(R.string.none))) {
                Toast.makeText(mContext.getApplicationContext(), "Please Select an Item!!", Toast.LENGTH_LONG).show();
            }
            if (isEmpty((editTextItemQty))) {
                editTextItemQty.setError(mContext.getString(R.string.please_enter_valid_quantity));
            } else {
                addNewTech();
            }
        }


    }

/*
    public boolean isValidateUserName(final String userName) {
        if (TextUtils.isEmpty(userName)) {
            mErrorMessage = mContext.getString(R.string.please_enter_userId);
        } else {
            mCheckCharPattern = "(?=.*^[a-zA-Z]).*$";
            mPattern = Pattern.compile(mCheckCharPattern);
            mMatcher = mPattern.matcher(userName);
            mResult = mMatcher.matches();
            if (!mResult) {
                mErrorMessage = mContext.getString(R.string.first_char_of_username);
            } else {
                mCheckCharPattern = "[a-zA-Z][a-zA-Z0-9&@#_]*$";
                mPattern = Pattern.compile(mCheckCharPattern);
                mMatcher = mPattern.matcher(userName);
                mResult = mMatcher.matches();
                if (!mResult) {
                    mErrorMessage = mContext.getString(R.string.username_should_contain_only);
                } else {
                    mCheckCharPattern = "^[a-zA-Z][a-zA-Z0-9&@#_]{5,14}$";
                    mPattern = Pattern.compile(mCheckCharPattern);
                    mMatcher = mPattern.matcher(userName);
                    mResult = mMatcher.matches();
                    if (!mResult) {
                        mErrorMessage = mContext.getString(R.string.username_must_be_of_more_than);
                    }
                }
            }
        }
        return mResult;
    }
*/

    private void addNewTech() {
        ViewUtils.hideKeyboard((HomeActivityNew) mContext);
        ((HomeActivityNew) mContext).mItemsLists.remove(0);
        //AddItem.Resultdata resultData = new AddItem.Resultdata();
        //resultData.setTempTechSrNo(mTechLists.size());
        //resultData.setName("");
        //resultData.setQuantity(0);
        //To handle the set error in onBindViewHolder
        if (mMessage != null && mMessage.equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.validation_failed, mContext))) {
            mMessage = "";
        }
        restoreItem(mAddItem, mTechLists.size());
    }

    public TasksList.MultipleItemAssigned getItem(int id) {
        return mTechLists.get(id);
    }

    public List<TasksList.MultipleItemAssigned> getData() {
        return mTechLists;
    }

    /*public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }*/

    private boolean isValidPhone(EditText text) {
        CharSequence phone = text.getText().toString();
        return phone.length() != 10;
    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private boolean isContainsSpace(EditText text) {
        return text.getText().toString().contains(" ");
    }

    public void removeItem(int position) {
        /*mTechLists.get(position).setFirstName(null);
        mTechLists.get(position).setLastName(null);
        mTechLists.get(position).setContactNo(null);
        mTechLists.get(position).setUserId(0);
        mTechLists.get(position).setUserName(null);*/
        mTechLists.remove(position);
       /* for (int i = position - 1; i > 0; i--) {
            mTechLists.get(i).setTempTechSrNo(mTechLists.get(i).getTempTechSrNo() - 1);
        }*/
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void restoreItem(TasksList.MultipleItemAssigned techList, int position) {
        mTechLists.add(position, techList);
        notifyItemInserted(position);
        notifyDataSetChanged();
    }

    public interface ReassignItemPostListener {
        void onReassignItemPostListener(ArrayList<TasksList.MultipleItemAssigned> SODArray);
    }


}
