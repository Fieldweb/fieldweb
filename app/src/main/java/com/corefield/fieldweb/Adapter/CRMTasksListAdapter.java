package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.CRMTask.CRMTaskList;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CRMTaskDetailsFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of task added by the Owner
 * @see
 */
public class CRMTasksListAdapter extends RecyclerView.Adapter<CRMTasksListAdapter.ViewHolder> {

    private List<CRMTaskList.ResultData> mTasksList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private static String TAG = CRMTasksListAdapter.class.getSimpleName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    CRMTaskList.ResultData tasksList;

    /**
     * Constructor
     *
     * @param context
     * @param mTasksList List of Task
     *                   //* @param taskCategory Task Category on the basis of Task Status and Task Type
     */
    public CRMTasksListAdapter(Context context, List<CRMTaskList.ResultData> mTasksList, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mTasksList = new ArrayList<>();
        this.mTasksList = mTasksList;
        this.mClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (position) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.task_list_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.crm_tasklist_new, parent, false);
        viewHolder = new ViewHolder(v1);
        return viewHolder;
    }

    protected class LoadingVH extends ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        tasksList = mTasksList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder viewFHolder = (ViewHolder) viewHolder;
                if (tasksList != null)
                    setTaskRowDetails(viewFHolder, tasksList);
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }

    private void setTaskRowDetails(ViewHolder viewHolder, CRMTaskList.ResultData tasksList) {
        viewHolder.mTaskAddress.setText(tasksList.getFullAddress());
        viewHolder.mTaskName.setText(tasksList.getName());
        viewHolder.mTextViewID.setText("[" + tasksList.getId() + "]");
        viewHolder.mTechName.setText(tasksList.getFirstNameLastName());
        viewHolder.mTextViewTaskStatus.setText(tasksList.getTaskStatus());
        viewHolder.mTextViewType.setText(tasksList.getTaskType());
        /*if (tasksList.getTaskType() != null) {
            switch (tasksList.getTaskType()) {
                case "Urgent":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.rejected_background));
                    break;
                case "Today":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.ongoing_background));
                    break;
                case "Schedule":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.schedule_background));
                    break;
                default:
                    break;
            }
        }*/
        if (tasksList.getTaskStatus() != null || !tasksList.getTaskStatus().isEmpty()) {
            switch (tasksList.getTaskStatus()) {
                case "InActive":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.inactive_background));
                    viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.inactive));
                    viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.light_gray));
                    break;
                case "Completed":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.completed_background));
                    viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.completed));
                    viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.green));
                    break;
                case "Rejected":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.rejected_background));
                    viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.rejected));
                    viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.red));
                    break;
                case "Ongoing":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.ongoing_background));
                    viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.ongoing));
                    viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.orange));
                    break;

                case "OnHold":
                    viewHolder.mTextViewType.setBackground(mContext.getResources().getDrawable(R.drawable.onhold_background));
                    viewHolder.mTextViewType.setText(mContext.getResources().getString(R.string.onhold));
                    viewHolder.mTextViewTaskStatus.setTextColor(mContext.getResources().getColor(R.color.onhold));
                    break;
                default:
                    break;
            }
        }

        String formattedDate = DateUtils.convertDateFormat(tasksList.getTaskDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
        String formattedTime = DateUtils.timeFormatConversion(tasksList.getTaskTime(), "HH:mm:ss", "hh:mm aa");
//        FWLogger.logInfo(TAG, "Time : "+formattedTime);
        if (formattedDate != null && formattedTime != null)
            viewHolder.mTextViewTaskDate.setText(formattedDate + " " + formattedTime);

        String todayDate = DateUtils.getDate(System.currentTimeMillis(), "dd-MM-yyyy");
        FWLogger.logInfo(TAG, "today's Date : " + todayDate);
        if (todayDate.equalsIgnoreCase(formattedDate))
            viewHolder.mCardViewTask.setCardBackgroundColor(mContext.getResources().getColor(R.color.lighter_red));
        else
            viewHolder.mCardViewTask.setCardBackgroundColor(mContext.getResources().getColor(R.color.gray));

//        viewHolder.mTextViewTaskDate.setText(DateUtils.convertDateFormat(tasksList.getTaskDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy"));
        Picasso.get().load(tasksList.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                viewHolder.mProfile.setImageResource(R.drawable.profile_icon);
            }
        });
        if (tasksList.getTaskState() == TaskState.PAYMENT_RECEIVED)
            viewHolder.mPNRImageView.setVisibility(View.GONE);
        else if (tasksList.getTaskStatus().equalsIgnoreCase("Ongoing") && tasksList.getTaskState() == TaskState.ENDED_NO_PAYMENT && tasksList.getPaymentModeId() == Constant.PaymentMode.RATE_ID)
            viewHolder.mPNRImageView.setVisibility(View.VISIBLE);
        else
            viewHolder.mPNRImageView.setVisibility(View.GONE);

        if (tasksList.getPaymentModeId() == 1)
            viewHolder.mImageViewPaymentMode.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_amc_red));
        else
            viewHolder.mImageViewPaymentMode.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_rate_red));

        if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) &&
                tasksList.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.InActive.name())) {
            // viewHolder.mTextViewReassign.setVisibility(View.VISIBLE);
            // viewHolder.mTextViewReassign.setText("");
            // viewHolder.mTextViewReassign.setBackground(ContextCompat.getDrawable(mContext, R.drawable.fw_edit_task_icon));
        } else if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER) &&
                tasksList.getTaskState() == TaskState.PAYMENT_RECEIVED &&
                tasksList.getTaskStatus().equalsIgnoreCase(Constant.TaskCategories.Completed.name())) {
        }
    }

    @Override
    public int getItemCount() {
        return mTasksList == null ? 0 : mTasksList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTaskName, mTaskAddress, mTechName, mTextViewTaskDate, mTextViewTaskStatus, mTextViewID, mTextViewType, mTextViewReassign;
        private ImageView mProfile, mPNRImageView, mImageViewPaymentMode;
        private CardView mCardViewTask;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            mTaskName = view.findViewById(R.id.task_name);
            mTextViewID = view.findViewById(R.id.task_id);
            mTaskAddress = view.findViewById(R.id.task_address);
            mPNRImageView = view.findViewById(R.id.imageView_pnr);
            mImageViewPaymentMode = view.findViewById(R.id.imageView_payment_mode);
            mTechName = view.findViewById(R.id.task_tech_name);
            mTextViewReassign = view.findViewById(R.id.textView_reassign_from_list);
            mTextViewTaskStatus = view.findViewById(R.id.task_status_text);
            mTextViewTaskDate = view.findViewById(R.id.task_date);
            mTextViewType = view.findViewById(R.id.textView_type);
            mCardViewTask = view.findViewById(R.id.task_card_view);

            if (mTextViewReassign != null) {
                mTextViewReassign.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mClickListener.onClick(v, getAdapterPosition());
                    }
                });
            }
            mProfile = view.findViewById(R.id.task_profile_pic);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
            try {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                if (mTasksList.get(getAdapterPosition()).getTaskStatus().equals(Constant.TaskCategories.Completed.name())) {
                    ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                    ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("downloadReport", true);
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putSerializable("taskResult", mTasksList.get(getAdapterPosition()));
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                } else if (mTasksList.get(getAdapterPosition()).getTaskStatus().equals(Constant.TaskCategories.InActive.name())) {
                    ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                    ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("downloadReport", true);
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putSerializable("taskResult", mTasksList.get(getAdapterPosition()));
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                } else if (mTasksList.get(getAdapterPosition()).getTaskStatus().equals(Constant.TaskCategories.Ongoing.name())) {
                    ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                    ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("downloadReport", true);
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putSerializable("taskResult", mTasksList.get(getAdapterPosition()));
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                } else if (mTasksList.get(getAdapterPosition()).getTaskStatus().equals(Constant.TaskCategories.Rejected.name())) {
                    ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                    ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putSerializable("taskResult", mTasksList.get(getAdapterPosition()));
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                } else if (mTasksList.get(getAdapterPosition()).getTaskStatus().equals(Constant.TaskCategories.OnHold.name())) {
                    ((HomeActivityNew) mContext).mLinearHeaderLayout.setVisibility(View.GONE);
                    ((HomeActivityNew) mContext).mDatesRecyclerView.setVisibility(View.GONE);
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putSerializable("taskResult", mTasksList.get(getAdapterPosition()));
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mTasksList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public CRMTaskList.ResultData getItem(int id) {
        return mTasksList.get(id);
    }

    public List<CRMTaskList.ResultData> getData() {
        return mTasksList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void add(CRMTaskList.ResultData resultData) {
        mTasksList.add(resultData);
        notifyItemInserted(mTasksList.size() - 1);
    }

    public void addAll(List<CRMTaskList.ResultData> tasksList) {
        for (CRMTaskList.ResultData resultData : tasksList) {
            add(resultData);
        }
    }

   /* public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new TasksList.ResultData());
    }*/

   /* public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mTasksList.size() - 1;
        if (position >= 0) {
            TasksList.ResultData item = getItem(position);

            if (item != null) {
                mTasksList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }*/

    public void removeItem(int position) {
        mTasksList.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(TasksList.ResultData resultData) {
        int position = mTasksList.indexOf(resultData);
        if (position > -1) {
            mTasksList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void restoreItem(CRMTaskList.ResultData tasksList, int position) {
        mTasksList.add(position, tasksList);
        notifyItemInserted(position);
    }
}


