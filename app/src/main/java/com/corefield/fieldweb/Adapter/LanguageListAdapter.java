package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show and Choose the list of languages
 * @see
 */
public class LanguageListAdapter extends RecyclerView.Adapter<LanguageListAdapter.ViewHolder> {

    private List<String> mLanguageList;
    private LayoutInflater mInflater;
    private LanguageClickListener mClickListener;
    Context mContext;

    // data is passed into the constructor
    public LanguageListAdapter(Context context, List<String> languageList) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mLanguageList = languageList;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.language_list_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String language = mLanguageList.get(position);
        holder.textViewLanguage.setText(language);

        FWLogger.logInfo("TAG", "LANG selected : " + SharedPrefManager.getInstance(mContext).getPreferredLanguagePos() + "   language : " + language);
        if (SharedPrefManager.getInstance(mContext).getPreferredLanguagePos() == position) {
            holder.imageViewChecked.setVisibility(View.VISIBLE);
        } else {
            holder.imageViewChecked.setVisibility(View.GONE);
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mLanguageList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewLanguage;
        ImageView imageViewChecked;
//        CheckedTextView textViewLanguage;

        ViewHolder(View itemView) {
            super(itemView);
            textViewLanguage = itemView.findViewById(R.id.textView_language);
            imageViewChecked = itemView.findViewById(R.id.imageView_checked);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mLanguageList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(LanguageClickListener languageClickListener) {
        this.mClickListener = languageClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface LanguageClickListener {
        void onItemClick(View view, int position);
    }
}