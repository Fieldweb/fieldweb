package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Notification.Notification;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.SharedPrefManager;
import com.corefield.fieldweb.Util.TaskState;
import com.corefield.fieldweb.Util.TaskStatus;

import java.util.List;

/**
 * //
 * Created by CFS on 2/17/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the notification list in notification fragment
 * //
 **/
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private static String TAG = NotificationListAdapter.class.getSimpleName();
    private List<Notification.ResultData> mResultData;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;

    /**
     * Constructor with on item click listener
     *
     * @param context
     * @param position      Expense List Item Position
     * @param resultData    List of notification
     * @param clickListener On Item Click Listener
     */
    public NotificationListAdapter(Context context, int position, List<Notification.ResultData> resultData, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResultData = resultData;
        this.mPosition = position;
        this.mContext = context;
        this.mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_notification_new, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Notification.ResultData resultData = mResultData.get(position);
        String notificationType = resultData.getNotificationType();

        switch (notificationType) {
            case Constant.NotificationType.TYPE_TASK:
                switch (resultData.getTaskStatusId()) {
                    case TaskStatus.REJECTED:
                        //set Details For REJECTED task status
                        setDetailsForRejected(holder, resultData);
                        break;
                    case TaskStatus.ON_GOING:
                        //set Details For ON_GOING task status
                        setDetailsForOnGoing(holder, resultData);
                        break;
                    case TaskStatus.COMPLETED:
                        //set Details For COMPLETED task status
                        setDetailsForCompleted(holder, resultData);
                        break;
                    case TaskStatus.IN_ACTIVE:
                        //set Details For IN_ACTIVE task status
                        setDetailsForInActive(holder, resultData);
                        break;
                    case TaskStatus.ONHOLD:
                        //set Details For IN_ACTIVE task status
                        setDetailsForOnHold(holder, resultData);
                        break;
                    default:
                        break;
                }
                holder.mLinearLayoutAMC.setVisibility(View.GONE);
                holder.mTextViewUrgent.setVisibility(View.GONE);
                break;
            case Constant.NotificationType.TYPE_USER_INFO:
                //set Details For TYPE_USER_INFO
                setDetailsForUserInfo(holder, resultData);
                break;
            case Constant.NotificationType.TYPE_EARNINGS:
                //set Details For TYPE_EARNINGS
                setDetailsForEarnings(holder, resultData);
                break;
            case Constant.NotificationType.TYPE_AMC:
                //set Details For TYPE_AMC
                setDetailsForAMC(holder, resultData);
                break;
            default:
                break;
        }

        if (resultData.getNotificationDay().equalsIgnoreCase(Constant.NotificationDay.DAY_TODAY)) {
            holder.mTextViewNotificationTime.setText(mContext.getString(R.string.today) + " " + resultData.getNotificationTime());
        } else if (resultData.getNotificationDay().equalsIgnoreCase(Constant.NotificationDay.DAY_YESTERDAY)) {
            holder.mTextViewNotificationTime.setText(mContext.getString(R.string.yesterday) + " " + resultData.getNotificationTime());
        } else if (resultData.getNotificationDay().equalsIgnoreCase(Constant.NotificationDay.DAY_OLD)) {
            holder.mTextViewNotificationTime.setText(resultData.getNotificationDate() + " " + resultData.getNotificationTime());
        }

        if (resultData.getIsRead()) {
            holder.mTextViewNewNotiFlag.setVisibility(View.GONE);
        } else {
            holder.mTextViewNewNotiFlag.setVisibility(View.VISIBLE);
        }
        holder.itemView.setTag(position);
    }

    private void setDetailsForAMC(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.VISIBLE);
        holder.mLinearLayoutAMC.setVisibility(View.VISIBLE);
        if (resultData.getAMCServiceDetailDtoObj().getAMCSetReminderType() != null &&
                resultData.getAMCServiceDetailDtoObj().getAMCSetReminderType().equalsIgnoreCase(mContext.getString(R.string.extreme))) {
            holder.mTextViewUrgent.setVisibility(View.GONE);
        } else {
            holder.mTextViewUrgent.setVisibility(View.GONE);
        }
        holder.mTextViewDueDate.setText(resultData.getAMCServiceDetailDtoObj().getAMCServiceDate().split("T")[0]);
        holder.mTextViewTotalService.setText("" + resultData.getAMCServiceDetailDtoObj().getServiceNo() + "/" + resultData.getAMCServiceDetailDtoObj().getTotalServices());
        holder.mTextViewNotificationTitle.setText(R.string.amc_reminder);
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        String subTitle = "You have a " + resultData.getAMCServiceDetailDtoObj().getAMCTypeName()
                + " " + resultData.getAMCServiceDetailDtoObj().getServiceOccuranceType()
                + " <b>" + resultData.getAMCServiceDetailDtoObj().getAMCName() + "</b> AMC for <b>"
                + resultData.getAMCServiceDetailDtoObj().getCustomerName() + "</b>";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_amc);
    }

    private void setDetailsForEarnings(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewNotificationTitle.setText(R.string.earnings);
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        String subTitle = "";
        if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER))
            subTitle = "Technician <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b> earned <b>Rs " + resultData.getEarningAmount() + "</b> for the task <b>" + resultData.getTaskName() + "</b>.";
        else if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
            subTitle = "</b> You have earned <b>Rs " + resultData.getEarningAmount() + "</b> for the task <b>" + resultData.getTaskName() + "</b>.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        /*holder.bindImageDrawableWithColor(R.drawable.ic_noti_expense);*/
        holder.bindImageDrawableWithColor(R.drawable.ic_book);
        holder.mLinearLayoutAMC.setVisibility(View.GONE);
        holder.mTextViewUrgent.setVisibility(View.GONE);
    }

    private void setDetailsForUserInfo(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.VISIBLE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewReAssignOrAssign.setText(mContext.getResources().getString(R.string.task_assign));
        holder.mTextViewNotificationTitle.setText(R.string.info);
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        String subTitle = "New technician <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b> is added into your technician list.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_add_tech);
        holder.mLinearLayoutAMC.setVisibility(View.GONE);
        holder.mTextViewUrgent.setVisibility(View.GONE);
    }

    private void setDetailsForInActive(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewNotificationTitle.setText(R.string.inactive); //Constant.TaskCategories.InActive.name()
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.blue));
        String subTitle = "Task <b>" + resultData.getTaskName() + "</b> is assign to you.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_noti_inactive);
    }


    private void setDetailsForOnHold(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewNotificationTitle.setText(R.string.onhold); //Constant.TaskCategories.InActive.name()
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        String subTitle = "Task <b>" + resultData.getTaskName() + "</b> is put on hold by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b>.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_noti_ongoing);
    }

    private void setDetailsForCompleted(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewNotificationTitle.setText(R.string.completed);  //Constant.TaskCategories.Completed.name()
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        String subTitle = "";
        if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER))//Rate and AMC
            subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b>.";
        else if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN))
            subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by you.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_noti_completed);
    }

    private void setDetailsForOnGoing(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.GONE);
        holder.mTextViewViewDetails.setVisibility(View.GONE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewNotificationTitle.setText(R.string.ongoing);  //Constant.TaskCategories.Ongoing.name()
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.orange));
        //set SubTitle
        setSubTitle(holder, resultData);
        holder.bindImageDrawableWithColor(R.drawable.ic_noti_ongoing);
    }

    private void setDetailsForRejected(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        holder.mTextViewReAssignOrAssign.setVisibility(View.VISIBLE);
        holder.mTextViewViewDetails.setVisibility(View.VISIBLE);
        holder.mImageViewRemoveNotification.setVisibility(View.GONE);
        holder.mTextViewReAssignOrAssign.setText(mContext.getResources().getString(R.string.re_assign));
        holder.mTextViewNotificationTitle.setText(R.string.rejected);  //Constant.TaskCategories.Rejected.name()
        holder.mTextViewNotificationTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        String subTitle = "Task <b>" + resultData.getTaskName() + "</b> has rejected by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b>.";
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
        holder.bindImageDrawableWithColor(R.drawable.ic_noti_reject);
    }

    private void setSubTitle(@NonNull ViewHolder holder, Notification.ResultData resultData) {
        String subTitle = "";
        if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.OWNER)) {
            if (resultData.getTaskState() == TaskState.NOT_STARTED) {// Rate And AMC
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is accepted by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b>.";
            } else if (resultData.getTaskState() == TaskState.STARTED_NOT_ENDED) { // Rate And AMC
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is in progress by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b>.";
            } else if (resultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && resultData.getPaymentModeId() == Constant.PaymentMode.AMC_ID) {//AMC
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b> and is waiting for closure.";
            } else if (resultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && resultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {//Rate
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b> and is waiting for payment.";
            } else if (resultData.getTaskState() == TaskState.PAYMENT_RECEIVED && resultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {//Rate
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by <b>" + resultData.getFirstName() + " " + resultData.getLastName() + "</b> and is waiting for closure.";
            }
        } else if (SharedPrefManager.getInstance(mContext).getUserGroup().equalsIgnoreCase(Constant.UserGroup.TECHNICIAN)) {
            if (resultData.getTaskState() == TaskState.ENDED_NO_PAYMENT && resultData.getPaymentModeId() == Constant.PaymentMode.RATE_ID) {
                subTitle = "Task <b>" + resultData.getTaskName() + "</b> is completed by you please collect cash";
            }
        }
        holder.mTextViewNotificationSubTitle.setText(Html.fromHtml(subTitle));
    }

    @Override
    public int getItemCount() {
        return mResultData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener// implements View.OnClickListener
    {
        private TextView mTextViewNotificationTitle, mTextViewNotificationSubTitle, mTextViewNotificationTime,
                mTextViewViewDetails, mTextViewReAssignOrAssign, mTextViewNewNotiFlag,
                mTextViewDueDate, mTextViewTotalService, mTextViewUrgent;
        private ImageView circleImageView;
        private ImageView mImageViewRemoveNotification;
        private LinearLayout mLinearLayout, mLinearLayoutAMC;
        private CardView mCardViewNotification;

        ViewHolder(View view) {
            super(view);
            circleImageView = view.findViewById(R.id.circular_image_view_notification);
            mTextViewNotificationTitle = view.findViewById(R.id.textView_noti_task_title);
            mTextViewNotificationSubTitle = view.findViewById(R.id.textView_noti_task_sub_title);
            mTextViewNotificationTime = view.findViewById(R.id.textView_noti_task_time);
            mImageViewRemoveNotification = view.findViewById(R.id.image_view_remove_noti);
            mTextViewViewDetails = view.findViewById(R.id.textView_view_details);
            mTextViewReAssignOrAssign = view.findViewById(R.id.textView_reassign);
            mTextViewNewNotiFlag = view.findViewById(R.id.textView_new_noti_flag);

            mTextViewDueDate = view.findViewById(R.id.textView_due_date);
            mTextViewTotalService = view.findViewById(R.id.textView_total_service);
            mTextViewUrgent = view.findViewById(R.id.textView_urgent);

            mLinearLayout = view.findViewById(R.id.linear_layout_noti_view);
            mLinearLayoutAMC = view.findViewById(R.id.linearLayout_amc);
            mCardViewNotification = view.findViewById(R.id.card_view_notification);
            mImageViewRemoveNotification.setOnClickListener(this);
            mTextViewReAssignOrAssign.setOnClickListener(this);
            mTextViewViewDetails.setOnClickListener(this);
            mLinearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }


        private void bindImageDrawableWithColor(int resID) {
            circleImageView.setImageDrawable(ContextCompat.getDrawable(mContext, resID));
        }
    }

    public Notification.ResultData getItem(int id) {
        return mResultData.get(id);
    }

    public List<Notification.ResultData> getData() {
        return mResultData;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mResultData.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItems(List<Notification.ResultData> mNewNotificationList) {
        final NotificationDiffCallback diffCallback = new NotificationDiffCallback(this.mResultData, mNewNotificationList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.mResultData.clear();
        this.mResultData.addAll(mNewNotificationList);
        diffResult.dispatchUpdatesTo(this);
    }

    private class NotificationDiffCallback extends DiffUtil.Callback {

        private final List<Notification.ResultData> mOldNotificationList;
        private final List<Notification.ResultData> mNewNotificationList;

        public NotificationDiffCallback(List<Notification.ResultData> oldNotificationList, List<Notification.ResultData> newNotificationList) {
            this.mOldNotificationList = oldNotificationList;
            this.mNewNotificationList = newNotificationList;
        }

        @Override
        public int getOldListSize() {
            return mOldNotificationList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewNotificationList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            final Notification.ResultData oldResultData = mOldNotificationList.get(oldItemPosition);
            final Notification.ResultData newResultData = mNewNotificationList.get(newItemPosition);
            return oldResultData.getId() == (newResultData.getId());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            final Notification.ResultData oldResultData = mOldNotificationList.get(oldItemPosition);
            final Notification.ResultData newResultData = mNewNotificationList.get(newItemPosition);
            return oldResultData.equals(newResultData);
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }

}
