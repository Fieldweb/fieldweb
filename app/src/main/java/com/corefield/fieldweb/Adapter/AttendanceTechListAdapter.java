package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of technician (Owner login)
 * @see
 */
public class AttendanceTechListAdapter extends RecyclerView.Adapter<AttendanceTechListAdapter.ViewHolder> implements Filterable {

    private List<UsersList.ResultData> mUserList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private List<UsersList.ResultData> mResultDataListFilter;

    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param userList List of Technician
     */
    public AttendanceTechListAdapter(Context context, List<UsersList.ResultData> userList) {
        this.mInflater = LayoutInflater.from(context);
        this.mUserList = userList;
        this.mResultDataListFilter = userList;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.attendance_tech_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        UsersList.ResultData usersList = mResultDataListFilter.get(position);

        viewHolder.mTechnicianName.setText(usersList.getFirstName() + " " + usersList.getLastName());
        Picasso.get().load(usersList.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                viewHolder.mProfile.setImageResource(R.drawable.profile_icon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTechnicianName;
        private ImageView mProfile;

        ViewHolder(View view) {
            super(view);

            mTechnicianName = view.findViewById(R.id.technician_name);
            mProfile = view.findViewById(R.id.technician_image);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }
    }

    public UsersList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<UsersList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mUserList;
                } else {
                    List<UsersList.ResultData> filteredList = new ArrayList<>();
                    for (UsersList.ResultData row : mUserList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getUserName().toLowerCase().contains(charString) || String.valueOf(row.getFirstName()).toLowerCase().contains(charSequence)
                                || row.getLastName().toLowerCase().contains(charString)) {
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<UsersList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void restoreItem(UsersList.ResultData usersList, int position) {
        mResultDataListFilter.add(position, usersList);
        notifyItemInserted(position);
    }
}


