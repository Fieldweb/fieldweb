package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.FieldWeb.Admin.CRMTaskAmcTabHostFragment;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerListAdapterNew extends RecyclerView.Adapter<CustomerListAdapterNew.ViewHolder> implements Filterable {

    protected final String TAG = CustomerListAdapterNew.class.getSimpleName();
    private List<CustomerList.ResultData> mResultDataListFilter;
    private List<CustomerList.ResultData> mResultDataList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;

    public CustomerListAdapterNew(List<CustomerList.ResultData> resultDataList, Context context, RecyclerTouchListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResultDataListFilter = resultDataList;
        this.mResultDataList = resultDataList;
        this.mContext = context;
        this.mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.customerlist_custom_new, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerListAdapterNew.ViewHolder viewHolder, int i) {
        CustomerList.ResultData resultData = mResultDataListFilter.get(i);
        viewHolder.textViewCustName.setText(resultData.getCustomerName());
        viewHolder.textViewAddress.setText(resultData.getAddress());
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewCustName, textViewAddress;
        ImageView imageViewMessage, imageViewCall, imageViewEdit;
        View layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = (View) itemView.findViewById(R.id.custCard);
            textViewCustName = itemView.findViewById(R.id.textView_customer_name);
            textViewAddress = itemView.findViewById(R.id.textView_address);
            imageViewMessage = itemView.findViewById(R.id.imageView_message);
            imageViewCall = itemView.findViewById(R.id.imageView_call);
            imageViewEdit = itemView.findViewById(R.id.imageView_edit);

            imageViewMessage.setOnClickListener(this);
            imageViewCall.setOnClickListener(this);
            imageViewEdit.setOnClickListener(this);
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
           /* // BY MANISH
            int CustomerDetailsId = mResultDataListFilter.get(getAdapterPosition()).getCustomerDetailsid();
            String custName = mResultDataListFilter.get(getAdapterPosition()).getCustomerName();
            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            CRMTaskAmcTabHostFragment myFragment = new CRMTaskAmcTabHostFragment();
            Bundle args = new Bundle();
            args.putInt("CustID", CustomerDetailsId);
            args.putString("CustName", custName);
            myFragment.setArguments(args);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();*/
        }
    }

    public CustomerList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<CustomerList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mResultDataList;
                } else {
                    List<CustomerList.ResultData> filteredList = new ArrayList<>();
                    for (CustomerList.ResultData row : mResultDataList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCustomerName().toLowerCase().contains(charString) || String.valueOf(row.getMobileNumber()).contains(charSequence)
                                || row.getAddress().toLowerCase().contains(charString)) {
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<CustomerList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mResultDataList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(CustomerList.ResultData usersList, int position) {
        mResultDataList.add(position, usersList);
        notifyItemInserted(position);
    }

}


