package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Expenditure.ExpenseDetails;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of expense of technicians to Owner
 * @see
 */

public class ExpenseListAdapterNew extends RecyclerView.Adapter<ExpenseListAdapterNew.ViewHolder> {

    private List<ExpenseDetails.ExpenseList> mExpenseList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;

    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param position Expense List Item Position
     * @param userList List of expenses
     */
    public ExpenseListAdapterNew(Context context, int position, List<ExpenseDetails.ExpenseList> userList) {
        this.mInflater = LayoutInflater.from(context);
        this.mExpenseList = userList;
        this.mPosition = position;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.expense_list_new, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ExpenseDetails.ExpenseList expenseList = mExpenseList.get(position);

        viewHolder.mExpenseName.setText(expenseList.getExpenseName());
        viewHolder.mExpenseAmount.setText(String.valueOf(expenseList.getAmount()));
        viewHolder.mExpensePhoto.setVisibility(View.GONE);

        if (expenseList.getExpensePhoto() != null && !expenseList.getExpensePhoto().isEmpty()) {
            viewHolder.mExpensePhoto.setVisibility(View.VISIBLE);

            Picasso.get().load(mExpenseList.get(position).getExpensePhoto()).placeholder(R.drawable.ic_image_view).
                    memoryPolicy(MemoryPolicy.NO_CACHE).
                    networkPolicy(NetworkPolicy.NO_CACHE)
                    .fit()
                    .into(viewHolder.mExpensePhoto, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    viewHolder.mExpensePhoto.setImageResource(R.drawable.ic_image_view);
                }
            });
        } else {
            viewHolder.mExpensePhoto.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mExpenseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder// implements View.OnClickListener
    {
        private TextView mExpenseName, mExpenseAmount;
        private ImageView mExpensePhoto;

        ViewHolder(View view) {
            super(view);
            mExpenseName = view.findViewById(R.id.expense_name);
            mExpenseAmount = view.findViewById(R.id.expense_amount);
            mExpensePhoto = view.findViewById(R.id.expense_photo);

            mExpensePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });*/
        }
    }

    public ExpenseDetails.ExpenseList getItem(int id) {
        return mExpenseList.get(id);
    }

    public List<ExpenseDetails.ExpenseList> getData() {
        return mExpenseList;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mExpenseList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ExpenseDetails.ExpenseList usersList, int position) {
        mExpenseList.add(position, usersList);
        notifyItemInserted(position);
    }
}


