package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryFragment;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerAttendanceFragment;
import com.corefield.fieldweb.FieldWeb.Admin.OwnerItemInventoryFragmentNew;
import com.corefield.fieldweb.FieldWeb.Admin.TechAttendanceFragment;
import com.corefield.fieldweb.FieldWeb.Admin.TechAttendanceFragmentForOwner;
import com.corefield.fieldweb.FieldWeb.BaseActivity;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of technician (Owner login)
 * @see
 */
public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.ViewHolder> implements Filterable {

    private List<UsersList.ResultData> mUserList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;
    private List<UsersList.ResultData> mResultDataListFilter;
    Fragment fragment;

    /**
     * Constructor with on item click listener
     *
     * @param context
     * @param position      position of Technician in UsersList
     * @param userList      List of Technician
     * @param clickListener On Item Click Listener
     */
    public UsersListAdapter(Context context, int position, List<UsersList.ResultData> userList, RecyclerTouchListener clickListener, Fragment fragment) {
        this.mInflater = LayoutInflater.from(context);
        this.mUserList = userList;
        this.mPosition = position;
        this.mContext = context;
        this.mClickListener = clickListener;
        this.mResultDataListFilter = userList;
        this.fragment = fragment;
    }

    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param position position of Technician in UsersList
     * @param userList List of Technician
     */
    public UsersListAdapter(Context context, int position, List<UsersList.ResultData> userList) {
        this.mInflater = LayoutInflater.from(context);
        this.mUserList = userList;
        this.mResultDataListFilter = userList;
        this.mPosition = position;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {//technician_list_details_new
        View view = mInflater.inflate(R.layout.technician_list_details_enhanced, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        UsersList.ResultData usersList = mResultDataListFilter.get(position);

        viewHolder.mTechnicianName.setText(usersList.getFirstName() + " " + usersList.getLastName());
        //viewHolder.mTechOccupation.setText(usersList.getRole());
        viewHolder.mTextViewBattery.setText(usersList.getBatteryPercentage() + "%");
        if (usersList.getBatteryPercentage() >= 51) {
            viewHolder.mTextViewBattery.setTextColor(mContext.getResources().getColor(R.color.green));
        } else if (usersList.getBatteryPercentage() >= 21 && usersList.getBatteryPercentage() <= 50) {
            viewHolder.mTextViewBattery.setTextColor(mContext.getResources().getColor(R.color.orange));
        } else {
            viewHolder.mTextViewBattery.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        if (usersList.isGPSOnOrOff()) {
            viewHolder.mTextViewGPS.setText("On");
            viewHolder.mTextViewGPS.setTextColor(mContext.getResources().getColor(R.color.green));
        } else {
            viewHolder.mTextViewGPS.setText("Off");
            viewHolder.mTextViewGPS.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }

        Picasso.get().load(usersList.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                viewHolder.mProfile.setImageResource(R.drawable.profile_icon);
            }
        });

        //set Attendance Background Color
        setAttendanceBackgroundColor(viewHolder, usersList);

        if (usersList.isHealthFeaturesEnabled()) {
            //viewHolder.mLinearLayoutHealthAndSafety.setVisibility(View.VISIBLE);
            if (usersList.getAarogyaSetuStatusId() != 0 && usersList.getArogyaSetuStatus() != null) {
                if (usersList.getDescription() != null) {
                    //viewHolder.mImageViewArogyaStatus.setVisibility(View.VISIBLE);
//                    viewHolder.viewLine.setVisibility(View.VISIBLE);
                    PorterDuffColorFilter arogyaImageFilter = new PorterDuffColorFilter(Color.parseColor(usersList.getDescription()), PorterDuff.Mode.SRC_ATOP);
                    //viewHolder.mImageViewArogyaStatus.getBackground().mutate().setColorFilter(arogyaImageFilter);
                }
            } else {
                //viewHolder.mImageViewArogyaStatus.setVisibility(View.INVISIBLE);
//                viewHolder.viewLine.setVisibility(View.INVISIBLE);
            }
           /* if (usersList.getTemperature() != 0) {
                //viewHolder.mImageViewThermometer.setVisibility(View.VISIBLE);
                //viewHolder.mTextViewTemp.setVisibility(View.VISIBLE);
                viewHolder.mTextViewTemp.setText(String.format("%.2f", usersList.getTemperature()) + mContext.getResources().getString(R.string.fahrenheit_unit));
                PorterDuffColorFilter thermoImageFilter;
                if (usersList.getTemperature() > 96 && usersList.getTemperature() < 100) {
                    thermoImageFilter = new PorterDuffColorFilter(ResourcesCompat.getColor(mContext.getResources(), R.color.green, null), PorterDuff.Mode.SRC_ATOP);
//                    viewHolder.mImageViewThermometer.getBackground().mutate().setColorFilter(thermoImageFilter);
                    viewHolder.mTextViewTemp.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.green, null));
                }
                if (usersList.getTemperature() > 100) {
                    thermoImageFilter = new PorterDuffColorFilter(ResourcesCompat.getColor(mContext.getResources(), R.color.task_rejected, null), PorterDuff.Mode.SRC_ATOP);
//                    viewHolder.mImageViewThermometer.getBackground().mutate().setColorFilter(thermoImageFilter);
                    viewHolder.mTextViewTemp.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.task_rejected, null));
                }
            } else {
                //viewHolder.mImageViewThermometer.setVisibility(View.INVISIBLE);
                viewHolder.mTextViewTemp.setVisibility(View.INVISIBLE);
            }*/
        }/* else {
            viewHolder.mLinearLayoutHealthAndSafety.setVisibility(View.INVISIBLE);
        }*/
    }

    private void setAttendanceBackgroundColor(ViewHolder viewHolder, UsersList.ResultData usersList) {
        if (usersList.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.present, mContext))) {
            viewHolder.mTechnicianAttendance.setBackgroundResource(R.drawable.tech_present);
            viewHolder.mTechnicianAttendance.setText(R.string.present);
        } else if (usersList.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.absent, mContext))) {
            viewHolder.mTechnicianAttendance.setBackgroundResource(R.drawable.tech_absentt);
            viewHolder.mTechnicianAttendance.setText(R.string.absent);
        } else if (usersList.getAttendance().equalsIgnoreCase(BaseActivity.getLocaleStringResource(new Locale(Constant.PreferredLanguage.LOCALE_ENGLISH), R.string.idle, mContext))) {
            viewHolder.mTechnicianAttendance.setBackgroundResource(R.drawable.tech_idle);
            viewHolder.mTechnicianAttendance.setText(R.string.idle);
        } else
            viewHolder.mTechnicianAttendance.setBackgroundResource(R.drawable.tech_defaault);
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTechnicianName, mTechnicianAttendance, mTechOccupation, mTextViewTemp, mTextViewBattery, mTextViewGPS, mTextView_trackHistory;
        private ImageView mProfile, mImageViewArogyaStatus, mImageViewThermometer, mImageViewCalender, mImageViewCall, mImageViewMessage;
        //        private View viewLine;
        private LinearLayout mLinearLayoutHealthAndSafety;
        private RelativeLayout relativeTechDetails;

        ViewHolder(View view) {
            super(view);

            mTechnicianName = view.findViewById(R.id.technician_name);
            mTechOccupation = view.findViewById(R.id.technician_occupation);
            mProfile = view.findViewById(R.id.technician_image);
            mTechnicianAttendance = view.findViewById(R.id.technician_attendance);
            mLinearLayoutHealthAndSafety = view.findViewById(R.id.linear_layout_health_and_safety);
            mTextViewTemp = view.findViewById(R.id.textView_body_temp);
            mTextViewBattery = view.findViewById(R.id.textView_battery_status);
            mTextViewGPS = view.findViewById(R.id.textView_gps_status);
            mImageViewArogyaStatus = view.findViewById(R.id.imageView_arogyastatus);
            mImageViewThermometer = view.findViewById(R.id.imageView_thermometer);
//            viewLine = view.findViewById(R.id.view_line);
            mImageViewCalender = view.findViewById(R.id.imageView_calender);
            mImageViewCall = view.findViewById(R.id.imageView_call);
            mImageViewMessage = view.findViewById(R.id.imageView_message);
            mTextView_trackHistory = view.findViewById(R.id.imageView_trackHistory);
            relativeTechDetails = view.findViewById(R.id.relative_tech_details);
            view.setOnClickListener(this);
            mImageViewCalender.setOnClickListener(this);
            mImageViewCall.setOnClickListener(this);
            mImageViewMessage.setOnClickListener(this);
            mTextView_trackHistory.setOnClickListener(this);
            relativeTechDetails.setOnClickListener(this);


            mImageViewCalender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*OwnerAttendanceFragment ownerAttendanceFragment = new OwnerAttendanceFragment();
                    ownerAttendanceFragment.showTechAttendanceDialogForAdapter(mContext, fragment,
                            mResultDataListFilter.get(getAdapterPosition()).getFirstName(),
                            mResultDataListFilter.get(getAdapterPosition()).getLastName(),
                            mResultDataListFilter.get(getAdapterPosition()).getId(),
                            mResultDataListFilter.get(getAdapterPosition()).getRole(),
                            mResultDataListFilter.get(getAdapterPosition()).getContactNo());*/

                    OwnerAttendanceFragment ownerAttendanceFragment = new OwnerAttendanceFragment();
                    ownerAttendanceFragment.showTechAttendanceDialogNew(mContext, fragment,mResultDataListFilter.get(getAdapterPosition()).getFirstName(),
                            mResultDataListFilter.get(getAdapterPosition()).getLastName(),
                            mResultDataListFilter.get(getAdapterPosition()).getId(),
                            mResultDataListFilter.get(getAdapterPosition()).getRole(),
                            mResultDataListFilter.get(getAdapterPosition()).getContactNo(),
                            mResultDataListFilter.get(getAdapterPosition()).getPhoto());

                   /* AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    TechAttendanceFragmentForOwner myFragment = new TechAttendanceFragmentForOwner();
                    Bundle bundle = new Bundle();
                    bundle.putInt("techID", mResultDataListFilter.get(getAdapterPosition()).getId());
                    bundle.putString("FirstName", mResultDataListFilter.get(getAdapterPosition()).getFirstName());
                    bundle.putString("LastName", mResultDataListFilter.get(getAdapterPosition()).getLastName());
                    bundle.putString("Role", mResultDataListFilter.get(getAdapterPosition()).getRole());
                    bundle.putString("ContactNo", mResultDataListFilter.get(getAdapterPosition()).getContactNo());
                    bundle.putString("Photo", mResultDataListFilter.get(getAdapterPosition()).getPhoto());
                    myFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();*/

                 /*   ((OwnerAttendanceFragment) fragment).showTechAttendanceDialogForAdapter(mContext, fragment,
                            mResultDataListFilter.get(getAdapterPosition()).getFirstName(),
                            mResultDataListFilter.get(getAdapterPosition()).getLastName(),
                            mResultDataListFilter.get(getAdapterPosition()).getId(),
                            mResultDataListFilter.get(getAdapterPosition()).getRole(),
                            mResultDataListFilter.get(getAdapterPosition()).getContactNo());*/

                    /*AppCompatActivity activity = (AppCompatActivity) mContext;
                    OwnerAttendanceFragment myFragment = new OwnerAttendanceFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();*/
                }
            });
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }
    }


    public UsersList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<UsersList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mUserList;
                } else {
                    List<UsersList.ResultData> filteredList = new ArrayList<>();
                    for (UsersList.ResultData row : mUserList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getUserName().toLowerCase().contains(charString) || row.getFirstName().toLowerCase().contains(charSequence)
                                || row.getLastName().toLowerCase().contains(charString)) {
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<UsersList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void restoreItem(UsersList.ResultData usersList, int position) {
        mResultDataListFilter.add(position, usersList);
        notifyItemInserted(position);
    }
}


