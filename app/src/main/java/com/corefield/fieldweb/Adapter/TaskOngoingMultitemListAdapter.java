package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class TaskOngoingMultitemListAdapter extends RecyclerView.Adapter<TaskOngoingMultitemListAdapter.ViewHolder> {

    private List<ItemsList.ResultData> mItemsList;
    private LayoutInflater mInflater;
    List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public TaskOngoingMultitemListAdapter(Context context, List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.task_details_multiple_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TasksList.MultipleItemAssigned itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtItemDetails.setText(itemList.getItemName());
            viewHolder.txtAssignQty.setText(String.valueOf(itemList.getItemQuantity()));
            viewHolder.txtUsedQty.setText(String.valueOf(itemList.getUsedItemQty()));

        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtItemDetails, txtAssignQty, txtUsedQty;

        ViewHolder(View view) {
            super(view);
            txtItemDetails = view.findViewById(R.id.txtItemDetails);
            txtAssignQty = view.findViewById(R.id.txtAssignQty);
            txtUsedQty = view.findViewById(R.id.txtUsedQty);

        }

    }

    public TasksList.MultipleItemAssigned getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<TasksList.MultipleItemAssigned> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


