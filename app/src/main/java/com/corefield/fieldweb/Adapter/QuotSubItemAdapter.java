package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Account.SaveQuotationDTO;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class QuotSubItemAdapter extends RecyclerView.Adapter<QuotSubItemAdapter.ViewHolder> {

    private List<SaveQuotationDTO.QuoteItemList> mItemsList;
    private LayoutInflater mInflater;
    List<SaveQuotationDTO.QuoteItemList> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public QuotSubItemAdapter(Context context, List<SaveQuotationDTO.QuoteItemList> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.quot_sub_service_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        SaveQuotationDTO.QuoteItemList itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtserviceName.setText(itemList.getItemName());
            viewHolder.txtQty.setText(String.valueOf(itemList.getUnitPrice()));
            viewHolder.txtTotal.setText("₹"+String.valueOf(itemList.getQuantity()));
            /*int pos = position + 1;
            viewHolder.txtSrNo.setText(String.valueOf(pos));*/
            //
            viewHolder.txtUnitPrice.setText("₹"+String.valueOf(itemList.getTotalPrice()));


        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSrNo, txtserviceName, txtQty, txtUnitPrice, txtTotal;

        ViewHolder(View view) {
            super(view);
            txtSrNo = view.findViewById(R.id.txtSrNo);
            txtserviceName = view.findViewById(R.id.txtserviceName);
            txtQty = view.findViewById(R.id.txtQty);
            txtUnitPrice = view.findViewById(R.id.txtUnitPrice);
            txtTotal = view.findViewById(R.id.txtTotal);

        }

    }

    public SaveQuotationDTO.QuoteItemList getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<SaveQuotationDTO.QuoteItemList> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(SaveQuotationDTO.QuoteItemList itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


