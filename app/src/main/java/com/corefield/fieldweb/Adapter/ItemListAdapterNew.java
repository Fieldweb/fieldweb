package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryDetailsEnhancedFragment;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryFragment;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class ItemListAdapterNew extends RecyclerView.Adapter<ItemListAdapterNew.ViewHolder> implements Filterable {

    private List<ItemsList.ResultData> mItemsList;
    private List<ItemsList.ResultData> mItemsListFilter;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener clickListener;
    private int mPosition;
    /*ItemsList.ResultData itemList;*/
    Fragment fragment;

    /**
     * Constructor
     *
     * @param context
     * @param position   position of item in ItemsList
     * @param mItemsList List of items
     */
    public ItemListAdapterNew(Context context, int position, List<ItemsList.ResultData> mItemsList, RecyclerTouchListener clickListener, Fragment fragment) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemsList = mItemsList;
        this.mItemsListFilter = mItemsList;
        this.mPosition = position;
        this.mContext = context;
        this.clickListener = clickListener;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.itemslist_new, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ItemsList.ResultData itemList = mItemsListFilter.get(position);
        try {
            if (itemList.getItemUnitTypeName() == null) {
                viewHolder.mItemName.setText(itemList.getName());
            } else {
                viewHolder.mItemName.setText(itemList.getName());
            }
        } catch (Exception e) {
            e.getMessage();
        }
        /* viewHolder.mItemName.setText(itemList.getName());*/
        //viewHolder.mItemUnit.setText(itemList.getItemUnitTypeName());
        try {
            if (itemList.getItemUnitTypeName() == null) {
                viewHolder.mItemUnit.setText("NA");
            } else {
                viewHolder.mItemUnit.setText(itemList.getItemUnitTypeName());
            }
        } catch (Exception e) {
            e.getMessage();
        }
        viewHolder.mItemQuantity.setText(String.valueOf(itemList.getUnAssignedQuantity()));
        viewHolder.textViewIssuedQty.setText(String.valueOf(itemList.getAssignedQuantity()));

        viewHolder.mID.setText("[" + (itemList.getId() + "]"));

        try {
            viewHolder.txtUsedQty.setText(String.valueOf(itemList.getUsedItemDetailsDtoObj().getUsedQty()));
        } catch (Exception ex) {
            ex.getMessage();
        }

        if (itemList.getDescription() != null && !itemList.getDescription().isEmpty())
            viewHolder.mItemDescription.setText(itemList.getDescription());

        //int total = itemList.getUnAssignedQuantity() + itemList.getAssignedQuantity();
        //viewHolder.textViewTotalQty.setText(String.valueOf(total));

        try {
            // FOR IMAGE DISPLAY
            Picasso.get().load(itemList.getItemImagePath()).placeholder(R.drawable.ic_default_item).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.itemImg, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    viewHolder.itemImg.setImageResource(R.drawable.ic_default_item);
                }
            });
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return mItemsListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mItemName, mItemUnit, mItemQuantity, mID, mItemDescription, textViewIssuedQty, txtUsedQty, textViewTotalQty;
        private ImageView mItemUpdate, mItemAssign, mItemDelete, itemImg;

        ViewHolder(View view) {
            super(view);
            mItemName = view.findViewById(R.id.item_name);
            mItemUnit = view.findViewById(R.id.item_unit);
            mID = view.findViewById(R.id.item_id);
            mItemQuantity = view.findViewById(R.id.item_quantity);
            mItemDescription = view.findViewById(R.id.item_description);
            mItemUpdate = view.findViewById(R.id.image_item_update);
            mItemAssign = view.findViewById(R.id.image_enter);
            mItemDelete = view.findViewById(R.id.image_delete);
            textViewIssuedQty = view.findViewById(R.id.textView_issued_qty);
            txtUsedQty = view.findViewById(R.id.textView_used_qty);
            textViewTotalQty = view.findViewById(R.id.textView_total_qty);
            itemImg = view.findViewById(R.id.itemImg);
            mItemUpdate.setOnClickListener(this);
            mItemAssign.setOnClickListener(this);
            mItemDelete.setOnClickListener(this);
            view.setOnClickListener(this);


            mItemUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemDialog itemDialog = ItemDialog.getInstance();
                    itemDialog.updateItemEditDialog(mContext, fragment, mItemsListFilter.get(getAdapterPosition()).getName(),
                            String.valueOf(mItemsListFilter.get(getAdapterPosition()).getQuantity()),
                            mItemsListFilter.get(getAdapterPosition()).getId(),
                            mItemsListFilter.get(getAdapterPosition()).getItemUnitTypeName(),
                            mItemsListFilter.get(getAdapterPosition()).getSalesPrice(),
                            mItemsListFilter.get(getAdapterPosition()).getPurchasePrice(),
                            mItemsListFilter.get(getAdapterPosition()).getItemImagePath());
                }
            });

            mItemAssign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemDialog itemDialog = ItemDialog.getInstance();
                    itemDialog.assignItemDialogFromAdapter(mContext, mItemsListFilter.get(getAdapterPosition()).getName(),
                            mItemsListFilter.get(getAdapterPosition()).getTechFullName(),
                            mItemsListFilter.get(getAdapterPosition()).getUserId(),
                            mItemsListFilter.get(getAdapterPosition()).getId());
                }
            });

            mItemDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mItemsListFilter.get(getAdapterPosition()).getAssignedQuantity() > 0) {
                        // if greater than ZERO then redirect
                        ItemDialog itemDialog = ItemDialog.getInstance();
                        itemDialog.deleteItemDialog(mContext, mItemsListFilter.get(getAdapterPosition()).getId());
                    } else {
                        ItemDialog itemDialog = ItemDialog.getInstance();
                        itemDialog.SureTodeleteItemDialog(mContext, mItemsListFilter.get(getAdapterPosition()).getId(), fragment);
                    }

                }
            });
        }


        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());

            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            //ItemInventoryFragment myFragment = new ItemInventoryFragment();
            ItemInventoryDetailsEnhancedFragment myFragment = new ItemInventoryDetailsEnhancedFragment();
            Bundle args = new Bundle();
            args.putInt("itemId", mItemsListFilter.get(getAdapterPosition()).getId());
            args.putDouble("PurchasePrice", mItemsListFilter.get(getAdapterPosition()).getPurchasePrice());
            args.putString("ItemImage", mItemsListFilter.get(getAdapterPosition()).getItemImagePath());
            myFragment.setArguments(args);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
        }
    }

    public ItemsList.ResultData getItem(int id) {
        return mItemsListFilter.get(id);
    }

    public List<ItemsList.ResultData> getData() {
        return mItemsListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mItemsListFilter = mItemsList;
                } else {
                    List<ItemsList.ResultData> filteredList = new ArrayList<>();
                    for (ItemsList.ResultData row : mItemsList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (String.valueOf(row.getUnAssignedQuantity()).toLowerCase().contains(charString) || row.getName().toUpperCase().contains(charSequence)
                                || String.valueOf(row.getAssignedQuantity()).toLowerCase().contains(charString) || row.getName().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    mItemsListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mItemsListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItemsListFilter = (ArrayList<ItemsList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


