package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to view the notes list in Task Details form
 * //
 **/
public class NotesViewAdapter extends RecyclerView.Adapter<NotesViewAdapter.ViewHolder> {

    protected final String TAG = NotesViewAdapter.class.getSimpleName();
    private List<TaskClosure.TechnicalNotedto> mNotesList;
    private LayoutInflater mInflater;
    private Context mContext;

    public NotesViewAdapter(List<TaskClosure.TechnicalNotedto> resultDataList, Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.mNotesList = resultDataList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.task_closure_note_view_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        TaskClosure.TechnicalNotedto notes = mNotesList.get(i);
        int noteNo = i;
        noteNo = noteNo + 1;
        viewHolder.textViewNoteSummary.setTextColor(ContextCompat.getColor(mContext, R.color.quantum_grey600));
        if (notes.getTechnicalNote1() != null && !notes.getTechnicalNote1().equalsIgnoreCase("")) {
            viewHolder.textViewNoteSummary.setText(noteNo + ". " + notes.getTechnicalNote1());
        }
    }

    @Override
    public int getItemCount() {
        return mNotesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNoteSummary;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNoteSummary = itemView.findViewById(R.id.textView_note_view_text);
        }
    }

    public TaskClosure.TechnicalNotedto getItem(int id) {
        return mNotesList.get(id);
    }

    public List<TaskClosure.TechnicalNotedto> getData() {
        return mNotesList;
    }

}


