package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.DatesDto;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.FWLogger;

import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class DatesListAdapter extends RecyclerView.Adapter<DatesListAdapter.ViewHolder> {

    private List<DatesDto> mDatesList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    int mSelectedPosition;

    /**
     * Constructor
     *
     * @param context
     * @param datesList List of dates and days
     */
    public DatesListAdapter(Context context, List<DatesDto> datesList, int selectedPosition) {
        this.mInflater = LayoutInflater.from(context);
        this.mDatesList = datesList;
        this.mContext = context;
        this.mSelectedPosition = selectedPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.dates_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        DatesDto date = mDatesList.get(position);

        viewHolder.mDate.setText("" + date.getDate());
        viewHolder.mDay.setText(date.getDay());

        FWLogger.logInfo("Dates : ", "mSelectedPosition : "+mSelectedPosition + " position : "+position);
        if (mSelectedPosition != position) {
            // view not selected
            viewHolder.relativeDate.setBackground(mContext.getResources().getDrawable(R.drawable.bg_curve_border));
            viewHolder.mDate.setTextColor(mContext.getResources().getColor(R.color.White));
            viewHolder.mDay.setTextColor(mContext.getResources().getColor(R.color.White));
        } else {
            // view is selected
            viewHolder.relativeDate.setBackground(mContext.getResources().getDrawable(R.drawable.bg_curve_border_selected));
            viewHolder.mDate.setTextColor(mContext.getResources().getColor(R.color.black));
            viewHolder.mDay.setTextColor(mContext.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return mDatesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mDate, mDay;
        private RelativeLayout relativeDate;

        ViewHolder(View view) {
            super(view);
            mDate = view.findViewById(R.id.textView_date);
            mDay = view.findViewById(R.id.textView_day);
            relativeDate = view.findViewById(R.id.relative_date);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClick(view, getAdapterPosition());
                    mSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition());
        }
    }

    public DatesDto getMonth(int id) {
        return mDatesList.get(id);
    }

    public List<DatesDto> getMonthData() {
        return mDatesList;
    }

    public void setClickListener(RecyclerTouchListener clickListener) {
        this.mClickListener = clickListener;
    }
}


