package com.corefield.fieldweb.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.R;

import java.util.List;

public class OpenAIResponseAdapter extends RecyclerView.Adapter<OpenAIResponseAdapter.ViewHolder> {
    private List<String> mUserResponseList;
    private List<String> mAIresponseList;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView userResponseText;
        public TextView aiResponseText;

        public ViewHolder(View view) {
            super(view);
            userResponseText = (TextView) view.findViewById(R.id.user_response_text);
            aiResponseText = (TextView) view.findViewById(R.id.ai_response_text);
        }
    }

    public OpenAIResponseAdapter(List<String> userResponseList, List<String> aiResponseList) {
        mUserResponseList = userResponseList;
        mAIresponseList = aiResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_ai_response_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userResponseText.setText(mUserResponseList.get(position));
        holder.aiResponseText.setText(mAIresponseList.get(position));
    }

    @Override
    public int getItemCount() {
        return mUserResponseList.size();
    }
}
