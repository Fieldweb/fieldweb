package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.AddTask;
import com.corefield.fieldweb.DTO.Task.TaskClosure;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class TaskClosureMultitemListAdapter extends RecyclerView.Adapter<TaskClosureMultitemListAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList;
    Context mContext;
    MultipleUsdItemPostListener listener;
    ArrayList<TaskClosure.UsedItemDetailsDto> selectedUsdItemArray = new ArrayList<TaskClosure.UsedItemDetailsDto>();

    /**
     * Constructor
     *
     * @param context
     */
    public TaskClosureMultitemListAdapter(MultipleUsdItemPostListener listener, Context context, List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.task_closure_multiple_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TasksList.MultipleItemAssigned itemList = multipleItemAssignedArrayList.get(position);
        try {
            viewHolder.txtItemDetails.setText(position + 1 + ". " + itemList.getItemName());
            viewHolder.txtAssignQty.setText(String.valueOf(itemList.getItemQuantity()));

        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtItemDetails, txtAssignQty, txtUsedQty;

        ViewHolder(View view) {
            super(view);
            txtItemDetails = view.findViewById(R.id.txtItemDetails);
            txtAssignQty = view.findViewById(R.id.txtAssignQty);
            txtUsedQty = view.findViewById(R.id.edittext_task_item_quantity);

            txtUsedQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        String qty = txtUsedQty.getText().toString();
                        int assignedQty = Integer.parseInt(txtAssignQty.getText().toString());
                        if (Integer.parseInt(qty) != 0 || !qty.isEmpty() && Integer.parseInt(qty) > 0) {
                            if (Integer.parseInt(qty) <= assignedQty) {
                                TaskClosure.UsedItemDetailsDto usedItemDetailsDto = new TaskClosure.UsedItemDetailsDto();
                                usedItemDetailsDto.setItemId(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemId());
                                usedItemDetailsDto.setUsedQty(Integer.parseInt(qty));
                                usedItemDetailsDto.setAssignedQty(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemQuantity());
                                usedItemDetailsDto.setItemIssuedId(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemIssuedId());
                            /*selectedUsdItemArray.add(getAdapterPosition(), usedItemDetailsDto);
                            listener.onMultipleUsdItemPostListener(selectedUsdItemArray);*/

                                try {
                                    int itemId = multipleItemAssignedArrayList.get(getAdapterPosition()).getItemId();
                                    int selectedItemid = selectedUsdItemArray.get(getAdapterPosition()).getItemId();
                                    if (selectedItemid != itemId) {
                                        selectedUsdItemArray.get(getAdapterPosition()).setUsedQty(Integer.parseInt(qty));
                                        selectedUsdItemArray.add(usedItemDetailsDto);
                                        listener.onMultipleUsdItemPostListener(selectedUsdItemArray);
                                    } else {
                                        selectedUsdItemArray.get(getAdapterPosition()).setUsedQty(Integer.parseInt(qty));
                                        listener.onMultipleUsdItemPostListener(selectedUsdItemArray);
                                    }
                                } catch (Exception e) {
                                    selectedUsdItemArray.add(usedItemDetailsDto);
                                    listener.onMultipleUsdItemPostListener(selectedUsdItemArray);
                                    e.getMessage();
                                }

                            } else {
                                Toast.makeText(mContext, "Used Qty should not be greater than assigned qty!!", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(mContext, "Used Qty should not be zero or empty!!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                   /* try {
                        String qty = txtUsedQty.getText().toString();
                        if (Integer.parseInt(qty) != 0 || !qty.isEmpty() && Integer.parseInt(qty) > 0) {
                                TaskClosure.UsedItemDetailsDto usedItemDetailsDto = new TaskClosure.UsedItemDetailsDto();
                                usedItemDetailsDto.setItemId(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemId());
                                usedItemDetailsDto.setUsedQty(Integer.parseInt(s.toString()));
                                usedItemDetailsDto.setAssignedQty(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemQuantity());
                                usedItemDetailsDto.setItemIssuedId(multipleItemAssignedArrayList.get(getAdapterPosition()).getItemIssuedId());
                                selectedUsdItemArray.add(usedItemDetailsDto);
                                listener.onMultipleUsdItemPostListener(selectedUsdItemArray);
                        } else {
                            Toast.makeText(mContext, "Used Qty should not be zero or empty!!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        ex.getMessage();
                    }*/

                    //Toast.makeText(mContext, multipleItemAssignedArrayList.get(getAdapterPosition()).getItemQuantity() + "POS::" + getAdapterPosition(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public TasksList.MultipleItemAssigned getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<TasksList.MultipleItemAssigned> getData() {
        return multipleItemAssignedArrayList;
    }

    public interface MultipleUsdItemPostListener {
        void onMultipleUsdItemPostListener(ArrayList<TaskClosure.UsedItemDetailsDto> SODArray);
    }

   /* public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }*/
}


