package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends ArrayAdapter<CustomerList.ResultData> {
    private LayoutInflater layoutInflater;
    List<CustomerList.ResultData> mCustomers;

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((CustomerList.ResultData)resultValue).getCustomerName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<CustomerList.ResultData> suggestions = new ArrayList<CustomerList.ResultData>();
                for (CustomerList.ResultData customer : mCustomers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getCustomerName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<CustomerList.ResultData>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mCustomers);
            }
            notifyDataSetChanged();
        }
    };

    public CustomerAdapter(Context context, int textViewResourceId, List<CustomerList.ResultData> customers) {
        super(context, textViewResourceId, customers);
        // copy all the customers into a master list
        mCustomers = new ArrayList<CustomerList.ResultData>(customers.size());
        mCustomers.addAll(customers);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.customer_name_item, null);
        }

        CustomerList.ResultData customer = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.textView_customer);
        name.setText(customer.getCustomerName());

        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
}
