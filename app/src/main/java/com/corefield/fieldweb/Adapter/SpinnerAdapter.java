package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.corefield.fieldweb.DTO.CountryList.GetCountryList;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.TouchLessSignupActivity;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    public List<GetCountryList.ResultData> mCountryLists;
    LayoutInflater inflter;
    private List<GetCountryList.ResultData> mResultDataListFilter;


    public SpinnerAdapter(Context context, List<GetCountryList.ResultData> resultDataList) {
        //super(context, R.layout.spinner_value_layout, resultDataList);
        this.mContext = context;
        this.mCountryLists = resultDataList;
        //this.mResultDataListFilter = resultDataList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return mCountryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

   /* @NonNull
    @Override
    public Filter getFilter() {
        return new ProductFilter(this, mCountryLists);
    }*/

   /* @Override
    public long getItemId(int i) {
        return 0;
    }*/

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.spinner_value_layout, null);
        ImageView icon = (ImageView) view.findViewById(R.id.spinnerImages);
        TextView names = (TextView) view.findViewById(R.id.spinnerTextView);

        try {
            Picasso.get().load(mCountryLists.get(i).getCountryFlag()).into(icon);
        } catch (Exception e) {
            e.getMessage();
        }
        names.setText(mCountryLists.get(i).getCountryName());


        return view;
    }




    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mCountryLists;
                } else {
                    List<GetCountryList.ResultData> filteredList = new ArrayList<>();
                    for (GetCountryList.ResultData row : mCountryLists) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCountryCode().toLowerCase().contains(charString) ||
                                row.getCountryName().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<GetCountryList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

   /* private class ProductFilter extends Filter {

        SpinnerAdapter countryListAdapter;
        List<GetCountryList.ResultData> originalList;
        List<GetCountryList.ResultData> filteredList;

        public ProductFilter(SpinnerAdapter countryListAdapter, List<GetCountryList.ResultData> originalList) {
            super();
            this.countryListAdapter = countryListAdapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (GetCountryList.ResultData row : originalList) {
                    if (row.getCountryCode().toLowerCase().contains(filterPattern) ||
                            row.getCountryName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(row);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            countryListAdapter.mResultDataListFilter.clear();
            countryListAdapter.mResultDataListFilter.addAll((List) results.values);
            countryListAdapter.notifyDataSetChanged();
        }
    }*/

}


