package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.TechWiseItemList;
import com.corefield.fieldweb.DTO.User.UsersList;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of technician (Owner login)
 * @see
 */
public class ItemInventoryAdapter extends RecyclerView.Adapter<ItemInventoryAdapter.ViewHolder> implements Filterable {

    private List<TechWiseItemList.ResultData> mUserList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private List<TechWiseItemList.ResultData> mResultDataListFilter;


    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param userList List of Technician
     */
    public ItemInventoryAdapter(Context context, List<TechWiseItemList.ResultData> userList) {
        this.mInflater = LayoutInflater.from(context);
        this.mUserList = userList;
        this.mResultDataListFilter = userList;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.advance_item_inventory_tech_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TechWiseItemList.ResultData usersList = mResultDataListFilter.get(position);

        /*viewHolder.mTechnicianName.setText(usersList.getFirstName() + " " + usersList.getLastName());*/
        /*
        viewHolder.txtTaskid.setText(String.valueOf(usersList.getTaskId()));*/
        if (usersList.getTaskId() == 0) {
            viewHolder.txtTaskid.setText("NA");
        } else {
            viewHolder.txtTaskid.setText(String.valueOf(usersList.getTaskId()));
        }
        viewHolder.mTechnicianName.setText(usersList.getTechFullName());
        viewHolder.txtItemQty.setText(String.valueOf(usersList.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTechnicianName, txtItemQty, txtRefresh, txtTaskid;
        private ImageView mProfile;

        ViewHolder(View view) {
            super(view);

            mTechnicianName = view.findViewById(R.id.technician_name);
            txtItemQty = view.findViewById(R.id.txtItemQty);
            txtTaskid = view.findViewById(R.id.txtTaskid);
            txtRefresh = view.findViewById(R.id.txtRefresh);
            mProfile = view.findViewById(R.id.technician_image);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });

            txtRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ItemDialog issueItemDialog = ItemDialog.getInstance();
                    issueItemDialog.updateReturnItem(mContext,
                            mResultDataListFilter.get(getAdapterPosition()).getTechFullName(),
                            mResultDataListFilter.get(getAdapterPosition()).getQuantity(),
                            mResultDataListFilter.get(getAdapterPosition()).getName(),
                            mResultDataListFilter.get(getAdapterPosition()).getItemId(),
                            mResultDataListFilter.get(getAdapterPosition()).getUserId(),
                            mResultDataListFilter.get(getAdapterPosition()).getTaskId(),
                            mResultDataListFilter.get(getAdapterPosition()).getId());
                }
            });
        }


        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }
    }


    public TechWiseItemList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<TechWiseItemList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mUserList;
                } else {
                    List<TechWiseItemList.ResultData> filteredList = new ArrayList<>();
                    for (TechWiseItemList.ResultData row : mUserList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTechFullName().toLowerCase().contains(charString) /*|| String.valueOf(row.get()).toLowerCase().contains(charSequence)
                                || row.getLastName().toLowerCase().contains(charString)*/) {
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<TechWiseItemList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void restoreItem(TechWiseItemList.ResultData usersList, int position) {
        mResultDataListFilter.add(position, usersList);
        notifyItemInserted(position);
    }
}


