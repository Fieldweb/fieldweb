package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Expenditure.ExpenseTechList;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of technicians to Owner in Expense Management
 * @see
 */

public class ExpenseTechListAdapter extends RecyclerView.Adapter<ExpenseTechListAdapter.ViewHolder> implements Filterable {

    private List<ExpenseTechList.ResultData> mExpenseTechList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private int mPosition;
    private List<ExpenseTechList.ResultData> mResultDataListFilter;

    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param position        position of Technician in ExpenseTechList
     * @param expenseTechList List of technician
     */
    public ExpenseTechListAdapter(Context context, int position, List<ExpenseTechList.ResultData> expenseTechList) {
        this.mInflater = LayoutInflater.from(context);
        this.mExpenseTechList = expenseTechList;
        this.mResultDataListFilter = expenseTechList;
        this.mPosition = position;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.technician_list_expense_new, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ExpenseTechList.ResultData expenseTechData = mResultDataListFilter.get(position);

        viewHolder.mTechnicianName.setText(expenseTechData.getName());
        viewHolder.mAdvanceBalance.setText(String.valueOf(expenseTechData.getRemainingBalance()));
        Picasso.get().load(expenseTechData.getPhoto()).placeholder(R.drawable.profile_icon).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(viewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                viewHolder.mProfile.setImageResource(R.drawable.profile_icon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTechnicianName, mAdvanceBalance;
        private ImageView mProfile, mAddBal, mDeductBal;

        ViewHolder(View view) {
            super(view);
            mTechnicianName = view.findViewById(R.id.technician_name);
            mAdvanceBalance = view.findViewById(R.id.technician_advance_balance);
            mProfile = view.findViewById(R.id.technician_image);
            mAddBal = view.findViewById(R.id.add_bal);
            mDeductBal = view.findViewById(R.id.deduct_bal);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });
            mAddBal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivityNew) mContext).addBalance(mResultDataListFilter.get(getAdapterPosition()).getName(),
                            mResultDataListFilter.get(getAdapterPosition()).getUserId(),
                            mResultDataListFilter.get(getAdapterPosition()).getRemainingBalance());
                }
            });
            mDeductBal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /* ((HomeActivityNew) mContext).DeductBalance();*/
                    ((HomeActivityNew) mContext).DeductBalance(mResultDataListFilter.get(getAdapterPosition()).getName(),
                            mResultDataListFilter.get(getAdapterPosition()).getUserId(),
                            mResultDataListFilter.get(getAdapterPosition()).getRemainingBalance());
                }
            });
        }
    }

    public ExpenseTechList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<ExpenseTechList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mExpenseTechList;
                } else {
                    List<ExpenseTechList.ResultData> filteredList = new ArrayList<>();
                    for (ExpenseTechList.ResultData row : mExpenseTechList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString)) {  //|| String.valueOf(row.get).contains(charSequence)||  row.getLastName().toLowerCase().contains(charString.toLowerCase())
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<ExpenseTechList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mExpenseTechList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ExpenseTechList.ResultData usersList, int position) {
        mExpenseTechList.add(position, usersList);
        notifyItemInserted(position);
    }
}


