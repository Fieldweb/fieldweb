package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.corefield.fieldweb.DTO.HealthAndSaftey.ArogyaSetuStatusList;
import com.corefield.fieldweb.R;

import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.Adapter
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This adapter is used to populate the Aarogya Setu status
 * //
 **/
public class ArogyaSetuSpinnerAdapter extends BaseAdapter {

    private String TAG = ArogyaSetuSpinnerAdapter.class.getSimpleName();
    private Context mContext;
    private List<ArogyaSetuStatusList.ResultData> mArogyaSetuStatusList;
    private LayoutInflater mInflater;

    public ArogyaSetuSpinnerAdapter(Context context, List<ArogyaSetuStatusList.ResultData> arogyaSetuStatusList) {
        mContext = context;
        mArogyaSetuStatusList = arogyaSetuStatusList;
        mInflater = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return mArogyaSetuStatusList.size() - 1;
    }

    @Override
    public Object getItem(int position) {
        return mArogyaSetuStatusList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.arogyasetu_status_spinner_item, null);
        TextView zone = (TextView) convertView.findViewById(R.id.textView_zone);
        if (position == getCount()) {
            zone.setText("");
            zone.setHint(mArogyaSetuStatusList.get(position).getArogyaSetuStatus()); //"Hint to be displayed"
        } else {
            zone.setText(mArogyaSetuStatusList.get(position).getArogyaSetuStatus());
            zone.setTextColor(Color.parseColor(mArogyaSetuStatusList.get(position).getDescription()));
        }
        return convertView;
    }
}
