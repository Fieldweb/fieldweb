package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.ServiceManagement.ServiceTypeListDTO;
import com.corefield.fieldweb.FieldWeb.ServiceManagement.ServiceDetailsFragment;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class ServiceTypeListAdapter extends RecyclerView.Adapter<ServiceTypeListAdapter.ViewHolder> implements Filterable {

    private List<ServiceTypeListDTO.ResultData> mItemsList;
    private List<ServiceTypeListDTO.ResultData> mItemsListFilter;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener clickListener;
    Fragment fragment;
    int mCount = 0;


    public ServiceTypeListAdapter(Context context, List<ServiceTypeListDTO.ResultData> mItemsList, RecyclerTouchListener clickListener, Fragment fragment) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemsList = mItemsList;
        this.mItemsListFilter = mItemsList;
        this.mContext = context;
        this.clickListener = clickListener;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.servicetype_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ServiceTypeListDTO.ResultData itemList = mItemsListFilter.get(position);
        try {
            if (itemList.getServiceName() == null) {
                viewHolder.mItemName.setText("NA");
            } else {
                viewHolder.mItemName.setText(itemList.getServiceName());
            }
        } catch (Exception e) {
            e.getMessage();
        }

        try {
            if (itemList.getDescription() == null) {
                viewHolder.mItemUnit.setText("NA");
            } else {
                viewHolder.mItemUnit.setText(itemList.getDescription());
            }
        } catch (Exception e) {
            e.getMessage();
        }
        viewHolder.mItemQuantity.setText("₹ " + String.valueOf(itemList.getPrice()));

        if (itemList.getDescription() != null && !itemList.getDescription().isEmpty())
            viewHolder.mItemDescription.setText(itemList.getDescription());

        try {
            if (itemList.getPrice() != 0) {
                int strikePrice = itemList.getPrice() + 200;
                viewHolder.mstrikeOutText.setText("₹ " + String.valueOf(strikePrice));
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

        // 1
        try {
            SpannableString spannableString = new SpannableString(viewHolder.mstrikeOutText.getText().toString());
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
            spannableString.setSpan(strikethroughSpan, 0, viewHolder.mstrikeOutText.getText().toString().length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
            viewHolder.mstrikeOutText.setText(spannableString);
        } catch (Exception ex) {
            ex.getMessage();
        }
        //2
       /* try {
            SpannableString spannableString = new SpannableString(viewHolder.mrupeeStrikOutText.getText().toString());
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
            spannableString.setSpan(strikethroughSpan, 0, viewHolder.mrupeeStrikOutText.getText().toString().length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
            viewHolder.mrupeeStrikOutText.setText(spannableString);
        } catch (Exception ex) {
            ex.getMessage();
        }*/
    }

    @Override
    public int getItemCount() {
        return mItemsListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView /*mrupeeStrikOutText*/ mstrikeOutText, mItemName, mItemUnit, mItemQuantity, mID, mItemDescription, mtextViewMinus, mtextViewPlus;
        private ImageView mItemUpdate, mItemAssign, mItemDelete, itemImg;
        private EditText mEdittextitemquantity;

        ViewHolder(View view) {
            super(view);
            mItemName = view.findViewById(R.id.item_name);
            mItemUnit = view.findViewById(R.id.item_unit);
            mID = view.findViewById(R.id.item_id);
            mItemQuantity = view.findViewById(R.id.item_quantity);
            mItemDescription = view.findViewById(R.id.item_description);
            mItemUpdate = view.findViewById(R.id.image_item_update);
            mItemAssign = view.findViewById(R.id.image_enter);
            mItemDelete = view.findViewById(R.id.image_delete);
            //
            mtextViewMinus = view.findViewById(R.id.textView_minus);
            mtextViewPlus = view.findViewById(R.id.textView_plus);
            mEdittextitemquantity = view.findViewById(R.id.edittext_item_quantity);
            mstrikeOutText = view.findViewById(R.id.strikeOutText);
            //mrupeeStrikOutText = view.findViewById(R.id.rupeeStrikOutText);


            itemImg = view.findViewById(R.id.itemImg);
            view.setOnClickListener(this);

            mtextViewMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEdittextitemquantity.getText() != null && !mEdittextitemquantity.getText().toString().isEmpty()) {
                        mCount = Integer.parseInt(mEdittextitemquantity.getText().toString());
                        mCount--;
                        if (mCount <= 0) {
                            //mtextViewMinus.setVisibility(View.INVISIBLE);
                            Toast.makeText(mContext, "Quantity should not less than 1", Toast.LENGTH_SHORT).show();
                        } else {
                            mtextViewMinus.setVisibility(View.VISIBLE);
                            mEdittextitemquantity.setText(String.valueOf(mCount));
                        }
                    }
                }
            });

            mtextViewPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEdittextitemquantity.getText() != null && !mEdittextitemquantity.getText().toString().isEmpty()) {
                        mCount = Integer.parseInt(mEdittextitemquantity.getText().toString());
                        mCount++;
                        if (mCount <= 0) {
                            //mtextViewMinus.setVisibility(View.INVISIBLE);
                            Toast.makeText(mContext, "Quantity should not less than 1", Toast.LENGTH_SHORT).show();
                        } else {
                            mtextViewMinus.setVisibility(View.VISIBLE);
                            mEdittextitemquantity.setText(String.valueOf(mCount));
                        }
                    }
                }
            });
        }


        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());

            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            ServiceDetailsFragment myFragment = new ServiceDetailsFragment();
            Bundle args = new Bundle();
            args.putInt("serviceID", mItemsListFilter.get(getAdapterPosition()).getId());
            args.putString("serviceName", mItemsListFilter.get(getAdapterPosition()).getServiceName());
            args.putInt("servicePrice", mItemsListFilter.get(getAdapterPosition()).getPrice());
            args.putString("contactNo", mItemsListFilter.get(getAdapterPosition()).getContactNo());
            args.putString("description", mItemsListFilter.get(getAdapterPosition()).getDescription());
            myFragment.setArguments(args);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
        }
    }

    public ServiceTypeListDTO.ResultData getItem(int id) {
        return mItemsListFilter.get(id);
    }

    public List<ServiceTypeListDTO.ResultData> getData() {
        return mItemsListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mItemsListFilter = mItemsList;
                } else {
                    List<ServiceTypeListDTO.ResultData> filteredList = new ArrayList<>();
                    for (ServiceTypeListDTO.ResultData row : mItemsList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (String.valueOf(row.getServiceName()).toLowerCase().contains(charString) || row.getServiceName().toUpperCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    mItemsListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mItemsListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItemsListFilter = (ArrayList<ServiceTypeListDTO.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ServiceTypeListDTO.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


