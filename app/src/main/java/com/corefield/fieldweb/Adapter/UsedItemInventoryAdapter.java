package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.GetUsedItemList;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.Task.CRMTaskDetailsFragmentNew;
import com.corefield.fieldweb.FieldWeb.Task.TaskDetailsFragmentNew;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.Constant;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of technician (Owner login)
 * @see
 */
public class UsedItemInventoryAdapter extends RecyclerView.Adapter<UsedItemInventoryAdapter.ViewHolder> implements Filterable {

    private List<GetUsedItemList.ResultData> mUserList;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener mClickListener;
    private List<GetUsedItemList.ResultData> mResultDataListFilter;
    Fragment fragment;


    /**
     * Constructor without on item click listener
     *
     * @param context
     * @param userList List of Technician
     */
    public UsedItemInventoryAdapter(Context context, List<GetUsedItemList.ResultData> userList/* Fragment fragment*/) {
        this.mInflater = LayoutInflater.from(context);
        this.mUserList = userList;
        this.mResultDataListFilter = userList;
        this.mContext = context;
        //this.fragment = fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.advance_useditem_inventory_task_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        GetUsedItemList.ResultData usersList = mResultDataListFilter.get(position);

        try {
            viewHolder.mTechnicianName.setText(usersList.getTechFullName());
            //viewHolder.txtItemQty.setText(String.valueOf(usersList.getUsedQty()));
            viewHolder.txtAssignedQty.setText(String.valueOf(usersList.getAssignedQty()));
            viewHolder.txtIssuedQty.setText(String.valueOf(usersList.getItemIssuedQuantity()));
            viewHolder.txtUsedQty.setText(String.valueOf(usersList.getUsedQty()));
            viewHolder.textViewtaskID.setText(String.valueOf(usersList.getTaskId()));
            viewHolder.task_name.setText(String.valueOf(usersList.getTaskName()));
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return mResultDataListFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTechnicianName, textViewtaskID, task_name, txtAssignedQty, txtIssuedQty, txtUsedQty/*txtItemQty*/, txtRefresh, task_details;
        private ImageView mProfile;

        ViewHolder(View view) {
            super(view);

            mTechnicianName = view.findViewById(R.id.task_tech_name);
            //txtItemQty = view.findViewById(R.id.txtItemQty);
            txtAssignedQty = view.findViewById(R.id.txtAssignedQty);
            txtIssuedQty = view.findViewById(R.id.txtIssuedQty);
            txtUsedQty = view.findViewById(R.id.txtUsedQty);

            txtRefresh = view.findViewById(R.id.txtRefresh);
            mProfile = view.findViewById(R.id.technician_image);
            textViewtaskID = view.findViewById(R.id.textViewtaskID);
            task_name = view.findViewById(R.id.task_name);
            task_details = view.findViewById(R.id.task_details);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, getAdapterPosition());
                }
            });

            txtRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemDialog issueItemDialog = ItemDialog.getInstance();
                    issueItemDialog.updateUsedItem(mContext, mResultDataListFilter.get(getAdapterPosition()));
                }
            });

            task_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    CRMTaskDetailsFragmentNew taskDetailsFragment = new CRMTaskDetailsFragmentNew();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("downloadReport", true);
                    bundle.putBoolean("allowToReassign", true);
                    bundle.putInt("taskID", mResultDataListFilter.get(getAdapterPosition()).getTaskId());
                    taskDetailsFragment.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, taskDetailsFragment).addToBackStack(null).commit();
                }
            });
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(v, getAdapterPosition());
        }
    }


    public GetUsedItemList.ResultData getItem(int id) {
        return mResultDataListFilter.get(id);
    }

    public List<GetUsedItemList.ResultData> getData() {
        return mResultDataListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void removeItem(int position) {
        mResultDataListFilter.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mResultDataListFilter = mUserList;
                } else {
                    List<GetUsedItemList.ResultData> filteredList = new ArrayList<>();
                    for (GetUsedItemList.ResultData row : mUserList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTechFullName().toLowerCase().contains(charString) /*|| String.valueOf(row.get()).toLowerCase().contains(charSequence)
                                || row.getLastName().toLowerCase().contains(charString)*/) {
                            filteredList.add(row);
                        }
                    }

                    mResultDataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultDataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultDataListFilter = (ArrayList<GetUsedItemList.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void restoreItem(GetUsedItemList.ResultData usersList, int position) {
        mResultDataListFilter.add(position, usersList);
        notifyItemInserted(position);
    }
}


