package com.corefield.fieldweb.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.corefield.fieldweb.DTO.CountryList.GetCountryList;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewAdapter extends ArrayAdapter<GetCountryList.ResultData> {

    Context context;
    ArrayList<Bitmap> mbitmap_array;

    public CustomListViewAdapter(Context context, int resourceId,
                                 List<GetCountryList.ResultData> items, ArrayList<Bitmap> bitmap_array) {
        super(context, resourceId, items);
        this.context = context;
        this.mbitmap_array = bitmap_array;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        GetCountryList.ResultData rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.country_list_item, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.spinnerTextView);
            holder.imageView = (ImageView) convertView.findViewById(R.id.spinnerImages);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        // holder.txtDesc.setText(rowItem.getDesc());
        //holder.imageView.setImageResource(Integer.parseInt(rowItem.getCountryFlag()));
        try {
            //Picasso.get().load(rowItem.getCountryFlag()).into(holder.imageView);
            holder.txtTitle.setText(rowItem.getCountryName());
            if (mbitmap_array.get(position) != null) {
                holder.imageView.setImageBitmap(mbitmap_array.get(position));
            } else {
                holder.imageView.setImageDrawable(context.getResources().getDrawable(
                        R.drawable.fw_logo));
            }
        } catch (Exception e) {
            e.getMessage();
        }


        return convertView;
    }
}
