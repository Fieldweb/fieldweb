package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Item.ItemsList;
import com.corefield.fieldweb.DTO.Task.TasksList;
import com.corefield.fieldweb.FieldWeb.Admin.ItemInventoryDetailsEnhancedFragment;
import com.corefield.fieldweb.FieldWeb.Dialogs.ItemDialog;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class MultipleItemListAdapter extends RecyclerView.Adapter<MultipleItemListAdapter.ViewHolder> {

    private List<ItemsList.ResultData> mItemsList;
    private LayoutInflater mInflater;
    List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList;

    /**
     * Constructor
     *
     * @param context
     */
    public MultipleItemListAdapter(Context context, List<TasksList.MultipleItemAssigned> multipleItemAssignedArrayList) {
        this.mInflater = LayoutInflater.from(context);
        this.multipleItemAssignedArrayList = multipleItemAssignedArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.multiple_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TasksList.MultipleItemAssigned itemList = multipleItemAssignedArrayList.get(position);
        try {
            if (itemList.getItemName() != null) {
                viewHolder.mtxtMultiItemName.setText(position + 1 + ". " + itemList.getItemName());
            }
        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public int getItemCount() {
        return multipleItemAssignedArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mtxtMultiItemName;

        ViewHolder(View view) {
            super(view);
            mtxtMultiItemName = view.findViewById(R.id.txtMultiItemName);

        }

    }

    public TasksList.MultipleItemAssigned getItem(int id) {
        return multipleItemAssignedArrayList.get(id);
    }

    public List<TasksList.MultipleItemAssigned> getData() {
        return multipleItemAssignedArrayList;
    }

    public void removeItem(int position) {
        mItemsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemsList.ResultData itemsList, int position) {
        mItemsList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


