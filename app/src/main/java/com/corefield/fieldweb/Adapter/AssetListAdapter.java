package com.corefield.fieldweb.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.corefield.fieldweb.DTO.Customer.CustomerList;
import com.corefield.fieldweb.DTO.LeadManagement.LeadListDTO;
import com.corefield.fieldweb.FieldWeb.AssetManagement.AddEditAssetDialog;
import com.corefield.fieldweb.FieldWeb.AssetManagement.AssetDetailsFragment;
import com.corefield.fieldweb.FieldWeb.AssetManagement.AssetManagementFragment;
import com.corefield.fieldweb.FieldWeb.HomeActivityNew;
import com.corefield.fieldweb.FieldWeb.LeadManagement.LeadDetailsFragment;
import com.corefield.fieldweb.Listener.RecyclerTouchListener;
import com.corefield.fieldweb.R;
import com.corefield.fieldweb.Util.CommonFunction;
import com.corefield.fieldweb.Util.DateUtils;
import com.corefield.fieldweb.Util.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CoreField
 * @version 1.1
 * @implNote This class is used to Show/Manage the list of items added by the Owner (Owner login)
 * @see
 */
public class AssetListAdapter extends RecyclerView.Adapter<AssetListAdapter.ViewHolder> implements Filterable {

    private List<LeadListDTO.ResultData> mLeadList;
    private List<LeadListDTO.ResultData> mLeadListFilter;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerTouchListener clickListener;
    Fragment fragment;
    String custContactnum;
    private boolean isLoadingAdded = false;
    LeadListDTO.ResultData leadDataTele;
    public List<CustomerList.ResultData> mCustomerList;


    public AssetListAdapter(Context context, List<LeadListDTO.ResultData> mLeadList, RecyclerTouchListener clickListener, Fragment fragment,
                            List<CustomerList.ResultData> mCustomerList) {
        this.mInflater = LayoutInflater.from(context);
        this.mLeadList = mLeadList;
        this.mLeadListFilter = mLeadList;
        this.mContext = context;
        this.clickListener = clickListener;
        this.fragment = fragment;
        this.mCustomerList = mCustomerList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.asset_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        LeadListDTO.ResultData leadData = mLeadListFilter.get(position);

        leadDataTele = leadData;

        final ViewHolder viewFHolder = (ViewHolder) viewHolder;
        if (leadData != null)
            setLeadRowDetails(viewFHolder, leadData);
    }

    @Override
    public int getItemCount() {
        return mLeadListFilter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mtextViewtype, mleadCustName, mCustLeadId, mdateTime, mtxtServiceName;
        ImageView mleadlistCallbtn;
        ImageView meditProduct, mdeleteProduct;


        ViewHolder(View view) {
            super(view);
            mleadCustName = view.findViewById(R.id.leadCustName);
            mtxtServiceName = view.findViewById(R.id.txtServiceName);
            mCustLeadId = view.findViewById(R.id.custLeadId);
            mtextViewtype = view.findViewById(R.id.textView_type);
            mdateTime = view.findViewById(R.id.dateTime);
            mleadlistCallbtn = view.findViewById(R.id.leadlistCallbtn);
            meditProduct = view.findViewById(R.id.editProduct);
            mdeleteProduct = view.findViewById(R.id.deleteProduct);

            view.setOnClickListener(this);

            meditProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AssetManagementFragment assetManagementFragment = new AssetManagementFragment();
                    assetManagementFragment.checkCameraPermission();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.editProduct(mContext, activity, ((HomeActivityNew) activity).mServiceTypeResultData, mCustomerList);
                }
            });

            mdeleteProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    AddEditAssetDialog addEditAssetDialog = AddEditAssetDialog.getInstance();
                    addEditAssetDialog.deleteProduct(mContext, activity);
                }
            });

        }


        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
            AppCompatActivity activity = (AppCompatActivity) view.getContext();
            AssetDetailsFragment myFragment = new AssetDetailsFragment();
            Bundle args = new Bundle();
            args.putInt("leadID", mLeadListFilter.get(getAdapterPosition()).getLeadId());
            myFragment.setArguments(args);
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, myFragment).addToBackStack(null).commit();
        }

    }

    private void setLeadRowDetails(ViewHolder viewHolder, LeadListDTO.ResultData leResultData) {

        try {

            if (leResultData.getLeadStatus() != null || !leResultData.getLeadStatus().isEmpty()) {

                viewHolder.mleadCustName.setText(leResultData.getCustomerName());
                viewHolder.mtxtServiceName.setText(leResultData.getServiceName());
                viewHolder.mCustLeadId.setText("[" + "Ld" + String.valueOf(leResultData.getLeadId()) + "]");

                String formattedDate = DateUtils.convertDateFormat(leResultData.getLeadDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy");
                String formattedTime = DateUtils.timeFormatConversion(leResultData.getLeadTime(), "HH:mm:ss", "hh:mm aa");
                if (formattedDate != null && formattedTime != null)
                    viewHolder.mdateTime.setText(formattedDate + "  " + formattedTime);

                switch (leResultData.getLeadStatus()) {
                    case "InActive":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.inactive_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.inactive));
                        break;

                    case "Assigned":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.assigned_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.assigned));
                        break;

                    case "In discussion":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.indiscussion_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.indiscussion));
                        break;

                    case "Called":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.called_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.called));
                        break;

                    case "Dormant":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.dormant_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.dormant));
                        break;

                    case "Quote Sent":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.quotesent_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.quotesent));
                        break;

                    case "Converted":
                        viewHolder.mtextViewtype.setBackground(mContext.getResources().getDrawable(R.drawable.converted_background));
                        viewHolder.mtextViewtype.setText(mContext.getResources().getString(R.string.converted));
                        break;
                    default:
                        break;
                }

            }

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public LeadListDTO.ResultData getItem(int id) {
        return mLeadListFilter.get(id);
    }

    public List<LeadListDTO.ResultData> getData() {
        return mLeadListFilter;
    }

    public void setClickListener(RecyclerTouchListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mLeadListFilter = mLeadList;
                } else {
                    List<LeadListDTO.ResultData> filteredList = new ArrayList<>();
                    for (LeadListDTO.ResultData row : mLeadList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (String.valueOf(row.getCustomerName()).toLowerCase().contains(charString) || row.getCustomerName().toUpperCase().contains(charSequence) ||
                                String.valueOf(row.getLeadId()).toLowerCase().contains(charString)) {
                            filteredList.add(row);
                        }
                    }
                    mLeadListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mLeadListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mLeadListFilter = (ArrayList<LeadListDTO.ResultData>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void add(LeadListDTO.ResultData resultData) {
        mLeadList.add(resultData);
        notifyItemInserted(mLeadList.size() - 1);
    }

    public void addAll(List<LeadListDTO.ResultData> resultDataList) {
        for (LeadListDTO.ResultData resultData : resultDataList) {
            add(resultData);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new LeadListDTO.ResultData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mLeadList.size() - 1;
        if (position >= 0) {
            LeadListDTO.ResultData leadData = getItem(position);

            if (leadData != null) {
                mLeadList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public void removeItem(int position) {
        mLeadList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(LeadListDTO.ResultData itemsList, int position) {
        mLeadList.add(position, itemsList);
        notifyItemInserted(position);
    }
}


