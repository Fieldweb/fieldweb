package com.corefield.fieldweb.DTO.TravelPathHistory;

import com.google.gson.annotations.SerializedName;

public class TravelledPathHistory {


    @SerializedName("ResultData")
    private ResultData ResultData;
    @SerializedName("RecordCount")
    private int RecordCount;
    @SerializedName("PageSize")
    private int PageSize;
    @SerializedName("PageIndex")
    private int PageIndex;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @SerializedName("UserID")
        private int UserID;
        @SerializedName("TaskId")
        private int TaskId;
        @SerializedName("LocAddress")
        private String LocAddress;
        @SerializedName("Longitude")
        private String Longitude;
        @SerializedName("Latitude")
        private String Latitude;
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public String getLocAddress() {
            return LocAddress;
        }

        public void setLocAddress(String LocAddress) {
            this.LocAddress = LocAddress;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
