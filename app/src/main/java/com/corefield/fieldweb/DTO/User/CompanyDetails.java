package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 3/6/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.User
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class CompanyDetails {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Activity")
        private String Activity;
        @Expose
        @SerializedName("CompanyLogo")
        private String CompanyLogo;
        @Expose
        @SerializedName("CompanyWebsite")
        private String CompanyWebsite;
        @Expose
        @SerializedName("CompanyStatus")
        private boolean CompanyStatus;
        @Expose
        @SerializedName("CompanyDomain")
        private String CompanyDomain;
        @Expose
        @SerializedName("CIN")
        private String CIN;
        @Expose
        @SerializedName("RegistrationNo")
        private String RegistrationNo;
        @Expose
        @SerializedName("ContactNo")
        private long ContactNo;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getActivity() {
            return Activity;
        }

        public void setActivity(String Activity) {
            this.Activity = Activity;
        }

        public String getCompanyLogo() {
            return CompanyLogo;
        }

        public void setCompanyLogo(String CompanyLogo) {
            this.CompanyLogo = CompanyLogo;
        }

        public String getCompanyWebsite() {
            return CompanyWebsite;
        }

        public void setCompanyWebsite(String CompanyWebsite) {
            this.CompanyWebsite = CompanyWebsite;
        }

        public boolean getCompanyStatus() {
            return CompanyStatus;
        }

        public void setCompanyStatus(boolean CompanyStatus) {
            this.CompanyStatus = CompanyStatus;
        }

        public String getCompanyDomain() {
            return CompanyDomain;
        }

        public void setCompanyDomain(String CompanyDomain) {
            this.CompanyDomain = CompanyDomain;
        }

        public String getCIN() {
            return CIN;
        }

        public void setCIN(String CIN) {
            this.CIN = CIN;
        }

        public String getRegistrationNo() {
            return RegistrationNo;
        }

        public void setRegistrationNo(String RegistrationNo) {
            this.RegistrationNo = RegistrationNo;
        }

        public long getContactNo() {
            return ContactNo;
        }

        public void setContactNo(int ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
