package com.corefield.fieldweb.DTO.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * //
 * Created by CFS on 2/17/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Notification
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class Notification {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("AMCServiceDetailDtoObj")
        private AMCServiceDetailDtoObj AMCServiceDetailDtoObj;
        @Expose
        @SerializedName("NotificationCount")
        private int NotificationCount;
        @Expose
        @SerializedName("CommonDateTime")
        private String CommonDateTime;
        @Expose
        @SerializedName("IsNotificationActive")
        private boolean IsNotificationActive;
        @Expose
        @SerializedName("IsRead")
        private boolean IsRead;
        @Expose
        @SerializedName("PaymentModeId")
        private int PaymentModeId;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("EarningAmount")
        private int EarningAmount;
        @Expose
        @SerializedName("TechAddTime")
        private String TechAddTime;
        @Expose
        @SerializedName("TechAddDate")
        private String TechAddDate;
        @Expose
        @SerializedName("TaskClosureStatus")
        private boolean TaskClosureStatus;
        @Expose
        @SerializedName("PaymentNotReceived")
        private boolean PaymentNotReceived;
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskState")
        private int TaskState;
        @Expose
        @SerializedName("TaskTypeId")
        private int TaskTypeId;
        @Expose
        @SerializedName("TaskType")
        private String TaskType;
        @Expose
        @SerializedName("TaskStatusId")
        private int TaskStatusId;
        @Expose
        @SerializedName("TaskName")
        private String TaskName;
        @Expose
        @SerializedName("TaskStatus")
        private String TaskStatus;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("NotificationDay")
        private String NotificationDay;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("NotificationTime")
        private String NotificationTime;
        @Expose
        @SerializedName("NotificationDate")
        private String NotificationDate;
        @Expose
        @SerializedName("NotificationType")
        private String NotificationType;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("IsRegistered")
        private boolean IsRegistered;

        public boolean isRegistered() {
            return IsRegistered;
        }

        public void setRegistered(boolean registered) {
            IsRegistered = registered;
        }

        public AMCServiceDetailDtoObj getAMCServiceDetailDtoObj() {
            return AMCServiceDetailDtoObj;
        }

        public void setAMCServiceDetailDtoObj(AMCServiceDetailDtoObj AMCServiceDetailDtoObj) {
            this.AMCServiceDetailDtoObj = AMCServiceDetailDtoObj;
        }

        public int getNotificationCount() {
            return NotificationCount;
        }

        public void setNotificationCount(int NotificationCount) {
            this.NotificationCount = NotificationCount;
        }

        public String getCommonDateTime() {
            return CommonDateTime;
        }

        public void setCommonDateTime(String CommonDateTime) {
            this.CommonDateTime = CommonDateTime;
        }

        public boolean getIsNotificationActive() {
            return IsNotificationActive;
        }

        public void setIsNotificationActive(boolean IsNotificationActive) {
            this.IsNotificationActive = IsNotificationActive;
        }

        public boolean getIsRead() {
            return IsRead;
        }

        public void setIsRead(boolean IsRead) {
            this.IsRead = IsRead;
        }

        public int getPaymentModeId() {
            return PaymentModeId;
        }

        public void setPaymentModeId(int PaymentModeId) {
            this.PaymentModeId = PaymentModeId;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public int getEarningAmount() {
            return EarningAmount;
        }

        public void setEarningAmount(int EarningAmount) {
            this.EarningAmount = EarningAmount;
        }

        public String getTechAddTime() {
            return TechAddTime;
        }

        public void setTechAddTime(String TechAddTime) {
            this.TechAddTime = TechAddTime;
        }

        public String getTechAddDate() {
            return TechAddDate;
        }

        public void setTechAddDate(String TechAddDate) {
            this.TechAddDate = TechAddDate;
        }

        public boolean getTaskClosureStatus() {
            return TaskClosureStatus;
        }

        public void setTaskClosureStatus(boolean TaskClosureStatus) {
            this.TaskClosureStatus = TaskClosureStatus;
        }

        public boolean getPaymentNotReceived() {
            return PaymentNotReceived;
        }

        public void setPaymentNotReceived(boolean PaymentNotReceived) {
            this.PaymentNotReceived = PaymentNotReceived;
        }

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public int getTaskState() {
            return TaskState;
        }

        public void setTaskState(int TaskState) {
            this.TaskState = TaskState;
        }

        public int getTaskTypeId() {
            return TaskTypeId;
        }

        public void setTaskTypeId(int TaskTypeId) {
            this.TaskTypeId = TaskTypeId;
        }

        public String getTaskType() {
            return TaskType;
        }

        public void setTaskType(String TaskType) {
            this.TaskType = TaskType;
        }

        public int getTaskStatusId() {
            return TaskStatusId;
        }

        public void setTaskStatusId(int TaskStatusId) {
            this.TaskStatusId = TaskStatusId;
        }

        public String getTaskName() {
            return TaskName;
        }

        public void setTaskName(String TaskName) {
            this.TaskName = TaskName;
        }

        public String getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(String TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public String getNotificationDay() {
            return NotificationDay;
        }

        public void setNotificationDay(String NotificationDay) {
            this.NotificationDay = NotificationDay;
        }

        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getNotificationTime() {
            return NotificationTime;
        }

        public void setNotificationTime(String NotificationTime) {
            this.NotificationTime = NotificationTime;
        }

        public String getNotificationDate() {
            return NotificationDate;
        }

        public void setNotificationDate(String NotificationDate) {
            this.NotificationDate = NotificationDate;
        }

        public String getNotificationType() {
            return NotificationType;
        }

        public void setNotificationType(String NotificationType) {
            this.NotificationType = NotificationType;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class AMCServiceDetailDtoObj {
        @Expose
        @SerializedName("TaskDetails")
        private TaskDetails TaskDetails;
        @Expose
        @SerializedName("AMCTypeName")
        private String AMCTypeName;
        @Expose
        @SerializedName("ActualAMCSeriveDate")
        private String ActualAMCSeriveDate;
        @Expose
        @SerializedName("HoursNo")
        private int HoursNo;
        @Expose
        @SerializedName("DaysNo")
        private int DaysNo;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;
        @Expose
        @SerializedName("TotalServices")
        private int TotalServices;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("ActivationTime")
        private String ActivationTime;
        @Expose
        @SerializedName("ActivationDate")
        private String ActivationDate;
        @Expose
        @SerializedName("AMCName")
        private String AMCName;
        @Expose
        @SerializedName("AMCTypeId")
        private int AMCTypeId;
        @Expose
        @SerializedName("ServiceNo")
        private int ServiceNo;
        @Expose
        @SerializedName("AMCServiceDate")
        private String AMCServiceDate;
        @Expose
        @SerializedName("AMCsId")
        private int AMCsId;
        @Expose
        @SerializedName("AMCServiceDetailsId")
        private int AMCServiceDetailsId;

        public TaskDetails getTaskDetails() {
            return TaskDetails;
        }

        public void setTaskDetails(TaskDetails TaskDetails) {
            this.TaskDetails = TaskDetails;
        }

        public String getAMCTypeName() {
            return AMCTypeName;
        }

        public void setAMCTypeName(String AMCTypeName) {
            this.AMCTypeName = AMCTypeName;
        }

        public String getActualAMCSeriveDate() {
            return ActualAMCSeriveDate;
        }

        public void setActualAMCSeriveDate(String ActualAMCSeriveDate) {
            this.ActualAMCSeriveDate = ActualAMCSeriveDate;
        }

        public int getHoursNo() {
            return HoursNo;
        }

        public void setHoursNo(int HoursNo) {
            this.HoursNo = HoursNo;
        }

        public int getDaysNo() {
            return DaysNo;
        }

        public void setDaysNo(int DaysNo) {
            this.DaysNo = DaysNo;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }

        public int getTotalServices() {
            return TotalServices;
        }

        public void setTotalServices(int TotalServices) {
            this.TotalServices = TotalServices;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getActivationTime() {
            return ActivationTime;
        }

        public void setActivationTime(String ActivationTime) {
            this.ActivationTime = ActivationTime;
        }

        public String getActivationDate() {
            return ActivationDate;
        }

        public void setActivationDate(String ActivationDate) {
            this.ActivationDate = ActivationDate;
        }

        public String getAMCName() {
            return AMCName;
        }

        public void setAMCName(String AMCName) {
            this.AMCName = AMCName;
        }

        public int getAMCTypeId() {
            return AMCTypeId;
        }

        public void setAMCTypeId(int AMCTypeId) {
            this.AMCTypeId = AMCTypeId;
        }

        public int getServiceNo() {
            return ServiceNo;
        }

        public void setServiceNo(int ServiceNo) {
            this.ServiceNo = ServiceNo;
        }

        public String getAMCServiceDate() {
            return AMCServiceDate;
        }

        public void setAMCServiceDate(String AMCServiceDate) {
            this.AMCServiceDate = AMCServiceDate;
        }

        public int getAMCsId() {
            return AMCsId;
        }

        public void setAMCsId(int AMCsId) {
            this.AMCsId = AMCsId;
        }

        public int getAMCServiceDetailsId() {
            return AMCServiceDetailsId;
        }

        public void setAMCServiceDetailsId(int AMCServiceDetailsId) {
            this.AMCServiceDetailsId = AMCServiceDetailsId;
        }
    }

    public static class TaskDetails {
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("TechnicianUserId")
        private String TechnicianUserId;
        @Expose
        @SerializedName("TechnicianName")
        private String TechnicianName;
        @Expose
        @SerializedName("TaskName")
        private String TaskName;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getTechnicianUserId() {
            return TechnicianUserId;
        }

        public void setTechnicianUserId(String TechnicianUserId) {
            this.TechnicianUserId = TechnicianUserId;
        }

        public String getTechnicianName() {
            return TechnicianName;
        }

        public void setTechnicianName(String TechnicianName) {
            this.TechnicianName = TechnicianName;
        }

        public String getTaskName() {
            return TaskName;
        }

        public void setTaskName(String TaskName) {
            this.TaskName = TaskName;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }
    }
}
