package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Tasks list (Owner/Tech login)
 */
public class TasksList {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("OnHoldTaskDtos")
        private OnHoldTaskDtos OnHoldTaskDtos;
        @Expose
        @SerializedName("DeviceInfoList")
        private List<DeviceInfoList> DeviceInfoList;
        @Expose
        @SerializedName("MultipleItemAssigned")
        private List<MultipleItemAssigned> MultipleItemAssigned;
        @Expose
        @SerializedName("RejectedTaskNotes")
        private String RejectedTaskNotes;
        @Expose
        @SerializedName("DistanceUnitType")
        private String DistanceUnitType;
        @Expose
        @SerializedName("RoundDistanceTravelled")
        private String RoundDistanceTravelled;
        @Expose
        @SerializedName("DistanceTravelled")
        private String DistanceTravelled;
        @Expose
        @SerializedName("AudioFilePath")
        private String AudioFilePath;
        @Expose
        @SerializedName("PreDeviceInfoDto")
        private PreDeviceInfoDto PreDeviceInfoDto;
        @Expose
        @SerializedName("FirstNameLastName")
        private String FirstNameLastName;
        @Expose
        @SerializedName("TaskClosureStatus")
        private boolean TaskClosureStatus;
        @Expose
        @SerializedName("DateTimeStr")
        private String DateTimeStr;
        @Expose
        @SerializedName("PaymentModeId")
        private int PaymentModeId;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("TechContactNo")
        private String TechContactNo;
        @Expose
        @SerializedName("EarningAmount")
        private int EarningAmount;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("FullAddress")
        private String FullAddress;
        @Expose
        @SerializedName("PaymentNotReceived")
        private boolean PaymentNotReceived;
        @Expose
        @SerializedName("EndDate")
        private long EndDate;
        @Expose
        @SerializedName("StartDate")
        private long StartDate;
        @Expose
        @SerializedName("TechLongitude")
        private String TechLongitude;
        @Expose
        @SerializedName("TechLatitude")
        private String TechLatitude;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("WagesPerHours")
        private int WagesPerHours;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("LocationDesc")
        private String LocationDesc;
        @Expose
        @SerializedName("Zipcode")
        private String Zipcode;
        @Expose
        @SerializedName("LocationName")
        private String LocationName;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AssignedTo")
        private String AssignedTo;
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("CustomerEmailId")
        private String CustomerEmailId;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskType")
        private String TaskType;
        @Expose
        @SerializedName("TaskTypeId")
        private int TaskTypeId;
        @Expose
        @SerializedName("TaskState")
        private int TaskState;
        @Expose
        @SerializedName("TaskStatusId")
        private int TaskStatusId;
        @Expose
        @SerializedName("TaskStatus")
        private String TaskStatus;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public OnHoldTaskDtos getOnHoldTaskDtos() {
            return OnHoldTaskDtos;
        }

        public void setOnHoldTaskDtos(OnHoldTaskDtos OnHoldTaskDtos) {
            this.OnHoldTaskDtos = OnHoldTaskDtos;
        }

        public List<DeviceInfoList> getDeviceInfoList() {
            return DeviceInfoList;
        }

        public void setDeviceInfoList(List<DeviceInfoList> DeviceInfoList) {
            this.DeviceInfoList = DeviceInfoList;
        }

        public List<MultipleItemAssigned> getMultipleItemAssigned() {
            return MultipleItemAssigned;
        }

        public void setMultipleItemAssigned(List<MultipleItemAssigned> MultipleItemAssigned) {
            this.MultipleItemAssigned = MultipleItemAssigned;
        }

        public String getRejectedTaskNotes() {
            return RejectedTaskNotes;
        }

        public void setRejectedTaskNotes(String RejectedTaskNotes) {
            this.RejectedTaskNotes = RejectedTaskNotes;
        }

        public String getDistanceUnitType() {
            return DistanceUnitType;
        }

        public void setDistanceUnitType(String DistanceUnitType) {
            this.DistanceUnitType = DistanceUnitType;
        }

        public String getRoundDistanceTravelled() {
            return RoundDistanceTravelled;
        }

        public void setRoundDistanceTravelled(String RoundDistanceTravelled) {
            this.RoundDistanceTravelled = RoundDistanceTravelled;
        }

        public String getDistanceTravelled() {
            return DistanceTravelled;
        }

        public void setDistanceTravelled(String DistanceTravelled) {
            this.DistanceTravelled = DistanceTravelled;
        }

        public String getAudioFilePath() {
            return AudioFilePath;
        }

        public void setAudioFilePath(String AudioFilePath) {
            this.AudioFilePath = AudioFilePath;
        }

        public PreDeviceInfoDto getPreDeviceInfoDto() {
            return PreDeviceInfoDto;
        }

        public void setPreDeviceInfoDto(PreDeviceInfoDto PreDeviceInfoDto) {
            this.PreDeviceInfoDto = PreDeviceInfoDto;
        }

        public String getFirstNameLastName() {
            return FirstNameLastName;
        }

        public void setFirstNameLastName(String FirstNameLastName) {
            this.FirstNameLastName = FirstNameLastName;
        }

       /* public boolean getTaskClosureStatus() {
            return TaskClosureStatus;
        }*/

        public boolean isTaskClosureStatus() {
            return TaskClosureStatus;
        }

        public void setTaskClosureStatus(boolean TaskClosureStatus) {
            this.TaskClosureStatus = TaskClosureStatus;
        }

        public String getDateTimeStr() {
            return DateTimeStr;
        }

        public void setDateTimeStr(String DateTimeStr) {
            this.DateTimeStr = DateTimeStr;
        }

        public int getPaymentModeId() {
            return PaymentModeId;
        }

        public void setPaymentModeId(int PaymentModeId) {
            this.PaymentModeId = PaymentModeId;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getTechContactNo() {
            return TechContactNo;
        }

        public void setTechContactNo(String TechContactNo) {
            this.TechContactNo = TechContactNo;
        }

        public int getEarningAmount() {
            return EarningAmount;
        }

        public void setEarningAmount(int EarningAmount) {
            this.EarningAmount = EarningAmount;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getFullAddress() {
            return FullAddress;
        }

        public void setFullAddress(String FullAddress) {
            this.FullAddress = FullAddress;
        }

        public Boolean getPaymentNotReceived() {
            return PaymentNotReceived;
        }

        public void setPaymentNotReceived(boolean PaymentNotReceived) {
            this.PaymentNotReceived = PaymentNotReceived;
        }

        public long getEndDate() {
            return EndDate;
        }

        public void setEndDate(int EndDate) {
            this.EndDate = EndDate;
        }

        public long getStartDate() {
            return StartDate;
        }

        public void setStartDate(int StartDate) {
            this.StartDate = StartDate;
        }

        public String getTechLongitude() {
            return TechLongitude;
        }

        public void setTechLongitude(String TechLongitude) {
            this.TechLongitude = TechLongitude;
        }

        public String getTechLatitude() {
            return TechLatitude;
        }

        public void setTechLatitude(String TechLatitude) {
            this.TechLatitude = TechLatitude;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getWagesPerHours() {
            return WagesPerHours;
        }

        public void setWagesPerHours(int WagesPerHours) {
            this.WagesPerHours = WagesPerHours;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLocationDesc() {
            return LocationDesc;
        }

        public void setLocationDesc(String LocationDesc) {
            this.LocationDesc = LocationDesc;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String Zipcode) {
            this.Zipcode = Zipcode;
        }

        public String getLocationName() {
            return LocationName;
        }

        public void setLocationName(String LocationName) {
            this.LocationName = LocationName;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getAssignedTo() {
            return AssignedTo;
        }

        public void setAssignedTo(String AssignedTo) {
            this.AssignedTo = AssignedTo;
        }

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getCustomerEmailId() {
            return CustomerEmailId;
        }

        public void setCustomerEmailId(String CustomerEmailId) {
            this.CustomerEmailId = CustomerEmailId;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public String getTaskType() {
            return TaskType;
        }

        public void setTaskType(String TaskType) {
            this.TaskType = TaskType;
        }

        public int getTaskTypeId() {
            return TaskTypeId;
        }

        public void setTaskTypeId(int TaskTypeId) {
            this.TaskTypeId = TaskTypeId;
        }

        public int getTaskState() {
            return TaskState;
        }

        public void setTaskState(int TaskState) {
            this.TaskState = TaskState;
        }

        public int getTaskStatusId() {
            return TaskStatusId;
        }

        public void setTaskStatusId(int TaskStatusId) {
            this.TaskStatusId = TaskStatusId;
        }

        public String getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(String TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class OnHoldTaskDtos implements Serializable {
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Pick3")
        private String Pick3;
        @Expose
        @SerializedName("Pick2")
        private String Pick2;
        @Expose
        @SerializedName("Pick1")
        private String Pick1;
        @Expose
        @SerializedName("OnHoldNotes")
        private String OnHoldNotes;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getPick3() {
            return Pick3;
        }

        public void setPick3(String Pick3) {
            this.Pick3 = Pick3;
        }

        public String getPick2() {
            return Pick2;
        }

        public void setPick2(String Pick2) {
            this.Pick2 = Pick2;
        }

        public String getPick1() {
            return Pick1;
        }

        public void setPick1(String Pick1) {
            this.Pick1 = Pick1;
        }

        public String getOnHoldNotes() {
            return OnHoldNotes;
        }

        public void setOnHoldNotes(String OnHoldNotes) {
            this.OnHoldNotes = OnHoldNotes;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class DeviceInfoList {
        @Expose
        @SerializedName("DeviceReading")
        private int DeviceReading;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private String CreatedBy;
        @Expose
        @SerializedName("UserId")
        private String UserId;
        @Expose
        @SerializedName("TaskClosureDetailsId")
        private int TaskClosureDetailsId;
        @Expose
        @SerializedName("DevicePhoto3")
        private String DevicePhoto3;
        @Expose
        @SerializedName("DevicePhoto2")
        private String DevicePhoto2;
        @Expose
        @SerializedName("DevicePhoto1")
        private String DevicePhoto1;
        @Expose
        @SerializedName("ModelNumber")
        private String ModelNumber;
        @Expose
        @SerializedName("DeviceName")
        private String DeviceName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getDeviceReading() {
            return DeviceReading;
        }

        public void setDeviceReading(int DeviceReading) {
            this.DeviceReading = DeviceReading;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public int getTaskClosureDetailsId() {
            return TaskClosureDetailsId;
        }

        public void setTaskClosureDetailsId(int TaskClosureDetailsId) {
            this.TaskClosureDetailsId = TaskClosureDetailsId;
        }

        public String getDevicePhoto3() {
            return DevicePhoto3;
        }

        public void setDevicePhoto3(String DevicePhoto3) {
            this.DevicePhoto3 = DevicePhoto3;
        }

        public String getDevicePhoto2() {
            return DevicePhoto2;
        }

        public void setDevicePhoto2(String DevicePhoto2) {
            this.DevicePhoto2 = DevicePhoto2;
        }

        public String getDevicePhoto1() {
            return DevicePhoto1;
        }

        public void setDevicePhoto1(String DevicePhoto1) {
            this.DevicePhoto1 = DevicePhoto1;
        }

        public String getModelNumber() {
            return ModelNumber;
        }

        public void setModelNumber(String ModelNumber) {
            this.ModelNumber = ModelNumber;
        }

        public String getDeviceName() {
            return DeviceName;
        }

        public void setDeviceName(String DeviceName) {
            this.DeviceName = DeviceName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class MultipleItemAssigned implements Serializable {
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemQuantity")
        private int ItemQuantity;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("UsedItemQty")
        private int UsedItemQty;


        public int getUsedItemQty() {
            return UsedItemQty;
        }

        public void setUsedItemQty(int usedItemQty) {
            UsedItemQty = usedItemQty;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int itemIssuedId) {
            ItemIssuedId = itemIssuedId;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemQuantity() {
            return ItemQuantity;
        }

        public void setItemQuantity(int ItemQuantity) {
            this.ItemQuantity = ItemQuantity;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class PreDeviceInfoDto implements Serializable {
        @Expose
        @SerializedName("DeviceInfoImagePath2")
        private String DeviceInfoImagePath2;
        @Expose
        @SerializedName("DeviceInfoImagePath1")
        private String DeviceInfoImagePath1;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("DeviceInfoNotes")
        private String DeviceInfoNotes;
        @Expose
        @SerializedName("DeviceInfoImagePath")
        private String DeviceInfoImagePath;
        @Expose
        @SerializedName("DeviceInfoImageName")
        private String DeviceInfoImageName;
        @Expose
        @SerializedName("PreDeviceInfoId")
        private int PreDeviceInfoId;

        public String getDeviceInfoImagePath2() {
            return DeviceInfoImagePath2;
        }

        public void setDeviceInfoImagePath2(String DeviceInfoImagePath2) {
            this.DeviceInfoImagePath2 = DeviceInfoImagePath2;
        }

        public String getDeviceInfoImagePath1() {
            return DeviceInfoImagePath1;
        }

        public void setDeviceInfoImagePath1(String DeviceInfoImagePath1) {
            this.DeviceInfoImagePath1 = DeviceInfoImagePath1;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public String getDeviceInfoNotes() {
            return DeviceInfoNotes;
        }

        public void setDeviceInfoNotes(String DeviceInfoNotes) {
            this.DeviceInfoNotes = DeviceInfoNotes;
        }

        public String getDeviceInfoImagePath() {
            return DeviceInfoImagePath;
        }

        public void setDeviceInfoImagePath(String DeviceInfoImagePath) {
            this.DeviceInfoImagePath = DeviceInfoImagePath;
        }

        public String getDeviceInfoImageName() {
            return DeviceInfoImageName;
        }

        public void setDeviceInfoImageName(String DeviceInfoImageName) {
            this.DeviceInfoImageName = DeviceInfoImageName;
        }

        public int getPreDeviceInfoId() {
            return PreDeviceInfoId;
        }

        public void setPreDeviceInfoId(int PreDeviceInfoId) {
            this.PreDeviceInfoId = PreDeviceInfoId;
        }
    }
}
