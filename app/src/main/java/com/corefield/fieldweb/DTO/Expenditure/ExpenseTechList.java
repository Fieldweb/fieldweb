package com.corefield.fieldweb.DTO.Expenditure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get list of  Technician in expense management (Owner login)
 */
public class ExpenseTechList {
    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("EarnedAmount")
        private int EarnedAmount;
        @Expose
        @SerializedName("RemainingBalance")
        private int RemainingBalance;
        @Expose
        @SerializedName("ReturnAmount")
        private int ReturnAmount;
        @Expose
        @SerializedName("OpeningBalance")
        private int OpeningBalance;
        @Expose
        @SerializedName("CreditedAmonut")
        private int CreditedAmonut;

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getEarnedAmount() {
            return EarnedAmount;
        }

        public void setEarnedAmount(int EarnedAmount) {
            this.EarnedAmount = EarnedAmount;
        }

        public int getRemainingBalance() {
            return RemainingBalance;
        }

        public void setRemainingBalance(int RemainingBalance) {
            this.RemainingBalance = RemainingBalance;
        }

        public int getReturnAmount() {
            return ReturnAmount;
        }

        public void setReturnAmount(int ReturnAmount) {
            this.ReturnAmount = ReturnAmount;
        }

        public int getOpeningBalance() {
            return OpeningBalance;
        }

        public void setOpeningBalance(int OpeningBalance) {
            this.OpeningBalance = OpeningBalance;
        }

        public int getCreditedAmonut() {
            return CreditedAmonut;
        }

        public void setCreditedAmonut(int CreditedAmonut) {
            this.CreditedAmonut = CreditedAmonut;
        }
    }
}
