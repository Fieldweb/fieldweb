package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to Add item  (Owner login)
 */
public class AddItem {

    @Expose
    @SerializedName("ResultData")
    private Resultdata resultdata;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Code")
    private String code;

    public Resultdata getResultdata() {
        return resultdata;
    }

    public void setResultdata(Resultdata resultdata) {
        this.resultdata = resultdata;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class Resultdata {
        @Expose
        @SerializedName("ItemType")
        private int itemtype;
        @Expose
        @SerializedName("Quantity")
        private int quantity;
        @Expose
        @SerializedName("Description")
        private String description;
        @Expose
        @SerializedName("Name")
        private String name;
        @Expose
        @SerializedName("IsModelError")
        private boolean ismodelerror;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean issuccessful;
        @Expose
        @SerializedName("UpdatedBy")
        private int updatedby;
        @Expose
        @SerializedName("CreatedBy")
        private int createdby;
        @Expose
        @SerializedName("Id")
        private int id;
        @Expose
        @SerializedName("ItemUnitTypeId")
        private int ItemUnitTypeId;
        @Expose
        @SerializedName("SalesPrice")
        private String SalesPrice;
        @Expose
        @SerializedName("PurchasePrice")
        private String PurchasePrice;
        @Expose
        @SerializedName("ItemImagePath")
        private String ItemImagePath;
        @Expose
        @SerializedName("ImageFileName")
        private String ImageFileName;
        @Expose
        @SerializedName("ImageFileBase64Str")
        private String ImageFileBase64Str;

        public int getItemUnitTypeId() {
            return ItemUnitTypeId;
        }

        public void setItemUnitTypeId(int itemUnitTypeId) {
            ItemUnitTypeId = itemUnitTypeId;
        }

        public String getSalesPrice() {
            return SalesPrice;
        }

        public void setSalesPrice(String salesPrice) {
            SalesPrice = salesPrice;
        }

        public String getPurchasePrice() {
            return PurchasePrice;
        }

        public void setPurchasePrice(String purchasePrice) {
            PurchasePrice = purchasePrice;
        }

        public String getItemImagePath() {
            return ItemImagePath;
        }

        public void setItemImagePath(String itemImagePath) {
            ItemImagePath = itemImagePath;
        }

        public String getImageFileName() {
            return ImageFileName;
        }

        public void setImageFileName(String imageFileName) {
            ImageFileName = imageFileName;
        }

        public String getImageFileBase64Str() {
            return ImageFileBase64Str;
        }

        public void setImageFileBase64Str(String imageFileBase64Str) {
            ImageFileBase64Str = imageFileBase64Str;
        }

        public int getItemtype() {
            return itemtype;
        }

        public void setItemtype(int itemtype) {
            this.itemtype = itemtype;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean getIsmodelerror() {
            return ismodelerror;
        }

        public void setIsmodelerror(boolean ismodelerror) {
            this.ismodelerror = ismodelerror;
        }

        public boolean getIssuccessful() {
            return issuccessful;
        }

        public void setIssuccessful(boolean issuccessful) {
            this.issuccessful = issuccessful;
        }

        /*public String getUpdatedby() {
            return updatedby;
        }

        public void setUpdatedby(String updatedby) {
            this.updatedby = updatedby;
        }

        public String getCreatedby() {
            return createdby;
        }

        public void setCreatedby(String createdby) {
            this.createdby = createdby;
        }*/

        public int getUpdatedby() {
            return updatedby;
        }

        public void setUpdatedby(int updatedby) {
            this.updatedby = updatedby;
        }

        public int getCreatedby() {
            return createdby;
        }

        public void setCreatedby(int createdby) {
            this.createdby = createdby;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
