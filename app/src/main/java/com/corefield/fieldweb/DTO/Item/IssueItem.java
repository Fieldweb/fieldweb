package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to  issue item from item inventory to technician(Owner login)
 */
public class IssueItem {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("TransactionType")
        private int TransactionType;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public int getTransactionType() {
            return TransactionType;
        }

        public void setTransactionType(int TransactionType) {
            this.TransactionType = TransactionType;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }
    }
}
