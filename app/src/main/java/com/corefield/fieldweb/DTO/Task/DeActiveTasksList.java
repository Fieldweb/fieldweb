package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to de-Activate Task (Testing Mode)
 */
public class DeActiveTasksList {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("TechLongitude")
        private String TechLongitude;
        @Expose
        @SerializedName("TechLatitude")
        private String TechLatitude;
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("WagesPerHours")
        private int WagesPerHours;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("LocationDesc")
        private String LocationDesc;
        @Expose
        @SerializedName("Zipcode")
        private String Zipcode;
        @Expose
        @SerializedName("LocationName")
        private String LocationName;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AssignedTo")
        private String AssignedTo;
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskType")
        private String TaskType;
        @Expose
        @SerializedName("TaskTypeId")
        private int TaskTypeId;
        @Expose
        @SerializedName("TaskStatusId")
        private int TaskStatusId;
        @Expose
        @SerializedName("TaskStatus")
        private String TaskStatus;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("StartDate")
        private long StartDate;
        @Expose
        @SerializedName("EndDate")
        private long EndDate;
        @Expose
        @SerializedName("PaymentNotReceived")
        private Boolean PaymentNotReceived;
        @Expose
        @SerializedName("FullAddress")
        private String FullAddress;

        public String getFullAddress() {
            return FullAddress;
        }

        public void setFullAddress(String fullAddress) {
            FullAddress = fullAddress;
        }

        public int getTaskState() {
            return TaskState;
        }

        public void setTaskState(int taskState) {
            TaskState = taskState;
        }

        @Expose
        @SerializedName("TaskState")
        private int TaskState;

        public Boolean getPaymentNotReceived() {
            return PaymentNotReceived;
        }

        public void setPaymentNotReceived(Boolean paymentNotReceived) {
            PaymentNotReceived = paymentNotReceived;
        }

        public long getStartDate() {
            return StartDate;
        }

        public void setStartDate(int startDate) {
            StartDate = startDate;
        }

        public long getEndDate() {
            return EndDate;
        }

        public void setEndDate(int endDate) {
            EndDate = endDate;
        }

        public String getTechLongitude() {
            return TechLongitude;
        }

        public void setTechLongitude(String TechLongitude) {
            this.TechLongitude = TechLongitude;
        }

        public String getTechLatitude() {
            return TechLatitude;
        }

        public void setTechLatitude(String TechLatitude) {
            this.TechLatitude = TechLatitude;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getWagesPerHours() {
            return WagesPerHours;
        }

        public void setWagesPerHours(int WagesPerHours) {
            this.WagesPerHours = WagesPerHours;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLocationDesc() {
            return LocationDesc;
        }

        public void setLocationDesc(String LocationDesc) {
            this.LocationDesc = LocationDesc;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String Zipcode) {
            this.Zipcode = Zipcode;
        }

        public String getLocationName() {
            return LocationName;
        }

        public void setLocationName(String LocationName) {
            this.LocationName = LocationName;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getAssignedTo() {
            return AssignedTo;
        }

        public void setAssignedTo(String AssignedTo) {
            this.AssignedTo = AssignedTo;
        }

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public String getTaskType() {
            return TaskType;
        }

        public void setTaskType(String TaskType) {
            this.TaskType = TaskType;
        }

        public int getTaskTypeId() {
            return TaskTypeId;
        }

        public void setTaskTypeId(int TaskTypeId) {
            this.TaskTypeId = TaskTypeId;
        }

        public int getTaskStatusId() {
            return TaskStatusId;
        }

        public void setTaskStatusId(int TaskStatusId) {
            this.TaskStatusId = TaskStatusId;
        }

        public String getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(String TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
