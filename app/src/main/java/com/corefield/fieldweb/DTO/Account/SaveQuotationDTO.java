package com.corefield.fieldweb.DTO.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaveQuotationDTO {

    @Expose
    @SerializedName("QuoteServiceList")
    private List<QuoteServiceList> QuoteServiceList;
    @Expose
    @SerializedName("QuoteTaxList")
    private List<QuoteTaxList> QuoteTaxList;
    @Expose
    @SerializedName("QuoteItemList")
    private List<QuoteItemList> QuoteItemList;
    @Expose
    @SerializedName("Customer")
    private Customer Customer;
    @Expose
    @SerializedName("SLAAttachmentFile")
    private SLAAttachmentFile SLAAttachmentFile;
    @Expose
    @SerializedName("UpdatedDate")
    private String UpdatedDate;
    @Expose
    @SerializedName("UpdatedBy")
    private int UpdatedBy;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("IsActive")
    private boolean IsActive;
    @Expose
    @SerializedName("StatusId")
    private int StatusId;
    @Expose
    @SerializedName("CustomerId")
    private int CustomerId;
    @Expose
    @SerializedName("GrandTotalAmount")
    private float GrandTotalAmount;
    @Expose
    @SerializedName("TotalAmount")
    private float TotalAmount;
    @Expose
    @SerializedName("TermCondition")
    private String TermCondition;
    @Expose
    @SerializedName("ValidityDate")
    private String ValidityDate;
    @Expose
    @SerializedName("QuoteTime")
    private String QuoteTime;
    @Expose
    @SerializedName("ExtraAmount")
    private int ExtraAmount;
    @Expose
    @SerializedName("ExtraItem")
    private String ExtraItem;
    @Expose
    @SerializedName("QuoteName")
    private String QuoteName;
    @Expose
    @SerializedName("Id")
    private int Id;

    public List<QuoteServiceList> getQuoteServiceList() {
        return QuoteServiceList;
    }

    public void setQuoteServiceList(List<QuoteServiceList> QuoteServiceList) {
        this.QuoteServiceList = QuoteServiceList;
    }

    public List<QuoteTaxList> getQuoteTaxList() {
        return QuoteTaxList;
    }

    public void setQuoteTaxList(List<QuoteTaxList> QuoteTaxList) {
        this.QuoteTaxList = QuoteTaxList;
    }

    public List<QuoteItemList> getQuoteItemList() {
        return QuoteItemList;
    }

    public void setQuoteItemList(List<QuoteItemList> QuoteItemList) {
        this.QuoteItemList = QuoteItemList;
    }

    public Customer getCustomer() {
        return Customer;
    }

    public void setCustomer(Customer Customer) {
        this.Customer = Customer;
    }

    public SLAAttachmentFile getSLAAttachmentFile() {
        return SLAAttachmentFile;
    }

    public void setSLAAttachmentFile(SLAAttachmentFile SLAAttachmentFile) {
        this.SLAAttachmentFile = SLAAttachmentFile;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public int getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(int UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public int getStatusId() {
        return StatusId;
    }

    public void setStatusId(int StatusId) {
        this.StatusId = StatusId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public float getGrandTotalAmount() {
        return GrandTotalAmount;
    }

    public void setGrandTotalAmount(float GrandTotalAmount) {
        this.GrandTotalAmount = GrandTotalAmount;
    }

    public float getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(float TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    public String getTermCondition() {
        return TermCondition;
    }

    public void setTermCondition(String TermCondition) {
        this.TermCondition = TermCondition;
    }

    public String getValidityDate() {
        return ValidityDate;
    }

    public void setValidityDate(String ValidityDate) {
        this.ValidityDate = ValidityDate;
    }

    public String getQuoteTime() {
        return QuoteTime;
    }

    public void setQuoteTime(String QuoteTime) {
        this.QuoteTime = QuoteTime;
    }

    public int getExtraAmount() {
        return ExtraAmount;
    }

    public void setExtraAmount(int ExtraAmount) {
        this.ExtraAmount = ExtraAmount;
    }

    public String getExtraItem() {
        return ExtraItem;
    }

    public void setExtraItem(String ExtraItem) {
        this.ExtraItem = ExtraItem;
    }

    public String getQuoteName() {
        return QuoteName;
    }

    public void setQuoteName(String QuoteName) {
        this.QuoteName = QuoteName;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public static class QuoteServiceList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Price")
        private int Price;
        @Expose
        @SerializedName("ServiceName")
        private String ServiceName;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getPrice() {
            return Price;
        }

        public void setPrice(int Price) {
            this.Price = Price;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class QuoteTaxList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TaxAmount")
        private float TaxAmount;
        @Expose
        @SerializedName("Tax")
        private float Tax;
        @Expose
        @SerializedName("WithoutTax")
        private float WithoutTax;
        @Expose
        @SerializedName("DiscountAmounnt")
        private float DiscountAmounnt;
        @Expose
        @SerializedName("Discount")
        private float Discount;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public float getTaxAmount() {
            return TaxAmount;
        }

        public void setTaxAmount(float TaxAmount) {
            this.TaxAmount = TaxAmount;
        }

        public float getTax() {
            return Tax;
        }

        public void setTax(float Tax) {
            this.Tax = Tax;
        }

        public float getWithoutTax() {
            return WithoutTax;
        }

        public void setWithoutTax(float WithoutTax) {
            this.WithoutTax = WithoutTax;
        }

        public float getDiscountAmounnt() {
            return DiscountAmounnt;
        }

        public void setDiscountAmounnt(float DiscountAmounnt) {
            this.DiscountAmounnt = DiscountAmounnt;
        }

        public float getDiscount() {
            return Discount;
        }

        public void setDiscount(float Discount) {
            this.Discount = Discount;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class QuoteItemList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TotalPrice")
        private int TotalPrice;
        @Expose
        @SerializedName("UnitPrice")
        private int UnitPrice;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(int TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public int getUnitPrice() {
            return UnitPrice;
        }

        public void setUnitPrice(int UnitPrice) {
            this.UnitPrice = UnitPrice;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Customer {
        @Expose
        @SerializedName("LocationList")
        private LocationList LocationList;
        @Expose
        @SerializedName("Instructions")
        private String Instructions;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public LocationList getLocationList() {
            return LocationList;
        }

        public void setLocationList(LocationList LocationList) {
            this.LocationList = LocationList;
        }

        public String getInstructions() {
            return Instructions;
        }

        public void setInstructions(String Instructions) {
            this.Instructions = Instructions;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }
    }

    public static class LocationList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class SLAAttachmentFile {
        @Expose
        @SerializedName("InputStream")
        private InputStream InputStream;

        public InputStream getInputStream() {
            return InputStream;
        }

        public void setInputStream(InputStream InputStream) {
            this.InputStream = InputStream;
        }
    }

    public static class InputStream {
        @Expose
        @SerializedName("WriteTimeout")
        private int WriteTimeout;
        @Expose
        @SerializedName("ReadTimeout")
        private int ReadTimeout;
        @Expose
        @SerializedName("Position")
        private int Position;

        public int getWriteTimeout() {
            return WriteTimeout;
        }

        public void setWriteTimeout(int WriteTimeout) {
            this.WriteTimeout = WriteTimeout;
        }

        public int getReadTimeout() {
            return ReadTimeout;
        }

        public void setReadTimeout(int ReadTimeout) {
            this.ReadTimeout = ReadTimeout;
        }

        public int getPosition() {
            return Position;
        }

        public void setPosition(int Position) {
            this.Position = Position;
        }
    }
}
