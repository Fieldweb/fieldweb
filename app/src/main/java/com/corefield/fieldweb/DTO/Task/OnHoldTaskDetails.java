package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnHoldTaskDetails {


    @Expose
    @SerializedName("TaskStatus")
    private int TaskStatus;
    @Expose
    @SerializedName("UpdatedDate")
    private String UpdatedDate;
    @Expose
    @SerializedName("UpdatedBy")
    private int UpdatedBy;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("Pick3")
    private String Pick3;
    @Expose
    @SerializedName("Pick2")
    private String Pick2;
    @Expose
    @SerializedName("Pick1")
    private String Pick1;
    @Expose
    @SerializedName("OnHoldNotes")
    private String OnHoldNotes;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("TaskId")
    private int TaskId;
    @Expose
    @SerializedName("Id")
    private int Id;

    public int getTaskStatus() {
        return TaskStatus;
    }

    public void setTaskStatus(int TaskStatus) {
        this.TaskStatus = TaskStatus;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public int getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(int UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public String getPick3() {
        return Pick3;
    }

    public void setPick3(String Pick3) {
        this.Pick3 = Pick3;
    }

    public String getPick2() {
        return Pick2;
    }

    public void setPick2(String Pick2) {
        this.Pick2 = Pick2;
    }

    public String getPick1() {
        return Pick1;
    }

    public void setPick1(String Pick1) {
        this.Pick1 = Pick1;
    }

    public String getOnHoldNotes() {
        return OnHoldNotes;
    }

    public void setOnHoldNotes(String OnHoldNotes) {
        this.OnHoldNotes = OnHoldNotes;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getTaskId() {
        return TaskId;
    }

    public void setTaskId(int TaskId) {
        this.TaskId = TaskId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
}
