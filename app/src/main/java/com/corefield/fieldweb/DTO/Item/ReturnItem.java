package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnItem {


    @Expose
    @SerializedName("Notes")
    private String Notes;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("TaskId")
    private int TaskId;
    @Expose
    @SerializedName("ItemIssuedId")
    private int ItemIssuedId;
    @Expose
    @SerializedName("Quantity")
    private int Quantity;
    @Expose
    @SerializedName("ItemId")
    private int ItemId;
    @Expose
    @SerializedName("UserId")
    private int UserId;

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getTaskId() {
        return TaskId;
    }

    public void setTaskId(int TaskId) {
        this.TaskId = TaskId;
    }

    public int getItemIssuedId() {
        return ItemIssuedId;
    }

    public void setItemIssuedId(int ItemIssuedId) {
        this.ItemIssuedId = ItemIssuedId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public int getItemId() {
        return ItemId;
    }

    public void setItemId(int ItemId) {
        this.ItemId = ItemId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }
}
