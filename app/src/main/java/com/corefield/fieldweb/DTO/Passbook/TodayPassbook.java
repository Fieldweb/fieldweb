package com.corefield.fieldweb.DTO.Passbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get today Passbook for both Owner and Technician login
 */
public class TodayPassbook {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("Balance")
        private int Balance;
        @Expose
        @SerializedName("Credit")
        private int Credit;
        @Expose
        @SerializedName("Return")
        private int Return;
        @Expose
        @SerializedName("Expenses")
        private int Expenses;
        @Expose
        @SerializedName("EstimatedAmount")
        private int EstimatedAmount;
        @Expose
        @SerializedName("EarningAmount")
        private int EarningAmount;

        public int getBalance() {
            return Balance;
        }

        public void setBalance(int Balance) {
            this.Balance = Balance;
        }

        public int getCredit() {
            return Credit;
        }

        public void setCredit(int Credit) {
            this.Credit = Credit;
        }

        public int getReturn() {
            return Return;
        }

        public void setReturn(int Return) {
            this.Return = Return;
        }

        public int getExpenses() {
            return Expenses;
        }

        public void setExpenses(int Expenses) {
            this.Expenses = Expenses;
        }

        public int getEstimatedAmount() {
            return EstimatedAmount;
        }

        public void setEstimatedAmount(int EstimatedAmount) {
            this.EstimatedAmount = EstimatedAmount;
        }

        public int getEarningAmount() {
            return EarningAmount;
        }

        public void setEarningAmount(int EarningAmount) {
            this.EarningAmount = EarningAmount;
        }
    }
}
