package com.corefield.fieldweb.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to validate login credential for user
 */
public class FWUser {

    @Expose
    @SerializedName("ResultData")
    private Resultdata resultdata;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Code")
    private String code;

    public Resultdata getResultdata() {
        return resultdata;
    }

    public void setResultdata(Resultdata resultdata) {
        this.resultdata = resultdata;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class Resultdata {
        @Expose
        @SerializedName("UserGroupName")
        private String usergroupname;
        @Expose
        @SerializedName("UserID")
        private int userid;
        @Expose
        @SerializedName("Token")
        private String token;
        @Expose
        @SerializedName("IsForgotPassword")
        private boolean IsForgotPassword;
        @Expose
        @SerializedName("UserPreferredLanguage")
        private String UserPreferredLanguage;
        @Expose
        @SerializedName("OTP")
        private String OTP;
        @Expose
        @SerializedName("CountryDetailsId")
        private int CountryDetailsId;

        public int getCountryDetailsId() {
            return CountryDetailsId;
        }

        public void setCountryDetailsId(int countryDetailsId) {
            CountryDetailsId = countryDetailsId;
        }

        public String getOTP() {
            return OTP;
        }

        public void setOTP(String OTP) {
            this.OTP = OTP;
        }

        public String getUsergroupname() {
            return usergroupname;
        }

        public void setUsergroupname(String usergroupname) {
            this.usergroupname = usergroupname;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isForgotPassword() {
            return IsForgotPassword;
        }

        public void setForgotPassword(boolean forgetPassword) {
            IsForgotPassword = forgetPassword;
        }

        public String getUserPreferredLanguage() {
            return UserPreferredLanguage;
        }

        public void setUserPreferredLanguage(String userPreferredLanguage) {
            UserPreferredLanguage = userPreferredLanguage;
        }
    }

    @SerializedName("UserName")
    public String UserName;
    @SerializedName("Password")
    public String Password;
    @SerializedName("AndroidID")
    public String AndroidID;
    @SerializedName("UserPreferredLanguage")
    public String UserPreferredLanguage;

    public FWUser(String UserName, String Password, String AndroidID, String UserPreferredLanguage) {
        this.UserName = UserName;
        this.Password = Password;
        this.AndroidID = AndroidID;
        this.UserPreferredLanguage = UserPreferredLanguage;
    }
}
