package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceOccurrenceList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }
    }
}
