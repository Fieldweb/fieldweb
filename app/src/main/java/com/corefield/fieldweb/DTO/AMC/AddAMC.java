package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddAMC {

    @Expose
    @SerializedName("ProductDetail")
    private ProductDetail ProductDetail;
    @Expose
    @SerializedName("UpdatedDate")
    private String UpdatedDate;
    @Expose
    @SerializedName("UpdatedBy")
    private int UpdatedBy;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("AMCSetReminderId")
    private int AMCSetReminderId;
    @Expose
    @SerializedName("TotalServices")
    private int TotalServices;
    @Expose
    @SerializedName("AMCNotes")
    private String AMCNotes;
    @Expose
    @SerializedName("AMCAmount")
    private int AMCAmount;
    @Expose
    @SerializedName("ActivationTime")
    private String ActivationTime;
    @Expose
    @SerializedName("ActivationDate")
    private String ActivationDate;
    @Expose
    @SerializedName("ProductId")
    private int ProductId;
    @Expose
    @SerializedName("ServiceOccuranceId")
    private int ServiceOccuranceId;
    @Expose
    @SerializedName("AMCName")
    private String AMCName;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("AMCsId")
    private int AMCsId;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public ProductDetail getProductDetail() {
        return ProductDetail;
    }

    public void setProductDetail(ProductDetail ProductDetail) {
        this.ProductDetail = ProductDetail;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    /*public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }*/

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    /*public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }*/

    public int getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        UpdatedBy = updatedBy;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public int getAMCSetReminderId() {
        return AMCSetReminderId;
    }

    public void setAMCSetReminderId(int AMCSetReminderId) {
        this.AMCSetReminderId = AMCSetReminderId;
    }

    public int getTotalServices() {
        return TotalServices;
    }

    public void setTotalServices(int TotalServices) {
        this.TotalServices = TotalServices;
    }

    public String getAMCNotes() {
        return AMCNotes;
    }

    public void setAMCNotes(String AMCNotes) {
        this.AMCNotes = AMCNotes;
    }

    public int getAMCAmount() {
        return AMCAmount;
    }

    public void setAMCAmount(int AMCAmount) {
        this.AMCAmount = AMCAmount;
    }

    public String getActivationTime() {
        return ActivationTime;
    }

    public void setActivationTime(String ActivationTime) {
        this.ActivationTime = ActivationTime;
    }

    public String getActivationDate() {
        return ActivationDate;
    }

    public void setActivationDate(String ActivationDate) {
        this.ActivationDate = ActivationDate;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    public int getServiceOccuranceId() {
        return ServiceOccuranceId;
    }

    public void setServiceOccuranceId(int ServiceOccuranceId) {
        this.ServiceOccuranceId = ServiceOccuranceId;
    }

    public String getAMCName() {
        return AMCName;
    }

    public void setAMCName(String AMCName) {
        this.AMCName = AMCName;
    }

    /*public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }*/

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getAMCsId() {
        return AMCsId;
    }

    public void setAMCsId(int AMCsId) {
        this.AMCsId = AMCsId;
    }

    public static class ProductDetail {
        @Expose
        @SerializedName("Location")
        private Location Location;
        @Expose
        @SerializedName("CustomerDetail")
        private CustomerDetail CustomerDetail;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UnderWarranty")
        private boolean UnderWarranty;
        @Expose
        @SerializedName("CustomerId")
        private int CustomerId;
        @Expose
        @SerializedName("CustomerLocationId")
        private int CustomerLocationId;
        @Expose
        @SerializedName("ProductBrand")
        private String ProductBrand;
        @Expose
        @SerializedName("ProductSerialNo")
        private String ProductSerialNo;
        @Expose
        @SerializedName("ProductName")
        private String ProductName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("ProductDetailsId")
        private int ProductDetailsId;

        public Location getLocation() {
            return Location;
        }

        public void setLocation(Location Location) {
            this.Location = Location;
        }

        public CustomerDetail getCustomerDetail() {
            return CustomerDetail;
        }

        public void setCustomerDetail(CustomerDetail CustomerDetail) {
            this.CustomerDetail = CustomerDetail;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getUnderWarranty() {
            return UnderWarranty;
        }

        public void setUnderWarranty(boolean UnderWarranty) {
            this.UnderWarranty = UnderWarranty;
        }

        public int getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(int CustomerId) {
            this.CustomerId = CustomerId;
        }

        public int getCustomerLocationId() {
            return CustomerLocationId;
        }

        public void setCustomerLocationId(int CustomerLocationId) {
            this.CustomerLocationId = CustomerLocationId;
        }

        public String getProductBrand() {
            return ProductBrand;
        }

        public void setProductBrand(String ProductBrand) {
            this.ProductBrand = ProductBrand;
        }

        public String getProductSerialNo() {
            return ProductSerialNo;
        }

        public void setProductSerialNo(String ProductSerialNo) {
            this.ProductSerialNo = ProductSerialNo;
        }

        public String getProductName() {
            return ProductName;
        }

        public void setProductName(String ProductName) {
            this.ProductName = ProductName;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getProductDetailsId() {
            return ProductDetailsId;
        }

        public void setProductDetailsId(int ProductDetailsId) {
            this.ProductDetailsId = ProductDetailsId;
        }
    }

    public static class Location {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class CustomerDetail {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }
        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }*/

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }
    }
}
