package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AMCDashboardCount {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("TotalCompleted")
        private int TotalCompleted;
        @Expose
        @SerializedName("TotalExpired")
        private int TotalExpired;
        @Expose
        @SerializedName("TotalRenewal")
        private int TotalRenewal;
        @Expose
        @SerializedName("TotalUpcomming")
        private int TotalUpcomming;

        public int getTotalCompleted() {
            return TotalCompleted;
        }

        public void setTotalCompleted(int TotalCompleted) {
            this.TotalCompleted = TotalCompleted;
        }

        public int getTotalExpired() {
            return TotalExpired;
        }

        public void setTotalExpired(int TotalExpired) {
            this.TotalExpired = TotalExpired;
        }

        public int getTotalRenewal() {
            return TotalRenewal;
        }

        public void setTotalRenewal(int TotalRenewal) {
            this.TotalRenewal = TotalRenewal;
        }

        public int getTotalUpcomming() {
            return TotalUpcomming;
        }

        public void setTotalUpcomming(int TotalUpcomming) {
            this.TotalUpcomming = TotalUpcomming;
        }
    }
}
