package com.corefield.fieldweb.DTO.RegisterHere;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TouchlessTempRegistration {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("NoOfUsers")
        private int NoOfUsers;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("CountryDetailsId")
        private int CountryDetailsId;
        @Expose
        @SerializedName("OTP")
        private int OTP;
        @Expose
        @SerializedName("EmailOrContactNo")
        private String EmailOrContactNo;
        @Expose
        @SerializedName("TouchlessSignupId")
        private int TouchlessSignupId;

        public int getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(int NoOfUsers) {
            this.NoOfUsers = NoOfUsers;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getCountryDetailsId() {
            return CountryDetailsId;
        }

        public void setCountryDetailsId(int CountryDetailsId) {
            this.CountryDetailsId = CountryDetailsId;
        }

        public int getOTP() {
            return OTP;
        }

        public void setOTP(int OTP) {
            this.OTP = OTP;
        }

        public String getEmailOrContactNo() {
            return EmailOrContactNo;
        }

        public void setEmailOrContactNo(String EmailOrContactNo) {
            this.EmailOrContactNo = EmailOrContactNo;
        }

        public int getTouchlessSignupId() {
            return TouchlessSignupId;
        }

        public void setTouchlessSignupId(int TouchlessSignupId) {
            this.TouchlessSignupId = TouchlessSignupId;
        }
    }
}
