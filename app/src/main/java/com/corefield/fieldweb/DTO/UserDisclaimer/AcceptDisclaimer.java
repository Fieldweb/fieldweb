package com.corefield.fieldweb.DTO.UserDisclaimer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcceptDisclaimer {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("DCN")
        private String DCN;
        @Expose
        @SerializedName("AcceptedDate")
        private String AcceptedDate;
        @Expose
        @SerializedName("isAccepted")
        private boolean isAccepted;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserDisclaimerId")
        private int UserDisclaimerId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public String getDCN() {
            return DCN;
        }

        public void setDCN(String DCN) {
            this.DCN = DCN;
        }

        public String getAcceptedDate() {
            return AcceptedDate;
        }

        public void setAcceptedDate(String AcceptedDate) {
            this.AcceptedDate = AcceptedDate;
        }

        public boolean getIsAccepted() {
            return isAccepted;
        }

        public void setIsAccepted(boolean isAccepted) {
            this.isAccepted = isAccepted;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        public int getUserDisclaimerId() {
            return UserDisclaimerId;
        }

        public void setUserDisclaimerId(int UserDisclaimerId) {
            this.UserDisclaimerId = UserDisclaimerId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
