package com.corefield.fieldweb.DTO.HealthAndSaftey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * //
 * Created by CFS on 5/19/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.HealthAndSaftey
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This DTO used to parse the Aarogya Setu status list
 * //
 **/
public class ArogyaSetuStatusList {
    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("ArogyaSetuStatus")
        private String ArogyaSetuStatus;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getArogyaSetuStatus() {
            return ArogyaSetuStatus;
        }

        public void setArogyaSetuStatus(String ArogyaSetuStatus) {
            this.ArogyaSetuStatus = ArogyaSetuStatus;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
