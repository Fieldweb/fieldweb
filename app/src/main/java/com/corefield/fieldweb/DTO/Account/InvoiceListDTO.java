package com.corefield.fieldweb.DTO.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InvoiceListDTO {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("RemainingAmount")
        private int RemainingAmount;
        @Expose
        @SerializedName("TotalRecord")
        private int TotalRecord;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("StatusId")
        private int StatusId;
        @Expose
        @SerializedName("StatusName")
        private String StatusName;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("QuoteTaskName")
        private String QuoteTaskName;
        @Expose
        @SerializedName("InvoiceAmount")
        private int InvoiceAmount;
        @Expose
        @SerializedName("CustomerId")
        private int CustomerId;
        @Expose
        @SerializedName("QuoteTaskId")
        private int QuoteTaskId;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("InvoiceDate")
        private String InvoiceDate;
        @Expose
        @SerializedName("InvoiceCode")
        private String InvoiceCode;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getRemainingAmount() {
            return RemainingAmount;
        }

        public void setRemainingAmount(int RemainingAmount) {
            this.RemainingAmount = RemainingAmount;
        }

        public int getTotalRecord() {
            return TotalRecord;
        }

        public void setTotalRecord(int TotalRecord) {
            this.TotalRecord = TotalRecord;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getStatusId() {
            return StatusId;
        }

        public void setStatusId(int StatusId) {
            this.StatusId = StatusId;
        }

        public String getStatusName() {
            return StatusName;
        }

        public void setStatusName(String StatusName) {
            this.StatusName = StatusName;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getQuoteTaskName() {
            return QuoteTaskName;
        }

        public void setQuoteTaskName(String QuoteTaskName) {
            this.QuoteTaskName = QuoteTaskName;
        }

        public int getInvoiceAmount() {
            return InvoiceAmount;
        }

        public void setInvoiceAmount(int InvoiceAmount) {
            this.InvoiceAmount = InvoiceAmount;
        }

        public int getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(int CustomerId) {
            this.CustomerId = CustomerId;
        }

        public int getQuoteTaskId() {
            return QuoteTaskId;
        }

        public void setQuoteTaskId(int QuoteTaskId) {
            this.QuoteTaskId = QuoteTaskId;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public String getInvoiceDate() {
            return InvoiceDate;
        }

        public void setInvoiceDate(String InvoiceDate) {
            this.InvoiceDate = InvoiceDate;
        }

        public String getInvoiceCode() {
            return InvoiceCode;
        }

        public void setInvoiceCode(String InvoiceCode) {
            this.InvoiceCode = InvoiceCode;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
