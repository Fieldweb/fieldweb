package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to update user details
 */
public class UpdateUser {
    @Expose
    @SerializedName("Code")
    private String Code;
    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public static class ResultData {
        @Expose
        @SerializedName("ReferralCodesId")
        private int ReferralCodesId;
        @Expose
        @SerializedName("CompanyGSTorPanNo")
        private String CompanyGSTorPanNo;
        @Expose
        @SerializedName("CompanyServiceTypeId")
        private int CompanyServiceTypeId;
        @Expose
        @SerializedName("CompanyLogoName")
        private String CompanyLogoName;
        @Expose
        @SerializedName("CompanyLogo")
        private String CompanyLogo;
        @Expose
        @SerializedName("CompanyWebsite")
        private String CompanyWebsite;
        @Expose
        @SerializedName("CompanyContactNo")
        private int CompanyContactNo;
        @Expose
        @SerializedName("CompanyAddress")
        private String CompanyAddress;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;
        @Expose
        @SerializedName("CompanyId")
        private int CompanyId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("File")
        private String File;
        @Expose
        @SerializedName("FileName")
        private String FileName;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("DOB")
        private String DOB;
        @Expose
        @SerializedName("CompanyCity")
        private String CompanyCity;
        @Expose
        @SerializedName("AadharCardNo")
        private String AadharCardNo;
        @Expose
        @SerializedName("NoOfUsers")
        private String NoOfUsers;
        @Expose
        @SerializedName("IsMobileNoUpdate")
        private boolean IsMobileNoUpdate;
        @Expose
        @SerializedName("IsEmailIdUpdate")
        private boolean IsEmailIdUpdate;

        public boolean isMobileNoUpdate() {
            return IsMobileNoUpdate;
        }

        public void setMobileNoUpdate(boolean mobileNoUpdate) {
            IsMobileNoUpdate = mobileNoUpdate;
        }

        public boolean isEmailIdUpdate() {
            return IsEmailIdUpdate;
        }

        public void setEmailIdUpdate(boolean emailIdUpdate) {
            IsEmailIdUpdate = emailIdUpdate;
        }

        public String getAadharCardNo() {
            return AadharCardNo;
        }

        public void setAadharCardNo(String aadharCardNo) {
            AadharCardNo = aadharCardNo;
        }

        public String getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(String noOfUsers) {
            NoOfUsers = noOfUsers;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getCompanyCity() {
            return CompanyCity;
        }

        public void setCompanyCity(String companyCity) {
            CompanyCity = companyCity;
        }

        public int getReferralCodesId() {
            return ReferralCodesId;
        }

        public void setReferralCodesId(int ReferralCodesId) {
            this.ReferralCodesId = ReferralCodesId;
        }

        public String getCompanyGSTorPanNo() {
            return CompanyGSTorPanNo;
        }

        public void setCompanyGSTorPanNo(String CompanyGSTorPanNo) {
            this.CompanyGSTorPanNo = CompanyGSTorPanNo;
        }

        public int getCompanyServiceTypeId() {
            return CompanyServiceTypeId;
        }

        public void setCompanyServiceTypeId(int CompanyServiceTypeId) {
            this.CompanyServiceTypeId = CompanyServiceTypeId;
        }

        public String getCompanyLogoName() {
            return CompanyLogoName;
        }

        public void setCompanyLogoName(String CompanyLogoName) {
            this.CompanyLogoName = CompanyLogoName;
        }

        public String getCompanyLogo() {
            return CompanyLogo;
        }

        public void setCompanyLogo(String CompanyLogo) {
            this.CompanyLogo = CompanyLogo;
        }

        public String getCompanyWebsite() {
            return CompanyWebsite;
        }

        public void setCompanyWebsite(String CompanyWebsite) {
            this.CompanyWebsite = CompanyWebsite;
        }

        public int getCompanyContactNo() {
            return CompanyContactNo;
        }

        public void setCompanyContactNo(int CompanyContactNo) {
            this.CompanyContactNo = CompanyContactNo;
        }

        public String getCompanyAddress() {
            return CompanyAddress;
        }

        public void setCompanyAddress(String CompanyAddress) {
            this.CompanyAddress = CompanyAddress;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }

        public int getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(int CompanyId) {
            this.CompanyId = CompanyId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getFile() {
            return File;
        }

        public void setFile(String File) {
            this.File = File;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }
    }

    /*@Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("File")
        private String File;
        @Expose
        @SerializedName("FileName")
        private String FileName;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;

        public String getFile() {
            return File;
        }

        public void setFile(String File) {
            this.File = File;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }
    }*/
}
