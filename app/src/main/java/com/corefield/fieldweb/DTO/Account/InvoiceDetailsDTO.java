package com.corefield.fieldweb.DTO.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InvoiceDetailsDTO {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private double RecordCount;
    @Expose
    @SerializedName("PageSize")
    private double PageSize;
    @Expose
    @SerializedName("PageIndex")
    private double PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public double getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(double RecordCount) {
        this.RecordCount = RecordCount;
    }

    public double getPageSize() {
        return PageSize;
    }

    public void setPageSize(double PageSize) {
        this.PageSize = PageSize;
    }

    public double getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(double PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("QuotationId")
        private double QuotationId;
        @Expose
        @SerializedName("ServiceList")
        private List<ServiceList> ServiceList;
        @Expose
        @SerializedName("PaymentList")
        private List<PaymentList> PaymentList;
        @Expose
        @SerializedName("ItemList")
        private List<ItemList> ItemList;
        @Expose
        @SerializedName("RemainingAmount")
        private double RemainingAmount;
        @Expose
        @SerializedName("ReceivedAmount")
        private double ReceivedAmount;
        @Expose
        @SerializedName("GrandTotalAmount")
        private double GrandTotalAmount;
        @Expose
        @SerializedName("TaxAmount")
        private double TaxAmount;
        @Expose
        @SerializedName("Tax")
        private double Tax;
        @Expose
        @SerializedName("DiscountAmounnt")
        private double DiscountAmounnt;
        @Expose
        @SerializedName("Discount")
        private double Discount;
        @Expose
        @SerializedName("SubTotalAmount")
        private double SubTotalAmount;
        @Expose
        @SerializedName("ValidTillDate")
        private String ValidTillDate;
        @Expose
        @SerializedName("InvoiceDateTime")
        private String InvoiceDateTime;
        @Expose
        @SerializedName("ExtraAmount")
        private double ExtraAmount;
        @Expose
        @SerializedName("InvoiceAmount")
        private double InvoiceAmount;
        @Expose
        @SerializedName("InvoiceCode")
        private String InvoiceCode;
        @Expose
        @SerializedName("Landmark")
        private String Landmark;
        @Expose
        @SerializedName("CustAddress")
        private String CustAddress;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CIN")
        private String CIN;
        @Expose
        @SerializedName("CompanyLogo")
        private String CompanyLogo;
        @Expose
        @SerializedName("ContactNo")
        private double ContactNo;
        @Expose
        @SerializedName("CompanyAddress")
        private String CompanyAddress;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;

        public double getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(double QuotationId) {
            this.QuotationId = QuotationId;
        }

        public List<ServiceList> getServiceList() {
            return ServiceList;
        }

        public void setServiceList(List<ServiceList> ServiceList) {
            this.ServiceList = ServiceList;
        }

        public List<PaymentList> getPaymentList() {
            return PaymentList;
        }

        public void setPaymentList(List<PaymentList> PaymentList) {
            this.PaymentList = PaymentList;
        }

        public List<ItemList> getItemList() {
            return ItemList;
        }

        public void setItemList(List<ItemList> ItemList) {
            this.ItemList = ItemList;
        }

        public double getRemainingAmount() {
            return RemainingAmount;
        }

        public void setRemainingAmount(double RemainingAmount) {
            this.RemainingAmount = RemainingAmount;
        }

        public double getReceivedAmount() {
            return ReceivedAmount;
        }

        public void setReceivedAmount(double ReceivedAmount) {
            this.ReceivedAmount = ReceivedAmount;
        }

        public double getGrandTotalAmount() {
            return GrandTotalAmount;
        }

        public void setGrandTotalAmount(double GrandTotalAmount) {
            this.GrandTotalAmount = GrandTotalAmount;
        }

        public double getTaxAmount() {
            return TaxAmount;
        }

        public void setTaxAmount(double TaxAmount) {
            this.TaxAmount = TaxAmount;
        }

        public double getTax() {
            return Tax;
        }

        public void setTax(double Tax) {
            this.Tax = Tax;
        }

        public double getDiscountAmounnt() {
            return DiscountAmounnt;
        }

        public void setDiscountAmounnt(double DiscountAmounnt) {
            this.DiscountAmounnt = DiscountAmounnt;
        }

        public double getDiscount() {
            return Discount;
        }

        public void setDiscount(double Discount) {
            this.Discount = Discount;
        }

        public double getSubTotalAmount() {
            return SubTotalAmount;
        }

        public void setSubTotalAmount(double SubTotalAmount) {
            this.SubTotalAmount = SubTotalAmount;
        }

        public String getValidTillDate() {
            return ValidTillDate;
        }

        public void setValidTillDate(String ValidTillDate) {
            this.ValidTillDate = ValidTillDate;
        }

        public String getInvoiceDateTime() {
            return InvoiceDateTime;
        }

        public void setInvoiceDateTime(String InvoiceDateTime) {
            this.InvoiceDateTime = InvoiceDateTime;
        }

        public double getExtraAmount() {
            return ExtraAmount;
        }

        public void setExtraAmount(double ExtraAmount) {
            this.ExtraAmount = ExtraAmount;
        }

        public double getInvoiceAmount() {
            return InvoiceAmount;
        }

        public void setInvoiceAmount(double InvoiceAmount) {
            this.InvoiceAmount = InvoiceAmount;
        }

        public String getInvoiceCode() {
            return InvoiceCode;
        }

        public void setInvoiceCode(String InvoiceCode) {
            this.InvoiceCode = InvoiceCode;
        }

        public String getLandmark() {
            return Landmark;
        }

        public void setLandmark(String Landmark) {
            this.Landmark = Landmark;
        }

        public String getCustAddress() {
            return CustAddress;
        }

        public void setCustAddress(String CustAddress) {
            this.CustAddress = CustAddress;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getCIN() {
            return CIN;
        }

        public void setCIN(String CIN) {
            this.CIN = CIN;
        }

        public String getCompanyLogo() {
            return CompanyLogo;
        }

        public void setCompanyLogo(String CompanyLogo) {
            this.CompanyLogo = CompanyLogo;
        }

        public double getContactNo() {
            return ContactNo;
        }

        public void setContactNo(double ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getCompanyAddress() {
            return CompanyAddress;
        }

        public void setCompanyAddress(String CompanyAddress) {
            this.CompanyAddress = CompanyAddress;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }
    }

    public static class ServiceList {
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ServiceName")
        private String ServiceName;
        @Expose
        @SerializedName("ServiceId")
        private double ServiceId;

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public double getServiceId() {
            return ServiceId;
        }

        public void setServiceId(double ServiceId) {
            this.ServiceId = ServiceId;
        }
    }

    public static class PaymentList {
        @Expose
        @SerializedName("PaymentDate")
        private String PaymentDate;
        @Expose
        @SerializedName("PaymentTransactionType")
        private String PaymentTransactionType;
        @Expose
        @SerializedName("Amount")
        private double Amount;
        @Expose
        @SerializedName("Id")
        private double Id;

        public String getPaymentDate() {
            return PaymentDate;
        }

        public void setPaymentDate(String PaymentDate) {
            this.PaymentDate = PaymentDate;
        }

        public String getPaymentTransactionType() {
            return PaymentTransactionType;
        }

        public void setPaymentTransactionType(String PaymentTransactionType) {
            this.PaymentTransactionType = PaymentTransactionType;
        }

        public double getAmount() {
            return Amount;
        }

        public void setAmount(double Amount) {
            this.Amount = Amount;
        }

        public double getId() {
            return Id;
        }

        public void setId(double Id) {
            this.Id = Id;
        }
    }

    public static class ItemList {
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("UnitPrice")
        private double UnitPrice;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemId")
        private double ItemId;

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getUnitPrice() {
            return UnitPrice;
        }

        public void setUnitPrice(double UnitPrice) {
            this.UnitPrice = UnitPrice;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public double getItemId() {
            return ItemId;
        }

        public void setItemId(double ItemId) {
            this.ItemId = ItemId;
        }
    }
}
