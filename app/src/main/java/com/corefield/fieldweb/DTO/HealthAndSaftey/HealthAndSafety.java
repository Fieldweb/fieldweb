package com.corefield.fieldweb.DTO.HealthAndSaftey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 5/18/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.HealthAndSaftey
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This DTO is used to parse the health and safety status
 * //
 **/
public class HealthAndSafety {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("IsTodayHealthStatus")
        private IsTodayHealthStatus IsTodayHealthStatus;
        @Expose
        @SerializedName("IsHealthFeaturesEnabled")
        private boolean IsHealthFeaturesEnabled;

        public IsTodayHealthStatus getIsTodayHealthStatus() {
            return IsTodayHealthStatus;
        }

        public void setIsTodayHealthStatus(IsTodayHealthStatus IsTodayHealthStatus) {
            this.IsTodayHealthStatus = IsTodayHealthStatus;
        }

        public boolean getIsHealthFeaturesEnabled() {
            return IsHealthFeaturesEnabled;
        }

        public void setIsHealthFeaturesEnabled(boolean IsHealthFeaturesEnabled) {
            this.IsHealthFeaturesEnabled = IsHealthFeaturesEnabled;
        }
    }

    public static class IsTodayHealthStatus {
        @Expose
        @SerializedName("isHealthAndSafetyValuesAddedForToday")
        private boolean isHealthAndSafetyValuesAddedForToday;

        public boolean getIsHealthAndSafetyValuesAddedForToday() {
            return isHealthAndSafetyValuesAddedForToday;
        }

        public void setIsHealthAndSafetyValuesAddedForToday(boolean isHealthAndSafetyValuesAddedForToday) {
            this.isHealthAndSafetyValuesAddedForToday = isHealthAndSafetyValuesAddedForToday;
        }
    }
}
