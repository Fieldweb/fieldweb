package com.corefield.fieldweb.DTO.Attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Monthly Attendance of technician for current Month (Tech login)
 */
public class TechMonthlyAttendance {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AttendanceTypeId")
        private int AttendanceTypeId;
        @Expose
        @SerializedName("Attendance")
        private String Attendance;
        @Expose
        @SerializedName("Date")
        private String Date;
        @Expose
        @SerializedName("CheckIn")
        private String CheckIn;
        @Expose
        @SerializedName("CheckOut")
        private String CheckOut;


        public String getCheckIn() {
            return CheckIn;
        }

        public void setCheckIn(String checkIn) {
            CheckIn = checkIn;
        }

        public String getCheckOut() {
            return CheckOut;
        }

        public void setCheckOut(String checkOut) {
            CheckOut = checkOut;
        }



        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getAttendanceTypeId() {
            return AttendanceTypeId;
        }

        public void setAttendanceTypeId(int AttendanceTypeId) {
            this.AttendanceTypeId = AttendanceTypeId;
        }

        public String getAttendance() {
            return Attendance;
        }

        public void setAttendance(String Attendance) {
            this.Attendance = Attendance;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }
    }
}
