package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddDeductUsedItem {


    @Expose
    @SerializedName("Notes")
    private String Notes;
    @Expose
    @SerializedName("UpdatedDate")
    private String UpdatedDate;
    @Expose
    @SerializedName("UpdateBy")
    private int UpdateBy;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("UsedQty")
    private int UsedQty;
    @Expose
    @SerializedName("AssignedQty")
    private int AssignedQty;
    @Expose
    @SerializedName("AvlQty")
    private int AvlQty;
    @Expose
    @SerializedName("TaskId")
    private int TaskId;
    @Expose
    @SerializedName("ItemIssuedId")
    private int ItemIssuedId;
    @Expose
    @SerializedName("ItemId")
    private int ItemId;
    @Expose
    @SerializedName("Id")
    private int Id;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public int getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(int UpdateBy) {
        this.UpdateBy = UpdateBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getUsedQty() {
        return UsedQty;
    }

    public void setUsedQty(int UsedQty) {
        this.UsedQty = UsedQty;
    }

    public int getAssignedQty() {
        return AssignedQty;
    }

    public void setAssignedQty(int AssignedQty) {
        this.AssignedQty = AssignedQty;
    }

    public int getAvlQty() {
        return AvlQty;
    }

    public void setAvlQty(int AvlQty) {
        this.AvlQty = AvlQty;
    }

    public int getTaskId() {
        return TaskId;
    }

    public void setTaskId(int TaskId) {
        this.TaskId = TaskId;
    }

    public int getItemIssuedId() {
        return ItemIssuedId;
    }

    public void setItemIssuedId(int ItemIssuedId) {
        this.ItemIssuedId = ItemIssuedId;
    }

    public int getItemId() {
        return ItemId;
    }

    public void setItemId(int ItemId) {
        this.ItemId = ItemId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
}
