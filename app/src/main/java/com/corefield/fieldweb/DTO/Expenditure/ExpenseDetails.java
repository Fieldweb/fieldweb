package com.corefield.fieldweb.DTO.Expenditure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Expense detail of  Technician for particular day (Owner login)
 */
public class ExpenseDetails {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("ExpenseList")
        private List<ExpenseList> ExpenseList;
        @Expose
        @SerializedName("Expenses")
        private int Expenses;
        @Expose
        @SerializedName("EarnedAmount")
        private int EarnedAmount;
        @Expose
        @SerializedName("RemainingBalance")
        private int RemainingBalance;
        @Expose
        @SerializedName("ReturnAmount")
        private int ReturnAmount;
        @Expose
        @SerializedName("OpeningBalance")
        private int OpeningBalance;
        @Expose
        @SerializedName("CreditedAmonut")
        private int CreditedAmonut;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public List<ExpenseList> getExpenseList() {
            return ExpenseList;
        }

        public void setExpenseList(List<ExpenseList> ExpenseList) {
            this.ExpenseList = ExpenseList;
        }

        public int getExpenses() {
            return Expenses;
        }

        public void setExpenses(int Expenses) {
            this.Expenses = Expenses;
        }

        public int getEarnedAmount() {
            return EarnedAmount;
        }

        public void setEarnedAmount(int EarnedAmount) {
            this.EarnedAmount = EarnedAmount;
        }

        public int getRemainingBalance() {
            return RemainingBalance;
        }

        public void setRemainingBalance(int RemainingBalance) {
            this.RemainingBalance = RemainingBalance;
        }

        public int getReturnAmount() {
            return ReturnAmount;
        }

        public void setReturnAmount(int ReturnAmount) {
            this.ReturnAmount = ReturnAmount;
        }

        public int getOpeningBalance() {
            return OpeningBalance;
        }

        public void setOpeningBalance(int OpeningBalance) {
            this.OpeningBalance = OpeningBalance;
        }

        public int getCreditedAmonut() {
            return CreditedAmonut;
        }

        public void setCreditedAmonut(int CreditedAmonut) {
            this.CreditedAmonut = CreditedAmonut;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }
    }

    public static class ExpenseList {
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("ExpenseName")
        private String ExpenseName;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Amount")
        private int Amount;
        @Expose
        @SerializedName("ExpenseDate")
        private String ExpenseDate;
        @Expose
        @SerializedName("ExpensePhoto")
        private String ExpensePhoto;

        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }*/

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getExpenseName() {
            return ExpenseName;
        }

        public void setExpenseName(String ExpenseName) {
            this.ExpenseName = ExpenseName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public int getAmount() {
            return Amount;
        }

        public void setAmount(int Amount) {
            this.Amount = Amount;
        }

        public String getExpenseDate() {
            return ExpenseDate;
        }

        public void setExpenseDate(String ExpenseDate) {
            this.ExpenseDate = ExpenseDate;
        }

        public String getExpensePhoto() {
            return ExpensePhoto;
        }

        public void setExpensePhoto(String expensePhoto) {
            ExpensePhoto = expensePhoto;
        }
    }
}
