package com.corefield.fieldweb.DTO.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadInvoicePdfDTO {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("InvoicePdfPath")
        private String InvoicePdfPath;
        @Expose
        @SerializedName("InvoiceId")
        private int InvoiceId;

        public String getInvoicePdfPath() {
            return InvoicePdfPath;
        }

        public void setInvoicePdfPath(String InvoicePdfPath) {
            this.InvoicePdfPath = InvoicePdfPath;
        }

        public int getInvoiceId() {
            return InvoiceId;
        }

        public void setInvoiceId(int InvoiceId) {
            this.InvoiceId = InvoiceId;
        }
    }
}
