package com.corefield.fieldweb.DTO.TeleCMI;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeleCMICall {

    @SerializedName("appid")
    @Expose
    private Integer appid;
    @SerializedName("secret")
    @Expose
    private String secret;
    @SerializedName("from")
    @Expose
    private Long from;
    @SerializedName("to")
    @Expose
    private Long to;
    @SerializedName("pcmo")
    @Expose
    private List<Pcmo> pcmo = null;

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public List<Pcmo> getPcmo() {
        return pcmo;
    }

    public void setPcmo(List<Pcmo> pcmo) {
        this.pcmo = pcmo;
    }

}





