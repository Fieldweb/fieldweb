package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPhotoBeforeTask {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("DeviceInfoImagePath2")
        private String DeviceInfoImagePath2;
        @Expose
        @SerializedName("DeviceInfoImagePath1")
        private String DeviceInfoImagePath1;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("DeviceInfoNotes")
        private String DeviceInfoNotes;
        @Expose
        @SerializedName("DeviceInfoImagePath")
        private String DeviceInfoImagePath;
        @Expose
        @SerializedName("DeviceInfoImageName")
        private String DeviceInfoImageName;
        @Expose
        @SerializedName("PreDeviceInfoId")
        private int PreDeviceInfoId;

        public String getDeviceInfoImagePath2() {
            return DeviceInfoImagePath2;
        }

        public void setDeviceInfoImagePath2(String DeviceInfoImagePath2) {
            this.DeviceInfoImagePath2 = DeviceInfoImagePath2;
        }

        public String getDeviceInfoImagePath1() {
            return DeviceInfoImagePath1;
        }

        public void setDeviceInfoImagePath1(String DeviceInfoImagePath1) {
            this.DeviceInfoImagePath1 = DeviceInfoImagePath1;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public String getDeviceInfoNotes() {
            return DeviceInfoNotes;
        }

        public void setDeviceInfoNotes(String DeviceInfoNotes) {
            this.DeviceInfoNotes = DeviceInfoNotes;
        }

        public String getDeviceInfoImagePath() {
            return DeviceInfoImagePath;
        }

        public void setDeviceInfoImagePath(String DeviceInfoImagePath) {
            this.DeviceInfoImagePath = DeviceInfoImagePath;
        }

        public String getDeviceInfoImageName() {
            return DeviceInfoImageName;
        }

        public void setDeviceInfoImageName(String DeviceInfoImageName) {
            this.DeviceInfoImageName = DeviceInfoImageName;
        }

        public int getPreDeviceInfoId() {
            return PreDeviceInfoId;
        }

        public void setPreDeviceInfoId(int PreDeviceInfoId) {
            this.PreDeviceInfoId = PreDeviceInfoId;
        }
    }
}
