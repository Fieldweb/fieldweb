package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get user/tech list (owner login)
 */
public class UsersList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("AttendanceId")
        private int AttendanceId;
        @Expose
        @SerializedName("Attendance")
        private String Attendance;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("MiddleName")
        private String MiddleName;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("Role")
        private String Role;
        @Expose
        @SerializedName("RoleId")
        private String RoleId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("Temperature")
        private float Temperature;
        @Expose
        @SerializedName("TemperatureUnitTypeId")
        private int TemperatureUnitTypeId;
        @Expose
        @SerializedName("TemperatureUnitType")
        private String TemperatureUnitType;
        @Expose
        @SerializedName("AarogyaSetuStatusId")
        private int AarogyaSetuStatusId;
        @Expose
        @SerializedName("ArogyaSetuStatus")
        private String ArogyaSetuStatus;
        @Expose
        @SerializedName("IsHealthFeaturesEnabled")
        private boolean IsHealthFeaturesEnabled;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("DOB")
        private String DOB;
        @Expose
        @SerializedName("GPSOnOrOff")
        private boolean GPSOnOrOff;
        @Expose
        @SerializedName("BatteryPercentage")
        private int BatteryPercentage;
        @Expose
        @SerializedName("AadharCardNo")
        private String AadharCardNo;
        @Expose
        @SerializedName("CompanyServiceTypeId")
        private int CompanyServiceTypeId;
        @Expose
        @SerializedName("NoOfUsers")
        private int NoOfUsers;

        public String getAadharCardNo() {
            return AadharCardNo;
        }

        public void setAadharCardNo(String aadharCardNo) {
            AadharCardNo = aadharCardNo;
        }

        public int getCompanyServiceTypeId() {
            return CompanyServiceTypeId;
        }

        public void setCompanyServiceTypeId(int companyServiceTypeId) {
            CompanyServiceTypeId = companyServiceTypeId;
        }

        public int getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(int noOfUsers) {
            NoOfUsers = noOfUsers;
        }


        public boolean isGPSOnOrOff() {
            return GPSOnOrOff;
        }

        public void setGPSOnOrOff(boolean GPSOnOrOff) {
            this.GPSOnOrOff = GPSOnOrOff;
        }

        public int getBatteryPercentage() {
            return BatteryPercentage;
        }

        public void setBatteryPercentage(int batteryPercentage) {
            BatteryPercentage = batteryPercentage;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public boolean isHealthFeaturesEnabled() {
            return IsHealthFeaturesEnabled;
        }

        public void setHealthFeaturesEnabled(boolean healthFeaturesEnabled) {
            IsHealthFeaturesEnabled = healthFeaturesEnabled;
        }

        public float getTemperature() {
            return Temperature;
        }

        public void setTemperature(float temperature) {
            Temperature = temperature;
        }

        public int getTemperatureUnitTypeId() {
            return TemperatureUnitTypeId;
        }

        public void setTemperatureUnitTypeId(int temperatureUnitTypeId) {
            TemperatureUnitTypeId = temperatureUnitTypeId;
        }

        public String getTemperatureUnitType() {
            return TemperatureUnitType;
        }

        public void setTemperatureUnitType(String temperatureUnitType) {
            TemperatureUnitType = temperatureUnitType;
        }

        public int getAarogyaSetuStatusId() {
            return AarogyaSetuStatusId;
        }

        public void setAarogyaSetuStatusId(int aarogyaSetuStatusId) {
            AarogyaSetuStatusId = aarogyaSetuStatusId;
        }

        public String getArogyaSetuStatus() {
            return ArogyaSetuStatus;
        }

        public void setArogyaSetuStatus(String arogyaSetuStatus) {
            ArogyaSetuStatus = arogyaSetuStatus;
        }

        public int getAttendanceId() {
            return AttendanceId;
        }

        public void setAttendanceId(int AttendanceId) {
            this.AttendanceId = AttendanceId;
        }

        public String getAttendance() {
            return Attendance;
        }

        public void setAttendance(String Attendance) {
            this.Attendance = Attendance;
        }

        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }*/

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getMiddleName() {
            return MiddleName;
        }

        public void setMiddleName(String MiddleName) {
            this.MiddleName = MiddleName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getRole() {
            return Role;
        }

        public void setRole(String Role) {
            this.Role = Role;
        }

        public String getRoleId() {
            return RoleId;
        }

        public void setRoleId(String RoleId) {
            this.RoleId = RoleId;
        }

        /*public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }*/

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }
    }
}
