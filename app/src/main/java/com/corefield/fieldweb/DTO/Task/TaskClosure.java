package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * //
 * Created by CFS on 6/2/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Task
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This DTO is used to parse the Task Closure.
 * //
 **/
public class TaskClosure {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("MultipleItemAssigned")
        private List<MultipleItemAssigned> MultipleItemAssigned;
        @Expose
        @SerializedName("UsedItemDetailsDto")
        private List<UsedItemDetailsDto> UsedItemDetailsDto;
        @Expose
        @SerializedName("FieldPhoto2")
        private String FieldPhoto2;
        @Expose
        @SerializedName("FieldPhoto1")
        private String FieldPhoto1;
        @Expose
        @SerializedName("PreDeviceInfoDto")
        private PreDeviceInfoDto PreDeviceInfoDto;
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("TaskState")
        private int TaskState;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private String CreatedBy;
        @Expose
        @SerializedName("TechnicalNotedto")
        private List<TechnicalNotedto> TechnicalNotedto;
        @Expose
        @SerializedName("DeviceInfoList")
        private List<DeviceInfoList> DeviceInfoList;
        @Expose
        @SerializedName("RatingBar")
        private float RatingBar;
        @Expose
        @SerializedName("WorkModeType")
        private String WorkModeType;
        @Expose
        @SerializedName("MobileNo")
        private long MobileNo;
        @Expose
        @SerializedName("RatingBarId")
        private int RatingBarId;
        @Expose
        @SerializedName("WorkModeId")
        private int WorkModeId;
        @Expose
        @SerializedName("FieldPhoto")
        private String FieldPhoto;
        @Expose
        @SerializedName("CustomerSignatureImage")
        private String CustomerSignatureImage;
        @Expose
        @SerializedName("SignedBy")
        private String SignedBy;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public List<MultipleItemAssigned> getMultipleItemAssigned() {
            return MultipleItemAssigned;
        }

        public void setMultipleItemAssigned(List<MultipleItemAssigned> MultipleItemAssigned) {
            this.MultipleItemAssigned = MultipleItemAssigned;
        }

        public List<UsedItemDetailsDto> getUsedItemDetailsDto() {
            return UsedItemDetailsDto;
        }

        public void setUsedItemDetailsDto(List<UsedItemDetailsDto> UsedItemDetailsDto) {
            this.UsedItemDetailsDto = UsedItemDetailsDto;
        }

        public String getFieldPhoto2() {
            return FieldPhoto2;
        }

        public void setFieldPhoto2(String FieldPhoto2) {
            this.FieldPhoto2 = FieldPhoto2;
        }

        public String getFieldPhoto1() {
            return FieldPhoto1;
        }

        public void setFieldPhoto1(String FieldPhoto1) {
            this.FieldPhoto1 = FieldPhoto1;
        }

        public PreDeviceInfoDto getPreDeviceInfoDto() {
            return PreDeviceInfoDto;
        }

        public void setPreDeviceInfoDto(PreDeviceInfoDto PreDeviceInfoDto) {
            this.PreDeviceInfoDto = PreDeviceInfoDto;
        }

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public int getTaskState() {
            return TaskState;
        }

        public void setTaskState(int TaskState) {
            this.TaskState = TaskState;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public List<TechnicalNotedto> getTechnicalNotedto() {
            return TechnicalNotedto;
        }

        public void setTechnicalNotedto(List<TechnicalNotedto> TechnicalNotedto) {
            this.TechnicalNotedto = TechnicalNotedto;
        }

        public List<DeviceInfoList> getDeviceInfoList() {
            return DeviceInfoList;
        }

        public void setDeviceInfoList(List<DeviceInfoList> DeviceInfoList) {
            this.DeviceInfoList = DeviceInfoList;
        }

        public float getRatingBar() {
            return RatingBar;
        }

        public void setRatingBar(float RatingBar) {
            this.RatingBar = RatingBar;
        }

        public String getWorkModeType() {
            return WorkModeType;
        }

        public void setWorkModeType(String WorkModeType) {
            this.WorkModeType = WorkModeType;
        }

        public long getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(long MobileNo) {
            this.MobileNo = MobileNo;
        }

        public int getRatingBarId() {
            return RatingBarId;
        }

        public void setRatingBarId(int RatingBarId) {
            this.RatingBarId = RatingBarId;
        }

        public int getWorkModeId() {
            return WorkModeId;
        }

        public void setWorkModeId(int WorkModeId) {
            this.WorkModeId = WorkModeId;
        }

        public String getFieldPhoto() {
            return FieldPhoto;
        }

        public void setFieldPhoto(String FieldPhoto) {
            this.FieldPhoto = FieldPhoto;
        }

        public String getCustomerSignatureImage() {
            return CustomerSignatureImage;
        }

        public void setCustomerSignatureImage(String CustomerSignatureImage) {
            this.CustomerSignatureImage = CustomerSignatureImage;
        }

        public String getSignedBy() {
            return SignedBy;
        }

        public void setSignedBy(String SignedBy) {
            this.SignedBy = SignedBy;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class MultipleItemAssigned {
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemQuantity")
        private int ItemQuantity;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("UsedItemQty")
        private int UsedItemQty;

        public int getUsedItemQty() {
            return UsedItemQty;
        }

        public void setUsedItemQty(int usedItemQty) {
            UsedItemQty = usedItemQty;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int itemIssuedId) {
            ItemIssuedId = itemIssuedId;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemQuantity() {
            return ItemQuantity;
        }

        public void setItemQuantity(int ItemQuantity) {
            this.ItemQuantity = ItemQuantity;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class UsedItemDetailsDto implements Serializable {
        @Expose
        @SerializedName("Notes")
        private String Notes;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdateBy")
        private int UpdateBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("UsedQty")
        private int UsedQty;
        @Expose
        @SerializedName("AssignedQty")
        private int AssignedQty;
        @Expose
        @SerializedName("AvlQty")
        private int AvlQty;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNotes() {
            return Notes;
        }

        public void setNotes(String Notes) {
            this.Notes = Notes;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdateBy() {
            return UpdateBy;
        }

        public void setUpdateBy(int UpdateBy) {
            this.UpdateBy = UpdateBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getUsedQty() {
            return UsedQty;
        }

        public void setUsedQty(int UsedQty) {
            this.UsedQty = UsedQty;
        }

        public int getAssignedQty() {
            return AssignedQty;
        }

        public void setAssignedQty(int AssignedQty) {
            this.AssignedQty = AssignedQty;
        }

        public int getAvlQty() {
            return AvlQty;
        }

        public void setAvlQty(int AvlQty) {
            this.AvlQty = AvlQty;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int ItemIssuedId) {
            this.ItemIssuedId = ItemIssuedId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class PreDeviceInfoDto implements Serializable {
        @Expose
        @SerializedName("DeviceInfoImagePath2")
        private String DeviceInfoImagePath2;
        @Expose
        @SerializedName("DeviceInfoImagePath1")
        private String DeviceInfoImagePath1;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("DeviceInfoNotes")
        private String DeviceInfoNotes;
        @Expose
        @SerializedName("DeviceInfoImagePath")
        private String DeviceInfoImagePath;
        @Expose
        @SerializedName("DeviceInfoImageName")
        private String DeviceInfoImageName;
        @Expose
        @SerializedName("PreDeviceInfoId")
        private int PreDeviceInfoId;

        public String getDeviceInfoImagePath2() {
            return DeviceInfoImagePath2;
        }

        public void setDeviceInfoImagePath2(String DeviceInfoImagePath2) {
            this.DeviceInfoImagePath2 = DeviceInfoImagePath2;
        }

        public String getDeviceInfoImagePath1() {
            return DeviceInfoImagePath1;
        }

        public void setDeviceInfoImagePath1(String DeviceInfoImagePath1) {
            this.DeviceInfoImagePath1 = DeviceInfoImagePath1;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public String getDeviceInfoNotes() {
            return DeviceInfoNotes;
        }

        public void setDeviceInfoNotes(String DeviceInfoNotes) {
            this.DeviceInfoNotes = DeviceInfoNotes;
        }

        public String getDeviceInfoImagePath() {
            return DeviceInfoImagePath;
        }

        public void setDeviceInfoImagePath(String DeviceInfoImagePath) {
            this.DeviceInfoImagePath = DeviceInfoImagePath;
        }

        public String getDeviceInfoImageName() {
            return DeviceInfoImageName;
        }

        public void setDeviceInfoImageName(String DeviceInfoImageName) {
            this.DeviceInfoImageName = DeviceInfoImageName;
        }

        public int getPreDeviceInfoId() {
            return PreDeviceInfoId;
        }

        public void setPreDeviceInfoId(int PreDeviceInfoId) {
            this.PreDeviceInfoId = PreDeviceInfoId;
        }
    }

    public static class TechnicalNotedto {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private String CreatedBy;
        @Expose
        @SerializedName("UserId")
        private String UserId;
        @Expose
        @SerializedName("TaskClosureDetailsId")
        private int TaskClosureDetailsId;
        @Expose
        @SerializedName("TechnicalNote1")
        private String TechnicalNote1;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public int getTaskClosureDetailsId() {
            return TaskClosureDetailsId;
        }

        public void setTaskClosureDetailsId(int TaskClosureDetailsId) {
            this.TaskClosureDetailsId = TaskClosureDetailsId;
        }

        public String getTechnicalNote1() {
            return TechnicalNote1;
        }

        public void setTechnicalNote1(String TechnicalNote1) {
            this.TechnicalNote1 = TechnicalNote1;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class DeviceInfoList {
        @Expose
        @SerializedName("DeviceReading")
        private double DeviceReading;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private String CreatedBy;
        @Expose
        @SerializedName("UserId")
        private String UserId;
        @Expose
        @SerializedName("TaskClosureDetailsId")
        private int TaskClosureDetailsId;
        @Expose
        @SerializedName("DevicePhoto3")
        private String DevicePhoto3;
        @Expose
        @SerializedName("DevicePhoto2")
        private String DevicePhoto2;
        @Expose
        @SerializedName("DevicePhoto1")
        private String DevicePhoto1;
        @Expose
        @SerializedName("ModelNumber")
        private String ModelNumber;
        @Expose
        @SerializedName("DeviceName")
        private String DeviceName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public double getDeviceReading() {
            return DeviceReading;
        }

        public void setDeviceReading(double DeviceReading) {
            this.DeviceReading = DeviceReading;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }

        public int getTaskClosureDetailsId() {
            return TaskClosureDetailsId;
        }

        public void setTaskClosureDetailsId(int TaskClosureDetailsId) {
            this.TaskClosureDetailsId = TaskClosureDetailsId;
        }

        public String getDevicePhoto3() {
            return DevicePhoto3;
        }

        public void setDevicePhoto3(String DevicePhoto3) {
            this.DevicePhoto3 = DevicePhoto3;
        }

        public String getDevicePhoto2() {
            return DevicePhoto2;
        }

        public void setDevicePhoto2(String DevicePhoto2) {
            this.DevicePhoto2 = DevicePhoto2;
        }

        public String getDevicePhoto1() {
            return DevicePhoto1;
        }

        public void setDevicePhoto1(String DevicePhoto1) {
            this.DevicePhoto1 = DevicePhoto1;
        }

        public String getModelNumber() {
            return ModelNumber;
        }

        public void setModelNumber(String ModelNumber) {
            this.ModelNumber = ModelNumber;
        }

        public String getDeviceName() {
            return DeviceName;
        }

        public void setDeviceName(String DeviceName) {
            this.DeviceName = DeviceName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
