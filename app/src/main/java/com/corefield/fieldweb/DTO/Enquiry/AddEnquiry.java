package com.corefield.fieldweb.DTO.Enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 11/5/2019.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Enquiry
 * Version : 1.0
 * Copyright (c) 2019 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class AddEnquiry {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("TaskTypeName")
        private String TaskTypeName;
        @Expose
        @SerializedName("ReferenceName")
        private String ReferenceName;
        @Expose
        @SerializedName("ServiceName")
        private String ServiceName;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("LocDescription")
        private String LocDescription;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("LocName")
        private String LocName;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("InquaryState")
        private int InquaryState;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("ReferenceId")
        private int ReferenceId;
        @Expose
        @SerializedName("TechnicalNote")
        private String TechnicalNote;
        @Expose
        @SerializedName("TechnicalProblem")
        private String TechnicalProblem;
        @Expose
        @SerializedName("ServicesId")
        private int ServicesId;
        @Expose
        @SerializedName("PreferableDate")
        private String PreferableDate;
        @Expose
        @SerializedName("PreferableTime")
        private String PreferableTime;
        @Expose
        @SerializedName("TaskTypeId")
        private int TaskTypeId;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public String getPreferableTime() {
            return PreferableTime;
        }

        public void setPreferableTime(String preferableTime) {
            PreferableTime = preferableTime;
        }

        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EnquiryId")
        private int EnquiryId;

        public String getTaskTypeName() {
            return TaskTypeName;
        }

        public void setTaskTypeName(String TaskTypeName) {
            this.TaskTypeName = TaskTypeName;
        }

        public String getReferenceName() {
            return ReferenceName;
        }

        public void setReferenceName(String ReferenceName) {
            this.ReferenceName = ReferenceName;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        /*public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String OwnerId) {
            this.OwnerId = OwnerId;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int ownerId) {
            OwnerId = ownerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLocDescription() {
            return LocDescription;
        }

        public void setLocDescription(String LocDescription) {
            this.LocDescription = LocDescription;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLocName() {
            return LocName;
        }

        public void setLocName(String LocName) {
            this.LocName = LocName;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getInquaryState() {
            return InquaryState;
        }

        public void setInquaryState(int InquaryState) {
            this.InquaryState = InquaryState;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getReferenceId() {
            return ReferenceId;
        }

        public void setReferenceId(int ReferenceId) {
            this.ReferenceId = ReferenceId;
        }

        public String getTechnicalNote() {
            return TechnicalNote;
        }

        public void setTechnicalNote(String TechnicalNote) {
            this.TechnicalNote = TechnicalNote;
        }

        public String getTechnicalProblem() {
            return TechnicalProblem;
        }

        public void setTechnicalProblem(String TechnicalProblem) {
            this.TechnicalProblem = TechnicalProblem;
        }

        public int getServicesId() {
            return ServicesId;
        }

        public void setServicesId(int ServicesId) {
            this.ServicesId = ServicesId;
        }

        public String getPreferableDate() {
            return PreferableDate;
        }

        public void setPreferableDate(String PreferableDate) {
            this.PreferableDate = PreferableDate;
        }

        public int getTaskTypeId() {
            return TaskTypeId;
        }

        public void setTaskTypeId(int TaskTypeId) {
            this.TaskTypeId = TaskTypeId;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public int getEnquiryId() {
            return EnquiryId;
        }

        public void setEnquiryId(int EnquiryId) {
            this.EnquiryId = EnquiryId;
        }
    }
}
