
package com.corefield.fieldweb.DTO.TeleCMI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pcmo {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("timeout")
    @Expose
    private Integer timeout;
    @SerializedName("from")
    @Expose
    private Long from;
    @SerializedName("loop")
    @Expose
    private Integer loop;
    @SerializedName("connect")
    @Expose
    private List<Connect> connect = null;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Integer getLoop() {
        return loop;
    }

    public void setLoop(Integer loop) {
        this.loop = loop;
    }

    public List<Connect> getConnect() {
        return connect;
    }

    public void setConnect(List<Connect> connect) {
        this.connect = connect;
    }

}