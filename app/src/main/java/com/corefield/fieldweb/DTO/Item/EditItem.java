package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 2/12/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Item
 * Version : 1.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class EditItem {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("ImageFileBase64Str")
        private String ImageFileBase64Str;
        @Expose
        @SerializedName("ImageFileName")
        private String ImageFileName;
        @Expose
        @SerializedName("ItemImagePath")
        private String ItemImagePath;
        @Expose
        @SerializedName("PurchasePrice")
        private int PurchasePrice;
        @Expose
        @SerializedName("SalesPrice")
        private int SalesPrice;
        @Expose
        @SerializedName("ItemUnitTypeId")
        private int ItemUnitTypeId;
        @Expose
        @SerializedName("ItemType")
        private int ItemType;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getImageFileBase64Str() {
            return ImageFileBase64Str;
        }

        public void setImageFileBase64Str(String ImageFileBase64Str) {
            this.ImageFileBase64Str = ImageFileBase64Str;
        }

        public String getImageFileName() {
            return ImageFileName;
        }

        public void setImageFileName(String ImageFileName) {
            this.ImageFileName = ImageFileName;
        }

        public String getItemImagePath() {
            return ItemImagePath;
        }

        public void setItemImagePath(String ItemImagePath) {
            this.ItemImagePath = ItemImagePath;
        }

        public int getPurchasePrice() {
            return PurchasePrice;
        }

        public void setPurchasePrice(int PurchasePrice) {
            this.PurchasePrice = PurchasePrice;
        }

        public int getSalesPrice() {
            return SalesPrice;
        }

        public void setSalesPrice(int SalesPrice) {
            this.SalesPrice = SalesPrice;
        }

        public int getItemUnitTypeId() {
            return ItemUnitTypeId;
        }

        public void setItemUnitTypeId(int ItemUnitTypeId) {
            this.ItemUnitTypeId = ItemUnitTypeId;
        }

        public int getItemType() {
            return ItemType;
        }

        public void setItemType(int ItemType) {
            this.ItemType = ItemType;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
