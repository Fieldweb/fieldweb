package com.corefield.fieldweb.DTO.HealthAndSaftey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 5/21/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.HealthAndSaftey
 * Version : 1.2.0
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment : This DTO is used to parse the health and safety status update
 * //
 **/
public class UpdateHealthAndSafetyStatus {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AarogyaSetuStatusId")
        private int AarogyaSetuStatusId;
        @Expose
        @SerializedName("TemperatureUnitTypeId")
        private int TemperatureUnitTypeId;
        @Expose
        @SerializedName("Temperature")
        private float Temperature;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getAarogyaSetuStatusId() {
            return AarogyaSetuStatusId;
        }

        public void setAarogyaSetuStatusId(int AarogyaSetuStatusId) {
            this.AarogyaSetuStatusId = AarogyaSetuStatusId;
        }

        public int getTemperatureUnitTypeId() {
            return TemperatureUnitTypeId;
        }

        public void setTemperatureUnitTypeId(int TemperatureUnitTypeId) {
            this.TemperatureUnitTypeId = TemperatureUnitTypeId;
        }

        public float getTemperature() {
            return Temperature;
        }

        public void setTemperature(float Temperature) {
            this.Temperature = Temperature;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
