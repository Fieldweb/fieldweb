package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AMCDetails {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("AMCServiceDetailDto")
        private List<AMCServiceDetailDto> AMCServiceDetailDto;
        @Expose
        @SerializedName("TaskDetails")
        private List<TaskDetails> TaskDetails;
        @Expose
        @SerializedName("ProductDetail")
        private ProductDetail ProductDetail;
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;
        @Expose
        @SerializedName("TotalServices")
        private int TotalServices;
        @Expose
        @SerializedName("AMCNotes")
        private String AMCNotes;
        @Expose
        @SerializedName("AMCAmount")
        private int AMCAmount;
        @Expose
        @SerializedName("ActivationTime")
        private String ActivationTime;
        @Expose
        @SerializedName("ActivationDate")
        private String ActivationDate;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;
        @Expose
        @SerializedName("AMCName")
        private String AMCName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AMCsId")
        private int AMCsId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("IsUpcomingServiceActive")
        private boolean IsUpcomingServiceActive;
        @Expose
        @SerializedName("UpcomingAmcsServiceDate")
        private String UpcomingAmcsServiceDate;
        @Expose
        @SerializedName("UpcomingAmcsServiceTime")
        private String UpcomingAmcsServiceTime;

        public boolean isUpcomingServiceActive() {
            return IsUpcomingServiceActive;
        }

        public void setUpcomingServiceActive(boolean upcomingServiceActive) {
            IsUpcomingServiceActive = upcomingServiceActive;
        }

        public String getUpcomingAmcsServiceDate() {
            return UpcomingAmcsServiceDate;
        }

        public void setUpcomingAmcsServiceDate(String upcomingAmcsServiceDate) {
            UpcomingAmcsServiceDate = upcomingAmcsServiceDate;
        }

        public String getUpcomingAmcsServiceTime() {
            return UpcomingAmcsServiceTime;
        }

        public void setUpcomingAmcsServiceTime(String upcomingAmcsServiceTime) {
            UpcomingAmcsServiceTime = upcomingAmcsServiceTime;
        }

        public boolean isActive() {
            return IsActive;
        }

        public void setActive(boolean active) {
            IsActive = active;
        }

        public List<AMCServiceDetailDto> getAMCServiceDetailDto() {
            return AMCServiceDetailDto;
        }

        public void setAMCServiceDetailDto(List<AMCServiceDetailDto> AMCServiceDetailDto) {
            this.AMCServiceDetailDto = AMCServiceDetailDto;
        }

        public List<TaskDetails> getTaskDetails() {
            return TaskDetails;
        }

        public void setTaskDetails(List<TaskDetails> TaskDetails) {
            this.TaskDetails = TaskDetails;
        }

        public ProductDetail getProductDetail() {
            return ProductDetail;
        }

        public void setProductDetail(ProductDetail ProductDetail) {
            this.ProductDetail = ProductDetail;
        }

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }

        public int getTotalServices() {
            return TotalServices;
        }

        public void setTotalServices(int TotalServices) {
            this.TotalServices = TotalServices;
        }

        public String getAMCNotes() {
            return AMCNotes;
        }

        public void setAMCNotes(String AMCNotes) {
            this.AMCNotes = AMCNotes;
        }

        public int getAMCAmount() {
            return AMCAmount;
        }

        public void setAMCAmount(int AMCAmount) {
            this.AMCAmount = AMCAmount;
        }

        public String getActivationTime() {
            return ActivationTime;
        }

        public void setActivationTime(String ActivationTime) {
            this.ActivationTime = ActivationTime;
        }

        public String getActivationDate() {
            return ActivationDate;
        }

        public void setActivationDate(String ActivationDate) {
            this.ActivationDate = ActivationDate;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }

        public String getAMCName() {
            return AMCName;
        }

        public void setAMCName(String AMCName) {
            this.AMCName = AMCName;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getAMCsId() {
            return AMCsId;
        }

        public void setAMCsId(int AMCsId) {
            this.AMCsId = AMCsId;
        }
    }

    public static class AMCServiceDetailDto {
        @Expose
        @SerializedName("TaskDetails")
        private TaskDetails TaskDetails;
        @Expose
        @SerializedName("AMCTypeName")
        private String AMCTypeName;
        @Expose
        @SerializedName("ActualAMCSeriveDate")
        private String ActualAMCSeriveDate;
        @Expose
        @SerializedName("HoursNo")
        private int HoursNo;
        @Expose
        @SerializedName("DaysNo")
        private int DaysNo;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;
        @Expose
        @SerializedName("TotalServices")
        private int TotalServices;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("ActivationTime")
        private String ActivationTime;
        @Expose
        @SerializedName("ActivationDate")
        private String ActivationDate;
        @Expose
        @SerializedName("AMCName")
        private String AMCName;
        @Expose
        @SerializedName("AMCTypeId")
        private int AMCTypeId;
        @Expose
        @SerializedName("ServiceNo")
        private int ServiceNo;
        @Expose
        @SerializedName("AMCServiceDate")
        private String AMCServiceDate;
        @Expose
        @SerializedName("AMCsId")
        private int AMCsId;
        @Expose
        @SerializedName("AMCServiceDetailsId")
        private int AMCServiceDetailsId;

        public TaskDetails getTaskDetails() {
            return TaskDetails;
        }

        public void setTaskDetails(TaskDetails TaskDetails) {
            this.TaskDetails = TaskDetails;
        }

        public String getAMCTypeName() {
            return AMCTypeName;
        }

        public void setAMCTypeName(String AMCTypeName) {
            this.AMCTypeName = AMCTypeName;
        }

        public String getActualAMCSeriveDate() {
            return ActualAMCSeriveDate;
        }

        public void setActualAMCSeriveDate(String ActualAMCSeriveDate) {
            this.ActualAMCSeriveDate = ActualAMCSeriveDate;
        }

        public int getHoursNo() {
            return HoursNo;
        }

        public void setHoursNo(int HoursNo) {
            this.HoursNo = HoursNo;
        }

        public int getDaysNo() {
            return DaysNo;
        }

        public void setDaysNo(int DaysNo) {
            this.DaysNo = DaysNo;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }

        public int getTotalServices() {
            return TotalServices;
        }

        public void setTotalServices(int TotalServices) {
            this.TotalServices = TotalServices;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getActivationTime() {
            return ActivationTime;
        }

        public void setActivationTime(String ActivationTime) {
            this.ActivationTime = ActivationTime;
        }

        public String getActivationDate() {
            return ActivationDate;
        }

        public void setActivationDate(String ActivationDate) {
            this.ActivationDate = ActivationDate;
        }

        public String getAMCName() {
            return AMCName;
        }

        public void setAMCName(String AMCName) {
            this.AMCName = AMCName;
        }

        public int getAMCTypeId() {
            return AMCTypeId;
        }

        public void setAMCTypeId(int AMCTypeId) {
            this.AMCTypeId = AMCTypeId;
        }

        public int getServiceNo() {
            return ServiceNo;
        }

        public void setServiceNo(int ServiceNo) {
            this.ServiceNo = ServiceNo;
        }

        public String getAMCServiceDate() {
            return AMCServiceDate;
        }

        public void setAMCServiceDate(String AMCServiceDate) {
            this.AMCServiceDate = AMCServiceDate;
        }

        public int getAMCsId() {
            return AMCsId;
        }

        public void setAMCsId(int AMCsId) {
            this.AMCsId = AMCsId;
        }

        public int getAMCServiceDetailsId() {
            return AMCServiceDetailsId;
        }

        public void setAMCServiceDetailsId(int AMCServiceDetailsId) {
            this.AMCServiceDetailsId = AMCServiceDetailsId;
        }
    }

    public static class TaskDetails {
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("TechnicianUserId")
        private String TechnicianUserId;
        @Expose
        @SerializedName("TechnicianName")
        private String TechnicianName;
        @Expose
        @SerializedName("TaskName")
        private String TaskName;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getTechnicianUserId() {
            return TechnicianUserId;
        }

        public void setTechnicianUserId(String TechnicianUserId) {
            this.TechnicianUserId = TechnicianUserId;
        }

        public String getTechnicianName() {
            return TechnicianName;
        }

        public void setTechnicianName(String TechnicianName) {
            this.TechnicianName = TechnicianName;
        }

        public String getTaskName() {
            return TaskName;
        }

        public void setTaskName(String TaskName) {
            this.TaskName = TaskName;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }
    }

    public static class ProductDetail {
        @Expose
        @SerializedName("CustomerLocationInfoDto")
        private CustomerLocationInfoDto CustomerLocationInfoDto;
        @Expose
        @SerializedName("CustomerDetailInfoDto")
        private CustomerDetailInfoDto CustomerDetailInfoDto;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UnderWarranty")
        private boolean UnderWarranty;
        @Expose
        @SerializedName("CustomerId")
        private int CustomerId;
        @Expose
        @SerializedName("CustomerLocationId")
        private int CustomerLocationId;
        @Expose
        @SerializedName("ProductBrand")
        private String ProductBrand;
        @Expose
        @SerializedName("ProductSerialNo")
        private String ProductSerialNo;
        @Expose
        @SerializedName("ProductName")
        private String ProductName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("ProductDetailsId")
        private int ProductDetailsId;

        public CustomerLocationInfoDto getCustomerLocationInfoDto() {
            return CustomerLocationInfoDto;
        }

        public void setCustomerLocationInfoDto(CustomerLocationInfoDto CustomerLocationInfoDto) {
            this.CustomerLocationInfoDto = CustomerLocationInfoDto;
        }

        public CustomerDetailInfoDto getCustomerDetailInfoDto() {
            return CustomerDetailInfoDto;
        }

        public void setCustomerDetailInfoDto(CustomerDetailInfoDto CustomerDetailInfoDto) {
            this.CustomerDetailInfoDto = CustomerDetailInfoDto;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getUnderWarranty() {
            return UnderWarranty;
        }

        public void setUnderWarranty(boolean UnderWarranty) {
            this.UnderWarranty = UnderWarranty;
        }

        public int getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(int CustomerId) {
            this.CustomerId = CustomerId;
        }

        public int getCustomerLocationId() {
            return CustomerLocationId;
        }

        public void setCustomerLocationId(int CustomerLocationId) {
            this.CustomerLocationId = CustomerLocationId;
        }

        public String getProductBrand() {
            return ProductBrand;
        }

        public void setProductBrand(String ProductBrand) {
            this.ProductBrand = ProductBrand;
        }

        public String getProductSerialNo() {
            return ProductSerialNo;
        }

        public void setProductSerialNo(String ProductSerialNo) {
            this.ProductSerialNo = ProductSerialNo;
        }

        public String getProductName() {
            return ProductName;
        }

        public void setProductName(String ProductName) {
            this.ProductName = ProductName;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getProductDetailsId() {
            return ProductDetailsId;
        }

        public void setProductDetailsId(int ProductDetailsId) {
            this.ProductDetailsId = ProductDetailsId;
        }
    }

    public static class CustomerLocationInfoDto {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class CustomerDetailInfoDto {
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }
    }
}
