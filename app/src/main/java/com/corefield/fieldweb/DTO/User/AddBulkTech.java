package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to add Technician (Owner login)
 */
public class AddBulkTech {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("IsMobileNo")
        private boolean IsMobileNo;
        @Expose
        @SerializedName("IsUserId")
        private boolean IsUserId;
        @Expose
        @SerializedName("TempTechSrNo")
        private int TempTechSrNo;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;
        @Expose
        @SerializedName("File")
        private String File;
        @Expose
        @SerializedName("FileName")
        private String FileName;
        @Expose
        @SerializedName("RoleGroup")
        private String RoleGroup;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("ConfirmPassword")
        private String ConfirmPassword;
        @Expose
        @SerializedName("Password")
        private String Password;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("UserCountryCode")
        private int UserCountryCode;

        public int getUserCountryCode() {
            return UserCountryCode;
        }

        public void setUserCountryCode(int userCountryCode) {
            UserCountryCode = userCountryCode;
        }

        public boolean getIsMobileNo() {
            return IsMobileNo;
        }

        public void setIsMobileNo(boolean IsMobileNo) {
            this.IsMobileNo = IsMobileNo;
        }

        public boolean getIsUserId() {
            return IsUserId;
        }

        public void setIsUserId(boolean IsUserId) {
            this.IsUserId = IsUserId;
        }

        public int getTempTechSrNo() {
            return TempTechSrNo;
        }

        public void setTempTechSrNo(int TempTechSrNo) {
            this.TempTechSrNo = TempTechSrNo;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }

        public String getFile() {
            return File;
        }

        public void setFile(String File) {
            this.File = File;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public String getRoleGroup() {
            return RoleGroup;
        }

        public void setRoleGroup(String RoleGroup) {
            this.RoleGroup = RoleGroup;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getConfirmPassword() {
            return ConfirmPassword;
        }

        public void setConfirmPassword(String ConfirmPassword) {
            this.ConfirmPassword = ConfirmPassword;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }
    }
}
