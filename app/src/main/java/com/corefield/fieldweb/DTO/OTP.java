package com.corefield.fieldweb.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 4/8/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.User
 * Version : 1.1.6
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class OTP {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("Otp")
        private int Otp;

        public int getOtp() {
            return Otp;
        }

        public void setOtp(int Otp) {
            this.Otp = Otp;
        }
    }
}
