package com.corefield.fieldweb.DTO.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * //
 * Created by CFS on 2/17/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Notification
 * Version : 1.1.5
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class UpdateNotificationIsRead {

    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;
    @Expose
    @SerializedName("ResultData")
    private RegisterDeviceId.ResultData ResultData;

    @Expose
    @SerializedName("TaskClosureStatus")
    private boolean TaskClosureStatus;
    @Expose
    @SerializedName("PaymentMode")
    private int PaymentMode;
    @Expose
    @SerializedName("TaskState")
    private int TaskState;
    @Expose
    @SerializedName("PaymentNotReceived")
    private boolean PaymentNotReceived;
    @Expose
    @SerializedName("TaskType")
    private int TaskType;
    @Expose
    @SerializedName("TaskStatus")
    private int TaskStatus;
    @Expose
    @SerializedName("NotificationType")
    private String NotificationType;
    @Expose
    @SerializedName("OwnerId")
    private int OwnerId;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("IsRead")
    private boolean IsRead;
    @Expose
    @SerializedName("TaskId")
    private int TaskId;
    @Expose
    @SerializedName("Id")
    private int Id;
    @Expose
    @SerializedName("AMCServiceDetailsId")
    private int AMCServiceDetailsId;

    public int getAMCServiceDetailsId() {
        return AMCServiceDetailsId;
    }

    public void setAMCServiceDetailsId(int AMCServiceDetailsId) {
        this.AMCServiceDetailsId = AMCServiceDetailsId;
    }

    public boolean getTaskClosureStatus() {
        return TaskClosureStatus;
    }

    public void setTaskClosureStatus(boolean TaskClosureStatus) {
        this.TaskClosureStatus = TaskClosureStatus;
    }

    public int getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(int PaymentMode) {
        this.PaymentMode = PaymentMode;
    }

    public int getTaskState() {
        return TaskState;
    }

    public void setTaskState(int TaskState) {
        this.TaskState = TaskState;
    }

    public boolean getPaymentNotReceived() {
        return PaymentNotReceived;
    }

    public void setPaymentNotReceived(boolean PaymentNotReceived) {
        this.PaymentNotReceived = PaymentNotReceived;
    }

    public int getTaskType() {
        return TaskType;
    }

    public void setTaskType(int TaskType) {
        this.TaskType = TaskType;
    }

    public int getTaskStatus() {
        return TaskStatus;
    }

    public void setTaskStatus(int TaskStatus) {
        this.TaskStatus = TaskStatus;
    }

    public String getNotificationType() {
        return NotificationType;
    }

    public void setNotificationType(String NotificationType) {
        this.NotificationType = NotificationType;
    }

    /*public String getOwnerId() {
        return OwnerId;
    }

    public void setOwnerId(String OwnerId) {
        this.OwnerId = OwnerId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }*/

    public int getOwnerId() {
        return OwnerId;
    }

    public void setOwnerId(int ownerId) {
        OwnerId = ownerId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public boolean getIsRead() {
        return IsRead;
    }

    public void setIsRead(boolean IsRead) {
        this.IsRead = IsRead;
    }

    public int getTaskId() {
        return TaskId;
    }

    public void setTaskId(int TaskId) {
        this.TaskId = TaskId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public boolean isTaskClosureStatus() {
        return TaskClosureStatus;
    }

    public boolean isPaymentNotReceived() {
        return PaymentNotReceived;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public RegisterDeviceId.ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(RegisterDeviceId.ResultData resultData) {
        ResultData = resultData;
    }
}
