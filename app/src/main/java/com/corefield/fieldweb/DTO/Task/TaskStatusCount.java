package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Task Status Count (Owner login)
 */
public class TaskStatusCount {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("Taskcount")
        private int Taskcount;
        @Expose
        @SerializedName("TaskStatusId")
        private int TaskStatusId;
        @Expose
        @SerializedName("Name")
        private String Name;

        public int getTaskcount() {
            return Taskcount;
        }

        public void setTaskcount(int Taskcount) {
            this.Taskcount = Taskcount;
        }

        public int getTaskStatusId() {
            return TaskStatusId;
        }

        public void setTaskStatusId(int TaskStatusId) {
            this.TaskStatusId = TaskStatusId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }
    }
}
