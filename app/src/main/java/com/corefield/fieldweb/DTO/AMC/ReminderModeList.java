package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReminderModeList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }
    }
}
