package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * //
 * Created by CFS on 8/21/2020.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO.Task
 * Version : 1.2.2
 * Copyright (c) 2020 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class UpdateTask {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("OnHoldTaskId")
        private int OnHoldTaskId;
        @Expose
        @SerializedName("MultipleItemAssigned")
        private List<MultipleItemAssigned> MultipleItemAssigned;
        @Expose
        @SerializedName("Base64AudioString")
        private String Base64AudioString;
        @Expose
        @SerializedName("NewAddedTaskId")
        private int NewAddedTaskId;
        @Expose
        @SerializedName("AudioFilePath")
        private String AudioFilePath;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;
        @Expose
        @SerializedName("AMCServiceDetailsId")
        private int AMCServiceDetailsId;
        @Expose
        @SerializedName("PaymentModeId")
        private int PaymentModeId;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("ItemQuantity")
        private int ItemQuantity;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("LocIsActive")
        private boolean LocIsActive;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("LocDescription")
        private String LocDescription;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("LocName")
        private String LocName;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("WagesPerHour")
        private int WagesPerHour;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("Time")
        private String Time;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskType")
        private int TaskType;
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getOnHoldTaskId() {
            return OnHoldTaskId;
        }

        public void setOnHoldTaskId(int OnHoldTaskId) {
            this.OnHoldTaskId = OnHoldTaskId;
        }

        public List<MultipleItemAssigned> getMultipleItemAssigned() {
            return MultipleItemAssigned;
        }

        public void setMultipleItemAssigned(List<MultipleItemAssigned> MultipleItemAssigned) {
            this.MultipleItemAssigned = MultipleItemAssigned;
        }

        public String getBase64AudioString() {
            return Base64AudioString;
        }

        public void setBase64AudioString(String Base64AudioString) {
            this.Base64AudioString = Base64AudioString;
        }

        public int getNewAddedTaskId() {
            return NewAddedTaskId;
        }

        public void setNewAddedTaskId(int NewAddedTaskId) {
            this.NewAddedTaskId = NewAddedTaskId;
        }

        public String getAudioFilePath() {
            return AudioFilePath;
        }

        public void setAudioFilePath(String AudioFilePath) {
            this.AudioFilePath = AudioFilePath;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }

        public int getAMCServiceDetailsId() {
            return AMCServiceDetailsId;
        }

        public void setAMCServiceDetailsId(int AMCServiceDetailsId) {
            this.AMCServiceDetailsId = AMCServiceDetailsId;
        }

        public int getPaymentModeId() {
            return PaymentModeId;
        }

        public void setPaymentModeId(int PaymentModeId) {
            this.PaymentModeId = PaymentModeId;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public int getItemQuantity() {
            return ItemQuantity;
        }

        public void setItemQuantity(int ItemQuantity) {
            this.ItemQuantity = ItemQuantity;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        public boolean getLocIsActive() {
            return LocIsActive;
        }

        public void setLocIsActive(boolean LocIsActive) {
            this.LocIsActive = LocIsActive;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLocDescription() {
            return LocDescription;
        }

        public void setLocDescription(String LocDescription) {
            this.LocDescription = LocDescription;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLocName() {
            return LocName;
        }

        public void setLocName(String LocName) {
            this.LocName = LocName;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getWagesPerHour() {
            return WagesPerHour;
        }

        public void setWagesPerHour(int WagesPerHour) {
            this.WagesPerHour = WagesPerHour;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String Time) {
            this.Time = Time;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public int getTaskType() {
            return TaskType;
        }

        public void setTaskType(int TaskType) {
            this.TaskType = TaskType;
        }

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class MultipleItemAssigned {
        @Expose
        @SerializedName("UsedItemQty")
        private int UsedItemQty;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("ItemQuantity")
        private int ItemQuantity;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;

        public int getUsedItemQty() {
            return UsedItemQty;
        }

        public void setUsedItemQty(int UsedItemQty) {
            this.UsedItemQty = UsedItemQty;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int ItemIssuedId) {
            this.ItemIssuedId = ItemIssuedId;
        }

        public int getItemQuantity() {
            return ItemQuantity;
        }

        public void setItemQuantity(int ItemQuantity) {
            this.ItemQuantity = ItemQuantity;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }
}
