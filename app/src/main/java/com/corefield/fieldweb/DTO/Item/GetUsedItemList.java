package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetUsedItemList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("ItemIssuedQuantity")
        private int ItemIssuedQuantity;
        @Expose
        @SerializedName("PurchasePrice")
        private int PurchasePrice;
        @Expose
        @SerializedName("SalesPrice")
        private int SalesPrice;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("TechFullName")
        private String TechFullName;
        @Expose
        @SerializedName("TaskName")
        private String TaskName;
        @Expose
        @SerializedName("Notes")
        private String Notes;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdateBy")
        private int UpdateBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("UsedQty")
        private int UsedQty;
        @Expose
        @SerializedName("CurrentAssignedQty")
        private int CurrentAssignedQty;
        @Expose
        @SerializedName("AssignedQty")
        private int AssignedQty;
        @Expose
        @SerializedName("AvlQty")
        private int AvlQty;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getItemIssuedQuantity() {
            return ItemIssuedQuantity;
        }

        public void setItemIssuedQuantity(int ItemIssuedQuantity) {
            this.ItemIssuedQuantity = ItemIssuedQuantity;
        }

        public int getPurchasePrice() {
            return PurchasePrice;
        }

        public void setPurchasePrice(int PurchasePrice) {
            this.PurchasePrice = PurchasePrice;
        }

        public int getSalesPrice() {
            return SalesPrice;
        }

        public void setSalesPrice(int SalesPrice) {
            this.SalesPrice = SalesPrice;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public String getTechFullName() {
            return TechFullName;
        }

        public void setTechFullName(String TechFullName) {
            this.TechFullName = TechFullName;
        }

        public String getTaskName() {
            return TaskName;
        }

        public void setTaskName(String TaskName) {
            this.TaskName = TaskName;
        }

        public String getNotes() {
            return Notes;
        }

        public void setNotes(String Notes) {
            this.Notes = Notes;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdateBy() {
            return UpdateBy;
        }

        public void setUpdateBy(int UpdateBy) {
            this.UpdateBy = UpdateBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getUsedQty() {
            return UsedQty;
        }

        public void setUsedQty(int UsedQty) {
            this.UsedQty = UsedQty;
        }

        public int getCurrentAssignedQty() {
            return CurrentAssignedQty;
        }

        public void setCurrentAssignedQty(int CurrentAssignedQty) {
            this.CurrentAssignedQty = CurrentAssignedQty;
        }

        public int getAssignedQty() {
            return AssignedQty;
        }

        public void setAssignedQty(int AssignedQty) {
            this.AssignedQty = AssignedQty;
        }

        public int getAvlQty() {
            return AvlQty;
        }

        public void setAvlQty(int AvlQty) {
            this.AvlQty = AvlQty;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int ItemIssuedId) {
            this.ItemIssuedId = ItemIssuedId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
