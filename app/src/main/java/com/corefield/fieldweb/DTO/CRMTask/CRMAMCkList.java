package com.corefield.fieldweb.DTO.CRMTask;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CRMAMCkList {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("AMCServiceDate")
        private String AMCServiceDate;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("AMCServicesLeftCount")
        private int AMCServicesLeftCount;
        @Expose
        @SerializedName("CustomerId")
        private int CustomerId;
        @Expose
        @SerializedName("ProductDetailsId")
        private int ProductDetailsId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("TotalServices1")
        private int TotalServices1;
        @Expose
        @SerializedName("TotalServices")
        private int TotalServices;
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;
        @Expose
        @SerializedName("ActivationTime")
        private String ActivationTime;
        @Expose
        @SerializedName("ActivationDate")
        private String ActivationDate;
        @Expose
        @SerializedName("AMCName")
        private String AMCName;
        @Expose
        @SerializedName("AMCsId")
        private int AMCsId;

        public String getAMCServiceDate() {
            return AMCServiceDate;
        }

        public void setAMCServiceDate(String AMCServiceDate) {
            this.AMCServiceDate = AMCServiceDate;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getAMCServicesLeftCount() {
            return AMCServicesLeftCount;
        }

        public void setAMCServicesLeftCount(int AMCServicesLeftCount) {
            this.AMCServicesLeftCount = AMCServicesLeftCount;
        }

        public int getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(int CustomerId) {
            this.CustomerId = CustomerId;
        }

        public int getProductDetailsId() {
            return ProductDetailsId;
        }

        public void setProductDetailsId(int ProductDetailsId) {
            this.ProductDetailsId = ProductDetailsId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getTotalServices1() {
            return TotalServices1;
        }

        public void setTotalServices1(int TotalServices1) {
            this.TotalServices1 = TotalServices1;
        }

        public int getTotalServices() {
            return TotalServices;
        }

        public void setTotalServices(int TotalServices) {
            this.TotalServices = TotalServices;
        }

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }

        public String getActivationTime() {
            return ActivationTime;
        }

        public void setActivationTime(String ActivationTime) {
            this.ActivationTime = ActivationTime;
        }

        public String getActivationDate() {
            return ActivationDate;
        }

        public void setActivationDate(String ActivationDate) {
            this.ActivationDate = ActivationDate;
        }

        public String getAMCName() {
            return AMCName;
        }

        public void setAMCName(String AMCName) {
            this.AMCName = AMCName;
        }

        public int getAMCsId() {
            return AMCsId;
        }

        public void setAMCsId(int AMCsId) {
            this.AMCsId = AMCsId;
        }
    }
}
