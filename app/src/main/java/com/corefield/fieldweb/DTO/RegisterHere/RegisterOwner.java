package com.corefield.fieldweb.DTO.RegisterHere;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterOwner {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("LgModel")
        private LgModel LgModel;
        @Expose
        @SerializedName("LoginOutPut")
        private LoginOutPut LoginOutPut;
        @Expose
        @SerializedName("NoOfUsers")
        private int NoOfUsers;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("CountryDetailsId")
        private int CountryDetailsId;
        @Expose
        @SerializedName("OTP")
        private int OTP;
        @Expose
        @SerializedName("EmailOrContactNo")
        private String EmailOrContactNo;
        @Expose
        @SerializedName("TouchlessSignupId")
        private int TouchlessSignupId;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        public LgModel getLgModel() {
            return LgModel;
        }

        public void setLgModel(LgModel LgModel) {
            this.LgModel = LgModel;
        }

        public LoginOutPut getLoginOutPut() {
            return LoginOutPut;
        }

        public void setLoginOutPut(LoginOutPut LoginOutPut) {
            this.LoginOutPut = LoginOutPut;
        }

        public int getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(int NoOfUsers) {
            this.NoOfUsers = NoOfUsers;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getCountryDetailsId() {
            return CountryDetailsId;
        }

        public void setCountryDetailsId(int CountryDetailsId) {
            this.CountryDetailsId = CountryDetailsId;
        }

        public int getOTP() {
            return OTP;
        }

        public void setOTP(int OTP) {
            this.OTP = OTP;
        }

        public String getEmailOrContactNo() {
            return EmailOrContactNo;
        }

        public void setEmailOrContactNo(String EmailOrContactNo) {
            this.EmailOrContactNo = EmailOrContactNo;
        }

        public int getTouchlessSignupId() {
            return TouchlessSignupId;
        }

        public void setTouchlessSignupId(int TouchlessSignupId) {
            this.TouchlessSignupId = TouchlessSignupId;
        }
    }

    public static class LgModel {
        @Expose
        @SerializedName("PortalDeviceId")
        private String PortalDeviceId;
        @Expose
        @SerializedName("UserPreferredLanguage")
        private String UserPreferredLanguage;
        @Expose
        @SerializedName("UserID")
        private int UserID;
        @Expose
        @SerializedName("AndroidID")
        private String AndroidID;
        @Expose
        @SerializedName("Password")
        private String Password;
        @Expose
        @SerializedName("UserName")
        private String UserName;

        public String getPortalDeviceId() {
            return PortalDeviceId;
        }

        public void setPortalDeviceId(String PortalDeviceId) {
            this.PortalDeviceId = PortalDeviceId;
        }

        public String getUserPreferredLanguage() {
            return UserPreferredLanguage;
        }

        public void setUserPreferredLanguage(String UserPreferredLanguage) {
            this.UserPreferredLanguage = UserPreferredLanguage;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getAndroidID() {
            return AndroidID;
        }

        public void setAndroidID(String AndroidID) {
            this.AndroidID = AndroidID;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }
    }

    public static class LoginOutPut {
        @Expose
        @SerializedName("NoOfUsers")
        private int NoOfUsers;
        @Expose
        @SerializedName("CountryDetailsId")
        private int CountryDetailsId;
        @Expose
        @SerializedName("IsAppTourCompleted")
        private boolean IsAppTourCompleted;
        @Expose
        @SerializedName("OTP")
        private int OTP;
        @Expose
        @SerializedName("PortalDeviceId")
        private String PortalDeviceId;
        @Expose
        @SerializedName("UserPreferredLanguage")
        private String UserPreferredLanguage;
        @Expose
        @SerializedName("IsForgotPassword")
        private boolean IsForgotPassword;
        @Expose
        @SerializedName("UserGroupName")
        private String UserGroupName;
        @Expose
        @SerializedName("UserID")
        private int UserID;
        @Expose
        @SerializedName("Token")
        private String Token;

        public int getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(int NoOfUsers) {
            this.NoOfUsers = NoOfUsers;
        }

        public int getCountryDetailsId() {
            return CountryDetailsId;
        }

        public void setCountryDetailsId(int CountryDetailsId) {
            this.CountryDetailsId = CountryDetailsId;
        }

        public boolean getIsAppTourCompleted() {
            return IsAppTourCompleted;
        }

        public void setIsAppTourCompleted(boolean IsAppTourCompleted) {
            this.IsAppTourCompleted = IsAppTourCompleted;
        }

        public int getOTP() {
            return OTP;
        }

        public void setOTP(int OTP) {
            this.OTP = OTP;
        }

        public String getPortalDeviceId() {
            return PortalDeviceId;
        }

        public void setPortalDeviceId(String PortalDeviceId) {
            this.PortalDeviceId = PortalDeviceId;
        }

        public String getUserPreferredLanguage() {
            return UserPreferredLanguage;
        }

        public void setUserPreferredLanguage(String UserPreferredLanguage) {
            this.UserPreferredLanguage = UserPreferredLanguage;
        }

        public boolean getIsForgotPassword() {
            return IsForgotPassword;
        }

        public void setIsForgotPassword(boolean IsForgotPassword) {
            this.IsForgotPassword = IsForgotPassword;
        }

        public String getUserGroupName() {
            return UserGroupName;
        }

        public void setUserGroupName(String UserGroupName) {
            this.UserGroupName = UserGroupName;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getToken() {
            return Token;
        }

        public void setToken(String Token) {
            this.Token = Token;
        }
    }
}
