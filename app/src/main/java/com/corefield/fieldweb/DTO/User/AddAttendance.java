package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to add Attendance (Tech login)
 */
public class AddAttendance {


    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("AttendanceTypeId")
        private int AttendanceTypeId;
        @Expose
        @SerializedName("AttendanceDate")
        private String AttendanceDate;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("AttendanceMarkedPlace")
        private String AttendanceMarkedPlace;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;

        public String getAttendanceMarkedPlace() {
            return AttendanceMarkedPlace;
        }

        public void setAttendanceMarkedPlace(String attendanceMarkedPlace) {
            AttendanceMarkedPlace = attendanceMarkedPlace;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        /*public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }*/

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            UpdatedBy = updatedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getAttendanceTypeId() {
            return AttendanceTypeId;
        }

        public void setAttendanceTypeId(int AttendanceTypeId) {
            this.AttendanceTypeId = AttendanceTypeId;
        }

        public String getAttendanceDate() {
            return AttendanceDate;
        }

        public void setAttendanceDate(String AttendanceDate) {
            this.AttendanceDate = AttendanceDate;
        }

        /*public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
