package com.corefield.fieldweb.DTO;

public class DatesDto {
    int date;
    String day;

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
