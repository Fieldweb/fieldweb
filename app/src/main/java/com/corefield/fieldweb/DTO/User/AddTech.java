package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to add Technician (Owner login)
 */
public class AddTech {


    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

}
