package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get user details
 */
public class UserDetails {
    @Expose
    @SerializedName("Code")
    private String Code;
    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public static class ResultData implements Serializable {
        @Expose
        @SerializedName("ReferralCodeName")
        private String ReferralCodeName;
        @Expose
        @SerializedName("ReferralCodesId")
        private int ReferralCodesId;
        @Expose
        @SerializedName("CompanyGSTorPanNo")
        private String CompanyGSTorPanNo;
        @Expose
        @SerializedName("CompanyServiceTypeId")
        private int CompanyServiceTypeId;
        @Expose
        @SerializedName("CompanyLogo")
        private String CompanyLogo;
        @Expose
        @SerializedName("CompanyWebsite")
        private String CompanyWebsite;
        @Expose
        @SerializedName("CompanyContactNo")
        private long CompanyContactNo;
        @Expose
        @SerializedName("CompanyAddress")
        private String CompanyAddress;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;
        @Expose
        @SerializedName("CompanyId")
        private int CompanyId;
        @Expose
        @SerializedName("CheckOutPlace")
        private String CheckOutPlace;
        @Expose
        @SerializedName("ModifiedDate")
        private String ModifiedDate;
        @Expose
        @SerializedName("IsRegistered")
        private boolean IsRegistered;
        @Expose
        @SerializedName("CheckOutTime")
        private String CheckOutTime;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("IsHealthFeaturesEnabled")
        private boolean IsHealthFeaturesEnabled;
        @Expose
        @SerializedName("ArogyaSetuStatus")
        private String ArogyaSetuStatus;
        @Expose
        @SerializedName("AarogyaSetuStatusId")
        private int AarogyaSetuStatusId;
        @Expose
        @SerializedName("TemperatureUnitType")
        private String TemperatureUnitType;
        @Expose
        @SerializedName("TemperatureUnitTypeId")
        private int TemperatureUnitTypeId;
        @Expose
        @SerializedName("Temperature")
        private int Temperature;
        @Expose
        @SerializedName("AttendanceMarkedTime")
        private String AttendanceMarkedTime;
        @Expose
        @SerializedName("AttendanceMarkedPlace")
        private String AttendanceMarkedPlace;
        @Expose
        @SerializedName("TechLongitude")
        private String TechLongitude;
        @Expose
        @SerializedName("TechLatitude")
        private String TechLatitude;
        @Expose
        @SerializedName("UserID")
        private int UserID;
        @Expose
        @SerializedName("AttendanceId")
        private int AttendanceId;
        @Expose
        @SerializedName("Attendance")
        private String Attendance;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("MiddleName")
        private String MiddleName;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("Role")
        private String Role;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("DOB")
        private String DOB;
        @Expose
        @SerializedName("CompanyCity")
        private String CompanyCity;
        @Expose
        @SerializedName("ProgressBarPercentage")
        private String ProgressBarPercentage;
        @Expose
        @SerializedName("NoOfUsers")
        private String NoOfUsers;
        @Expose
        @SerializedName("AadharCardNo")
        private String AadharCardNo;
        @Expose
        @SerializedName("OwnerFirstName")
        private String OwnerFirstName;
        @Expose
        @SerializedName("OwnerLastName")
        private String OwnerLastName;
        @Expose
        @SerializedName("IsTeleCmiEnabled")
        private String IsTeleCmiEnabled;

        public String getIsTeleCmiEnabled() {
            return IsTeleCmiEnabled;
        }

        public void setIsTeleCmiEnabled(String isTeleCmiEnabled) {
            IsTeleCmiEnabled = isTeleCmiEnabled;
        }

        public String getOwnerFirstName() {
            return OwnerFirstName;
        }

        public void setOwnerFirstName(String ownerFirstName) {
            OwnerFirstName = ownerFirstName;
        }

        public String getOwnerLastName() {
            return OwnerLastName;
        }

        public void setOwnerLastName(String ownerLastName) {
            OwnerLastName = ownerLastName;
        }

        public String getNoOfUsers() {
            return NoOfUsers;
        }

        public void setNoOfUsers(String noOfUsers) {
            NoOfUsers = noOfUsers;
        }

        public String getAadharCardNo() {
            return AadharCardNo;
        }

        public void setAadharCardNo(String aadharCardNo) {
            AadharCardNo = aadharCardNo;
        }


        public String getProgressBarPercentage() {
            return ProgressBarPercentage;
        }

        public void setProgressBarPercentage(String progressBarPercentage) {
            ProgressBarPercentage = progressBarPercentage;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getCompanyCity() {
            return CompanyCity;
        }

        public void setCompanyCity(String companyCity) {
            CompanyCity = companyCity;
        }

        public String getReferralCodeName() {
            return ReferralCodeName;
        }

        public void setReferralCodeName(String ReferralCodeName) {
            this.ReferralCodeName = ReferralCodeName;
        }

        public int getReferralCodesId() {
            return ReferralCodesId;
        }

        public void setReferralCodesId(int ReferralCodesId) {
            this.ReferralCodesId = ReferralCodesId;
        }

        public String getCompanyGSTorPanNo() {
            return CompanyGSTorPanNo;
        }

        public void setCompanyGSTorPanNo(String CompanyGSTorPanNo) {
            this.CompanyGSTorPanNo = CompanyGSTorPanNo;
        }

        public int getCompanyServiceTypeId() {
            return CompanyServiceTypeId;
        }

        public void setCompanyServiceTypeId(int CompanyServiceTypeId) {
            this.CompanyServiceTypeId = CompanyServiceTypeId;
        }

        public String getCompanyLogo() {
            return CompanyLogo;
        }

        public void setCompanyLogo(String CompanyLogo) {
            this.CompanyLogo = CompanyLogo;
        }

        public String getCompanyWebsite() {
            return CompanyWebsite;
        }

        public void setCompanyWebsite(String CompanyWebsite) {
            this.CompanyWebsite = CompanyWebsite;
        }

        public long getCompanyContactNo() {
            return CompanyContactNo;
        }

        public void setCompanyContactNo(long CompanyContactNo) {
            this.CompanyContactNo = CompanyContactNo;
        }

        public String getCompanyAddress() {
            return CompanyAddress;
        }

        public void setCompanyAddress(String CompanyAddress) {
            this.CompanyAddress = CompanyAddress;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String CompanyName) {
            this.CompanyName = CompanyName;
        }

        public int getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(int CompanyId) {
            this.CompanyId = CompanyId;
        }

        public String getCheckOutPlace() {
            return CheckOutPlace;
        }

        public void setCheckOutPlace(String CheckOutPlace) {
            this.CheckOutPlace = CheckOutPlace;
        }

        public String getModifiedDate() {
            return ModifiedDate;
        }

        public void setModifiedDate(String ModifiedDate) {
            this.ModifiedDate = ModifiedDate;
        }

        public boolean getIsRegistered() {
            return IsRegistered;
        }

        public void setIsRegistered(boolean IsRegistered) {
            this.IsRegistered = IsRegistered;
        }

        public String getCheckOutTime() {
            return CheckOutTime;
        }

        public void setCheckOutTime(String CheckOutTime) {
            this.CheckOutTime = CheckOutTime;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public boolean getIsHealthFeaturesEnabled() {
            return IsHealthFeaturesEnabled;
        }

        public void setIsHealthFeaturesEnabled(boolean IsHealthFeaturesEnabled) {
            this.IsHealthFeaturesEnabled = IsHealthFeaturesEnabled;
        }

        public String getArogyaSetuStatus() {
            return ArogyaSetuStatus;
        }

        public void setArogyaSetuStatus(String ArogyaSetuStatus) {
            this.ArogyaSetuStatus = ArogyaSetuStatus;
        }

        public int getAarogyaSetuStatusId() {
            return AarogyaSetuStatusId;
        }

        public void setAarogyaSetuStatusId(int AarogyaSetuStatusId) {
            this.AarogyaSetuStatusId = AarogyaSetuStatusId;
        }

        public String getTemperatureUnitType() {
            return TemperatureUnitType;
        }

        public void setTemperatureUnitType(String TemperatureUnitType) {
            this.TemperatureUnitType = TemperatureUnitType;
        }

        public int getTemperatureUnitTypeId() {
            return TemperatureUnitTypeId;
        }

        public void setTemperatureUnitTypeId(int TemperatureUnitTypeId) {
            this.TemperatureUnitTypeId = TemperatureUnitTypeId;
        }

        public int getTemperature() {
            return Temperature;
        }

        public void setTemperature(int Temperature) {
            this.Temperature = Temperature;
        }

        public String getAttendanceMarkedTime() {
            return AttendanceMarkedTime;
        }

        public void setAttendanceMarkedTime(String AttendanceMarkedTime) {
            this.AttendanceMarkedTime = AttendanceMarkedTime;
        }

        public String getAttendanceMarkedPlace() {
            return AttendanceMarkedPlace;
        }

        public void setAttendanceMarkedPlace(String AttendanceMarkedPlace) {
            this.AttendanceMarkedPlace = AttendanceMarkedPlace;
        }

        public String getTechLongitude() {
            return TechLongitude;
        }

        public void setTechLongitude(String TechLongitude) {
            this.TechLongitude = TechLongitude;
        }

        public String getTechLatitude() {
            return TechLatitude;
        }

        public void setTechLatitude(String TechLatitude) {
            this.TechLatitude = TechLatitude;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getAttendanceId() {
            return AttendanceId;
        }

        public void setAttendanceId(int AttendanceId) {
            this.AttendanceId = AttendanceId;
        }

        public String getAttendance() {
            return Attendance;
        }

        public void setAttendance(String Attendance) {
            this.Attendance = Attendance;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getMiddleName() {
            return MiddleName;
        }

        public void setMiddleName(String MiddleName) {
            this.MiddleName = MiddleName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getRole() {
            return Role;
        }

        public void setRole(String Role) {
            this.Role = Role;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    /*@Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("Photo")
        private String Photo;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("MiddleName")
        private String MiddleName;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("Role")
        private String Role;
        @Expose
        @SerializedName("RoleId")
        private String RoleId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("IsRegistered")
        private boolean IsRegistered;

        public boolean isRegistered() {
            return IsRegistered;
        }

        public void setRegistered(boolean registered) {
            IsRegistered = registered;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String Photo) {
            this.Photo = Photo;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getMiddleName() {
            return MiddleName;
        }

        public void setMiddleName(String MiddleName) {
            this.MiddleName = MiddleName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getRole() {
            return Role;
        }

        public void setRole(String Role) {
            this.Role = Role;
        }

        public String getRoleId() {
            return RoleId;
        }

        public void setRoleId(String RoleId) {
            this.RoleId = RoleId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }
    }*/
}
