package com.corefield.fieldweb.DTO.Task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to update Task (Test Mode)
 */
public class UpdateTaskStatus {

    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Code")
    private String code;
    @Expose
    @SerializedName("TotalDistance")
    private Double TotalDistance;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getTotalDistance() {
        return TotalDistance;
    }

    public void setTotalDistance(Double totalDistance) {
        TotalDistance = totalDistance;
    }
}
