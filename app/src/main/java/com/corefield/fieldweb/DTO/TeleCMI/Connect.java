package com.corefield.fieldweb.DTO.TeleCMI;

//public class Connect {

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Connect {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("number")
    @Expose
    private Long number;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

}


