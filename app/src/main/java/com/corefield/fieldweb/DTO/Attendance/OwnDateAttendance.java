package com.corefield.fieldweb.DTO.Attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Attendance of technician (Owner login)
 */
public class OwnDateAttendance {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("IdleCount")
        private int IdleCount;
        @Expose
        @SerializedName("PresentCount")
        private int PresentCount;
        @Expose
        @SerializedName("AbsentCount")
        private int AbsentCount;
        @Expose
        @SerializedName("Date")
        private String Date;

        public int getIdleCount() {
            return IdleCount;
        }

        public void setIdleCount(int IdleCount) {
            this.IdleCount = IdleCount;
        }

        public int getPresentCount() {
            return PresentCount;
        }

        public void setPresentCount(int PresentCount) {
            this.PresentCount = PresentCount;
        }

        public int getAbsentCount() {
            return AbsentCount;
        }

        public void setAbsentCount(int AbsentCount) {
            this.AbsentCount = AbsentCount;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }
    }
}
