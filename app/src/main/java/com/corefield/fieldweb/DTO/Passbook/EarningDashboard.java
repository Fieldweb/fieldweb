package com.corefield.fieldweb.DTO.Passbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EarningDashboard {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("EstimatedAmountYesterDay")
        private int EstimatedAmountYesterDay;
        @Expose
        @SerializedName("EarningAmountYesterDay")
        private int EarningAmountYesterDay;
        @Expose
        @SerializedName("Balance")
        private int Balance;
        @Expose
        @SerializedName("Credit")
        private int Credit;
        @Expose
        @SerializedName("Return")
        private int Return;
        @Expose
        @SerializedName("Expenses")
        private int Expenses;
        @Expose
        @SerializedName("EstimatedAmount")
        private int EstimatedAmount;
        @Expose
        @SerializedName("EarningAmount")
        private int EarningAmount;

        public int getEstimatedAmountYesterDay() {
            return EstimatedAmountYesterDay;
        }

        public void setEstimatedAmountYesterDay(int EstimatedAmountYesterDay) {
            this.EstimatedAmountYesterDay = EstimatedAmountYesterDay;
        }

        public int getEarningAmountYesterDay() {
            return EarningAmountYesterDay;
        }

        public void setEarningAmountYesterDay(int EarningAmountYesterDay) {
            this.EarningAmountYesterDay = EarningAmountYesterDay;
        }

        public int getBalance() {
            return Balance;
        }

        public void setBalance(int Balance) {
            this.Balance = Balance;
        }

        public int getCredit() {
            return Credit;
        }

        public void setCredit(int Credit) {
            this.Credit = Credit;
        }

        public int getReturn() {
            return Return;
        }

        public void setReturn(int Return) {
            this.Return = Return;
        }

        public int getExpenses() {
            return Expenses;
        }

        public void setExpenses(int Expenses) {
            this.Expenses = Expenses;
        }

        public int getEstimatedAmount() {
            return EstimatedAmount;
        }

        public void setEstimatedAmount(int EstimatedAmount) {
            this.EstimatedAmount = EstimatedAmount;
        }

        public int getEarningAmount() {
            return EarningAmount;
        }

        public void setEarningAmount(int EarningAmount) {
            this.EarningAmount = EarningAmount;
        }
    }
}
