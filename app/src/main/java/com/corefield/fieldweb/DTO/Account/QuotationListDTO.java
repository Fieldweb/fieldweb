package com.corefield.fieldweb.DTO.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuotationListDTO {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("QuoteServiceList")
        private List<QuoteServiceList> QuoteServiceList;
        @Expose
        @SerializedName("QuoteTaxList")
        private List<QuoteTaxList> QuoteTaxList;
        @Expose
        @SerializedName("QuoteItemList")
        private List<QuoteItemList> QuoteItemList;
        @Expose
        @SerializedName("Status")
        private Status Status;
        @Expose
        @SerializedName("Customer")
        private Customer Customer;
        @Expose
        @SerializedName("SLAAttachmentFile")
        private SLAAttachmentFile SLAAttachmentFile;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("StatusId")
        private int StatusId;
        @Expose
        @SerializedName("CustomerId")
        private int CustomerId;
        @Expose
        @SerializedName("GrandTotalAmount")
        private double GrandTotalAmount;
        @Expose
        @SerializedName("TotalAmount")
        private double TotalAmount;
        @Expose
        @SerializedName("TermCondition")
        private String TermCondition;
        @Expose
        @SerializedName("ValidityDate")
        private String ValidityDate;
        @Expose
        @SerializedName("QuoteTime")
        private String QuoteTime;
        @Expose
        @SerializedName("QuoteName")
        private String QuoteName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public List<QuoteServiceList> getQuoteServiceList() {
            return QuoteServiceList;
        }

        public void setQuoteServiceList(List<QuoteServiceList> QuoteServiceList) {
            this.QuoteServiceList = QuoteServiceList;
        }

        public List<QuoteTaxList> getQuoteTaxList() {
            return QuoteTaxList;
        }

        public void setQuoteTaxList(List<QuoteTaxList> QuoteTaxList) {
            this.QuoteTaxList = QuoteTaxList;
        }

        public List<QuoteItemList> getQuoteItemList() {
            return QuoteItemList;
        }

        public void setQuoteItemList(List<QuoteItemList> QuoteItemList) {
            this.QuoteItemList = QuoteItemList;
        }

        public Status getStatus() {
            return Status;
        }

        public void setStatus(Status Status) {
            this.Status = Status;
        }

        public Customer getCustomer() {
            return Customer;
        }

        public void setCustomer(Customer Customer) {
            this.Customer = Customer;
        }

        public SLAAttachmentFile getSLAAttachmentFile() {
            return SLAAttachmentFile;
        }

        public void setSLAAttachmentFile(SLAAttachmentFile SLAAttachmentFile) {
            this.SLAAttachmentFile = SLAAttachmentFile;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getStatusId() {
            return StatusId;
        }

        public void setStatusId(int StatusId) {
            this.StatusId = StatusId;
        }

        public int getCustomerId() {
            return CustomerId;
        }

        public void setCustomerId(int CustomerId) {
            this.CustomerId = CustomerId;
        }

        public double getGrandTotalAmount() {
            return GrandTotalAmount;
        }

        public void setGrandTotalAmount(int GrandTotalAmount) {
            this.GrandTotalAmount = GrandTotalAmount;
        }

        public double getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(int TotalAmount) {
            this.TotalAmount = TotalAmount;
        }

        public String getTermCondition() {
            return TermCondition;
        }

        public void setTermCondition(String TermCondition) {
            this.TermCondition = TermCondition;
        }

        public String getValidityDate() {
            return ValidityDate;
        }

        public void setValidityDate(String ValidityDate) {
            this.ValidityDate = ValidityDate;
        }

        public String getQuoteTime() {
            return QuoteTime;
        }

        public void setQuoteTime(String QuoteTime) {
            this.QuoteTime = QuoteTime;
        }

        public String getQuoteName() {
            return QuoteName;
        }

        public void setQuoteName(String QuoteName) {
            this.QuoteName = QuoteName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class QuoteServiceList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(int Price) {
            this.Price = Price;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class QuoteTaxList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TaxAmount")
        private double TaxAmount;
        @Expose
        @SerializedName("Tax")
        private double Tax;
        @Expose
        @SerializedName("WithoutTax")
        private double WithoutTax;
        @Expose
        @SerializedName("DiscountAmounnt")
        private double DiscountAmounnt;
        @Expose
        @SerializedName("Discount")
        private double Discount;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public double getTaxAmount() {
            return TaxAmount;
        }

        public void setTaxAmount(double TaxAmount) {
            this.TaxAmount = TaxAmount;
        }

        public double getTax() {
            return Tax;
        }

        public void setTax(double Tax) {
            this.Tax = Tax;
        }

        public double getWithoutTax() {
            return WithoutTax;
        }

        public void setWithoutTax(int WithoutTax) {
            this.WithoutTax = WithoutTax;
        }

        public double getDiscountAmounnt() {
            return DiscountAmounnt;
        }

        public void setDiscountAmounnt(int DiscountAmounnt) {
            this.DiscountAmounnt = DiscountAmounnt;
        }

        public double getDiscount() {
            return Discount;
        }

        public void setDiscount(double Discount) {
            this.Discount = Discount;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class QuoteItemList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("UnitPrice")
        private double UnitPrice;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("QuotationId")
        private int QuotationId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(int TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getUnitPrice() {
            return UnitPrice;
        }

        public void setUnitPrice(int UnitPrice) {
            this.UnitPrice = UnitPrice;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getQuotationId() {
            return QuotationId;
        }

        public void setQuotationId(int QuotationId) {
            this.QuotationId = QuotationId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Status {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("ModuleId")
        private int ModuleId;
        @Expose
        @SerializedName("StatusName")
        private String StatusName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getModuleId() {
            return ModuleId;
        }

        public void setModuleId(int ModuleId) {
            this.ModuleId = ModuleId;
        }

        public String getStatusName() {
            return StatusName;
        }

        public void setStatusName(String StatusName) {
            this.StatusName = StatusName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Customer {
        @Expose
        @SerializedName("LocationList")
        private LocationList LocationList;
        @Expose
        @SerializedName("Instructions")
        private String Instructions;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public LocationList getLocationList() {
            return LocationList;
        }

        public void setLocationList(LocationList LocationList) {
            this.LocationList = LocationList;
        }

        public String getInstructions() {
            return Instructions;
        }

        public void setInstructions(String Instructions) {
            this.Instructions = Instructions;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }
    }

    public static class LocationList {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class SLAAttachmentFile {
        @Expose
        @SerializedName("InputStream")
        private InputStream InputStream;
        @Expose
        @SerializedName("FileName")
        private String FileName;
        @Expose
        @SerializedName("ContentType")
        private String ContentType;
        @Expose
        @SerializedName("ContentLength")
        private int ContentLength;

        public InputStream getInputStream() {
            return InputStream;
        }

        public void setInputStream(InputStream InputStream) {
            this.InputStream = InputStream;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public String getContentType() {
            return ContentType;
        }

        public void setContentType(String ContentType) {
            this.ContentType = ContentType;
        }

        public int getContentLength() {
            return ContentLength;
        }

        public void setContentLength(int ContentLength) {
            this.ContentLength = ContentLength;
        }
    }

    public static class InputStream {
        @Expose
        @SerializedName("WriteTimeout")
        private int WriteTimeout;
        @Expose
        @SerializedName("ReadTimeout")
        private int ReadTimeout;
        @Expose
        @SerializedName("Position")
        private int Position;
        @Expose
        @SerializedName("Length")
        private int Length;
        @Expose
        @SerializedName("CanWrite")
        private boolean CanWrite;
        @Expose
        @SerializedName("CanTimeout")
        private boolean CanTimeout;
        @Expose
        @SerializedName("CanSeek")
        private boolean CanSeek;
        @Expose
        @SerializedName("CanRead")
        private boolean CanRead;

        public int getWriteTimeout() {
            return WriteTimeout;
        }

        public void setWriteTimeout(int WriteTimeout) {
            this.WriteTimeout = WriteTimeout;
        }

        public int getReadTimeout() {
            return ReadTimeout;
        }

        public void setReadTimeout(int ReadTimeout) {
            this.ReadTimeout = ReadTimeout;
        }

        public int getPosition() {
            return Position;
        }

        public void setPosition(int Position) {
            this.Position = Position;
        }

        public int getLength() {
            return Length;
        }

        public void setLength(int Length) {
            this.Length = Length;
        }

        public boolean getCanWrite() {
            return CanWrite;
        }

        public void setCanWrite(boolean CanWrite) {
            this.CanWrite = CanWrite;
        }

        public boolean getCanTimeout() {
            return CanTimeout;
        }

        public void setCanTimeout(boolean CanTimeout) {
            this.CanTimeout = CanTimeout;
        }

        public boolean getCanSeek() {
            return CanSeek;
        }

        public void setCanSeek(boolean CanSeek) {
            this.CanSeek = CanSeek;
        }

        public boolean getCanRead() {
            return CanRead;
        }

        public void setCanRead(boolean CanRead) {
            this.CanRead = CanRead;
        }
    }
}
