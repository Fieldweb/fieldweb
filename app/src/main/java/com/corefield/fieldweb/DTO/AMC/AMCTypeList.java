package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AMCTypeList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private String CreatedBy;
        @Expose
        @SerializedName("AMCTypeName")
        private String AMCTypeName;
        @Expose
        @SerializedName("AMCTypeId")
        private int AMCTypeId;

        public ResultData(int AMCTypeId, String AMCTypeName) {
            this.AMCTypeId = AMCTypeId;
            this.AMCTypeName = AMCTypeName;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getAMCTypeName() {
            return AMCTypeName;
        }

        public void setAMCTypeName(String AMCTypeName) {
            this.AMCTypeName = AMCTypeName;
        }

        public int getAMCTypeId() {
            return AMCTypeId;
        }

        public void setAMCTypeId(int AMCTypeId) {
            this.AMCTypeId = AMCTypeId;
        }

        //to display object as a string in spinner
        @Override
        public String toString() {
            return AMCTypeName;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof ResultData){
                ResultData c = (ResultData)obj;
                if(c.getAMCTypeName().equals(AMCTypeName) && c.getAMCTypeId()==AMCTypeId) return true;
            }

            return false;
        }
    }
}
