package com.corefield.fieldweb.DTO.Passbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to  get Yearly Passbook for both Owner and Technician login
 */
public class YearlyPassbook {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("GetYearlyData")
        private List<GetYearlyData> GetYearlyData;
        @Expose
        @SerializedName("TotalOpening")
        private int TotalOpening;
        @Expose
        @SerializedName("TotalEarned")
        private int TotalEarned;
        @Expose
        @SerializedName("TotalEstimated")
        private int TotalEstimated;
        @Expose
        @SerializedName("TotalDeduction")
        private int TotalDeduction;
        @Expose
        @SerializedName("TotalCredit")
        private int TotalCredit;
        @Expose
        @SerializedName("TotalExpenses")
        private int TotalExpenses;

        public List<GetYearlyData> getGetYearlyData() {
            return GetYearlyData;
        }

        public void setGetYearlyData(List<GetYearlyData> GetYearlyData) {
            this.GetYearlyData = GetYearlyData;
        }

        public int getTotalOpening() {
            return TotalOpening;
        }

        public void setTotalOpening(int TotalOpening) {
            this.TotalOpening = TotalOpening;
        }

        public int getTotalEarned() {
            return TotalEarned;
        }

        public void setTotalEarned(int TotalEarned) {
            this.TotalEarned = TotalEarned;
        }

        public int getTotalEstimated() {
            return TotalEstimated;
        }

        public void setTotalEstimated(int TotalEstimated) {
            this.TotalEstimated = TotalEstimated;
        }

        public int getTotalDeduction() {
            return TotalDeduction;
        }

        public void setTotalDeduction(int TotalDeduction) {
            this.TotalDeduction = TotalDeduction;
        }

        public int getTotalCredit() {
            return TotalCredit;
        }

        public void setTotalCredit(int TotalCredit) {
            this.TotalCredit = TotalCredit;
        }

        public int getTotalExpenses() {
            return TotalExpenses;
        }

        public void setTotalExpenses(int TotalExpenses) {
            this.TotalExpenses = TotalExpenses;
        }
    }

    public static class GetYearlyData {
        @Expose
        @SerializedName("Expenses")
        private int Expenses;
        @Expose
        @SerializedName("Estimate")
        private int Estimate;
        @Expose
        @SerializedName("Earned")
        private int Earned;
        @Expose
        @SerializedName("Year")
        private int Year;

        public int getExpenses() {
            return Expenses;
        }

        public void setExpenses(int Expenses) {
            this.Expenses = Expenses;
        }

        public int getEstimate() {
            return Estimate;
        }

        public void setEstimate(int Estimate) {
            this.Estimate = Estimate;
        }

        public int getEarned() {
            return Earned;
        }

        public void setEarned(int Earned) {
            this.Earned = Earned;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int Year) {
            this.Year = Year;
        }
    }
}
