package com.corefield.fieldweb.DTO.AMC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AMCList {

    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("TaskDetails")
        private TaskDetails TaskDetails;
        @Expose
        @SerializedName("AMCTypeName")
        private String AMCTypeName;
        @Expose
        @SerializedName("ActualAMCSeriveDate")
        private String ActualAMCSeriveDate;
        @Expose
        @SerializedName("HoursNo")
        private int HoursNo;
        @Expose
        @SerializedName("DaysNo")
        private int DaysNo;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("ServiceOccuranceType")
        private String ServiceOccuranceType;
        @Expose
        @SerializedName("AMCSetReminderType")
        private String AMCSetReminderType;
        @Expose
        @SerializedName("AMCSetReminderId")
        private int AMCSetReminderId;
        @Expose
        @SerializedName("TotalServices")
        private int TotalServices;
        @Expose
        @SerializedName("ServiceOccuranceId")
        private int ServiceOccuranceId;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("ActivationTime")
        private String ActivationTime;
        @Expose
        @SerializedName("ActivationDate")
        private String ActivationDate;
        @Expose
        @SerializedName("AMCAmount")
        private int AMCAmount;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("AMCName")
        private String AMCName;
        @Expose
        @SerializedName("AMCTypeId")
        private int AMCTypeId;
        @Expose
        @SerializedName("ServiceNo")
        private int ServiceNo;
        @Expose
        @SerializedName("AMCServiceDate")
        private String AMCServiceDate;
        @Expose
        @SerializedName("AMCsId")
        private int AMCsId;
        @Expose
        @SerializedName("AMCServiceDetailsId")
        private int AMCServiceDetailsId;

        public TaskDetails getTaskDetails() {
            return TaskDetails;
        }

        public void setTaskDetails(TaskDetails TaskDetails) {
            this.TaskDetails = TaskDetails;
        }

        public String getAMCTypeName() {
            return AMCTypeName;
        }

        public void setAMCTypeName(String AMCTypeName) {
            this.AMCTypeName = AMCTypeName;
        }

        public String getActualAMCSeriveDate() {
            return ActualAMCSeriveDate;
        }

        public void setActualAMCSeriveDate(String ActualAMCSeriveDate) {
            this.ActualAMCSeriveDate = ActualAMCSeriveDate;
        }

        public int getHoursNo() {
            return HoursNo;
        }

        public void setHoursNo(int HoursNo) {
            this.HoursNo = HoursNo;
        }

        public int getDaysNo() {
            return DaysNo;
        }

        public void setDaysNo(int DaysNo) {
            this.DaysNo = DaysNo;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getServiceOccuranceType() {
            return ServiceOccuranceType;
        }

        public void setServiceOccuranceType(String ServiceOccuranceType) {
            this.ServiceOccuranceType = ServiceOccuranceType;
        }

        public String getAMCSetReminderType() {
            return AMCSetReminderType;
        }

        public void setAMCSetReminderType(String AMCSetReminderType) {
            this.AMCSetReminderType = AMCSetReminderType;
        }

        public int getAMCSetReminderId() {
            return AMCSetReminderId;
        }

        public void setAMCSetReminderId(int AMCSetReminderId) {
            this.AMCSetReminderId = AMCSetReminderId;
        }

        public int getTotalServices() {
            return TotalServices;
        }

        public void setTotalServices(int TotalServices) {
            this.TotalServices = TotalServices;
        }

        public int getServiceOccuranceId() {
            return ServiceOccuranceId;
        }

        public void setServiceOccuranceId(int ServiceOccuranceId) {
            this.ServiceOccuranceId = ServiceOccuranceId;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getActivationTime() {
            return ActivationTime;
        }

        public void setActivationTime(String ActivationTime) {
            this.ActivationTime = ActivationTime;
        }

        public String getActivationDate() {
            return ActivationDate;
        }

        public void setActivationDate(String ActivationDate) {
            this.ActivationDate = ActivationDate;
        }

        public int getAMCAmount() {
            return AMCAmount;
        }

        public void setAMCAmount(int AMCAmount) {
            this.AMCAmount = AMCAmount;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getAMCName() {
            return AMCName;
        }

        public void setAMCName(String AMCName) {
            this.AMCName = AMCName;
        }

        public int getAMCTypeId() {
            return AMCTypeId;
        }

        public void setAMCTypeId(int AMCTypeId) {
            this.AMCTypeId = AMCTypeId;
        }

        public int getServiceNo() {
            return ServiceNo;
        }

        public void setServiceNo(int ServiceNo) {
            this.ServiceNo = ServiceNo;
        }

        public String getAMCServiceDate() {
            return AMCServiceDate;
        }

        public void setAMCServiceDate(String AMCServiceDate) {
            this.AMCServiceDate = AMCServiceDate;
        }

        public int getAMCsId() {
            return AMCsId;
        }

        public void setAMCsId(int AMCsId) {
            this.AMCsId = AMCsId;
        }

        public int getAMCServiceDetailsId() {
            return AMCServiceDetailsId;
        }

        public void setAMCServiceDetailsId(int AMCServiceDetailsId) {
            this.AMCServiceDetailsId = AMCServiceDetailsId;
        }
    }

    public static class TaskDetails {
        @Expose
        @SerializedName("TaskTime")
        private String TaskTime;
        @Expose
        @SerializedName("TaskDate")
        private String TaskDate;
        @Expose
        @SerializedName("TaskStatus")
        private int TaskStatus;
        @Expose
        @SerializedName("TechnicianUserId")
        private int TechnicianUserId;
        @Expose
        @SerializedName("TechnicianName")
        private String TechnicianName;
        @Expose
        @SerializedName("TaskName")
        private String TaskName;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;

        public String getTaskTime() {
            return TaskTime;
        }

        public void setTaskTime(String TaskTime) {
            this.TaskTime = TaskTime;
        }

        public String getTaskDate() {
            return TaskDate;
        }

        public void setTaskDate(String TaskDate) {
            this.TaskDate = TaskDate;
        }

        public int getTaskStatus() {
            return TaskStatus;
        }

        public void setTaskStatus(int TaskStatus) {
            this.TaskStatus = TaskStatus;
        }

        public int getTechnicianUserId() {
            return TechnicianUserId;
        }

        public void setTechnicianUserId(int TechnicianUserId) {
            this.TechnicianUserId = TechnicianUserId;
        }

        public String getTechnicianName() {
            return TechnicianName;
        }

        public void setTechnicianName(String TechnicianName) {
            this.TechnicianName = TechnicianName;
        }

        public String getTaskName() {
            return TaskName;
        }

        public void setTaskName(String TaskName) {
            this.TaskName = TaskName;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }
    }
}
