package com.corefield.fieldweb.DTO.Passbook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get Monthly Passbook for both Owner and Technician login
 */
public class MonthlyPassbook {

    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("MonthlyALlDataList")
        private List<MonthlyALlDataList> MonthlyALlDataList;
        @Expose
        @SerializedName("TotalOpening")
        private int TotalOpening;
        @Expose
        @SerializedName("TotalEarned")
        private int TotalEarned;
        @Expose
        @SerializedName("TotalEstimated")
        private int TotalEstimated;
        @Expose
        @SerializedName("TotalDeduction")
        private int TotalDeduction;
        @Expose
        @SerializedName("TotalCredit")
        private int TotalCredit;
        @Expose
        @SerializedName("TotalExpenses")
        private int TotalExpenses;

        public List<MonthlyALlDataList> getMonthlyALlDataList() {
            return MonthlyALlDataList;
        }

        public void setMonthlyALlDataList(List<MonthlyALlDataList> MonthlyALlDataList) {
            this.MonthlyALlDataList = MonthlyALlDataList;
        }

        public int getTotalOpening() {
            return TotalOpening;
        }

        public void setTotalOpening(int TotalOpening) {
            this.TotalOpening = TotalOpening;
        }

        public int getTotalEarned() {
            return TotalEarned;
        }

        public void setTotalEarned(int TotalEarned) {
            this.TotalEarned = TotalEarned;
        }

        public int getTotalEstimated() {
            return TotalEstimated;
        }

        public void setTotalEstimated(int TotalEstimated) {
            this.TotalEstimated = TotalEstimated;
        }

        public int getTotalDeduction() {
            return TotalDeduction;
        }

        public void setTotalDeduction(int TotalDeduction) {
            this.TotalDeduction = TotalDeduction;
        }

        public int getTotalCredit() {
            return TotalCredit;
        }

        public void setTotalCredit(int TotalCredit) {
            this.TotalCredit = TotalCredit;
        }

        public int getTotalExpenses() {
            return TotalExpenses;
        }

        public void setTotalExpenses(int TotalExpenses) {
            this.TotalExpenses = TotalExpenses;
        }
    }

    public static class MonthlyALlDataList {
        @Expose
        @SerializedName("Expenses")
        private int Expenses;
        @Expose
        @SerializedName("Estimated")
        private int Estimated;
        @Expose
        @SerializedName("Earning")
        private int Earning;
        @Expose
        @SerializedName("Month")
        private String Month;
        @Expose
        @SerializedName("Year")
        private String Year;

        public int getExpenses() {
            return Expenses;
        }

        public void setExpenses(int Expenses) {
            this.Expenses = Expenses;
        }

        public int getEstimated() {
            return Estimated;
        }

        public void setEstimated(int Estimated) {
            this.Estimated = Estimated;
        }

        public int getEarning() {
            return Earning;
        }

        public void setEarning(int Earning) {
            this.Earning = Earning;
        }

        public String getMonth() {
            return Month;
        }

        public void setMonth(String Month) {
            this.Month = Month;
        }

        public String getYear() {
            return Year;
        }

        public void setYear(String Year) {
            this.Year = Year;
        }
    }
}
