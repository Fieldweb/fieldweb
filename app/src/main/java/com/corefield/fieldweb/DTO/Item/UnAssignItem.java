package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to  UnAssigned item  (Owner login)
 */
public class UnAssignItem {

    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Code")
    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
