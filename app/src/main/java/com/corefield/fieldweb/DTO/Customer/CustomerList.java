package com.corefield.fieldweb.DTO.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * //
 * Created by CFS on 11/7/2019.
 * Project Name : fieldweb
 * Package : com.corefield.fieldweb.DTO
 * Version : 1.0
 * Copyright (c) 2019 CFS. All rights reserved.
 * Comment :
 * //
 **/
public class CustomerList {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("SecondaryDescription")
        private String SecondaryDescription;
        @Expose
        @SerializedName("SecondaryAddress")
        private String SecondaryAddress;
        @Expose
        @SerializedName("Instructions")
        private String Instructions;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("SecondaryInstructions")
        private String SecondaryInstructions;
        @Expose
        @SerializedName("SecondaryMobileNumber")
        private String SecondaryMobileNumber;
        @Expose
        @SerializedName("SecondaryEmailId")
        private String SecondaryEmailId;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;

        public String getSecondaryDescription() {
            return SecondaryDescription;
        }

        public void setSecondaryDescription(String SecondaryDescription) {
            this.SecondaryDescription = SecondaryDescription;
        }

        public String getSecondaryAddress() {
            return SecondaryAddress;
        }

        public void setSecondaryAddress(String SecondaryAddress) {
            this.SecondaryAddress = SecondaryAddress;
        }

        public String getInstructions() {
            return Instructions;
        }

        public void setInstructions(String Instructions) {
            this.Instructions = Instructions;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getSecondaryInstructions() {
            return SecondaryInstructions;
        }

        public void setSecondaryInstructions(String SecondaryInstructions) {
            this.SecondaryInstructions = SecondaryInstructions;
        }

        public String getSecondaryMobileNumber() {
            return SecondaryMobileNumber;
        }

        public void setSecondaryMobileNumber(String SecondaryMobileNumber) {
            this.SecondaryMobileNumber = SecondaryMobileNumber;
        }

        public String getSecondaryEmailId() {
            return SecondaryEmailId;
        }

        public void setSecondaryEmailId(String SecondaryEmailId) {
            this.SecondaryEmailId = SecondaryEmailId;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }
    }
}
