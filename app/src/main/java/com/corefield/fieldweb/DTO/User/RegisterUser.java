package com.corefield.fieldweb.DTO.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to register User (SignUp )
 */
public class RegisterUser {
    @Expose
    @SerializedName("ResultData")
    private ResultData ResultData;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public ResultData getResultData() {
        return ResultData;
    }

    public void setResultData(ResultData ResultData) {
        this.ResultData = ResultData;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("File")
        private String File;
        @Expose
        @SerializedName("FileName")
        private String FileName;
        @Expose
        @SerializedName("RoleGroup")
        private String RoleGroup;
        @Expose
        @SerializedName("IsModelError")
        private boolean IsModelError;
        @Expose
        @SerializedName("IsSuccessful")
        private boolean IsSuccessful;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("ConfirmPassword")
        private String ConfirmPassword;
        @Expose
        @SerializedName("Password")
        private String Password;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("CompanyName")
        private String CompanyName;

        @Expose
        @SerializedName("CompanyAddress")
        private String CompanyAddress;
        @Expose
        @SerializedName("CompanyContactNo")
        private int CompanyContactNo;
        @Expose
        @SerializedName("CompanyWebsite")
        private String CompanyWebsite;
        @Expose
        @SerializedName("CompanyLogo")
        private String CompanyLogo;
        @Expose
        @SerializedName("CompanyLogoName")
        private String CompanyLogoName;
        @Expose
        @SerializedName("CompanyGSTorPanNo")
        private String CompanyGSTorPanNo;

        public String getCompanyAddress() {
            return CompanyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            CompanyAddress = companyAddress;
        }

        public int getCompanyContactNo() {
            return CompanyContactNo;
        }

        public void setCompanyContactNo(int companyContactNo) {
            CompanyContactNo = companyContactNo;
        }

        public String getCompanyWebsite() {
            return CompanyWebsite;
        }

        public void setCompanyWebsite(String companyWebsite) {
            CompanyWebsite = companyWebsite;
        }

        public String getCompanyLogo() {
            return CompanyLogo;
        }

        public void setCompanyLogo(String companyLogo) {
            CompanyLogo = companyLogo;
        }

        public String getCompanyLogoName() {
            return CompanyLogoName;
        }

        public void setCompanyLogoName(String companyLogoName) {
            CompanyLogoName = companyLogoName;
        }

        public String getCompanyGSTorPanNo() {
            return CompanyGSTorPanNo;
        }

        public void setCompanyGSTorPanNo(String companyGSTorPanNo) {
            CompanyGSTorPanNo = companyGSTorPanNo;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        @Expose
        @SerializedName("referralCode")
        private String referralCode;

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        public String getFile() {
            return File;
        }

        public void setFile(String File) {
            this.File = File;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public String getRoleGroup() {
            return RoleGroup;
        }

        public void setRoleGroup(String RoleGroup) {
            this.RoleGroup = RoleGroup;
        }

        public boolean getIsModelError() {
            return IsModelError;
        }

        public void setIsModelError(boolean IsModelError) {
            this.IsModelError = IsModelError;
        }

        public boolean getIsSuccessful() {
            return IsSuccessful;
        }

        public void setIsSuccessful(boolean IsSuccessful) {
            this.IsSuccessful = IsSuccessful;
        }

        /*public String getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(String CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }*/

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int createdBy) {
            CreatedBy = createdBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int userId) {
            UserId = userId;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getConfirmPassword() {
            return ConfirmPassword;
        }

        public void setConfirmPassword(String ConfirmPassword) {
            this.ConfirmPassword = ConfirmPassword;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }
    }
}
