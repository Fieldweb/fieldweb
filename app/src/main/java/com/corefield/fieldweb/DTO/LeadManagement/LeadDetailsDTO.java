package com.corefield.fieldweb.DTO.LeadManagement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadDetailsDTO {


    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("PhotoPath2")
        private String PhotoPath2;
        @Expose
        @SerializedName("PhotoPath1")
        private String PhotoPath1;
        @Expose
        @SerializedName("PhotoPath")
        private String PhotoPath;
        @Expose
        @SerializedName("LeadStatusLogObj")
        private List<LeadStatusLogObj> LeadStatusLogObj;
        @Expose
        @SerializedName("LeadStatusId")
        private int LeadStatusId;
        @Expose
        @SerializedName("PrefDate")
        private String PrefDate;
        @Expose
        @SerializedName("ReferenceName")
        private String ReferenceName;
        @Expose
        @SerializedName("ServiceName")
        private String ServiceName;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("AudioFilePath")
        private String AudioFilePath;
        @Expose
        @SerializedName("AssignedTo")
        private String AssignedTo;
        @Expose
        @SerializedName("FollowUpNotes")
        private String FollowUpNotes;
        @Expose
        @SerializedName("PinCode")
        private String PinCode;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("LocDescription")
        private String LocDescription;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("LocName")
        private String LocName;
        @Expose
        @SerializedName("OTP")
        private int OTP;
        @Expose
        @SerializedName("LeadTime")
        private String LeadTime;
        @Expose
        @SerializedName("LeadDate")
        private String LeadDate;
        @Expose
        @SerializedName("ServicesId")
        private int ServicesId;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("MobileNumber")
        private String MobileNumber;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("LeadSource")
        private String LeadSource;
        @Expose
        @SerializedName("LeadStatus")
        private String LeadStatus;
        @Expose
        @SerializedName("LeadState")
        private int LeadState;
        @Expose
        @SerializedName("LeadType")
        private String LeadType;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("ReferenceId")
        private int ReferenceId;
        @Expose
        @SerializedName("CustomerDetailsid")
        private int CustomerDetailsid;
        @Expose
        @SerializedName("LocationId")
        private int LocationId;
        @Expose
        @SerializedName("LeadNo")
        private int LeadNo;
        @Expose
        @SerializedName("LeadId")
        private int LeadId;

        public String getPhotoPath2() {
            return PhotoPath2;
        }

        public void setPhotoPath2(String PhotoPath2) {
            this.PhotoPath2 = PhotoPath2;
        }

        public String getPhotoPath1() {
            return PhotoPath1;
        }

        public void setPhotoPath1(String PhotoPath1) {
            this.PhotoPath1 = PhotoPath1;
        }

        public String getPhotoPath() {
            return PhotoPath;
        }

        public void setPhotoPath(String PhotoPath) {
            this.PhotoPath = PhotoPath;
        }

        public List<LeadStatusLogObj> getLeadStatusLogObj() {
            return LeadStatusLogObj;
        }

        public void setLeadStatusLogObj(List<LeadStatusLogObj> LeadStatusLogObj) {
            this.LeadStatusLogObj = LeadStatusLogObj;
        }

        public int getLeadStatusId() {
            return LeadStatusId;
        }

        public void setLeadStatusId(int LeadStatusId) {
            this.LeadStatusId = LeadStatusId;
        }

        public String getPrefDate() {
            return PrefDate;
        }

        public void setPrefDate(String PrefDate) {
            this.PrefDate = PrefDate;
        }

        public String getReferenceName() {
            return ReferenceName;
        }

        public void setReferenceName(String ReferenceName) {
            this.ReferenceName = ReferenceName;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getAudioFilePath() {
            return AudioFilePath;
        }

        public void setAudioFilePath(String AudioFilePath) {
            this.AudioFilePath = AudioFilePath;
        }

        public String getAssignedTo() {
            return AssignedTo;
        }

        public void setAssignedTo(String AssignedTo) {
            this.AssignedTo = AssignedTo;
        }

        public String getFollowUpNotes() {
            return FollowUpNotes;
        }

        public void setFollowUpNotes(String FollowUpNotes) {
            this.FollowUpNotes = FollowUpNotes;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String PinCode) {
            this.PinCode = PinCode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLocDescription() {
            return LocDescription;
        }

        public void setLocDescription(String LocDescription) {
            this.LocDescription = LocDescription;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLocName() {
            return LocName;
        }

        public void setLocName(String LocName) {
            this.LocName = LocName;
        }

        public int getOTP() {
            return OTP;
        }

        public void setOTP(int OTP) {
            this.OTP = OTP;
        }

        public String getLeadTime() {
            return LeadTime;
        }

        public void setLeadTime(String LeadTime) {
            this.LeadTime = LeadTime;
        }

        public String getLeadDate() {
            return LeadDate;
        }

        public void setLeadDate(String LeadDate) {
            this.LeadDate = LeadDate;
        }

        public int getServicesId() {
            return ServicesId;
        }

        public void setServicesId(int ServicesId) {
            this.ServicesId = ServicesId;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String MobileNumber) {
            this.MobileNumber = MobileNumber;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getLeadSource() {
            return LeadSource;
        }

        public void setLeadSource(String LeadSource) {
            this.LeadSource = LeadSource;
        }

        public String getLeadStatus() {
            return LeadStatus;
        }

        public void setLeadStatus(String LeadStatus) {
            this.LeadStatus = LeadStatus;
        }

        public int getLeadState() {
            return LeadState;
        }

        public void setLeadState(int LeadState) {
            this.LeadState = LeadState;
        }

        public String getLeadType() {
            return LeadType;
        }

        public void setLeadType(String LeadType) {
            this.LeadType = LeadType;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getReferenceId() {
            return ReferenceId;
        }

        public void setReferenceId(int ReferenceId) {
            this.ReferenceId = ReferenceId;
        }

        public int getCustomerDetailsid() {
            return CustomerDetailsid;
        }

        public void setCustomerDetailsid(int CustomerDetailsid) {
            this.CustomerDetailsid = CustomerDetailsid;
        }

        public int getLocationId() {
            return LocationId;
        }

        public void setLocationId(int LocationId) {
            this.LocationId = LocationId;
        }

        public int getLeadNo() {
            return LeadNo;
        }

        public void setLeadNo(int LeadNo) {
            this.LeadNo = LeadNo;
        }

        public int getLeadId() {
            return LeadId;
        }

        public void setLeadId(int LeadId) {
            this.LeadId = LeadId;
        }
    }

    public static class LeadStatusLogObj {
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private int UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("LeadNotes")
        private String LeadNotes;
        @Expose
        @SerializedName("LeadStatusName")
        private String LeadStatusName;
        @Expose
        @SerializedName("LeadStatusId")
        private int LeadStatusId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("LeadId")
        private int LeadId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(int UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getLeadNotes() {
            return LeadNotes;
        }

        public void setLeadNotes(String LeadNotes) {
            this.LeadNotes = LeadNotes;
        }

        public String getLeadStatusName() {
            return LeadStatusName;
        }

        public void setLeadStatusName(String LeadStatusName) {
            this.LeadStatusName = LeadStatusName;
        }

        public int getLeadStatusId() {
            return LeadStatusId;
        }

        public void setLeadStatusId(int LeadStatusId) {
            this.LeadStatusId = LeadStatusId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getLeadId() {
            return LeadId;
        }

        public void setLeadId(int LeadId) {
            this.LeadId = LeadId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
