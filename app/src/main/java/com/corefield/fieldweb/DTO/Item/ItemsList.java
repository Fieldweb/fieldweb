package com.corefield.fieldweb.DTO.Item;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * DTO
 *
 * @author CoreField
 * @version 1.1
 * @implNote JSON serialized data transfer object to get all item list from item inventory (Owner login)
 */
public class ItemsList {
    @Expose
    @SerializedName("ResultData")
    private List<ResultData> ResultData;
    @Expose
    @SerializedName("RecordCount")
    private int RecordCount;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("PageIndex")
    private int PageIndex;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Code")
    private String Code;

    public List<ResultData> getResultData() {
        return ResultData;
    }

    public void setResultData(List<ResultData> ResultData) {
        this.ResultData = ResultData;
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int RecordCount) {
        this.RecordCount = RecordCount;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int PageIndex) {
        this.PageIndex = PageIndex;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public static class ResultData {
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("UsedItemDetailsDtoObj")
        private UsedItemDetailsDtoObj UsedItemDetailsDtoObj;
        @Expose
        @SerializedName("ImageFileBase64Str")
        private String ImageFileBase64Str;
        @Expose
        @SerializedName("ImageFileName")
        private String ImageFileName;
        @Expose
        @SerializedName("ItemImagePath")
        private String ItemImagePath;
        @Expose
        @SerializedName("PurchasePrice")
        private int PurchasePrice;
        @Expose
        @SerializedName("SalesPrice")
        private int SalesPrice;
        @Expose
        @SerializedName("ItemUnitTypeName")
        private String ItemUnitTypeName;
        @Expose
        @SerializedName("ItemUnitTypeId")
        private int ItemUnitTypeId;
        @Expose
        @SerializedName("TechFullName")
        private String TechFullName;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("UnAssignedQuantity")
        private int UnAssignedQuantity;
        @Expose
        @SerializedName("AssignedQuantity")
        private int AssignedQuantity;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("OwnerId")
        private int OwnerId;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdatedBy")
        private String UpdatedBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("ItemType")
        private String ItemType;
        @Expose
        @SerializedName("ItemTypeId")
        private int ItemTypeId;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public UsedItemDetailsDtoObj getUsedItemDetailsDtoObj() {
            return UsedItemDetailsDtoObj;
        }

        public void setUsedItemDetailsDtoObj(UsedItemDetailsDtoObj UsedItemDetailsDtoObj) {
            this.UsedItemDetailsDtoObj = UsedItemDetailsDtoObj;
        }

        public String getImageFileBase64Str() {
            return ImageFileBase64Str;
        }

        public void setImageFileBase64Str(String ImageFileBase64Str) {
            this.ImageFileBase64Str = ImageFileBase64Str;
        }

        public String getImageFileName() {
            return ImageFileName;
        }

        public void setImageFileName(String ImageFileName) {
            this.ImageFileName = ImageFileName;
        }

        public String getItemImagePath() {
            return ItemImagePath;
        }

        public void setItemImagePath(String ItemImagePath) {
            this.ItemImagePath = ItemImagePath;
        }

        public int getPurchasePrice() {
            return PurchasePrice;
        }

        public void setPurchasePrice(int PurchasePrice) {
            this.PurchasePrice = PurchasePrice;
        }

        public int getSalesPrice() {
            return SalesPrice;
        }

        public void setSalesPrice(int SalesPrice) {
            this.SalesPrice = SalesPrice;
        }

        public String getItemUnitTypeName() {
            return ItemUnitTypeName;
        }

        public void setItemUnitTypeName(String ItemUnitTypeName) {
            this.ItemUnitTypeName = ItemUnitTypeName;
        }

        public int getItemUnitTypeId() {
            return ItemUnitTypeId;
        }

        public void setItemUnitTypeId(int ItemUnitTypeId) {
            this.ItemUnitTypeId = ItemUnitTypeId;
        }

        public String getTechFullName() {
            return TechFullName;
        }

        public void setTechFullName(String TechFullName) {
            this.TechFullName = TechFullName;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getUnAssignedQuantity() {
            return UnAssignedQuantity;
        }

        public void setUnAssignedQuantity(int UnAssignedQuantity) {
            this.UnAssignedQuantity = UnAssignedQuantity;
        }

        public int getAssignedQuantity() {
            return AssignedQuantity;
        }

        public void setAssignedQuantity(int AssignedQuantity) {
            this.AssignedQuantity = AssignedQuantity;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(int OwnerId) {
            this.OwnerId = OwnerId;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String UpdatedBy) {
            this.UpdatedBy = UpdatedBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getItemType() {
            return ItemType;
        }

        public void setItemType(String ItemType) {
            this.ItemType = ItemType;
        }

        public int getItemTypeId() {
            return ItemTypeId;
        }

        public void setItemTypeId(int ItemTypeId) {
            this.ItemTypeId = ItemTypeId;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        @Override
        public String toString() {
            return Name;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class UsedItemDetailsDtoObj {
        @Expose
        @SerializedName("Notes")
        private String Notes;
        @Expose
        @SerializedName("UpdatedDate")
        private String UpdatedDate;
        @Expose
        @SerializedName("UpdateBy")
        private int UpdateBy;
        @Expose
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("UsedQty")
        private int UsedQty;
        @Expose
        @SerializedName("AssignedQty")
        private int AssignedQty;
        @Expose
        @SerializedName("AvlQty")
        private int AvlQty;
        @Expose
        @SerializedName("TaskId")
        private int TaskId;
        @Expose
        @SerializedName("ItemIssuedId")
        private int ItemIssuedId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNotes() {
            return Notes;
        }

        public void setNotes(String Notes) {
            this.Notes = Notes;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public int getUpdateBy() {
            return UpdateBy;
        }

        public void setUpdateBy(int UpdateBy) {
            this.UpdateBy = UpdateBy;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getUsedQty() {
            return UsedQty;
        }

        public void setUsedQty(int UsedQty) {
            this.UsedQty = UsedQty;
        }

        public int getAssignedQty() {
            return AssignedQty;
        }

        public void setAssignedQty(int AssignedQty) {
            this.AssignedQty = AssignedQty;
        }

        public int getAvlQty() {
            return AvlQty;
        }

        public void setAvlQty(int AvlQty) {
            this.AvlQty = AvlQty;
        }

        public int getTaskId() {
            return TaskId;
        }

        public void setTaskId(int TaskId) {
            this.TaskId = TaskId;
        }

        public int getItemIssuedId() {
            return ItemIssuedId;
        }

        public void setItemIssuedId(int ItemIssuedId) {
            this.ItemIssuedId = ItemIssuedId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
